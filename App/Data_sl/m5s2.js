import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je bančni račun?</Text>
                <Text style={{marginTop: 10}}>Bančni račun je varno in uporabno mesto, po vaši izbiri, za polog vašega denarja. Do svojega denarja lahko dostopate iz katerega koli bankomata. Olajša
                    vam tudi varčevanje in vlaganje svojega denarja za prihodnost.
                    {"\n\n"}Vrste bančnih računov
                    {"\n"}Večina bank in kreditnih združb ponuja naslednje vrste računov:
                </Text>
                <FlatList
                    data={[

                        {key: '1.  Varčevalni računi'},
                        {key: '2.  Tekoči račun'},
                        {key: '3.  Račun denarnega trga'},
                        {key: '4.  Denarna vloga (CDs)'},
                        {key: '5.  Zapadli računi'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>Kaj je tekoči račun? Kaj je mobilno in spletno bančništvo?</Text>
                <Text style={{marginTop: 10}}>Tekoči račun omogoča preprost dostop do vašega denarja za dnevne potrebe po transakciji in pomaga pri zagotavljanju varnosti vašega denarja. Stranke lahko
                    za nakupe ali plačevanje računov uporabijo debetno kartico ali čeke. Obstaja več možnosti ali paketov za račune, da se izognete mesečnim provizijam za storitve. Če želite določiti
                    najbolj ekonomično izbiro, primerjajte prednosti različnih paketov preverjanja s storitvami, ki jih dejansko potrebujete.
                    {"\n\n"}Mobilno bančništvo omogoča, da namesto računalnika uporabljate številne dejavnosti spletnega bančništva na pametnem telefonu ali tabličnem računalniku. Vsestranskost
                    mobilnega bančništva vključuje:
                </Text>
                <FlatList
                    data={[

                        {key: 'Prijavo na mobilno spletno mesto banke'},
                        {key: 'Uporabo aplikacije za mobilno bančništvo'},
                        {key: 'Bančništvo s storitvijo SMS'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Spletno bančništvo se nanaša na vse bančne transakcije, ki se lahko izvajajo preko spleta, običajno prek spletne strani banke pod zasebnim profilom ob
                    uporabi namiznega ali prenosnega računalnika. Značilnosti spletnega bančništva:</Text>
                <FlatList
                    data={[

                        {key: 'Finančne transakcije prek varnega spletnega mesta banke.'},
                        {key: 'Fizične lokacije podružnic ali samo spletne lokacije.'},
                        {key: 'Uporabnik mora ustvariti ID za prijavo in geslo.'},

                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
