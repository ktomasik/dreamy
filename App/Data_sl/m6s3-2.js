import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S3_2 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Izdajanje računov na mobilnih napravah</Text>
                <Text style={styles.dmlH3}>Za iOS</Text>
                <Text style={{marginTop: 10,fontSize: 18}}><Text style={{fontWeight:'bold'}}>2. korak:</Text>Kliknite na gumb Prenesi za vašo izbrano predlogo in vnesite geslo za Apple ID.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_20_3319624.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
