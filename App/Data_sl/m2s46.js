import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S46 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Naredite Twitter račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 16: Kliknite na besedilo in vpišite svoje besedilo, ki bi ga radi objavili… Ko ste napisali kratko objavo kliknite gumb Tweet.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img94.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img95.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img96.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
