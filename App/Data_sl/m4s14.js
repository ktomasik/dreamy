import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Prijava k strokovnim in poklicnim združenjem</Text>
                <Text style={{marginTop: 10}}>Sledite navodilom za registracijo poklicnih in strokovnih združenj:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>TDavčni urad vas vodi do ustrezne zbornice,{"\n"} kjer se lahko vloži prijavo za poklicna{"\n"}in strokovna združenja.
                        </Text>
                    </View>
                </View>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 320}}
                        source={require('../Images/images/30321.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
