import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S41_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Naredite Twitter račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 6: Označite Poveži s poznanimi ljudmi (Connect with people you know), da boste bolj učinkovito delili sporočila in klikni Naprej
                    (Next).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img80.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
