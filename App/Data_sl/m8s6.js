import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>1.5 Prevozni dokumenti – Carinska deklaracija in obrazci</Text>
                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Kaj je carinska deklaracija?</Text>
                <Text>Carinska deklaracija je uradni dokument, na katerem so navedeni izdelki in daje podrobno informacijo o blagu, ki se uvaža ali izvaža.
                    {"\n\n"}S pravnega vidika je carinska deklaracija dejanje, s katerim stranka privoli v plasiranje blaga v dan carinski postopek (European Commission 2018).
                </Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Kdo naj predloži carinsko deklaracijo?</Text>
                <Text>Na splošno lastnik blaga ali oseba, ki deluje v njegovem imenu (predstavnik ). Tudi oseba, ki ima nadzor nad blagom jo lahko opravi. Te osebe so lahko predstavniki ali podjetja,
                    tudi združenja oseb v določenih primerih (European Commission 2018).
                </Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Kje je treba vložiti carinsko deklaracijo?</Text>
                <Text>Carinsko deklaracijo je treba vložiti na carinskem uradu, kjer je blago bilo ali bo kmalu predstavljeno (European Commission 2018).</Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Kje je treba vložiti carinsko deklaracijo?</Text>
                <Text>Za izpolnitev zakonskih obveznosti in predložitev blaga v carinski postopek , je potrebno vložiti carinsko deklaracijo (European Commission 2018). To je potrebno storiti v dveh
                    primerih: </Text>
                <FlatList
                    data={[
                        {key: 'Pri uvozu, ko se blago vnese v carinsko območje , mora biti potrjeno s carinskim dovoljenjem o rabi ali uporabi'},
                        {key: 'In blago namenjeno izvozu – mora predložiti v izvozni postopek'},
                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
