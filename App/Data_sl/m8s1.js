import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M8S1 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Učinkovita strategija prevoza in izvedba</Text>
                <Text style={{marginTop: 10}}>Transport je eden od ključnih korakov dobave izdelkov strankam z ugodnimi stroški od izdelave, dostave in prejema plačila zanje. Da bi dostavili blago varno z minimalnimi stroški do stranke, vplivajo na to naslednji faktorji: destinacija, specifikacija vsebine, masa, dimenzije idr.
                    {"\n\n"}Stroški pošiljanja so eden od največih stroškov za mala podjetja in podjetnike, a prav dostava in izpolnjevanje naročil pomagata podjetnikom, da lahko zadovoljijo svoje stranke pravočasno in po dostopnih cenah.
                    {"\n\n"}Izračun cen za mala podjetja je odvisen od velikosti in količine pošiljk, ki jih posredujejo transportna podjetja. Če obseg pošiljk narašča, se mora cena v osnovi znižati. Kakorkoli, možna so pogajanja pri številnih logističnih partnerjih. Za mnoga podjetja so na voljo članski računi, ki pomagajo znižati stroške pošiljanja.
                    {"\n\n"}Uporaba standardnih paketov, s katerimi nas zalagajo logistična podjetja, nas zaščiti pred plačilom dodatnih stroškov v primeru nestandardnih dimenzij. Podjetjem lahko dobavijo različno embalažo za različne izdelke. Najpogostejši so: plastične vrečke, kartonske škatle, embalaža z zračnimi mehurčki za občutljive izdelke, kartonske role za slike in papir različnih dimenzij in velikosti.
                    {"\n\n"}Strategija pošiljk in izpolnjevanja naročil sta integralni del donosne prodaje. Če sta pravilno izvedeni, lahko strategija transporta in pakiranje pomagata pospešiti ponovitev naročil in hkrati pomagata pridobiti nove stranke.
                </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Produkcija
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Doseg strank
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Potrditev & Plačilo
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Transport
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
