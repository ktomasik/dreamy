import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img6.png')
    }
}];

export default class M9S3_9 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Namestitev Facebook strani za podjetje</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Korak 8:</Text> Dodajte tipko “Call-to-Action / poziv k dejanju” na vašo Facebook stran. Zahvaljujoč
                    “Call-to-Action /poziv k dejanju” tipkam je, da vaše stranke lahko komunicirajo z vami preko e-pošte, telefona ali spleta in lahko opravijo nakup….
                    {"\n\n"}Če želite na svojo stran dodati tipko “Call-to-Action /poziv k dejanju”:

                    {"\n\n"}To add call to action button to your page
                </Text>
                <FlatList
                    data={[
                        {key: 'Pod krovno fotografijo na vaši strani kliknite “Add a Button / Dodaj gumb”'},
                        {key: 'V pojavnem meniju izberite gumb in sledite navodilom na zaslonu .'},
                        {key: 'Kliknite “Done / Končano”.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img6.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
