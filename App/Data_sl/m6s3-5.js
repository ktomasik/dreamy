import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img3.png')
    }
}];

export default class M6S3_5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Izdajanje računov na mobilnih napravah</Text>
                <Text style={styles.dmlH3}>Android in iOS</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text>Wave omogoča prikaz obeh pogledov za Android in Chrom v isti denarnici. Aplikacija, ki jo lahko dosežete s katerega koli mobilnega telefona, je opisana spodaj.</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>1. korak:</Text>V spletnem brskalniku Google vtipkajte “wave invoicing on iOS and Android”, kar v prevodu
                    pomeni "izdajanje računov na iOS in Android".</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img3.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
