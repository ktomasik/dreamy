import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Davčne obveznosti</Text>
                <Text style={{marginTop: 10}}>Sledite navodilom za izpolnitev davčnih obveznosti:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Oddaja vloge na davčni urad{"\n"}(naslov stalnega prebivališča)
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Pridobitev potrdila o davčni oprostitvi
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
