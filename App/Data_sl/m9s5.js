import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S5 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Instagram poslovni račun</Text>
                <Text>Oblikovanje računa: Pred tem je bilo razloženo, kako odpreti račun za Instagram v Modulu 2. Kot je opisano v Modulu 2, lahko prenesete aplikacijo Instagram iz App Store za iOS,
                    Google Play store za Android, ali Windows Phone Store za Windows telefone.
                    {"\n\n"} Ko je aplikacija naložena se dotaknite ikone za uporabo na vašem mobilnem telefonu, da odprete Instagram.

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
