import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Donacije za mala podjetja za ženske</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>
                <Text style={styles.dmlH3}>Slovenski podjetniški sklad (Slovene Enterprise Fund)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Zagonska sredstva za novoustanovljena inovativna podjetja (P2A in P2B): začetni kapital za nova inovativna podjetja, ugodni viri financiranja razvojnih investicijskih podjetij (subvencije, garancije), spodbujanje zasebnih naložb (lastniški kapital, posojila, garancije).',
                                val2: 'https://www.podjetniskisklad.si/en'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Agencija RS za kmetijske trge in razvoj podeželja</Text>
                <Text>Podpora za ustanavljanje in razvoj mikropodjetij.</Text>
                <Text style={styles.dmlH3}>Zavod RS za zaposlovanje</Text>
                <Text>Subvencije za samozaposlitev / občasno zagotavljanje subvencij za samozaposlitev.</Text>
                <Text style={styles.dmlH3}>Slovenski regionalno razvojni sklad</Text>
                <Text>Finančne spodbude, zlasti v obliki vračljivih sredstev, za začetne naložbe na področju podjetništva, kmetijstva, regionalnega razvoja, finančnih naložb v regionalne jamstvene
                    sheme, projektov predhodnega financiranja z odobrenimi evropskimi sredstvi.</Text>
                <Text style={styles.dmlH3}>SID banka</Text>
                <Text>Zagotavljanje ugodnih finančnih virov za podjetja, zavarovanje izvoza.</Text>
                <Text style={styles.dmlH3}>Eko sklad </Text>
                <Text>Zagotavljanje ugodnih finančnih sredstev za naložbe v okoljsko usmerjene projekte in energetsko učinkovitost.</Text>
            </View>
        )
    }
}
