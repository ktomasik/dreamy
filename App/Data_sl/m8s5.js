import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img8.png')
    }
}];

export default class M8S5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Pakiranje & Trženje</Text>
                <Text style={styles.dmlH3}>Nalepke za posebno ravnanje</Text>
                <Text>Nalepke za dostavo in posebno ravnanje so že natisnjene nalepke, ki označujejo ustrezne smernice pri posebnem ravnanju in v nekaterih primerih tudi informacije o destinaciji. Na
                    nalepkah se lahko označi vsebino paketa, kot so vnetljivost ali krhkost ali označuje smer postavitve (Anonymous 2018j). Pošiljke in nalepke za posebno ravnanje opozarjajo osebe pri
                    prevozu, z vsemi potrebnimi navodili in ravnanjem. Obstaja več tipov nalepk za posebno ravnanje s pošiljkami, vključno s prikazanim spodaj.
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>E-mail sporočila in male paketne oznake </Text>se uporabljajo, da bi opozorili osebje na posebno ravnanje s paketi, poslanimi preko
                    spleta (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Proizvodne oznake in male paketne nalepke </Text>bodo pritegnile pozornost pri paketih skozi postopek izdelave in pošiljanja (Anonymous
                    2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Mednarodne slikovne oznake </Text>se uporabljajo za mednarodni prevoz blaga. Slikovne nalepke zmanjšujejo tveganje napak razumevanja, saj
                    osebje drugih držav morda ne razume jezika države izvora paketa, vendar je za pravilno ravnanje priporočljivo uporabiti tudi nalepke v jeziku prejemnika. Oznake je potrebno
                    uporabljati za zagotavljanje skladnosti z okoljskimi in varnostnimi standardi ter za prepoznavanje nevarnih snovi (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Nalepke s puščico navzgor </Text>pomagajo zaščiti občutljive pakete, da varno prispejo. Nalepke s puščico navzgor so del mednarodnih
                    slikovnih oznak (Anonymous 2018j). </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img2.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Nalepke z oznako Krhko/Steklo </Text>opozarjajo na vsebino paketa in na posebno ravnanje, kot je “tega na koncu”
                    (Anonymous 2018j). </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img3.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Nalepke za hiter prevoz </Text>se uporabljajo pri pošiljkah odvisnih od temperature ali pri zdravilih. Pokvarljiva
                    zdravila, ki morajo biti na hladnem in se uporabljajo pri urgentnih situacijah, pogosto nosijo nalepke za hiter prevoz (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img4.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img5.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Nalepke, ki obveščajo o temperaturi, </Text>naročajo osebju pri transportu, kako pravilno ravnati v primeru zahtevane
                    temperature. Te nalepke se uporabljajo za zamrznjeno in/ali hitro pokvarljivo blago (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img6.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img7.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Nalepke za nevarno blago </Text>označujejo pakete z nevarnim blagom. Pri prevozu opozarjajo osebje na ločeno
                    shranjevanje blaga in potrebo po segregaciji. Obstajajo strogi predpisi za prevoz nevarnega blaga in pogosto je potrebna posebna dokumentacija, kjer so navedeni kemični podatki.
                    Nalepke za nevarno blago je potrebno uporabljati za: eksplozivno blago, pline, vnetljive tekočine, vnetljive trdne snovi, snovi ki so podvržene samovžigu ali z vodo reaktivne
                    snovi, strupene in jedke snovi (Anonymous 2018j). </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Nalepke za biološko nevarne snovi </Text>kot so biološki odpadki, človeški in živalski vzorci in uporabljena
                    laboratorijska oprema (Anonymous 2018j). </Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Nalepke za posebno ravnanje </Text>vključujejo vsa potrebna navodila za pošiljanje in ravnanje. Nalepke so lahko
                    narejene po želji stranke s točno določenimi navodili. Pogosto so opozorilne nalepke ali navodila za pošiljanje (Anonymous 2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Oblikovane nalepke: </Text>Nalepke se uporabljajo za tri strani paketa, najpogosteje s strani, in/ ali na dnu in vrhu (Anonymous 2018j).
                    {"\n\n"}Če blago zahteva posebno ravnanje ali skladiščenje, mora biti to označeno na paketu in navedeno na nakladnici (Anonymous 2018j).
                </Text>
            </View>
        )
    }
}
