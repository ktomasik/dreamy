import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Mikrofinančni in drugi zagonski skladi za ženske</Text>
                <Text style={styles.dmlH2}>Za Francijo:</Text>
                <Text>Posojilo, ki je na splošno manjše od 25.000 EUR je namenjeno osebam, ki želijo ustvariti ali prevzeti dejavnost, vendar njihova sredstva ne zadostujejo za pridobitev
                    konvencionalnega posojila. Za pridobitev mikrokredita mora posojilojemalec spremljati strokovna in kompetentna podporna mreža, kot so: "France Active", "Francija pobuda",
                    "Boutiques de Gestion" ali "Fundacija 2e Chance". Ta omrežja mu bodo pomagala vzpostaviti svoj projekt, raziskati njegovo prošnjo za financiranje in razviti svojo dejavnost. Glavni
                    akter je ADIE (Združenje za razvoj gospodarske pobude).</Text>
                <Text style={styles.dmlH3}>ADIE</Text>
                <Text>Ozavešča, usmerja in obvešča ženske o ustvarjanju podjetij. Od leta 2015 organizira letno kampanjo ozaveščanja žensk.
                    {"\n\n"}Spodbuja financiranje nosilcev projektov s spremljajočimi mikrokrediti za podjetja, ki nimajo dostopa do bančnih posojil.
                    {"\n\n"}Krepi podporo ustvarjalcem podjetij s programi usposabljanja in ozaveščenosti, prilagojenimi njihovim posebnostim.
                </Text>
                <FlatList
                    data={[
                        {key: 'https://www.adie.org/nos-actions/pour-les-femmes'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />

                <Text style={styles.dmlH3}>FRANCE ACTIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'France Active, ki podpira in financira podjetja je že skoraj 30 let, je lani je mobiliziralo 270 milijonov evrov za servisiranje 7.400 podjetij. Je veliko več kot le omrežje, France Active je resnično gibanje zavezanih podjetnikov, katerih ambicija je ustvariti bolj vključujočo se družbo. Poslanstvo France Active je pospešiti uspeh podjetnikov, tako da jim daje sredstva za vključitev.',
                                val2: 'https://www.franceactive.org/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />


                <Text style={styles.dmlH3}>FRANCE INITIATIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Po imenovanju Francoske mreže za pobudo in zatem še Francoske pobude, je mreža preoblikovala svoj sistem blagovnih znamk. Od 1. oktobra 2012 se nacionalno združenje imenuje Iniciative France. Lokalne platforme in regionalni koordinatorji izvajajo enake spremembe. To je več kot le preobrat besed. Ime zdaj poudarja izraz, ki je skupen vsem: pobuda, ob prikazovanju imena ozemlja. To spremlja logotip, ki grafično odraža moč nacionalne mreže in njeno raznolikost povezano z njenimi lokalnimi koreninami. Mreža prinaša podpis, ki daje našemu skupnemu dejanju poln pomen: "mreža, duh".',
                                val2: 'http://www.initiative-france.fr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>BGE</Text>
                <FlatList
                    data={[
                        {
                            key: {

                                val1: 'BGE že več kot 35 let podpira ustvarjanje in delo podjetij, da je resničnost dostopna vsem. S spremljanjem podjetnikov na vseh stopnjah od ustvarjanja, ustanovitve do razvoja poslovanja to omogoča vsakomur, ki tvega možnosti za uspeh. Kot mreža neprofitnih združenj je BGE sestavljen iz 50 združenj, ustanovljenih na ozemljih, ki odpirajo perspektive, zagotavljajo podjetniško pot in ustvarjajo trajne rešitve za zaposlovanje in lokalni razvoj.',
                                val2: 'http://www.bge.asso.fr/nous-sommes/notre-engagement.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>2nd CHANCE FOUNDATION</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Namen Fundacije 2nd Chance je podpora ljudem, starim med 18 in 62 let, ki so preživeli težke življenjske dogodke in so trenutno v zelo negotovi situaciji, vendar imajo resnično željo, da bi se vrnili nazaj. Fundacija za drugo priložnost jim nudi človeško in finančno podporo za izvedbo realističnega in trajnostnega strokovnega projekta: kvalificiranega usposabljanja, ustvarjanja ali prevzema podjetja.',
                                val2: 'http://www.deuxiemechance.org/fr'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
