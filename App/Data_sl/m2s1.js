import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je mreženje in zakaj je pomembno?</Text>
                <Text>V Oxfordovem slovarju (‘English Dictionary, Thesaurus, & Grammar Help | Oxford Dictionaries’ 2018) lahko zasledimo, da je mreženje “skupina ljudi, ki izmenjuje informacije,
                    kontakte in izkušnje tako za osebni interes kot tudi socialno širši”. Vendar, če boste vprašali deset različnih ljudi: “Kaj si predstavljajo pod tem terminom?”, boste dobili deset
                    različnih odgovorov. Najverjetneje je njihova osebna definicija medmreženja odvisna od načina in namena uporabe tovrstnih aktivnosti. Neglede na to kako in na kakšen način
                    uporabljate mreženje ali ga uporabljate za pridobivanje prijateljev, iskanje službe, pomoč za ustvarjanje vaše kariere, iskanje novih poslovnih možnosti ali enostavno le za
                    širjenje vašega obzorja… lahko na mreženje gledamo kot na izmenjavo informacij, kontaktov in izkušenj (WHAT Is Networking’, n.d.). V industriji ali na poklicni poti nam lahko
                    mreženje koristi pri navezovanju bolj osebnih kontaktov. To je spretnost, ki jo mora obvladati vsaka resna poslovna ženska 21. stoletja.Ne glede na to v katerem poklicu delujete
                    poznan je pravih ljudi se vam lahko obrestuje.
                    {"\n\n"}Če je mreženje pravilno načrtovano in izpeljano lahko vodi v povečano število vaših uporabnikov s tem si boste pridobili več priložnosti in vaše podjetje bo napredovalo. Za
                    ljudi, ki s svojim poslom šele začenjajo je mreženje izrednega pomena saj si lahko na ta način pridobijo prve stranke in povečajo odmevnost svojega podjetja(‘What Is Networking? |
                    Reed.co.uk’, n.d.).
                </Text>
                <Text style={styles.dmlH2}>Kaj so socialna omrežja?</Text>
                <Text>Socialna omrežja so računalniška orodja, ki jih lahko uporabljamo za namen širjenja idej in informacij. S temi orodji je možno tudi graditi virtualna omrežja in skupnosti. Zato
                    so zasnovana tako, da na podlagi spletnih povezav omogočajo uporabniku enostavno izmenjevanje informacij in tudi drugih vsebin kot so na primer slike in videoposnetki. Uporabniki
                    za ta namen lahko uporabljajo spletne programe, ki so dostopni preko računalnikov pametnih tablic in pametnih telefonov.
                    {"\n\n"}Prvotno so ljudje uporabljali socialna omrežja za ohranjanje stikov s prijatelji in družinskimi člani. Kasneje je to obliko komunikacije prevzel tudi poslovni svet in
                    popularno metodo obveščanja uporabil za nagovarjanje množice ljudi kot potencialnih kupcev. Prav v tem se kažejo prednosti uporabe socialnih omrežij s katerimi lahko danes delimo
                    informacije s komerkoli na zemlji če tudi oni uporabljajo ta orodja (Silver, n.d.).
                </Text>
                <Text style={styles.dmlH2}>Kratek zgodovinski pregled socialnih omrežij</Text>
                <Text>Razvoj socialnih omrežjih sega v leto 1970. Prvo tovrstno orodje se je imenovalo APRANET Ki ga je bilo mogoče uporabljati od leta 1969. V nekaj letih se je na tem omrežju razvila
                    bogata skupnost izmenjave informacij z nevladno in poslovno vsebino. Kmalu za tem v računalniškem laboratoriju MIT izdajo knjigo z naslovom “ A 1982 handbook on computing at MIT’s
                    AI Lab stated regarding network etiquette” v kateri prvič najdemo termin socialna omrežja. V čikagu so leta 1973 vzpostavili prvo elektronsko oglasno desko mesta. oglasna deska se
                    je imenovala Community Memory in in jo je leta 1979 zamenjala izpopolnjena z imenom Usenet. Do konca 70-ih let so imela vsa večja mesta po ameriki vsaj eno tako oglasno desko. Ta
                    spletna orodja so tekla na računalnikih kot so: TRS-80, Apple II, Atari, IBM PC, Commodore 64, Sinclair in podobno (‘Social Media - Wikipedia’, n.d.).
                    {"\n\n"}Nato se je leta 1981 predstavilo podjetje IBM PC, ki je splošnim uporabnikom ponudilo dostopnejše osebne računalnike. S tem se je interes po spletnih vsebinah izredno
                    povečal in pričela se je razvijati tudi namenska strojna oprema kot na primer modemi, ki so omogočali sočasnost večjih uporabnikov v medmrežju. Compuserve, Prodigy in AOL So bila
                    največja podjetja ki so omogočala elektronske oglasne deske in so v 90ih letih prva ponudila internetno uporabo. V teh letih so elektronske oglasne deske imele več deset tisoč
                    uporabnikov le v severni Ameriki. V tem obdobju so se začele razvijati tudi druge oblike socialnih omrežij, kot so naprimer forumi (Edvards 2016), ki so pritegnili veliko več
                    uporabnikov kot predhodne elektronske oglasne deske(‘Social Media - Wikipedia’, n.d.).
                    {"\n\n"}V novembru leta 1994 je podjetje GepCities ponudilo prvo spletno stran z vsebinami socialnega mreženja. Kmalu zatem je takemu načinu izmenjavi informacij sledilo veliko
                    ponudnikov: Classmates (1995), Six Degrees (1997), Open Diary (1998), LiveJournal (1999), Ryze (2001), Friendster (2002), LinkedIn (2003), hi5 (2003), MySpace (2003), Orkut (2004),
                    Facebook (2004), Yahoo! 360° (2005), Bebo (2005), Twitter (2006), Tumblr (2007), and Google+ (2010) (‘December 1995: Classmates - Then and Now: A History of Social Networking Sites
                    - Pictures - Cbs News’, n.d.; ‘History and Different Types of Social Media’, n.d.; Ortutay 2012).
                </Text>
            </View>
        )
    }
}
