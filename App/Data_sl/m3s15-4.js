import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Upoštevajte spodnja navodila za prodajo po najboljši ceni v spletnem okolju</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>4. Korak: Opozorilo o popustih ob nakupu dveh ali več izdelkov istočasno, napišite vrednost za zmanjšanje</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/24911.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
