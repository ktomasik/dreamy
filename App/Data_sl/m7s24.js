import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S24 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Mikrofinančni in drugi zagonski skladi za ženske</Text>
                <Text style={styles.dmlH2}>Za Grčijo:</Text>
                <Text style={styles.dmlH3}>Ljudski sklad</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Ljudski sklad ponuja mikro dotacije grškim podjetnikom, ki želijo ustvariti novo podjetje ali razviti obstoječe podjetje, vendar imajo težave pri dostopu do posojila. Donacija znaša do 10.000 EUR na podjetje za začetni kapital novega podjetja ali delovnega kapitala za obstoječe podjetje. Ta program financiranja se osredotoča na skupine z nizkim dostopom do drugih oblik financiranja.',
                                val2: 'http://www.thepeoplestrust.org'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Microfinancing (AFI & Eurobank)</Text>
                <Text>AFI (Action Finance Initiative) je civilna nepridobitna družba. Leta 2014 sta jo v Grčiji ustanovili ActionAid Hellas in francoska organizacija ADIE, vodilni v mikrokreditih v
                    Evropi.</Text>
                <Text>Eurobank sodeluje z AFI za pomoč pri dolgoročno brezposelnih osebah, ki pripadajo ranljivim kategorijam državljanov in mikropodjetjem brez dostopa do bančnih posojil (do 15.000
                    evrov). Ponujajo jim priložnost, da ustvarijo lastno službo (samozaposlitev) ali razvijejo majhne poslovne enote in ustvarijo nova delovna mesta.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'AFI prevzema predizbor, usposabljanje in mentorstvo kandidatov, Eurobank pa prevzema kreditni nadzor in financiranje.',
                                val2: 'https://www.eurobank.gr/el/business/proionta-kai-upiresies/proionta-upiresies/xrimatodotiseis/anaptuksiaka/easy-afi'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
