import React, {Component} from 'react'
import {Text, View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S15 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Viri in literatura</Text>
                <FlatList
                    data={[
                        {key: 'https://www.techopedia.com/definition/15802/email-virus'},
                        {key: 'https://www.techopedia.com/definition/4928/web-server'},
                        {key: 'https://www.avira.com/en/support-what-is-email-spam'},
                        {key: 'http://support.melbourneit.com.au/articles/help/How-do-I-setup-my-Exchange-email-on-my-iPhone'},
                        {key: 'https://support.apple.com/en-us/HT201320'},
                        {key: 'https://help.one.com/hc/en-us/articles/115005863205-Setting-up-email-in-Gmail-on-Android'},
                        {key: 'https://ccm.net/faq/34658-android-enable-the-contact-recognition-feature'},
                        {key: 'https://www.samsung.com/uk/support/mobile-devices/how-can-i-quickly-make-changes-to-the-apps-menu-on-my-samsung-galaxy-alpha/'},
                        {key: 'https://www.techopedia.com/definition/2419/internet'},
                        {key: 'https://ocw.metu.edu.tr/pluginfile.php/348/mod_resource/content/0/Lecture_1.pdf'},
                        {key: 'https://en.wikipedia.org/wiki/Website'},
                        {key: 'https://en.wikipedia.org/wiki/Web_page'},
                        {key: 'https://www.quora.com/What-is-the-difference-between-the-World-Wide-Web-and-the-Internet'},
                        {key: 'https://www.collinsdictionary.com/dictionary/english/e-mail'},
                        {key: 'https://www.webopedia.com/TERM/_/_sign.html'},
                        {key: 'https://www.webopedia.com/TERM/W/World_Wide_Web.html'},
                        {key: 'https://en.wikipedia.org/wiki/Web_search_engine'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
