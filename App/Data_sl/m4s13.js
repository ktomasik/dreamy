import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";


export default class M4S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Pridobivanje davčne številke</Text>
                <Text style={{marginTop: 10}}>Upoštevajte navodila za pridobitev davčne številke:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Vložite zahtevo v davčnem uradu{"\n"}(v kraju kjer živite)
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Prijavite naslov stalnega prebivališča
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Prikaz preproste obdavčitve
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Priskrbeti si račun iz ustreznih zbornic (ni treba, da to storite sami)
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
