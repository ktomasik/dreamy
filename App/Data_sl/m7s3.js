import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je mikrofinanciranje? </Text>
                <Text>Mikrofinanciranje je krovni izraz, ki se uporablja za označevanje finančnih produktov in storitev za ljudi, ki so izključeni iz tradicionalnega bančnega kroga. Takšnim ljudem
                    mikrofinanciranje omogoča, da financirajo svoje preživetje, varčujejo, zagotavljajo preživetje svoje družine in se ščitijo pred vsakdanjimi življenjskimi tveganji.</Text>
                <Text style={styles.dmlH2}>Kako mikrofinanciranje deluje?</Text>
                <Text>Mikrofinančne institucije nudijo posojilojemalcem mikro posojila skupaj s pomočjo (financiranje novega podjetja ali načrta širitve, plačevanje nujnih družinskih potreb, olajšanje
                    mobilnosti za pridobitev službe itd.), kljub dejstvu, da ti posojilojemalci ne ponujajo trdnega jamstva povračila. Prihodki, ustvarjeni z gospodarsko dejavnostjo mikroposojil,
                    omogočajo, da odplačajo posojilno bilanco.</Text>
                <Text style={styles.dmlH2}>Kako spodbuja ženske podjetnice prispevek mikrofinanciranja?</Text>
                <Text>Prispevek mikrofinanciranja je pomoč revnim ljudem, vključno z ženskami pri pridobivanju zaposlitve, povečanju zaupanja, krepitvi komunikacijskih veščin in drugih vidikih. Ženske pridobijo večji nadzor nad sredstvi, kot so materialna posest; intelektualnimi viri kot so znanje, informacije, ideje in izvajanjem odločitev doma, v skupnosti, v družbi z vključevanjem v te mikrofinančne programe.
                    {"\n\n"}Mikrofinanciranje je pomembno orodje za podjetniško krepitev vloge žensk v perspektivi virov. Posojila skupinam, ki imajo člane s pomočjo mrežnega povezovanja, so pomembno orodje za zmanjševanje revščine,
                </Text>
            </View>
        )
    }
}
