import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Kje lahko najdete posojila za majhna podjetja za ženske?</Text>
                <Text style={styles.dmlH2}>Za Slovenijo:</Text>
                <Text style={styles.dmlH3}>Banke</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Enostavno in hitro spletno posojilo do 7000 EUR brez stroškov odobritve.',val2: 'http://www.hipkredit.si/'}},
                        {key: {val1: 'Mikrokrediti z nizko obrestno mero in stroški z ugodnimi fiksnimi mesečnimi stroški posojila v celotnem obdobju njegovega odplačevanja.', val2: 'http://www.intesasanpaolobank.si/'}},
                        {key: {val1: 'Atraktivna ponudba za s.p.',val2:'http://www.sparkasse.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Finančne storitve</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Posojilo v vrednosti 500 ali 1000 EUR.',val2:'http://www.skupina8.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Regionalna gospodarska zbornica</Text>
                <FlatList
                    data={[
                    {key: {val1: 'Odobravanje posojil članom s subvencijo dela obrestne mere za kratkoročna posojila.',val2:'http://www.eng.gzs.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Podjetniški naložbeni center</Text>
                <FlatList
                        data={[
                        {key: {val1: 'Hitra, spletna posojila od 1000 EUR do 30000 EUR.',val2:'http://www.pnc.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                       onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
