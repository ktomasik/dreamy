import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Načini pošiljanja</Text>
                <Text style={{marginTop: 10}}>Glede na območje, je transport razdeljen v dve skupini: na domači in mednarodni prevoz. Domači prevoz vključuje prevoz blaga po državi in blagovni
                    dokumenti so podrejeni nacionalni zakonodaji. Logistična podjetja prevzamejo pošiljke in jih dostavijo strankam. Glede na vrsto prevoznega sredstva so na voljo: letalski prevoz,
                    ladijski prevoz in cestni prevoz. Letalski in ladijski prevoz sta najbolj pogosti obliki prevoza blaga med celinami in za velike pošiljke. Poleg tega je cestni transport najbolj
                    pogosta oblika prevoza blaga med sosednjimi državami. Ročno izdelane izdelke manjših količin povezana logistična podjetja prevažajo v zbirnih pošiljkah s cestnim transportom. Morda
                    bo potreben račun ali plačan račun. V nekaterih primerih pošiljke potrebujejo deklaracijo po meri, lahko so potrebni še drugi dokumenti. Logistična podjetja lahko svetujejo in
                    izberejo pravo pot.</Text>
                <Text style={styles.dmlH3}>Ponudba brezplačnega pošiljanja</Text>
                <Text>Brezplačno pošiljanje je lahko opredeljeno z najmanjšo vrednostjo naročila ali minimalno količino. Ponudba brezplačnega pošiljanja je eden od načinov, s katerim lahko pritegnemo
                    pozornost pri strankah. Na splošno je znano, da objavljanje “Brezplačno pošilanje” s strain prodajalca zagotavlja pomembno prednost pred konkurenti na enakem področju. (Anonymous
                    2018a).</Text>
                <Text style={styles.dmlH3}>Cenejša dostava </Text>
                <Text>Pogodbeni dogovor z logističnim podjetjem za stalne prevoze lahko pomaga znižati transportne stroške glede na količino. Prav tako, lahko izbira najboljšega prevoznika za vsako
                    naročilo posebej pomeni nižje stroške. Lahko se nam zdi, da je treba vložiti veliko časa in intenzivnega dela v izbran prevoz za vsako naročilo, saj nam različni prevozniki
                    ponujajo drastične razlike pri ceni prevoza, ki temeljijo na masi, dimenzijah in destinaciji naših pošiljk. Pogajanje z različnimi logističnimi podjetji spodbuja ponudbo najbolj
                    konkurenčnih cen. Kratek pregled bodočih proizvodnih kapacitet in plana za prihodnost lahko učinkovito vpliva na stroške. Po drugi strani, ko so transportni stroški določeni z
                    izračunom mase in dimenzij, nam pomaga znižati stroške prevoza uporaba standardizirane embalaže, s strani logističnih podjetij.
                </Text>
            </View>
        )
    }
}
