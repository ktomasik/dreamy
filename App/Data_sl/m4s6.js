import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Davčne osnove</Text>
                <Text>Davčna obveznost je znesek, ki ga posameznik dolguje davčnim organom (Cameron 2017).
                    {"\n"}Vlada uporablja davčna plačila za financiranje socialnih programov in administrativnih vlog.
                    {"\n"}V osnovi je davčna obveznost običajno določen odstotek dohodka in se razlikuje glede na dohodek.

                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>TURČIJA</Text>
                <FlatList
                    data={[
                        {key: 'Treba je pripraviti izjavo o naslovu. Domači naslov je lahko naslov za račun.'},
                        {key: 'Preproste obdavčitve je potrebno med prijavo zmanjšati.'},
                        {key: 'Davčni urad pošlje davčnemu zavezancu davčno kartico.'},
                        {key: 'Prijavo je treba opraviti na Konfederaciji turških obrtnikov.'},
                        {key: 'Vlogo, z zahtevo za dovoljenje za delo, je treba poslati občini.'},
                        {key: 'Davčni zavezanci, ki so predmet obdavčitve malih podjetij, dobijo dokumente iz zbornic in združenj.'},
                        {key: 'Davčni zavezanci morajo svoje prihodke prijaviti z letno izjavo.'},
                        {key: 'Izjava je poslana davčnemu uradu, davčni zavezanec na podlagi  računa plača davek (National Report Turkey, 2018).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>SLOVENIJA</Text>
                <FlatList
                    data={[
                        {key: 'DDV (davek na dodano vrednost)'},
                        {key: 'Davek od dohodkov pravnih oseb'},
                        {key: 'Dohodnina'},
                        {key: 'Prispevek za socialno varstvo'},
                        {key: 'Davek na nepremičnine'},
                        {key: 'Davek na kapitalske deleže'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Izbira oblike podjetništva</Text>
                <FlatList
                    data={[
                        {key: 'Samostojni podjetnik, predmet obdavčitve je dohodek iz dejavnosti, ki izhaja iz izvajanja kakršnekoli podjetniške, kmetijske, gozdarske, poklicne ali druge samostojne dejavnosti.'},
                        {key: 'Družba z omejeno odgovornostjo je pravna oseba in je zato obdavčena z davkom od dobička pravnih oseb.'},
                        {key: 'Davki e-prodajalcev: Prodajalec, ki je davčni zavezanec je v Sloveniji identificiran za namene DDV in mora izračunati DDV od dostave izdelka kupcu v Sloveniji (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>POLJSKA</Text>
                <Text>Za posameznike, ki želijo voditi lastno dejavnost, se davčne obveznosti plačujejo na podlagi davka od dohodka, kot velja za zaposlene s polnim delovnim časom. Posameznik izbere
                    eno izmed naslednjih oblik obdavčitve:</Text>
                <FlatList
                    data={[
                        {key: 'Splošna pravila obdavčitve,'},
                        {key: '19 % davek (enotni davek),'},
                        {key: 'Pavšalni znesek iz registriranega dohodka,'},
                        {key: 'Davčna kartica (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>GRČIJA</Text>
                <FlatList
                    data={[
                        {key: 'Posamezniki in podjetja morajo predložiti elektronsko davčno napoved prek spletnega sistema  (Independent Authority of Public Revenue), na podlagi katerega bodo obdavčeni.'},
                        {key: 'Na koncu vsakega meseca predložijo seznam računov in DDV.'},
                        {key: 'Za posameznike, ki želijo voditi lastno dejavnost, se davčne obveznosti plačujejo na podlagi davka na dohodek, kot velja za zaposlene s polnim delovnim časom in upokojence, vendar brez davčnih olajša'},
                        {key: 'Ne glede na dobiček so vsi grški subjekti obdavčeni s stopnjo 29 %.'},
                        {key: 'Delnice so obdavčene s stopnjo 15 % (National Report Greece, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>FRANCIJA</Text>
                <Text>Obdavčitev dobička je odvisna od pravne strukture podjetja. Družbe se lahko obdavčijo z davkom na dohodek (IR) ali davka od dohodka pravnih oseb (CI).
                    {"\n"}Podjetja so predmet:</Text>
                <FlatList
                    data={[
                        {key: 'Obdavčitve dobička,'},
                        {key: 'Ozemeljskega gospodarskega prispevka (CET),'},
                        {key: 'DDV-ja.'},
                        {key: 'Posamezna podjetja (obrtniki, trgovci), svobodni poklici in družba EURL (družba z omejeno odgovornostjo) morajo plačati IR.'},
                        {key: 'Partnerji se obdavčijo osebno, glede dohodnine, samo na plače ali dividende  (National Report France, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
