import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img14.png')
    }
}];

export default class M9S7_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Oblikovanje Twitter poslovnega računa::</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Korak 2:</Text> Kliknite gumb “Edit Profile / Uredi profil” in nato na svoji Twitter strani izpolnite vsa
                    potrebna polja, kot sledi: </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img14.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <FlatList
                    data={[
                        {key: '1. Uporabniško ime: (bolje bo, če bo odražalo  ime vašega podjetja)'},
                        {key: '2. Fotografija profila ( priporočene dimenzije 400x400  točk)'},
                        {key: '3. Opis vašega podjetja/dejavnosti ( Na voljo je 160 znakov za kratko predstavitev)'},
                        {key: '4. Slika glave: lahko uporabite fotografije dogodkov, izdelkov, reklamnih sporočil/ fotografij, ali  jih uporabite za objavo novih prodaj in promocij, ki se trenutno izvajajo v vašem podjetju ( priporočljive dimenzije so 1500x500 pik) .'},
                        {key: '5. Priprto oglašanje: Če vedno dajete najbolj pomembne ali najbolj sveže novice na vrh strani, obiskovalcem olajšate najti novosti, da jih ne potrebujejo iskati znotraj vaše strani.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Prepričajte se, da dokončate sporočila, prav tako poskrbite za uporabo kakovostnih fotografij ali logotipa na vseh vaših socialnih omrežjih....</Text>
            </View>
        )
    }
}
