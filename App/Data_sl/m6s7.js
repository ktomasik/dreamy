import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img20.png')
    }
}];

export default class M6S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Izdelajte elektronski podpis </Text>
                <Text style={{marginTop: 15}}>S premikom transakcijskih sistemov na papirju na spletu so se pojavile nove poslovne priložnosti. Uporaba elektronskega podpisa v poslovnih dokumentih,
                    kot so računi, pogodbe, e-pošte itd. je pomembno za izboljšanje vašega poslovanja. Na mobilnih telefonih obstaja veliko razpoložljivih platform za prenos za elektronski podpis.
                    Eden od njih je DocuSign. DocuSign je brezplačna in priročna platforma za elektronski podpis, ki se uporablja na mobilnih telefonih. Na voljo je za: iPhone, iPad, Android in
                    Windows.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img20.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
