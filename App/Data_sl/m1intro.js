import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1INTRO extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Uvod</Text>
                <Text>Ciljna skupina projekta Dreamy m- Learning so ženske z nižjo stopnjo izobrazbe, ki izdelujejo svoje izdelke doma. Namen je, da bi jim pomagali osvojiti
                    uporabo digitalnih in podjetniških veščin pri prodaji lastnih izdelkov preko spletne trgovine, s pomočjo učinkovite uporabe pametnih telefonov. Razvili bomo
                    mobilno aplikacijo, ki bo preprosto razumljiva in enostavna za uporabo; za operacijska sistema Android in iOS, z vmesnikom na spletu, ki bo povezoval mobilne
                    aplikacije z m- Learning portalom. V skladu s tem namenom, smo pripravili module, s postopnim pristopom za potrebe ciljne skupine. Vse aplikacije v modulih, ki
                    smo jih pripravili, so bile izbrane za brezplačne mobilne aplikacije za pametne telefone.</Text>
                <Text style={{marginTop: 10}}>Skladno s tem, je cilj tega modula pojasniti, kako ženske lahko ustvarijo račun za e-naslov in ga namestijo na svoje pametne telefone (iOS in Android), v
                    prvi fazi. Poleg tega bo ta modul uporabno orodje za osebe, ki imajo enako ozadje in se želijo naučiti ustreznih tem.</Text>
            </View>
        )
    }
}
