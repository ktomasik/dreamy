import React, {Component} from 'react'
import {Text,View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M8S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Viri in literatura</Text>
                <FlatList
                    data={[
                        {key: {val1:'Anonymous 2018a. 27.09.2018.', val2:'https://snapretail.com/snapretail-blog/charge-customers-shipping'}},
                        {key: {val1:'Anonymous 2018b. 27.09.2018.', val2:'https://support.bigcommerce.com/articles/Learning/Shipping-Options'}},
                        {key: {val1:'Anonymous 2018c. 27.09.2018. How to determine density', val2:'https://pnglc.wordpress.com/2011/01/17/how-to-determine-density'}},
                        {key: {val1:'Anonymous 2018d. 27.09.2018. Pallet Density Calculator.', val2:'http://palletrater.com/density-calculator'}},
                        {key: {val1:'Anonymous 2018e. 27.09.2018.', val2:'https://support.easyship.com/hc/en-us/articles/115003423492-Packing-Instruction'}},
                        {key: {val1:'Anonymous 2018f. 27.09.2018.', val2:'https://www.shopify.com/blog/14069585-the-beginners-guide-to-ecommerce-shipping-and-fulfillment'}},
                        {key: {val1:'Anonymous 2018j. 02.10.2018. Shipping and Handling Labels Information.', val2:'https://www.globalspec.com/learnmore/material_handling_packaging_equipment/packaging_labeling/shipping_handling_labels'}},
                        {key: {val1:'European Commission. 2018. 02.10.2018. Customs Declaration.', val2:'https://ec.europa.eu/taxation_customs/business/customs-procedures/general-overview/customs-declaration_en'}},
                        {key: {val1:'Robinson, A. 2016. 25.08.2016. Cargo Insurance: What is it, What are the Types, & Do I Need it for My Shipment?', val2:'https://cerasis.com/2016/08/25/cargo-insurance'}},
                        {key: {val1:'Ufford, L. 2018. 21.03.2018. Shipping Success: Put Your Customer In Control of Delivery Options.', val2:'https://www.shopify.com/retail/shipping-success-put-your-customer-in-control-of-delivery-options'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom:10}}>{item.key.val1} :<Text style={{color: 'blue'}} onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
