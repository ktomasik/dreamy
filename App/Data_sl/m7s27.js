import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Slovenjo:</Text>
                <Text style={styles.dmlH3}>1. korak:</Text>
                <Text>Na zavodu se prijavite v evidenco brezposelnih oseb. Osebnemu svetovalcu že takoj na prvem sestanku povejte, da bi želeli subvencijo. Ta bo pripravil vse potrebne dokumente -
                    zaposlitveni načrt in izjavo o vključitvi v program pomoči pri samozaposlitvi.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
