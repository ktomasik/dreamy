import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S31_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Poljsko:</Text>


                <Text style={styles.dmlH3}>2. korak:</Text>
                <Text>Za vas bo določen datum sestanka s pisarniškim svetovalcem.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_22_3319622.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
