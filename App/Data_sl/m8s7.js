import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Zavarovanje & Sledenje</Text>
                <Text style={{marginTop:10, fontWeight:'bold', color: 'green', textAlign: 'center'}}>Kaj je zavarovanje tovora?</Text>
                <Text>Pravno je, da morajo vsi prevozniki nositi minimalen znesek zavarovanja, Poznan kot odgovornost prevoznika. Vendar pa odgovornost prevoznika zagotavlja omejeno pokritost, in vse od naravnih nesreč do avtomobilskih nesreč ali celo vojno dejanje lahko poškoduje vaš tovor. Zato lahko prevozniki zahtevajo tovorno zavarovanje, da zaščitijo svoje blago pred izgubo, poškodbo ali krajo med prevozom. Na splošno je blago zavarovano med skladiščenjem in med prevozom, dokler ne prispe do kupca (Robinson 2016).
                    {"\n\n"}Obstajajo različne vrste zavarovanj za zavarovanje tovora, nekatere pa se nanašajo na klavzule kot je “vsa tveganja”, “ široka oblika”, “pravna odgovornost” in “pošiljke za kamionski prevoz”. Vendar jih je večina potrebnih za velike kapacitete pošiljk blaga po morju. Tovorno zavarovanje je lahko izvzeto tako pri mednarodnem kot tudi domačem prevozu (Robinson 2016).
                </Text>
            </View>
        )
    }
}
