import React, {Component} from 'react'
import {Text, Image, View, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Zakaj so slike izdelka pomembne?</Text>
                <Text>Doba, v kateri živimo, je takšna, da je vizualnost zelo pomembna. Ko ljudje razmišljajo ali nekaj naredijo, si vedno v mislih izoblikujejo nek izgled, saj si človek vedno rad
                    predstavlja stvari zelo konkretno, pa četudi gre za zelo abstraktne pojme. Zato so vizualne podobe zelo pomembne, tudi pri prodaji ročno izdelanih izdelkov je tako.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 120}}
                    source={require('../Images/m3/img10.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img11.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 130}}
                    source={require('../Images/m3/img12.png')}
                    resizeMode="contain"/>

                <Text style={styles.dmlH2}>Kako prodajati svoje izdelke na Instagramu?</Text>
                <Text>Prodaja vaših izdelkov na vaši spletni strani ni več edini način za povečanje dobička. Z vedno večjo priljubljenostjo socialnega poslovanja v zgodovini nikoli nismo razpolagali s
                    toliko različnimi možnostmi in načinov prodaje in promocija blagovne znamke preko spletnih orodij. Z več kot 700 milijoni uporabnikov je Instagram hitro postal tudi medmrežna
                    trgovina. Približno 80 odstotkov uporabnikov Instagramma sledi poslovanju na Instagramu in 60 odstotkov jih pravi, da jih uporabljajo za odkrivanje novih izdelkov.</Text>
                <Image
                    style={{flex: 1, width: undefined}}
                    source={require('../Images/m3/img13.png')}
                    resizeMode="contain"/>
                <Text style={{marginTop: 10}}>Za prodajo izdelka na instagramu lahko upoštevate nekaj nasvetov:</Text>
                <FlatList
                    data={[
                        {key: 'Ustvarite si poslovni račun.'},
                        {key: 'Objavljajte visokokakovostne fotografije in videoposnetke.'},
                        {key: 'Osredotočite se na pripovedovanje zgodb, da bi spodbudili sodelovanje.'},
                        {key: 'Ustvarite privlačne Instagram oglase.'},
                        {key: 'Zanašajte se na ambasadorje blagovnih znamk in vplivov razširjanja prepoznavnosti.'},
                        {key: 'Klepetajte z odjemalci.'},
                        {key: 'Poiščite nove stranke s primernimi #klučniki.'},
                        {key: 'Pogosto posodabljajte povezavo s profilom.'},
                        {key: 'Ponudite posebne popuste za privržence Instagrama.'}
                        ]}
                    renderItem={({item}) => <Text style={{marginBottom: 5, marginLeft: 12}}>{item.key}</Text>}
                />
                <Image
                    style={{flex: 1, width: 300, height: 480}}
                    source={require('../Images/m3/img14.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 400}}
                    source={require('../Images/m3/img15.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
