import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m5/img1.png')
    }
}];

export default class M5S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>2. Stvari, ki jih morate vedeti pred prejemom prve kreditne kartice (npr. kakšna je izjava o kreditni kartici, kako se obračuna obresti na krediti kartici,
                    kako so določena najmanjša plačila)</Text>
                <Text style={{marginTop: 15}}>Bančni odrezek je periodičen odrezek, v katerem so navedeni vsi nakupi, plačila in druge obremenitve ter krediti, ki so na vašem računu kreditne kartice v
                    ciklu obračunavanja. Izdajatelj kreditne kartice pošlje bančni odrezek enkrat na mesec.
                    {"\n\n"}Kaj vsebuje bančni odrezek?
                    {"\n"}Bančni odrezek vsebuje vse informacije, ki jih morate vedeti o vašem računu. Vključuje:
                </Text>
                <FlatList
                    data={[

                        {key: 'Stanje iz prejšnjega obračunskega obdobja'},
                        {key: 'Minimalno plačilo'},
                        {key: 'Roke plačila'},
                        {key: 'Zamudne obresti, ki so zaračunane ob prepoznem plačilu'},
                        {key: 'Povzetek in podroben seznam plačil, kreditov, nakupov, transferjev, gotovinskih predujmov, pristojbin, obresti in drugih obremenitev računa'},
                        {key: 'Razčlenitev vrst stanj na računu, obrestne mere in obresti'},
                        {key: 'Kreditno omejitev in razpoložljiv kredit'},
                        {key: 'Število dni v obračunskem obdobju'},
                        {key: 'Skupni znesek obresti in pristojbin'},
                        {key: 'Kontaktne informacije za izdajatelja kreditne kartice'},
                        {key: 'Nagrade, pridobljene ali odkupljene'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 10}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m5/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 10}}>Bančni odrezek bo vključeval objavo minimalnega plačila s podrobnostmi o količini časa, ki ga še imate, da poravnate plačilo in višino zneska, ki ga
                    morate poravnati ter skupni znesek, ki ga boste plačali. Vključeval bo tudi mesečno plačilo, ki ga želite opraviti, če želite v roku treh let izplačati svoje stanje. Te informacije
                    so v pomoč pri iskanju najboljšega načina za izplačilo svojega stanja. V bančnem odrezku bo vključeno tudi zapoznelo plačilno opozorilo, ki vam pove, kaj se zgodi, če plačate
                    pozneje-povečanje obresti.
                    {"\n\n"}Najmanjše plačilo s kreditno kartico je najmanjši znesek, ki ga lahko plačate do stanja vase kreditne kartice, ne da bi ga kaznovali z zamudo in morebitnim povečanjem
                    obrestne mere. Če ste vsak mesec pozorni na bančni odrezek, lahko opazite, da se lahko najmanjše plačilo spremeni od enega meseca do drugega.
                    {"\n\n"}Nekateri izdajatelji kreditnih kartic izračunajo najmanjše plačilo kot odstotek stanja, običajno med 2 % in 5 %, na koncu obračunskega obdobja.
                    {"\n\n"}Minimalno plačilo se lahko izračuna tudi tako, da ob koncu obdobja zaračunavanja porabite odstotek stanja in dodate mesečne finančne stroške.
                    {"\n\n"}Iz pogodbe o kreditni kartici lahko izveste, kateri način uporablja izdajatelj kreditne kartice. Poiščite razdelek z naslovom “Kako izračunati najnižje plačilo” ali
                    “Izvedba plačil”.

                </Text>
            </View>
        )
    }
}
