import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Priljubljene spletne strani družbenih medijev</Text>
                <Text>Za leto 2017 so ocenili, da v Ameriki že 81 % ljudi uporablja socialna omrežja in število le-teh še narašča. kar eno petino svojega časa uporabniki namenijo socialnim medijem.
                    Trend uporabe socialnih medijev je v velikem porastu, saj je bilo ocenjeno da je v letu 2005 take vsebine uporabljalo le 5 % odraslih. Ocenjujejo, da je v letu 1027 socialna
                    omrežja uporabljlo 1.96 miljarde ljudi po celem svetu, v letu 2018 pa že kar 2.5 miljarde ljudi (Silver, n.d.). Po raziskavi Pew Research centra (2018), je povprečni uporabnik
                    socialnih omrežij mlajši (približno 90 % uporabnikov, starih med 18 in 29 let, uporablja vsaj eno obliko tega medija), bolje izobraženih in bolj premožnih. Združene države Amerike
                    in Kitajska beležita pogostost uporabe socialnih omrežij (‘Leading Global Social Networks 2018 | Statistic’ 2018): Facebook (2.167 miljard uporabnikov v januarju 2018), YouTube
                    (1.5 miljarde), WhatsApp (1.3 miljarde), Facebook Messenger (1.3 miljarde), WeChat (980 milijonov), QQ (843M), Instagram (800M), Tumblr (794M), QZone (568M), Sina Weibo (376M),
                    Twitter (330M), Baidu Tieba (300M), Skype (300M), LinkedIn (260M), Viber (260M), Snapchat (255M), Reddit (250M), LINE (203M), Pinterest (200M), YY (117M)...
                    {"\n\n"}Različne spletne strani in aplikacije za socialna omrežja so namenjene različnim namenom uporabe komuniciranja in različnim vsebinam. V nadaljevanju lahko najdete kratko
                    predstavitev najbolj popularnih socialnih medijev (Rouse 2016):
                </Text>
                <Text style={styles.dmlH3}>Facebook</Text>
                <Text>Je zelo popularna spletna stran ki nudi uporabnikom izdelavo svojih profilov nalaganje slike in video gradiva pošiljanje sporočil in vzdrževanje razmerij s prijatelji družinskimi
                    člani in kolegi. Po statističnih podatkih Nielsen Group internetni uporabnik znotraj Združenih držav Amerike posveti tej spletni strani več časa kot katerikoli drugi.</Text>
                <Text style={styles.dmlH3}>Twitter</Text>
                <Text>Je brezplačen mini-blogging portal, ki omogoča registriranim uporabnikom objavljanje kratkih obvestil imenovanjih twitt ali “novička”. Člani twitter portala lahko objavljajo
                    “novičko” in sledijo ostalim novičkam z uporabo najrazličnejših platform in mobilnih naprav.</Text>
                <Text style={styles.dmlH3}>Google+</Text>
                <Text>Je Googlov projekt za socialnega omrežja, ki naj bi posnemal bolj osebno interakcijo ljudi iz vsakdanjega življenja in ne sledil običajnim strukturam spletnih medmrežij. Slogan
                    projekta je: “Preusmeritev delitve informacij na splet”.</Text>
                <Text style={styles.dmlH3}>Wikipedia</Text>
                <Text>Je brezplačna enciklopedija s prosto dostopno vsebino ki jo lahko soustvarjajo uporabniki. Vsakdo ki se registrira na tej spletni strani lahko objavila na novo ustvarjeno
                    vsebino, za recenzijo le-teh pa registracija ni potrebna. Wikipedia je bila ustvarjena januarja leta 2001.</Text>
                <Text style={styles.dmlH3}>LinkedIn</Text>
                <Text>Je socialno omrežje ki je namenjeno poslovnim skupnostim. poglavitni cilj tega omrežja je združevati prijavljene s skupnimi poslovnimi nameni.</Text>
                <Text style={styles.dmlH3}>Reddit</Text>
                <Text>Je spletno socialno okolje novice in forumov. na spletnem portalu je združenih na stotine skupin s svojimi temami kot so: tehnologija, politika ali glasba. člani portala lahko
                    objavijo katerokoli vsebino prispevki pa so nato glasovani in predstavljeni po tem kriteriju. </Text>
                <Text style={styles.dmlH3}>Pinterest</Text>
                <Text>Je socialno omrežje preko katerega lahko delimo slike, ki jih lahko kategoriziramo v različne vsebine. Slike so opremljene z zelo malo besedila, ker je glavni fokus portala
                    vizualen. Uporabnik pa si lahko s klikom na sliko ogleda izvorno vsebino, na primer če kliknete na sliko s parom čevljev boste najverjetneje napoteni na spletno trgovino, kjer jih
                    lahko tudi kupite. Klik na sliko z borovnicami pa vas lahko pripelje do kakega recepta s slaščicami.
                </Text>
            </View>
        )
    }
}
