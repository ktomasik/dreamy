import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Francijo:</Text>
                <Text style={styles.dmlH3}>Če niste iskalec zaposlitve - POMOČ DRŽAVE - ACCRE
                </Text>

                <Text style={styles.dmlH3}>1. korak:</Text>
                <Text>Vlogo je treba vložiti pri pristojnem centru za formalnosti podjetij (CFE) ob nastanku ali ponovni vzpostavitvi vašega podjetja ali v 45 dneh po njem. Na vašo zahtevo morate priložiti:
                    {"\n"}obrazec za izjavo družbe na CFE ali njegovo kopijo,
                    {"\n"}posebna oblika obrazca zahtevka za pomoč, ki velja za čast ne-imetja te pomoči za obdobje treh let,
                    {"\n"}dokaz, da pripadate eni od kategorij, ki imajo koristi od ACCRE in
                    {"\n"}CFE vas  lahko zaprosi za druge dokumente, svetujemo vam, da se obrnete na njih.
                </Text>

            </View>
        )
    }
}
