import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uvod</Text>
                <Text>Namen modula je pojasnitev različnih finančnih možnosti in načinov za ženske, ki želijo prodajati svoje ročne izdelke. Modul vsebuje informacije in povezave za ženske do finančne
                    podpore in kreditnih možnosti za pet držav ((Slovenija, Francija, Turčija, Grčija in Poljska) v projektu Dreamy m-learning.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_48_3319639.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
