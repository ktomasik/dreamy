import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Zagon mobilnega in spletnega bančništva</Text>
                <Text style={{marginTop: 15}}>Sledite navodilom za zagon mobilnega in spletnega bančništva.</Text>
                <Text style={{fontWeight: 'bold'}}>Kaj potrebujete?</Text>
                <FlatList
                    data={[
                        {key: 'Veljavno debetno ali kreditno kartico.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Kakšen je postopek?</Text>
                <Text>Na mobilni napravi:
                    {"\n"}Na Android ali Apple napravi prenesite in odprite aplikacijo mobilno bančništvo
                    {"\n\n"}Z računalnika:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Korak 1',
                                val2: 'Obiščite domačo stran banke'
                            }
                        },
                        {
                            key: {
                                val1: 'Korak 2',
                                val2: 'Izberite “Registriraj se”'
                            }
                        },
                        {
                            key: {
                                val1: 'Korak 3',
                                val2: 'Vnesite številko kartice in datum poteka in izberite “Nadaljuj”'
                            }
                        },
                        {
                            key: {
                                val1: 'Korak 4',
                                val2: 'Povejte, kako želite prejemati potrditveno kodo'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />

                <Text style={{marginTop: 15}}>Obstajajo tudi nekatere banke, ki ponujajo naslednje storitve:</Text>
                <FlatList
                    data={[
                        {key: {val1: '1. korak', val2: 'po dokončanju občutljivih transakcij, vas potrditvena koda ščiti z dodatnim slojem varnosti. Preko besedilnega sporočila, elektronske pošte ali glasovnega klica vam pošljejo 6-mestno kodo za preverjanje.'}},
                        {key: {val1: '2. korak', val2: 'Nekateri lahko pošljejo enkratne potrditvene kode osebnim ali brezplačnim e-poštnim storitvam.'}},
                        {key: {val1: '3 .korak', val2: 'V polje vnesite potrditveno kodo in izberite “Nadaljuj”.'}},
                        {key: {val1: '4 .korak', val2: 'Izberite geslo.'}},
                        {key: {val1: '5. korak', val2: 'Preglejte drobni tisk.'}},
                        {key: {val1: '6. korak', val2: 'Skoraj ste končali. Preberite sporazum o elektronskem dostopu in potrdite, da ste ga prebrali.'}},
                        {key: {val1: '7. korak', val2: 'Prijavite se in začnite z bančništvom.'}},
                        {key: {val1: '8. korak', val2: 'Po registraciji, lahko do mobilnega bančništva dostopate s katerega koli računalnika, pametnega telefona ali tabličnega računalnika.'}},
                        {key: {val1: '9. korak', val2: 'Obstaja nekaj bančnih aplikacij, ki jih lahko uporabljate s čitalnikom prstnih odtisov (če vaša naprava to podpira) za preverjanje identitete'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
