import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Grčijo:</Text>

                <Text style={styles.dmlH3}>3. korak:</Text>
                <Text>Po sestanku sledi oblikovanje in dokončanje poslovnega načrta s sodelovanjem pristojnega svetovalca. Ta stopnja lahko zahteva več kot en sestanek, odvisno od stopnje
                    pripravljenosti podjetnika in potreb njegovega poslovanja.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
