import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Slovenjo:</Text>

                <Text style={styles.dmlH3}>4. korak:</Text>
                <Text>Po uspešnem zaključenem usposabljanju, po potrjenem podjetniškem načrtu in po podpisu zaposlitvenega načrta na ZRSZ se morate najkasneje v 30 dneh (po potrjenem podjetniškem
                    načrtu) samozaposliti v podjetju.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
