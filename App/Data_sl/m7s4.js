import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj so poslovni angeli?</Text>
                <Text>Poslovni angeli (znani tudi kot angeli ali angelski vlagatelji) so posamezniki, ki svoje osebno bogastvo uporabljajo za zagotovitev kapitala za zagonska podjetja in podjetja v
                    zgodnji fazi v zameno za delež lastniškega kapitala podjetja. Priliv kapitala lahko pomaga ideji, da se razvije v uspešno podjetje in zagotovi osnovo za začetek izdelave
                    predlaganega izdelka ali storitve. Opredelitev poslovnega angela ostaja nejasna v domeni izrazov "poslovni angel", "neformalni vlagatelj" in "neformalni tvegani kapital".</Text>
                <Text style={styles.dmlH2}>Kaj lahko ponudijo?</Text>
                <Text>Poslovni angeli lahko ponudijo:
                    {"\n"}"- denarno injekcijo za sorazmerno majhne zneske, ki sicer ne bi bili na voljo prek tveganega kapitala,
                    {"\n"}"- pogosto sledijo kasnejšim krogom financiranja istega podjetja,
                    {"\n"}"- so v splošnem zainteresirani za sodelovati v projektu kot usmerjevalci ali mentorji,
                    {"\n"}"- vlagajo svoj čas in zagotavljajo povezave z njihovo večjo mrežo, da bi pomagali podjetniku pri novem poslovnem podvigu.

                </Text>
                <Text style={styles.dmlH2}>Ženski poslovni angelski sklad</Text>
                <Text>Ženski poslovni angelski sklad je običajno del združenja, ki je poklicna neprofitna, nepolitična organizacija, ki podpira udeležbo žensk pri odločanju o inovacijah. Naložbe
                    poslovnega angela ne zajemajo le zagotavljanje finančnih sredstev, temveč tudi izmenjavo znanja, izkušenj in socialnega kapitala. Prepričanje združenja je, da bi udeležba žensk v
                    večjem številu in v novih oblikah prinesla gospodarsko korist za vse.</Text>
            </View>
        )
    }
}
