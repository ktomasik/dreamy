import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M7S29_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Grčijo:</Text>

                <Text style={styles.dmlH3}>4. korak:</Text>
                <Text>Končni poslovni načrt oceni ustrezni odbor. Če je odobren se nadaljuje z zagotavljanjem mikrofinanciranja do 10.000 evrov in svetovalno podporo za začetek izvajanja.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
