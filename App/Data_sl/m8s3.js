import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>1.3 Vrste pošiljk strankam </Text>
                <Text style={{marginTop: 10}}>Prevoz in dostava sta glavni gonilni sili trgovine na drobno glede na pričakovanja strank, da bi dobili predmete hitro in enostavno. Nekatere možnosti pošiljanja so navedene v nadaljevanju (Ufford 2018).  </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Dostava v istem dnevu</Text>
                <Text>Dostava v istem dnevu je storitev, ki lahko preko spleta naročene pošiljke dostavi do stranke v istem dnevu, kar je neke vrste ekspresna storitev (Ufford 2018). </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Prevzem v trgovini</Text>
                <Text>Prevzem v trgovini omogoča strankam nakup izdelkov preko spleta, preverjanje in prevzem blaga v določenem časovnem okviru pa v lokalni trgovini ali na določenem mestu (Ufford 2018). </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Dobava iz trgovine</Text>
                <Text>Dobava iz trgovine je storitev, ki trgovine na drobno spreminja iz off-line lokacij v izpolnitvene centre, ki servisirajo oboje, običajne stranke in stranke preko spleta (Ufford 2018). </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Načrtovana dostava</Text>
                <Text>Načrtovana dostava je storitev, ki jo transportna podjetja ponujajo strankam, kot možnost načrtovanja njihovih dostav z odlogom v predal za nizko najemnino ali brezplačno za njihove stalne stranke (Ufford 2018).</Text>

                <Text style={styles.dmlH3}>1.3.1	Kako zaračunati strankam  </Text>
                <Text>Ena od stresnih točk na začetku prodaje je zaračunati kupcem. Poleg stroškov proizvodnje, ki nastanejo med proizvodnjo, lahko nastanejo nekateri stroški po proizvodnji, ki bi utegnili pritegniti pozornost kupcev. Stroški pošiljanja so ovrednoteni v tej opciji.
                    {"\n\n"}Da bi pritegnili pozornost z nižjo ceno produkta, je smiselno strošek prevoza navesti ločeno. Pri prodaji, je ceno brez transporta smiselno navesti takrat, ko strošek prevoza plača kupec. Na tej točki, je prednost za logistično podjetje saj bi pogoje lahko določal kupec , in glede na potrebo stranke, bi se prevoz opravil. Po drugi strani, pa se ceno produkta in strošek transporta določen pri logistu navede ločeno.
                    {"\n\n"}Obračun transportnih stroškov k ceni produkta lahko zagotavlja konkurenčne cene produktov, ki pritegnejo pozornost kupcev.

                </Text>

                <Text style={styles.dmlH3}>Ponudba brezplačne dostave  </Text>
                <Text>Ponudba brezplačne dostave (običajno le pri domačih naročilih) je preizkušena metoda pridobivanja pozornosti strank in povečane menjave. Vendar lahko znatno zniža dobiček, v odvisnosti od marže (Anonymous 2018b).
                    {"\n\n"}Če se ponudi brezplačno dostavo pri katerikoli količini in vrednosti naročila, je nujno potrebno vedeti, kolikšni so dejanski transportni stroški, kako konkurenca vkalkulira transport in dobiček v marži. To vam bo pomagalo pri pravilni odločitvi, ali ponuditi prevoz brezplačno ali pa ne. Druga možnost je, da določimo minimalno skupno vrednost naročila glede na povprečno vrednost, ali minimalno količino naročenih izdelkov (Anonymous 2018b).
                    {"\n\n"}Nekatere prednosti brezplačnega transporta so ohranjanje konkurenčnosti ali nelojalnost transportnih možnosti pri konkurenci, zagotavljanje dogovarjanja, povečanje povprečne vrednosti naročil in zmanjšanje izpuščenih priložnosti. Po drugi strani, povečane cene, ki pokrivajo strošek prevoza bi morali primerjati s konkurenco na trgu (Anonymous 2018b).

                </Text>
            </View>
        )
    }
}
