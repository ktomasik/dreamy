import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {

        "quiz7": {
            "question1": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Nižje kot so obresti cenejša je izposoja posojila."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) V banki.",
                    "option2": "b) Regionalna gospodarska zbornica in industrija.",
                    "option3": "c) Finančne storitve.",
                    "option4": "d) Podjetniški investicijski center."
                },
                "question": "Kje bi pričakovali najboljšo ponudbo za posojilo za mala podjetja?"
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Donacije so plačljivi skladi, ki jih pogosto zaračunava vladni oddelek"
            },
            "question4": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Državne spodbude za zagon podjetja so le nefinančne"
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Spodbujanje zasebnih naložb.",
                    "option2": "b) Podpora za ustanavljanje in razvoj mikropodjetij.",
                    "option3": "c) Zagotavljanje ugodnih finančnih sredstev podjetjem.",
                    "option4": "d) Finančne spodbude v regionalnih jamstvenih shemah."
                },
                "question": "Kakšna je najbolj tipična vrsta bančne podpore za ženske za donacije za mala podjetja?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Davčna spodbuda je vladni ukrep, ki spodbuja posameznike, da prihranijo denar z zmanjšanjem zneska davka, ki ga morajo plačati"
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Mikro krediti.",
                    "option2": "b) Finančnim dežnikom.",
                    "option3": "c) Mikro posojili.",
                    "option4": "d) Nič od zgoraj navedenega."
                },
                "question": "Mikro financiranje je bilo sprva tesno povezano z:"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Mikro financiranje in socialni kapital imata značilno povezavo s krepitvijo podjetništva"
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Poslovni angeli se na splošno zanimajo za vključevanjem v projekt kot usmerjevalci ali mentorji."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Kredit",
                    "option2": "b) Ravnovesje",
                    "option3": "c) Posojilo",
                    "option4": "d) Mikro kredit"
                },
                "question": "____________ je, ko si izposodite denar pri banki ali drugem posojilodajalcu."
            }
        }
    }
};

export default class M7Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz7;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
