import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je Internet?</Text>
                <Text>Internet je mreža globalnih izmenjav, vključno z zasebnim, javnim, poslovnim, akademskim in vladnim omrežjem – povezanim preko vodene, brezžične in optične tehnologije.</Text>
                <Text style={styles.dmlH2}>Kratka zgodovina interneta</Text>
                <Text>Začetki Interneta segajo v 60. leta kot vladni projekt Združenih držav Amerike in njihovega oddelka za obrambo, da bi oblikovali necentralizirano omrežje. Ta projekt se je
                    imenoval ARPANET (Advanced Research Projects Agency Network) . Da bi naredili omrežje bolj globalno, so razvili nov in standardiziran protokol IP (Inetrnet Protokol) tehnologije,
                    ki definira, kako naj bodo elektronska sporočila zapakirana, naslovljena in poslana preko omrežja. </Text>
                <Text style={styles.dmlH2}> Kaj je Web - (World Wide Web)?</Text>
                <Text>Web ali World Wide Web (W3) je v osnovi sistem spletnih strežnikov, ki podpirajo posebno formatirane dokumente.</Text>
                <Text style={styles.dmlH2}>Razlike med Internetom in Webom </Text>
                <Text>Internet, ki povezuje računalnik z ostalimi računalniki po svetu, je način prenosa vsebine. Imeti morate dostop do Interneta, da si ogledate svetovni splet in katerokoli spletno
                    stran ali drugo vsebino, ki jo vsebuje . Svetovni splet je del spleta za izmenjavo informacij. Svetovni splet uporablja tudi brskalnike, kot so Google ali Internet, za dostop do
                    spletnih dokumentov, imenovanih spletnih strani, ki so med seboj povezane preko hiperpovezav.</Text>
            </View>
        )
    }
}

