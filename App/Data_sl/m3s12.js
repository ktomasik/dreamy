import React, {Component} from 'react'
import {Text, Image, View, Linking, TouchableHighlight, Modal} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen9 = [{
    url: '',
    props: {
        source: require('../Images/m3/img49.png')
    }
}];

export default class M3S12 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sledite spodnjim korakom, da dobite ime domene</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>1. Korak: podjute na: <Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://godaddy.com/domains/domain-name-search')}>godaddy.com/domains/domain-name-search</Text></Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen9}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img49.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
