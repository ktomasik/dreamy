import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je registracija blagovne znamke? Ali moram registrirati svojo blagovno znamko (ime podjetja)?</Text>
                <Text>Razvoj in uvedba nove blagovne znamke zahteva naložbo velikega finančnega, duševnega in čustvenega kapitala in zato mora biti registracija blagovne znamke glavna prednostna
                    naloga za katero koli podjetje. To velja v primerih, da je nova blagovna znamka novo podjetje, nv izdelek ali storitev ali nova spletna dejavnost.
                    {"\n\n"}Registracija blagovne znamke je drugo ime za registracijo trgovskega imena. To je edini način, da lahko lastnik blagovne znamke pridobi izključne pravice za uporabo nove
                    blagovne znamke na določenem nacionalnem ozemlju. Niti družba z omejeno odgovornostjo niti domensko ime ne zagotavljata pravne zaščite za novo blagovno znamko (Forbes Agency
                    Council 2017).
                    {"\n\n"}Konkurentom ne moremo na noben način preprečiti uporabe iste blagovne znamke, če trgovskega imena ne registriramo.
                </Text>
                <Text style={styles.dmlH2}>Kako registrirati blagovno znamko?</Text>
                <Text>Vsaka država ima svoj urad za registracijo trgovskega imena ali logotipa. Vsaka država ima tudi različne postopke v skladu z zakoni in predpisi države. Za registracijo trgovskega
                    imena je potrebno plačati pristojbino za registracijo.
                    {"\n\n"}Izjava o registraciji trgovske znamke se lahko razlikuje, vendar običajno traja deset let. V primeru plačila dodatnih pristojbin, se lahko podaljša za nedoločen čas.
                    Pravice do znamke so zasebne pravice, zaščita pa s izvršuje s sodnimi nalogi.
                </Text>
            </View>
        )
    }
}
