import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kje lahko najdete posojila za majhna podjetja za ženske?</Text>
                <Text style={styles.dmlH2}>Za Francijo:</Text>
                <Text style={styles.dmlH3}>Žensko ustanovljeni jamstveni sklad(FGIF)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Predmet: Omogočiti pridobitev bančnih posojil za kritje zahtev po obratnem kapitalu in / ali naložb v fazi ustanovitve, predelave ali razvoja podjetja.',
                                val2: 'https://www.afecreation.fr/pid14855/appuis-pour-les-femmes.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Omrežja</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Našteta nacionalna omrežja so na voljo, da vas spodbujamo, obveščamo in spremljamo pri urejanju vašega projekta. Nekatera so namenjena ženskim ustvarjalkam, druga so namenjena vsem, vendar imajo posebne ukrepe za ženske, drugi pa še vedno sledijo podjetjem, ki so jih financirali.',
                                val2: 'http://www.ellesentreprennent.fr/pid14416/les-reseaux-au-service-des-creatrices.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>The BPI France (Public Investment Bank)</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'BPI je organizacija pod nadzorom države. Pripada vam pri financiranju in razvojnih pripomočkih. Ponuja povezovanje in jamčenje rešitev, ki prepričajo vašo banko, da vas spremljajo v svojih projektih.',
                                val2: 'http://www.bpifrance.com/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
