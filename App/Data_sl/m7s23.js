import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S23 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Mikrofinančni in drugi zagonski skladi za ženske</Text>
                <Text style={styles.dmlH2}>Za Turčijo:</Text>
                <Text style={styles.dmlH3}>Kredi Garanti Fonu (The Credit Guarantee Fund- KGF)</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'KGF je neprofitno združeno podjetje in deluje kot porok za MSP in podjetja, ki niso MSP, ki zaradi nezadostnega zavarovanja ne morejo dobiti posojila. KGF podpira MSP in podjetja, ki niso MSP pri dostopu do financiranja. Posojilo podpira ženske podjetnice, da razvijejo svoje poslovanje ali odstranijo svoja podjetja iz težkega položaja.',
                                val2: 'http://www.kgf.com.tr/index.php/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Turški Grameen mikrofinančni program </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Turški Grameen mikrofinančni program (TGMP) je neprofitna gospodarska fundacija. Namesto tradicionalnih donacij in "dobrodelnosti" TGMP ponuja "mikrokreditne" storitve, ki pomagajo zmanjšati revščino v Turčiji. Cilj mikro kreditnega sistema je pomagati ženskam z nizkimi dohodki, ki se ukvarjajo s trajnostnimi dejavnostmi, ki ustvarjajo dohodek in prispevajo k njihovim družinskim proračunom. Za razliko od formalnega (komercialnega) bančnega sektorja se ponujajo posojila za mikro posojila ne da bi zahtevali zavarovanje ali drugo dokumentacijo, ki ni turška nacionalna identifikacijska izkaznica. Nekateri mikrokreditni produkti so navedeni v nadaljevanju.',
                                val2: 'http://www.tgmp.net/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Osnovno posojilo</Text>
                <Text>Osnovno posojilo je prva vrsta posojil za stare in nove člane. Novim članom se lahko odobri posojilo od 100 TL do 1.000 TL, odplačilo posojil pa 46 tednov.</Text>

                <Text style={styles.dmlH3}>Podjetniško posojilo</Text>
                <Text>Je vrsta posojila za podjetnike in uspešne člane katerim se lahko odobri posojilo od 1.000 TL do 5.000 TL in odplačilo posojila v 46 tednih.</Text>

                <Text style={styles.dmlH3}>Posojilo digitalnega razkoraka</Text>
                <Text>Poleg posojil, ki so jih prejeli naši člani, je ta vrsta posojila namenjena zagotavljanju tehnološkega razvoja članov. Kredit omogoča članom, da imajo lahko pametne telefone, ki
                    se uporabljajo z današnjo tehnologijo. Odplačila posojil potekajo 46 tednov.</Text>
            </View>
        )
    }
}
