import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Izdaja naročila za e-trgovino</Text>
                <Text style={{marginTop: 10}}>Glavni koraki, ki veljajo za vse države so opisani v nadaljevanju:</Text>
                <FlatList
                    data={[
                        {key: '1. korak:  vzpostavitev ponudbe in postopek sprejemanja'},
                        {key: '2. korak:  izpolnjevanje naročilnice'},
                        {key: '3. korak: vključitev določil in pogojev'},
                        {key: '4. korak:  sprejemanje podatkov o bančni kartici potrošnika'},
                        {key: '5. korak: potrditev prejema naročila.'},
                        {key: '6. korak: zagotavljanje potrditve predloženih informacij in pravice do preklica.'},
                        {key: '7. korak: dostava.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>TURČIJA</Text>
                <Text>Predpisi o elektronskem poslovanju zahtevajo, da vse komercialne spletne strani neposredno in trajno potrošnikom priskrbijo naslednje informacije na spletni strani:</Text>
                <FlatList
                    data={[
                        {key: 'Ime podjetja, poštni naslov (in naslov sedeža podjetja, če je to drugačno) in elektronski naslov;'},
                        {key: 'Registracijsko številko podjetja;'},
                        {key: 'Članstvo v trgovinskem ali poklicnem združenju;'},
                        {key: 'Davčna številka podjetja.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Vsi ti podatki morajo biti vključeni, ne glede na to, ali je prodaja izdelkov spletna. Poleg tega morajo te informacije prikazovati vsa komercialna
                    sporočila, poslana preko e-pošte ali SMS storitev.
                    {"\n"}Pravilnik o elektronskem poslovanju zahteva tudi, da morajo biti vse cene jasne, spletne strani pa morajo navajati, ali cene vključujejo davke in stroške dostave (National
                    Report Turkey, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>SLOVENIJA</Text>
                <Text>Registracija podjetja v Upravi Republike Slovenije za javna plačila.
                    {"\n\n"}Pridobitev dostopa do uporabe portal za izdajanje računov (pred tem je potrebno pridobiti digitalno potrdilo).
                    {"\n\n"}Dostopate lahko do spletne strani Uprave Republike Slovenije za javna plačila (National Report Slovenia, 2018).

                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>POLJSKA</Text>
                <Text>Institution-called PLATNIK) (National Report Poland, 2018).
                    {"\n\n"}Od leta 2018 mora vsak podjetnik pripraviti davek.
                    {"\n\n"}Podjetnik pripravi račun (priprave e-računa ne podajajo samo računalniški programi ampak tudi nekatere banke preko bančnih računov).
                    {"\n\n"}Vse potrebne dokumente v zvezi s socialno varnostjo podjetnika je treba videti v elektronski obliki prek preko posebnega programa, ki ga pripravlja SII (Social Insurance Institution-called PLATNIK) (National Report Poland, 2018).

                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>GRČIJA</Text>
                <Text>Izdajanje e-računov je bilo delno uvedeno v Grčiji, leta 2006.
                    {"\n\n"}Elektronski sistem pa še vedno ne deluje v celoti, končan naj bi bil do konca leta 2019.
                    {"\n\n"}Takoj, ko je račun izdan, je v realnem času obveščen računovodski sitem stranke, da sprejme plačilo, hkrati pa davčni urad zbira davke (National Report Greece, 2018).

                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>FRANCIJA</Text>
                <Text>Komercialne strani, ki zbirajo osebne podatke (ime, elektronska pošta…) in predstavljajo datoteke strank in prospektov, morajo poenostaviti prijavo pri Nacionalni komisiji za informatiko in svoboščine.
                    {"\n\n"}Spletna trgovina na spletu običajno spada pod poenostavljene standarde 48 (National Report France, 2018).

                </Text>
            </View>
        )
    }
}
