import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Iskanje v brskalniku Google in oblikovanje računa za e-naslov na pametnemu telefonu</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 4: Potem prosim napišite “želim odpreti e-mail račun” v vašem domačem jeziku</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img4.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
