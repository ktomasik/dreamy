import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Naredi Facebook račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 4: Kliknite NAPREJ (NEXT) in DOVOLI (ALLOW) Facebook programu dostop do vaših osebnih podatkov.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex:1, width: 320, height: 400}}
                        source={require('../Images/m2/img8.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex:1, width: 320, height: 400, marginTop:10}}
                        source={require('../Images/m2/img9.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex:1, width: 320, height: 400, marginTop:10}}
                        source={require('../Images/m2/img10.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
