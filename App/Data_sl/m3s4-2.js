import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S4_2 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kako zajeti fotografije izdelka in jih naložiti v spletu</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>2. Korak: slikajte fotografijo svojega izdelka</Text>
                <View style={{alignItems: 'center'}}>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img18.png')}
                    resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
