import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Instagram poslovni račun</Text>
                <Text styl={{marginTop: 10}}>Glede na zgoraj navedeno, če že imate odprt račun za podjetje na Facebook-u, je z vidika podjetja, Instagram  še preprostejši in manj zamuden, kot druge spletne strani.
                    {"\n\n"}Če želite, lahko razširite svojo mrežo uporabnikov preko Facebook-a in Instagrama.

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
