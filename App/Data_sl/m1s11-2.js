import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S11_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računa za e-naslov na mobilnih napravah sistema iOS (iPhone, iPad, ali iPod touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 5: Kliknite Next</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex:1, width: 320, height: 300}}
                        source={require('../Images/images/315654-P9L4NL-114.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
