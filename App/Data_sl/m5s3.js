import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kako odpreti poslovni bančni račun ob začetku zagona podjetja?</Text>
                <Text style={{marginTop: 15}}>Poslovni bančni račun omogoča enostavno spremljanje stroškov, upravljanje plač zaposlenih, posredovanje finančnih sredstev vlagateljem, prejemanje in
                    vplačilo plačil ter natančnejši načrt vašega proračuna. Ustvarjanje poslovnega bančnega računa poteka preko preprostih korakov:</Text>
                <FlatList
                    data={[

                        {key: 'Določite, kakšen račun potrebujete'},
                        {key: 'Izberite banko'},
                        {key: 'Pridobite poslovno ime'},
                        {key: 'Pridobite dokumente'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>Razlika med debetno in kreditno kartico</Text>
                <Text style={{marginTop: 15}}>Kreditna kartica je kartica, ki omogoča izposojanje denarja, glede na kreditni limit kartice. Kartico uporabite za izvedbo osnovnih transakcij, ki se nato
                    odražajo na vašem računu. Debetne kartice pri nakupu porabijo denar neposredno iz tekočega računa. Da se to zgodi, lahko traja nekaj dni.</Text>
            </View>
        )
    }
}
