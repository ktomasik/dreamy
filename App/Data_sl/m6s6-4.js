import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Informacije o pogodbi</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>3. korak:</Text> Zadnji korak je izvedba nakupa izbranega izdelka. Če kupujete na spletu izvedite še
                    postopek preiskave zavarovanja.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_7_3319599.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
