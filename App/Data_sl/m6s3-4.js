import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S3_4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Izdajanje računov na mobilnih napravah</Text>
                <Text style={styles.dmlH3}>Za Android</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight:'bold'}}>2. korak:</Text>Kliknite na gumb "Install" za prenos za svojo izbrano predlogo in nato vnesite geslo za Google.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_20_3319624.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
