import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Slovenjo:</Text>
                <Text style={styles.dmlH3}>2. korak:</Text>
                <Text>Nujna je udeležba na predstavitvenem dnevu na zavodu, kamor vas bo napotil svetovalec. Tam boste izpolnili prijavo, na podlagi katere bo komisija odločala o tem, ali boste
                    sprejeti v program ali ne.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
