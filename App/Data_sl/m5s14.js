import React, {Component} from 'react'
import {Text, View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M5S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Viri in literatura</Text>
                <FlatList
                    data={[
                        {key: 'http://www.nerdwallet.com/blog/11-credit-card/'},
                        {key: 'https://www.wikihow.com/Open-a-Bank-Account'},
                        {key: 'https://en.wikipedia.org/wiki/Mobile_banking'},
                        {key: 'https://www.cibc.com/en/personal-banking/ways-to-bank/how-to/register-for-mobile-and-online-banking.html'},
                        {key: 'https://due.com/blog/set-up-your-business-to-receive-payments/'},
                        {key: 'https://wpforms.com/how-to-create-a-simple-order-form-in-wordpress/'},
                        {key: 'http://www.banking.org.za/consumer-information/conventional-banking/what-is-a-bank-account'},
                        {key: 'https://www.thebalance.com/types-of-bank-accounts-315458'},
                        {key: 'https://www.wellsfargo.com/financial-education/basic-finances/manage-money/options/bank-account-types/'},
                        {key: 'https://www.discover.com/online-banking/banking-topics/whats-the-difference-between-online-and-mobile-banking/'},
                        {key: 'https://www.inc.com/aj-agrawal/how-to-open-a-business-bank-account-for-your-startup.html'},
                        {key: 'https://www.thebalance.com/difference-between-a-credit-card-and-a-debit-card-2385972'},
                        {key: 'https://www.thebalance.com/credit-card-billing-statement-959999'},
                        {key: 'https://www.discover.com/credit-cards/resources/how-does-my-credit-card-interest-work'},
                        {key: 'https://www.thebalance.com/credit-card-minimum-payment-calculation-960238'},
                        {key: 'https://securionpay.com/blog/e-payment-system/'},
                        {key: 'https://www.investopedia.com/terms/a/automatic-bill-payment.asp'},
                        {key: 'https://blog.mypos.eu/virtual-pos-terminal/'},
                        {key: 'https://paysimple.com/blog/all-the-ways-you-can-accept-online-payments-in-2018/'},
                        {key: 'https://fitsmallbusiness.com/accept-credit-cards-online/'},
                        {key: 'http://www.transact.money/2007/10/definition-of-online-payment-systems.html'},
                        {key: 'https://www.paypal.com/c2/webapps/mpp/paypal-get-started?locale.x=en_C2'},
                        {key: 'http://www.billbaren.com/creditcardguide/'},
                        {key: 'https://www.vantiv.com/vantage-point/smarter-payments/processing-through-point-of-sale'}
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
