import React, {Component} from 'react'
import {Text, View, SectionList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Trženje z uporabo socialnega med mreža</Text>
                <Text style={styles.dmlH3}>Cilji</Text>
                <Text>Zelo dober članek na temo o trženju z uporabo socialnih omrežij je napisal Alex York (2018). V članku izpostavi, da je konstantno sledenje določenemu cilju najbolj pomembna
                    stvar, ki vodi podjetje po uspešni poti. Še bolj dobrodošlo je, če lahko podjetnik te cilje zapiše in jih tako bolj natančno definira.</Text>
                <Text>Izpostavitev ciljev je zelo pomemben korak v vsakršnem trženju ali podjetništvu. Trženje s podporo socialnega medmrežja ni nobena izjema. Glede na široko paleto možnosti socialna
                    medmrežja je še toliko težje definirati naše cilje. Za orientacijo bomo našteli nekaj pogostih ciljev, ki jih lahko vzamete v razmislek, če se lotevate trženja z uporabo socialnega
                    medmrežja:
                </Text>

                <SectionList
                    sections={[
                        {
                            title: 'Povečanje prepoznavnosti blagovne znamke:',
                            data: ['Če želite ustvariti verodostojno in trajno prepoznavnost blagovne znamke,  se izogibajte številnim promocijskim sporočilom. Namesto tega na pomembne vsebine in močna osebna prepričanja podjetja preko vaših  socialnih omrežij.']
                        },
                        {
                            title: 'Večja kakovost prodaje:',
                            data: ['Iskanje vsebin na portalih socialnega medmrežja  je skoraj nemogoče brez spremljanja določenih ključnih besed,  besednih zvez ali označb. Prav te učinkovite označbe pripomorejo da vaše vsebine bistveno hitreje dosežejo željeno ciljno skupino.']
                        },
                        {
                            title: 'Privabljanje kupcev v trgovine:',
                            data: ['Mnogi trgovci se zanašajo na socialna omrežja kot orodje za privabljanje kupcev v njihove trgovine. Poskrbeti morate da nagradite tiste kupce ki vas spremljajo' +
                            ' na socialnem omrežju. Tako bodo vaše stranke primerno obveščene o vseh dogodkih ki se dogajajo v vaši trgovini.']

                        },
                        {
                            title: 'Izboljšajte donosnost naložbe:',
                            data: ['Najverjetneje ni blagovne znamke, ki ne bi želela povečati donosnosti naložbe. Na področju socialnega mreženja naložbe predstavljajo predvsem stroški dela oglaševanja, izdelave spletnih vsebin,  oblikovanje in tudi analiza kanalov socialnega medmrežja vašega podjetja.']

                        },
                        {
                            title: 'Ustvarjanje zveste publike:',
                            data: ['Ali vaša blagovna znamka spodbuja objavljanje vsebin vaših uporabnikov? Ali se vaši privrženci lahko pozitivno odzivajo na vaše vsebine brez kakršnihkoli' +
                            ' predhodnih pogojev? Da dosežete ta dva cilja je potrebno nekaj časa in truda z ustvarjanjem prepoznavnosti imena  blagovne znamke na portalih socialnega medmrežja.']
                        },
                        {
                            title: 'Spremljanje konkurence:',
                            data: ['Katere učinkovite metode trženja uporabljajo vaši konkurenti? Katere strategije uporabljajo pri spodbujanju posla ali prodaje? S spremljanjem vaših konkurentov se lahko mnogo naučite in lahko s tem pridobite tudi nekaj nasvetov, kako postopati v trženju podjetja z uporabo socialnih medmrežij.']
                        }

                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Ciljna publika</Text>
                <Text>Imamo veliko priložnosti in možnosti za objavo naših izdelkov. Za izbiro enega ali več izmed njih se moramo vprašati: "Kaj je naša ciljna skupina?". Ko imamo odgovor, lahko
                    sledimo statističnim demografskim podatkom ljudi, ki uporabljajo različne socialne medije: </Text>
                <SectionList
                    sections={[
                        {
                            title: 'Facebook:',
                            data: ['ženski spol (89%)', '18-29 let (88%)', 'meščani in ljudje iz podeželja 81%)', 'letni zaslužek imajo manjši od $30.000 (84%)', 'z višješolsko izobrazbo ' +
                            ' (82%)']
                        },
                        {
                            title: 'Instagram:',
                            data: ['ženski spol (38%)', '18-29 let (59%)', 'ljudje iz podeželja (39%)', 'letni zaslužek imajo manjši od $30.000 (38%)', 'z višješolsko izobrazbo (37%)']
                        },
                        {
                            title: 'Twitter:',
                            data: ['ženski spol (25%)', '18-29 let (36%)', 'ljudje iz podeželja (26%)', 'letni zaslužek imajo manjši $50,000-$74,999 (28%)', 'z višješolsko ali fakultetno izobrazbo (29%)']
                        }
                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{'\u2022'}{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Podobni izdelki</Text>
                <Text>Predno začnete predstavljati vaše izdelke si je pametno pogledati konkurente s podobnimi izdelki. To analizo je smiselno narediti še preden boste začeli z ustvarjanjem vaše
                    podobe podjetja, saj tako lahko bolj kritično lahko presojate različne načine podajanja vsebin. Najenostavnejši način pregleda vaših konkurentov je enostavno iskanje preko
                    Googlovega iskalnika tako, da vpišete ključne besede in podjetniške izraze, ter analizirate rezultate iskanja.</Text>
                <Text style={styles.dmlH3}>Vsebina</Text>
                <Text>Za začetnike predlagamo da se vsebina neposredno nanaša na identiteto blagovne znamke. Kar pomeni, v začetku ne nagovarjate demografskih skupin ljudi ki niso neposredno povezani
                    z vašimi izdelki. Vsebina naj bo kratka in jedrnata, spletna stran podjetja pa naj ne bo prenapolnjena z oglasi. Spletni kupci si bodo raje ogledali video vsebine kot le slike ali
                    samostojno besedilo. Če je le mogoče si prej pripravite predloge objav ki vam bodo v pomoč celostni podobi, s katero ne boste zmedli vaših bralcev.</Text>
                <Text style={styles.dmlH3}>Ne prezirajte</Text>
                <Text>Socialna omrežja so v svojem jedru namenjena pogovoru, objavljanju vsebin in diskusiji. Zato vaša blagovna znamka na te bistvene elemente socialnih omrežij ne sme pozabiti in
                    mora izkoristiti vsakršno priložnost za nadaljevanje razgovora.
                    Imejte v mislih da na socialnih omrežjih pridobivate spoštovanje vaših uporabnikov z navzočo prisotnostjo in primernimi pogovori s svojim občinstvom. Zato je skrb za aktivnost na
                    socialnih omrežjih izrednega pomena in le tako si lahko povečate prepoznavnost blagovne znamke.
                </Text>
            </View>
        )
    }
}
