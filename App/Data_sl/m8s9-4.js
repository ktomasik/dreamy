import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img15.png')
    }
}];

export default class M8S9_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Za pošiljanje tovora upoštevajte spodaj navedene korake: </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>KORAK 8: SLEDENJE</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img15.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Nasveti za delo z logističnimi podjetji:</Text>
                <FlatList
                    data={[
                        {key: 'Vprašati stranko po željenem (podjetju, branži idr.) pred odpremo bi bila dobra ideja. On/ona ima lahko posebne zahteve po tem.'},
                        {key: 'Izdelek mora biti ustrezno zapakiran. Primerna embalaža lahko zniža stroške.'},
                        {key: 'Pogodbeno sodelovanje s prevoznikom lahko zniža stroške.'},
                        {key: 'Pogodba lahko vsebuje nekatere prednosti, kot je sprejemanje pošiljk na vašem naslovu brez kakršnih koli stroškov.'},
                        {key: 'Pred prevozom, se mora pri prevozniku preveriti tip prevoza in čas dostave.'},
                        {key: 'Pridobiti številko pošiljke in obvestiti stranko o njej, nam lahko pomaga pri pravočasni dostavi blaga.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

            </View>
        )
    }
}
