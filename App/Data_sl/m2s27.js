import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ustvari Instagram računu</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 14: kliknite DODAJ FOTOGRAFIJO (add a photo) in izberite vir. Lahko jo vstavite if Facebook portala, slikate ali izberete iz vaše
                    galerije posnetkov.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img50.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img51.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
