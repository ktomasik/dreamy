import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S17 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Davčne spodbude za zagonska podjetja žensk</Text>
                <Text style={styles.dmlH2}>Za Slovenijo:</Text>
                <Text>Delna oprostitev plačila prispevkov do dveh let po prvem vstopu. Za 2 leti za prvo odprtje s.p., ki temelji na samozaposlitvi in vključuje socialno zavarovanje (samo tiste osebe,
                    ki so vključene v pokojninsko in invalidsko zavarovanje na podlagi samozaposlenih).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_41_3319603.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
