import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S52 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uporaba socialnih omrežij</Text>
                <Text style={styles.dmlH2}>Ustvarite FaceBook stran</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 8: Dodajte fotografijo profila iz vaše galerije fotografij tako, da kliknete DODAJ SLIKO PROFILA, izberete fotografijo in kliknete
                    ZAKLJUČI (DONE) in SHRANI (SAVE).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img105.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img106.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img107.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
