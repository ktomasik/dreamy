import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S11_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računa za e-naslov na mobilnih napravah sistema iOS (iPhone, iPad, ali iPod touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 7: Ko prejmete kodo za preverjanje, jo vnesite v okno</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex:1, width: 320, height: 350}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
