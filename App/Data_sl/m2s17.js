import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S17 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ustvari Instagram računu</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 2: Klikni v iskalno polje in vpišite Instagram.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img27.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:10}}
                        source={require('../Images/m2/img28.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
