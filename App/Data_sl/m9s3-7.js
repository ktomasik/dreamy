import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img4.png')
    }
}];

export default class M9S3_7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Namestitev Facebook strani za podjetje</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Korak 6:</Text> Dodajte slike: Naložite profil in namestite krovno fotografijo na vašo Facebook stran. V
                    nadaljevanju lahko namestite tudi logotip vašega podjetja. Če nimate svojega logotipa, dajte fotografijo enega od vaših zadnjih izdelkov in planirajte, da jo boste posodabljali.
                    Pomembno je, da boste ustvarili vizualno dober prvi vtis. (Sledite temi v Modulu 3).
                    {"\n\n"}Po drugi strani pa je pomembna tudi velikost slike.  Sredinska krovna fotografija je 563 x 315 slikovnih pik na zaslonu mobilnih naprav .
                </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img4.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
