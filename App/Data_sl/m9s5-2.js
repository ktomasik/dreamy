import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img8.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m9/img9.png')
    }
}];

export default class M9S5_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Instagram poslovni račun</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Korak 1:</Text> Prijavite se z e-mail naslovom ali telefonsko številko, in vnesite uporabniško
                    ime.</Text>
                <Text style={{textAlign: 'center', color: 'red'}}>ALI</Text>
                <Text>Če že imate račun za Facebook, se lahko prijavite z istimi podatki in povezanimi računi. (Kako odpreti Facebook račun, je bilo opisano v Modulu 1).</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Dotaknite se “Sign up / Prijava”, potem vnesite vaš e-mail naslov in kliknite “Next / Naslednji”, ali kliknite “ Sign in with Facebook / Prijavljeno v
                    Facebook-u”, da se prijavite preko Facebook računa. </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
