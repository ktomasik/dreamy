import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj pomeni spletno nakupovanje? </Text>
                <Text>Spletno nakupovanje je oblika elektronskega poslovanja, ki potrošnikom omogoča, neposreden nakup blaga ali storitev od prodajalca preko internet, s pomočjo spletnega
                    brskalnika.</Text>

                <Text style={styles.dmlH2}>Kaj pomeni spletno trženje?</Text>
                <Text>Spletno trženje je nabor orodij in metodologij, za promocijo izdelkov in storitev prek spleta . Spletno trženje vsebuje širši nabor tržnih elementov kot tradicionalno poslovno
                    trženje, zaradi dodatnih kanalov in tržnih mehanizmov, ki so na spletu na voljo.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmark2.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}

