import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S48 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uporaba socialnih omrežij</Text>
                <Text style={{marginTop: 10}}>Pomembno je, da ljudi obveščate o vašem podjetju, kako napreduje in o izdelkih, ki jih ponujate. Veliko ljudi se odloči za tako početje in vsebine
                    objavljajo na več portalih. Najbolj popularen je FaceBook, ki ga verjetno ni potrebno posebej predstavljati…</Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 300, height: 300}}
                        source={require('../Images/images/1016.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}

