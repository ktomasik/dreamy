import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";

// Styles

export default class M4S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Licence in dovoljenja za novo podjetje</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>TURČIJA</Text>
                <Text>Če želijo biti ženske oproščene plačilu davka, izpolnijo Obrtno potrdilo o oprostitvi (ang. Craft Certificate of Exemption) (Esnaf Vergi Muafiyet Belgesi).
                    {"\n\n"}
                    Če ženske želijo biti davčne zavezanke, izpolnijo izjavo v e-Declaration System. Ta sistem uporabljajo v elektronski platformi na spletu. Evidenca dokumentov, ki so jih prejeli in
                    poslali davčni zavezanci se vodi v obrtnih zbornicah, katerim davčni zavezanci pripadajo (National Report Turkey, 2018). {"\n\n"}‘Craft Certificate of Exemption’ (Esnaf Vergi
                    Muafiyet Belgesi) (National Report Turkey, 2018);
                </Text>
                <FlatList
                    data={[
                        {key: 'Po pridobitvi tega potrdila, je treba registracijo opraviti pri trgovskem združenju, ki je povezano s Konfederacijo turških obrtnikov.'},
                        {key: 'Za delovno dovoljenje je treba vložiti prošnjo na občino.'},
                        {key: 'Za poslovanje na domu morajo vsi lastniki stanovanj imeti sklenjeno pogodbo z notarjem.'},
                        {key: 'Račun in izjava sta obvezna.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15, fontWeight: 'bold'}}>Davčni zavezanci (National Report Turkey, 2018):</Text>
                <FlatList
                    data={[

                        {key: 'Uporabijo e-Declaration System.'},
                        {key: 'Po odobritvi se je treba registrirati pri združenju povezanem s Konfederacijo turških obrtnikov.'},
                        {key: 'Na občino je treba vložiti prošnjo za dovoljenje za delo.'},
                        {key: 'Davčni zavezanci bodo pridobili dokumente iz zbornic ali združenj.'},
                        {key: 'Račun in izjava sta obvezna.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>SLOVENIJA</Text>
                <Text>Za ustanovitev podjetja je obvezno izpolniti prijavo CEIDG-1 z naslednjimi podatki:</Text>
                <FlatList
                    data={[
                        {key: 'potrebno je pridobiti davčno številko,'},
                        {key: 'izjavo o izbiri oblike obdavčitve ,'},
                        {key: 'izjavo o prispevku k socialni varnosti (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>Obstajajo tri področja, ki zahtevajo licence/dovoljenja:</Text>
                <FlatList
                    data={[
                        {key: 'nepremičninsko in borzno posredništvo,'},
                        {key: 'opravljanje storitev transporta,'},
                        {key: 'vodenje delovne agencije, agencije za začasno delo, usposabljanja za javne sklade za brezposelnost (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>POLJSKA</Text>
                <Text>Za ustanovitev podjetja je obvezno izpolniti prijavo CEIDG-1 z naslednjimi podatki</Text>
                <FlatList
                    data={[
                        {key: 'potrebno je pridobiti davčno številko,'},
                        {key: 'izjavo o izbiri oblike obdavčitve ,'},
                        {key: 'izjavo o prispevku k socialni varnosti (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>Obstajajo tri področja, ki zahtevajo licence/dovoljenja:</Text>
                <FlatList
                    data={[
                        {key: 'nepremičninsko in borzno posredništvo,'},
                        {key: 'opravljanje storitev transporta,'},
                        {key: 'vodenje delovne agencije, agencije za začasno delo, usposabljanja za javne sklade za brezposelnost (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>GRČIJA</Text>
                <Text>Koraki Opis:</Text>
                <FlatList
                    data={[

                        {key: '1. pridobitev odobritve imena podjetja pri Gospodarski zbornici'},
                        {key: '2. predložitev dokumentov Atenskemu odvetniškemu združenju'},
                        {key: '3. podpis členov pri notarju'},
                        {key: '4. položitev kapitala na banki'},
                        {key: '5. plačilo davka na kapital'},
                        {key: '6. pridobitev žiga odvetniškega pokojninskega sklada'},
                        {key: '7. pridobitev potrdila odvetniškega socialnega sklada'},
                        {key: '8. predložitev členov o ustanovitvi podjetja na sekretariat sodišča in pridobitev registracijske številke'},
                        {key: '9. predložitev členov o ustanovitvi za objavo v Uradnem listu'},
                        {key: '10. registracija pri Gospodarski zbornici'},
                        {key: '11. registracija pri samo zaposlitveni zavarovalni organizaciji, kmetijski zavarovalni organizaciji, itd.'},
                        {key: '12. pridobitev davčne številke za podjetje'},
                        {key: '13. izdelava žiga'},
                        {key: '14. pridobitev vezane knjige računov in računovodskega dnevnika'},
                        {key: '15. obvestitev Zavoda za zaposlovanje v roku 8 dni po zaposlitvi delavca'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Vsi novi podjetniki morajo izpolniti teh 15 korakov.
                    {"\n"}Samozaposleni morajo izpolniti korak 4 in korake od 10-15.
                    {"\n"}Za odprtje spletne trgovine za prodajo rokodelskih izdelkov ni potrebno pridobiti posebne licence ali dovoljenja (National Report Greece, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>FRANCIJA</Text>
                <Text>Ob začetku poslovanja, podjetnik uporbi INPI (National Report France, 2018).</Text>
                <Text><Text style={{fontWeight: 'bold'}}>What is INPI?</Text>
                    {"\n"}INPI ali National Institute of Industrial Property (Nacionalni inštitut za industrijsko lastnino ) je javni organ pod nadzorom Ministrstva za gospodarstvo, finance in zunanjo
                    trgovino Ministrstva za produktivno predelavo in ministrskega delegata za mala in srednje velika podjetja, inovacije in gospodarstvo (National Report France, 2018).
                    {"\n\n"}Ko je ime podjetja zavedeno pri INPI, traja 10 let.
                    {"\n"}Pravna struktura podjetja;
                </Text>
                <FlatList
                    data={[
                        {key: 'samo-zaposleni,'},
                        {key: 'samostojni podjetnik,'},
                        {key: 'družba.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Za ustanovitev podjetja sledite korakom (National Report France, 2018):</Text>
                <FlatList
                    data={[
                        {key: '1. Na Gospodarsko zbornico je potrebno vložiti zahtevane dokumente,'},
                        {key: '2. Na podlagi glavne dejavnosti je podana APE koda,'},
                        {key: '3. Davčne formalnosti,'},
                        {key: '4. Socialne formalnosti (izvaja CFE),'},
                        {key: '5. Odprtje kartoteke na pošti,'},
                        {key: '6. Odprtje bančnega računa.'}

                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
