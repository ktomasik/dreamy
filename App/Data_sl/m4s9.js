import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Zakoni o elektronskem poslovanju (e-trgovina)</Text>
                <Text style={{marginTop: 10}}>Uporabnikom morajo biti na spletnem mestu na voljo naslednji podatki:</Text>
                <Text style={{marginLeft: 10, marginTop: 5}}>Informacije o:</Text>
                <FlatList
                    data={[
                        {key: 'Ime trgovine, naslov trgovine, davčna številka ali registrska številka, elektronski naslov, telefonska številka in ime skrbnika spletne strani (in, če deluje na spletnem trgu, uradne komunikacijske informacije prodajalcev/ponudnikov);'},
                        {key: 'Podatek o tem ali spletna stran deluje pod licenco ali dovoljenjem vladne agencije,'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 25}}>{item.key}</Text>}
                />
                <FlatList
                    data={[
                        {key: 'Pogoji za obisk in uporabo spletne strani.'},
                        {key: 'Politika zasebnosti.'},
                        {key: 'Obvezen je elektronski podpis.'},
                        {key: 'Uporabniški sporazum (če je potrebno članstvo).'},
                        {key: 'Pogodba o razdalji, ki jo je treba pripraviti v skladu z uredbo o  pogodbah o razdaljah (če spletna stran prodaja blago ali storitve potrošnikom) v skladu z Zakonom o varstvu potrošnikov št.6502.'},
                        {key: 'Če želite uskladiti denarni tok med potrošniki in spletnim podjetje, mora spletno podjetje sodelovati z banko ali ponudnikom plačilnih storitev (Zakon o plačilnih storitvah št.. 6493) (Dora et al. 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
