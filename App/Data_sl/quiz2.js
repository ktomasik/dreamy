import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz2": {
            "question1": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Lovljenje rib z velikimi mrežami.",
                    "option2": "b) Tehnično osebje, ki nastavlja in popravlja spletne sisteme.",
                    "option3": "c) Skupina ljudi, ki si izmenjuje informacije.",
                    "option4": "d) Vrsta javne službe, ki pomaga ljudem."
                },
                "question": "Kaj je mreženje?"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Ustvarjanje računov socialnih medijev se razlikuje glede na različne blagovne znamke pametnih telefonov."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Skupina ljudi, ki gleda nogometno tekmo.",
                    "option2": "b) Računalniško podprta tehnologija za ustvarjanje navideznih omrežij in z namenom delitve informacij.",
                    "option3": "c) Vsakdanja tehnologija (TV, radio, itd.), kjer je možno pregledati informacije.",
                    "option4": "d) Zasebni televizijski kanal za gledanje filmov."
                },
                "question": "Kaj so socialna omrežja?"
            },
            "question4": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Instagram",
                    "option2": "b) YouTube",
                    "option3": "c) Facebook",
                    "option4": "d) Google",

                },
                "question": "Kaj izmed naštetega ni družabno omrežje?"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Redno (vsakodnevno) pošiljanje reklamnih gradiv.",
                    "option2": "b) Pošiljanje/Objavljanje čim večje količine vsebin, da je naš kanal čim bolj \"živ\".",
                    "option3": "c) Odzivanje na komentarje spremljevalcev.",
                    "option4": "d) Blokiranje ljudi na družabnih omrežjih."
                },
                "question": "Kaj je dobra praksa, da obdržimo občinstvo?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Na nekaterih spletnih mestih družabnih omrežij člani ne morejo ustvariti tematskih strani."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Izoblikovanje ciljev trženja.",
                    "option2": "b) Da imamo dovolj finančnih sredstev za zagon podjetja.",
                    "option3": "c) Da lahko zagotovimo kvalitetne proizvode.",
                    "option4": "d) Oglaševanje."
                },
                "question": "Kaj je najpomembnejše v socialnem trženju in podjetništvu?"
            },
            "question8": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Zagotovitev brezplačnega spletnega prostora.",
                    "option2": "b) Spodbujanje članov, da naložijo vsebino.",
                    "option3": "c) Dovoliti klepet v živo.",
                    "option4": "d) Vse našteto."
                },
                "question": "Katera od slednjih je značilnost skupnih družbenih medijev?"
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Isti e-mail naslovi se lahko uporabijo za prijavo v različna družabna omrežja."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) E-mail račun",
                    "option2": "b) Iskalnik",
                    "option3": "c) Družabna omrežja",
                    "option4": "d) Spam mail"
                },
                "question": "_______ pomaga deliti specifikacije izdelkov in ustvarjati oglase ter komunicirati z ljudmi neposredno na internetu."
            }
        }
    }
};

export default class M2Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz2;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                            <AwesomeAlert
                                show={showAlertTrue}
                                showProgress={false}
                                title={I18n.t('_correct')}
                                message={I18n.t('_correctFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="green"
                                onConfirmPressed={() => {
                                    this.hideAlert();
                                    this._next();
                                }}
                                titleStyle={{color: 'green', fontWeight: 'bold'}}
                            />
                            <AwesomeAlert
                                show={showAlertFalse}
                                showProgress={false}
                                title={I18n.t('_incorrect')}
                                message={I18n.t('_incorrectFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="#DD6B55"
                                onConfirmPressed={() => {
                                    this.hideAlert();
                                    this._next();
                                }}
                                titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
