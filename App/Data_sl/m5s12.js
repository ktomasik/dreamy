import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sprejemanje plačil s kreditno kartico</Text>
                <Text style={{marginTop: 15}}>Sledite navodilom za sprejemanje plačil s kreditno kartico.
                    {"\n"}Za sprejem plačil s kreditno kartico boste potrebovali:
                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Merchant Account:',
                                val2: 'Vrsta bančnega računa, na katerem so deponirana plačila s kreditno in debetno kartico.'
                            }
                        },
                        {
                            key: {
                                val1: 'Virtualni Terminal: ',
                                val2: 'Tako kot sistem za potiskanje digitalnih kreditnih kartic lahko ta sistem vnaša podatke o kreditni kartici v računalnik.'
                            }
                        },
                        {
                            key: {
                                val1: 'Gateway:',
                                val2: 'Priključek med vašo spletno trgovino in banko, ki varno pošlje podatke o plačilu, ki jih je treba odobriti ali zavrniti.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />

                <Text style={{marginTop: 10}}>Kakšen je postopek:</Text>
                <FlatList
                    data={[
                        {key: {val1: '1. korak:', val2: 'Ko stranka stopi v stik z vami, se prijavlja na vaš virtualni terminal in izbere izdelke ali storitve, ki jih želi kupiti. Nato mora vnesti informacije o svoji kreditni kartici.'}},
                        {key: {val1: '2. korak:', val2: 'Podatki o plačilu preidejo prek varnega plačilnega prehoda in se prenesejo na vir avtorizacije.'}},
                        {key: {val1: '3. korak:', val2: 'Banka, ki je izdala kreditno kartico, prejem transakcijske podatke, preveri, ali so sredstva na voljo, in odobri ali zavrne plačilo.'}},
                        {key: {val1: '4. korak:', val2: 'Plačilni prehod vam pove, če je bilo plačilo sprejeto ali zavrnjeno.'}},
                        {key: {val1: '5. korak:', val2: 'Če je plačilo odobreno, se sredstva na vaš bančni račun prenesejo v 2-3 dneh.'}},


                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />
            </View>
        )
    }
}
