import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Francijo:</Text>

                <Text style={styles.dmlH3}>Če ste iskalec zaposlitve</Text>
                <Text style={styles.dmlH3}>1. korak:</Text>
                <Text>Registracija v Zavodu za zaposlovanje kot brezposelna oseba.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
