import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je blagovna znamka?</Text>
                <Text>Blagovna znamka je ime, logotip, znak ali oblika, ki edinstveno ali v kombinaciji omogoča potrošniku, da razlikuje izdelek ali storitev od drugih na trgu (Shim 2009).</Text>
                <Text style={styles.dmlH2}>Kaj je ime blagovne znamke?</Text>
                <Text>Blagovna znamka je bodisi beseda ali so številke v neki kombinaciji, ki jih je mogoče verbalno izraziti (Shim 2009).
                    {"\n\n"}Na primer: 3M, Google, XEROX, etc.
                </Text>
                <Text style={styles.dmlH2}>Kakšna je razlika med blagovno znamko in trgovskim imenom?</Text>
                <Text>Trgovsko ime je ime za prepoznavanje podjetja. To je pravno ime, pod katerim se omenjeni družba ali posameznik odloči za poslovanje (Cameron 2017).
                    {"\n\n"}Je beseda, fraza, logotip, simbol, oblika, barva ali kombinacija več teh elementov, ki ločujejo proizvode ali storitve enega podjetja od proizvodov drugih podjetij (Cameron
                    2017, Shravani 2017).
                    {"\n\n"}Razlike med blagovno znamko in trgovskim imenom so navedene v nadaljevanju (Anonymous 2018a, Shravani 2017):
                </Text>
                <FlatList
                    data={[

                        {key: 'Blagovne znamke in trgovska imena so koristna sredstva za podjetje. Pogosto blagovna znamka ali trgovsko ime postaneta sinonim za izdelek. Napačno je, če izraza blagovna znamka in trgovsko ime zamenjujemo, saj imata zelo pomembne razlike.'},
                        {key: 'Medtem ko blagovna znamka predstavlja ugled in poslovanje v javnosti, trgovsko ime pravno ščiti tiste vidike blagovne znamke, ki so edinstveni in specifični za podjetje.'},
                        {key: 'Trgovsko ime je pravno varstvo blagovne znamke, ki jo podeljuje Urad za intelektualno lastnino.'},
                        {key: 'Vsa trgovska imena so blagovne znamke, vse blagovne znamke pa niso trgovska imena.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}><Text style={{fontWeight: 'bold'}}>Primer: </Text>
                    {"\n"}Coco Chanel je odličen primer imena za trgovsko ime. Slavna oblikovalka Coco Chanel je s svojim imenom zgradil uspešen modni imperij. Ljudje so vedeli, da bodo, ob nakupu
                    izdelka Coco Chanel, dobili kvalitetno izdelavo. Po njenem ugledu odličnega okusa je postala prepoznavna po vsem svetu. Njeno ime (Coco) se šteje za trgovsko ime, njen priimek
                    (Chanel) pa za blagovno znamko (Husbey 2016).</Text>
            </View>
        )
    }
}
