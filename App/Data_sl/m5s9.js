import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Obrazec prvega naročila</Text>
                <Text style={{marginTop: 15}}>Sledite navodilom za nastavitev obrazca za prvo naročilo.
                    {"\n\n"}Kako ustvarite preprost obrazec za spletno naročanje?
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Da bi prejeli naročila:</Text>: dobili boste informacije o te, kako ustvariti obrazec za WordPress, ki bo sprejel plačila s kreditno
                    kartico in PayPal.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1.korak: ustvarite enostaven obrazec za naročilo v programu WordPress</Text>
                <FlatList
                    data={[

                        {key: 'Pojdite na WPForms » Dodaj nov, če želite ustvariti obrazec.'},
                        {key: 'Poimenujte svoj obrazec in izberite predlogo za obrazec za plačilo/naročilo'},
                        {key: 'Pomaknite se navzdol na razdelek “Razpoložljivi predmeti” na zaslonu za predogled na desni in kliknite na to.'},
                        {key: 'Na levi se odprejo možnosti. Tukaj lahko preimenujete polje, dodate ali odstranite elemente za naročilo in spremenite cene.'},
                        {key: 'Če želite osebam dati slike iz katerih lahko izbirajo med izpolnjevanjem obrazca za naročanje, v urejevalniku obrazcev kliknite na potrditveno polje “Uporabi izbrane slike”.'},
                        {key: 'Nazadnje lahko v naročilnico dodate še druga polja, tako da jih povlečete z leve strani na desno stran.'},
                        {key: 'Ko končate, kliknite Shrani.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2.korak: Nastavite obvestila o naročilu</Text>
                <FlatList
                    data={[
                        {key: 'Obvestila so odličen način za pošiljanje e-pošte ob potrditvi vašega obrazca.'},
                        {key: 'E-poštno obvestilo lahko pošljete sami sebi, članu vase ekipe, tako da dodate njihove naslove v polje Pošiljanje e-pošte ali strankam, da  jim sporočite, da je bilo njihovo naročilo prejeto.'},
                        {key: 'V zavihku oblika kliknite jeziček Nastavitev in nato kliknite Obvestila.'},
                        {key: 'Kliknite Pokaži pametne oznake v polju Pošlji na e-poštni naslov.'},
                        {key: 'Kliknite na elektronski naslov.'},
                        {key: 'Spremenite obvestilo o e-poštnem sporočilu v bolj specifičnega. Poleg tega  lahko prilagodite e-pošto.'},
                        {key: 'Vključite osebno sporočilo, če je sporočilo namenjeno samo vam.'},
                        {key: 'Uporabite pametne oznake, če želite vključiti vse informacije, najdene v obrazcih v predloženem obrazcu za naročilo.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3.korak: konfigurirajte obliko potrditve naročilnice</Text>
                <Text>Potrditve obrazca o sporočila, ki se prikažejo strankam, ko pošljejo naročilnico.
                    {"\n"}Izbirate lahko med tremi vrstami potrditev:
                </Text>
                <FlatList
                    data={[
                        {key: '1.	Sporočilo. Ko stranka pošlje obrazec za naročila, se prikaže preprosto sporočilo, ki stranki omogoča, da ve, da je bil njihov obrazec obdelan.'},
                        {key: '2.	Pokaži stran. Ta vrsta potrditve bo stranke preusmerila na določeno spletno stran na vaši spletni strani in se jim zahvalila za naročilo.'},
                        {key: '3.	Preusmeritev na URL. Ta možnost se uporablja, če želite stranke preusmeriti na drugo spletno mesto'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Oglejmo si, kako nastaviti preprosto potrditev obrazca v WPFForms, da boste lahko sporočilo, ki ga bodo uporabniki videli po predložitvi svojih naročil,
                    prilagodili:</Text>
                <FlatList
                    data={[

                        {key: 'V Nastavitvah kliknite jeziček Potrditev.'},
                        {key: 'Izberite vrsto potrditve, ki jo želite ustvariti.'},
                        {key: 'Po želji prilagodite potrditveno sporočilo in, ko končate, kliknite Shrani.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>4.korak: Konfiguracija plačilnih nastavitev</Text>
                <Text>WPForms integrira PayPal za sprejemanje plačil.
                    {"\n"}Če želite na obrazcu za naročanje konfigurirati nastavitve plačil, morate najprej namestiti in aktivirati ustrezen vtičnik.
                    {"\n"}Ko to storite:

                </Text>
                <FlatList
                    data={[

                        {key: 'Kliknite jeziček Plačila v urejevalniku obrazcev.'},
                        {key: 'kliknite PayPal,'},
                        {key: 'Vnesite PayPal elektronski naslov,'},
                        {key: 'Izberite način proizvodnje;'},
                        {key: 'Izberite  Izdelki in storitve,'},
                        {key: 'Konfigurirajte nastavitve plačil,'},
                        {key: 'Kliknite  Shrani, da shranite nastavitve.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Zdaj ste pripravljeni na dodajanje preprostega obrazca za naročilo na vašem spletnem mestu.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>5.korak: Dodajte preprost obrazec za naročilo na svojo spletno stran</Text>
                <FlatList
                    data={[

                        {key: 'Ustvarite novo objavo ali stran v programu Word Press in kliknite gumb Dodaj obrazec.'},
                        {key: 'V meniju izberite preprost obrazec za naročanje in kliknite Dodaj obrazec.'},
                        {key: 'Objavite svojo objavo ali stran, tako da bo vase naročilo na vašem spletnem mestu.'},
                        {key: 'Pojdite na  izgled » Pripomočki  in dodajte WPForms gradnik v stransko vrstico.'},
                        {key: 'Izberite obrazec za obračun/naročilo'},
                        {key: 'Kliknite Shrani.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Zdaj lahko vidite svoj objavljen obrazec za naročilo na svojem spletnem mestu. Pri izbiri elementov na obrazcu se samodejno spremeni cena.
                    {"\n"}Zdaj veste, kako ustvariti preprost obrazec za naročanje v programi WordPress, ki sprejema spletna plačila.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', marginTop: 15}}>Če želite poslati naročila kot podjetje, lahko uporabite naročilnico v obliki Google Obrazci.</Text>
                <Text>Google Dokumenti vam omogočajo oblikovanje obrazcev, ki jih je mogoče uporabiti kot naročilnico. Ko se podatki shranijo v preglednico v Google Dokumentih, lahko obdržite obračun ali naročanje od tam.</Text>
                <FlatList
                    data={[

                        {key: '1.	Odprite Google Dokumente in kliknite gub Ustvari. Izberite Obrazec.'},
                        {key: '2.	Izpolnite ime in opis vase naročilnice.'},
                        {key: '3.	Razčlenite obrazec na dele, če je potrebno.'},
                        {key: '4.	Dodajte vprašanja s klikom na Dodaj element.'},
                        {key: '5.	S klikom na Tema izberite temo za vaš obrazec.'},
                        {key: '6.	Kliknite povezavo na dnu okna obrazca, da si ogledate obrazec v brskalniku.'},
                        {key: '7.	Delite obrazec.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}


