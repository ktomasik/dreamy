import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Donacije za mala podjetja za ženske</Text>
                <Text style={styles.dmlH2}>Za Grčijo:</Text>
                <Text style={styles.dmlH3}>Grška čezmejna skupnost</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Tudi grška skupnost v tujini je aktivna in je ustvarila nove ukrepe, kot je Envolve Award Greece, ki se nanaša na brezobrestno posojilo v višini do 500 tisoč evrov, s povračilom v petih letih.',
                                val2: 'https://envolveglobal.org/el/envolve-awards/envolve-greece'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Semenski kapital</Text>
                <Text>Semenski kapital je majhno financiranje za začetek podjetja, ki se navadno daje na določene kategorije prebivalstva, kot so mladi ali brezposelni. Glavna značilnost teh dejanj
                    je, da je kapital precej majhen (15 do 50 tisoč evrov), je predplačan in je namenjen kritju stroškov poslovanja prvega leta poslovanja družbe, da bi omogočil čas za razvoja
                    poslovanja. Običajno obstajajo ukrepi javnega programa (OAED) in zasebnega sektorja (TheOpenFund) za različne gospodarske sektorje (tradicionalni izdelki, informacijska tehnologija
                    itd.). Postopek pridobitve je preprost in ga lahko predloži kateregakoli zanima. Vendar pa te dejavnosti niso odprto skozi vse leto, medtem ko obstajajo nekateri predpogoji, kot so
                    seminarji in brezposelne kartice za javne programe (OAED).{"\n"}</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: '',
                                val2: 'http://www.digitalplan.gov.gr/portal/resource/Prosklhsh-Ypobolhs-Protasewn-sta-Ergaleia-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Sporas-Seed-ICT-Fund-kai-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Ekkinhshs-Early-Stage-ICT-Fund-gia-epiheirhseis-ston-klado-twn-Tehnologiwn-Plhroforikhs-kai-Epikoinwniwn-ICT-ths-Prwtoboylias-JEREMIE'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Partnership Agreement (PA) 2014-2020 (ESPA)</Text>
                <Text>ESPA je grški program, ki usmerja sredstva iz programa Evropske unije za odpravo neenakosti med regijami EU. V okviru ESPA vlada razporeja sredstva, namenjena trgovini, predelavi
                    ali primarni proizvodnji. Vloge se oddajo v časovno omejenih obdobjih, ki jih objavijo ustrezna ministrstva in generalni sekretariat ESPA. Ocenjevanje predlogov pripravijo
                    neodvisni ocenjevalci, rezultati so objavljeni, izvajanje predlogov se lahko odobri za obdobje od enega do treh let. Financiranje je lahko odstotek naložbe ali znižanja davka
                    (običajno 40-60%). Potrebni so dokumenti o izdatkih, ki se jih pregleduje na mestu pregleda samega.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'The great advantage of ESPA is its availability, but as funding follows costs, it is not particularly useful for new businesses. However, it is a particularly attractive option to be combined with other forms of financing (bank loans, venture capital, etc.).',
                                val2: 'https://www.espa.gr/en/pages/staticPartnershipAgreement.aspx'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}} onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />


                <Text>Velika prednost ESPA je njena razpoložljivost. Ker financiranje sledi stroškom ni posebej uporabno za nova podjetja. Še posebej je privlačna možnost združitve z drugimi oblikami
                    financiranja (bančna posojila, tvegani kapital itd.).</Text>

                <Text>Med drugim program ESPA 2014-2020 financira naslednje pobude:</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Zagonska in nova podjetja', val2: 'https://www.efepae.gr/frontend/articles.php?cid=496&t=Neofuis-Epixeirimatikotita'}},
                        {
                            key: {
                                val1: 'Nadgradnja mikro in malih podjetij za razvoj svojega znanja na novih trgih',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=497&t=Anabathmisi-polu-mikrwn-&-mikrwn-epixeirisewn-gia-tin-anaptuksi-twn-ikanotitwn-tous-stis-nees-agores'
                            }
                        },
                        {key: {val1: 'Čezmejno poslovanje', val2: 'https://www.efepae.gr/frontend/articles.php?cid=539&t=Epixeiroume-Eksw'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
