import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img1.png')
    }
}];

export default class M6S3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Izdajanje računov na mobilnih napravah</Text>
                <Text style={styles.dmlH3}>Za iOS</Text>
                <Text>Brezplačna različica Invoice Simple je na voljo za iPhone, iPad in iPod Touch in se lahko uporablja za ustvarjanje 3 brezplačnih računov ali ocen na vaši mobilni napravi.</Text>
                <Text style={{fontSize: 18, marginTop: 10}}><Text style={{fontWeight: 'bold'}}>1. korak:</Text> odprite aplikacijo App Store in poiščite »račun« (za potek iskanja glejte moduli 1 in
                    2).</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
