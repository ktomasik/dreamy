import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Plačilni sistem za elektronsko poslovanje</Text>
                <Text style={{marginTop: 15}}>Plačilni sistem za elektronsko poslovanje je način opravljanja transakcij ali plačevanja blaga in storitev preko elektronskega medija brez uporabe čekov
                    ali gotovine.</Text>
                <Text style={styles.dmlH3}>Kaj je nastavitev Samodejno plačilo?</Text>
                <Text> Samodejno plačilo računa je prenos denarja, ki je predviden na vnaprej določen datum za plačilo ponavljajočega računa. Samodejna plačila so rutinska plačila bančnega,
                    posredniškega ali vzajemnega sklada računom prodajalcem. Običajno so samodejna plačila nastavljena pri podjetju, ki prejme plačilo, čeprav je mogoče samodejno plačilo izvesti tudi
                    prek tekočega računa na spletu.</Text>
                <Text style={styles.dmlH3}>Sprejemanje spletnega plačila s kreditno kartico, debetno kartico ali PayPal</Text>
                <Text>Omogočanje strankam, da s svojo kreditno kartico plačajo na vašem spletnem mestu, je najbolj osnovni način za sprejetje spletnega plačila. Če želite ponuditi to funkcijo svojim
                    strankam, se morate odločiti, ali želite imeti svoj namenski trgovski račun ali uporabiti posredniški račun. Mala podjetja in organizacije, ki želijo za storitve, naročnine ali
                    izdelke, ki jih prodajajo na spletnem mestu sprejeti plačila s spletnimi plačilnimi karticami, lahko dodajo gumb PayPal na katerokoli spletno mesto.
                </Text>
                <Text style={styles.dmlH3}>Varen spletni plačilni sistem</Text>
                <Text>Spletna varnost je nekaj, kar nas vse potrošnike skrbi. Še bolj pomembna je za lastnike podjetij. S spletnimi plačili prevzamete odgovornost za zaščito podatkov vaših strank in
                    varno upravljanje je lahko drago breme. Olajšate si lahko z uporabo plačilne rešitve, ki je združljiva s PCI. Skladnost s PCI se nanaša na pravila in predpise, ki urejajo varstvo
                    podatkov.</Text>
                <Text style={styles.dmlH3}>Rešitve za spletno plačilo s kartico, računom in banko</Text>
                <Text>Spletna plačila so narejena takoj, zato so priročna, prihranijo veliko časa.
                    {"\n"}Tako imenovane “spletne denarnice” svojim strankam omogočajo, da:
                </Text>
                <FlatList
                    data={[

                        {key: '1. Plačujejo na spletu z razkrivanjem podatkov o kreditni kartici,'},
                        {key: '2. Plačujejo račun,'},
                        {key: '3. Plačujejo na bančni račun.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH3}>Kaj je virtualen POS?</Text>
                <Text>Virtualen POS Terminal. Je plačilni prehod, ki spletnim trgovcem in prodajalcem storitev omogoča ročno odobritev kartičnih transakcij, ki jih sproži kupec. Ta postopek močno širi
                    svoje vire plačil in zmanjša čas plačilnega procesa, pri čemer dodaja dodatno varnost. Integracija virtualnega POS terminal je mogoča s številnimi platformami e-poslovanja.</Text>
            </View>
        )
    }
}
