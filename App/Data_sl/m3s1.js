import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je e-poslovanje in e-prodaja?</Text>
                <Text>Elektronsko poslovanje (e-poslovanje) se nanaša na uporabo spleta, interneta, intranetov, ekstranetov ali nekaterih kombinacij v namene podjetništva.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img1.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img2.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>Kako in kje prodajati ročne izdelke preko spleta?</Text>
                <Text>Internet je edinstveno okolje s katerim lahko promovirate in prodajate ročno izdelane izdelke strankam po vsem svetu.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img3.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>Najboljše spletne strani za prodajo izdelkov</Text>
                <Text style={styles.dmlH3}>Etsy</Text>
                <Text>Etsy (<Text style={{color: 'blue'}}
                                  onPress={() => Linking.openURL('https://www.etsy.com/')}>etsy.com</Text>) je raznolika skupnost več kot 30 milijonov kreativnih poslovnežev ki so
                    registrirani na tej spletni strani. Na tem portalu lahko najdemo zelo široko paleto izdelkov ti vključujejo tudi Potrebščine in surovine za umetniške potrebe kot tudi polizdelke in
                    končne umetnine.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img4.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Amazon</Text>
                <Text>Svetovno največja spletna trgovina z zelo veliko izbiro knjig, revij, glasbe, video vsebin, računalnikov, elektronike, programov, dodatkov, čevljev… Je zelo učinkovita platforma
                    za reklamiranje kakršnih koli izdelkov. (<Text style={{color: 'blue'}}
                                                                   onPress={() => Linking.openURL('https://www.amazon.com/')}>amazon.com</Text>)</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img5.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Bonanza</Text>
                <Text>Bonanza (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.bonanza.com/')}>bonanza.com</Text>) ponuja najpreprostejšo navigacijo
                    in privlačno stran, ki vam bo omogočila, da prodajate vaše ročno izdelane predmete potrošnikom. Z nekaj preprostimi kliki lahko ustvarite svoj profil, naložite oglase in začnite
                    prodajati.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img6.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>eBay</Text>
                <Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.ebay.com/')}>eBay</Text> je eno od tistih spletnih mest, ki je dobesedno spremenilo svet elektronske
                    trgovine. Z več kot 100 milijoni kupcev je eBay eden najbolj prepoznavnih spletnih trgov po vsem svetu.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img7.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>ArtFire</Text>
                <Text>ArtFire (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.artfire.com/')}>artfire.com</Text>) je hitro rastoče spletno tržišče za prodajo ročno izdelanih
                    izdelkov, ki so jih izdelali obrtniki po vsem svetu. Z več kot 30.000 registriranimi aktivnimi prodajalci, ArFire ponuja preprosto platformo, ki omogoča tudi prilagoditvene
                    možnosti.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img8.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>DaWanda</Text>
                <Text>DaWanda (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('http://en.dawanda.com/')}>dawanda.com</Text>) je eno vodilnih spletnih trgov za prodajo edinstvenih in
                    ročno izdelanih predmetov. Z več kot 280.000 prodajalci, ki delujejo na DaWandi, je plemenit pristop za privabljanje kupcev.</Text>
                <Text style={styles.dmlH3}>Zibbet</Text>
                <Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.zibbet.com/')}>Zibbet</Text> je spletno tržišče za prodajo ročno izdelanih predmetov, od
                    umetniških stvaritev in fotografij do starinskega in obrtnega pripomočkov in surovin. </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img9.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
