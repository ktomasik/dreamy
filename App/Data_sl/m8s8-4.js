import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img11.png')
    }
}];

export default class M8S8_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Za pošiljanje tovora upoštevajte spodaj navedene korake: </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>KORAK 4: IZBERITE PODJETJE IN ODDAJTE PAKET </Text>
                <Text>Nekateri načini, kako izbrati prevozno podjetje::</Text>
                <FlatList
                    data={[
                        {key: 'Združenja (branže),'},
                        {key: 'Združenja prevoznikov (branže prevoznikov),'},
                        {key: 'Internetna spletna stran, '},
                        {key: 'Mobilne aplikacije, '},
                        {key: 'Sms sporočilo,'},
                        {key: 'Samopostrežni avtomat,'},
                        {key: 'Klicni center.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img11.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
