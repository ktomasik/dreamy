import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'


// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kje lahko najdete posojila za majhna podjetja za ženske?</Text>
                <Text style={styles.dmlH2}>Za Grčijo:</Text>
                <Text style={styles.dmlH3}>Banks</Text>
                <Text>Ko banke financirajo nova podjetja, zahtevajo ogled poslovnega načrta. Aplikacijo načrta prouči več skupin, ki preverjajo različne teme, še zlasti je pomembno, da je poslovni
                    načrt popolnejši, da bi se izognili zamudam.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
