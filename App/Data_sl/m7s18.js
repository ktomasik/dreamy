import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S18 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Davčne spodbude za zagonska podjetja žensk</Text>
                <Text style={styles.dmlH2}>Za Turčijo:</Text>
                <Text>V skladu s členom 9/6 Zakona o davku od dohodka so tisti, ki izdelujejo in prodajajo ročno izdelane izdelke v domovih, v katerih živijo, izvzeti iz davka (zakon št. 193, Uradni
                    list št. 28366 z dne 27.7.2012).</Text>
                <Text>Za oprostitev davka je potrebna pridobitev "Obrtnega potrdila o oprostitvi" (Esnaf Vergi Muafiyeti Belgesi).{"\n"}</Text>

                <Text>
                    Za pridobitev oprostitve obrtnega dovoljenja se izvaja sledeča procedura:
                    {"\n\n"}Korak 1: Osebe, ki želijo pridobiti potrdilo, se morajo prijaviti pri davčnem uradu v kraju, kjer se nahaja njihovo prebivališče s peticijo.
                    {"\n\n"}Korak 2: Na potrdilu o davčni oprostitvi  se zapišejo informacije kot vrsto dejavnosti, ki se izvaja na: Izdelava in prodaja ročno izdelanih izdelkov v hišah, v katerih živijo (člen ITL: 9/6).
                    {"\n\n"}Korak 3: Domači naslov je prikazan kot naslov izvajanja .dela
                    {"\n\n"}Korak 4:  Če je sprejeto, da so izpolnjeni pogoji iz 9. člena zakona o davku od dohodka, bo davčni urad izdal potrdilo.
                    {"\n\n"}Korak 5: Za imetnika certifikata se taksa ne zaračuna.
                    {"\n\n"}Korak 6: Potrdilo o oprostitvi davka na olajšave velja tri leta od datuma izdaje. Pridobitev novih dokumentov je mogoča s prijavo na davčnem uradu po koncu tega obdobja.
                </Text>

            </View>
        )
    }
}
