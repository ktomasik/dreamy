import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kako zajeti fotografije izdelka in jih naložiti v spletu</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>1. Korak: kliknite na "fotoaparat" na ekranu vašega mobilnega telefona</Text>
                <View style={{alignItems: 'center'}}>
                <Image
                    style={{flex: 1, width: 300, height:350}}
                    source={require('../Images/m3/img17.png')}
                    resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
