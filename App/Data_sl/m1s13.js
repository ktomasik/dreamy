import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računa za e-naslov na mobilnih telefonih sistema Android (Samsung, Sony, HTC ..)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 4: Vnesite vaš polni elektronski naslov in kliknite “manual setup” (z vašim prejšnjim email računom, geslom in opisom vašega računa)
                </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img15.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
