import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S64 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uporaba socialnih omrežij</Text>
                <Text style={styles.dmlH2}>Ustvarite Blog</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 19: Če želite, da to vsebino objavite tudi v portalu Google+, potem izberite Deli (Share).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img136.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
