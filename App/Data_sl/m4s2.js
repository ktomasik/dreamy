import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";
// Styles

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m4/img1.png')
    }
}];

export default class M4S2 extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kakšna je razlika med poslovnim imenom, trgovskim imenom in pravnim imenom?</Text>
                <Text>Ime podjetja je uradno ime osebe, ki je lastnik podjetja.
                    {"\n"}Je pravno ime podjetja. Uporablja se na vladnih obrazcih in vlogah (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Primer:</Text> Podjetnikovo ime je John Smith in ima v lasti zavarovalništvo. Zakonito ime podjetja je Zavarovalništvo John Smith
                    (Anonymous 2018b).
                    {"\n\n"}Lastniki podjetij lahko uporabljajo trgovsko ime za namene oglaševanja in prodaje. Trgovsko ime je ime, ki ga javnost vidi, tako kot na znakih in internetu (Cameron 2017).
                    {"\n"}Ime podjetja in trgovsko ime sta lahko različna. Trgovskemu imenu ni treba vključevati pravnih končnic (npr. d.o.o ali s.p., itd.), ki se uporabljajo za postopke obdavčitve
                    (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Primer:</Text> McDonald’s je trgovsko ime. Pravno ime podjetja je McDonald’s Corporation (Anonymous 2018c).
                </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m4/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 10}}>
                    Pravno ime podjetja je ime osebe, ki je lastnik podjetja. Če je podjetje partnerstvo, je pravno ime, ki je navedeno v sporazumu o partnerstvu ali priimek partnerjev.
                    {"\n\n"}Za družbe z omejeno odgovornostjo (d.o.o) in korporacije, je poslovno pravno ime, ki je bilo registrirano pri državni vladi. Ta imena imajo pogosto pravno sprejete konce,
                    npr. d.o.o (Fishman 2015).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10}}>Kdaj naj se uporabi pravno in trgovsko ime?</Text>
                <Text>Pri komuniciranju z vlado ali drugimi podjetji je treba uporabiti pravno ime.
                    {"\n"}Na primer, pravno ime podjetja je treba uporabiti pri vložitvi davčnih napovedi, nakupu lastnine ali pisanju čekov.
                    {"\n"}Podjetje lahko uporablja trgovsko ime za namene oglaševanja in trgovanja Pogosto je to ime, ki ga splošna javnost vidi na znakih, internet in v oglasih (Anonymous 2018d).
                    {"\n\n"}V bistvu, pravno ime se uporablja za pravne postopke, trgovsko ime pa za odnose z javnostjo.
                </Text>
            </View>
        )
    }
}
