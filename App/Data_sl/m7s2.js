import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je posojilo?</Text>
                <Text>Posojilo je denar izposojen na banki ali od drugega posojilodajalca. Plačilo posojila je razdeljeno na dva dela, na glavnico in na obresti.</Text>
                <Text>Glavnica je znesek, ki si ga izposodite in je glavni del salda tega računa.</Text>
                <Text>Obresti so dajatve zaračunane za čas posojila in se izračunajo na glavnico.</Text>
                <Text style={styles.dmlH2}>Kakšna je posojilna bilanca?</Text>
                <Text>Saldo posojila je znesek, ki ga je treba plačati za vaše posojilo. Vsako posojilo ima posojilno bilanco, dokler posojilo ni v celoti izplačano. Spreminja se vsak dan (obresti se
                    dodajajo dnevno).</Text>
                <Text>Časovni razpored amortizacije posojila; glavnica in obresti sta ločeni, da lahko vidimo, kateri del mesečnega plačila gre za odplačilo glavnice in kateri del za plačilo
                    obresti..</Text>
                <Text style={styles.dmlH2}>Kaj so donacije?</Text>
                <Text>Donacije so nepovratna sredstva ali izdelki, izplačani ali podarjen od ene stranke, pogosto vladnega oddelka, korporacije, fundacije ali zaupnika prejemniku. Državna podpora
                    (donacija) je finančna nagrada, ki jo dodeli država ali lokalna vlada upravičencu.</Text>
                <Text style={styles.dmlH2}>Kaj je davčna spodbuda?</Text>
                <Text>Davčna spodbuda je vladni ukrep, katerega namen je spodbuditi posameznike in podjetja, da porabijo denar ali da prihranijo denar z zmanjšanjem zneska davka, ki ga morajo plačati.</Text>
            </View>
        )
    }
}
