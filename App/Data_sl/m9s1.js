import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Uvod</Text>
                <Text>Do tega modula ste se naučili oblikovati e-mail račun in ga namestiti na vaše pametne telefone. Poleg tega ste se naučili nastaviti račune spletnih socialnih omrežij. Hkrati pa
                    ste se naučili, kako fotografirati vaše izdelke in jih prenesti na te platforme. Namen tega modula je razložiti, kako namestiti vaše obstoječe račune socialnih spletnih omrežij za
                    vaše podjetje.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
