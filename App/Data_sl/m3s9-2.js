import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_2 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Upoštevajte spodnja navodila za prodajo izdelkov v Instagramu</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>2. Korak: uporabite svoje že nameščeno ime instagram računa ali sprejmite novo ime poslovnega računa za komercializacijo vaših izdelkov</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m3/img37.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
