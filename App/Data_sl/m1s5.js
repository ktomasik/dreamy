import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Iskanje v brskalniku Google in oblikovanje računa za e-naslov na pametnemu telefonu</Text>
                <Text style={{marginTop: 10, fontSize: 18 }}>Korak 3: Napišite karkoli želite ali stavek, izberite enega, ki ga želite pogledati, da vidite rezultate</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img2.png')}
                        resizeMode="contain"/>
                        <Text style={{marginTop: 10}}/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m1/img3.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
