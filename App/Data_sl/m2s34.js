import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S34 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sledite naslednjim korakom za povezavo Instagrama z ostalimi socialnimi omrežji</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 4: Kliknite v polje “Napiši komentar” (write a comment) in na kratko opišite fotografijo.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img65.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
