import React, {Component} from 'react'
import {Text,View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S16 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Viri in literatura</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Techopedia',val2: ' https://www.techopedia.com/definition/1493/electronic-business-e-business'}},
                        {key: {val1: 'Small Business',val2: 'http://smallbusiness.chron.com/definition-ecommerce-e-business-2255.html'}},
                        {key: {val1: 'Crafter Coach',val2: 'http://craftercoach.com/10-best-places-to-sell-handmade-crafts/'}},
                        {key: {val1: 'Orderhive',val2: 'https://www.orderhive.com/top-5-online-marketplaces-to-sell-handmade-items'}},
                        {key: {val1: 'Shopify',val2: 'https://www.shopify.com/guides/product-photography/importance'}},
                        {key: {val1: 'Volusion',val2: 'https://www.volusion.com/blog/how-to-sell-products-on-instagram/'}},
                        {key: {val1: 'Wikipedia',val2: 'https://en.wikipedia.org/wiki/Logo'}},
                        {key: {val1: 'Bourn Creative',val2: 'https://www.bourncreative.com/what-is-the-difference-between-a-logo-and-a-brand/'}},
                        {key: {val1: 'MH Themes',val2: 'https://www.mhthemes.com/blog/how-choose-right-logo-design-for-business/'}},
                        {key: {val1: 'Roggio, A. (2017, February 23). Practical E-commerce:',val2: 'https://www.practicalecommerce.com/3-Retail-Pricing-Strategies-for-Ecommerce'}},
                        {key: {val1: 'Tanır, B. (2016, Novermber 28). Prisync Blog',val2: 'https://blog.prisync.com/ecommerce-pricing-strategies/'}},
                        {key: {val1: 'etsy.com',val2: 'https://www.etsy.com/'}},
                        {key: {val1: 'godaddy.com/domains/domain-name-search',val2: 'https://godaddy.com/domains/domain-name-search'}},
                        {key: {val1: 'wix.com',val2: 'https://www.wix.com/'}},
                        {key: {val1: 'freelogoservices.com',val2: 'https://www.freelogoservices.com/'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
