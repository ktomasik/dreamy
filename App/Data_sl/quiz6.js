import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz6": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Samo iz Google Play.",
                    "option2": "b) Samo iz Apple Store.",
                    "option3": "c) Iz nobenega naštetega.",
                    "option4": "d) Iz obeh."
                },
                "question": "S kje si lahko prenesete predlogo računa za Apple telefon?"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Apple Store lahko uporabite za ustvarjanje računa na Androidu."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Nove knjige.",
                    "option2": "b) Moon račun.",
                    "option3": "c) Wave.",
                    "option4": "d) Pinterest."
                },
                "question": "Katera aplikacija omogoča ustvarjanje elektronskega računa na Apple-u in na Androidu?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Dodajanje logotipa na račun ni nujno, je pa bolje, če ga uporabimo."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Ime in naslov.",
                    "option2": "b) Naslov kontaktne osebe, telefon.",
                    "option3": "c) Davek.",
                    "option4": "d) Vse našteto."
                },
                "question": "Kateri podatki morajo biti navedeni na računu?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Identitete trgovca.",
                    "option2": "b) Računa za družbena omrežja.",
                    "option3": "c) Podrobnosti o DDV.",
                    "options": "d) Kontaktne podatke."
                },
                "question": "Kaj ni potrebno, da vsebuje tipična spletna prodajna pogodba?"
            },
            "question7": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Prodajalec ne more biti odgovoren za izdelke, prodane pod imenom njegovega podjetja."
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Ni koristno, da imate zavarovanje za poslovno blago."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Mesto za razvrščanje dokumentov.",
                    "option2": "b) Prosta in priročna platforma za elektronski podpis, ki se uporablja na mobilnih telefonih.",
                    "option3": "c) Dokument, ki ga moramo podpisati pri vsaki prodaji.",
                    "option4": "d) Nič od naštetega."
                },
                "question": "Kaj je DocuSign?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Blagajna",
                    "option2": "b) DocuSign",
                    "option3": "c) DropBox",
                    "option4": "d) Pinterest"
                },
                "question": "_______ ponuja predlogo, ki jo lahko prenesete v Microsoft Word."
            }
        }
    }
};

export default class M6Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz6;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>

                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
