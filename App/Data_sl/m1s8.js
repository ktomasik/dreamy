import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Iskanje v brskalniku Google in oblikovanje računa za e-naslov na pametnemu telefonu</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 7: Uporabite račun , ki si ste ga ustvarili za prijavo v Gmail-u in po končanem postopku pošljite e-sporočilo na:
                    <Text style={{color: 'blue'}}>m-learning@gmail.com</Text>.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img7.png')}
                        resizeMode="contain"/>
                    <Text style={{marginTop: 10}}/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m1/img8.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Čestitamo, ustvarili ste svoj e-mail račun!</Text>
            </View>
        )
    }
}
