import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S21 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ustvari Instagram računu</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 8: Izpolnite vaše ime (full name) in geslo (password). Nato kliknite NAPREJ (next). To je zadnji korak ustvarjanja Instagram
                    računa.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img37.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img38.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img39.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
