import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S56 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uporaba socialnih omrežij</Text>
                <Text style={styles.dmlH2}>Ustvarite Blog</Text>
                <Text style={{marginTop: 10}}>Predno začnete z objavami na blogu morate izbrati primerno spletno mesto za blog. V svetovnem spletu lahko najdete več takih spletnih strani, ki ponujajo
                    brezplačne možnosti kot naprimer: WordPress in Blogger. Obe platformi omogočata objavo vsebin preko mobilnih naprav.</Text>
                <Text style={{marginTop: 10}}>Bistvena razlika meb Blogger in WordPress je, da je portal Blogger bolj enostaven za uporabo, WordPress pa je bolj prilagodljiv in omogoča lažji prenos
                    vsebin na domači strežnik (Bozzo 2014).</Text>
                <Text style={{marginTop: 10}}>Zaradi enostavnost, bomo v tem primeru uporabili portal Blogger...</Text>
            </View>
        )
    }
}
