import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sprejemanje plačil z bančnimi računi</Text>
                <Text style={{marginTop: 15}}>Sledite spodnjim navodilom za sprejemanje plačil z bančnimi računi.
                    {"\n\n"}Bančno nakazilo se izvede na naslednji način:
                </Text>
                <FlatList
                    data={[
                        {key: 'Podjetje, ki želi opraviti transfer pride v banko in ji da nalog, da prenese določen znesek denarja. IBAN n BIC kode so podane tako, da banka ve, kam denar nakazati.'},
                        {key: 'Banka pošiljateljica pošlje banki prejemnici preko varnega Sistema zahtevo, da izvede plačilo v skladu z danimi navodili.'},
                        {key: 'Sporočilo vsebuje tudi navodila za poravnavo. Dejanski prenos ni takojšen: sredstva se prenesejo v nekaj urah ali celo dneh.'},
                        {key: 'Vse vpletene banke morajo imeti med seboj vzajemni račun.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Preden lahko prejmete mednarodno plačilo, morate pošiljatelju poslati nekaj podrobnosti, vključno z:</Text>
                <FlatList
                    data={[

                        {key: 'vašo mednarodno številko bančnega računa (IBAN)'},
                        {key: 'številko banke'},
                        {key: 'številko računa'},
                        {key: 'vaše polno ime'},
                        {key: 'vaš naslov'},
                        {key: 'znesek in valuto v kateri želite prejeti plačilo'},

                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
