import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Donacije za mala podjetja za ženske</Text>
                <Text style={styles.dmlH2}>Za Turčijo:</Text>
                <Text style={styles.dmlH3}>KOSGEB</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Organizacija za razvoj malih in srednjih podjetij (KOSGEB) je glavni organ, odgovoren za razvoj, koordinacijo in izvajanje malih in srednje velikih podjetij. KOSGEB nudi vladno posojilno podporo podjetnicam, ki želijo ustanoviti lastno podjetje. V okviru tega področja se zagotavlja kreditna podpora v višini 50.000 TL (10.000 EUR) za vloge za posojilo KOSGEB. Ta podpora, ki jo zagotavlja država, je nepovratna. Poleg te podpore dotacije nudijo ženskam brezobrestno kreditno podporo. Do leta 2018 je posojilo v vrednosti 70.000 TL (14.000 EUR). Prvo poslovanje ženskih podjetnic bo oproščeno davkov v prvih treh letih poslovanja.',
                                val2: 'https://www.kosgeb.gov.tr/site/tr/genel/detay/6057/kadin-girisimciligi-women-entrepreneurship'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
