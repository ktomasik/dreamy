import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S15 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Donacije za mala podjetja za ženske</Text>
                <Text style={styles.dmlH2}>Za Francijo:</Text>
                <Text style={styles.dmlH3}>PRI (Regionalno inovativno partnerstvo)</Text>

                <FlatList
                    data={[
                        {key: {val1: '', val2: 'https://www.bpifrance.fr/Toutes-nos-solutions/Aides-concours-et-labels/Aides-a-l-innovation-projets-individuels/PRI-Faisabilite'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text>Ta inštrument je odprt za ustvarjanje inovativnih MSP, vendar to ni njihov prednostni cilj.
                    {"\n\n"} Poteka v partnerstvu le s 5 regijami: Grand Est (Alsace, Champagne-Ardenne, Lorraine), Hauts de France (Aquitaine / Poitou Charentes, Pays de la Loire, PACA).
                    {"\n\n"} Izbrani bodo najbolj inovativni projekti katerim bo dodeljeno največ 100.000 do 200.000 evrov na projekt. Pomoč omogoča kritje stroškov, povezanih s predhodnimi študijami
                    in izvajanjem projekta. Izplačuje se v dveh obrokih (70% in 30%).
                    {"\n\n"} Projekt MSP mora potekati največ 12 mesecev.</Text>

            </View>
        )
    }
}
