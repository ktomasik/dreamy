import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S4_8 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kako izdelamo račun v Google Dokumentih</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight:'bold'}}>8. korak: </Text> Na padajočem meniju izberite in kliknite na gumb "My Drive" ali v prevodu "moj pogon".</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m6/img17.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
