import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S49_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uporaba socialnih omrežij</Text>
                <Text style={styles.dmlH2}>Ustvarite FaceBook stran</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 3: Kliknite na gumb USTVARI STRAN (CREATE PAGE).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img100.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
