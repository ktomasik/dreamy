import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img1.png')
    }
}];

export default class M8S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Pakiranje & Trženje</Text>
                <Text style={styles.dmlH3}>Kalkuliranje</Text>
                <Text>Ko gre za obračun tovora, je eno izmed pomembnih dejstev gostota. Vsak predmet, za katerega ste slišali, ima gostoto. Tipično je, da večja kot je gostota predmeta, nižje so
                    obračunske vrednosti. Pri obračunu tovora blaga nižje kakovosti velike gostote, bodo lahko stroški transporta nižji, kot pa pri izdelkih visoke kakovosti z manjšo gostoto
                    (Anonymous 2018c).
                    {"\n\n"}Izračun tovorne gostote lahko zagotovi priporočen razred pošiljk. Grafikon klasifikacije tovornih razredov nam je lahko v pomoč pri razvrščanju bodočih pošiljk (Anonymous
                    2018d).

                </Text>

                <Text style={styles.dmlH3}>Tehnike pakiranja </Text>
                <Text style={{fontWeight: 'bold'}}>Osnovna metoda pakiranja (metoda enonjnih kartonov)</Text>
                <FlatList
                    data={[

                        {key: 'Pakiranje ne krhkih izdelkov kot je mehko blago znotraj trdnega kartona.'},
                        {key: 'Za polnilo se uporabi zmečkan časopisni papir, stiroporne kosmiče ali z zrakom napolnjene vrečke, ki se jih naloži po praznem prostoru, da se izogne premikanju blaga znotraj kartona med prevozom.'},
                        {key: 'Blago, ki bi se med prevozom lahko umazalo, zmočilo ali navlažilo, se namesti v plastično vrečko.'},
                        {key: 'Združite manjše dele ali zrnate izdelke v močno zaprto embalažo, kot je močna plastična vreča, odporna proti obrabi in jo naložimo v kartonsko škatlo.'},
                        {key: 'Za zapiranje embalaže uporabite lepilni trak v obliki H (Anonymous 2018e).'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{fontWeight: 'bold', marginTop: 5}}>1.4.2.2 Metoda paket v paketu</Text>
                <FlatList
                    data={[

                        {key: 'Ovijte izdelek posamično z vsaj 5-cm (2”) debelo mehurčkasto folijo ali penasto folijo, da se zapolni prostor znotraj kartona.'},
                        {key: 'Da se blago ne bi premikalo po škatli, uporabite polnilo kot je zmečkan časopisni papir, stiroporne kosmiče ali drug z zrakom polnjen material.'},
                        {key: 'Zaprite in zalepite notranji karton z lepilnim trakom v obliki H. To  bo pomagalo preprečiti nenadno odpiranje.'},
                        {key: 'Uporabite zunanji karton, ki je vsaj 15 cm (6”) daljši, širši in globlji kot notranji karton.'},
                        {key: 'Izberite ovojno metodo ali metodo polnila, pri notranjem paketu znotraj večjega kartona.'},
                        {key: 'Pošiljajte občutljivo blago posamično, ovitega z vsaj 8-cm (3”) debelo mehurčkasto folijo .'},
                        {key: 'Ovijte notranji karton z 8-cm (3”) mehurčkasto folijo ali uporabite vsaj 8-cm(3”) na debelo polnilo iz stiropornih kosmičev oz. drug z zrakom napolnjen material, da bi zapolnili prostor med notranjim in zunanjim kartonom na vrhu, dnu in z vseh strani.'},
                        {key: 'Zapolnite vse prazne dele z več zračne folije.'},
                        {key: 'Pri zapiranju kartona uporabite lepilni trak v obliki H (Anonymous 2018e).'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
