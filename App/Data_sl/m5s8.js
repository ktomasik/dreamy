import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Prejemanje plačil</Text>
                <Text style={{marginTop: 15}}>Sledite navodilom za nastavitev podjetja za prejemanje plačil.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1.korak: uredite svoje podjetje</Text>
                <Text>Preverite možnosti v naprej, da ustvarite pravo organizacijo in strukturo podjetja. Na ta način ne boste na koncu imeli napačne vrste poslovanja. V številnih primerih ga lahko
                    pozneje spremenite, vendar raziskovanje pred časom pomaga, da vidite, kaj bi bilo najbolje za vas.
                    {"\n\n"}Če imate partnerje, se prepričajte, da so vsi njihovi podatki točni.

                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2.korak: Pridobite davčno številko podjetja</Text>
                <Text>Bistven korak je pridobitev davčne številke podjetja. Ta številka je potrebna za odprtje poslovnega bančnega računa. Morda boste potrebovali več kot eno davčno številko (za DDV
                    pri registraciji pri davčnem organu, ob prijavi v lokalno zbornico, itd.). Davčna številka je lahko tudi koristna, če želite prejemati plačila prek procesorja, kot je PayPal. Prav
                    tako se mora davčna številka pojaviti na vseh prejemkih in računih, ki jih izdate.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3.korak: Odprtje poslovnega bančnega računa</Text>
                <Text>Ko začnete prejemati plačila, potrebujete mesto, kamor želite dobiček shranjevati. Kot lastnik podjetja je pomembno, da mate ločene račune. Dobro je, da ostanejo vaša poslovna
                    sredstva ločena od vaših osebnih sredstev. Olajša vodenje evidenc, ker je vse na enem mestu.
                    {"\n\n"}Če pomešate svoj denar s finančnim premoženjem vašega podjetja, imate lahko resne težave. Vedno morate imate v mislih, da čas, ko ste prejeli denar od svojih strank ne
                    sovpada s časom, ko morate plačati npr. najemnino, davke, vašim dobaviteljem, itd. Če ne razumete, kako pomembno je, da se razlikuje med osebnimi sredstvi in finančnimi sredstvi
                    podjetja, lahko rezultira pomanjkanje denarja, ko pride do dejanskih plačil, ker ste denar porabili za osebne stroške.
                    {"\n\n"}Na poslovnem bančnem računu bi morali imeti svoj dohodek in opravljati vse transakcije vašega podjetja.

                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>4.korak: Nastavite prejemanja plačil prek tretje osebe</Text>
                <Text>Pomembno je, da sprejmete več načinov plačila. Vaše stranke imajo svoje plačilne preference. Eden od najboljših načinov, kako jih prepričate, da jih sprejemate je uporaba
                    procesorja tretje osebe. Lahko vzpostavite račun trgovca s procesiranjem kartic ali prejemate plačila prek spleten strani, kot je PayPal. Pri uporabi tega načina se poskrbi za
                    kreditne kartice. Možno je tudi, da procesorje uporabite za sprejemanje fizičnih kreditnih kartic iz vase mobilne naprave. To je lahko koristno, če imate za prodajo fizične
                    predmete in lahko plačilo izvedete osebno.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>5.korak: Državne poslovne zahteve</Text>
                <Text>Preden uredite nastavitve za prejemanje plačil se prepričajte, da poznate zahteve za podjetja v vaši državi. Ministrstvo za gospodarstvo (ali drug pristojni organ) v vaši državi
                    mora imeti informacije o tem, kaj morate narediti, če želite postaviti svoje podjetje in začnete poslovati.</Text>
            </View>
        )
    }
}



