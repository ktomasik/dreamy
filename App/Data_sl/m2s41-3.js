import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S41_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Naredite Twitter račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 7: Nato kliknite Vpiši se (Sign up) in počakajte na SMS sporočilo s kodo za preverjanje…</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img81.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
