import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Osnovni pojmi in pojasnila</Text>
                <Text>
                    Kot ste videli v modulu 1, morate razlikovati med internetom in spletom: internet je mreža globalnih izmenjav - vključno z zasebnimi, javnimi, poslovnimi, akademskimi in vladnimi omrežji - povezana z vodenimi, brezžičnimi in optičnimi tehnologijami
                    {"\n\n"}Spletni ali svetovni splet (W3) je v bistvu sistem internetnih strežnikov, ki podpirajo posebej oblikovane dokumente.
                    {"\n\n"}Internet, ki povezuje računalnik z drugimi računalniki po vsem svetu, je način prevoza vsebine. Do interneta morate dostopati, da si ogledate World Wide Web in katero koli spletno stran ali drugo vsebino, ki jo vsebuje spletna stran . Splet je del informacijsko-deljenega dela interneta. Splet uporablja tudi brskalnike, kot so Google ali Chrome za dostop do spletnih dokumentov, imenovanih spletnih strani, ki so med seboj povezane prek hiperpovezav.
                    {"\n\n"}Davčni zavezanci, ki imajo status družbe z omejeno odgovornostjo in/ ali samostojnega podjetnika, prodajajo blago in storitve prek spleta lahko profilirajo z aplikacijo  elektronskega trgovanja (računa). Le ta nima omejitev prometa za prehod na elektronsko trgovanje računa. Elektronsko izdajanje računov lahko koristi tudi tistim z zelo majhnim prometom.
                    {"\n\n"}Hitra poizvedba na Google-u bo razkrila veliko možnosti. Programska orodja, kot je Microsoft Office, imajo v svoji programski opremi prilagodljivo predlogo. Predlogo lahko prenesete in najdete spletne strani, ki so na voljo v Office-u.
                    {"\n\n"}Cash board: Cash board ponuja prenosljivo predlogo, ki jo lahko odprete v programu Microsoft Word. Predloge so preproste, vendar prilagodljive.
                    {"\n\n"}Blagajna na računu - Office, Open Office in Excel Predloge
                    {"\n\n"}Microsoft - če vam predloga v Microsoft Office-u ni všeč, pojdite v galerijo Microsoftovih predlog za več možnosti.
                    {"\n\n"}Poleg Microsoft Office-a imajo Google Dokumenti predloge, ki jih lahko izberete v galeriji predlog. Apple ponuja možnosti za produktivne  aplikacije.
                    {"\n\n"}Nekatera spletna mesta vam omogočajo, da ustvarite, natisnete, shranite in pošljite račune neposredno z njihovega spletnega mesta. Nobenih drugih programov ne potrebujete, razen spletnega brskalnika.
                    {"\n\n"}Naslednji razdelek prikazuje nekaj brezplačnih aplikacij za učenje za ustvarjanje elektronskih računov.

                </Text>
            </View>
        )
    }
}
