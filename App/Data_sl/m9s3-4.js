import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S3_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Namestitev Facebook strani za podjetje</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Korak 3:</Text> Izberite “Pages/Strani” v razdelku “Explore section/Poiskati” na levi strani profila vaše
                    domače strani. Pri naboru možnosti izberite “Create Page/Ustvarite stran”.
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m9/img1.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
