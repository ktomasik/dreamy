import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements'
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz4": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Logotip",
                    "option2": "b) Blagovna znamka",
                    "option3": "c) Patent",
                    "option4": "d) Davčna identifikacija"
                },
                "question": "_______ je ime, logotip, znak ali oblika, ki edinstveno ali v kombinaciji omogoča potrošniku, da razlikuje proizvod ali storitev od drugih proizvodov ali storitev na trgu."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a)Pravilno",
                    "option2": "b)Napačno"
                },
                "question": "Ime blagovne znamke in blagovna znamka morata biti enaka."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Lokalna občina.",
                    "option2": "b) Davčni urad.",
                    "option3": "c) Stanovanjski odjemalec.",
                    "option4": "d) Zbornica."
                },
                "question": "Kateri je prvi korak za pridobitev lokalnih licenc in dovoljenja za delo doma?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Pravno ime je za vladne postopke, trgovsko ime je za odnose z javnostjo."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Blagovna znamka je pravna zaščita znamke, ki jo podeljuje Urad za intelektualno lastnino."
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prijava na davčni urad.",
                    "option2": "b) Prositi za stanovanje.",
                    "option3": "c) Obisk zbornice.",
                    "option4": "d) Priskrbeti si račun."
                },
                "question": "Kateri je prvi korak, da postanete davkoplačevalec"
            },
            "question7": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Blagovno znamko.",
                    "option2": "b) Račun.",
                    "option3": "c) Članstvo v zbornici.",
                    "option4": "d) Davčno številko."
                },
                "question": "Kaj potrebujete za uporabo sistema e-deklaracije?"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Ime podjetja se ne sme uporabljati v vladnih obrazcih in aplikacijah."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Uporabniški sporazum.",
                    "option2": "b) Politika zasebnosti.",
                    "option3": "c) Dovoljenje odjemalcev stanovanja.",
                    "option4": "d) Patentiran logotip."
                },
                "question": "Kaj ni potrebno za spletno trgovanje?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Brez registracije blagovne znamke ni možno preprečiti, da bi konkurenti ali \"posnemovalci\" uporabljali isto blagovno znamko"
            }
        }
    }
};

export default class M4Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz4;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
