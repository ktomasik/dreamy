import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Uvod</Text>
                <Text style={{marginTop: 10}}>Modul se nanaša na običajno izvedene računovodske postopke za zagon ženskega podjetništva. Natančneje, na odprtje bančnih računov, vrste bančnih računov,
                    načine bančnih transakcij in informacije o elektronskem poslovanju in spletnem plačilnem sistemu (ali preko kreditne kartice). Vsebuje tudi navodila za njihovo izvedbo. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 280, height: 340}}
                        source={require('../Images/images/6931.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
