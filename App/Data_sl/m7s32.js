import React, {Component} from 'react'
import {ScrollView, Text, Image, View, Linking, FlatList} from 'react-native'
import {Images} from '../Themes'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

// references v1. val1: link

//                    \/
export default class M7S32 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Viri in literatura</Text>
                <FlatList
                    data={[
                        {key: 'https://www.thebalancesmb.com/loan-principal-questions-and-answers-398400'},
                        {key: 'https://erpnext.org/docs/user/manual/en/non_profit/Grant%20Application'},
                        {key: 'http://evem.gov.si/info/razmisljam/drzavne-spodbude-za-zagon/'},
                        {key: 'https://podjetniskisklad.si/'},
                        {key: 'https://www.collinsdictionary.com/dictionary/english/tax-incentive'},
                        {key: 'https://data.si/blog/2016/02/12/oprostitev-placila-prispevkov-za-s-p-izkljucno-ob-prvem-vpisu-v-poslovni-register/'},
                        {key: 'https://www.banquedeluxembourgnews.com/en/bank/blnews/-/microfinance-comment-ca-fonctionne'},
                        {key: 'https://www.microfinancegateway.org/what-is-microfinance'},
                        {key: 'https://group.bnpparibas/en/news/mfi-work'},
                        {key: 'https://www.omicsonline.org/open-access/impact-of-microfinance-on-women-empowerment-an-empirical-evidence-from- andhra-pradesh--2169-026X-1000141.php?aid=56415'},
                        {key: 'https://www.worldfinance.com/banking/microfinance-empowering-female-entrepreneurs'},
                        {key: 'https://www.syndicateroom.com/learn/glossary/business-angel '},
                        {key: 'https://debitoor.com/dictionary/business-angel'},
                        {key: 'http://theconversation.com/business-angels-what-are-they-and-why-are-they-important-8794 '},
                        {key: 'https://startups.co.uk/what-are-business-angels-and-what-can-they-offer/'},
                        {key: 'www.wbusinessangels.com http://www.37angels.com/female/'},
                        {key: 'http://www.thewealthyyogi.com/blog/2016/9/25/28-top-angel-groups-venture-capital-firms-for-women-entrepreneurs'},
                        {key: 'https://ec.europa.eu/growth/smes/promoting-entrepreneurship/we-work-for/family-business_en https://www.podjetniskisklad.si/sl/pomoc-uporabnikom/pripomocki-za-pripravo-vloge/elektronski-obrazci-za-prijavo-na-razpise http://evem.gov.si/info/razmisljam/drzavne-spodbude-za-zagon/'},
                        {key: 'www.wbusinessangels.com http://www.37angels.com/female/'},
                        {key: 'https://www.ess.gov.si/obvestila/obvestilo/spodbujanje-zenskega-podjetnistva-1'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
