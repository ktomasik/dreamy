import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S6 extends Component {
    render() {
        return (
            <View style={styles.section}>

                <Text style={styles.dmlH2}>Odprtje bančnega računa</Text>
                <Text style={{marginTop: 15}}>Za odprtje računa sledite navodilom.
                    {"\n"}Razmislite o svojih možnostih.
                    {"\n"}Ko ugotovite svoje potrebe, ocenite svoje možnosti:

                </Text>
                <FlatList
                    data={[

                        {key: 'Tekoči račun'},
                        {key: 'Varčevalni račun'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5, fontWeight: 'bold'}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>
                    Izbrali ste, kateri bančni račun vam najbolj ustreza, zagotoviti si morate še, da ste upravičeni do odprtja računa. Preden se odpravite v banko, preverite, ali izpolnjujete
                    vsa merila za odprtje računa.

                    {"\n\n"}Praviloma, banke zahtevajo naslednje:
                    {"\n\n"}Veljavna identifikacija. V nekaterih državah boste morda potrebovali tudi številko socialnega zavarovanja.
                    {"\n\n"}Najmanjši znesek za odprtje računa. To se lahko razlikuje glede na banko in račun, ki ga izberete. Na primer, varčevalni račun povprečne banke zahteva minimalni depozit v
                    viši\nni 300 €.

                    {"\n\n"}Izberite banko, ki je najboljša za vas. Obrnite se na podružnico banke v vašem lokalnem okolju in se pogovorite o tem, kaj natančno ste dobili, če ste odprli osnovni račun.
                    Bank\ne so si med seboj različne, vendar jih je na splošno povzeti v dve kategoriji: banke velikih verig in manjše lokalne banke.

                    {"\n\n"}Banke velikih verig: velike banke imajo običajno podružnice v večini mest po vsej državi. Lahko se izognete taksam, ki jih boste morali plačati za uporabo storitev drugih
                    bank\n (provizija na bankomatih, itd.). Velike banke ponujajo tudi storitve, kot so 24-urne linije pomoči za svoje stranke. Poleg tega imajo te banke po navadi stabilen, zaupanja
                    vred\nen ugled.

                    {"\n\n"}Manjše lokalne banke: majhne banke ponujajo bolj osebne, prijaznejše in človeške izkušnje. Manjše banke navadno zaračunavajo manjše pristojbine za uporabo svojih storitev.
                    Manj\nše banke pogosto vlagajo svoj denar v lokalno skupnost. Po drugi strani pa manjše banke propadejo pogosteje kot velike banke (je še vedno zelo redko).

                    {"\n\n"}Poleg tega so kreditne zadruge še ena možnost za bančništvo. Kreditne zadruge so neprofitne finančne institucije, pogosto z misijo, da so “usmerjene v skupnost” in “služijo
                    ljud\nem”, ne pa dobiček. Kreditne zadruge so svoje storitve naredile bolj dostopne s partnerstvom z drugimi kreditnimi zadrugami, da bi ponudile deljeno bančno podružnico in
                    bank\nomate.

                    {"\n\n"}Obiščite svojo banko in prosite za odprtje računa. Osebno odpiranje računa je po navadi najboljša možnost za prve imetnike računov. Razrešite lahko vsa vprašanja in dvome, ki
                    jih \nimate. Postopek odpiranja računa je prav tako navadno hitrejši.

                    {"\n\n"}Preden odprete račun vprašajte za morebitna pojasnila v zvezi z vašim računom.


                    {"\n\n"}Posredujte potrebne podatke, da ustvarite račun. Če želite odpreti tekoči račun, potrebujete nekaj osnovnih osebnih podatkov. Dobro je, da imate osebno izkaznico s
                    foto\ngrafijo na njej (tudi vozniški izpit in potni list sta dovolj).

                    {"\n\n"}Dokazilo o naslovu: Pokažite račun za telefon, vozniški izpit, ali katerikoli uradni dokument z vašim imenom in naslovom.

                    {"\n\n"}Dokaz, da ste registrirani državljan: Banka bo zaprosila za številko vašega socialnega zavarovanja, davčno številko ali identifikacijsko številko delodajalca.

                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Varno hranite dokumente računa, ki jih prejmete. </Text> Ko končate s postopkom odprtja računa, prejmete dokumente, ki vsebujejo pomembne
                    podatke o vašem računu. Hranite jih na varnem mestu. Če je mogoče, si naslednje informacije zapomnite, da vam v prihodnosti ni potrebno uporabiti dokumentov:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Štirimestna PIN številka',
                                val2: 'To potrebujete pri nakupih z debetno kartico.'
                            }
                        },
                        {
                            key: {
                                val1: 'Številka bančnega računa',
                                val2: 'To potrebujete za finančne naloge, kot je vzpostavitev neposrednih depozitov.'
                            }
                        },
                        {
                            key: {
                                val1: 'Številka socialnega zavarovanja',
                                val2: 'To potrebujete za različne davčne in finančne naloge v prihodnosti.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
