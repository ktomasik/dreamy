import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Obdelava kreditne kartice na prodajnem mestu</Text>
                <Text style={{marginTop: 15}}>Sledite navodilom za obdelavo kreditne kartice na prodajnem mestu.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1. Najprej izberite pravi sistem za vase podjetje. </Text>
                <Text>Potreben je premislek pri izbiri POS Sistema. Najprej razmislite o tem, kako bi radi plačila sprejeli. Ali boste imeli večino poslovanja v trgovini ali spletnega poslovanja ali
                    kombinacijo obojega? Kakšen je vaš proračun za nakup POS strojne opreme, programske opreme, perifernih naprav, kot so čitalci črtnih kod in tiskalniki? Kakšni so vaši načrti za
                    širitev vašega podjetja v prihodnosti?
                    {"\n\n"}Odgovori na vsa ta vprašanja vam bodo pomagali privesti vas do sistema, ki je primeren za vase podjetje.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2. Izberite združljiv plačilni procesor in plačilni prehod.</Text>
                <Text>Odločiti se morate, s katerim plačilnim procesorjem ali plačilnim prehodom delate. Plačilni prehod je plačilna rešitev tretje osebe, ki omogoča povezavo z izbrani plačilnim
                    procesorjem, če prehod deluje s tem procesorjem.</Text>


                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3. Prepričajte se, da vaša rešitev vključuje potrebne varnostne funkcije, da ohranja transakcije
                    varne in, da zaščitijo vaše stranke in vase podjetje pred kršitvijo.</Text>
                <Text>Kršenje varnosti podatkov ni le slabo za podjetje, lahko je uničujoče. Torej, v vašem interesu je, da storite vse, kar lahko, da preprečite kršitev.
                    {"\n\n"}Varnost plačil zahteva pristop, ki vključuje več tehnoloških rešitev in najboljše prakse. To najbolje dosežemo v partnerstvu s ponudnikom obdelave plačil.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>4. Prednostno določite funkcij z dodano vrednostjo v svoji plačilni rešitvi.</Text>
                <Text>Vaše podjetje je edinstveno in kot tako bo imelo edinstvene potrebe, ko gre za obdelavo plačil.
                    {"\n\n"}D bi kar najbolje izkoristili vašo POS rešitev, je vredno razmišljati o značilnostih, ki jih želite in oceni razpoložljivost in stroškov, ki jih ponujajo različni
                    ponudniki.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>5. Ne pozabite vprašati za pomoč pri namestitvi in usposabljanje osebja.</Text>
                <Text>Najboljša plačilna rešitev na svetu ponuja majhno vrednost vašemu podjetju, če jo je težko namestiti ali se naučiti, kako jo uporabljati. Vprašajte za predstavitev izdelka, da
                    boste lahko rešitev preizkusili v realnem času. Bodite prepričani, da vprašate o vzpostavitvi in integraciji s svojimi obstoječimi sistemi. Preberite o goljufiji in, ali so
                    potrebne posodobitve samodejno izvršene ali jih morate izvajati sami. Preverite, kako boste izvajali različne vrste plačil.</Text>
            </View>
        )
    }
}
