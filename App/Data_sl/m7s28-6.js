import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28_6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Turčijo:</Text>

                <Text style={styles.dmlH3}>6. korak: </Text>
                <Text>Ženska podjetnica predstavi svoj poslovni načrt KOSGEB (priprava izobraževalnega programa za poslovanje je podana med usposabljanjem KOSGEB).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
