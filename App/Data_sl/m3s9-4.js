import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_4 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Upoštevajte spodnja navodila za prodajo izdelkov v Instagramu</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>4. Korak: Napišite besedilo v zvezi z vašim izdelkom, pojasnilo in ceno, navedite svoj zasebni e-poštni naslov v primeru komunikacije s
                    stranko</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522778-PIXKA7-878.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
