import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S5 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Nekaj sredstev za ženske ustanovitelje</Text>
                <Text>500 Women:   brezhibno financiranje ustanoviteljev
                    {"\n\n"}Astia:  omrežje, ki omogoča dostop do kapitala in usposabljanje / podporo podjetnicam
                    {"\n\n"}BBG (Built By Girls) Ventures: vlaga v potrošniška internetna zagonska podjetja z vsaj eno žensko ustanoviteljico
                    {"\n\n"}Chloe Capital: za podjetja v začetni fazi tveganega kapitala, osredotočena ne podjetja, ki jih vodijo ženske
                    {"\n\n"}Female Founders Fund: vlaga v novoustanovljena podjetja na področju e-poslovanja, platform in spletnih storitev
                    {"\n\n"}Golden Seeds: investicijska mreža angelov in sklada, ki vlaga v podjetnice
                    {"\n\n"}Intel Capital Diversity Fund: sklad, ki vlaga v zagonska podjetja , ki jih vodijo ženske in manjšine
                    {"\n\n"}Mergelane: pospeševalec in investitor zagonskih podjetij, ki jih vodijo ženske (Boulder, CO)
                    {"\n\n"}Next Wave Impact: sklad za zgodnje tvegane naložbe na osnovi inovativnega aktivnega učenja
                    {"\n\n"}Pipeline Fellowship: ženske vlagateljice, ki vlagajo v socialna podjetja, ki jih vodijo ženske
                    {"\n\n"}Valor Ventures: sklad, iz Atlante, ki financira ženske ustanoviteljice
                    {"\n\n"}Women’s Venture Fund: podpora podjetnicam s tečaji, svetovanji, krediti in več
                </Text>
            </View>
        )
    }
}
