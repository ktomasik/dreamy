import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S31 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sledite naslednjim korakom za povezavo Instagrama z ostalimi socialnimi omrežji</Text>
                <Text style={{marginTop: 10}}>V naslednjih poglavjih bomo pokazali:</Text>
                <FlatList
                    data={[
                        {key: 'kako objaviti vsebino'},
                        {key: 'kako deliti vsebino na ostala socialna omrežja'},
                        {key: 'kako dodajati vsebine v primerne teme.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 3, marginLeft: 15}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Korak 1: Za objavo nove vsebine pritisnite ikono s fotoaparatom na dnu ekrana.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img59.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
