import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S26 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Mikrofinančni in drugi zagonski skladi za ženske</Text>
                <Text style={styles.dmlH2}>Za Poljsko:</Text>
                <Text style={styles.dmlH3}>STARTUP ACADEMY</Text>
                <Text>Usposabljanje, mentorstvo, inovativne zagonske metode, programi pospeševanja.</Text>

                <Text style={styles.dmlH3}>TWÓJ STARTUP</Text>
                <Text>Predinkubacija, pravno in računovodsko svetovanje, IT in trženjsko svetovanje, usposabljanje.</Text>

                <Text style={styles.dmlH3}>Inkubator Technologiczny Podkarpckiego Parku Naukowo-Technologicznego</Text>
                <Text>Pisarniške prostore, svetovalne storitve, razvojna podpora.</Text>

                <Text style={styles.dmlH3}>Przedsiębiorcze kobiety 2.0</Text>
                <Text>Projekt je naslovljen na nezaposlene ženske, ki jim pomagajo pri ustanavljanju lastnega podjetja.</Text>

                <Text style={styles.dmlH3}>AIP</Text>
                <Text>Poslovno svetovanje, mentorstvo in coaching, računovodske storitve, pravna podpora, začetni trening.</Text>
            </View>
        )
    }
}
