import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je logotip blagovne znamke?</Text>
                <Text>Logotip je grafična podoba, emblem ali simbol, ki se uporablja za pomoč in spodbujanje javne prepoznavnosti. Lahko je abstraktna ali figurativna oblika ali vključuje del besedila
                    v imenu podjetja, ki ga predstavlja.</Text>
                <Text>Blagovna znamka je vsaka interakcija in tržna praksa imena ali oblikovne podobe, ki identificira in razlikuje eno podjetje, izdelek ali storitev od druge.</Text>

                <Text style={styles.dmlH2}>Kako izbrati pravo zasnovo logotipa?</Text>
                <Text>Pri odločitvi za oblikovanje logotipa morate imeti v mislih nekaj stvari, ki vam bodo pomagale pripisati identiteto blagovne znamke.</Text>
                <Text>Če želite imeti ustvarjalni logotip za vaše podjetje, poskusite najprej navdahniti z logotipom, tako da si ogledate koncepte uspešnih blagovnih znamk.</Text>

                <Text style={styles.dmlH2}>Strategija cen za elektronsko poslovanje (nastavitev cene za vaš izdelek)</Text>
                <Text>V spletnem nakupovalnem okolju je najbolje, da uporabnik vidi ceno skupaj z izdelkom. Strankam bo lažje kupiti izdelek, kadar izdelek vidi na premišljeno dodelan način skupaj s
                    svojimi stroški.</Text>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img16.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={styles.dmlH2}>Različne vrste strategij oblikovanja cen</Text>
                <Text style={styles.dmlH3}>Stroškovno oblikovanje cene</Text>
                <Text>Stroškovno oblikovanje cen je lahko najbolj priljubljen model oblikovanja cene izdelka v maloprodajni industriji, ki zagotavlja najnižjo donosnost za vsak prodan izdelek.</Text>
                <Text style={styles.dmlH3}>Konkurenčne cene za e-trgovino</Text>
                <Text>Pri strategiji oblikovanja cene, ki temelji na tekmecih, morate spremljati, kaj vaši neposredni konkurenti zaračunavajo za določen izdelek in določiti svojo ceno glede na njihove
                    cene.</Text>
                <Text style={styles.dmlH3}>Vrednotenje dodane vrednosti izdelka e-trgovine</Text>
                <Text>Pri tem pristopi lahko določite ceno izdelka glede na potrebe vsakega kupca posebej. Pri tem procesu morate dobro analizirati kupca in ceno izdelka prilagoditi vrednosti, ki jo
                    ima izdelek v očeh kupca in ugotoviti, koliko je kupec še pripravljen plačati zanj. Pogosto je tak pristop zelo učinkovit in zagotavlja večjo donosnost (Roggio, 2017).</Text>
            </View>
        )
    }
}
