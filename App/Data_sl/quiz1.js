import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz1": {
            "question1": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) uporabniškega imena.",
                    "option2": "b) gesla.",
                    "option3": "c) kontaktnih informacij.",
                    "option4": "d) naslova stalnega prebivališča."
                },
                "question": "Za odprtje e-mail računa, ne potrebujem"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "E-mail spam in e-mail virus pomenita isto."
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Morda obstajajo razlike pri nastavitvi e-mail računa na različnih blagovnih znamkah pametnih telefonov."
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Svetovni splet",
                    "option2": "b) Internet",
                    "option3": "c) E-mail račun",
                    "option4": "d) Spam mail"
                },
                "question": "________ je narejen iz milijonov med seboj povezanih spletnih strani."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Za uporabo e-maila, je potreben račun."
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Bančni račun.",
                    "option2": "b) E-mail račun.",
                    "option3": "c) Naslov stalnega prebivališča.",
                    "option4": "d) Telefonsko številko."
                },
                "question": "Kaj potrebujemo, če želimo poslati e-mail preko interneta?"
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Ime družabnih medijev.",
                    "option2": "b) E-mail račun.",
                    "option3": "c) Programski sistem za iskanje informacij na internetu.",
                    "option4": "d) Spletna stran."
                },
                "question": "Kaj pomeni iskalnik?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Uporabniški e-mail račun se lahko uporabi tudi za račun na pametnem telefonu."
            },
            "question9": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Za iskanje po internetu.",
                    "option2": "b) Za čiščenje virusa.",
                    "option3": "c) Za prenos podatkov na internet.",
                    "option4": "d) Če želite ločiti ime uporabnika od uporabnikovega internetnega naslova."
                },
                "question": "Za kaj se uporablja @?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Iskalnik.",
                    "option2": "b) E-mail naslov.",
                    "option3": "c) Nekakšen virus.",
                    "option4": "d) Spam mail."
                },
                "question": "Kaj je Google?"
            }
        }
    }
};

export default class M1Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz1;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
        /*console.log('countCheck: ' + this.state.countCheck);
        console.log('punkty: ' + this.score)*/
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
