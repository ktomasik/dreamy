import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računa za e-naslov na mobilnih telefonih sistema Android (Samsung, Sony, HTC ..)</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Čestitamo, ustvarili ste svoj e-mail račun!</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Lahko se prijavite za e-mail račun, ki ste ga ustvarili.</Text>
                <Text style={{marginTop:10}}>Naslednji modul vam bo pojasnil, kako namestiti vaše račune na socialnih omrežjih.</Text>
            </View>
        )
    }
}
