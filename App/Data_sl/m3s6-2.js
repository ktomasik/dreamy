import React, {Component} from 'react'
import {Image, Text, TouchableHighlight, View, Modal} from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen3 = [{
    url: '',
    props: {
        source: require('../Images/m3/img25.png')
    }
}];

export default class M3S6_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kako zajeti fotografije izdelka in jih naložiti v spletu</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen3}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>

                <Text style={{marginTop: 10, fontSize: 18}}>10. Korak: izpolnite tabelo:</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img25.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
