import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S19 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Davčne spodbude za zagonska podjetja žensk</Text>
                <Text style={styles.dmlH2}>Za Grčijo:</Text>
                <Text>V Grčiji so spodbude namenjene krepitvi inovacij. V skladu z novim zakonom o državnem pravu so podjetja, ki proizvajajo izdelke ali zagotavljajo storitve mednarodnega
                priznavanja registracije patenta, izvzeta plačila od davka na dohodek za tri leta.
                {"\n\n"}Zlasti je mogoče predvideti, da so dobički podjetja, ki izhajajo iz prodaje izdelkov, za proizvodnjo katerih je mednarodno priznan patent v imenu podjetja, tri zaporedna leta
                oproščeni davka od dohodka od uporabe, v kateri so bili prvi prihodki od prodaje izdelkov, ki uporabljajo patent.
                {"\n\n"}Oprostitev se odobri tudi, kadar se izdelki proizvajajo s strani tretjih oseb. Oprostitev velja tudi za dobičke, ki izhajajo iz opravljanja storitev, kadar gre za patent, ki je
                    tudi mednarodno priznan.</Text>
            </View>
        )
    }
}
