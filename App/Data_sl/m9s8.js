import React, {Component} from 'react'
import {Text, View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M9S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Viri in literatura</Text>
                <FlatList
                    data={[
                        {key: 'https://digitalyogaacademy.com/create-facebook-yoga-business-page-7-simple-steps/'},
                        {key: 'https://blog.hootsuite.com/steps-to-create-a-facebook-business-page/ '},
                        {key: 'https://www.quora.com/What-is-e-commerce-and-online-shopping'},
                        {key: 'https://www.techopedia.com/definition/26363/online-marketing'},
                        {key: 'https://digitalyogaacademy.com/create-facebook-yoga-business-page-7-simple-steps/'},
                        {key: 'https://blog.hootsuite.com/steps-to-create-a-facebook-business-page/'},
                        {key: 'https://pagevamp.zendesk.com/hc/en-us/articles/202902669-What-is-a-Facebook-short-description'},
                        {key: 'https://blog.hootsuite.com/steps-to-create-a-facebook-business-page/'},
                        {key: 'https://www.instagram.com/'},
                        {key: 'https://power-marketing.com/maximize-facebook-business-page-part-two-call-action/'},
                        {key: 'https://www.kitzandco.com/blog/why-your-biz-needs-an-instagram-business-account'},
                        {key: 'https://www.socialmediaexaminer.com/instagram-business-profiles-how-to-set-up-and-analyze-your-activities/'},
                        {key: 'https://welfordmedia.co.uk/blog/seo-marketing/switch-to-instagram-business-account'},
                        {key: 'http://onlinepresence.coach/marketing/social-media-marketing/'},
                        {key: 'http://wbninc.com/create-a-twitter-business-page'},
                        {key: 'https://help.twitter.com/en/managing-your-account/how-to-customize-your-profile'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
