import React, {Component} from 'react'
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M6S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Izdelovanje predlog pogodb za spletno nakupovanje med kupcem in dobaviteljem.</Text>
                <Text style={{marginTop: 15}}>Ker je internetno nakupovanje drugačno od tradicionalnega nakupovanja, zlasti za transakcije, opravljene prek interneta, velja Uredba o oddaljenih
                    pogodbah. Ta pogodba je sklenjena med trgovcem in kupcem, ki nista skupaj, o kateri se pogaja in se strinja z enim ali več organiziranimi sredstvi za komunikacijo na daljavo - na
                    primer po telefonu, pošti ali prek interneta.</Text>
                <View style={styles2.border}>
                    <Text>Splošna načela pri oblikovanju pogodbenega razmerja glede nakupov na spletu med prodajalcem in kupcem se lahko v večinskem delu uporabijo po naslednjih splošnih
                        ukrepih:</Text>
                    <FlatList
                        data={[
                            {key: 'vabilo k kupčiji: prikazovanje blaga za prodajo na spletnem mestu e-trgovine se običajno obravnava kot povabilo k kupčiji in ne na ponudbo, ki je v zameno podobna tradicionalni fizični trgovini. Ponudbo namesto tega izpolnjuje kupec ki obiščejo spletno stran s svojimi dejanji, namesto prodajalca;'},
                            {key: 'stranka sporoči svojo ponudbo: ker je oglaševanje blaga ali storitev na spletni strani ni ponudba, je spodbuda dati ponudbo na kupčevi strani. Kupec bo posredoval ponudbo elektronsko - kar se lahko opravi na spletni strani - ponudba nakupa oglaševanega izdelka;'},
                            {key: 'prodajalec sprejme ponudbo: ko enkrat kupec poda ponudbo, je nato na prodajalcu da ponudbo nedvoumno in brezpogojno sprejme, tako da sporoči svojo privolitev kupcu.'},
                        ]}
                        renderItem={({item}) => <Text
                            style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                    />
                    <Text style={{marginTop: 15}}>Ob sprejemu prodajalca ponudbe stranke se je vzpostavilo pogodbeno razmerje.</Text>
                    <Text><Text style={{fontWeight: 'bold'}}>Vir. </Text><Text style={{color: 'blue'}}
                                                                               onPress={() => Linking.openURL('http://www.findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f.aspx')}>findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f</Text></Text>
                </View>
                <Text style={{marginTop: 15}}>Pred sklenitvijo pogodbe na internetu med potrošnikom in prodajalcem mora ponudnik storitve ali blaga o zadevah v uredbi obvestiti potrošnika. Nekatere
                    informacije, ki jih mora prodajalec dati potrošniku vezne na področje uporabe uredbe pred sklenitvijo pogodbe, so naslednje:</Text>
                <FlatList
                    data={[

                        {key: 'ime, naziv, naslov, telefonska številka in druge informacije za dostop do prodajalca ali ponudnika,'},
                        {key: 'osnovne značilnosti blaga ali storitve, ki je predmet pogodbe,'},
                        {key: 'prodajna cena blaga ali storitve, vključno z vsemi davki,'},
                        {key: 'če obstajajo stroški dostave,'},
                        {key: 'informacije o plačilu in dostavi ali izvedbi;'},
                        {key: 'pogoje za uveljavljanje pravice do odstopa.'},

                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Preden se nakup na spletu opravi mora potrošnik potrditi te predhodne informacije v nasprotnem primeru pogodbeno razmerje ni dokazano.</Text>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
