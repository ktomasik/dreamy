import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S32 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sledite naslednjim korakom za povezavo Instagrama z ostalimi socialnimi omrežji</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 2: Sliko lahko izberemo iz treh virov: galerije, fotografije ali video posnetki. Da izberemo sliko iz galerije kliknite “GALERIJA”
                    (GALLERY), nato izberete fotografijo in kliknite NAPREJ (NEXT).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img60.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img61.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img62.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
