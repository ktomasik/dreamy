import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>1.6. Registracija strokovnih in poklicnih združenj</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>TURČIJA</Text>
                <FlatList
                    data={[
                        {key: 'KOSGEB (organizacija za razvoj malih in srednje velikih podjetij)'},
                        {key: 'Generalni direktorat za status žensk (KSGM)'},
                        {key: 'Turško poslovno združenje (İŞKUR)'},
                        {key: 'Turška unija zbornic in izmenjav skupnosti ( TOBB)'},
                        {key: 'Podpora ministrstva za družinsko in socialno politiko'},
                        {key: 'Turški Grameen  mikrofinančni program'},
                        {key: 'Banni rediti za ženske'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>SLOVENIJA</Text>
                <Text style={{fontWeight: 'bold'}}>Nefinančna državna podpora</Text>
                <FlatList
                    data={[
                        {key: 'VEM točke'},
                        {key: 'Poslovni inkubatorji'},
                        {key: 'Univerzitetni inkubatorji'},
                        {key: 'Tehnološki parki'},
                        {key: 'Initiative start up Slovenia'},
                        {key: 'Evropska podjetniška mreža'},
                        {key: 'SPIRIT Slovenija'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Finančna državna pomoč</Text>
                <FlatList
                    data={[
                        {key: 'Slovenski podjetniški sklad'},
                        {key: 'Zavod Republike Slovenije za zaposlovanje'},
                        {key: 'Slovenski sklad za regionalni razvoj'},
                        {key: 'Bančna posojila'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Druge oblike podpor</Text>
                <FlatList
                    data={[
                        {key: 'Gospodarska zbornica Slovenije'},
                        {key: 'Obrtno-podjetniška  zbornica Slovenije'},
                        {key: 'Poslovni angeli Slovenije'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>POLJSKA</Text>
                <FlatList
                    data={[
                        {key: 'Skladi Evropske unije za podjetnice (NGOs, Public Bodies,..etc)'},
                        {key: 'Evropski socialni skladi (PO WER-Operational Program Knowledge Education Development)'},
                        {key: 'Viri, ki niso iz EU (vladni, zasebni, itd.)'},
                        {key: 'Poljska agencija za razvoj podjetništva'},
                        {key: 'Sklad za posojila ženskam'},
                        {key: 'Poslovni angeli'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>GRČIJA</Text>
                <FlatList
                    data={[
                        {key: 'Semenski kapital (majhen kapital, namenjen določenim skupinam prebivalstva-brezposelnim, mladim)'},
                        {key: 'OAED Program (za javni sektor)'},
                        {key: 'The Open Fund (za zaseben sektor)'},
                        {key: 'Bančna posojila'},
                        {key: 'Partnerski sporazum (PA) 2014-2020 (ESPA)'},
                        {key: 'Grška skupnost v tujini'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>FRANCIJA</Text>
                <FlatList
                    data={[
                        {key: 'CFE'},
                        {key: 'Register trgovin in podjetij (RFS)'},
                        {key: 'ACCRE-pomaga iskalcem zaposlitve in olajša oblikovanje njihovih podjetij.'},
                        {key: 'BPI France (Javna investicijska banka)'},
                        {key: 'PRI (Regionalna partnerstva za inovacije)'},
                        {key: 'Poslovni angeli'},
                        {key: 'Entreprendre au Feminin'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
