import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Informacije o pogodbi</Text>
                <Text style={{marginTop: 15}}>Komisija EU je decembra 2015 predlagala direktivo o pogodbah za spletno prodajo blaga in prodajo blaga na daljavo (direktiva o spletni prodaji blaga).
                    Predlagana direktiva o spletni prodaji blaga zagotovlja največjo uskladitev in s tem državam članicam prepoveduje uvedbo višje ravni varstva potrošnikov na področju uporabe
                    direktive . Kot navedeno zgoraj, mora najprej trgovec pred nakupom izdelka najprej navesti jasne, točne in razumljive informacije o blagu ali storitvi, če je blago ali storitev
                    kupljena v EU.
                    {"\n\n"}V tem kontekstu, skupaj s predlaganimi novimi dogovori, bi morala značilna spletna nakupna pogodba vključevati naslednje elemente :
                </Text>
                <FlatList
                    data={[
                        {key: 'identiteto, naslov, e-pošto in telefonsko številko trgovca'},
                        {key: 'strokovni naziv in podatki trgovca o DDV (če je relevantno)'},
                        {key: 'številka trgovskega registra trgovca'},
                        {key: 'glavne značilnosti blaga'},
                        {key: 'celotno ceno, ki vključuje davke in vse stroške'},
                        {key: 'stroške dostave, (če pride v poštev) - in vse druge dodatne stroške'},
                        {key: 'načine plačila, dostave ali izvedbe'},
                        {key: 'trajanje pogodbe (če je relevantno)'},
                        {key: 'kakršnekoli omejitve dobave v posameznih državah'},
                        {key: 'pravico do preklica naročila v 14 dneh'},
                        {key: 'razpoložljive po-prodajne storitve'},
                        {key: 'mehanizme za reševanje sporov'},

                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Prijava za zavarovanje za prodajo spletnih izdelkov
                    {"\n\n"}Odgovorni ste za prodane izdelke, ker prodajate izdelke, ki ste jih izdelali pod imenom vašega podjetja. Na primer, ste spletni trgovec, ki prodaja ekstravagantne torte po naročilu in jih dostavlja na vrata strank. Če vas nekdo krivi za zastrupitev s hrano kmalu po tem, ko pojedo eno izmed vaših tort, bi bili odgovorni plačati odškodninski zahtevek. Poleg tega se lahko izdelek poškoduje med prevozom tovora ali pa ga ukradejo. Na tej stopnji bi zavarovanje lahko pokrilo stroške zamenjave, ki bi temeljilo na ceni stroška. V zvezi s tem bo stik z zavarovalnim zastopnikom pravilen pristop, ki vam bo pomagal razumeti, kakšno vrsto zavarovanja res potrebujete, kot so odgovornost za izdelke in komercialna odgovornost.
                    {"\n\n"}Po drugi strani, kot je razvidno iz naslednjih korakov, lahko postane zavarovanje za nakup zdaj zelo enostavno in časovno varčen proces. V najkrajšem možnem času se pripravijo samo najbolj primerne ponudbe z vašimi informacijami. Po predhodnem pregledu boste prejeli predlog. Zato lahko kupite zavarovanje, za katerega menite, da je najprimernejši.
                </Text>
            </View>
        )
    }
}
