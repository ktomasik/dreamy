import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz5": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Tekoči račun ne omogoča dostopa do vašega denarja za vaše dnevne potrebe po transakciji"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Samodejna plačila računov so neredna plačila prodajalcem iz bančnega, posredniškega ali vzajemnega sklada"
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Plačevanje na spletu z razkrivanjem lastnih podatkov o kreditni kartici.",
                    "option2": "b) Plačevanje z gotovino.",
                    "option3": "c) Plačevanje na bančni račun.",
                    "option4": "d) Plačevanje računa."
                },
                "question": "Kateri način ni način plačevanja preko spleta?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Spletno bančništvo se nanaša na vse bančne transakcije, ki se lahko izvajajo preko interneta."
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Izbira najprimernejše banke.",
                    "option2": "b) Obisk banke in prošnja za odprtje računa.",
                    "option3": "c) Osebne podatke hranimo skrite pred banko.",
                    "option4": "d) Dokumente računa varno hranimo."
                },
                "question": "Katero navodilo za odprtje bančnega računa je napačno?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Debetne kartice z nakupom takoj vzamejo denar, neposredno iz vašega tekočega računa."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Pridobitev identifikacijske številko podjetja.",
                    "option2": "b) Nastavitev prejemanja plačil prek tretje osebe.",
                    "option3": "c) Spoznati nacionalne poslovne zahteve.",
                    "option4": "d) Odprtje poslovnega bančnega računa."
                },
                "question": "Po ustanovitvi/registraciji podjetja morate za prejemanje plačil opraviti še spodaj navedene korake. Kateri ni v pravilnem vrstnem redu?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Izdajatelj kreditne kartice pošlje bančni odrezek enkrat na mesec."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) ustvariti enostaven obrazec za naročilo na Facebooku.",
                    "option2": "b) konfigurirati svoja obvestila o naročilu.",
                    "option3": "c) konfigurirati potrdila o naročilu.",
                    "option4": "d) konfigurirati nastavitve plačil."
                },
                "question": "Katera od naslednjih trditev je napačna? Da bi prejeli naročila, morate:"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) računa trgovca.",
                    "option2": "b) virtualnega terminala.",
                    "option3": "c) gotovine.",
                    "option4": "d) prehoda."
                },
                "question": "Za prejem plačil s kreditno kartico ne potrebujete?"
            }
        }
    }
};

export default class M5Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz5;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
