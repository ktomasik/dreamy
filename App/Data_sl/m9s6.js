import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S6 extends Component {

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Twitter za podjetje</Text>
                <Text styl={{marginTop: 10}}>Je eden od najbolj razširjenih socialnih omrežij ; svoje stranke lahko nagovorite direktno preko Twitterja. Sledilci na Twitterju so lahko vaše potencialne stranke. Morali bi imeti  svoje lastne sledilce, da bi lahko ocenili prednosti Twitterja kot trženega orodja.  Lahko si ustvarite svoj lasten seznam strank ali pa se priključite že oblikovani skupini.
                    {"\n\n"}Če želite odpreti poslovni račun v Twitterju, morate že imeti Twitter račun. Kot je opisano v Modulu 2, pojdite v brskalnik Google; napišite “twitter” in poiščite;  Do spletnega mesta Twitter dostopate tako, da izberete “mobile.twitter.com”, kliknite “Sign up / Prijava” v twitter ( za podrobnosti poglejte v Modul 2)
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/twitter.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
