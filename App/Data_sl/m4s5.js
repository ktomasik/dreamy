import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kaj je davčna številka?</Text>
                <Text>Identifikacijska številka davčnega zavezanca je identifikacijska številka, ki se uporablja za davčne namene v državi (Anonymous 2018f).
                    {"\n"}Davčno številko morajo pridobiti vse pravne osebe, nekorporativne osebe in posamezniki.
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>TURČIJA</Text>
                <Text>Obstaja veljaven sistem: e-Declaration system. Pravzaprav se zdaj kot identifikacijska številka osebe uporablja davčna identifikacijska številka. ID mora biti overjene ali
                    odobrene s strani uradnikov davčnega urada. Davčno številko pridobijo (National Report Turkey, 2018).</Text>
                <FlatList
                    data={[
                        {key: 'Za namene opravljanja poklicnih ali poslovnih aktivnosti v Turčiji.'},
                        {key: 'S 1. Julijem 2006 se nacionalna identifikacijska številka (TIN) uporablja kot enotna identifikacijska številka za prebivalce Turčije. Vse TIN številke so bile usklajene z njihovo nacionalno identifikacijsko številko iz davčnega sistema.'},
                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>SLOVENIJA</Text>
                <Text>Ob vnosu zahtevanih podatkov v davčni register.
                    {"\n"}Zahtevajo se lahko dodatne numerične ali črkovne oznake.
                    {"\n"}Če se posameznik v davčnem registru prijavi kot samostojni podjetnik, finančna uprava ne dodeli nove davčne številke (National Report Slovenia, 2018).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>For POLAND</Text>
                <Text>Vsaka odrasla oseba moa imeti davčno številko.
                    {"\n"}V primeru, da se prijavite kot samostojni podjetnik, vam ne dodelijo nove davčne številke temveč uporabljate svojo davčno številko (National Report Poland, 2018).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>GRČIJA</Text>
                <Text>Vsaka odrasla oseba mora imeti davčno številko.
                    {"\n"}V primeru, da želi odrasla oseba postati samostojni podjetnik, mora podjetje registrirati v davčnem uradu.
                    {"\n"}V poslovne namene lahko uporablja osebno davčno števiko (National Report Greece, 2018).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>FRANCIJA</Text>
                <Text>Francoski davčni organi izdajo davčno številko vsem fizičnim osebam.
                    {"\n"}TIN se dodeli, ko se oseba vpiše v bazo davčnih organov.
                    {"\n"}Dodeljena je vsem osebam, registriranim v sistem Generalnega direktorata za javne finance. Je edinstvena, zanesljiva in trajna identifikacijska številka.
                    {"\n"}Davčna številka je navedena na vnaprej natisnjenem obrazcu za obračun davka na dohodek davka na nepremičnino.
                    {"\n"}TIN mora pridobiti imetnik pogodbe, imetnik sredstev ali upravičenec dohodka (National Report France, 2018).
                </Text>
            </View>
        )
    }
}
