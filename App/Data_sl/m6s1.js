import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Uvod</Text>
                <Text style={{marginTop: 10}}>Ciljna skupina Dreamy m- Learning projekta so ženske z nizko pridobljeno izobrazbo, ki izdelujejo ročne izdelke doma. Cilj jim je pomagati pri
                    pridobivanju mobilnih, digitalnih in podjetniških sposobnosti za učinkovito prodajo njihovih izdelkov na digitalnih trgih z uporabo pametnih telefonov. V skladu s tem ciljem smo
                    razvili mobilno aplikacijo, ki je lahko razumljiva in enostavna za uporabo; za operacijske sisteme Android in iOS in spletni vmesnik za premostitev mobilnih aplikacij na portal
                    m-učenje. V programu pripravljenega usposabljanja, pojasnjujemo ženskam kako korak za korakom postopamo z brezplačnimi in pogosto uporabljenimi mobilnimi aplikacijami.
                    {"\n\n"}V skladu s tem okvirom je cilj tega modula razložiti izhodišča, kot so ustvarjanje računov, sklepanje pogodb med kupcem in dobaviteljem za nakupovanje na internetu,
                    vlaganje zahtevkov za zavarovanje za prodajo spletnih izdelkov in ustvarjanje elektronskih podpisov za ženske, ki želijo prodati svoje ročne izdelke na internetu.
                </Text>
            </View>
        )
    }
}
