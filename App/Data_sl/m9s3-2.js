import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računov za socialna omrežja za podjetje </Text>
                <Text style={styles.dmlH3}>Namestitev  Facebook strani za podjetje</Text>
                <Text>Če želite prodajati preko Facebook-a, morate imeti stran na Facebook-u. (Sledite temi v Modulu 2)
                    {"\n\n"}Na vaši Facebook strani, bodo kupci videli povezavo do vaše trgovine in prek nje dostopali do vaših izdelkov.

                </Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Korak 1:</Text> log into
                    your Facebook profile
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
