import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S15_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Naredi Facebook račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 15: Sedaj imate osnovne nastavitve Facebook računa nastavljene in bi morali videti zaslon z vašim profilom.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img26.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
