import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Spletne strani:</Text>
                <Text>Spletna stran je dokument, ki ustreza svetovnemu spletu in spletnim brskalnikom.</Text>
                <Text style={styles.dmlH2}>Spletna stran:</Text>
                <Text>Spletna stran je zbirka povezanih spletnih strani, vključno z večpredstavnostjo, identificirana z uporabniškim imenom (domeno), in objavljena na vsaj enem od spletnih
                    strežnikov. </Text>
                <Text style={styles.dmlH2}>Spletni strežnik: </Text>
                <Text>Spletni strežnik je sistem, ki končnim uporabnikom prenaša vsebino ali storitve preko spleta.</Text>
                <Text style={styles.dmlH2}>Iskalnik:</Text>
                <Text>Sistemski spletni iskalnik je programski sistem, ki je zasnovan za iskanje informacij v svetovnem spletu . </Text>
                <Text style={styles.dmlH2}>Iskanje po spletu: </Text>
                <Text>Internetno iskanje je postopek raziskovanja interneta za informacije s pomočjo brskalnikov kot sta Google ali Internet Explorer. </Text>
                <Text style={styles.dmlH2}>Pomen e-maila: </Text>
                <Text>E-mail je sistem elektronskega pošiljanja pisanih sporočil iz enega računalnika na drugega .</Text>
                <Text style={styles.dmlH2}>Kaj je Email naslov? </Text>
                <Text>Email naslov je ime elektronskega poštnega predala, ki lahko sprejema (in pošilja) elektronska sporočila preko spleta. </Text>
                <Text style={styles.dmlH2}>Kaj pomeni symbol @? </Text>
                <Text>Je simbol v vsakem e-naslovu, ki loči ime uporabnika od uporabniškega spletnega naslova.</Text>
                <Text style={styles.dmlH2}>Kaj je Email virus?</Text>
                <Text>Email virus je virus, ki je poslan z ali pripet k elektronskim sporočilom. Medtem ko različni tipi virusov delujejo na različne načine, obstaja tudi vrsta metod za zaščito pred
                    virusi ali napadi na računalnik.</Text>
                <Text style={styles.dmlH2}>Kaj je email spam?</Text>
                <Text>Spam se nanaša na nenaročeno množično e-sporočilo (nezaželena pošta). To je ponavadi pošta z ali brez pomena.</Text>
            </View>
        )
    }
}
