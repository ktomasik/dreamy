import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financiranje v državah partnericah projektov - korak po korak</Text>
                <Text style={styles.dmlH2}>Za Francijo:</Text>
                <Text style={styles.dmlH3}>Če niste iskalec zaposlitve - POMOČ DRŽAVE - ACCRE</Text>

                <Text style={styles.dmlH3}>3. korak:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'V primeru ugodnega odgovora vam URSSAF izda potrdilo o sprejemu. V nasprotnem primeru navede razloge in obvesti svojo odločitev o zavrnitvi. Če v roku enega meseca ni odgovora, se ACCRE šteje za odobreno.',
                                val2: 'https://www.legalstart.fr/fiches-pratiques/aides-creation-entreprise/aide-creation-entreprise-femmes/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
