import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {Button} from 'react-native-elements';
import {quizStyles} from '../Containers/Styles/general';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz3": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno",
                },
                "question": "E-trgovina je začela vstopati v naše življenje že od petdesetih let prejšnjega stoletja, ko so se začeli uporabljati računalniki."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno",
                },
                "question": "Računi podjetja Instagram ne zagotavljajo več informacij o vaši blagovni znamki, kot tudi ne analitičnega orodja za merjenje vašega uspeha."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Etsy ne zaračuna za ustvarjanje trgovine.",
                    "option2": "b) Bonanza vam omogoča brezplačen seznam predmetov.",
                    "option3": "c) V Zibbetu lahko prodajate električna vozila.",
                    "option4": "d) Za odprtje trgovine v podjetju DaWanda ni nobene pristojbine."
                },
                "question": "Katera od naslednjih trditev je napačna?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno",
                },
                "question": "E-trgovina je nakup in prodaja blaga in storitev v spletni platformi."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Človeški um si običajno izgradi vizualno podobo abstraktnih pojmov.",
                    "option2": "b) Kakovostne slike izdelkov so ključni gonilnik v trgovini.",
                    "option3": "c) Študije o sledenju očesa kažejo, da se obiskovalci trgovin najprej ukvarjajo z vizualnimi elementi.",
                    "option4": "d) Povezava med logotipom in blagovno znamko ne sme biti razvidna."
                },
                "question": "Katera od naslednjih trditev o pomenu fotografije izdelka je napačna?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno",
                },
                "question": "Ključnik (hashtag) na Instagramu pomaga, da se izdelki pojavljajo na domači strani uporabnikov pogosteje."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Osredotočiti se na pripovedovanje zgodb, da povečate zavzetost.",
                    "option2": "b) Odprtje računa z ''modro kljukico'' (''blue tick'').",
                    "option3": "c) Odprtje lažnega računa, da pohvalite izdelek.",
                    "option4": "d) Preureditev izvorne slike z brisanjem pomanjkljivosti.",
                },
                "question": "Kaj je eden od načinov uspešne prodaje na Instagramu?"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Pri spletni prodaji obrtnih izdelkov uporaba realne podobe ni tako pomembna kot cena izdelka."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Pravilno",
                    "option2": "b) Napačno"
                },
                "question": "Vaš logotip mora predstavljati vaš izdelek in vašo poslovno filozofijo."
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Več kot 60 % spletnih kupcev po vsem svetu meni, da so cene pri e-trgovanju prvi kriterij, ki vpliva na njihovo odločitev o nakupu.",
                    "option2": "b) Poglavitna prednost ''strategije cene za e-trgovanje na podlagi stroškov'' je preprostost.",
                    "option3": "c) Cenik, ki temelji na konkurenci, lahko pripelje do prodaje vašega izdelka z nižjo ceno, kot si želite.",
                    "option4": "d) Oblikovanje cene izdelka na ''vrednotenju dodane vrednosti'' obljublja kratkoročni dobiček.",
                },
                "question": "Katera od naslednjih trditev je napačna?"
            }
        }
    }
};

export default class M3Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz3;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
