import React, { Component } from 'react'
import {Text, View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S20 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Davčne spodbude za zagonska podjetja žensk</Text>
                <Text style={styles.dmlH2}>Za Francijo:</Text>
                <Text>
                    Ženske podjetnice so predmet več specifičnih podpor. Javnost podjetnikov je predmet posebne pozornosti z namenom, da jim pomaga bolje uresničiti svoj projekt ustvarjanja ali obnovitve. Ženske podjetnice se včasih soočajo z bolj zapletenimi osebnimi situacijami ali večjim zunanjim nezaupanjem. Spremljevalci specifičnih pomoči so jim na razpolago, da jim pomagajo pospešiti njihov poslovni projekt ustvarjanja.
                    {"\n\n"}V smislu podpore so že nekaj let vzpostavljene specializirane mreže. Omrežje Les Premières je vzpostavilo inkubatorje in poslovne inkubatorje namenjene projektom za ustvarjanje poslovnih projektov, ki jih vodijo ženske. Ti ustvarjalni projekti morajo imeti inovativno stran in Interes za oblike spremstva. Inkubator zagotavlja spremljanje poslovnega ustvarjalca in hkrati gostovanje poslovnega projekta za 1 leto. Tako obdana kreativna poslovna ženska lahko začne prvo leto njenega poslovanja. Mreža Force Femmes podpira ženske v drugi polovici svoje kariere, t.j.  po starosti 45 let. Tiste z ustvarjalnim projektom ali projektom prevzema podjetja se jih pri tem ves čas spremlja v obliki: validacije projekta, oblikovanja, izvedbe poslovnega načrta itd. Actionelles podpira tudi ženske pri svojem ustvarjanju poslovnih projektov. Združenje ponuja poleg podpore vključuje tudi odnos med ustvarjalnimi ženskami in izkušenimi lastniki podjetij, za prekinitev osamitve.
                    {"\n\n"}Ta 3 omrežja niso nujno prisotna na celotnem nacionalnem ozemlju.
                    {"\n\n"}Podpora dostopa za financiranje ženskih podjetnic.
                    {"\n\n"}V zvezi z dostopom do financiranja je jamstvo EGALITE Femmes ustanovil France Active. Gre za bančno garancijo, namenjeno pospeševanju pridobivanja bančnih posojil podjetnicam. To jamstvo je mogoče mobilizirati za projekte ustvarjanja, predelave ali razvoja podjetij.
                    {"\n\n"}Ta naprava ni izključena iz drugih pripomočkov za ustvarjanje podjetij temveč dopolnjuje sistem podpore za žensko ustanovljene projekte ali prevzeme.

                </Text>
                <FlatList
                    data={[
                        {key: 'https://les-aides.fr/focus/a5Zi/les-aides-pour-les-femmes-creatrices-ou-repreneuses-d-entreprise.html'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
