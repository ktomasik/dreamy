import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Najpogostejše značilnosti socialnih medijev</Text>
                <Text>Danes lahko najdemo zelo veliko spletnih strani ki ponujajo spletna orodja za socialno mreženje. različna spletna orodja, imajo različne uporabnike In predstavljajo zelo različne
                    identitete. Vsem pa so skupne nekatere karakteristike (Sunil 2017). V nadaljevanju so naštete najbolj značilne:</Text>
                <Text style={styles.dmlH3}>Brezplačni spletni prostor:</Text>
                <Text>Člani teh spletnih mest ne potrebujejo spletnih strežnikov za objavljanje njihovih vsebin. Vso to strojno opremo zagotovi ponudnik spletnega medmrežja.</Text>
                <Text style={styles.dmlH3}>Provide free web address:</Text>
                <Text>Članom je dodeljen unikaten spletni naslov ki ga uporabnik uporablja za svojo identiteto na spletnem portalu. Ta spletni naslov je uporabljen tako za objavljanje novic, kot tudi
                    za sledenje drugim vsebinam.</Text>
                <Text style={styles.dmlH3}>Profil uporabnika:</Text>
                <Text>Vse te spletne strani zahtevajo od uporabnika da izpolni njihov profil. S temi informacijami ponudnik spletnega medmrežja lažje dodeli primernejše vsebine in prijatelje vsakemu
                    uporabniku posebej in na ta način izgradi učinkovito medmrežje v katerem so povezani ljudje s podobno vsebino in interesi iz celega sveta.</Text>
                <Text style={styles.dmlH3}>Spodbujanje uporabnikov k objavljanju vsebine:</Text>
                <Text>Vse te strani omogočajo uporabniku da objavlja novice v obliki besedila fotografij avdio in video datotek. Vse novice so objavljene in razvrščene v kronološkem vrstnem redu za
                    zadnjo novico na prvem pojavljanju. Še bolj pomembno pa je da je ta vsebina objavljena nemudoma in drugi uporabniki jo lahko takoj preberejo vidijo ali delijo z drugimi
                    uporabniki.</Text>
                <Text style={styles.dmlH3}>Omogočanje komentarjev:</Text>
                <Text>Uporabniki lahko objavljene vsebine tudi komentirajo in s tem omogočajo da se tudi drugi člani vključujejo v pogovor in širijo svoje ideje in poglede navezujoč se na neko
                    vsebino.</Text>
                <Text style={styles.dmlH3}>Omogočanje klepetalnic:</Text>
                <Text>Nekateri portali socialnih med mreži omogočajo tudi pogovore v živo. Tako si lahko člani omrežja izmenjujejo mnenja v realnem času.</Text>
                <Text style={styles.dmlH3}>Neposredna sporočila:</Text>
                <Text>Nekateri portali socialnega medmrežja omogočajo naslavljanje sporočil posameznim uporabnikom. S tem načinom sporočil je mogoče krepiti bolj intimne in osebne odnose.</Text>
                <Text style={styles.dmlH3}>Označevanje vsebin (tagging):</Text>
                <Text>Večina portalov za socialna omrežja omogočajo označevanje vsebin. Te označbe sistem izkorišča za povezovanje in ponujanje podobnih vsebin.</Text>
                <Text style={styles.dmlH3}>Omogočanje izdelave spletnih strani:</Text>
                <Text>Na nekaterih portalih spletnega medmrežja njihovi člani lahko ustvarijo tematske spletne strani. Take spletne strani lahko uporabljamo za objavljanje novice ali fotografij s
                    podobno tematiko. Prav take spletne strani lahko s pridom izkoristimo v poslovnem svetu za predstavitev podjetja ali predstavitev samih produktov projekta. </Text>
            </View>
        )
    }
}
