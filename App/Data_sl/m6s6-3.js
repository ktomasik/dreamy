import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Informacije o pogodbi</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>2. korak:</Text> Po vnosu vaših podatkov bodo merila vrednotena in podane vam bodo najprimernejše
                    ponudbe. Primerjati jih je mogoče z izbiro tistih, ki jih želite. Primerjavo na spletu je smiselno pregledati glede omejitev, garancije in cene.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_32_3319612.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
