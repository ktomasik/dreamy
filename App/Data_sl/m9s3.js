import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nastavitev računov za socialna omrežja za podjetje</Text>
                <Text styl={{marginTop: 10}}>Z vsakodnevnim večanjem števila uporabnikov, so socialni mediji postali del sodobnega tržno povezanega prostora. V nadaljevanju je vodič, ki vas bo po
                    korakih vodil, kako namestiti račune na socialna omrežja za vaše podjetje. </Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
