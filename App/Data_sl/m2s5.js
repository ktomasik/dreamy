import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Naredi Facebook račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 1: Presnemi Facebook program</Text>
                <Text>iz Apple Store</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img1.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 15}}>iz Windows Store</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img2.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 15}}>iz Google Play</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img3.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
