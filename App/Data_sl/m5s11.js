import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>PayPal poslovni račun</Text>
                <Text style={{marginTop: 15}}>Sledite navodilom za nastavitev poslovnega PayPal računa.
                    {"\n\n"}Sledite navodilom, da dokončate nastavitev računa . Potrditi boste morali e-poštni naslov svoj PayPal račun in izbrati plačilno rešitev PayPal.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1. korak:</Text>
                <Text>Ko ste se prijavili za PayPal poslovni račun, ste dobili e-poštno sporočilo. Kliknite na povezavo v e-poštne sporočilu, da potrdite svoj e-poštni naslov. Če ne najdete pošte, se
                    prijavite v svoj PayPal in kliknite “Potrdi e-poštni naslov”.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2. korak:</Text>
                <Text>S potrditvijo boste pridobili verodostojnost pri prodajalcih in kupcih ter odstranili preklic limita na vašem računu.</Text>
                <FlatList
                    data={[
                        {key: '1.	Potrdite kartico UnionPay  (takojšnjep reverjanje)\nDodajte ali preglejte podrobnosti kartice UnionPay. Nadaljujte s potrditvijo kartice s pooblastili China UnionPay (potrditveno kodo pošljete preko SMS).'},
                        {key: '2.	Potrdite svojo kreditno kartico\nDodajte kartico Visa ali MasterCard na svoj PayPal račun in nadaljujte s potrditvijo. To ustvari 4-mestno kodo, kar se odraža v izjavi  kartici znotraj 2-3 delovnih dneh. Prijavite ste v PayPal račun in vnesite kodo, da zaključite postopek preverjanja.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3 korak:</Text>
                <Text>Izberite plačilno rešitev, ki ustreza vašemu podjetju.</Text>
            </View>
        )
    }
}
