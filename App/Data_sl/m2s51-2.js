import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S51_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uporaba socialnih omrežij</Text>
                <Text style={styles.dmlH2}>Ustvarite FaceBook stran</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 7: Na tem mestu lahko vpišete URL naslov in obiskovalci bodo preusmerjeni na to stran. Če take strani nimate, preskočite ta korak in
                    kliknite NAPREJ (NEXT).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img104.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
