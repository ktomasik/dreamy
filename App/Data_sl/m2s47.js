import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S47 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Naredite Twitter račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 17: Vašo objavo boste lahko videli na začetni strani vašega profila (HOME).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img97.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
