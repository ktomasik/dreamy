import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S67 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Viri in literatura</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Bozzo, Frank. 2014. ‘5 Apps to Start and Run a Blog Entirely from Your Phone’. 2014.',val2: 'https://www.lifehack.org/articles/technology/5-apps-start-and-run-blog-entirely-from-your-phone.html'}},
                        {key: {val1: 'Center, Power Research. 2018. ‘Demographics of Social Media Users and Adoption in the United States | Pew Research Center’.',val2: 'http://www.pewinternet.org/fact-sheet/social-media/'}},
                        {key: {val1: '‘December 1995: Classmates - Then and Now: A History of Social Networking Sites - Pictures - Cbs News’. n.d.',val2: 'https://www.cbsnews.com/pictures/then-and-now-a-history-of-social-networking-sites/'}},
                        {key: {val1: 'Edvards, Benj. 2016. ‘The Lost Civilization of Dial-up Bulletin Board Systems - the Atlantic’.',val2: 'https://www.theatlantic.com/technology/archive/2016/11/the-lost-civilization-of-dial-up-bulletin-board-systems/506465/'}},
                        {key: {val1: '‘English Dictionary, Thesaurus, & Grammar Help | Oxford Dictionaries’. 2018.',val2: 'https://en.oxforddictionaries.com/'}},
                        {key: {val1: '‘History and Different Types of Social Media’. n.d.',val2: 'http://scalar.usc.edu/works/everything-you-always-wanted-to-know-about-social-media-but-were-too-afraid-to-ask/history-and-different-types-of-social-media'}},
                        {key: {val1: '‘Leading Global Social Networks 2018 | Statistic’. 2018. ',val2: 'https://www.statista.com/statistics/272014/global-social-networks-ranked-by-number-of-users/'}},
                        {key: {val1: 'Ortutay, Barbara. 2012. ‘Yahoo Is Now a Part of Oath’',val2: 'https://guce.oath.com/collectConsent?sessionId=3_cc-session_4721ad81-8f8c-40ec-9236-9909d53af443&lang=&inline=false&jsVersion=null&experiment=null'}},
                        {key: {val1: 'Ries, Monica. 2016. ‘The Complete History of Social Media’',val2: 'https://www.dailydot.com/debug/history-of-social-media/'}},
                        {key: {val1: 'Rouse, Margaret. 2016. ‘What Is Social Media? - Definition from Whatis.com’.',val2: 'https://whatis.techtarget.com/definition/social-media'}},
                        {key: {val1: 'Silver, Caleb. n.d. ‘Social Media’.',val2: 'https://www.investopedia.com/terms/s/social-media.asp'}},
                        {key: {val1: '‘Social Media - Wikipedia’. n.d.',val2: 'https://en.wikipedia.org/wiki/Social_media'}},
                        {key: {val1: 'Sunil, Saxena. 2017. What is Social Media and what are its main features - Easymedia.in. Retrieved from',val2: 'http://www.easymedia.in/social-media-main-features/'}},
                        {key: {val1: '‘WHAT Is Networking’. n.d.',val2: 'https://www.mccormick.northwestern.edu/career-development/documents/getting-started/job-search/networking/what-is-networking.pdf'}},
                        {key: {val1: '‘What Is Networking? | Reed.co.uk’. n.d.',val2: 'https://www.reed.co.uk/career-advice/what-is-networking/'}},
                        {key: {val1: 'York, Alex. 2018. ‘7 Step Social Media Marketing Strategy for 2018 | Sprout Social’.',val2: 'https://sproutsocial.com/insights/social-media-marketing-strategy/'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
