import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Naredi Facebook račun</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Korak 7: Kliknite na gumb VPIŠI SE (SIGN UP) in počakajte, da program zaključi ustvarjanje vašega računa.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img15.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img16.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
