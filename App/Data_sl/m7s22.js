import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S22 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Mikrofinančni in drugi zagonski skladi za ženske</Text>
                <Text style={styles.dmlH2}>Za Slovenijo:</Text>
                <Text style={styles.dmlH3}>Vstopne točke SPOT (VEM)</Text>
                <Text>Informacije, osnovni nasveti, registracija podjetja.</Text>

                <Text style={styles.dmlH3}>Podjetniški inkubatorji</Text>
                <Text>Opremljene pisarne, poslovne in druge podporne storitve</Text>

                <Text style={styles.dmlH3}>Univerzitetni inkubatorji</Text>
                <Text>Opremljene pisarne, poslovno svetovanje in mentorstvo, brezplačne izobraževalne delavnice.
                    {"\n\n"}Podjetniški inkubator Univerze v Mariboru
                    {"\n\n"}Ljubljanski univerzitetni inkubator
                    {"\n\n"}Univerzitetni razvojni center in inkubator Primorske
                </Text>


                <Text style={styles.dmlH3}>Tehnološki parki</Text>
                <Text>Opremljene pisarne, mentorstvo, svetovanje, sodelovanje, informacije.</Text>

                <Text style={styles.dmlH3}>Iniciativa Start:up Slovenija</Text>
                <Text>Mreženje, organizacija tekmovanja.</Text>

                <Text style={styles.dmlH3}>Mreža European Enterprise Network (EEN)</Text>
                <Text>Iskanje poslovnih partnerjev, informacije, svetovanje.</Text>

                <Text style={styles.dmlH3}>Coworking prostori Coworking MB Hekovnik</Text>
                <Text>Začetek poslovanja, mreženje, iskanje poslovnih partnerjev, obveščanje, svetovanje, usposabljanje.</Text>

                <Text style={styles.dmlH3}>Gospodarska zbornica Slovenije</Text>
                <Text>Svetovanje, usposabljanje, pomoč pri internacionalizaciji.</Text>

                <Text style={styles.dmlH3}>Obrtno podjetniška zbornica Slovenije</Text>
                <Text>Svetovanje, usposabljanje, izdajanje obrtnih dovoljenj, EU spričevala, potrdila o občasni obrti v Republiki Sloveniji, dovoljenja za opravljanje prevoza.</Text>

                <Text style={styles.dmlH3}>Program Erasmus za mlade podjetnike</Text>
                <Text>Sofinanciranje podjetniškega usposabljanja - izmenjava izkušenj med podjetniki v EU.</Text>
            </View>
        )
    }
}
