import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S16 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Donacije za mala podjetja za ženske</Text>
                <Text style={styles.dmlH2}>Za Poljsko:</Text>

                <Text style={styles.dmlH3}>Sredstva EU</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Skupna informativna spletna stran o sredstvih EU na Poljskem. Na tej spletni strani lahko vsak podjetnik ali bodoči podjetnik (pa tudi nevladne organizacije, javni organi itd.) poišče zanimive informacije o razpoložljivem financiranju.',
                                val2: 'https://www.funduszeeuropejskie.gov.pl/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Poljska agencija za razvoj podjetij</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Na Poljskem Poljska agencija za razvoj podjetništva ponuja posojilni sklad le ženskam, katerega namen je podpirati poklicno aktiviranje žensk in izboljšati njihov položaj na trgu dela s spodbujanjem žensk k ustanavljanju lastnih podjetij. Sklad za posojila za ženske prispeva k zmanjšanju problema brezposelnosti v tej skupini. Ženske se lahko prijavijo za ugodno posojilo v znesku med 5 in 10 tisoč evrov (20 in 40 tisoč PLN).',
                                val2: 'http://en.parp.gov.pl/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
