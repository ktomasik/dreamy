import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 13: Επιλέξτε την εικόνα του προφίλ σας. Μπορείτε να τραβήξετε τη φωτογραφία με τη φωτογραφική μηχανή ή να επιλέξετε φωτογραφία από τη
                    συλλογή. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img22.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img23.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img24.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
