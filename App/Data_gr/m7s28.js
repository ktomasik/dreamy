import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Τουρκία::</Text>
                <Text style={styles.dmlH3}>Βήμα 1:</Text>
                <Text>Οι υποψήφιες επιχειρηματίες που επιθυμούν να λάβουν κρατική στήριξη πρέπει να συμμετάσχουν σε πρόγραμμα κατάρτισης για την επιχειρηματικότητα, που πραγματοποιείται από την
                    KOSGEB. Περίπου το 45% της συνολικής κατάρτισης KOSGEB πραγματοποιείται σε γυναίκες.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
