import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img3.png')
    }
}];

export default class M6S3_5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Τιμολόγηση στο κινητό</Text>
                <Text style={styles.dmlH3}>Android και iOS</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text>Η Wave παρέχει δυνατότητα προβολής Android και Chrome στο ίδιο πορτοφόλι. Η εφαρμογή, στην οποία μπορείτε να έχετε πρόσβαση από οποιοδήποτε κινητό τηλέφωνο, περιγράφεται
                    παρακάτω:</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 1:</Text> Πληκτρολογήστε "τιμολόγηση Wave σε iOS και Android" στο Διακομιστή Ιστού της Google
                </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img3.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
