import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";


export default class M4S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Λήψη Αριθμού Φορολογικού Μητρώου ( ΑΦΜ ) </Text>
                <Text style={{marginTop: 10}}>Ακολουθήστε τις οδηγίες για να πάρετε Αριθμό Φορολογικού Μητρώου: </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Κάνετε αίτηση στη ΔΟΥ του τόπου διαμονής σας
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Δηλώστε τη διεύθυνση κατοικίας σας/ έδρα σας και τα υπόλοιπα στοιχεία σας
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Πάρτε κλειδάριθμο για να μπαίνετε στο σύστημα TAXIS
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
