import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Γαλλία:</Text>

                <Text style={styles.dmlH3}>Για την περίπτωση που ψάχνετε για δουλειά</Text>

                <Text style={styles.dmlH3}>Βήμα 3:</Text>
                <Text>Συμπληρώστε την αίτηση επιχορήγησης.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
