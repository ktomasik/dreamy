import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S35 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να ανταλλάξετε πληροφορίες από το Instagram στα άλλα μέσα κοινωνικής δικτύωσης</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 5: Προτείνεται να προσθέσετε μια # hashtag στην περιγραφή σας για να συμπεριλάβετε την ανάρτηση στο περιεχόμενο #hashtags. Είναι έξυπνο
                    να συμπεριλαμβάνει #hashtag που έχει ήδη πολλούς οπαδούς. Εφόσον τοποθετούμε μια ασπίδα ardunio για τη ρομποτική, συμπεριλαμβάνουμε το #arduino και το arduinoshield. Και τα δύο
                    #hashtags ακολουθούνται από αρκετούς δέκα χιλιάδες followers. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img66.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img67.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
