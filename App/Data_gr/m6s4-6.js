import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";


const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img14.png')
    }
}];

export default class M6S4_6 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Πώς να δημιουργήσετε ένα τιμολόγιο στα Έγγραφα Google</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight:'bold'}}>Βήμα 6:</Text> Ανοίξτε τη σελίδα Google, επάνω δεξιά και κάντε κλικ στην “Εφαρμογή Google” </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img14.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
