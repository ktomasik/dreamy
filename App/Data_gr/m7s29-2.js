import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Ελλάδα:</Text>

                <Text style={styles.dmlH3}>Βήμα 2:</Text>
                <Text> Θα πραγματοποιηθεί μια πρώτη συνάντηση με τον Αρμόδιο Φορέα, για να γίνει μια πιο λεπτομερής επισκόπηση του επιχειρηματικού σχεδίου. Σκοπός της συνάντησης είναι να επιβεβαιωθεί
                    η εκπλήρωση των κριτηρίων αξιολόγησης και ο εντοπισμός των χρηματοδοτικών αναγκών, καθώς και το κατάλληλο χρηματοδοτικό μέσο για την κάλυψη των αναγκών αυτών.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
