import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S41_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Twitter</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 6: Ελέγξτε τη σύνδεση με ανθρώπους που γνωρίζετε για να εξαπλώσετε τα tweets σας πιο αποτελεσματικά και κάντε κλικ στο Επόμενο. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img80.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
