import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img6.png')
    }
}];

export default class M9S3_9 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Ρύθμιση της σελίδας στο Facebook για επαγγελματική χρήση </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 8:</Text> Προσθέστε ένα “Κουμπί Προτροπής” στην εταιρική σας σελίδα. Με τη βοήθεια αυτού του
                    κουμπιού, οι πελάτες σας μπορούν να επικοινωνούν μαζί σας μέσω email,
                    {"\n\n"}τηλεφώνου ή ιστοσελίδας ενώ ταυτόχρονα έχουν τη δυνατότητα να πραγματοποιήσουν αγορές, ακόμη και να ξεκινήσουν ένα παιχνίδι online.
                    {"\n\n"}Για να προσθέσετε ένα “Κουμπί Προτροπής” στη σελίδα σας:

                </Text>
                <FlatList
                    data={[
                        {key: 'Πηγαίνετε ακριβώς από κάτω από τη φωτογραφία του εξωφύλλου σας και πατήστε: Προσθέστε ένα Κουμπί Προτροπής.'},
                        {key: 'Επιλέξτε μια δυνατότητα από το αναδυόμενο μενού και ακολουθείστε τις οδηγίες που εμφανίζονται στην οθόνη, ανάλογα με τις ανάγκες σας.'},
                        {key: 'Πατήστε Τέλος.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img6.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
