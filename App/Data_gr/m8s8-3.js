import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M8S8_3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>ΕΦΑΡΜΟΓΗ ΣΤΗΝ ΠΡΑΞΗ</Text>

                <Text style={styles.dmlH3}>ΒΗΜΑ 3 : ΕΠΙΛΕΞΤΕ ΤΟΝ ΤΥΠΟ ΦΟΡΤΙΟΥ ΚΑΙ ΤΗΝ ΕΤΑΙΡΕΙΑ ΑΠΟΣΤΟΛΗΣ</Text>
                <Text>Ορισμένες από τις εταιρείες που ειδικεύονται στις αποστολές προϊόντων στο εσωτερικό αλλά και στο εξωτερικό είναι: </Text>
                <FlatList
                    data={[

                        {key: {val1: 'Acs Courier:', val2: 'http://www.acscourier.net/'}},
                        {key: {val1: 'Γενική Ταχυδρομική:', val2: 'http://www.taxydromiki.com'}},
                        {key: {val1: 'Speedex:', val2: 'http://www.speedex.gr/'}},
                        {key: {val1: 'ΕΛΤΑ Courier:', val2: 'http://www.elta-courier.gr'}},
                        {key: {val1: 'DHL:', val2: 'http://www.logistics.dhl/global-en/home.html'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key.val1}: <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}

