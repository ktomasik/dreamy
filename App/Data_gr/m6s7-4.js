import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S7_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργία ηλεκτρονικής υπογραφής</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 3:</Text> Για να στήσετε τον λογαριασμό σας DocuSign πληκτρολογήστε τις απαιτούμενες πληροφορίες και
                    δημιουργήστε έναν λογαριασμό </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_6_3319634.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
