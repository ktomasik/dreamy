import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S28 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 15: Θα επιλέξουμε μια κατάλληλη φωτογραφία από μια συλλογή. Κατά τη διάρκεια της επιλογής πρέπει να επιτρέψουμε στην εφαρμογή του
                    Instagram να έχει πρόσβαση στη δική μας συλλογή φωτογραφιών. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img52.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img53.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img54.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
