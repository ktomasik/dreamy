import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img11.png')
    }
}];

export default class M9S5_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Δημιουργία εταιρικού λογαριασμού στο Instagram </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop:10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 3:</Text> Προχωρήστε στα “Εργαλεία του Instagram για επιχειρήσεις”</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img11.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
