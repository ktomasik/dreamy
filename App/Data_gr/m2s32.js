import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S32 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να ανταλλάξετε πληροφορίες από το Instagram στα άλλα μέσα κοινωνικής δικτύωσης</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 2: Μπορείτε να επιλέξετε να μεταφορτώσετε την επιθυμητή εικόνα από τρεις πηγές: γκαλερί, φωτογραφία ή βίντεο. Για να εισάγετε τη
                    φωτογραφία από τις φωτογραφίες σας, κάντε κλικ στη γκαλερί, επιλέξτε την επιθυμητή φωτογραφία και κάντε κλικ στο κουμπί Επόμενο. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img60.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img61.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img62.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
