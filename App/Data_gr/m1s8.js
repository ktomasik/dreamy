import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Αναζήτηση από τη Google και Δημιουργία λογαριασμού ηλεκτρονικού ταχυδρομείου από smartphone</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Χρησιμοποιήστε το λογαριασμό που δημιουργήσατε για να συνδεθείτε στο Gmail στο τέλος των βημάτων, στείλτε e-mail στη διεύθυνση:
                    <Text style={{color: 'blue'}}>dreamy m-learning@gmail.com</Text>.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img7.png')}
                        resizeMode="contain"/>
                    <Text style={{marginTop: 10}}/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m1/img8.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Συγχαρητήρια για τη δημιουργία του λογαριασμού αλληλογραφίας σας!</Text>
            </View>
        )
    }
}
