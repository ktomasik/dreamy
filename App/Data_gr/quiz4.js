import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements'
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz4": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Ένα λογότυπο",
                    "option2": "b) Μια μάρκα",
                    "option3": "c) Μια ευρεσιτεχνία. (Πατέντα.)",
                    "option4": "d) Το ΑΦΜ"
                },
                "question": "_______ είναι ένα όνομα, λογότυπο, σύμβολο ή σχήμα το οποίο ξεχωριστά ή σε συνδυασμό επιτρέπει στον καταναλωτή να διαφοροποιεί το προϊόν ή την υπηρεσία από άλλα στην αγορά"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η μάρκα και το εμπορικό σήμα πρέπει να είναι τα ίδια."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Να απευθυνθείτε στον τοπικό Δήμο",
                    "option2": "b) Να απευθυνθείτε στην Εφορία",
                    "option3": "c) Να έχετε ενοικιαστήριο που να το επιτρέπει, ή να είναι δικό σας το σπίτι",
                    "option4": "d) Να απευθυνθείτε στο Επιμελητήριο"
                },
                "question": "Τι πρέπει να κάνετε πρώτα για να μπορείτε να πάρετε άδεια για να εργάζεστε από το σπίτι;"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η νομική επωνυμία χρησιμοποιείται για τις κυβερνητικές διαδικασίες, η εμπορική επωνυμάι για τις δημόσιες σχέσεις."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Το εμπορικό σήμα αποτελεί νομική προστασία της μάρκας, που χορηγείται από το Υπουργείο Εμπορίου."
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Αίτηση στην εφορία",
                    "option2": "b) Ερώτηση στον ιδιοκτήτη του διαμερίσματος",
                    "option3": "c) Εγγραφή στο Επιμελητήριο",
                    "option4": "d) Προμήθεια μπλοκ τιμολογίων"
                },
                "question": "Ποιο είναι το πρώτο βήμα για να ανοίξει κανείς μερίδα φορολογούμενου;"
            },
            "question7": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Η μάρκα",
                    "option2": "b) Τιμολόγιο",
                    "option3": "c) Συνδρομή στο Επιμελητήριο",
                    "option4": "d) Αριθμός Φορολογικού Μητρώου και κλειδάριθμος"
                },
                "question": "Τι χρειάζεται για να μπει κανείς στο σύστημα ηλεκτρονικών δηλώσεων;"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η επωνυμία της επιχείρησης δεν μπορεί να χρησιμοποιηθεί στην επικοινωνία με δημόσιους οργανισμούς."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Συμφωνία χρήστη",
                    "option2": "b) Πολιτική απορρήτου",
                    "option3": "c) Άδεια του ιδιοκτήτη του διαμερίσματος ",
                    "option4": "d) Πατενταρισμένο λογότυπο"
                },
                "question": "Ποιο από τα παρακάτω δεν απαιτείται για την έναρξη ηλεκτρονικού εμπορίου μέσω ιστοσελίδας;"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Χωρίς να καταχωρίσουμε το εμπορικό σήμα, δεν μπορούμε να εμποδίσουμε τους ανταγωνιστές ή τους σφετεριστές να χρησιμοποιούν το ίδιο εμπορικό σήμα."
            }
        }
    }
};

export default class M4Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz4;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
