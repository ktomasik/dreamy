import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να πουλάτε το προϊόντα σας στις καλύτερες τιμές της αγοράς του διαδικτύου.</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 4: Προσφέρετε εκπτώσεις για αγορές άνω των 2 προϊόντων και προσδιορίστε το ακριβές κέρδος για τον αγοραστή.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/24911.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
