import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img3.png')
    }
}];

export default class M9S3_6 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Ρύθμιση της σελίδας στο Facebook για επαγγελματική χρήση </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 5:</Text> Προσθέτετε εικόνες: Ανεβάστε εικόνες προφίλ και εξωφύλλου για την σελίδα σας στο Facebook.
                    Επιπλέον, μπορείτε να ανεβάσετε και το logo της επιχείρησής σας. Σε περίπτωση που δεν έχετε, μπορείτε να ανεβάσετε μια εικόνα από μια πρόσφατη δημιουργία σας και φροντίστε να την
                    επικαιροποιείτε. Είναι πολύ σημαντικό να φτιάξετε μια δυνατή, αισθητικά, εικόνα ώστε να κάνετε καλή εντύπωση. (Δείτε σχετικά με το θέμα στην ενότητα 3)
                    {"\n\n"}Από την άλλη μεριά, το μέγεθος της εικόνας είναι εξίσου σημαντικό. Οι σωστές διαστάσεις για την φωτογραφία εξωφύλλου του λογαριασμού σας πρέπει να είναι
                    {"\n\n"}563 x 315px και να είναι στοιχισμένη στο κέντρο ώστε να εμφανίζεται σωστά και σε κινητά τηλέφωνα και tablets .
                </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img3.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
