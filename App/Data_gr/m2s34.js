import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S34 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να ανταλλάξετε πληροφορίες από το Instagram στα άλλα μέσα κοινωνικής δικτύωσης</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 4: Κάντε κλικ στο πεδίο γράψτε ένα σχόλιο και γράψτε μια σύντομη περιγραφή της φωτογραφίας σας. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img65.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
