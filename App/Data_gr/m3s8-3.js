import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S8_3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να φωτογραφίσετε τα προϊόντα σας και να αναρτήσετε τις φωτογραφίες τους στο etsy.com</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 19: Είστε πλέον έτοιμοι να ξεκινήσετε να πουλάτε στην Etsy!</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/28647.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
