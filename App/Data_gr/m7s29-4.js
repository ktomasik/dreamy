import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M7S29_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Ελλάδα:</Text>

                <Text style={styles.dmlH3}>Βήμα 4:</Text>
                <Text>Το τελικό επιχειρησιακό σχέδιο αξιολογείται από την αρμόδια επιτροπή και, εάν εγκριθεί, θα ακολουθήσει η παροχή μικροχρηματοδότησης μέχρι € 10.000 συνοδευόμενη από συμβουλευτική
                    υποστήριξη για την έναρξη της εφαρμογής του επιχειρησιακού πλάνου.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
