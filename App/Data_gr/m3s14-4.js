import React, {Component} from 'react'
import {Text, Image, View, TouchableHighlight, Modal} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen11 = [{
    url: '',
    props: {
        source: require('../Images/m3/img66.png')
    }
}];

export default class M3S14_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Εάν θέλετε να δημιουργήσετε μια ιστοσελίδα, ακολουθήστε τα παρακάτω βήματα</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen11}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 9: Είτε να αγοράσετε ένα ξεχωριστό όνομα domain για εσάς μέσω της παρακάτω σελίδας. Επίσης μπορείτε να χρησιμοποιήσετε και κάποιο
                    domain name που έχετε ήδη αγοράσει (π.χ. από το godaddy)</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img66.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
