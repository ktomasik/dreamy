import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για τη Σλοβενία: </Text>
                <Text style={styles.dmlH3}>Βήμα 1:</Text>
                <Text>Εγγραφείτε ως άνεργοι στην Υπηρεσία Απασχόλησης της Σλοβενίας. Στη συνέχεια, αναφέρετε στον προσωπικό σας σύμβουλο, κατά την πρώτη σας συνάντηση, ότι θέλετε μια επιδότηση. Ο
                    προσωπικός σύμβουλος θα ετοιμάσει ένα σχέδιο πρόσληψης και μια δήλωση συμμετοχής στο πρόγραμμα βοήθειας για αυτοαπασχόληση.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
