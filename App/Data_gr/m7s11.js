import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Αναζήτηση δάνειων μικρών επιχειρήσεων για τις γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Πολωνία:</Text>
                <Text style={styles.dmlH3}>mBank</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Δάνεια για άτομα που παρουσιάζουν επιχειρηματική δραστηριότητα που δεν υπερβαίνει τους 6 μήνες σε ποσό μέχρι 30.000 PLN. Μπορείτε να απευθυνθείτε online.',
                                val2: 'https://www.mbank.pl/firmy/kredyty/na-start/kredyty-na-start/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Aasa Polska</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Δάνειο για επιχειρηματική ανάπτυξη μέχρι 20.000 PLN από την πρώτη ημέρα λειτουργίας.', val2: 'https://aasadlabiznesu.pl/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Credit Agricole</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Δάνεια για μικρές και μεσαίες επιχειρήσεις.', val2: 'https://www.credit-agricole.pl/male-i-srednie-firmy/kredyty/pozyczka-biznes/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>PKO BP</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Δάνειο εκκίνησης για επιχειρήσεις.', val2: 'https://www.pkobp.pl/firmy/kredyty/produkty-kredytowe/pozyczka-na-start/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>ING</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Δάνειο για επιχειρήσεις.', val2: 'https://www.ingbank.pl/male-firmy/kredyty-i-pozyczki/pozyczka-dla-malych-firm'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
