import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Ρύθμιση της σελίδας στο Facebook για επαγγελματική χρήση  </Text>
                <Text>Για να κινηθείτε επαγγελματικά και να πουλήσετε στο Facebook, αρχικά χρειάζεται να έχετε σελίδα στο Facebook.
                    {"\n\n"}"(Δείτε σχετικά με το θέμα στην Ενότητα 2)
                    {"\n\n"}Στη σελίδα σας στο Facebook, οι επισκέπτες θα δουν το Tab για το κατάστημά σας και θα έχουν πρόσβαση στα προϊόντα σας μέσω της σελίδας αυτής.

                </Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 1:</Text> Εισέλθετε στο προφίλ σας στο Facebook
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
