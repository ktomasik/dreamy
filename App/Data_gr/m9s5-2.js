import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img8.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m9/img9.png')
    }
}];

export default class M9S5_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Δημιουργία εταιρικού λογαριασμού στο Instagram </Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 1:</Text>Πραγματοποιείστε είσοδο στην εφαρμογή με τα στοιχεία του email σας ή με τον αριθμό του
                    τηλεφώνου σας και μετά εισαγάγετε το όνομα χρήστη.</Text>
                <Text style={{textAlign: 'center', color: 'red'}}> Η΄</Text>
                <Text>Αν έχετε λογαριασμό στο Facebook, μπορείτε να εισέλθετε με τα ίδια στοιχεία και να συσχετίσετε τους λογαριασμούς.
                    {"\n\n"}"(Στην ενότητα 1 υπάρχει η περιγραφή της Δημιουργίας Λογαριασμού στο Facebook)
                </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Πατήστε Είσοδος, και μετά εισαγάγετε το email σας και πατήστε Επόμενο ή Πραγματοποιείστε Είσοδο μέσω Facebook, για να εισέλθετε με τον λογαριασμό σας στο
                    Facebook.</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
