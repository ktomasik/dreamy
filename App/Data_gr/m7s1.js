import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Εισαγωγή </Text>
                <Text>Αυτή η ενότητα έχει ως σκοπό να εξηγήσει τις διάφορες οικονομικές ευκαιρίες και τους τρόπους με τους οποίους οι γυναίκες μπορούν να πουλήσουν τα χειροτεχνήματα τους. Η ενότητα
                    παρέχει πληροφορίες και συνδέσμους για τις γυναίκες αυτές σχετικά με την εύρεση οικονομικής υποστήριξης και πιστωτικών διευκολύνσεων σε πέντε χώρες (Σλοβενία, Γαλλία, Τουρκία,
                    Ελλάδα και Πολωνία), στα πλαίσια του προγράμματος Dreamy m-learning.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_48_3319639.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
