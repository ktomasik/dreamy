import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img7.png')
    }
}];

export default class M9S3_10 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Ρύθμιση της σελίδας στο Facebook για επαγγελματική χρήση </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 9:</Text> Δημιουργήστε την πρώτη σας ανάρτηση επί τόπου (ή για τον καλύτερο έλεγχο των αναρτήσεών
                    σας, μπορείτε να τις προγραμματίσετε σε βάθος χρόνου ορισμένων ημερών) κι αισίως στήσατε με επιτυχία την εταιρική σας σελίδα!
                    {"\n\n"}Τώρα, περιηγηθείτε μέσω του “Εργαλεία Δημοσίευσης”, για να δείτε όλες τις επιλογές που σας δίνονται, προκειμένου να δημιουργήσετε την πρώτη σας ανάρτηση.

                </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img7.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
