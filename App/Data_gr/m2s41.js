import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S41 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Twitter</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 5: Κάντε κλικ στην επιλογή Ξεκινήστε και πληκτρολογήστε τη διεύθυνση του ηλεκτρονικού σας ταχυδρομείου και τον αριθμό του τηλεφώνου σας
                    και κάντε κλικ στο κουμπί Επόμενο. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img79.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
