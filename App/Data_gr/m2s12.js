import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 8: Τώρα πρέπει να ρυθμίσετε το λογαριασμό σας στο Facebook. Πρώτα μπορείτε να ΕΠΙΤΡΕΨΕΤΕ στο Facebook να στείλει και να προβάλει τα
                    μηνύματά σας ( SMS ). </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img17.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
