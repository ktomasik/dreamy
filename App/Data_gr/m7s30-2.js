import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

//                   \/
export default class M7S30_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Γαλλία:</Text>
                <Text style={styles.dmlH3}>Για την περίπτωση που δεν ψάχνετε δουλειά: ΚΡΑΤΙΚΗ ΒΟΗΘΕΙΑ - ACCRE</Text>

                <Text style={styles.dmlH3}>Βήμα 2:</Text>
                <Text>Στη συνέχεια θα γίνει έκδοση μιας απόδειξης για την εγγραφή στο ACCRE και σαν αντάλλαγμα για την εγγραφή, θα γίνει ενημέρωση των κοινωνικών οργανώσεων σχετικά με το αίτημά σας.
                    Αυτό θα σταλεί κατόπιν στον αρμόδιο της URSSAF εντός 24 ωρών, ο οποίος διαχειρίζεται την αίτηση εντός ενός μήνα.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
