import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Εγγραφή σε επαγγελματικές ενώσεις</Text>
                <Text style={{marginTop: 10}}>Ακολουθήστε τις οδηγίες για εγγραφή σε Επαγγελματικές Ενώσεις: </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Η φορολογική υπηρεσία μπορεί να σας κατευθύνει στο σχετικό Επιμελητήριο, όπου μπορείτε να υποβάλετε αίτηση εγγραφής ως μέλος
                        </Text>
                    </View>
                </View>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 320}}
                        source={require('../Images/images/30321.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
