import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S24 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Μικροχρηματοδότηση κι άλλα Κεφάλαια Εκκίνησης για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Ελλάδα:</Text>
                <Text style={styles.dmlH3}>The People's Trust</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Ο οργανισμός “The People’s Trust” προσφέρει μικρο-επιχορηγήσεις σε Έλληνες επιχειρηματίες που επιθυμούν είτε να δημιουργήσουν μια νέα επιχείρηση είτε να αναπτύξουν μια υπάρχουσα επιχείρηση, αλλά αντιμετωπίζουν δυσκολίες πρόσβασης στην πίστωση. Η επιχορήγηση είναι μέχρι  10.000€ ανά επιχείρηση, ποσό που παρέχεται ως αρχικό κεφάλαιο για μια νέα επιχείρηση ή ως ένα κεφάλαιο κίνησης για μία υπάρχουσα. Αυτό το πρόγραμμα χρηματοδότησης επικεντρώνεται σε ομάδες με χαμηλή πρόσβαση σε άλλες μορφές χρηματοδότησης.',
                                val2: 'http://www.thepeoplestrust.org'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Microfinancing (AFI & Eurobank)</Text>
                <Text>Η AFI (Action Finance Initiative) είναι μια μη κερδοσκοπική εταιρεία. Δημιουργήθηκε στην Ελλάδα το 2014 από την ActionAid Hellas και τον γαλλικό οργανισμό ADIE, ηγέτη στην
                    μικροπίστωση στην Ευρώπη.
                    {"\n\n"}Η Eurobank συνεργάζεται με την AFI για να βοηθήσει με μικροπιστώσεις (μέχρι € 15.000) τους μακροχρόνια ανέργους, άτομα που ανήκουν σε ευάλωτες κατηγορίες πολιτών και
                    μικροεπιχειρηματίες χωρίς πρόσβαση σε τραπεζικό δανεισμό, προσφέροντας την ευκαιρία να δημιουργήσουν τη δική τους δουλειά (αυτοαπασχόληση) ή να αναπτύξουν μονάδες μικρών
                    επιχειρήσεων και να δημιουργηθούν έτσι νέες θέσεις εργασίας.
                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Η AFI αναλαμβάνει την προεπιλογή, την κατάρτιση και την καθοδήγηση των υποψηφίων. Η Eurobank αναλαμβάνει τον πιστωτικό έλεγχο και τη χρηματοδότηση.',
                                val2: 'https://www.eurobank.gr/el/business/proionta-kai-upiresies/proionta-upiresies/xrimatodotiseis/anaptuksiaka/easy-afi'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
