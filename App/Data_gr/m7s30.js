import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Γαλλία:</Text>
                <Text style={styles.dmlH3}>Για την περίπτωση που δεν ψάχνετε δουλειά: ΚΡΑΤΙΚΗ ΒΟΗΘΕΙΑ - ACCRE</Text>

                <Text style={styles.dmlH3}>Βήμα 1:</Text>
                <Text>Η αίτησή σας πρέπει να κατατεθεί στο Αρμόδιο Κέντρο Σύστασης Επιχειρήσεων (CFE) όταν συσταθεί η επιχείρηση ή όταν επανασυσταθεί ή 45 ημέρες από την σύσταση/ επανασύσταση.
                    {"\n\n"}Στο αίτησή σας πρέπει να επισυνάψετε:
                    {"\n\n"}Το έντυπο δήλωσης της εταιρείας στο CFE ή το αντίγραφό του.
                    {"\n\n"}Τη φόρμα της αίτησης ενίσχυσης, η οποία ισχύει με την προϋπόθεση ότι δεν έχετε λάβει την εν λόγω ενίσχυση για 3 έτη.
                    {"\n\n"}Ένα αποδεικτικό ότι ανήκετε σε μία από τις κατηγορίες που επωφελούνται από το ACCRE.
                    {"\n\n"}Ενδέχεται να ζητηθούν κι άλλα έγγραφα από το CFE σας, επομένως θα ήταν καλό να επικοινωνήσετε μαζί τους.
                </Text>

            </View>
        )
    }
}
