import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S3 extends Component {
  render() {
    return (
      <View style={styles.section}>
        <Text style={styles.dmlH2}>Αναζήτηση από τη Google και Δημιουργία λογαριασμού ηλεκτρονικού ταχυδρομείου από smartphone</Text>
        <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 1: Ανοίξτε την "καρτέλα Internet" από το κινητό σας τηλέφωνο</Text>
          <View style={{alignItems: 'center'}}>
              <Image
                  style={{flex:1, width: 320, height: 400}}
                  source={require('../Images/images/search.jpg')}
                  resizeMode="contain"/>
          </View>
      </View>
    )
  }
}
