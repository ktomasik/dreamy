import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S59_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα ιστολόγιο ( Blog )</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 7: Αν δεν έχετε ακόμα λογαριασμό,  θα πρέπει πρώτα να εγγραφείτε. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img122.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
