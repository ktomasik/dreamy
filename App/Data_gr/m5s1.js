import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Εισαγωγή</Text>
                <Text style={{marginTop: 10}}>Η ενότητα αυτή αναφέρεται στις συνήθεις πρακτικές λογιστικών διαδικασιών για την ίδρυση γυναικείων επιχειρήσεων. Συγκεκριμένα αναφέρεται στο άνοιγμα των
                    τραπεζικών λογαριασμών, στους τύπους τους, στις μεθόδους των τραπεζικών συναλλαγών και στις πληροφορίες σχετικά με το ηλεκτρονικό εμπόριο και το ηλεκτρονικό σύστημα πληρωμών
                    (online ή μέσω πιστωτικής κάρτας). Περιέχει επίσης οδηγίες για την εφαρμογή τους.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 280, height: 340}}
                        source={require('../Images/images/6931.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
