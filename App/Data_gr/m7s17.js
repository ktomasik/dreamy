import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S17 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Φορολογικά κίνητρα για γυναίκες σε νεοσύστατες επιχειρήσεις </Text>
                <Text style={styles.dmlH2}>Για τη Σλοβενία: </Text>
                <Text>Μερική απαλλαγή από την καταβολή εισφορών έως 2 έτη από την σύσταση της εταιρείας. Αυτή η απαλλαγή συμβαίνει για πρώτη φορά και αφορά αυτοαπασχολούμενους που περιλαμβάνονται στην
                    κοινωνική ασφάλιση.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_41_3319603.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
