import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Τουρκία::</Text>

                <Text style={styles.dmlH3}>Βήμα 3:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Οι ημερομηνίες έναρξης της κατάρτισης για την επιχειρηματικότητα δημοσιεύονται στην επίσημη ιστοσελίδα του KOSGEB. Αυτές οι εκπαιδεύσεις επιχειρηματικότητας είναι εντελώς δωρεάν. ',
                                val2: 'http://www.kosgeb.gov.tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
