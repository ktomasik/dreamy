import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Ελλάδα:</Text>
                <Text style={styles.dmlH3}>Βήμα 1:</Text>
                <Text>Η διαδικασία έγκρισης χρηματοδότησης περιλαμβάνει 4 βήματα. Το πρώτο βήμα είναι να συμπληρώσετε τη φόρμα ενδιαφέροντος.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
