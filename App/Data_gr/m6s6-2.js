import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img19.png')
    }
}];

export default class M6S6_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Πληροφορίες σχετικά με τις συμβάσεις</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 1:</Text> Συμπληρώστε τη φόρμα που περιλαμβάνεται στο ερωτηματολόγιο ασφάλισης, που φαίνεται
                    παρακάτω. Φροντίστε ώστε οι πληροφορίες που συμπληρώνετε να είναι οι σωστές. Λανθασμένες πληροφορίες δεν πρέπει να εισαχθούν. Οι πληροφορίες που απαιτούνται για κάθε εμπορική
                    δραστηριότητα ποικίλλουν.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img19.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
