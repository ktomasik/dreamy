import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Επιχορηγήσεις μικρών επιχειρήσεων για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για τη Σλοβενία: </Text>
                <Text style={styles.dmlH3}>Slovenski podjetniški sklad (Ταμείο Επιχειρήσεων της Σλοβενίας)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Επιδοτήσεις, εγγυήσεις, προώθηση ιδιωτικών επενδύσεων (μετοχικό κεφάλαιο, δάνεια, εγγυήσεις) και κεφάλαια εκκίνησης για νέες καινοτόμες εταιρείες.",
                                val2: 'https://www.podjetniskisklad.si/en'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Οργανισμός της Δημοκρατίας της Σλοβενίας για τις γεωργικές αγορές και την αγροτική ανάπτυξη</Text>
                <Text>Υποστήριξη για τη δημιουργία και την ανάπτυξη πολύ μικρών επιχειρήσεων.</Text>
                <Text style={styles.dmlH3}>Υπηρεσία Απασχόλησης της Σλοβενίας</Text>
                <Text>Επιδοτήσεις αυτοαπασχόλησης / περιστασιακή παροχή επιχορηγήσεων για αυτοαπασχόληση.</Text>
                <Text style={styles.dmlH3}>Σλοβενικό Ταμείο Περιφερειακής Ανάπτυξης</Text>
                <Text>Χρηματοδοτικά κίνητρα, ιδίως υπό μορφή επιστρεφόμενων κεφαλαίων, για αρχικές επενδύσεις στον τομέα της επιχειρηματικότητας, της γεωργίας, της περιφερειακής ανάπτυξης, των
                    χρηματοοικονομικών επενδύσεων σε περιφερειακά καθεστώτα εγγυήσεων, των προχρηματοδοτήσεων με εγκεκριμένα ευρωπαϊκά κονδύλια.</Text>
                <Text style={styles.dmlH3}>SID banka</Text>
                <Text>Παροχή ευνοϊκών οικονομικών πόρων για επιχειρήσεις, ασφαλιστικές επιχειρήσεις εξαγωγών.</Text>
                <Text style={styles.dmlH3}>Eko sklad </Text>
                <Text>Παροχή ευνοϊκών οικονομικών πόρων για επενδύσεις σε περιβαλλοντικά προσανατολισμένα έργα και ενεργειακή απόδοση.</Text>

            </View>
        )
    }
}
