import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S31_5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Πολωνία:</Text>

                <Text style={styles.dmlH3}>Βήμα 5:</Text>
                <Text>Περιμένετε έως ότου η αίτηση λάβει θετική βαθμολογία (μπορεί να διαρκέσει έως και 30 ημέρες)</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_22_3319622.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
