import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για τη Σλοβενία: </Text>
                <Text style={styles.dmlH3}>Βήμα 5: </Text>
                <Text>Πραγματοποιήστε εγγραφή της αυτοαπασχολούμενης επιχείρησης. Υποβάλετε αίτηση για υποχρεωτική κοινωνική ασφάλιση και δώστε την συμπληρωμένη αίτηση στο γραφείο απασχόλησης εντός 30
                    ημερών μετά το εγκεκριμένο επιχειρηματικό σχέδιο.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
