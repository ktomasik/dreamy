import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για τη Σλοβενία: </Text>

                <Text style={styles.dmlH3}>Βήμα 4:</Text>
                <Text>Αφού ολοκληρώσετε με επιτυχία την εκπαίδευση, ακολουθήστε ένα εγκεκριμένο επιχειρηματικό σχέδιο και υπογράψτε ένα σχέδιο απασχόλησης στην Αρμόδια Υπηρεσία: θα πρέπει να
                    αυτο-απασχοληθείτε εντός 30 ημερών.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
