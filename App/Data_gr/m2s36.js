import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S36 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να ανταλλάξετε πληροφορίες από το Instagram στα άλλα μέσα κοινωνικής δικτύωσης</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 6: Για να συμπεριλάβετε αυτήν την ανάρτηση σε άλλα δίκτυα κοινωνικής δικτύωσης, ελέγξτε το απότυχε κάτω από το Μοιράσου με το Κοινή
                    χρήση. Τέλος, κάντε κλικ στην επιλογή Κοινή χρήση στην επάνω δεξιά γωνία. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img68.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
