import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28_6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Τουρκία::</Text>

                <Text style={styles.dmlH3}>Βήμα 6: </Text>
                <Text>Σε αυτό το στάδιο, η γυναίκα επιχειρηματίας παρουσιάζει το επιχειρησιακό της σχέδιο στην KOSGEB (η προετοιμασία του επιχειρηματικού σχεδίου γίνεται κατά τη διάρκεια της
                    εκπαίδευσης της KOSGEB).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
