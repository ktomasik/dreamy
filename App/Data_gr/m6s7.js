import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img20.png')
    }
}];

export default class M6S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργία ηλεκτρονικής υπογραφής</Text>
                <Text style={{marginTop: 15}}>Κατά τη μετάβαση από την έντυπη στην ηλεκτρονική μορφή αρχείων, αναδύονται νέες επαγγελματικές δυνατότητες. Η χρήση της ηλεκτρονικής υπογραφής στις
                    επιχειρηματικές διαδικασίες έντυπης μορφής, όπως τα τιμολόγια, οι συμβάσεις, τα μηνύματα ηλεκτρονικού ταχυδρομείου κ.λπ., είναι σημαντική για τη βελτίωση της επιχείρησής σας.
                    Υπάρχουν πολλές πλατφόρμες διαθέσιμες για κινητά τηλέφωνα σχετικά με την ηλεκτρονική υπογραφή. Μια από αυτές είναι το DocuSign. Το DocuSign είναι μια δωρεάν πλατφόρμα ηλεκτρονικής
                    υπογραφής, προσβάσιμη από κινητά τηλέφωνα. Είναι διαθέσιμη σε: iPhone, iPad, Android και Windows.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img20.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
