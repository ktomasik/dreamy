import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να πουλάτε το προϊόντα σας στις καλύτερες τιμές της αγοράς του διαδικτύου.</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 2: Ψάξτε για παρόμοια προϊόντα στην ίδια πλατφόρμα,</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/OAV9EA0.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
