import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text styl={{marginTop: 10}}>Με δεδομένη την αύξηση των χρηστών σε καθημερινή βάση, τα social media έχουν γίνει ένα αναπόσπαστο κομμάτι στο χώρο του Marketing σήμερα. Όσα ακολουθούν
                    είναι ένας οδηγός που θα σας βοηθήσει να ρυθμίσετε τους λογαριασμούς σας στα social media για επαγγελματική χρήση. </Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
