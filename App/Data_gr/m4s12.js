import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Έκδοση τοπικών αδειών </Text>
                <Text style={{marginTop: 10}}>Ακολουθήστε τις οδηγίες για να υποβάλετε αίτηση για άδειες: </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>ΧΩΡΟΣ ΕΓΚΑΤΑΣΤΑΣΗΣ
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>ΕΓΓΡΑΦΗ ΣΤΟ ΕΠΙΜΕΛΗΤΗΡΙΟ
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>ΕΓΓΡΑΦΗ ΣΤΟΝ ΕΦΚΑ
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>ΑΔΕΙΑ ΕΓΚΑΤΑΣΤΑΣΗΣ & ΛΕΙΤΟΥΡΓΙΑΣ
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>ΈΝΑΡΞΗ ΣΤΗΝ ΕΦΟΡΙΑ & ΕΙΔΙΚΕΣ ΑΔΕΙΕΣ/ΠΙΣΤΟΠΟΙΗΣΕΙΣ
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
