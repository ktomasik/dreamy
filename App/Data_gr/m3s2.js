import React, {Component} from 'react'
import {Text, Image, View, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Γιατί είναι τόσο σημαντικές οι εικόνες των προϊόντων;</Text>
                <Text>Είναι γνωστό ότι ζούμε στην εποχή της εικόνας. Όταν οι άνθρωποι σκέφτονται ή κάνουν κάτι, σχηματίζουν πάντα στο νου τους μια εικόνα γι’ αυτό. Και αυτό διότι, ο ανθρώπινος
                    εγκέφαλος τείνει να μετατρέπει μια αόριστη ιδέα σε κάτι διακριτό και συμπαγές. Συνεπώς, η οπτική παρουσίαση των προσφερόμενων προς πώληση χειροποίητων προϊόντων σας είναι
                    καθοριστική για την εμπορική τους επιτυχία.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 120}}
                    source={require('../Images/m3/img10.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img11.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 130}}
                    source={require('../Images/m3/img12.png')}
                    resizeMode="contain"/>

                <Text style={styles.dmlH2}>Πώς να πουλήσετε τα προϊόντα σας στο Instagram;</Text>
                <Text>Η χρησιμοποίηση μιας προσωπικής ιστοσελίδας για την πώληση των προϊόντων σας δεν αποτελεί πλέον μονόδρομο. Με τη ραγδαία ανάπτυξη που γνώρισε το ηλεκτρονικό εμπόριο, σήμερα τα
                    διαθέσιμα διαδικτυακά κανάλια πώλησης και διαφήμισης είναι περισσότερα από ποτέ. Με περισσότερους από 700 εκατομμύρια χρήστες, το Instagram μετατρέπεται με γρήγορους ρυθμούς σε
                    έναν εμπορικό κολοσσό. Περίπου το 80% των χρηστών του Instagram ακολουθούν τουλάχιστον μια επιχείρηση εντός της πλατφόρμας, ενώ το 60% από αυτούς αναφέρει πως το χρησιμοποιεί για
                    την ανακάλυψη νέων προϊόντων. </Text>
                <Image
                    style={{flex: 1, width: undefined}}
                    source={require('../Images/m3/img13.png')}
                    resizeMode="contain"/>
                <Text style={{marginTop: 10}}>Για να πουλήσετε το προϊόν σας στο Instagram καλό θα ήταν να λάβετε υπόψη σας τα παρακάτω:</Text>
                <FlatList
                    data={[
                        {key: 'Βεβαιωθείτε πως ο λογαριασμός σας στο Instagram είναι επαγγελματικός.'},
                        {key: 'Όλες οι φωτογραφίες και τα βίντεο που αναρτάτε να είναι υψηλής ποιότητας.'},
                        {key: 'Χρησιμοποιήστε αφήγηση για να κρατήσετε το ενδιαφέρον του κοινού σας.'},
                        {key: 'Φροντίστε οι διαφημίσεις σας να είναι δημιουργικές και πρωτότυπες.'},
                        {key: 'Χρησιμοποιήστε brand ambassadors και influencers για την προώθηση των προϊόντων σας.'},
                        {key: 'Διατηρήστε άμεση επικοινωνία με τους πελάτες σας.'},
                        {key: 'Βρείτε νέους πελάτες μέσω δημοφιλών και σχετικών hashtag.'},
                        {key: 'Να ενημερώνετε συχνά το σύνδεσμο (link) που οδηγεί στο προφίλ σας.'},
                        {key: 'Να προσφέρετε συχνά εκπτώσεις αποκλειστικά για τους ακολούθους σας στο Instagram.'},

                        ]}
                    renderItem={({item}) => <Text style={{marginBottom: 5, marginLeft: 12}}>{item.key}</Text>}
                />
                <Image
                    style={{flex: 1, width: 300, height: 480}}
                    source={require('../Images/m3/img14.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 400}}
                    source={require('../Images/m3/img15.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
