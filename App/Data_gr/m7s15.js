import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S15 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Επιχορηγήσεις μικρών επιχειρήσεων για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Γαλλία:</Text>
                <Text style={styles.dmlH3}>PRI (Περιφερειακές εταιρικές σχέσεις καινοτομίας)</Text>

                <FlatList
                    data={[
                        {key: {val1: '',val2: 'https://www.bpifrance.fr/Toutes-nos-solutions/Aides-concours-et-labels/Aides-a-l-innovation-projets-individuels/PRI-Faisabilite'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text>Δράση ανοιχτή σε καινοτόμες μικρο-μεσαίες επιχειρήσεις , ωστόσο αυτές δεν αποτελούν το βασικό κοινό στο οποίο η δράση κατά κανόνα απευθύνεται.
                    {"\n\n"}Διεξάγεται σε συνεργασία με 5 περιφέρειες: Grand Est (Alsace, Champagne-Ardenne, Lorraine), Hauts de France (Nord Pas de Calais Picardy), Aquitaine / Poitou Charentes, Pays de la Loire, PACA.
                    {"\n\n"}Επιλέγονται τα πιο καινοτόμα έργα, στα οποία θα χορηγηθεί επιχορήγηση 100.000 έως 200.000 € κατ' ανώτατο όριο ανά έργο. Αυτή η βοήθεια επιτρέπει την κάλυψη εξόδων που σχετίζονται με τις προκαταρκτικές μελέτες και την υλοποίηση του έργου. Καταβάλλεται σε 2 δόσεις (70% και 30%).
                    {"\n\n"}Τέλος, το έργο που θα επιλεγεί θα πρέπει να υλοποιηθεί μέσα σε διάστημα που δεν θα ξεπερνά τους 12 μήνες.
                </Text>
            </View>
        )
    }
}
