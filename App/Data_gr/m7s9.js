import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'


// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Αναζήτηση δάνειων μικρών επιχειρήσεων για τις γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Ελλάδα:</Text>
                <Text style={styles.dmlH3}>Τράπεζες:</Text>
                <Text>Για την χρηματοδότηση νέων επιχειρήσεων, οι τράπεζες ζητούν να δουν το επιχειρηματικό τους σχέδιο, το οποίο θα μελετηθεί από διάφορα τμήματα, που ελέγχουν διαφορετικά σημεία.
                    Είναι έτσι ιδιαίτερα σημαντικό το επιχειρηματικό σχέδιο να είναι όσο το δυνατόν πληρέστερο για να αποφευχθούν τυχόν καθυστερήσεις.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
