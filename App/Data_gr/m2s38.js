import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S38 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Δημιουργήστε ένα λογαριασμό στο Twitter </Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 1: Κατεβάστε το πρόγραμμα του Twitter από </Text>
                <Text>το Apple Store και ακολουθήστε τα παρακάτω βήματα. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img1.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 15}}>το Windows Store και ακολουθήστε τα παρακάτω βήματα. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img2.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 15}}>το πρόγραμμα του Twitter από το Google Play.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img3.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
