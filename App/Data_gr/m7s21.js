import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S21 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Φορολογικά κίνητρα για  γυναίκες σε νεοσύστατες επιχειρήσεις </Text>
                <Text style={styles.dmlH2}>Για την Πολωνία:</Text>
                <Text>Στην Πολωνία, δεν υπάρχουν ξεχωριστά φορολογικά κίνητρα για τις γυναίκες που ιδρύουν μια επιχείρηση.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_24_3319620.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
