import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S6 extends Component {
    render() {
        return (
            <View style={styles.section}>

                <Text style={styles.dmlH2}>Άνοιγμα τραπεζικού λογαριασμού</Text>
                <Text style={{marginTop: 15}}>Ακολουθήστε τις παρακάτω οδηγίες για να ανοίξετε έναν τραπεζικό λογαριασμό.
                    {"\n\n"}Εξετάστε τις επιλογές σας και μόλις εντοπίσετε τις ανάγκες σας, αξιολογήστε τις επιλογές σας:

                </Text>
                <FlatList
                    data={[
                        {key: 'Τρεχούμενος λογαριασμός'},
                        {key: 'Λογαριασμός ταμιευτηρίου'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5, fontWeight: 'bold'}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Μπορεί να έχετε επιλέξει ποιος τραπεζικός λογαριασμός σάς ταιριάζει καλύτερα αλλά θα πρέπει επίσης να βεβαιωθείτε ότι έχετε το δικαίωμα να ανοίξετε έναν
                    λογαριασμό. Πριν κατευθυνθείτε προς την τράπεζα, θα πρέπει να ελέγξετε αν πληρούνται όλα τα κριτήρια για το άνοιγμα ενός λογαριασμού.

                    {"\n\n"}Κατά γενικό κανόνα, οι τράπεζες απαιτούν τα εξής:
                    {"\n\n"}Έγκυρη ταυτοποίηση. Σε ορισμένες χώρες μπορεί επίσης να χρειαστείτε τον Αριθμό Μητρώου Κοινωνικής Ασφάλισης.
                    {"\n\n"}Ένα ελάχιστο χρηματικό ποσό για το άνοιγμα του λογαριασμού. Αυτό μπορεί να διαφέρει ανάλογα με την τράπεζα και τον λογαριασμό που επιλέγετε. Για παράδειγμα, ένας
                    λογαριασμός ταμιευτηρίου μιας μέσης τράπεζας απαιτεί ελάχιστη κατάθεση 300 €.

                    {"\n\n"}Επιλέξτε την τράπεζα που είναι καλύτερη για εσάς. Επικοινωνήστε με το υποκατάστημα της τράπεζας στην περιοχή σας, για να συζητήσετε τι ακριβώς θα λάβετε, αν ανοίξετε ένα
                    βασικό λογαριασμό. Παρ’ όλο που όλες οι τράπεζες είναι διαφορετικές, μπορούν γενικά να συνοψιστούν σε δύο γενικές κατηγορίες: μεγάλες αλυσίδες τραπεζών και μικρότερες, τοπικές.

                    {"\n\n"}Μεγάλες αλυσίδες τραπεζών: Οι μεγάλες τράπεζες έχουν συνήθως υποκαταστήματα στις περισσότερες πόλεις σε ολόκληρη τη χώρα. Μπορείτε έτσι να αποφύγετε τις χρεώσεις από τη
                    χρήση υπηρεσιών άλλων τραπεζών (όπως τα τέλη ATM κ.λπ.). Οι μεγάλες τράπεζες προσφέρουν επίσης υπηρεσίες, όπως 24ωρη γραμμή βοήθειας για τους πελάτες τους. Επιπλέον, οι τράπεζες
                    αυτές τείνουν να έχουν μια σταθερή και αξιόπιστη φήμη.

                    {"\n\n"}Μικρότερες τοπικές τράπεζες: Οι μικρές τράπεζες προσφέρουν μια πιο προσωπική, φιλική κι ανθρώπινη εμπειρία. Επίσης, χρεώνουν μικρότερα τέλη για τη χρήση των υπηρεσιών τους.
                    Οι μικρότερες τράπεζες επενδύουν συχνά τα χρήματά τους στην τοπική κοινότητα. Από την άλλη πλευρά, αποτυγχάνουν συχνότερα από τις μεγάλες τράπεζες (αυτό είναι ακόμα πολύ σπάνιο).

                    {"\n\n"}Επιπλέον, μια άλλη επιλογή για τραπεζικές εργασίες είναι τα πιστωτικά ιδρύματα. Πρόκειται για μη-κερδοσκοπικά χρηματοπιστωτικά ιδρύματα, συχνά με την ιδιότητα να είναι
                    «κοινωνικά προσανατολισμένα» και να «εξυπηρετούν τους ανθρώπους», παρά να είναι κερδοφόρα. Έχουν καταφέρει με επιτυχία να κάνουν τις υπηρεσίες τους πιο προσιτές μέσω της
                    συνεργασίας τους με άλλες πιστωτικές ενώσεις, για να προσφέρουν κοινά τραπεζικά υποκαταστήματα και ΑΤΜ.

                    {"\n\n"}Επισκεφθείτε την τράπεζά σας και ζητήστε να ανοίξετε ένα λογαριασμό. Το άνοιγμα λογαριασμού αυτοπροσώπως είναι συνήθως η καλύτερη επιλογή για τους κατόχους λογαριασμών την
                    πρώτη φορά. Μπορείτε να συζητήσετε με τον υπάλληλο της τράπεζας όλες τις ερωτήσεις και τις αμφιβολίες που τυχόν έχετε και να λάβετε άμεσες απαντήσεις. Επίσης, η διαδικασία
                    ανοίγματος ενός λογαριασμού επιτόπου είναι συνήθως πιο γρήγορη.

                    {"\n\n"}Διατυπώστε όλες τις σημαντικές ερωτήσεις, προτού ολοκληρώσετε τον λογαριασμό σας. Ζητήστε διευκρινίσεις για τυχόν προβλήματα σχετικά με το λογαριασμό σας.

                    {"\n\n"}Δώστε τις απαραίτητες πληροφορίες για τη δημιουργία του λογαριασμού σας. Το άνοιγμα ενός λογαριασμού ταμιευτηρίου απαιτεί κάποιες βασικές προσωπικές πληροφορίες. Γενικά,
                    είναι καλή ιδέα να έχετε:
                    {"\n\n"}Δελτίο ταυτότητας: Να έχετε το δελτίο ταυτότητας μαζί σας (μπορεί επίσης να αρκεί η άδεια οδήγησης ή το διαβατήριο).

                    {"\n\n"}Απόδειξη της διεύθυνσης κατοικίας: Ένας λογαριασμός τηλεφώνου ή οποιοδήποτε άλλο επίσημο έγγραφο με το όνομα και τη διεύθυνσή σας συνήθως αρκεί.

                    {"\n\n"}Α.Φ.Μ & Α.Μ.Κ.Α: Η τράπεζα θα ζητήσει τον αριθμό φορολογικού μητρώου (ΑΦΜ), τον αριθμό μητρώου κοινωνικής ασφάλισης (ΑΜΚΑ), ή τον αριθμό αναγνώρισης του εργοδότη, για να
                    βεβαιωθεί ότι είστε "εγγεγραμμένοι" στα κρατικά αρχεία.

                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Διατηρήστε ασφαλή τα έγγραφα λογαριασμού που θα λάβετε. </Text> Όταν ολοκληρώσετε την αίτηση του λογαριασμού σας, θα λάβετε έγγραφα που
                    περιέχουν σημαντικές πληροφορίες σχετικά με αυτόν.
                    Κρατήστε τα
                    σε ασφαλές μέρος. Εάν μπορείτε, καλό είναι να απομνημονεύσετε τις παρακάτω πληροφορίες, ώστε να μην χρειάζεται να βασίζεστε στα έγγραφα αυτά στο μέλλον:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Τετραψήφιος αριθμός PIN: ',
                                val2: 'Χρειάζεται όταν χρησιμοποιήσετε τη χρεωστική σας κάρτα για αγορές και ανάληψη μετρητών.'
                            }
                        },
                        {
                            key: {
                                val1: 'Αριθμός τραπεζικού λογαριασμού: ',
                                val2: 'Χρειάζεται για διάφορες οικονομικές συναλλαγές.'
                            }
                        },
                        {
                            key: {
                                val1: 'Αριθμός κοινωνικής ασφάλισης: ',
                                val2: 'Χρειάζεται για διάφορες φορολογικές και οικονομικές εργασίες στο μέλλον.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
