import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz7": {
            "question1": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Όσο χαμηλότερο είναι το επιτόκιο, τόσο πιο συμφέρον είναι να πάρει κανείς ένα δάνειο."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σε μία τράπεζα.",
                    "option2": "b) Σε συνεταιριστική τράπεζα του Επιμελητηρίου(εφόσον υπάρχει).",
                    "option3": "c) Σε χρηματοπιστωτικές υπηρεσίες.",
                    "option4": "d) Σε επιχειρηματικό κέντρο επενδύσεων."
                },
                "question": "Πού μπορεί μια μικρή επιχείρηση να βρει την καλύτερη δανειακή συμφωνία;"
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Οι επιχορηγήσεις είναι κεφάλαια, που πρέπει να επιστραφούν σε οργανισμούς του δημοσίου τομέα"
            },
            "question4": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Τα κρατικά κίνητρα για τις νεοσύστατες επιχειρήσεις είναι αποκλειστικά μη χρηματοοικονομικού χαρακτήρα"
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Προώθηση ιδιωτικών επενδύσεων.",
                    "option2": "b) Υποστήριξη για τη δημιουργία και την ανάπτυξη πολύ μικρών επιχειρήσεων.",
                    "option3": "c) Παροχή ευνοϊκών οικονομικών πόρων για επιχειρήσεις.",
                    "option4": "d) Χρηματοδοτικά κίνητρα σε περιφερειακά σχήματα εγγυοδοσίας."
                },
                "question": "Ποια είναι η πιο συνηθισμένη τραπεζική υποστήριξη των μικρών γυναικείων επιχειρήσεων;"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Ένα φορολογικό κίνητρο είναι κυβερνητικό μέτρο ενθάρρυνσης των ατόμων να εξοικονομήσουν χρήματα μειώνοντας το ποσό του φόρου που πρέπει να πληρώσουν"
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Μικροπίστωση.",
                    "option2": "b) Ομπρέλα χρηματοδότησης.",
                    "option3": "c) Μικροδάνειο.",
                    "option4": "d) Κανένα από τα παραπάνω."
                },
                "question": "Ποιο από τα παρακάτω σχετίζεται με τη μικροχρηματοδότηση;"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η μικροχρηματοδότηση και το κοινωνικό κεφάλαιο σχετίζονται σε μεγάλο βαθμό με την προώθηση της επιχειρηματικότητας."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Οι επιχειρηματικοί άγγελοι ενδιαφέρονται γενικά να συμμετάσχουν στην πρωτοβουλία ενεργώντας ως οδηγοί ή μέντορες."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Πίστωση.",
                    "option2": "b) Ισολογισμός.",
                    "option3": "c) Δανειακό κεφάλαιο.",
                    "option4": "d) Μικροπίστωση."
                },
                "question": "____________ ονομάζεται όταν δανείζεστε χρήματα από μια τράπεζα ή άλλο δανειστή."
            }
        }
    }
};

export default class M7Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz7;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
