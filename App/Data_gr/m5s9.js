import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Φόρμα πρώτης παραγγελίας</Text>
                <Text style={{marginTop: 15}}>Ακολουθήστε τις παρακάτω οδηγίες για να δημιουργήσετε τη φόρμα πρώτης παραγγελίας.
                    {"\n\n"}Πώς να φτιάξετε ένα απλό έντυπο παραγγελίας για ηλεκτρονικές πληρωμές.

                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Για να λάβετε παραγγελίες: </Text>θα πάρετε μια ιδέα για το πώς να δημιουργήσετε μια φόρμα παραγγελίας στο WordPress που θα δέχεται
                    πληρωμές με πιστωτικές κάρτες και PayPal.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Βήμα 1: Δημιουργήστε μια απλή φόρμα παραγγελίας στο WordPress
                    {"\n\n"}Εγκαταστήστε και ενεργοποιήστε την προσθήκη WPForms.
                </Text>
                <FlatList
                    data={[
                        {key: 'Πηγαίνετε στο WPForms -» Προσθήκη Νέας για να δημιουργήσετε μια νέα φόρμα.'},
                        {key: 'Ονομάστε τη φόρμα σας και επιλέξτε το πρότυπο φόρμας χρέωσης / παραγγελίας.'},
                        {key: 'Κυλίστε προς τα κάτω στην ενότητα "Διαθέσιμα στοιχεία" στην οθόνη προεπισκόπησης στα δεξιά και κάντε κλικ σε αυτήν.'},
                        {key: 'Αυτό θα ανοίξει τις "Επιλογές πεδίου" στον αριστερό πίνακα. Εδώ μπορείτε να μετονομάσετε το πεδίο, να προσθέσετε ή να αφαιρέσετε στοιχεία παραγγελιών και να αλλάξετε τις τιμές.'},
                        {key: 'Εάν θέλετε να δώσετε στους χρήστες, εικόνες τις οποίες μπορούν να επιλέξουν κατά την συμπλήρωση της φόρμας παραγγελίας σας, κάντε κλικ στο πλαίσιο ελέγχου “Χρήση επιλογών εικόνας” στον επεξεργαστή της φόρμας.'}, {key: 'Τέλος, μπορείτε να προσθέσετε επιπλέον πεδία στη φόρμα παραγγελίας, σύροντάς τα από την αριστερή προς τη δεξιά πλευρά.'},
                        {key: 'Όταν ολοκληρώσετε, κάντε κλικ στην επιλογή “Αποθήκευση”.'},


                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Βήμα 2: Ρυθμίστε τις ειδοποιήσεις στη φόρμα παραγγελίας</Text>
                <FlatList
                    data={[
                        {key: 'Οι ειδοποιήσεις μέσω ηλεκτρονικού ταχυδρομείου είναι ένας πολύ καλός τρόπος επικοινωνίας όταν υποβληθεί η φόρμα.'},
                        {key: 'Μπορείτε να στείλετε μια τέτοια ειδοποίηση στον εαυτό σας ή σ’ ένα μέλος της ομάδας σας, προσθέτοντας τη διεύθυνση ηλεκτρονικού ταχυδρομείο στο πεδίο “Αποστολή” ή σε έναν πελάτη, για να τους ενημερώσετε ότι η παραγγελία τους έχει καταχωρηθεί.'},
                        {key: 'Κάντε κλικ στην καρτέλα “Settings” (Ρυθμίσεις) στο εργαλείο δημιουργίας φόρμας και, στη συνέχεια, κάντε κλικ στην επιλογή “Ειδοποιήσεις”.'},
                        {key: 'Κάντε κλικ στην επιλογή “Εμφάνιση έξυπνων ετικετών” στο πεδίο “Αποστολή” προς διεύθυνση ηλεκτρονικού ταχυδρομείου.'},
                        {key: 'Κάντε κλικ στο “Email”'},
                        {key: 'Αλλάξτε το θέμα ηλεκτρονικού ταχυδρομείου της ειδοποίησής σας, ώστε να είναι πιο συγκεκριμένο. Επιπλέον, μπορείτε να προσαρμόσετε τα μηνύματα "Ανά Όνομα", "Ανά Email" και "Απάντηση σε".'},
                        {key: 'Συμπεριλάβετε ένα εξατομικευμένο μήνυμα, εάν το μήνυμα μέσω ηλεκτρονικού ταχυδρομείου απευθύνεται σε οποιονδήποτε άλλο εκτός από τον εαυτό σας.'},
                        {key: 'Χρησιμοποιήστε την έξυπνη ετικέτα {all fields}, εάν θέλετε να συμπεριλάβετε όλες τις πληροφορίες που υπάρχουν στα πεδία φόρμας της υποβληθείσας φόρμας παραγγελίας.'},


                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Βήμα 3: Ρυθμίστε τις φόρμες επιβεβαίωσης της παραγγελίας</Text>
                <Text>Οι φόρμες επιβεβαίωσης είναι μηνύματα που εμφανίζονται στους πελάτες μόλις υποβάλλουν μια φόρμα παραγγελίας.
                    {"\n\n"}Υπάρχουν τρεις τύποι επιβεβαίωσης από τους οποίους μπορείτε να επιλέξετε:

                </Text>
                <FlatList
                    data={[
                        {key: '1.	Μήνυμα. Όταν ένας πελάτης υποβάλλει παραγγελίας, θα λάβει ένα απλό μήνυμα επιβεβαίωσης, ενημερώνοντάς τον ότι η παραγγελία του έχει υποστεί επεξεργασία.'},
                        {key: '2.	Εμφάνιση σελίδας. Αυτός ο τύπος επιβεβαίωσης θα μεταφέρει τους πελάτες σε μια συγκεκριμένη ιστοσελίδα στον ιστότοπό σας, ευχαριστώντας τους για την παραγγελία τους.'},
                        {key: '3.	Μετάβαση σε διεύθυνση URL (Ανακατεύθυνση). Αυτή η επιλογή χρησιμοποιείται όταν θέλετε να στείλετε πελάτες σε διαφορετικό ιστότοπο.'},


                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Ας δούμε πώς μπορείτε να διαμορφώσετε μια απλή φόρμα επιβεβαίωσης στο WPForms, ώστε να μπορείτε να προσαρμόσετε το μήνυμα που θα δουν οι χρήστες μετά την
                    υποβολή των παραγγελιών τους.</Text>
                <FlatList
                    data={[
                        {key: 'Κάντε κλικ στην καρτέλα “Επιβεβαίωση” στον επεξεργαστή φόρμας κάτω από τις Ρυθμίσεις.'},
                        {key: 'Επιλέξτε τον τύπο του τρόπου επιβεβαίωσης που θέλετε να λαμβάνουν οι πελάτες σας.'},
                        {key: 'Προσαρμόστε το μήνυμα επιβεβαίωσης σύμφωνα με τις προτιμήσεις σας και κάντε κλικ στην επιλογή “Αποθήκευση” όταν τελειώσετε.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Βήμα 4: Διαμορφώστε τις Συνθήκες Πληρωμής</Text>
                <Text>Το WPForms συγχρονίζεται με το PayPal για την αποδοχή πληρωμών.
                    {"\n\n"}Για να διαμορφώσετε τις ρυθμίσεις πληρωμών στη φόρμα παραγγελίας, θα πρέπει πρώτα να εγκαταστήσετε και να ενεργοποιήσετε το κατάλληλο πρόσθετο πληρωμής.
                    {"\n\n"}Μόλις το κάνετε αυτό:

                </Text>
                <FlatList
                    data={[
                        {key: 'Κάντε κλικ στην καρτέλα “Πληρωμές” στον επεξεργαστή φόρμας.'},
                        {key: 'Πατήστε “PayPal”,'},
                        {key: 'Πληκτρολογήστε τη διεύθυνση ηλεκτρονικού ταχυδρομείου του PayPal σας,'},
                        {key: 'Επιλέξτε τη λειτουργία “Παραγωγή”,'},
                        {key: 'Επιλέξτε “Προϊόντα και Υπηρεσίες”,'},
                        {key: 'Διαμορφώστε τις συνθήκες πληρωμής,'},
                        {key: 'Πατήστε την επιλογή “Αποθήκευση” για να αποθηκεύσετε τις αλλαγές σας.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Τώρα είστε έτοιμοι να προσθέσετε την απλή φόρμα παραγγελίας στον ιστότοπο σας.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Βήμα 5: Προσθέστε την απλή φόρμα παραγγελίας στον ιστότοπο σας</Text>
                <FlatList
                    data={[
                        {key: 'Δημιουργήστε μια νέα δημοσίευση ή μια σελίδα στο WordPress και στη συνέχεια κάντε κλικ στο κουμπί “Προσθήκη φόρμας”.'},
                        {key: 'Επιλέξτε την απλή φόρμα παραγγελίας από το αναπτυσσόμενο μενού και κάντε κλικ στην επιλογή “Προσθήκη φόρμας”.'},
                        {key: 'Δημοσιεύστε την ανάρτηση ή τη σελίδα σας έτσι ώστε η φόρμα παραγγελίας σας να εμφανίζεται στον ιστότοπο σας.'},
                        {key: 'Μεταβείτε στην “Εμφάνιση-»Widgets” και προσθέστε ένα γραφικό στοιχείο WPForms στην πλαϊνή σας μπάρα.'},
                        {key: 'Επιλέξτε τη φόρμα χρέωσης /παραγγελίας από το αναπτυσσόμενο μενού.'},
                        {key: 'Πατήστε “Αποθήκευση”.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Τώρα μπορείτε να δείτε τη δημοσιευμένη φόρμα παραγγελίας απευθείας στον ιστότοπο σας. Παρατηρήστε ότι όταν επιλέγετε στοιχεία στη φόρμα σας, οι τιμές
                    αλλάζουν αυτόματα.
                    {"\n\n"}Τώρα γνωρίζετε πώς να δημιουργήσετε μια απλή φόρμα παραγγελίας στο WordPress, η οποία θα δέχεται ηλεκτρονικές πληρωμές.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', marginTop: 15}}>Αν θέλετε να στείλετε παραγγελίες ως επιχείρηση, μπορείτε να χρησιμοποιήσετε τη φόρμα παραγγελίας των Εγγράφων
                    Google (Google Docs).</Text>
                <Text>Τα Έγγραφα Google σάς επιτρέπουν να δημιουργείτε φόρμες που μπορούν να χρησιμοποιηθούν ως φόρμα παραγγελίας. Μετά την αποθήκευση των δεδομένων στο υπολογιστικό φύλλο σας στα
                    Έγγραφα Google, μπορείτε να χειριστείτε τη χρέωση ή την παραγγελία από εκεί.</Text>
                <FlatList
                    data={[
                        {key: '1.	Ανοίξτε τα Έγγραφα Google και κάντε κλικ στο κουμπί “Δημιουργία”. Επιλέξτε “Φόρμα”.'},
                        {key: '2.	Συμπληρώστε το όνομα και την περιγραφή της φόρμας παραγγελίας.'},
                        {key: '3.	Χωρίστε το έντυπό σας σε τμήματα, εάν είναι απαραίτητο.'},
                        {key: '4.	Προσθέστε ερωτήσεις κάνοντας κλικ στο “Προσθήκη στοιχείου”.'},
                        {key: '5.	Επιλέξτε ένα θέμα για τη φόρμα σας κάνοντας κλικ στο κουμπί “Θέμα” δίπλα στο κουμπί “Προσθήκη στοιχείου”.'},
                        {key: '6.	Κάντε κλικ στο σύνδεσμο στο κάτω μέρος του παραθύρου φόρμας, για να δείτε τη φόρμα σας στο πρόγραμμα περιήγησης.'},
                        {key: '7.	Διανείμετε τη φόρμα σας.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
