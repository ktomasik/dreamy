import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S13_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 12: Αν θέλετε, μπορείτε να επιλέξετε να επιτρέψετε στο Facebook να έχει πρόσβαση στις φωτογραφίες σας και στα άλλα προσωπικά σας
                    δεδομένα. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img21.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
