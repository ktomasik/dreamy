import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S31 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να ανταλλάξετε πληροφορίες από το Instagram στα άλλα μέσα κοινωνικής δικτύωσης</Text>
                <Text style={{marginTop: 10}}>Στην επόμενη άσκηση θα δείξουμε:</Text>
                <FlatList
                    data={[

                        {key: 'πώς να δημοσιεύσετε ένα περιεχόμενο'},
                        {key: 'πώς να μοιραστείτε το περιεχόμενο με τα άλλα μέσα κοινωνικής δικτύωσης και'},
                        {key: 'πώς να προσθέσετε ένα περιεχόμενο σε σχετικό θέμα.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 3, marginLeft: 15}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>
                    Βήμα 1: Για να δημοσιεύσετε νέο περιεχόμενο πιέστε το κουμπί της φωτογραφικής μηχανής στο κάτω μέρος της οθόνης.
                </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img59.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
