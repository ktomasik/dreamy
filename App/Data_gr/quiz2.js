import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View, Alert} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz2": {
            "question1": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Το ψάρεμα θαλασσινών με μεγάλα δίχτυα.",
                    "option2": "b) Ένας τεχνικός, ο οποίος επισκευάζει το σύστημα του δικτύου.",
                    "option3": "c) Μία ομάδα ατόμων που ανταλλάσσουν πληροφορίες. ",
                    "option4": "d) Ένα είδος δημόσιας υπηρεσίας, που βοηθάει τους πολίτες."
                },
                "question": "Τι είναι η δικτύωση;"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η δημιουργία λογαριασμών κοινωνικών μέσων δικτύωσης διαφέρει ανάλογα με τις διάφορες μάρκες έξυπνων κινητών τηλεφώνων (smartphones)."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Μία ομάδα ατόμων που βλέπουν έναν αγώνα ποδοσφαίρου.",
                    "option2": "b) Μια τεχνολογία, που βασίζεται στον υπολογιστή για τη δημιουργία εικονικών δικτύων και την ανταλλαγή πληροφοριών.",
                    "option3": "c) Μια κοινή τεχνολογία (π.χ. τηλεόραση, ραδιόφωνο), όπου μπορούν να συγκεντρωθούν τα κοινωνικά νέα.",
                    "option4": "d) Ένα ιδιωτικό κανάλι τηλεόρασης για να παρακολουθήσετε ταινίες."
                },
                "question": "Τι είναι τα κοινωνικά μέσα δικτύωσης;"
            },
            "question4": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Instagram",
                    "option2": "b) YouTube",
                    "option3": "c) Facebook",
                    "option4": "d) Google",

                },
                "question": "Ποιο από τα παρακάτω δεν ανήκει στα μέσα κοινωνικής δικτύωσης;"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Τακτική αποστολή διαφημίσεων – κάθε μέρα.",
                    "option2": "b) Δημοσίευση όσον το δυνατόν περισσότερου περιεχομένου, για να είναι το κανάλι ζωντανό.",
                    "option3": "c) Το να απαντάς στα σχόλια των ανθρώπων.",
                    "option4": "d) Αποκλεισμός ατόμων από τα κοινωνικά μέσα δικτύωσης."
                },
                "question": "Ποια είναι η καλύτερη τακτική για τη διατήρηση ενός κοινού;"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Σε ορισμένους ιστότοπους κοινωνικών μέσων δικτύωσης, τα μέλη δεν μπορούν να δημιουργήσουν σελίδες βασισμένες σε θέματα."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Να υπάρχουν σαφείς και καθορισμένοι στόχοι μάρκετινγκ.",
                    "option2": "b) Να υπάρχουν αρκετά χρήματα για να ανοίξει μια επιχείρηση.",
                    "option3": "c) Η διασφάλιση καλής ποιότητας προϊόντων.",
                    "option4": "d) Οι διαφημίσεις."
                },
                "question": "Τι είναι το πιο σημαντικό στοιχείο στο κοινωνικό μάρκετινγκ και στην επιτυχία μιας επιχείρησης;"
            },
            "question8": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Η παροχή δωρεάν χώρου στον παγκόσμιο ιστό.",
                    "option2": "b) Το ότι ενθαρρύνουν τα μέλη τους να ανεβάσουν περιεχόμενο.",
                    "option3": "c) Το ότι επιτρέπουν τις ζωντανές συζητήσεις.",
                    "option4": "d) Όλα τα παραπάνω."
                },
                "question": "Ποιο από τα παρακάτω αποτελεί κοινό χαρακτηριστικό των κοινωνικών μέσων δικτύωσης;"
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η ίδια διεύθυνση ηλεκτρονικού ταχυδρομείου μπορεί να χρησιμοποιηθεί για την εγγραφή σε διαφορετικούς λογαριασμούς κοινωνικών μέσων δικτύωσης"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Η διεύθυνση του ηλεκτρονικού ταχυδρομείου ",
                    "option2": "b) Η μηχανή αναζήτησης",
                    "option3": "c) Τα κοινωνικά μέσα δικτύωσης",
                    "option4": "d) Η ανεπιθύμητη αλληλογραφία"
                },
                "question": "_______ συμβάλλει/συμβάλλουν στη γνωστοποίηση των προδιαγραφών των προϊόντων, στη διαφήμιση των προϊόντων και στην απευθείας επικοινωνία των ανθρώπων μέσω διαδικτύου."
            }
        }
    }
};

export default class M2Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz2;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                            <AwesomeAlert
                                show={showAlertTrue}
                                showProgress={false}
                                title={I18n.t('_correct')}
                                message={I18n.t('_correctFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="green"
                                onConfirmPressed={() => {
                                    this.hideAlert(); this._next();
                                }}
                                titleStyle={{color: 'green', fontWeight: 'bold'}}
                            />
                            <AwesomeAlert
                                show={showAlertFalse}
                                showProgress={false}
                                title={I18n.t('_incorrect')}
                                message={I18n.t('_incorrectFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="#DD6B55"
                                onConfirmPressed={() => {
                                    this.hideAlert(); this._next();
                                }}
                                titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
