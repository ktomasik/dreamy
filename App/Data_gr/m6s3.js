import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img1.png')
    }
}];

export default class M6S3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Τιμολόγηση στο κινητό</Text>
                <Text style={styles.dmlH3}>Για λογισμικό iOS</Text>
                <Text>Η δωρεάν έκδοση του Invoice Simple είναι διαθέσιμη σε iPhone, iPad και iPod Touch και μπορεί να χρησιμοποιηθεί για τη δημιουργία τριών δωρεάν τιμολογίων ή υπολογισμών στο κινητό
                    ή τις φορητές συσκευές σας.</Text>
                <Text style={{fontSize: 18, marginTop: 10}}><Text style={{fontWeight: 'bold'}}>Βήμα 1:</Text> Μεταβείτε στο App Store και αναζητήστε "τιμολόγιο" (ανατρέξτε στις ενότητες 1 και 2 για να
                    δείτε πώς να μεταβείτε και πώς να κάνετε αναζήτηση)</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
