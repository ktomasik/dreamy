import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Τρόποι Αποστολής για τον πελάτη / Παράδοση παραγγελίας</Text>
                <Text style={{marginTop: 10}}>Η αποστολή και η παράδοση μιας παραγγελίας είναι μείζονος σημασίας για της επιχειρήσεις λιανικού εμπορίου, εξαιτίας της προσδοκίας των πελατών να λάβουν
                    την παραγγελία τους εύκολα και γρήγορα. Μερικές από τις επιλογές που έχουν οι πελάτες παρουσιάζονται παρακάτω (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Παράδοση αυθημερόν</Text>
                <Text>Η παράδοση αυθημερόν είναι μια υπηρεσία όπου παραγγέλλεις online και λαμβάνεις το δέμα την ίδια ημέρα, το οποίο είναι ένα είδος ταχείας εξυπηρέτησης. (Ufford 2018). </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Παραλαβή από το κατάστημα</Text>
                <Text>Η παραλαβή από το κατάστημα επιτρέπει στους πελάτες να αγοράζουν αντικείμενα online, να τα ελέγχουν και να τα παραλαμβάνουν εντός καθορισμένου χρονικού πλαισίου σε ένα τοπικό
                    κατάστημα ή συγκεκριμένη τοποθεσία. (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Αποστολή από το απόθεμα</Text>
                <Text>Η αποστολή από το απόθεμα είναι μια υπηρεσία με την οποία οι επιχειρήσεις λιανικής μετατρέπουν τις αποθήκες τους σε κέντρο ολοκληρωμένης διεκπεραίωσης που φιλοξενεί και το φυσικό
                    κατάστημα και το ηλεκτρονικό. (Ufford 2018). </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Προγραμματισμένη παράδοση</Text>
                <Text>Η προγραμματισμένη παράδοση είναι μια υπηρεσία, κατά την οποία οι εταιρείες αποστολής προσφέρουν στους πελάτες τους την επιλογή να ορίσουν παράδοση της παραγγελίας τους μέσα
                    σ’ ένα συγκεκριμένο χρονικό πλαίσιο, με μια μικρή επιβάρυνση ή και δωρεάν, αν οι πελάτες αυτοί είναι σταθεροί. (Ufford 2018).</Text>

                <Text style={styles.dmlH3}> Χρεώσεις</Text>
                <Text>Ένα από τα αγχωτικά σημεία στην αρχή των πωλήσεων είναι η χρέωση των πελατών. Εκτός από τις δαπάνες του προϊόντος κατά τη διάρκεια της παραγωγής του, ενδέχεται να υπάρχουν και
                    ορισμένα άλλα κόστη που πρέπει να ληφθούν υπόψη, προκειμένου να τραβήξουν την προσοχή των πελατών. Τα έξοδα αποστολής υπολογίζονται από αυτή την επιλογή.
                    {"\n\n"}Για να δοθεί έμφαση στη μείωση της τιμής του προϊόντος, τα έξοδα μεταφοράς μπορούν να αναγράφονται χωριστά. Κατά τη διάρκεια των πωλήσεων, μπορεί να δοθεί τιμή χωρίς έξοδα
                    αποστολής, όταν τα έξοδα αυτά θα καλυφθούν από τους πελάτες. Σε αυτό το σημείο, η προτίμηση της μεταφορικής εταιρείας και των όρων αποστολής μπορούν να οριστούν από τον πελάτη και,
                    ανάλογα με τις ανάγκες του, θα προχωρήσει και η αποστολή. Από την άλλη πλευρά, η τιμή του προϊόντος και το κόστος αποστολής που καθορίζεται από την μεταφορική εταιρεία μπορούν να
                    αναφέρονται ξεχωριστά.
                    {"\n\n"}Η χρέωση του κόστους αποστολής στην τιμή πώλησης μπορεί να προσφέρει ανταγωνιστικές τιμές για το προϊόν, κάτι που τραβά το ενδιαφέρον των πελατών.

                </Text>

                <Text style={styles.dmlH3}>Δωρεάν αποστολή</Text>
                <Text>Η Δωρεάν Αποστολή (ουσιαστικά μόνο για παραγγελίες εσωτερικού) είναι μια αποδεδειγμένη μέθοδος να τραβήξουμε την προσοχή των πελατών και να αυξήσουμε την αλληλεπίδραση. Αλλά σε
                    ρεαλιστική βάση, η παροχή αυτή μειώνει κατά πολύ το τελικό κέρδος. (Anonymous 2018b).
                    {"\n\n"}Αν η Δωρεάν Αποστολή προσφέρεται για οποιαδήποτε ποσότητα, διάσταση και αριθμό παραγγελιών θα πρέπει να γνωρίζουμε ποιο είναι το πραγματικό κόστος αποστολής, πώς οι
                    ανταγωνιστές διαχειρίζονται τις αποστολές αλλά και τί περιθώριο κέρδους υπάρχει. Αυτές είναι οι παράμετροι που θα βοηθήσουν να αποφασίσετε αν τελικά θα προχωρήσετε σε Δωρεάν
                    Αποστολή. Μια άλλη επιλογή είναι να ορίσετε ελάχιστο αριθμό παραγγελιών πάνω από τον μέσο όρο της αξίας της παραγγελίας ή να ορίσετε έναν ελάχιστο αριθμό προϊόντων ανά παραγγελία.
                    (Anonymous 2018b).
                    {"\n\n"}ή να αποδειχθείτε φθηνότεροι από τον ανταγωνιστή σας στα έξοδα αποστολής, να βελτιώσετε την επικοινωνία (με τους πελάτες), να αυξήσετε τον μέσο όρο αξίας της παραγγελίας
                    και να περιορίσετε έτσι τις παραγγελίες που δεν ολοκληρώθηκαν. Από την άλλη πλευρά, οι υψηλές τιμές των προϊόντων που θα καλύπτουν τα έξοδα αποστολής θα πρέπει να είναι
                    ανταγωνιστικές σε σχέση με ό,τι αντίστοιχο κυκλοφορεί στην αγορά. (Anonymous 2018b).

                </Text>
            </View>
        )
    }
}
