import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S44_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Twitter</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 13: Εισάγετε τα αγαπημένα σας ενδιαφέροντα… και πατήστε Επόμενο. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img89.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
