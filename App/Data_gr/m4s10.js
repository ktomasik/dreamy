import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Φορολογικές Υποχρεώσειςs</Text>
                <Text style={{marginTop: 10}}>Ακολουθήστε τις οδηγίες για την εκπλήρωση των φορολογικών υποχρεώσεων: </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Κάνετε αίτηση στην εφορία (του τόπου διαμονής σας)
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Λάβετε πιστοποιητικό φοροαπαλλαγής
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
