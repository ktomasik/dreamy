import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S61_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα ιστολόγιο ( Blog )</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 12: Επιλέξτε ξανά το Λογαριασμό σας στη Google,  πληκτρολογήστε τον *κωδικό* πρόσβασής σας  και κάντε κλικ στο κουμπί Επόμενο. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img128.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img129.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
