import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Μικροχρηματοδότηση κι άλλα Κεφάλαια Εκκίνησης για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Γαλλία:</Text>
                <Text>Η μικροχρηματοδότηση αποτελεί ένα δάνειο, γενικά μικρότερο των 25.000 ευρώ, που προορίζεται για άτομα που επιθυμούν να δημιουργήσουν ή να αναλάβουν μια επιχείρηση, αλλά των
                    οποίων οι πόροι είναι ανεπαρκείς, ώστε να δικαιούνται ένα συμβατικό δάνειο. Για να επωφεληθεί από μια μικροπίστωση, ο οφειλέτης πρέπει να διαθέτει ένα εξειδικευμένο και ικανό
                    δίκτυο υποστήριξης, όπως: το "France Active", το "France Initiative", το "Boutiques de Gestion" ή το "Τhe Fondation of 2e Chance". Αυτά τα δίκτυα θα τον βοηθήσουν να οργανώσει το
                    έργο του, να διερευνήσει το αίτημά του για χρηματοδότηση και να αναπτύξει τη δραστηριότητά του. Ο κύριος φορέας που εμπλέκεται στη μικροχρηματοδότηση είναι η ADIE (Ένωση για την
                    Ανάπτυξη της Οικονομικής Πρωτοβουλίας).</Text>

                <Text style={styles.dmlH3}>ADIE</Text>
                <Text> Ευαισθητοποιεί, καθοδηγεί και ενημερώνει τις γυναίκες για τη δημιουργία επιχειρήσεων, αφού οργανώνει από το 2015 εκστρατεία ετήσιας ευαισθητοποίησης.
                    {"\n\n"}Προωθεί τη χρηματοδότηση φορέων έργων, με συνοδευόμενη μικροπίστωση, για επιχειρήσεις που δεν έχουν πρόσβαση σε τραπεζικές πιστώσεις.
                    {"\n\n"}Ενισχύει την υποστήριξη των δημιουργών επιχειρήσεων, με ενότητες εκπαίδευσης και ευαισθητοποίησης προσαρμοσμένες στις ιδιαιτερότητές τους.
                </Text>
                <FlatList
                    data={[
                        {key: 'https://www.adie.org/nos-actions/pour-les-femmes'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />

                <Text style={styles.dmlH3}>FRANCE ACTIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Η France Active υποστηρίζει και χρηματοδοτεί εταιρείες για σχεδόν 30 χρόνια και πέρυσι επένδυσε 270.000.000 €  για την εξυπηρέτηση 7.400 εταιρειών.\n' +
                                'Η France Active είναι πολλά περισσότερα από ένα δίκτυο, είναι μια πραγματική κίνηση αφοσιωμένων επιχειρηματιών, των οποίων η φιλοδοξία είναι να οικοδομήσουν μια κοινωνία χωρίς αποκλεισμούς. Η αποστολή της France Active είναι να επιταχύνει την επιτυχία των επιχειρηματιών παρέχοντάς τους τα μέσα για να δραστηριοποιηθούν. \n',
                                val2: 'https://www.franceactive.org/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />


                <Text style={styles.dmlH3}>FRANCE INITIATIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Η πρωτοβουλία France Initiative, που αρχικά ονομάζονταν France Initiative Network,  έχει επανασχεδιάσει το σύστημα της λειτουργία της, καθώς από την 1η Οκτωβρίου 2012, λειτουργεί με τοπικές πλατφόρμες και περιφερειακό συντονισμό. Η νέα επωνυμία υπογραμμίζει τώρα τον κοινό παρονομαστή, τον όρο «Πρωτοβουλία», ενώ περιλαμβάνει το όνομα της επικράτειας. Συνοδεύεται από λογότυπο που αντικατοπτρίζει γραφικά την ισχύ ενός εθνικού δικτύου και την ποικιλομορφία του, που συνδέεται με τις τοπικές του ρίζες. Τέλος, φέρει μια υπογραφή που δίνει μεγάλη σημασία στη συλλογική δράση: "Ένα δίκτυο, ένα πνεύμα".',
                                val2: 'http://www.initiative-france.fr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>BGE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Για περισσότερα από 35 χρόνια, το BGE υποστηρίζει τη δημιουργία επιχειρήσεων και εργάζεται για να την καταστήσει πραγματικότητα προσιτή σε όλους. Με την υποστήριξη επιχειρηματιών σε κάθε στάδιο, από την ανάδυση ως την επιχειρηματική ανάπτυξη, δίνουν σε όλους πιθανότητες επιτυχίας. Ως μη κερδοσκοπικός φορέας, το BGE αποτελείται από 50 ενώσεις που στοχεύουν στην ανάπτυξη νέας προοπτικής και την εξασφάλιση της πορείας των επιχειρηματιών ώστε να δημιουργήσουν μόνιμες λύσεις για την απασχόληση και την τοπική ανάπτυξη.',
                                val2: 'http://www.bge.asso.fr/nous-sommes/notre-engagement.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>2nd CHANCE FOUNDATION</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Ο σκοπός του ιδρύματος “2nd CHANCE FOUNDATION” είναι να υποστηρίξει άτομα ηλικίας 18 έως 62 ετών, που έχουν αντιμετωπίσει αρκετές δυσκολίες στη ζωή τους και βρίσκονται σήμερα σε πολύ επισφαλή κατάσταση, αλλά έχουν πραγματική επιθυμία να ορθοποδήσουν. Το “Ίδρυμα 2ης Ευκαιρίας” προσφέρει ανθρώπινη και οικονομική υποστήριξη για τη διεξαγωγή ενός ρεαλιστικού και βιώσιμου επαγγελματικού έργου: κατάρτιση, δημιουργία ή ανάληψη εταιρείας.",
                                val2: 'http://www.deuxiemechance.org/fr'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
