import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για τη Σλοβενία: </Text>
                <Text style={styles.dmlH3}>Βήμα 2:</Text>
                <Text>Θα πρέπει να παρευρεθείτε στην “ημέρα γνωριμίας” στην υπηρεσία απασχόλησης που θα σας ορίσει ο σύμβουλός σας. Εκεί θα συμπληρώσετε μια αίτηση και στη συνέχεια μια επιτροπή θα
                    αποφασίσει εάν θα γίνει αποδεκτή ή όχι.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
