import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 14: κάντε κλικ στην προσθήκη μιας φωτογραφίας για να προσθέσετε τη φωτογραφία του προφίλ σας και επιλέξτε την πηγή της φωτογραφίας του
                    προφίλ σας. Μπορείτε να την κατεβάσετε από το Facebook, μπορείτε να τραβήξετε μια φωτογραφία με τη φωτογραφική μηχανή του κινητού σας τηλεφώνου ή να επιλέξετε μια φωτογραφία από τη
                    βιβλιοθήκη. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img50.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img51.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
