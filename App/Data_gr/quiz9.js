import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz9": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Το μάρκετινγκ μέσω του διαδικτύου είναι μια μορφή ηλεκτρονικού εμπορίου."
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η δημιουργία μιας επιχειρηματικής σελίδας στο Facebook, πρέπει να είναι συνδεδεμένη με προσωπικό λογαριασμό στο Facebook."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Twitter",
                    "option2": "b) Facebook",
                    "option3": "c) Ιστοσελίδα στο διαδίκτυο",
                    "option4": "d) Instagram"
                },
                "question": "Ποιο από τα παρακάτω δεν ανήκει στα κοινωνικά μέσα δικτύωσης;"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Εάν έχει ήδη δημιουργήσει ένα λογαριασμό στο Facebook, με αυτόν το λογαριασμό μπορείτε να δημιουργήσετε μια επιχειρηματική σελίδα στο Instagram."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Για να ανοίξετε έναν επιχειρηματικό λογαριασμό στο Twitter, θα πρέπει πρώτα να δημιουργήσετε έναν προσωπικό λογαριασμό στο Twitter."
            },
            "question6": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Επιχείρηση ή Τοποθεσία.",
                    "option2": "b) Εταιρεία, Οργανισμός ή Ινστιτούτο",
                    "option3": "c) Επωνυμία ή Προϊόν",
                    "option4": "d) Αιτία ή Κοινότητα"
                },
                "question": "Κατά τη ρύθμιση μιας επιχειρηματικής σελίδας στο Facebook, ποια από τις παρακάτω κατηγορίες πρέπει να επιλέξετε για να φτιάξετε ένα μενού;"
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Με τα κουμπιά \"call to action\", οι πελάτες μπορούν να επικοινωνούν μέσω ηλεκτρονικού ταχυδρομείου, τηλεφώνου ή δικτυακού τόπου και είναι σε θέση να κάνουν αγορές"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Οι πελάτες μπορούν να δουν εύκολα τα προϊόντα μέσα από τα μέσα κοινωνικής δικτύωσης."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Επεξεργασία προφίλ",
                    "option2": "b) Μετάβαση στο προφίλ «επιχειρήσεις»",
                    "option3": "c) Αλλαγή Κωδικού",
                    "option4": "d) Ιδιωτικός Λογαριασμός"
                },
                "question": "Ποια \"επιλογή\" πρέπει να χρησιμοποιήσετε για τη δημιουργία ενός επιχειρηματικού λογαριασμού Instagram;"
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Η δημιουργία επιχειρηματικών λογαριασμών",
                    "option2": "b) Η συνεχής αναβάθμιση των προϊόντων",
                    "option3": "c) Το να είσαι διαθέσιμος σε διαφορετικά μέσα κοινωνικής δικτύωσης",
                    "option4": "d) Όλα τα παραπάνω"
                },
                "question": "Ποιο από τα παρακάτω μπορεί να είναι θετικό για την ανάπτυξη του ηλεκτρονικού εμπόριο;"
            }
        }
    }
};


export default class M9Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz9;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
