import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz8": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Τα έξοδα αποστολής είναι από τα υψηλότερα έξοδα σε έργα και επιχειρήσεις μεγάλης κλίμακας"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Παράδοση αυτοπροσώπως.",
                    "option2": "b) Παράδοση την ίδια μέρα.",
                    "option3": "c) Παραλαβή στο κατάστημα.",
                    "option4": "d) Αποστολή από το κατάστημα."
                },
                "question": "Ποιο από τα παρακάτω δεν περιλαμβάνεται στις επιλογές αποστολής;"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η στρατηγική, που ακολουθείται ως προς τα έξοδα αποστολής και παραλαβής αποτελεί αναπόσπαστο μέρος του υπολογισμού της κερδοφορίας των πωλήσεων."
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Οι μεταφορικές εταιρείες μπορεί να μεταφέρουν χειροτεχνήματα σε μικρές ποσότητες οδικά."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Πλαστικές σακούλες.",
                    "option2": "b) Κούτες από χαρτόνι.",
                    "option3": "c) Συσκευασία με φυσαλίδες αέρα.",
                    "option4": "d) Όλα τα παραπάνω."
                },
                "question": "Ποιο από τα παρακάτω χρησιμοποιείται για τη συσκευασία αγαθών για μεταφορά;"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η μεταφορά των αγαθώνδωρεάν δεν αποτελεί τρόπο για να τραβήξετε την προσοχή του πελάτη."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Η ασφάλεια.",
                    "option2": "b) Το τιμολόγιο.",
                    "option3": "c) Η δήλωση του τελωνείου.",
                    "option4": "d) Η ετικέτα του πακέτου."
                },
                "question": "______ είναι ένα επίσημο έγγραφο, που δίνει τον κατάλογο και λεπτομέρειες των εμπορευμάτων, που εισάγονται ή εξάγονται."
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η συμφωνία με μεταφορική εταιρεία για τακτική συνεργασία μπορεί να συμβάλει στη μείωση των ναύλων λόγω του όγκου εργασιών."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Υποκαταστήματα.",
                    "option2": "b) Αυτοεξυπηρέτηση.",
                    "option3": "c) Φίλοι.",
                    "option4": "d) Τηλεφωνικό κέντρο."
                },
                "question": "Ποιο από τα παρακάτω δεν συγκαταλέγεται στους τρόπους επαφής με μια μεταφορική εταιρεία;"
            },
            "question10": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Ονοματεπώνυμο, διεύθυνση και τηλέφωνο αποστολέα.",
                    "option2": "b) Το χρώμα του αντικειμένου που βρίσκεται μέσα στο πακέτο.",
                    "option3": "c) Ονοματεπώνυμο, διεύθυνση και τηλέφωνο παραλήπτη.",
                    "option4": "d) Λεπτομέρειες πληρωμής (ποιος θα πληρώσει)."
                },
                "question": "Ποιο από τα παρακάτω δεν συμπεριλαμβάνεται σε ένα τιμολόγιο;"
            }
        }
    }
};

export default class M8Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz8;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
