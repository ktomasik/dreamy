import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img14.png')
    }
}];

export default class M9S7_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Δημιουργία εταιρικού λογαριασμού στο Twitter:</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 2:</Text> Πατήστε το κουμπί “Create Profile” και μετά συμπληρώστε όλα τα απαραίτητα πεδία στη σελίδα
                    του Twitter, όπως δείχνουν οι οδηγίες:</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img14.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <FlatList
                    data={[
                        {key: '1. Username: Ιδανικό θα ήταν το όνομα να αντιστοιχεί σε αυτό της επιχείρησής σας'},
                        {key: '2.  Edit Profile: Έχετε στη διάθεσή σας 160 χαρακτήρες για να αναδείξετε τα ιδιαίτερα στοιχεία που αντιπροσωπεύουν τον λογαριασμό σας'},
                        {key: '3. Profile Photo: Είναι η φωτογραφία λογαριασμού με προβλεπόμενες διαστάσεις 400x400 pixels. Ενδείκνυται να ορίσετε ως φωτογραφία προφίλ εκείνη του λογότυπού σας.'},
                        {key: '4. Header Photo: Είναι η φωτογραφία εξωφύλλου. Εδώ μπορείτε να χρησιμοποιήσετε φωτογραφίες από τις εκδηλώσεις ή τα προϊόντα σας, πληροφορίες ή και εικόνες που αφορούν την προώθηση της επιχείρησής σας. Ακόμη μπορείτε να χρησιμοποιήσετε τη  φωτογραφία εξωφύλλου ως χώρο για τις ανακοινώσεις σας, ειδικά αν αυτές αφορούν πωλήσεις ή προωθητικές ενέργειες που είναι σε εξέλιξη την τρέχουσα περίοδο. (Προβλεπόμενες διαστάσεις για αυτή τη φωτογραφία είναι 1500x500 pixels) .'},
                        {key: '5. Καρφιτσωμένο (Pinned) Tweet: Αν έχετε πάντα τα πιο σημαντικά ή τα τελευταία νέα στην αρχή της σελίδας καρφιτσωμένα, διευκολύνετε έτσι τους επισκέπτες σας να τα βρίσκουν αμέσως χωρίς να χρειάζεται να περιηγηθούν σε ολόκληρη τη σελίδα σας.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Σιγουρευτείτε ότι έχετε συμπληρώσει όσο το δυνατόν περισσότερα πεδία με τις απαραίτητες πληροφορίες που χρειάζονται για τους λογαριασμούς σας, καθώς επίσης
                    χρησιμοποιήστε μια καθαρή εικόνα / φωτογραφία ή λογότυπο για όλους τους λογαριασμούς σας στα Social Media.</Text>
            </View>
        )
    }
}
