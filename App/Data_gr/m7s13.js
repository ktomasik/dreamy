import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Επιχορηγήσεις μικρών επιχειρήσεων για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Τουρκία::</Text>
                <Text style={styles.dmlH3}>KOSGEB</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Ο Οργανισμός Ανάπτυξης Μικρών και Μεσαίων Επιχειρήσεων (KOSGEB) είναι ο κύριος αρμόδιος φορέας για την ανάπτυξη, τον συντονισμό και την εφαρμογή πολιτικής στις ΜΜΕ. Η KOSGEB προσφέρει στήριξη δανείων σε γυναίκες επιχειρηματίες που επιθυμούν να ιδρύσουν δική τους επιχείρηση. Στο πλαίσιο αυτό, παρέχεται πίστωση 50.000 ΤL (10.000 Ευρώ). Αυτή η υποστήριξη, η οποία παρέχεται από το κράτος είναι εντελώς μη επιστρέψιμη. Εκτός από αυτήν την επιχορήγηση, οι γυναίκες λαμβάνουν δωρεάν πίστωση. Μέχρι το 2018 το δάνειο αυτό ανέρχεται σε 70.000 TL (14.000 ευρώ). Η πρώτη επιχείρηση των γυναικών επιχειρηματιών θα είναι επίσης απαλλαγμένη από φόρους για τα τρία πρώτα χρόνια.",
                                val2: 'https://www.kosgeb.gov.tr/site/tr/genel/detay/6057/kadin-girisimciligi-women-entrepreneurship'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
