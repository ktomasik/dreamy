import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Τι σημαίνει ο όρος «online αγορές»?</Text>
                <Text>Οι Online αγορές είναι μια μορφή ηλεκτρονικού εμπορίου, που επιτρέπει στους καταναλωτές να αγοράζουν αγαθά και υπηρεσίες κατευθείαν από έναν πωλητή στο διαδίκτυο χρησιμοποιώντας
                    ένα πρόγραμμα περιήγησης στο διαδίκτυο .</Text>

                <Text style={styles.dmlH2}>Τι σημαίνει ο όρος «Online Marketing»;</Text>
                <Text>Το Online marketing είναι μια σειρά εργαλείων και μεθοδολογιών που χρησιμοποιούνται για την προώθηση προϊόντων και υπηρεσιών μέσα από το διαδίκτυο . Το Online marketing
                    περιλαμβάνει ένα μεγαλύτερο εύρος από στοιχεία marketing σε σχέση με εκείνο του παραδοσιακού business marketing λόγω των επιπλέον καναλιών και μηχανισμών που είναι διαθέσιμα στο
                    διαδίκτυο.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmark2.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
