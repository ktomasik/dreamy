import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S31_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Πολωνία:</Text>


                <Text style={styles.dmlH3}>Βήμα 2:</Text>
                <Text>Αναμένετε την ημερομηνία συνέντευξης, που θα οριστεί, μ’ έναν σύμβουλο γραφείου.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_22_3319622.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
