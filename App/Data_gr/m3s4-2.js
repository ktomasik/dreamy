import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S4_2 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να φωτογραφίσετε τα προϊόντα σας και να αναρτήσετε τις φωτογραφίες τους στο etsy.com</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 2: Φωτογραφίστε το προϊόν σας</Text>
                <View style={{alignItems: 'center'}}>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img18.png')}
                    resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
