import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Αναζήτηση δάνειων μικρών επιχειρήσεων για τις γυναίκες</Text>
                <Text style={styles.dmlH2}>Για τη Σλοβενία: </Text>
                <Text style={styles.dmlH3}>Τράπεζες</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Απλό και γρήγορο ηλεκτρονικό δάνειο έως 7.000€ χωρίς κόστος έγκρισης.',val2: 'http://www.hipkredit.si/'}},
                        {key: {val1: 'Μικροπίστωση με χαμηλό επιτόκιο και κόστος, όπου ένα ευνοϊκό και σταθερό μηνιαίο κόστος κατανέμεται καθ \'όλη τη διάρκεια της αποπληρωμής του δανείου.', val2: 'http://www.intesasanpaolobank.si/'}},
                        {key: {val1: 'Ελκυστικές προσφορές δανείων',val2:'http://www.sparkasse.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Οικονομικές Υπηρεσίες</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Δάνειο αξίας 500 ή 1.000€.',val2:'http://www.skupina8.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Τοπικό Εμπορικό και Βιομηχανικό Επιμελητήριο</Text>
                <FlatList
                    data={[
                    {key: {val1: 'Παρέχει βραχυπρόθεσμα δάνεια με επιδότηση μέρους του επιτοκίου προς τα μέλη.',val2:'http://www.eng.gzs.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Επιχειρηματικό Κέντρο Επενδύσεων</Text>
                <FlatList
                        data={[
                        {key: {val1: 'Γρήγορο ηλεκτρονικό δάνειο από 1.000 EUR έως 30.000€.',val2:'http://www.pnc.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                       onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
