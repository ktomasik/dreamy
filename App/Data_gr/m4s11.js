import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";


export default class M4S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Καταχώριση διακριτικού τίτλου </Text>
                <Text style={{marginTop: 10}}>Ακολουθήστε τις οδηγίες για να καταχωρήσετε το διακριτικό τίτλο ή εμπορικό σήμα της επιχείρησης: </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Αποφασίστε την επωνυμία της επιχείρησης
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Κάνετε αίτηση στο Υπουργείο Εμπορίου
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Πληρώστε ένα τέλος εγγραφής
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
