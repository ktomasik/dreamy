import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";
// Styles

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m4/img1.png')
    }
}];

export default class M4S2 extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>1.2.2. Ποια είναι η διαφορά μεταξύ μιας εταιρικής επωνυμίας, ενός διακριτικού τίτλου και μιας νομικής επωνυμίας;</Text>
                <Text>Η εταιρική επωνυμία είναι το επίσημο όνομα του φυσικού ή νομικού προσώπου που κατέχει την εταιρεία. Είναι η επωνυμία της εταιρείας απέναντι στο νόμο. Η εταιρική επωνυμία
                    χρησιμοποιείται σε κυβερνητικά έντυπα και εφαρμογές (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Για παράδειγμα: </Text> Το όνομα του επιχειρηματία είναι John Smith και έχει μια ασφαλιστική εταιρεία. Η νομική επωνυμία της εταιρείας
                    του μπορεί να είναι το «Ασφάλειες John Smith» (Ανώνυμος 2018b).
                    {"\n\n"}Οι ιδιοκτήτες επιχειρήσεων μπορούν να χρησιμοποιούν ένα διακριτικό τίτλο για σκοπούς διαφήμισης και πωλήσεων. Ο διακριτικός τίτλος είναι η ονομασία, που βλέπει το κοινό για
                    παράδειγμα στις αφίσες και στο διαδίκτυο (Cameron 2017).
                    {"\n\n"}Η επωνυμία της επιχείρησης και ο διακριτικός της τίτλος μπορούν να είναι διαφορετικά. Ο διακριτικός τίτλος δεν χρειάζεται να συνοδεύεται από το χαρακτηρισμό Ε.Π.Ε., Α.Ε. ή
                    άλλες νομικές μορφές εταιρείας, που χρησιμοποιούνται για φορολογικούς λόγους (Cameron 2017).

                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Για παράδειγμα: </Text> Το McDonald's είναι ένας διακριτικός τίτλος. Η νομική επωνυμία της εταιρείας είναι McDonald's Corporation
                    (Ανώνυμος 2018c).
                </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m4/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 10}}>
                    Η νομική επωνυμία μιας επιχείρησης είναι το όνομα του προσώπου ή της οντότητας που κατέχει την επιχείρηση. Εάν η επιχείρηση είναι προσωπική εταιρεία (ΟΕ, ΕΕ), η νομική ονομασία είναι τα επώνυμα των εταίρων.
                    {"\n\n"}Για τις εταιρείες η επωνυμία αναφέρεται στο καταστατικό τους και περιλαμβάνει στο τέλος τη νομική μορφή της εταιρείας, όπως Ε.Π.Ε., A.E.  (Fishman 2015).

                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10}}>Πότε πρέπει να χρησιμοποιείται η νομική επωνυμία και πότε ο διακριτικός τίτλος;</Text>
                <Text>Η νομική επωνυμία πρέπει να χρησιμοποιείται όταν επικοινωνείτε με το δημόσιο ή με άλλες επιχειρήσεις.
                    {"\n\n"}Για παράδειγμα, η νομική επωνυμία της επιχείρησης θα πρέπει να χρησιμοποιείται κατά την υποβολή φορολογικών δηλώσεων, την αγορά ακινήτου ή τη σύνταξη επιταγών.
                    {"\n\n"}Μια εταιρεία μπορεί να χρησιμοποιεί το διακριτικό της τίτλο για διαφημιστικούς και εμπορικούς σκοπούς. Είναι συχνά το όνομα που βλέπει το ευρύ κοινό σε αφίσες, στο διαδίκτυο και σε διαφημίσεις (Ανώνυμος 2018d).
                    {"\n\n"}Βασικά, η νομική ονομασία είναι για τα επίσημα έγγραφα (π.χ. τιμολόγια), ενώ ο διακριτικός τίτλος για τις δημόσιες σχέσεις.

                </Text>
            </View>
        )
    }
}
