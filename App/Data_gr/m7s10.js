import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Αναζήτηση δάνειων μικρών επιχειρήσεων για τις γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Γαλλία:</Text>
                <Text style={styles.dmlH3}>Ταμείο Εγγυήσεων Γυναικών (FGIF)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Σκοπός: Η διευκόλυνση της απόκτησης τραπεζικών δανείων για την κάλυψη των απαιτήσεων κεφαλαίου κίνησης ή / και επενδύσεων στη φάση δημιουργίας, ανάκτησης ή ανάπτυξης μιας επιχείρησης.',
                                val2: 'https://www.afecreation.fr/pid14855/appuis-pour-les-femmes.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Τα δίκτυα</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Τα εθνικά δίκτυα που παρατίθενται παρακάτω είναι στη διάθεσή σας και σας καλωσορίζουν, προκειμένου να σας ενημερώσουν και να σας συνοδεύσουν κατά τη διάρκεια υλοποίησης του έργου σας. Ορισμένοι ασχολούνται με γυναίκες δημιουργούς, άλλοι απευθύνονται σε όλους, αλλά ασχολούνται με συγκεκριμένες δράσεις για γυναίκες, άλλοι απλώς παρακολουθούν τις εταιρείες που έχουν χρηματοδοτήσει.",
                                val2: 'http://www.ellesentreprennent.fr/pid14416/les-reseaux-au-service-des-creatrices.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>The BPI France (Τράπεζα Δημοσίων Επενδύσεων):</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Η BPI είναι ένας οργανισμός που τελεί υπό την εποπτεία του κράτους. Παρέχει χρηματοδότηση και αναπτυξιακά βοηθήματα. Επιπλέον, προσφέρει λύσεις διασύνδεσης και εγγύησης για να πείσει την τράπεζά σας να σας ακολουθήσει στα σχέδιά σας.",
                                val2: 'http://www.bpifrance.com/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
