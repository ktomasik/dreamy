import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Πληροφορίες σχετικά με τις συμβάσεις</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 2:</Text> Αφού καταχωρήσετε τα στοιχεία σας, αυτά θα αξιολογηθούν και κατόπιν θα σας αποσταλούν οι
                    πιο κατάλληλες προσφορές. Μπορείτε να συγκρίνετε τις διάφορες προσφορές και να επιλέξετε εκείνη που θέλετε. Η σύγκριση θα πρέπει να αφορά το όριο, την εγγύηση και την τιμή.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_32_3319612.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
