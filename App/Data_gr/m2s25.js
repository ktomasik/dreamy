import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 12: Προτείνεται επίσης να ακολουθείτε τους φίλους σας από τις επαφές σας. Για να γίνει αυτό, θα πρέπει να επιτρέψετε στην εφαρμογή του
                    Instagram να έχει πρόσβαση στα στοιχεία επικοινωνίας σας. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img46.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
