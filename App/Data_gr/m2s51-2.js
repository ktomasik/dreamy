import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S51_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε μια σελίδα στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 7: Μπορείτε να πληκτρολογήσετε μια διεύθυνση URL της αρχικής σας σελίδας για να στείλετε τους επισκέπτες σας στη συγκεκριμένη σελίδα (
                    ή να παραλείψετε αυτό το βήμα ) και να κάνετε κλικ στο κουμπί ΕΠΟΜΕΝΟ. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img104.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
