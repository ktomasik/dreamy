import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Επιχορηγήσεις μικρών επιχειρήσεων για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Ελλάδα:</Text>
                <Text style={styles.dmlH3}>Ελληνική κοινότητα στο εξωτερικό </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Η ελληνική κοινότητα στο εξωτερικό δραστηριοποιείται με σκοπό να δημιουργήσει νέες δράσεις, όπως το Envolve Award Greece, που αφορά άτοκο δάνειο ύψους μέχρι 500.000€, με αποπληρωμή εντός 5 ετών.',
                                val2: 'https://envolveglobal.org/el/envolve-awards/envolve-greece'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Κεφάλαιο Σποράς </Text>
                <Text>Το Κεφάλαιο Σποράς (Seed Capital) είναι μικρή χρηματοδότηση για την έναρξη μιας επιχείρησης που δίνεται συνήθως σε συγκεκριμένες κατηγορίες πληθυσμού, όπως οι νέοι ή οι άνεργοι.
                    Τα βασικά χαρακτηριστικά του είναι ότι το κεφάλαιο είναι αρκετά μικρό (15.000 έως 50.000 €), προπληρώνεται και προορίζεται να καλύψει τα λειτουργικά έξοδα του πρώτου έτους της
                    εταιρείας, ώστε να δοθεί χρόνος για την ανάπτυξη της επιχείρησης. Ενδεικτικά, υπάρχουν δράσεις τόσο του δημοσίου (πρόγραμμα OAEΔ) όσο και του ιδιωτικού τομέα (The Open Fund), για
                    διάφορους τομείς της οικονομίας (παραδοσιακά προϊόντα, τεχνολογία πληροφορικής κλπ.) Η διαδικασία είναι απλή και η υποβολή μπορεί να γίνει από οποιονδήποτε ενδιαφέρεται. Ωστόσο,
                    αυτές οι δράσεις δεν είναι δυνατές καθ' όλη τη διάρκεια του έτους, ενώ υπάρχουν ορισμένες προϋποθέσεις, όπως σεμινάρια και κάρτες ανεργίας για τα δημόσια προγράμματα
                    (ΟΑΕΔ).{"\n"}</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: '',
                                val2: 'http://www.digitalplan.gov.gr/portal/resource/Prosklhsh-Ypobolhs-Protasewn-sta-Ergaleia-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Sporas-Seed-ICT-Fund-kai-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Ekkinhshs-Early-Stage-ICT-Fund-gia-epiheirhseis-ston-klado-twn-Tehnologiwn-Plhroforikhs-kai-Epikoinwniwn-ICT-ths-Prwtoboylias-JEREMIE'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Συμφωνία εταιρικής σχέσης (PA) 2014-2020 (ESPA) </Text>
                <Text>Το ΕΣΠΑ είναι το ελληνικό πρόγραμμα, το οποίο διοχετεύει κεφάλαια από προγράμματα της Ευρωπαϊκής Ένωσης για την εξάλειψη των ανισοτήτων μεταξύ των χωρών της ΕΕ. Στο πλαίσιο του
                    ΕΣΠΑ, η κυβέρνηση διανέμει κεφάλαια που στοχεύουν στο εμπόριο, τη μεταποίηση ή την πρωτογενή παραγωγή. Οι αιτήσεις υποβάλλονται σε περιορισμένες χρονικά περιόδους, οι οποίες
                    ανακοινώνονται από τα αρμόδια Υπουργεία και τη Γενική Γραμματεία του ΕΣΠΑ. Η αξιολόγηση των προτάσεων γίνεται από ανεξάρτητους αξιολογητές, τα αποτελέσματα δημοσιεύονται και στη
                    συνέχεια δίνεται μια περίοδος 1-3 ετών για την υλοποίηση κάθε δράσης. Η χρηματοδότηση μπορεί να είναι ένα ποσοστό της επένδυσης ή της φορολογικής μείωσης (συνήθως 40-60%). Τα
                    έγγραφα δαπανών απαιτούνται και εξετάζονται κατά τους επιτόπιους ελέγχους.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Το μεγάλο πλεονέκτημα του ΕΣΠΑ είναι η διαθεσιμότητά του αλλά καθώς η χρηματοδότηση ακολουθεί την αρχική ιδιωτική επένδυση, δεν είναι ιδιαίτερα χρήσιμη για νέες επιχειρήσεις. Ωστόσο, είναι ιδιαίτερα ελκυστική η δυνατότητα συνδυασμού με άλλες μορφές χρηματοδότησης (τραπεζικά δάνεια, επιχειρηματικά κεφάλαια κ.λπ.).",
                                val2: 'https://www.espa.gr/en/pages/staticPartnershipAgreement.aspx'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}} onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text>Μεταξύ άλλων, το πρόγραμμα ΕΣΠΑ 2014-2020 χρηματοδοτεί τις ακόλουθες πρωτοβουλίες:</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Νεοσύστατες και νέες επιχειρήσεις ', val2: 'https://www.efepae.gr/frontend/articles.php?cid=496&t=Neofuis-Epixeirimatikotita'}},
                        {
                            key: {
                                val1: 'Αναβάθμιση πολύ μικρών και μικρών επιχειρήσεων, ώστε να αναπτύξουν ικανότητες χρήσιμες στις νέες αγορές',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=497&t=Anabathmisi-polu-mikrwn-&-mikrwn-epixeirisewn-gia-tin-anaptuksi-twn-ikanotitwn-tous-stis-nees-agores'
                            }
                        },
                        {key: {val1: 'Επιχειρηματικότητα στο εξωτερικό', val2: 'https://www.efepae.gr/frontend/articles.php?cid=539&t=Epixeiroume-Eksw'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
