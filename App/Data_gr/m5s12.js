import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Αποδοχή πληρωμών μέσω πιστωτικής κάρτας</Text>
                <Text style={{marginTop: 15}}>Ακολουθήστε τις παρακάτω οδηγίες για να αποδεχτείτε τις πληρωμές με Πιστωτική Κάρτα.
                    {"\n\n"}Για την αποδοχή πληρωμών μέσω πιστωτικής κάρτας, θα χρειαστείτε:

                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Εμπορικός λογαριασμός: :',
                                val2: 'Ένας τραπεζικός λογαριασμός όπου κατατίθενται πληρωμές με πιστωτικές και χρεωστικές κάρτες'
                            }
                        },
                        {
                            key: {
                                val1: 'Εικονικό Τερματικό:',
                                val2: 'Like a digital credit card swipe machine, this system allows you to input credit card information on your computer.'
                            }
                        },
                        {
                            key: {
                                val1: 'Πύλη:',
                                val2: 'Ο σύνδεσμος μεταξύ του ηλεκτρονικού σας καταστήματος και της τράπεζας, ο οποίος στέλνει τις πληροφορίες πληρωμής με ασφάλεια ώστε η πληρωμή είτε να εγκριθεί είτε να απορριφθεί.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />

                <Text style={{marginTop: 10}}>Πώς λειτουργεί βήμα-βήμα</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Βήμα 1:',
                                val2: 'Όταν κάποιος πελάτης θέλει να πραγματοποιήσει μια αγορά από το κατάστημά σας, συνδέεται με το εικονικό σας τερματικό και επιλέγει τα προϊόντα ή τις υπηρεσίες που επιθυμεί να αγοράσει. Στη συνέχεια, πρέπει να εισαγάγει τα στοιχεία της πιστωτικής του κάρτας.'
                            }
                        },
                        {
                            key: {
                                val1: 'Βήμα 2:',
                                val2: 'Οι πληροφορίες πληρωμής περνούν μέσω της ασφαλούς πύλης πληρωμής και διαβιβάζονται στην πηγή εξουσιοδότησης.'
                            }
                        },
                        {
                            key: {
                                val1: 'Βήμα 3:',
                                val2: 'Η τράπεζα που εξέδωσε την πιστωτική κάρτα λαμβάνει τις πληροφορίες συναλλαγής, ελέγχει εάν τα χρήματα είναι διαθέσιμα και εγκρίνει ή απορρίπτει την απελευθέρωση της πληρωμής..'
                            }
                        },
                        {
                            key: {
                                val1: 'Βήμα 4:',
                                val2: 'Η πύλη πληρωμής "σας ενημερώνει" εάν η πληρωμή έγινε αποδεκτή ή απορρίφθηκε.'
                            }
                        },
                        {
                            key: {
                                val1: 'Βήμα 5:',
                                val2: 'Εάν η πληρωμή εγκριθεί, τα χρήματα μεταφέρονται στον τραπεζικό σας λογαριασμό  εντός 2-3 ημερών..'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />
            </View>
        )
    }
}
