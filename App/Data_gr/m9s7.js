import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img13.png')
    }
}];

export default class M9S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Δημιουργία εταιρικού λογαριασμού στο Twitter:</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 1:</Text> Δημιουργείστε έναν λογαριασμό μεταβαίνοντας στο Twitter.com ή ανοίξτε την εφαρμογή Twitter
                    (για iOS ή Android). Πραγματοποιείστε Είσοδο στο Twitter πατώντας Log in. </Text>

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img13.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
