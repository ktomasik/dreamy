import React, {Component} from 'react'
import {Image, Linking, Text, TouchableHighlight, View, Modal} from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m3/img19.png')
    }
}];

export default class M3S4_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να φωτογραφίσετε τα προϊόντα σας και να αναρτήσετε τις φωτογραφίες τους στο etsy.com</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>

                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 4: Για να επιλέξετε τη γλώσσα, πηγαίντε στο κάτω μέρος της σελίδας και διαλέξτε τη γλώσσα που επιθυμείτε:</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img19.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
