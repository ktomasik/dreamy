import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img13.png')
    }
}];

export default class M8S9_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>ΕΦΑΡΜΟΓΗ ΣΤΗΝ ΠΡΑΞΗ</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>ΒΗΜΑ 6ο: ΖΗΤΗΣΤΕ ΤΟ ΤΙΜΟΛΟΓΙΟ.</Text>
                <Text>Το τιμολόγιο περιλαμβάνει ορισμένες πληροφορίες όπως τις παρακάτω:</Text>
                <FlatList
                    data={[
                        {key: 'Όνομα, διεύθυνση, τηλέφωνο κλπ. του αποστολέα,'},
                        {key: 'Όνομα, διεύθυνση, τηλέφωνο κ.λπ. του παραλήπτη,'},
                        {key: 'Λεπτομέρειες μεγέθους φορτίου και τύπου, όπως ποσότητα, διαστάσεις κ.λπ.,'},
                        {key: 'Πληροφορίες χρέωσης (αποστολέα ή παραλήπτη)'},
                        {key: 'Ημερομηνία, στοιχεία τιμολογίου και αριθμός παρακολούθησης.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img13.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
