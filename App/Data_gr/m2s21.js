import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S21 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 8: Συμπληρώστε το όνομά σας και τον κωδικό πρόσβασής σας. Στη συνέχεια, κάντε κλικ στο κουμπί Επόμενο. Αυτό θα είναι το τελευταίο βήμα
                    της δημιουργίας του λογαριασμού σας στο Instagram πριν από τη ρύθμιση του προφίλ σας…</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img37.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img38.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img39.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
