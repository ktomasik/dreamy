import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz5": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Ένας λογαριασμός, για τον οποίο εκδίδεται βιβλιάριο επιταγών, δεν προσφέρει πρόσβαση σε χρήματα για την κάλυψη των καθημερινών συναλλαγών"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Οι πληρωμές λογαριασμών με πάγια εντολή είναι μη τακτικές πληρωμές σε πωλητές, που γίνονται από έναν τραπεζικό, χρηματιστηριακό λογαριασμό ή από λογαριασμό αμοιβαίων κεφαλαίων."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Πληρωμή διαδικτυακά, με αποκάλυψη των λεπτομερειών των στοιχείων της πιστωτικής σας κάρτας. ",
                    "option2": "b) Πληρωμή με μετρητά.",
                    "option3": "c) Πληρωμή μέσω μεταφοράς από τραπεζικό λογαριασμό.",
                    "option4": "d) Πληρωμή έναντι τιμολογίου."
                },
                "question": "Ποιο από τα παρακάτω δεν αποτελεί τρόπο ηλεκτρονικής πληρωμής;"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η ηλεκτρονική τραπεζική αναφέρεται σε οποιαδήποτε τραπεζική συναλλαγή, που μπορεί να πραγματοποιηθεί μέσω του Διαδικτύου."
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Διαλέξτε την τράπεζα που ανταποκρίνεται καλύτερα στον σκοπό σας.",
                    "option2": "b) Επισκεφτείτε μία τράπεζα και ζητήστε να ανοίξετε έναν λογαριασμό.",
                    "option3": "c) Μην αποκαλύπτετε στην τράπεζα τις προσωπικές σας πληροφορίες .",
                    "option4": "d) Φυλάξτε τα έγγραφα του λογαριασμού σε ασφαλές μέρος."
                },
                "question": "Ποια από τις παρακάτω οδηγίες για άνοιγμα τραπεζικού λογαριασμού είναι λάθος;"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Οι χρεωστικές κάρτες αντλούν χρήματα απευθείας από τον λογαριασμό εφόσον αυτά που αγοράζετε περνάνε άμεσα στο λογιστικό σας σύστημα."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Αποκτήστε έναν αριθμό αναγνώρισης για την επιχείρηση.",
                    "option2": "b) Ενεργοποιήστε τη ρύθμιση για να Λαμβάνετε Πληρωμές από Τρίτους.",
                    "option3": "c) Ανοίξτε έναν επιχειρηματικό τραπεζικό λογαριασμό.",
                    "option4": "d) Δημιουργείστε έναν λογαριασμό στα μέσα κοινωνικής δικτύωσης."
                },
                "question": "Ποιο από τα παρακάτω βήματα απαιτείται για να δέχεστε πληρωμές;"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Ο εκδότης της πιστωτικής σας κάρτας, σας αποστέλλει μια συνοπτική εικόνα κινήσεων της κάρτας περίπου μία φορά το μήνα."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Να δημιουργήσετε μια απλή αίτηση παραγγελίας στο Facebook",
                    "option2": "b) Να ρυθμίσετε τις ειδοποιήσεις της φόρμας παραγγελίας.",
                    "option3": "c) Να ρυθμίσετε τις επιβεβαιώσεις της φόρμας παραγγελίας.",
                    "option4": "d) Να κανονίσετε τις ρυθμίσεις πληρωμής."
                },
                "question": "Ποιο από τα παρακάτω είναι λάθος; Για την παραλαβή παραγγελιών απαιτείται:"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Εμπορικός λογαριασμός",
                    "option2": "b) Εικονικό τερματικό",
                    "option3": "c) Μετρητά",
                    "option4": "d) Πύλη"
                },
                "question": "Για την αποδοχή πληρωμών από την πιστωτική Κάρτα, ποιο από τα παρακάτω δεν είναι απαραίτητο;"
            }
        }
    }
};

export default class M5Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz5;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
