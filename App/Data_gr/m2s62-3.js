import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S62_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα ιστολόγιο ( Blog )</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 15: Συμπληρώστε τον τίτλο του ιστολογίου σας, για παράδειγμα: το Blog του David και τη διεύθυνση URL του ιστολογίου σας, για
                    παράδειγμα: davidsblog . blogspot . com. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img132.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
