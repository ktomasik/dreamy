import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S45_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Twitter</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 15: Τώρα ο λογαριασμός σας είναι έτοιμος για τιτίβισμα-Tweet . Μπορείτε να γράψετε το πρώτο σας τιτίβισμα κάνοντας κλικ στο εικονίδιο
                    f. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img92.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img93.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
