import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S23 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Μικροχρηματοδότηση κι άλλα Κεφάλαια Εκκίνησης για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Τουρκία::</Text>
                <Text style={styles.dmlH3}>Kredi Garanti Fonu (The Credit Guarantee Fund- KGF)</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Η KGF είναι μη κερδοσκοπική εταιρία και ενεργεί ως εγγυητής για μικρές και μεσαίες επιχειρήσεις (ΜΜΕ) και τις επιχειρήσεις που δεν είναι ΜΜΕ, αλλά δεν μπορούν να λάβουν δάνειο λόγω ανεπάρκειας. Έτσι, το KGF υποστηρίζει τις επιχειρήσεις αυτές στην πρόσβαση στη χρηματοδότηση. Το δάνειο υποστηρίζει τις γυναίκες επιχειρηματίες να αναπτύξουν τις δραστηριότητές τους ή να βγάλουν τις επιχειρήσεις τους από μια δύσκολη κατάσταση.",
                                val2: 'http://www.kgf.com.tr/index.php/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Turkish Grameen Πρόγραμμα Μικροχρηματοδότησης </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Το πρόγραμμα μικροχρηματοδοτήσεων της Turkish Grameen (TGMP) είναι ένα μη κερδοσκοπικό, οικονομικό ίδρυμα. Αντί για παραδοσιακές δωρεές και \"φιλανθρωπικούς σκοπούς\", η TGMP προσφέρει υπηρεσίες μικροπιστώσεων για τη μείωση της φτώχειας στην Τουρκία. Ο στόχος του συστήματος μικροπιστώσεων είναι να βοηθήσει τις γυναίκες με χαμηλό εισόδημα να συμμετάσχουν σε βιώσιμες δραστηριότητες δημιουργίας εισοδήματος και να συμβάλουν στον οικογενειακό προϋπολογισμό τους. Σε αντίθεση με τον επίσημο (εμπορικό) τραπεζικό τομέα, τα μικροπιστωτικά δάνεια προσφέρονται χωρίς να απαιτείται εγγύηση ή άλλη τεκμηρίωση εκτός από την τουρκική ταυτότητα.\n" +
                                "Ορισμένα προϊόντα μικροπιστώσεων παρατίθενται παρακάτω.", val2: 'http://www.tgmp.net/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Βασικό Δάνειο</Text>
                <Text>Το βασικό δάνειο είναι ο πρώτος τύπος δανείου για παλιά και νέα μέλη. Τα νέα μέλη μπορούν να λάβουν δάνειο από 100 TL έως 1.000 TL και η αποπληρωμή των δανείων αυτών γίνεται σε
                    46 εβδομάδες.</Text>

                <Text style={styles.dmlH3}>•Επιχειρηματικό Δάνειο</Text>
                <Text>Ένας τύπος δανείου για επιχειρηματίες και επιτυχημένα μέλη μπορεί να είναι από 1.000 TL μέχρι 5.000 TL και η αποπληρωμή του γίνεται μέσα σε 46 εβδομάδες</Text>

                <Text style={styles.dmlH3}>• Δάνειο ψηφιακού χάσματος</Text>
                <Text>Εκτός από τα άλλα δάνεια που είναι δυνατόν να λάβουν τα μέλη, αυτός ο τύπος δανείου αποσκοπεί στην παροχή τεχνολογικής ανάπτυξης. Η αποπληρωμή του γίνεται μέσα σε 46
                    εβδομάδες.</Text>
            </View>
        )
    }
}
