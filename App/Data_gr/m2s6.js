import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 2: Στο παράθυρο «Αναζήτηση» πληκτρολογήστε "Facebook" και κάντε κλικ στο κουμπί ΕΓΚΑΤΑΣΤΑΣΗ. Στη συνέχεια, περιμένετε να ολοκληρωθεί η
                    εγκατάσταση.
                </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img4.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img5.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img6.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
