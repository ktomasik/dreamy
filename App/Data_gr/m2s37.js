import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S37 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να ανταλλάξετε πληροφορίες από το Instagram στα άλλα μέσα κοινωνικής δικτύωσης</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 8: …και περιλαμβάνεται επίσης στην επιλεγμένη ενότητα #hashtag. Μπορείτε να το ελέγξετε κάνοντας κλικ σε ένα σύνδεσμο #hashtag. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img70.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img71.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img72.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
