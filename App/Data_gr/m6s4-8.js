import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S4_8 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Πώς να δημιουργήσετε ένα τιμολόγιο στα Έγγραφα Google</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight:'bold'}}>Βήμα 8:</Text> Κάντε κλικ στο κουμπί "My Drive" στο μενού που ανόίγει προς τα κάτω</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m6/img17.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
