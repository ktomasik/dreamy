import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Εταιρικός λογαριασμός στο Instagram </Text>
                <Text styl={{marginTop: 10}}>Με βάση τις παραπάνω οδηγίες, (κι εφ’ όσον έχετε ήδη δημιουργήσει έναν εταιρικό λογαριασμό στο Facebook), το Instagram - αν το δείτε από μια επαγγελματική
                    σκοπιά - είναι πιο απλό στη χρήση και χρειάζεται λιγότερο χρόνο για να το διαχειριστεί κανείς.
                    {"\n\n"}Αν θέλετε, μπορείτε να επεκτείνετε το δίκτυο των πελατών σας χρησιμοποιώντας ταυτόχρονα και το Facebook και το Instagram.

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
