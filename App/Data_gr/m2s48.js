import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S48 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης </Text>
                <Text style={{marginTop: 10}}>Η ενημέρωση των ανθρώπων σχετικά με την πρόοδο της επιχείρησής σας ή τα προϊόντα σας είναι πολύ σημαντική δουλειά. Πολλοί άνθρωποι αποφασίζουν να το
                    κάνουν από διάφορους ιστότοπους ή πλατφόρμες μέσων κοινωνικής δικτύωσης. Μέχρι στιγμής το πιο δημοφιλές είναι το FaceBook που δεν χρειάζεται να συστηθεί σήμερα… </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 300, height: 300}}
                        source={require('../Images/images/1016.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}

