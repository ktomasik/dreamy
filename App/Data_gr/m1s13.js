import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμού ηλεκτρονικού ταχυδρομείου σε συσκευή κινητού τηλεφώνου Android (Samsung, Sony,
                    HTC ..)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 4: Εισάγετε την πλήρη διεύθυνση ηλεκτρονικού ταχυδρομείου σας και πατήστε τη χειροκίνητη ρύθμιση (χρησιμοποιώντας τον προηγούμενο
                    λογαριασμό ηλεκτρονικού ταχυδρομείου, τον κωδικό πρόσβασης και μια περιγραφή του λογαριασμού σας)
                </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img15.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
