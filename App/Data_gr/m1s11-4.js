import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S11_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμού ηλεκτρονικού ταχυδρομείου σε συσκευή κινητoύ iOS (iPhone, iPad ή iPod
                    touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 7: Όταν λάβετε τον κωδικό επαλήθευσης, καταχωρίστε τον στο ανάλογο πεδίο</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex:1, width: 320, height: 350}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
