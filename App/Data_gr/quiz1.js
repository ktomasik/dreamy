import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz1": {
            "question1": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Όνομα χρήστη",
                    "option2": "b) Κωδικός",
                    "option3": "c) Στοιχεία επικοινωνίας",
                    "option4": "d) Διεύθυνση κατοικίας"
                },
                "question": "Για να ανοίξετε έναν λογαριασμό ηλεκτρονικού ταχυδρομείου, ποιο από τα παρακάτω δεν απαιτείται;"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Η ανεπιθύμητη ηλεκτρονική αλληλογραφία και ο ιός ηλεκτρονικού ταχυδρομείου έχουν την ίδια έννοια"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Μπορεί να υπάρχουν διαφορές στις διαδικασίες δημιουργίας ενός λογαριασμού ηλεκτρονικού ταχυδρομείου στις διαφορετικές μάρκες έξυπνων κινητών τηλεφώνων (smartphones)"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Ο παγκόσμιος ιστός",
                    "option2": "b) Το διαδίκτυο",
                    "option3": "c) Ο λογαριασμός ηλεκτρονικού ταχυδρομείου",
                    "option4": "d) Η ανεπιθύμητη αλληλογραφία"
                },
                "question": "________ αποτελείται από εκατομμύρια αλληλένδετες ιστοσελίδες."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Για να χρησιμοποιήσετε το ηλεκτρονικό ταχυδρομείο, χρειάζεται να έχετε λογαριασμό."
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Τραπεζικός λογαριασμός",
                    "option2": "b) Λογαριασμός ηλεκτρονικού ταχυδρομείου",
                    "option3": "c) Διεύθυνση κατοικίας",
                    "option4": "d) Νούμερο τηλεφώνου"
                },
                "question": "Για να στείλετε ένα ηλεκτρονικό μήνυμα μέσω διαδικτύου, ποιο από τα παρακάτω είναι απαραίτητο;"
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Ένα όνομα κοινωνικής δικτύωσης",
                    "option2": "b) Ένας λογαριασμός ηλεκτρονικού ταχυδρομείου",
                    "option3": "c) Ένα σύστημα λογισμικού για την αναζήτηση πληροφοριών στο διαδίκτυο",
                    "option4": "d) Μια ιστοσελίδα"
                },
                "question": "Τι είναι μία μηχανή αναζήτησης;"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Ένας χρήστης  μπορεί να χρησιμοποιήσει το λογαριασμό του ηλεκτρονικού ταχυδρομείου και μέσω ενός έξυπνου κινητού τηλέφωνου (smartphone)."
            },
            "question9": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Για να κάνετε αναζήτηση στο διαδίκτυο.",
                    "option2": "b) Για να καθαρίσετε ιούς.",
                    "option3": "c) Για να μεταφέρετε δεδομένα στο διαδίκτυο.",
                    "option4": "d) Για να διαχωρίσετε το όνομα του χρήστη από τη διεύθυνση διαδικτύου του."
                },
                "question": "Που χρησιμοποιείται το @;"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Μια μηχανή αναζήτησης.",
                    "option2": "b) Μια διεύθυνση ηλεκτρονικού ταχυδρομείου",
                    "option3": "c) Ένα είδος ιού.",
                    "option4": "d) Ανεπιθύμητη αλληλογραφία."
                },
                "question": "Τι είναι το Google;"
            }
        }
    }
};

export default class M1Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz1;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
        /*console.log('countCheck: ' + this.state.countCheck);
        console.log('punkty: ' + this.score)*/
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
