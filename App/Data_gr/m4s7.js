import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Εγγραφή σε Επαγγελματικές Ενώσεις και οργανισμούς απασχόλησης</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>Για την ΤΟΥΡΚΙΑ</Text>
                <FlatList
                    data={[
                        {key: 'KOSGEB ( Οργανισμός Ανάπτυξης Μικρών και Μεσαίων Επιχειρήσεων )'},
                        {key: 'Γενική Διεύθυνση για τη Θέση των Γυναικών ( KSGM )'},
                        {key: 'Ένωση Επιχειρήσεων Τουρκίας ( İŞKUR )'},
                        {key: 'Ένωση των Επιμελητηρίων και των Κοινοτικών Ανταλλαγών της Τουρκίας (TOBB)'},
                        {key: 'Υποστήριξη από το Υπουργείο της Δημοκρατίας της Τουρκίας και των Κοινωνικών Πολιτικών'},
                        {key: 'Τουρκικό πρόγραμμα μικροχρηματοδότησης Grameen'},
                        {key: 'Τραπεζικά δάνεια για γυναίκες'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για τη ΣΛΟΒΕΝΙΑ</Text>
                <Text style={{fontWeight: 'bold'}}>Μη χρηματοοικονομική κρατική στήριξη</Text>
                <FlatList
                    data={[
                        {key: 'Σημεία VEM'},
                        {key: 'Θερμοκοιτίδες επιχειρήσεων'},
                        {key: 'Θερμοκοιτίδες Πανεπιστημίων'},
                        {key: 'Τεχνολογικά πάρκα'},
                        {key: 'Πρωτοβουλία start up επιχειρήσεων στη Σλοβενία'},
                        {key: 'Το Ευρωπαϊκό Δίκτυο Επιχειρήσεων'},
                        {key: 'SPIRIT	Σλοβενίας'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Οικονομικές ενισχύσεις στη Σλοβενία</Text>
                <FlatList
                    data={[
                        {key: 'Σλοβενικό Ταμείο Επιχειρήσεων'},
                        {key: 'Υπηρεσία Απασχόλησης της Σλοβενίας'},
                        {key: 'Ταμείο Περιφερειακής Ανάπτυξης της Σλοβενίας'},
                        {key: 'Τραπεζικά δάνεια'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Άλλες ενισχύσεις </Text>
                <FlatList
                    data={[
                        {key: 'Εμπορικό και Βιομηχανικό Επιμελητήριο'},
                        {key: 'Βιοτεχνικό και Επαγγελματικό Επιμελητήριο της Σλοβενίας'},
                        {key: 'Επιχειρηματικοί Άγγελοι της Σλοβενίας'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για την ΠΟΛΩΝΙΑ</Text>
                <FlatList
                    data={[
                        {key: 'Χρηματοδότηση της Ευρωπαϊκής Ένωσης για γυναίκες-επιχειρηματίες (ΜΚΟ, Δημόσιοι Φορείς, κτλ.)'},
                        {key: 'Ευρωπαϊκό Κοινωνικό Ταμείο (Επιχειρησιακό Πρόγραμμα PO WER Ανάπτυξη Εκπαίδευση Γνώση)'},
                        {key: 'Μη κοινοτικές πηγές ( κυβερνητικές, ιδιωτικές κτλ.)'},
                        {key: 'Πολωνική Υπηρεσία Ανάπτυξης Επιχειρήσεων'},
                        {key: 'Το Ταμείο Δανειοδότησης για τις Γυναίκες'},
                        {key: 'Επιχειρηματικοί Άγγελοι'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για την ΕΛΛΑΔΑ</Text>
                <FlatList
                    data={[
                        {key: 'Πρόγραμμα του ΟΑΕΔ ( για νέες επιχειρηματίες)'},
                        {key: 'Τραπεζικά δάνεια'},
                        {key: 'Εθνικό Στρατηγικό Πλαίσιο Αναφοράς  2014-2020 (ΕΣΠΑ)'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για τη ΓΑΛΛΙΑ</Text>
                <FlatList
                    data={[
                        {key: 'CFE'},
                        {key: 'Εγγραφή στο Μητρώο Εμπορίου και Εταιρειών ( RFS )'},
                        {key: 'ACCRE- Έχει δημιουργηθεί για να βοηθήσει τους αναζητούντες εργασία και να διευκολύνει τη δημιουργία επιχειρήσεων από αυτούς'},
                        {key: 'Η BPI Γαλλίας (Τράπεζα Δημόσιων Επενδύσεων )'},
                        {key: 'PRI (Περιφερειακή Εταιρική Σχέση Καινοτομίας)'},
                        {key: 'Επιχειρηματικοί Άγγελοι'},
                        {key: 'Γυναικεία Επιχειρηματικότητα'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
