import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_4 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να πουλήσετε τα προϊόντα σας στο Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 4: Σαν συνοδευτικό κείμενο για τις φωτογραφίες σας, προσθέστε μια περιγραφή του εικονιζόμενου προϊόντος και προσδιορίστε την τιμή του,
                    ενώ μη ξεχνάτε κάθε φορά να αναφέρετε τη διεύθυνση e-mail σας, ώστε να μπορεί εύκολα ένας πελάτης να έρθει σε επαφή μαζί σας.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522778-PIXKA7-878.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
