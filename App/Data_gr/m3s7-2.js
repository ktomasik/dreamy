import React, {Component} from 'react'
import {Text, Image, View, TouchableHighlight, Modal} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen4 = [{
    url: '',
    props: {
        source: require('../Images/m3/img30.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m3/img31.png')
    }
}];

export default class M3S7_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ακολουθήστε τα παρακάτω βήματα για να φωτογραφίσετε τα προϊόντα σας και να αναρτήσετε τις φωτογραφίες τους στο etsy.com</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen4}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 14: Συμπληρώστε τον παρακάτω πίνακα και κάντε κλικ στο “Save and continue” [Αποθήκευση και συνέχεια]:</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img30.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img31.png')}
                        resizeMode="contain"/>

                </TouchableHighlight>
            </View>
        )
    }
}
