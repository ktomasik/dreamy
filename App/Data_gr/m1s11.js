import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμού ηλεκτρονικού ταχυδρομείου σε συσκευή κινητoύ iOS (iPhone, iPad ή iPod
                    touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 4: Πληκτρολογήστε το όνομά σας, τη διεύθυνση ηλεκτρονικού ταχυδρομείου σας (από τον προηγούμενο λογαριασμό ηλεκτρονικού ταχυδρομείου
                    που δημιουργήσατε), τον κωδικό πρόσβασης και μια περιγραφή του λογαριασμού σας</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img11.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
