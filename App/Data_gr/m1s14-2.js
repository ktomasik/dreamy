import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμού ηλεκτρονικού ταχυδρομείου σε συσκευή κινητού τηλεφώνου Android (Samsung, Sony,
                    HTC ..)</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>
                    Συγχαρητήρια για τη δημιουργία του λογαριασμού ηλεκτρονικού ταχυδρομείου σας!
                </Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Μπορείτε να εγγραφείτε σε όλα τα κοινωνικά κανάλια με το λογαριασμό ηλεκτρονικού ταχυδρομείου που
                    δημιουργήσατε.</Text>
                <Text style={{marginTop: 10}}>Η επόμενη ενότητα θα σας εξηγήσει πώς να ρυθμίσετε λογαριασμούς κοινωνικών δικτύων.</Text>
            </View>
        )
    }
}
