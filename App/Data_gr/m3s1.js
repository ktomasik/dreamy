import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Τι είναι το ηλεκτρονικό επιχειρείν και το ηλεκτρονικό εμπόριο;</Text>
                <Text>Ο όρος ‘’ηλεκτρονικό επιχερείν’’ (e-business) αναφέρεται στη χρήση του Διαδικτύου, ενδοδικτύων, εξωδικτύων, καθώς και συνδυασμών αυτών, για τη διεξαγωγή οποιασδήποτε
                    επιχειρηματικής δραστηριότητας.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img1.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img2.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>Πώς και πού μπορείτε να πουλήσετε τις χειροποίητες δημιουργίες σας μέσω Διαδικτύου;</Text>
                <Text>Το Διαδίκτυο είναι μια τεράστια αγορά στην οποία μπορείτε να διαφημίσετε και να πουλήσετε τα χειροποίητα προϊόντα σας σε πελάτες από όλο τον κόσμο.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img3.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>Οι καλύτερες ιστοσελίδες για να πουλήσετε τις χειροτεχνίες σας</Text>
                <Text style={styles.dmlH3}>Etsy</Text>
                <Text>Η Etsy (<Text style={{color: 'blue'}}
                                    onPress={() => Linking.openURL('https://www.etsy.com/')}>etsy.com</Text>) είναι μια ενεργή κοινότητα που απαρτίζεται από 30 εκατομμύρια
                    εγγεγραμμένες επιχειρήσεις. Εντός της θα βρείτε μια ευρεία γκάμα διαθέσιμων προς πώληση αντικειμένων, όπως π.χ. εικαστικό εξοπλισμό, χειροποίητα προϊόντα, και διάφορα εξαρτήματα.
                </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img4.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Amazon</Text>
                <Text>Η Amazon προσφέρει τη μεγαλύτερη ποικιλία σε βιβλία, περιοδικά, μουσική, DVD’s, βίντεο, ηλεκτρονικά, υπολογιστές, λογισμικό, ρούχα & αξεσουάρ, παπούτσια, καθώς και μια εύχρηστη
                    πλατφόρμα για να διαφημίσετε και να πουλήσετε τα προϊόντα σας. (<Text style={{color: 'blue'}}
                                                                                          onPress={() => Linking.openURL('https://www.amazon.com/')}>amazon.com</Text>)</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img5.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Bonanza</Text>
                <Text>Η Bonanza (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.bonanza.com/')}>bonanza.com</Text>) προσφέρει μια πολύ απλή εμπειρία πλοήγησης στον χρήστη,
                    ενώ
                    η καλοσχεδιασμένη της ιστοσελίδα θα κάνει την όλη διαδικασία πώλησης των αντικειμένων σας απολαυστική! Με λίγα κλικ, φτιάξτε εύκολα το δικό σας προφίλ, αναρτήστε τα προς πώληση
                    αντικείμενά σας και ξεκινήστε να πουλάτε! </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img6.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>eBay</Text>
                <Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.ebay.com/')}>eBay</Text> Το γνωστό σε όλους eBay αδιαμφισβήτητα άλλαξε τον κόσμο του ηλεκτρονικού
                    εμπορίου. Με περισσότερους από 100 εκατομμύρια αγοραστές, το eBay αποτελεί την πιο αναγνωρίσιμη πλατφόρμα ηλεκτρονικού εμπορίου στον κόσμο.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img7.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>ArtFire</Text>
                <Text>Η ArtFire (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.artfire.com/')}>artfire.com</Text>) είναι μια ταχέως αναπτυσσόμενη διαδικτυακή αγορά πώλησης
                    χειροποίητων προϊόντων από καλλιτέχνες σε όλο τον κόσμο. Με περισσότερους από 30.000 εγγεγραμμένους ενεργούς πωλητές, η ArtFire παρέχει μια ευκολόχρηστη πλατφόρμα που επιδέχεται
                    και ορισμένες τροποποιήσεις από εσάς.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img8.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>DaWanda</Text>
                <Text>H DaWanda (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('http://en.dawanda.com/')}>dawanda.com</Text>) είναι μια από τις κορυφαίες διαδικτυακές αγορές για πώληση
                    μοναδικών και χειροποίητων αντικειμένων. Με περισσότερους από 280.000 ενεργούς πωλητές, η Bonanza αποτελεί μια ελκυστική επιλογή για τον κάθε ενδιαφερόμενο αγοραστή.</Text>
                <Text style={styles.dmlH3}>Zibbet</Text>
                <Text>Στη <Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.zibbet.com/')}>Zibbet</Text> φιλοξενούνται διαδικτυακές αγοραπωλησίες μεγάλου εύρους χειροτεχνιών,
                    από έργα τέχνης και φωτογραφίες μέχρι παλαϊκά αντικείμενα και χειροτεχνικό εξοπλισμό.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img9.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
