import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S3_2 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Τιμολόγηση στο κινητό</Text>
                <Text style={styles.dmlH3}>For iOS</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 2:</Text> Κάντε κλικ στο κουμπί “Λήψη” για το πρότυπο που επιλέξατε και στη συνέχεια πληκτρολογήστε
                    τον κωδικό πρόσβασης στο αναγνωριστικό της Apple</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_20_3319624.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
