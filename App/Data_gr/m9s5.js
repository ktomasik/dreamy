import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S5 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Δημιουργία εταιρικού λογαριασμού στο Instagram </Text>
                <Text>Δημιουργία λογαριασμού: Στην ενότητα 2 εξηγήσαμε πώς να ανοίξουμε έναν λογαριασμό στο Instagram. Όπως αναφέρθηκε στην ενότητα αυτή, μπορείτε να κατεβάσετε την εφαρμογή του Instagram για κινητά είτε από το App Store (για iOS) είτε από το κατάστημα της Google Play (για Android τεχνολογία) είτε από το Windows Phone Store (για το Windows τηλέφωνό σας).
                    {"\n\n"}Όταν η εφαρμογή κατέβει, αγγίξτε το εικονίδιο της εφαρμογής στο κινητό σας τηλέφωνο, ώστε να ανοίξετε το Instagram.

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
