import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Twitter για επαγγελματική χρήση</Text>
                <Text styl={{marginTop: 10}}>Πρόκειται για ένα από τα πιο διαδεδομένα Social Media. Μπορείτε να απευθυνθείτε στους πελάτες σας άμεσα μέσω του Twitter. Οι ακόλουθοί σας στο Twitter
                    είναι οι εν δυνάμει πελάτες σας. Χρειάζεται να δημιουργήσετε το δικό σας κοινό, προκειμένου να δείτε τα πλεονεκτήματα της πλατφόρμας ως εργαλείο Marketing. Μπορείτε να φτιάξετε τη
                    δική σας λίστα με ακολούθους ή να γίνετε μέλος ενός γκρουπ που έχει ήδη δημιουργηθεί και το θέμα του σας αφορά.
                    {"\n\n"}Προκειμένου να δημιουργήσετε έναν εταιρικό λογαριασμό στο Twitter, πρέπει πρώτα να έχετε έναν προσωπικό λογαριασμό στο Twitter. Στην ενότητα 2 δόθηκαν οδηγίες – πηγαίνετε
                    στην αναζήτηση του Google. Γράψτε “Twitter” και πατήστε αναζήτηση. Μπορείτε να έχετε πρόσβαση και από κινητά, επιλέγοντας «mobile.twitter.com» και μετά πατήστε Είσοδο στο Twitter.
                    (Για λεπτομέρειες δείτε και την ενότητα 2)

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/twitter.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
