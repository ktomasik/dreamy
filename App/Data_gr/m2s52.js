import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S52 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε μια σελίδα στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 8: Προσθέστε μια φωτογραφία προφίλ από την γκαλερί σας, πατώντας το κουμπί ΠΡΟΣΘΗΚΗ ΦΩΤΟΓΡΑΦΙΑΣ, επιλέξτε φωτογραφίες και κάντε κλικ
                    στην επιλογή ΕΓΚΑΤΑΣΤΑΣΗ και ΑΠΟΘΗΚΕΥΣΗ. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img105.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img106.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img107.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
