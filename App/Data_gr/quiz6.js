import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz6": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Μόνο στο Google play.",
                    "option2": "b) Μόνο στο Apple store.",
                    "option3": "c) Σε κανένα από τα παραπάνω.",
                    "option4": "d) Και στα 2 παραπάνω."
                },
                "question": "Πού μπορεί να γίνει λήψη ενός προτύπου τιμολογίου για χρήση σε iphone της apple;"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Το Apple Store μπορεί να χρησιμοποιηθεί για τη δημιουργία τιμολογίου σε τηλέφωνο Android."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Fresh books",
                    "option2": "b) Moon Invoice",
                    "option3": "c) Wave",
                    "option4": "d) Pinterest"
                },
                "question": "Ποια εφαρμογή σας δίνει τη δυνατότητα να εκδώσετε τιμολόγιο σε κινητά apple και android;"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Το να υπάρχει το λογότυπο στο τιμολόγιο δεν είναι απολύτως απαραίτητο, αλλά συνιστάται."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Επωνυμία και επάγγελμα",
                    "option2": "b) Στοιχεία επικοινωνίας, τηλέφωνο κτλ.",
                    "option3": "c) ΑΦΜ και ΦΠΑ",
                    "option4": "d) Όλα τα παραπάνω"
                },
                "question": "Ποιες πληροφορίες πρέπει να εμφανίζονται σε ένα τιμολόγιο;"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Η ταυτότητα του πωλητή.",
                    "option2": "b) Λογαριασμός στα μέσα κοινωνικής δικτύωσης.",
                    "option3": "c) Λεπτομέρειες σχετικά με το ΦΠΑ.",
                    "options": "d) Στοιχεία επικοινωνίας."
                },
                "question": "Ποιο από τα παρακάτω στοιχεία δεν απαιτείται σε ένα τυπικό συμβόλαιο διαδικτυακών αγορών;"
            },
            "question7": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Ο προμηθευτής δεν μπορεί να θεωρηθεί υπεύθυνος για τα προϊόντα που πωλούνται με την επωνυμία της επιχείρησής του."
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Δεν αξίζει να είναι ασφαλισμένα τα εμπορεύματα."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Ένας χώρος για την ταξινόμηση των εγγράφων.",
                    "option2": "b) Μια δωρεάν και βολική πλατφόρμα ηλεκτρονικής υπογραφής, που χρησιμοποιείται από κινητά τηλέφωνα",
                    "option3": "c) Ένα έγγραφο που πρέπει να υπογράφεται σε κάθε πώληση.",
                    "option4": "d) Τίποτα από τα παραπάνω."
                },
                "question": "Τι είναι το DocuSign;"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Cash board",
                    "option2": "b) DocuSign",
                    "option3": "c) DropBox",
                    "option4": "d) Pinterest"
                },
                "question": "To _______ προσφέρει ένα πρότυπο, που μπορείτε να κατεβάσετε και να ανοίξετε στο Microsoft Word."
            }
        }
    }
};

export default class M6Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz6;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>

                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
