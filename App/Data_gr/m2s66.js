import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S66 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα ιστολόγιο ( Blog )</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 23: Συμπληρώστε τον τίτλο Poste, προσθέστε κάποιο νέο περιεχόμενο και κάντε κλικ στο εικονίδιο αποστολής ( εικονίδιο βέλους / χαρτιού
                    ). </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img141.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img142.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img143.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
