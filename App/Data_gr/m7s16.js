import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S16 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Επιχορηγήσεις μικρών επιχειρήσεων για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Πολωνία:</Text>

                <Text style={styles.dmlH3}>Χρηματοδότηση της Ε.Ε.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Σ’ αυτό το site, κάθε επιχειρηματίας ή μελλοντικός επιχειρηματίας (αλλά και ΜΚΟ, δημόσιοι φορείς κλπ.) μπορεί να βρει ενδιαφέρουσες πληροφορίες σχετικά με τη διαθέσιμη χρηματοδότηση.',
                                val2: 'https://www.funduszeeuropejskie.gov.pl/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Πολωνική Υπηρεσία Ανάπτυξης Επιχειρήσεων</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Στην Πολωνία, η Υπηρεσία Ανάπτυξης Επιχειρήσεων προσφέρει δάνειο που αφορά μόνο γυναίκες, το οποίο στοχεύει στη στήριξη της επαγγελματικής ενεργοποίησης των γυναικών και στη βελτίωση της κατάστασής τους στην αγορά εργασίας, ενθαρρύνοντας τις γυναίκες να ιδρύσουν δική τους επιχείρηση. Το Ταμείο Δανείων για γυναίκες στοχεύει να συμβάλει στη μείωση του προβλήματος της ανεργίας σε αυτή την κοινωνική ομάδα. Οι γυναίκες μπορούν να υποβάλουν αίτηση για δάνειο ύψους 5.000 -10.000€ (20 και 40 χιλιάδες PLN).',
                                val2: 'http://en.parp.gov.pl/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
