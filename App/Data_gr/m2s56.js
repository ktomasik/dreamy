import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S56 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}> Δημιουργήστε ένα ιστολόγιο ( Blog )</Text>
                <Text style={{marginTop: 10}}>Πριν μπορέσετε να ξεκινήσετε το blogging, θα πρέπει να επιλέξετε μια πλατφόρμα για τη δημοσίευση του ιστοτόπου σας. Υπάρχουν πολλές ελεύθερες,
                    φιλοξενούμενες επιλογές, όπως το πολύ δημοφιλές WordPress και το Blogger. Και οι δύο αυτές πλατφόρμες διαθέτουν εφαρμογές που θα επιτρέπουν στους χρήστες να συνθέτουν, να
                    επεξεργάζονται και να δημοσιεύουν τις αναρτήσεις τους εν κινήσει. </Text>
                <Text style={{marginTop: 10}}>Η βασική διαφορά μεταξύ του Blogger και του WordPress είναι ότι ενώ το Blogger είναι λίγο πιο απλό να το ρυθμίσετε και να το χρησιμοποιήσετε,  το WordPress είναι ευκολότερο να το προσαρμόσετε και τελικά να μεταβείτε σε έναν αυτο-φιλοξενούμενο ιστότοπο,  όταν υπερβείτε τα όρια των δωρεάν προσφορών του.  Όποιο και αν επιλέξετε,  υπάρχουν διαθέσιμες επίσημες εφαρμογές για τις μεγάλες κινητές πλατφόρμες ( Bozzo 2014 ).</Text>
                <Text style={{marginTop: 10}}>Σε αυτό το σεμινάριο θα χρησιμοποιήσουμε το Blogger λόγω της απλότητας… </Text>
            </View>
        )
    }
}
