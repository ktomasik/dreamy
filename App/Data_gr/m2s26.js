import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S26 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 13: Επιλέξτε τις σχετικές επαφές για να ακολουθήσετε το δημοσιευμένο περιεχόμενό τους. Όταν τελειώσετε, πατήστε «επόμενο». </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img47.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img48.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img49.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
