import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import { Button } from 'react-native-elements';
import {quizStyles} from '../Containers/Styles/general';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz3": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος",
                },
                "question": "Το ηλεκτρονικό εμπόριο άρχισε να εισέρχεται στη ζωή μας από τη δεκαετία του 1950, όταν ξεκίνησαν να χρησιμοποιούνται οι υπολογιστές."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος",
                },
                "question": "Οι λογαριασμοί Instagram Business δεν παρέχουν περισσότερες πληροφορίες για τη μάρκα σας, ούτε αποτελούν εργαλείο μέτρησης και ανάλυσης της επιτυχίας σας."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Το Etsy δεν χρεώνει για τη δημιουργία ενός καταστήματος..",
                    "option2": "b) Η Bonanza σας επιτρέπει να καταχωρίσετε τα στοιχεία σας δωρεάν.",
                    "option3": "c) Στο Zibbet επιτρέπεται να πουλήσετε ηλεκτρικά οχήματα.",
                    "option4": "d) Δεν υπάρχει καμία χρέωση για το άνοιγμα ενός καταστήματος στο DaWanda."
                },
                "question": "Ποιο από τα παρακάτω δεν ισχύει;"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος",
                },
                "question": "Το ηλεκτρονικό εμπόριο συνιστά αγορά και πώληση αγαθών και υπηρεσιών σε μια ηλεκτρονική πλατφόρμα."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Το ανθρώπινο μυαλό τείνει να ενσωματώνει κάτι που είναι αφηρημένο.",
                    "option2": "b) Οι ποιοτικές εικόνες προϊόντων αποτελούν βασικό παράγοντα για να παραμείνει κανείς στην ιστοσελίδα ενός καταστήματος.",
                    "option3": "c) Οι μελέτες που εξετάζουν τις οφθαλμικές κινήσεις, δείχνουν ότι οι επισκέπτες των καταστημάτων έλκονται αρχικά από τα οπτικά μηνύματα.",
                    "option4": "d) Η σύνδεση μεταξύ του λογότυπου και της επωνυμίας δεν είναι αναπόφευκτη."
                },
                "question": "Ποιο από τα παρακάτω δεν ισχύει σχετικά με τη σημασία της εικόνας των προϊόντων;"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος",
                },
                "question": "Η χρησιμοποίηση Hashtag στο instagram βοηθά τα προϊόντα να εμφανίζονται συχνότερα στην αρχική σελίδα των χρηστών."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Εστιάστε στην αφήγηση ιστοριών για να αυξήσετε τη συμμετοχή",
                    "option2": "b) Ανοίξτε έναν λογαριασμό στο instagram.",
                    "option3": "c) Ανοίξτε πλαστούς λογαριασμούς για να επαινέσετε το προϊόν.",
                    "option4": "d) Επεξεργαστείτε και μετατρέψτε την αρχική εικόνα. ",
                },
                "question": "Ποιο από τα παρακάτω αποτελεί επιτυχημένο τρόπο πώλησης στο instagram;"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Στην ηλεκτρονική πώληση χειροτεχνικών προϊόντων, δεν είναι τόσο σημαντική η χρήση μιας πραγματικής εικόνας όσο η τιμή του προϊόντος."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Σωστό",
                    "option2": "b) Λάθος"
                },
                "question": "Ο σχεδιασμός του λογότυπου πρέπει να αντιπροσωπεύει το προϊόν και την επιχειρηματική σας φιλοσοφία."
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Πάνω από το 60% των ανθρώπων που αγοράζουν από το διαδίκτυο θεωρεί ότι το πρωταρχικό κριτήριο, που επηρεάζει το αν θα προβούν σε μία αγορά ή όχι είναι η τιμή.",
                    "option2": "b) Το μεγαλύτερο πλεονέκτημα της Στρατηγικής Τιμολόγησης του Ηλεκτρονικού εμπορίου με βάση το Κόστος είναι η απλότητα.",
                    "option3": "c) Η τιμολόγηση στο Ηλεκτρονικό εμπόριο βάσει των τιμών των ανταγωνιστών μπορεί να οδηγήσει στην πώληση του προϊόντος σας σε χαμηλότερη τιμή από την επιθυμητή.",
                    "option4": "d) Η τιμολόγηση του Ηλεκτρονικού Εμπορίου με βάση την Αξία υπόσχεται βραχυπρόθεσμο κέρδος.",
                },
                "question": "Ποιο από τα παρακάτω δεν ισχύει;"
            }
        }
    }
};

export default class M3Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz3;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
