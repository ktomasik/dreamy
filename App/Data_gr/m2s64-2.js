import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S64_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα ιστολόγιο ( Blog )</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 20: Μπορείτε να δείτε το περιεχόμενο του ιστολογίου σας κάνοντας κλικ στην επιλογή Εμφάνιση ιστολογίου. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img137.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img138.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
