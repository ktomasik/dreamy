import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Φορολογικά </Text>
                <Text>Φορολογική υποχρέωση ονομάζεται το χρηματικό ποσό, που ένα άτομο οφείλει στις φορολογικές αρχές (Cameron 2017).
                    {"\n\n"}Η κυβέρνηση χρησιμοποιεί τα χρήματα, που αντλεί από τους φόρους για τη χρηματοδότηση της διοίκησης και των κοινωνικών προγραμμάτων.
                    {"\n\n"}Σε γενικές γραμμές, η φορολογική υποχρέωση υπολογίζεται συνήθως ως ποσοστό του εισοδήματος ενός ατόμου και ποικίλλει ανάλογα με το εισόδημα.

                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για την ΤΟΥΡΚΙΑ</Text>
                <FlatList
                    data={[
                        {key: 'Είναι απαραίτητο να δηλώσετε μια διεύθυνση. Η διεύθυνση κατοικίας μπορεί να χρησιμοποιείται και ως η διεύθυνση, που αναγράφεται στο τιμολόγιο.'},
                        {key: 'Κατά τη διάρκεια της αίτησης για την ίδρυση επιχείρησης, δεν τρέχει η φορολόγηση.'},
                        {key: 'Μετά την αξιολόγηση της φορολογικής υπηρεσίας, η εφορία στέλνει στον φορολογούμενο σημείωμα με το ποσόν που οφείλει.'},
                        {key: 'Πρέπει να γίνει εγγραφή στη « Συνομοσπονδία Τούρκων Εμπόρων και Βιοτεχνών ».'},
                        {key: 'Πρέπει να υποβληθεί αίτηση στον δήμο για άδεια λειτουργίας.'},
                        {key: 'Οι φορολογούμενοι που υπόκεινται στη φορολογία μικρών επιχειρήσεων θα λάβουν τα έγγραφα από τα Επιμελητήρια ή τις Επαγγελματικές Ενώσεις.'},
                        {key: 'Οι φορολογούμενοι πρέπει να δηλώνουν το εισόδημά τους με ετήσια δήλωση.'},
                        {key: 'Η δήλωση πρέπει να γίνεται στην αρμόδια φορολογική υπηρεσία. Ως αποτέλεσμα, ο φορολογούμενος θα πληρώσει τον φόρο βάσει τιμολογίων (Εθνική Έκθεση Τουρκίας, 2018).'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για τη ΣΛΟΒΕΝΙΑ</Text>
                <FlatList
                    data={[
                        {key: 'ΦΠΑ ( Φόρος Προστιθέμενης Αξίας )'},
                        {key: 'Εταιρικός φόρος εισοδήματος'},
                        {key: 'Προσωπικός Φόρος Εισοδήματος'},
                        {key: 'Εισφορές κοινωνικής ασφάλισης'},
                        {key: 'Φόρος Μεταβίβασης Ακίνητης Περιουσίας'},
                        {key: 'Φόρος Κεφαλαιουχικών Κερδών'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Επιλέξτε επιχειρηματική φόρμα.</Text>
                <FlatList
                    data={[
                        {key: 'Για τις ατομικές επιχειρήσεις, η φορολογία συνιστά ένα εισόδημα που απορρέει από μια επιχειρηματική, γεωργική, δασική, επαγγελματική ή άλλη ανεξάρτητη δραστηριότητα αυτοαπασχολούμενου.'},
                        {key: 'Η Εταιρεία Περιορισμένης Ευθύνης, είναι ένα νομικό πρόσωπο και φορολογείται με εταιρικό φόρο εισοδήματος.'},
                        {key: 'Οι φόροι που οφείλει ένα άτομο που ασκεί δραστηριότητα πωλήσεων μέσω του διαδικτύου, η οποία είναι υποκείμενο σε φόρο, προσδιορίζονται και ως προς τον ΦΠΑ. Π'},
                        {key: 'ρέπει να υπολογίζονται με βάση τα τιμολόγια, που εκδίδονται κατά την παράδοση του προϊόντος στον πελάτη στη Σλοβενία (Εθνική Έκθεση Σλοβενίας, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για την ΠΟΛΩΝΙΑ</Text>
                <Text>Οι φορολογικές υποχρεώσεις των ατόμων, που επιθυμούν να ασκήσουν τη δική τους επιχείρηση, καταβάλλονται με βάση τη φορολογική τους δήλωση εισοδήματος, όπως ισχύει και για τους
                    εργαζόμενους με πλήρη απασχόληση. Το άτομο πρέπει να επιλέξει ένα από τα παρακάτω:</Text>
                <FlatList
                    data={[
                        {key: 'Φόρος σύμφωνα με τις γενικές διατάξεις,'},
                        {key: 'Φόρος 19% (κατ \'αποκοπή φόρος),'},
                        {key: 'Εφάπαξ ποσό με βάση το εγγεγραμμένο εισόδημα,'},
                        {key: 'Φορολογική κάρτα (Εθνική Έκθεση Πολωνίας, 2018).'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για την ΕΛΛΑΔΑ</Text>
                <FlatList
                    data={[
                        {key: 'Οι ιδιώτες και οι επιχειρήσεις πρέπει να υποβάλλουν ηλεκτρονική φορολογική δήλωση διαδικτυακά, βάσει της οποίας φορολογούνται. Η δήλωση υποβάλλεται μέσω του ηλεκτρονικού συστήματος της Ανεξάρτητης Αρχής Δημόσιων Εσόδων.'},
                        {key: 'Στο τέλος κάθε μήνα (οι μικρές επιχειρήσεις κάθε τρίμηνο), υποβάλλουν τον κατάλογο των τιμολογίων και του ΦΠΑ.'},
                        {key: 'Στις ατομικές επιχειρήσεις, οι φορολογικές υποχρεώσεις καταβάλλονται με βάση το φόρο εισοδήματος.'},
                        {key: 'Το ίδιο ισχύει και για τους εργαζόμενους με πλήρη απασχόληση – στην περίπτωση αυτήν όμως προβλέπονται κάποιες φοροαπαλλαγές.'},
                        {key: 'Ανεξάρτητα από τα κέρδη, όλα τα νομικά πρόσωπα φορολογούνται με ποσοστό 29%.'},
                        {key: 'Τα μερίσματα φορολογούνται με συντελεστή 15% (Εθνική Έκθεση Ελλάδας, 2018).'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Για τη ΓΑΛΛΙΑ</Text>
                <Text>Taxation of profits depends on the legal structure of the business. Entities may subject to Income Tax (IR) or Corporate Tax(CI). Οι εταιρείες υπόκεινται σε:</Text>
                <FlatList
                    data={[
                        {key: 'Φορολόγηση των κερδών τους,'},
                        {key: 'Δημοτικά τέλη (CET),'},
                        {key: 'ΦΠΑ.'},
                        {key: 'Οι ατομικές εταιρείες (βιοτέχνες, έμποροι), οι ελεύθεροι επαγγελματίες και οι μονοπρόσωπες ΕΠΕ πρέπει να πληρώσουν φόρο εισοδήματος.'},
                        {key: 'Οι εταίροι φορολογούνται προσωπικά όσον αφορά τον φόρο εισοδήματος μόνο από τους μισθούς ή τα μερίσματα ( Εθνική Έκθεση Γαλλίας, 2018 ).'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
