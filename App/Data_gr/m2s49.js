import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S49 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Χρησιμοποιώντας τις ιστοσελίδες της κοινωνικής δικτύωσης</Text>
                <Text style={styles.dmlH2}>Δημιουργήστε μια σελίδα στο Facebook </Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 1: Πηγαίντε στο FaceBook, συνδεθείτε στο λογαριασμό σας… </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img98.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
