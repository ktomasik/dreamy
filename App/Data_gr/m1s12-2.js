import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S12_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμού ηλεκτρονικού ταχυδρομείου σε συσκευή κινητού τηλεφώνου Android (Samsung, Sony,
                    HTC ..)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 2: Μεταβείτε στο “Λογαριασμό” και, στη συνέχεια, επιλέξτε “Προσθήκη λογαριασμού”.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img13.png')}
                        resizeMode="contain"/>

                </View>
            </View>
        )
    }
}
