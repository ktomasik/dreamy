import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3_3 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ρύθμιση λογαριασμών στα Social Media για επαγγελματική χρήση</Text>
                <Text style={styles.dmlH3}>Ρύθμιση της σελίδας στο Facebook για επαγγελματική χρήση  </Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 2:</Text> Πατήστε επάνω στο κουμπί “Αρχική σελίδα” δίπλα στο όνομά σας
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
