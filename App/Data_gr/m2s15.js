import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S15 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 14: Μετά επιλέξτε τους φίλους σας και κάντε κλικ στο ΠΡΟΣΘΕΣΕ ΦΙΛΟΥΣ για να ξεκινήσετε την κοινωνική σας δικτύωση... </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img25.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
