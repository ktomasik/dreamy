import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S12_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Δημιουργήστε ένα λογαριασμό στο Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Βήμα 9: Στη συνέχεια,  μπορείτε να επιλέξετε να αποθηκεύσετε τον κωδικό πρόσβασης από την εφαρμογή. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img18.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
