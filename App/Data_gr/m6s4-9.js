import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";


const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img18.png')
    }
}];

export default class M6S4_9 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Πώς να δημιουργήσετε ένα τιμολόγιο στα Έγγραφα Google</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Βήμα 9:</Text> Αυτό σας φέρνει στη σελίδα προτύπων. Μπορείτε να προσθέσετε, να επεξεργαστείτε ή να
                    διαμορφώσετε κείμενο σ’ ένα υπολογιστικό φύλλο. Επιπλέον, μπορείτε να μοιράζεστε αρχεία και φακέλους με τους άλλους και να επιλέξετε αν εκείνοι θα μπορούν να προβάλλουν, να
                    επεξεργαστούν ή και να σχολιάσουν τα αρχεία και τους φακέλους αυτούς.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 280}}
                        source={require('../Images/m6/img18.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
