import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Εισαγωγή</Text>
                <Text>Μέχρι αυτή την ενότητα, έχετε μάθει πώς να δημιουργείτε έναν λογαριασμό email και να ρυθμίζετε τα κινητά σας τηλέφωνα. Παράλληλα, έχετε μάθει να φτιάχνετε λογαριασμούς στα social
                    media. Από την άλλη, μάθατε να φωτογραφίζετε τα προϊόντα σας και να ανεβάζετε τις φωτογραφίες σε πλατφόρμες. Ο σκοπός αυτής της ενότητας είναι να εξηγήσει πώς να ρυθμίσετε τους ήδη
                    υπάρχοντες λογαριασμούς σας στα social media ώστε να γίνουν εταιρικοί.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
