import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S26 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Μικροχρηματοδότηση κι άλλα Κεφάλαια Εκκίνησης για γυναίκες</Text>
                <Text style={styles.dmlH2}>Για την Πολωνία:</Text>
                <Text style={styles.dmlH3}>STARTUP ACADEMY</Text>
                <Text>Κατάρτιση, καθοδήγηση, καινοτόμες μέθοδοι οικοδόμησης νεοσύστατων επιχειρήσεων, προγράμματα επιτάχυνσης.</Text>

                <Text style={styles.dmlH3}>TWÓJ STARTUP</Text>
                <Text>Όλες οι διαδικασίες που αφορούν νομική και λογιστική συμβουλευτική, συμβουλευτικές υπηρεσίες πληροφορικής και μάρκετινγκ, εκπαίδευση.</Text>

                <Text style={styles.dmlH3}>Inkubator Technologiczny Podkarpckiego Parku Naukowo-Technologicznego</Text>
                <Text>Αίθουσες γραφείων, συμβουλευτικές υπηρεσίες, υποστήριξη ανάπτυξης.</Text>

                <Text style={styles.dmlH3}>Przedsiębiorcze kobiety 2.0</Text>
                <Text>Απευθύνεται σε γυναίκες που δεν εργάζονται, για να τις βοηθήσουν να ιδρύσουν δική τους επιχείρηση.</Text>

                <Text style={styles.dmlH3}>AIP</Text>
                <Text>Επιχειρηματικές, συμβουλευτικές υπηρεσίες, καθοδήγηση, λογιστική, νομική υποστήριξη και εκπαίδευση στη νεοσύστατη επιχειρηματικότητα.</Text>
            </View>
        )
    }
}
