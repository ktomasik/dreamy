import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Χρηματοδότηση βήμα προς βήμα στις χώρες - εταίρους του προγράμματος</Text>
                <Text style={styles.dmlH2}>Για την Γαλλία:</Text>
                <Text style={styles.dmlH3}>Για την περίπτωση που δεν ψάχνετε δουλειά: ΚΡΑΤΙΚΗ ΒΟΗΘΕΙΑ - ACCRE</Text>

                <Text style={styles.dmlH3}>Βήμα 3:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Σε περίπτωση ευνοϊκής απάντησης, η URSSAF θα σας εκδώσει πιστοποιητικό αποδοχής. Εάν όχι, αιτιολογεί και κοινοποιεί τον λόγο της απόρριψής. Αν δεν υπάρξει απόκριση μέσα σε ένα μήνα, τότε η αίτηση θεωρείται ότι έχει γίνει δεκτή.',
                                val2: 'https://www.legalstart.fr/fiches-pratiques/aides-creation-entreprise/aide-creation-entreprise-femmes/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
