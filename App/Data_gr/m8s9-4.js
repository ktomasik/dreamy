import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img15.png')
    }
}];

export default class M8S9_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>ΕΦΑΡΜΟΓΗ ΣΤΗΝ ΠΡΑΞΗ</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>ΒΗΜΑ 8: ΔΙΑΔΡΟΜΗ</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img15.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Συμβουλές για τη συνεργασία σας με τις εταιρείες αποστολών:</Text>
                <FlatList
                    data={[
                        {key: 'Το να ζητήσετε από τον πελάτη να σας υποδείξει την προτίμησή του (σχετικά με την εταιρεία, το υποκατάστημα κλπ) πριν από την αποστολή, ίσως είναι μια καλή ιδέα. Ενδεχομένως, εκείνος ή εκείνη να έχουν ένα συγκεκριμένο αίτημα σχετικά με την αποστολή.'},
                        {key: 'Το προϊόν πρέπει να χωρά στη συσκευασία. Όταν αυτή είναι η κατάλληλη, ίσως και να μειωθεί το κόστος αποστολής.'},
                        {key: 'Μια σύναψη συμφωνίας με την εταιρεία αποστολών ίσως μειώσει το κόστος αποστολής.'},
                        {key: 'Η συμφωνία αυτή ίσως προσφέρει ορισμένα πλεονεκτήματα, π.χ η παραλαβή της αποστολής στη διεύθυνσή σας, χωρίς επιβάρυνση.'},
                        {key: 'Πριν την αποστολή, πρέπει να ελεγχθούν τα υποκαταστήματα και ο χρόνος παράδοσης από την εταιρεία αποστολών.'},
                        {key: 'Η καταγραφή και η κοινοποίηση του αριθμού αποστολής στον πελάτη ενδεχομένως να συμβάλλει στη έγκαιρη παράδοση του δέματος.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

            </View>
        )
    }
}
