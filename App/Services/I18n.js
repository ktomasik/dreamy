import I18n from 'react-native-i18n';
import en from './locales/en';
import pl from './locales/pl';
import tr from './locales/tr';
import sl from './locales/sl';
import fr from './locales/fr';
import gr from './locales/gr';

I18n.fallbacks = true;

I18n.translations = {
    en,
    pl,
    tr,
    sl,
    fr,
    gr
};

export default I18n;

export const switchLanguage = (lang, component) =>
{
    I18n.locale = lang;
    component.forceUpdate();
};

export const updateLanguage = (lang) =>
{
    I18n.locale = lang;
};
