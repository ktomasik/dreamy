import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>What is e-business and e-commerce?</Text>
                <Text>Electronic business (e-business) refers to the use of the Web, Internet, intranets, extranets or some combinations to do the business.</Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img1.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img2.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>How and where to sell your handmade items online?</Text>
                <Text>The Internet is a unique marketing place to promote and sell handmade products to customers all
                    over the world.</Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img3.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>The best websites to sell crafts</Text>
                <Text style={styles.dmlH3}>Etsy</Text>
                <Text>Etsy (<Text style={{color: 'blue'}}
                                  onPress={() => Linking.openURL('https://www.etsy.com/')}>etsy.com</Text>) is a vibrant
                    community of 30 million creative businesses registered on the website. A wide range of products are
                    sold on Etsy including art supplies, handmade products and pieces. </Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img4.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Amazon</Text>
                <Text>Online shopping from the earth’s biggest selection of books, magazines, music, DVD’s, videos,
                    electronics, computers, software, apparel & accessories, shoes and a good platform to commercialize
                    the products. (<Text style={{color: 'blue'}}
                                         onPress={() => Linking.openURL('https://www.amazon.com/')}>amazon.com</Text>)</Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img5.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Bonanza</Text>
                <Text>Bonanza (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.bonanza.com/')}>bonanza.com</Text>) offers the most simplest navigation and an attractive site that will make you enjoy selling your handmade items to consumers. With few simple clicks, easily create your profile, upload listings and start selling.</Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img6.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>eBay</Text>
                <Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.ebay.com/')}>eBay</Text> is one of those sites that has quite literally changed the world of ecommerce. With over 100 million buyers, eBay is one of the most recognized online marketplace across the globe.</Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img7.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>ArtFire</Text>
                <Text>ArtFire (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.artfire.com/')}>artfire.com</Text>) is a fast-growing online marketplace for selling handmade products designed by artisans across the globe. With more than 30,000 registered active sellers, ArFire offers an easy-to-use platform which allows customization options too.</Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img8.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>DaWanda</Text>
                <Text>DaWanda (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('http://en.dawanda.com/')}>dawanda.com</Text>) is one of the leading online marketplace for selling unique and handmade items. With over 280,000 sellers active on DaWanda, it is a noble approach to attract buyers.</Text>
                <Text style={styles.dmlH3}>Zibbet</Text>
                <Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.zibbet.com/')}>Zibbet</Text> is an online marketplace to sell handmade crafts online for everything from fine art and photography to vintage and craft supplies.</Text>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img9.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
