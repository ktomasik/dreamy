import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S26 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Micro finance and Other Start-Up Funds for women</Text>
                <Text style={styles.dmlH2}>For Poland:</Text>
                <Text style={styles.dmlH3}>STARTUP ACADEMY</Text>
                <Text>training, mentoring, innovative start up building methods, acceleration programs</Text>

                <Text style={styles.dmlH3}>TWÓJ STARTUP</Text>
                <Text>pre-incubation, legal and accounting consulting, IT and marketing consulting, training</Text>

                <Text style={styles.dmlH3}>Inkubator Technologiczny Podkarpckiego Parku Naukowo-Technologicznego</Text>
                <Text>office rooms, consultancy services, development support</Text>

                <Text style={styles.dmlH3}>Przedsiębiorcze kobiety 2.0</Text>
                <Text>The project is addressed to non-working women to help them set up their own business</Text>

                <Text style={styles.dmlH3}>AIP</Text>
                <Text>Business Consulting, Mentoring and coaching, Accounting service, Legal support, Start-up Training</Text>
            </View>
        )
    }
}
