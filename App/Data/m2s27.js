import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create an Instagram account</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 14: click add a photo to add your profile photo. And choose the
                    source of your profile photo. You can download it from Face book, you can take a photo with phones’
                    camera or choose a photo from library.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img50.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img51.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
