import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S23 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Micro finance and Other Start-Up Funds for women</Text>
                <Text style={styles.dmlH2}>For Turkey:</Text>
                <Text style={styles.dmlH3}>Kredi Garanti Fonu (The Credit Guarantee Fund- KGF)</Text>
                <FlatList
                    data={[
                        {key: {val1: 'KGF is a non-profit incorporated company and acts as a guarantor for SMEs and non-SME enterprises that cannot get a loan due to insufficient collateral. Thus, KGF supports SMEs and non-SME enterprises in access to financing. The loan supports women entrepreneurs to grow their business or to remove their businesses from the difficult situation. ',val2: 'http://www.kgf.com.tr/index.php/tr/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Turkish Grameen Microfinance Program</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Turkish Grameen Microfinance Program (TGMP) is a non-profit economic foundation. Instead of traditional donations and “charity,” TGMP offers “microcredit” services to help reduce poverty in Turkey. The goal of the micro credit system is to help low-income women engage in sustainable income-generating activities and contribute to their family budgets. Unlike the formal (commercial) banking sector, micro credit loans are offered without requiring collateral or any documentation other than a person’s Turkish national ID card. Some microcredit products are listed below.',val2: 'http://www.tgmp.net/tr/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Basic Loan</Text>
                <Text>Basic Loan is the first type of loan for old and new members. New members may be granted a loan from 100 TL up to 1.000 TL and repayment of loans are made for 46 weeks. </Text>

                <Text style={styles.dmlH3}>Entrepreneurial Loan</Text>
                <Text>A type of loan for entrepreneur and successful members may be granted a loan from 1.000 TL up to 5.000 TL and the repayment of loans are made during the 46 weeks. </Text>

                <Text style={styles.dmlH3}>Digital Divide Loan</Text>
                <Text>In addition to the loans received by our members, this kind of loan is aimed at providing technological development for members. Thanks to this credit, members can have smart phones used with today's technology. Loan repayments are made for 46 weeks.</Text>
            </View>
        )
    }
}
