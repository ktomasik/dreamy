import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text>Up to this module, you have learned to create an email account and set up on your smartphones. Besides, you have learned to set up social media accounts. And
                    on the other hand, you learned how to take pictures of your products and upload them to these platforms. The purpose of this module is to explain how to set up
                    your existing social media accounts for your business.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
