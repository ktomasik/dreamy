import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Setting up a Facebook Page for business</Text>
                <Text>To sell on Facebook, firstly needed to have a page on Facebook (See this topic Module 2).
                    {"\n\n"}Inside your Facebook page, your visitors will see your store tab and will be able to access
                    your products from there.
                </Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 1:</Text> log into
                    your Facebook profile
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
