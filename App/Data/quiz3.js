import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import { Button } from 'react-native-elements';
import {quizStyles} from '../Containers/Styles/general';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz3": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False",
                },
                "question": "E-commerce has begun to enter our lives since the 1950s when computers began to be used."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False",
                },
                "question": "Instagram Business accounts does not provide more information about your brand, as well as not an analytics tool to measure your success."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Etsy does not charge for creating a shop.",
                    "option2": "b) Bonanza allows you to list your items for free.",
                    "option3": "c) In Zibbet, you are allowed to sell electronic vehicles.",
                    "option4": "d) There is no charge for opening a shop in DaWanda."
                },
                "question": "Which of the following statements is false?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False",
                },
                "question": "E-commerce is the buying and selling of goods and services in an online platform."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) The human mind tends to embody something that is abstract.",
                    "option2": "b) Quality product images are a key driver of store engagement.",
                    "option3": "c) Eye-tracking studies show that store visitors are first engaged by visual elements.",
                    "option4": "d) The connection between the logo and the brand is not inevitable."
                },
                "question": "Which of the following statements about the importance of product images is false?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False",
                },
                "question": "Hashtag discovery in Instagram helps the products to appear on the homepage of the users more often. "
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Focus on storytelling to boost engagement",
                    "option2": "b) Open an account with a blue tick",
                    "option3": "c) Open fake accounts to praise the product",
                    "option4": "d) Transform the original image with an edit",
                },
                "question": "Which is one of the ways to make a successful sale in Instagram?"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "In the online sale of handicrafts, the use of real image is not as important as price of product."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Your logo design should represent your product and your business philosophy."
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) More than 60% of online shoppers worldwide considers e-commerce pricing as the very first criteria affecting their buying decision.",
                    "option2": "b) Most advantage of Cost-based E-commerce Pricing Strategy is simplicity.",
                    "option3": "c) Competitor-based E-commerce Pricing can lead to sell your product a lower price than you desire.",
                    "option4": "d) Value-based E-commerce Pricing promises a short-term profit.",
                },
                "question": "Which of the following statements is false?"
            }
        }
    }
};

export default class M3Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz3;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
