import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Step-by-step funding in project partner countries</Text>
                <Text style={styles.dmlH2}>For France:</Text>
                <Text style={styles.dmlH3}>If you are not a job seeker: THE AID OF THE STATE - ACCRE</Text>

                <Text style={styles.dmlH3}>Step 3:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'In the event of a favourable response, Urssaf will issue you a certificate of admission. If not, it shall give reasons and notify its decision of rejection. If there is no response within one month, the ACCRE is considered granted.',
                                val2: 'https://www.legalstart.fr/fiches-pratiques/aides-creation-entreprise/aide-creation-entreprise-femmes/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
