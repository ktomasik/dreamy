import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>What does online shopping mean?</Text>
                <Text>Online shopping is a form of electronic commerce, which allows consumers to directly buy goods or services from a seller over the Internet using a web
                    browser.</Text>

                <Text style={styles.dmlH2}>What does Online Marketing mean?</Text>
                <Text>Online marketing is a set of tools and methodologies used for promoting products and services through the internet. Online marketing includes a wider range of
                    marketing elements than traditional business marketing due to the extra channels and marketing mechanisms available on the internet .</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmark2.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
