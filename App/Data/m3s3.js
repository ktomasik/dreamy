import React, { Component } from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S3 extends Component {
    render() {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What is a brand logo?</Text>
                <Text>A logo is a graphic mark, emblem, or symbol used to aid and promote public recognition. It may be of an abstract or figurative design or include the text of the name it represents as in a logotype or wordmark.</Text>
                <Text>A brand is every interaction with and marketing practice of a name or design that identifies and differentiates one business, product, or service from another.</Text>

                <Text style={styles.dmlH2}>How to choose the right logo design?</Text>
                <Text>When making the decision for a logo design, you have to keep a few things in mind that will help give your business the brand identity it deserves.</Text>
                <Text>If you want to have a creative logo for your business, try to get some logo inspiration first by looking at concepts from successful brands.</Text>

                <Text style={styles.dmlH2}>Pricing Strategy for E-commerce (setting a price for your product)</Text>
                <Text>In online shopping environment, the best thing is to see the price with the product together. It will be more easier for the customer to buy the product whenever he/she sees the product in a good designed way on the platform with its cost.</Text>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                <Image
                    style={{flex:1, width: 300, height: 200}}
                    source={require('../Images/m3/img16.png')}
                    resizeMode="contain"/>
                </View>
                <Text style={styles.dmlH2}>Different types of pricing strategies</Text>
                <Text style={styles.dmlH3}>Cost-based E-commerce Pricing</Text>
                <Text>Cost-based pricing may be the most popular pricing model in the retail industry ensuring a minimum return on each product sold.</Text>
                <Text style={styles.dmlH3}>Competitor-based Ecommerce Pricing</Text>
                <Text>With a competitor-based pricing strategy, you simply monitor what your direct competitors are charging for a particular product, and set your price relative to theirs.</Text>
                <Text style={styles.dmlH3}>Value-based E-commerce Pricing</Text>
                <Text>If you focus on the value you can deliver to a customer, setting prices based on what you perceive a shopper — in the industry segment you serve — will pay for a particular product at a particular time, you have taken the value-based or value-optimized approach to e-commerce pricing (Roggio, 2017).</Text>
            </View>
        )
    }
}
