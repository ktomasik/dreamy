import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S10 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Where to Find the Small Business Loans for Women?</Text>
                <Text style={styles.dmlH2}>For France:</Text>
                <Text style={styles.dmlH3}>Women-Initiated Guarantee Fund (FGIF)</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Object: Facilitate the obtaining of bank loans to cover working capital requirements and / or investments in the creation phase, recovery or development of a company',val2: 'https://www.afecreation.fr/pid14855/appuis-pour-les-femmes.html'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>The networks</Text>
                <FlatList
                    data={[
                        {key: {val1: 'The national networks listed below are at your disposal to welcome you, inform you and accompany you in the editing of your project. Some are dedicated to women creators, others are aimed at all but have specific actions for women, others still follow the companies they have funded.',val2:'http://www.ellesentreprennent.fr/pid14416/les-reseaux-au-service-des-creatrices.html'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>The BPI France (Public Investment Bank)</Text>
                <FlatList
                    data={[
                        {key: {val1: 'The BPI is an organization that is under the supervision of the State. She accompanies you in financing and development aids. Instead, it offers bonding and guarantee solutions to convince your bank to follow you in your projects',val2:'http://www.bpifrance.com/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
