import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m5/img1.png')
    }
}];

export default class M5S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Things You Should Know Before You Get Your First Credit Card (e.g. what is a
                    credit card statement, how credit card interest is calculated, how minimum payments are
                    determined)</Text>
                <Text style={{marginTop: 15}}>A billing statement is a periodic statement that lists all the purchases,
                    payments and other debits and credits made to your credit card account within the billing cycle.
                    Your credit card issuer sends your billing statement about once a month.
                    {"\n\n"}What's on the billing statement?
                    {"\n"}Your billing statement lists everything you need to know about your credit card account. It
                    includes:
                </Text>
                <FlatList
                    data={[
                        {key: 'Your balance from the previous billing cycle'},
                        {key: 'The minimum payment due'},
                        {key: 'The payment due date'},
                        {key: 'Late fee that will be charged if you pay late'},
                        {key: 'A summary and detailed list of payments, credits, purchases, balance transfers, cash advances, fees, interest, and other debits made to your account'},
                        {key: 'A breakdown of the types of balances on your account and the interest rate and interest charges for each'},
                        {key: 'Your credit limit and available credit'},
                        {key: 'The number of days in your billing period'},
                        {key: 'Total amount of interest and fees paid year-to-date'},
                        {key: 'Contact information for your credit card issuer'},
                        {key: 'Rewards earned or redeemed, if applicable'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center', marginTop:10}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m5/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 10}}>Your credit card statement will include a minimum payment disclosure detailing the amount of time it will take to pay off your balance if you only make the minimum payment and the total amount you'll end up paying. It will also include the monthly payment to make if you want to pay your balance off in three years. This information is helpful for figuring out the best way to pay off your credit card balance.
                    {"\n\n"}Your credit card billing statement will also include a late payment warning that tells you the impact of sending your payment late - a late payment and penalty rate increase.
                    {"\n\n"}Your credit card minimum payment  is the least amount you can pay toward your credit card balance without being penalized with a late fee and possible interest rate increase. If you pay attention to your billing statement each month, you've probably noticed that your minimum payment can change from one month to the next.
                    {"\n\n"}Some credit card issuers calculate the minimum payment as a percent of the balance, typically between 2% and 5%, at the end of the billing cycle.
                    {"\n\n"}Your minimum payment may be also calculated by taking a percent of the balance at the end of the billing cycle and adding the monthly finance charge.
                    {"\n\n"}You can find out which method your credit card issuer uses by reading your credit card agreement. Look for a section titled "How your minimum payment is calculated" or "Making payments."
                </Text>
            </View>
        )
    }
}
