import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Follow the steps below to sell at the best price in the on-line environment</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 1: Put your product on the web page that you will be selling</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/28647.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
