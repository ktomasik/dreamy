import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S1 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What is brand?</Text>
                <Text>A brand is a name, logo, sign or shape which singularly, or in combination, allow the consumer to differentiate the product or service from others in the marketplace (Shim 2009).  </Text>
                <Text style={styles.dmlH2}>What is brand name?</Text>
                <Text>A brand name is either a word or numbers in some combination which can be verbally expressed (Shim 2009).
                    {"\n\n"}As an example; 3M, Google, XEROX, etc.
                </Text>
                <Text style={styles.dmlH2}>What is the difference between a brand name & a trade mark?</Text>
                <Text>A business/ trade/ company name is; a name or a way to help identify a business, an entity or an individual.  It is the official name under which the said entity or individual chooses to do business (Cameron 2017).
                    {"\n\n"}A trademark is; a word, phrase, logo, symbol, design, colour or a combination of one or more of these elements that distinguishes one company’s products/services from that of another (Cameron 2017, Shravani 2017).
                    {"\n\n"}Differences between a brand name and a trade mark are as below 	(Anonymous 2018a, Shravani 2017):
                </Text>
                <FlatList
                    data={[
                        {key: 'Brand names and trademarks are valuable assets to a business. Often a brand or trademark becomes synonymous with the product. It is a mistake to use the terms "trademark" and "brand" interchangeably, as they have very important differences.'},
                        {key: 'While brand represents reputation and business in the public eye, a trademark legally protects those aspects of the brand that are unique and specific to the company.'},
                        {key: 'A trademark is legal protection of the brand, granted by the Trademark and Patent Office.'},
                        {key: 'All trademarks are brands, while not all brands are trademarks.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:15}}><Text style={{fontWeight:'bold'}}>As an example:</Text>
                    {"\n"}Coco Chanel is a perfect example of a name that is a trademark. The famous designer Coco Chanel built her successful fashion empire by using her name.  People knew that if they were to purchase a Coco Chanel product they were going to receive quality craftsmanship. Through her reputation of having excellent taste, her name became recognizable around the world. Coco Chanel, her name is considered a trademark, surname Chanel is considered as a brand (Husbey 2016).</Text>
            </View>
        )
    }
}
