import React, {Component} from 'react'
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M6S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create contract formation of buyer-supplier relationships for the Internet
                    shopping</Text>
                <Text style={{marginTop: 15}}>Because Internet shopping is different from the traditional shopping,
                    especially the transactions made via the internet are subject to the Regulation on Distance
                    Contracts. This contract made between a trader and a consumer where they are not together, which is
                    negotiated and agreed by one or more organised means of distance communication - for example, by
                    phone, post or over the internet.</Text>
                <View style={styles2.border}>
                    <Text>For the most part, the general principles in the formation of a contractual relationship
                        regarding online purchases can be applied after the following general actions between a vendor,
                        and the purchaser:</Text>
                    <FlatList
                        data={[
                            {key: 'invitation to treat: the displaying of goods for sale on an e-commerce site is generally seen as an invitation to treat, rather than an offer, which is similar in vain to a traditional brick and mortar store. The offer is instead fulfilled by the actions of the customer visiting the site, rather than the seller;'},
                            {key: 'customer communicates their offer: because the advertising of goods or services on a website is not an offer, the impetus is then on the purchaser to make an offer, and a customer will communicate an offer electronically – which can be done within the website – offering to purchase the advertised product;'},
                            {key: 'the vendor accepts the offer: once an offer has been made by a customer, it is then up to the vendor to accept the offer unequivocally and unconditionally by communicating their acceptance to the customer.'}
                        ]}
                        renderItem={({item}) => <Text
                            style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                    />
                    <Text style={{marginTop:15}}>Upon the acceptance from the vendor of a customer’s offer, a contractual relationship has now been established</Text>
                    <Text><Text style={{fontWeight: 'bold'}}>Source: </Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('http://www.findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f.aspx')}>findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f</Text></Text>
                </View>
                <Text style={{marginTop:15}}>Before the establishment of a contract between the consumer and the seller on the internet, the service or the goods provider about the issues in the regulation must inform the consumer. Some of the information that should be given to the consumer by the seller within the scope of the regulation before the establishment of the contract is as follows:</Text>
                <FlatList
                    data={[
                        {key: 'name, title, address, telephone and other access information of the vendor or provider,'},
                        {key: 'the basic characteristics of the goods or services subject to the contract'},
                        {key: 'the selling price of the goods or services, including all taxes, '},
                        {key: 'if there is any delivery costs,'},
                        {key: 'information on payment and delivery or performance;'},
                        {key: 'conditions for the exercise of the right of withdrawal.'}
                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:15}}>These preliminary information must be confirmed by the consumer before the purchase is made online, otherwise the contractual relationship is not established.</Text>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
