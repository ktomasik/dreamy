import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S6 extends Component {
    render() {
        return (
            <View style={styles.section}>

                <Text style={styles.dmlH2}>Opening a bank account</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to open a bank account
                    {"\n"}Consider your options
                    {"\n"}Once you’ve identified your needs, evaluate your options:
                </Text>
                <FlatList
                    data={[
                        {key: 'Checking account'},
                        {key: 'Savings account'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5, fontWeight: 'bold'}}>{item.key}</Text>}
                />
                <Text style={{marginTop:10}}>As a general rule, banks will require the following:
                    {"\n"}Valid Identification. In some countries you may also need your Social Security number.
                    {"\n"}A minimum amount of money for opening the account. This can vary based on the bank and account
                    you choose. For example, a savings account of an average bank requires a minimum deposit of 300€.
                    {"\n\n"}Choose the bank that's best for you. Contact the bank branch in your local area to discuss
                    what exactly you'd get if you opened a basic account. While all banks are different, they can
                    generally be summed up into two general categories: large chain banks and smaller local ones.
                    {"\n\n"}Large chain banks: Large banks usually have branches in most towns and cities across the
                    country. You can avoid fees you'll have to pay for using other banks' services (like ATM fees, etc.)
                    Large banks also offer services like 24-hour help lines for their customers. In addition, these
                    banks tend to have a stable, trusted reputation.
                    {"\n\n"}Smaller local banks: Small banks offer a more personal, friendlier and human experience.
                    Smaller banks also usually charge smaller fees for using their services. Smaller banks often invest
                    their money into the local community. On the other hand, smaller banks fail more frequently than
                    large banks (this is still very rare, though).
                    {"\n\n"}In addition, credit unions are another option for banking. Credit unions are not-for-profit
                    financial institutions, often with a mission to be "community-oriented" and "serve people, not
                    profit. Credit unions have successfully made their services more accessible by partnering with other
                    credit unions to offer shared branch banking and ATMs.
                    {"\n\n"}Visit your bank and ask to open an account. Opening an account in person is usually the best
                    option for first-time account holders. You can ask the teller all the questions and doubts you have
                    and get immediate answers. Also, the process of opening an account is also usually speedier in
                    person.
                    {"\n\n"}Ask all the important questions before you finalize your account. Ask for clarification on
                    any issues regarding your account.
                    {"\n\n"}Supply the necessary information to create your account. Opening a checking account requires
                    a few basic pieces of personal information. In general, it's a good idea to have:
                    {"\n"}Proof that you are who you say you are: Have a government-issued ID with your photo on it with
                    you (a driver's license or a passport may also be enough).
                    {"\n\n"}Proof of address: A phone bill, driver's license, or any other official document with your
                    name and address will usually do.
                    {"\n\n"}Proof you are a registered citizen: The bank will ask for your Social Security number,
                    taxpayer identification number, or employer identification number to ensure that you are "on record"
                    with the government
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Keep the account documents you receive
                        secure.</Text> When you finish completing your account, you will receive documents that contain
                    important information about your account. Keep these in a safe place. If you can, it's a wise idea
                    to commit the following information to memory so that you don't need to rely on the documents in the
                    future:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Your four-digit PIN number',
                                val2: 'You need this to use your debit card for purchases.'
                            }
                        },
                        {
                            key: {
                                val1: 'Your bank account number',
                                val2: 'You need this for financial tasks like setting up direct deposits'
                            }
                        },
                        {
                            key: {
                                val1: 'Your Social Security number',
                                val2: 'You need this for various tax and financial tasks in the future'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
