import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Point of sale credit card processing</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to point of sale credit card processing:</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1. First, choose the right point of sale system for your business.</Text>
                <Text>There are many considerations in choosing a point of sale (POS) system. First, you’ll want to think about how you would like to accept payments. Will you do most of your business instore or online or a combination of both? Do you want to be able to take payments offsite? What is your budget for purchasing POS hardware, software and peripherals like barcode scanners and receipt printers? What are your plans for expanding your business in the future?
                    {"\n\n"}The answers to all these questions will help lead you to the system that’s right for your business.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2. Choose a compatible payment processor or payment gateway</Text>
                <Text>You also need to decide which payment processor or payment gateway to work with. A payment gateway is a third party hosted payment solution that allows you to connect with your chosen payment processor as long as the gateway works with that processor.</Text>


                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3. Make sure your solution includes the necessary security features to keep transactions safe and protect your customers and your business from a breach.</Text>
                <Text>A data security breach is not only bad for business, it can be devastating. So, it’s in your best interest to do everything you can to prevent a breach from happening in the first place.
                    {"\n\n"}Payment security demands a multi-pronged approach encompassing both technology solutions and best practices. It’s best accomplished in partnership with a payment processing provider
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>4. Prioritize the value-added features you want in your payment solution.</Text>
                <Text>Your business is unique and as such, will have unique needs when it comes to processing payments.
                    {"\n\n"}In order to get the most out of your POS solution, it’s worth thinking about the features you want, and evaluating the availability and costs offered by various providers.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>5. Don’t forget to ask about installation assistance and staff training.</Text>
                <Text>The best payment solution in the world offers little value to your business if it’s too difficult to install or to learn how to use it. Ask for a product demo so you can try out the solution in real-time. Be sure to ask about set-up and integration to your existing systems. Find out about fraud protocols and whether necessary updates are automatically executed or if you have to implement them yourself. And of course, there’s the basics of processing. Find out how you will run various payment types.</Text>
            </View>
        )
    }
}
