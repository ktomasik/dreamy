import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img8.png')
    }
}];

export default class M8S5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Packaging & Marketing</Text>
                <Text style={styles.dmlH3}>Special handling label</Text>
                <Text>Shipping and Handling Labels are preprinted labels that identify proper handling directions and in
                    some cases destination information. These labels may indicate contents of package such as
                    Flammability or Fragile or they may indicate directions for handling (Anonymous 2018j). Shipping and
                    handling labels alert shipping personal to all handling instructions and needs. There are several
                    types of shipping and handling labels, including as indicated below.
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Mailers and small package labels </Text>are used to
                    caution personnel on specific handling instructions for small packages sent through the mail
                    (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Production and small package labels </Text>will draw
                    attention to packages through the production and shipping process (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>International pictorial markings </Text>are used for
                    international shipping. Picture labels reduce the risk of error since personnel in other countries
                    may not understand the language of the package's origin country but it is recommended that
                    additional labels in the language of the shipment's destination be used to ensure proper handling.
                    Labels should be used to ensure compliance with environmental and safety standards and to identify
                    hazardous materials (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Up arrow labels </Text>help protect fragile packages so
                    they arrive safely. Up arrow labels are part of the international pictorial labels (Anonymous
                    2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img2.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Fragile/Glass labels </Text>draw
                    attention to package contents and proper handling instructions such as "this end up" (Anonymous
                    2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img3.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Rush Labels </Text>are used for
                    temperature dependent or medical supplies packages. Perishable drugs which need to be refrigerated
                    and are needed in life saving situations often use rush labels (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img4.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img5.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Temperature awareness labels </Text>instruct
                    shipping and handling personnel on the proper temperature requirements for the package. These labels
                    are used for frozen and/or perishable goods (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img6.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img7.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Hazard labels </Text>identify packages
                    with hazardous goods. They alert shipping and handling personnel to special storage and segregation
                    needs during transportation. There are strict regulations for the shipping of dangerous goods and
                    special documentation, such as chemical data, is often needed. Hazard labels should be used for
                    explosive materials, gases, flammable liquids, flammable solids, spontaneously combustible or water
                    reactive substances, poisonous substances, and corrosive materials (Anonymous 2018j).</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Biohazardous labels </Text>identify
                    packages of bio hazardous goods such as bio waste, human or animal specimens and used laboratory
                    equipment (Anonymous 2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Special handling labels </Text>cover all
                    special shipping and handling instructions. Labels can be custom designed with specific
                    instructions. They are often warning labels or shipping instructions (Anonymous 2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Design Tip </Text>The labels should be
                    applied on three faces of the package, preferably side, and/or ends and top (Anonymous 2018j).
                    {"\n\n"}If commodities require special handling or storage, the shipping package should be so
                    marked, and this information should also appear on the bill of lading (Anonymous 2018j).
                </Text>
            </View>
        )
    }
}
