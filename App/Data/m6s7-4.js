import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S7_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create electronic signature</Text>
                <Text style={{marginTop:10, fontSize:18}}><Text style={{fontWeight:'bold'}}>Step 3:</Text> To customize your DocuSign Account enter the required information and create your account</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_6_3319634.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
