import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Registration to professional and occupational associations</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>For TÜRKİYE</Text>
                <FlatList
                    data={[
                        {key: 'KOSGEB (The Small and Medium Enterprises Development Organization)'},
                        {key: 'The General Directorate of the Status of Women (KSGM)'},
                        {key: 'Turkey Business Association (İŞKUR)'},
                        {key: 'Turkey Union of Chambers and Community Exchanges( TOBB)'},
                        {key: 'Supports of Republic of Turkey Ministry and Social Policies'},
                        {key: 'Turkish Grameen Microfinance Program'},
                        {key: 'Bank loans for women'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For SLOVENIA</Text>
                <Text style={{fontWeight: 'bold'}}>Non-financial State Support</Text>
                <FlatList
                    data={[
                        {key: 'VEM Points'},
                        {key: 'Business incubators'},
                        {key: 'University incubators'},
                        {key: 'Technology parks'},
                        {key: 'Initiative start up Slovenia'},
                        {key: 'The European Enterprises Network'},
                        {key: 'SPIRIT Slovenia'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Financial Supports in Slovenia</Text>
                <FlatList
                    data={[
                        {key: 'Slovenian Enterprise Fund'},
                        {key: 'Employment Service of Slovenia'},
                        {key: 'Slovenian Regional Development Fund'},
                        {key: 'Bank Loans'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Other Supports</Text>
                <FlatList
                    data={[
                        {key: 'Chamber of Commerce &Industry'},
                        {key: 'Chamber of Craft and Small Business of Slovenia'},
                        {key: 'Business Angels of Slovenia'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For POLAND</Text>
                <FlatList
                    data={[
                        {key: 'European Union Funds for women-entrepreneurs (NGOs, Public Bodies,..etc)'},
                        {key: 'European Social Fund (PO WER-Operational Program Knowledge Education Development)'},
                        {key: 'Non-EU sources (governmental, private, etc)'},
                        {key: 'Polish Agency for Enterprise Development'},
                        {key: 'The Loan Fund for Women'},
                        {key: 'Business Angels'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For GREECE</Text>
                <FlatList
                    data={[
                        {key: 'Seed Capital (small funding for a specific population such as young people, unemployment)'},
                        {key: 'OAED Program (for the public)'},
                        {key: 'The Open Fund (for the private sector)'},
                        {key: 'Bank Loans'},
                        {key: 'Partnership Agreement (PA) 2014-2020 (ESPA)'},
                        {key: 'Greek Community Abroad'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For FRANCE</Text>
                <FlatList
                    data={[
                        {key: 'CFE'},
                        {key: 'Register of Commerce and Companies (RFS)'},
                        {key: 'ACCRE-This Device is set up to help job seekers and facilitate the creation of their businesses.'},
                        {key: 'The BPI France (Public Investment Bank)'},
                        {key: 'PRI (Regional Innovation Partnership)'},
                        {key: 'Business Angels'},
                        {key: 'Entreprendre au Feminin'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
