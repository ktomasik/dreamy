import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Instagram Business Account</Text>
                <Text styl={{marginTop: 10}}>In the direction of the above, if you have already opened an business account on Facebook, from a
                    business perspective, Instagram is simpler and less time consuming than other sites.,
                    {"\n\n"}If you like, you can expand your customer network using both Facebook and Instagram.
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
