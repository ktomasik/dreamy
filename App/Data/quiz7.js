import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {

        "quiz7": {
            "question1": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "The lower the interest are the cheaper the loan is to borrow."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) In a bank",
                    "option2": "b) Regional chamber of commerce and industry",
                    "option3": "c) Financial services",
                    "option4": "d) Enterprise investment center"
                },
                "question": "Where would the best deal for small business loan be found?"
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Grants are payable funds often charged by a government department"
            },
            "question4": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "State incentives for start-ups are only of non-financial type"
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Promotion of private investment",
                    "option2": "b) Support for the creation and development of micro-enterprises",
                    "option3": "c) Providing favorable financial resources for companies",
                    "option4": "d) Financial incentives in regional guarantee schemes"
                },
                "question": "What is most typical bank support  of small-business grant for women?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "A tax incentive is a government measure that encourages individuals to save  money by reducing the amount of tax that they have to pay."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) MSicrocredit",
                    "option2": "b) Financial umbrella",
                    "option3": "c) Microloan",
                    "option4": "d) None of the above"
                },
                "question": "What is associated with microfinance?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Microfinance and social capital have a significant relationship with entrepreneurial empowerment."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Business angels are generally interested in becoming involved in the project by acting as a guide or mentor."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Credit",
                    "option2": "b) Balance",
                    "option3": "c) Loan",
                    "option4": "d) Microcredit"
                },
                "question": "____________ is when you borrow money from a bank or other lender."
            }
        }
    }
};

export default class M7Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz7;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
