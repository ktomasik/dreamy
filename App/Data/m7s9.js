import React, { Component } from 'react'
import {Image, Text, View} from 'react-native'


// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S9 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Where to Find the Small Business Loans for Women?</Text>
                <Text style={styles.dmlH2}>For Greece:</Text>
                <Text style={styles.dmlH3}>Banks</Text>
                <Text>When banks are financing new enterprises, they ask to see the business plan. The application will be studied by several groups checking different topics, it is particularly important that the business plan is as complete as possible to avoid any delays.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
