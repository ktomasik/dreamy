import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S32 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Share from Instagram to other social networks</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 2: You can choose to upload the desired picture from three sources:
                    gallery, photo or video. To insert the photo from your photos click gallery, choose desired photo
                    and click next.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img60.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img61.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img62.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
