import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Accepting payments with Bank Accounts</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to accept payments with Bank Accounts
                    {"\n\n"}A bank transfer is effected as follows:
                </Text>
                <FlatList
                    data={[
                        {key: 'The entity wishing to do a transfer approaches a bank and gives the bank the order to transfer a certain amount of money. IBAN and BIC codes are given as well so the bank knows where the money needs to be sent.'},
                        {key: 'The sending bank transmits a message, via a secure system, to the receiving bank, requesting that it effect payment according to the instructions given.'},
                        {key: 'The message also includes settlement instructions. The actual transfer is not instantaneous: funds may take several hours or even days to move from the sender\'s account to the receiver\'s account.'},
                        {key: 'Either the banks involved must hold a reciprocal account with each other, or the payment must be sent to a bank with such an account, a correspondent bank, for further benefit to the ultimate recipient.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:15}}>Before you can receive an international payment, you'll need to provide the sender with some details, including:</Text>
                <FlatList
                    data={[
                        {key: 'Your International Bank Account Number (IBAN)'},
                        {key: 'Your sort code'},
                        {key: 'Account number'},
                        {key: 'Your full name'},
                        {key: 'Your address'},
                        {key: 'The amount and the currency you’d like to receive the payment in.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
