import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S11 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Where to Find the Small Business Loans for Women?</Text>
                <Text style={styles.dmlH2}>For Poland:</Text>
                <Text style={styles.dmlH3}>mBank</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Loans for people running a business no longer than 6 months in the amount of up to PLN 30,000. You can do it online.',val2: 'https://www.mbank.pl/firmy/kredyty/na-start/kredyty-na-start/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Aasa Polska</Text>
                <FlatList
                    data={[
                        {key: {val1: 'A loan for business development up to PLN 20,000. From the first day of operation.',val2:'https://aasadlabiznesu.pl/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Credit Agricole</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Loans for small and medium enterprises.',val2:'https://www.credit-agricole.pl/male-i-srednie-firmy/kredyty/pozyczka-biznes/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>PKO BP</Text>
                <FlatList
                    data={[
                        {key: {val1: 'A start-up loan for business.',val2:'https://www.pkobp.pl/firmy/kredyty/produkty-kredytowe/pozyczka-na-start/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>ING</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Loan for companies.',val2:'https://www.ingbank.pl/male-firmy/kredyty-i-pozyczki/pozyczka-dla-malych-firm'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
