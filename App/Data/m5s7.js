import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Starting Mobile & Online Banking</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to start Mobile & Online Banking:</Text>
                <Text style={{fontWeight: 'bold'}}>What you’ll need </Text>
                <FlatList
                    data={[
                        {key: 'A valid debit or credit card'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop:10}}>What you do</Text>
                <Text>On your mobile device:
                    {"\n"}Download and open the Mobile Banking App on your Android or Apple device. Follow the steps below.
                    {"\n\n"}From a computer:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Step 1',
                                val2: 'Visit the bank’s homepage'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 2',
                                val2: 'Select “Register”'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 3',
                                val2: 'Enter your card number and expiry date and select “Continue”'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 4',
                                val2: 'Say how you want to receive your verification code'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />

                <Text style={{marginTop:15}}>There are some banks that provide, also, the following service:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Step 1',
                                val2: 'When you complete sensitive transactions, a verification code protects you with an extra layer of security. They may send you a 6-digit verification code by text message, email or voice call. You’ll enter that code to complete the transaction.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 2',
                                val2: 'There are some that may send one-time verification codes to personal or free email services.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 3',
                                val2: 'Enter the verification code in the Identity Verification box and select “Continue”'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 4',
                                val2: 'Choose a password'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 5',
                                val2: 'Review the fine print'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 6',
                                val2: 'You’re almost done! Read the Electronic Access Agreement and check the box to confirm you’ve read it.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 7',
                                val2: 'Sign on and start banking'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 8',
                                val2: 'Now that you’ve registered, you can bank from any computer, smartphone or tablet.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 9',
                                val2: 'If your device supports fingerprint sign-on, there are some Bank’s applications that you can use the fingerprint reader to verify your identity and access your accounts in a tap.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
