import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Step-by-step funding in project partner countries</Text>
                <Text style={styles.dmlH2}>For France:</Text>

                <Text style={styles.dmlH3}>If you are a job seeker</Text>
                <Text style={styles.dmlH3}>Step 1:</Text>
                <Text>Registration at Employment centre as unemployed.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
