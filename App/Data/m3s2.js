import React, { Component } from 'react'
import {Text, Image, View, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Why are product images important?</Text>
                <Text>The age we live in is such an age that visuality is of great importance. When people think or do something, they always design a shape in their mind. Because the human mind works to transform something that is abstract into concrete. That is why visual items have a big significance in sales of handmade products.</Text>
                <Image
                    style={{flex:1, width: 300, height:120}}
                    source={require('../Images/m3/img10.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex:1, width: 300, height:200}}
                    source={require('../Images/m3/img11.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex:1, width: 300, height:130}}
                    source={require('../Images/m3/img12.png')}
                    resizeMode="contain"/>

                <Text style={styles.dmlH2}>How to sell your products on Instagram?</Text>
                <Text>Selling your products on your website is no longer the only way to turn a profit. With the ever growing popularity of social commerce, there have never been more ways to sell online and promote your brand. With over 700 million users, Instagram is quickly becoming the social commerce powerhouse. About 80 percent of Instagrammers follow a business on Instagram and 60 percent say they use it to discover new products.</Text>
                <Image
                    style={{flex:1, width: undefined}}
                    source={require('../Images/m3/img13.png')}
                    resizeMode="contain"/>
                <Text style={{marginTop: 10}}>To sell your product on the instagram you need to use a few tips :</Text>
                <FlatList
                    data={[
                        {key: 'Make sure you have a business account.'},
                        {key: 'Post high quality photos and videos.'},
                        {key: 'Focus on storytelling to boost engagement.'},
                        {key: 'Create compelling Instagram ads.'},
                        {key: 'Rely on brand ambassadors and influencers to spread awareness.'},
                        {key: 'Converse with customers.'},
                        {key: 'Find new customers through hashtag discovery.'},
                        {key: 'Update your profile link often. '},
                        {key: 'Offer special discounts for Instagram followers. '},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 5, marginLeft: 12}}>{item.key}</Text>}
                />
                <Image
                    style={{flex:1, width: 300, height:480}}
                    source={require('../Images/m3/img14.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex:1, width: 300, height:400}}
                    source={require('../Images/m3/img15.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
