import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S56 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Use social networking sites</Text>
                <Text style={styles.dmlH2}>Set up a Blog</Text>
                <Text style={{marginTop: 10}}>Before you can begin blogging, you will need to select a platform on which to publish your site. Several free, hosted options exist, such as the very popular WordPress and Blogger. Both of these platforms have apps that will allow users to compose, edit and publish their posts on the go.</Text>
                <Text style={{marginTop: 10}}>The key difference between Blogger and WordPress is that while Blogger is a little more simple to configure and use, WordPress is easier to customize and to eventually transition over to a self-hosted site when you exceed the bounds of their free offerings. Whichever you choose, there are official apps available for the major mobile platforms.(Bozzo 2014)</Text>
                <Text style={{marginTop: 10}}>In this tutorial we will be using Blogger because of simplicity…</Text>
            </View>
        )
    }
}
