import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S9 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Online business (e-trade) laws</Text>
                <Text style={{marginTop: 10}}>For E-trade, the following information must be made available on the website for users:</Text>
                <Text style={{marginLeft: 10, marginTop:5}}>Information regarding:</Text>
                <FlatList
                    data={[
                        {key: 'The commercial title, commercial address, tax or trade registry number, e-mail address, telephone number, and names of administrator(s) of the website (and, where operating an online marketplace, the official communication information of the sellers/providers);'},
                        {key: 'Whether the website is operating under the license or permission of a governmental agency, and the relevant agency.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 25}}>{item.key}</Text>}
                />
                <FlatList
                    data={[
                        {key: 'Terms and conditions of visiting and using the website.'},
                        {key: 'Privacy policy.'},
                        {key: 'Electronic signature is mandatory.'},
                        {key: 'User agreement (if membership is required).'},
                        {key: 'Distance contract to be prepared according to the Regulation for Distance Contracts (if the website will sell any goods or services to consumers) under Consumer Protection Law No. 6502.'},
                        {key: 'To co-ordinate the money flow between the consumers and the online business enterprise, the online business must collaborate with a bank or a payment service provider (Payment Services Law No. 6493) (Dora et al. 2018).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
