import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S10_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Setting up e mail account on your phone for iOS device (iPhone, iPad, or iPod
                    touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 3: Go to "Add Account" and choose "Google" from browser</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img10.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
