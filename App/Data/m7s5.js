import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S5 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Some founds for female founders</Text>
                <Text>500 Women: Funding Flawless Female Founders</Text>
                <Text>Astia: Network that offers access to capital and training/support for women entrepreneurs</Text>
                <Text>BBG (Built By Girls) Ventures: Invests in consumer internet start-ups with at least one female founder</Text>
                <Text>Chloe Capital: Seed stage VC firm focused on women-led companies</Text>
                <Text>Female Founders Fund: Invests in female led start-ups in e-commerce, platforms, and web-enabled services</Text>
                <Text>Golden Seeds: Angel investor network and fund investing in women entrepreneurs</Text>
                <Text>Intel Capital Diversity Fund: Fund that invests in female and minority led start-ups</Text>
                <Text>Mergelane: Accelerator and investor of women led start-ups (Boulder, CO)</Text>
                <Text> Next Wave Impact: An innovative learning-by-doing early stage venture fund</Text>
                <Text>Pipeline Fellowship: Women investors investing in women led social enterprises</Text>
                <Text>Valor Ventures: Fund based in Atlanta that funds female founders</Text>
                <Text>Women’s Venture Fund: Helps entrepreneurs through courses, counselling, credit and more</Text>
            </View>
        )
    }
}
