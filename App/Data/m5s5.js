import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S5 extends Component {
        render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>E-commerce payment system</Text>
                <Text style={{marginTop: 15}}>An e-commerce payment system  is a way of making transactions or paying for goods and services through an electronic medium, without the use of checks or cash.</Text>
                <Text style={styles.dmlH3}>What is to set up Automatic Payments?</Text>
                <Text>An automatic bill payment  is a money transfer scheduled on a predetermined date to pay a recurring bill. Automatic bill payments are routine payments made from a banking, brokerage or mutual fund account to vendors. They are usually set up with the company receiving the payment, though it’s also possible to schedule automatic payments through a checking account’s online bill pay service.</Text>
                <Text style={styles.dmlH3}>Accepting online payment with a credit card, debit card, or PayPal</Text>
                <Text>Enabling your customers to pay with their credit card on your website is the most basic way you can accept online payments. To offer this feature to your customers, you’ll want to decide whether to have your own dedicated merchant account or use an intermediary holding account.
                    {"\n\n"}Small businesses or organizations that want to accept online credit card payments for services, subscriptions, or products sold on a website may add PayPal buttons to any website.
                </Text>
                <Text style={styles.dmlH3}>Secure online payment system requires</Text>
                <Text>Online security  is something that concerns us all as consumers. As a business owner, it’s even more important. By taking online payments, you take responsibility for protecting your customers’ data, and managing it securely can be a costly burden. But you can make it easy on yourself by using a PCI-compliant payment solution. PCI compliance refers to the rules and regulations that govern data protection.</Text>
                <Text style={styles.dmlH3}>Online payment solutions with card, invoice and bank</Text>
                <Text>Online payments  are made instantly, so it’s convenient and saves lots of time.
                    {"\n"}The so called "online wallets" allow their customers to:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Pay online with revealing their credit card details.'},
                        {key: '2. Pay an invoice.'},
                        {key: '3. Pay to a bank account.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH3}>What is Virtual Point of Sale (POS)?</Text>
                <Text>Virtual POS  Terminal. It is a payment gateway which allows online merchants and service sellers to manually authorize card transactions initiated by the buyer. This process expands greatly their payment sources and reduces the time of the payment process, while adding additional security. The integration of virtual POS terminal is possible with numerous e-commerce platforms.</Text>
            </View>
        )
    }
}
