import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_4 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Step-by-step funding in project partner countries</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>

                <Text style={styles.dmlH3}>Step 4:</Text>
                <Text>After successfully completing the training and following an approved entrepreneurial plan and signing an employment plan at the Employment Service, you must self-employ within 30 days.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
