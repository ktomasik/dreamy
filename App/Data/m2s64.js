import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S64 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Use social networking sites</Text>
                <Text style={styles.dmlH2}>Set up a Blog</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 19: If you want to publish the same content also in Google+ blog
                    click Share.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img136.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
