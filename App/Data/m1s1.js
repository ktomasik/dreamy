import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>What is the Internet?</Text>
                <Text>The internet is a network of global exchanges – including private, public, business, academic and
                    government networks – connected by guided, wireless and fiber-optic technologies.</Text>
                <Text style={styles.dmlH2}>A short history of the Internet</Text>
                <Text>The Internet had its roots during the 1960's as a project of the United States government's
                    Department of Defense, to create a non-centralized network. This project was called ARPANET
                    (Advanced Research Projects Agency Network). In order to make the network more global a new
                    sophisticated and standard protocol developed IP (Internet Protocol) technology which defined how
                    electronic messages were packaged, addressed, and sent over the network.</Text>
                <Text style={styles.dmlH2}>What is the Web – (World Wide Web)</Text>
                <Text>The Web, or World Wide Web (W3), is basically a system of Internet servers that support specially
                    formatted documents.</Text>
                <Text style={styles.dmlH2}>The differences between the Internet and the Web</Text>
                <Text>The Internet, linking your computer to other computers around the world, is a way of transporting
                    content. One has to access the internet to view the World Wide Web and any of the web pages or
                    other content it contains. The web is the information-sharing portion of the internet. The Web also
                    utilizes browsers, such as Google or Internet to access Web documents called Web pages that are
                    linked to each other via hyperlinks.</Text>
            </View>
        )
    }
}
