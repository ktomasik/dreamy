import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S2 extends Component {
  render() {
    return (
      <View style={styles.section}>
        <Text style={styles.dmlH2}>Web pages</Text>
        <Text>A web page is a document that is suitable for the World Wide Web and web browsers</Text>
        <Text style={styles.dmlH2}>Website</Text>
        <Text>A website is a collection of related web pages, including multimedia content, typically identified with a common domain name, and published on at least one web server.</Text>
        <Text style={styles.dmlH2}>Web servers</Text>
        <Text>A web server is a system that delivers content or services to end users over the internet.</Text>
        <Text style={styles.dmlH2}>Search engine</Text>
        <Text>A web search engine is a software system that is designed to search for information on the World Wide Web.</Text>
        <Text style={styles.dmlH2}>Searching on internet</Text>
        <Text>Internet search is the process of exploring the Internet for information with the use of a searchengines like Google or Internet Explorer.</Text>
        <Text style={styles.dmlH2}>Meaning of e-mail</Text>
        <Text>E-mail is a system of sending written messages electronically from one computer to another.</Text>
        <Text style={styles.dmlH2}>What is an Email Address?</Text>
        <Text>An email address is the address of an electronic postbox that can receive (and send) email messages on a network.</Text>
        <Text style={styles.dmlH2}>What Does @ mean?</Text>
        <Text>It is the symbol in an e-mail address that separates the name of the user from the user's Internet address.</Text>
        <Text style={styles.dmlH2}>What is an Email Virus?</Text>
        <Text>An email virus is a virus that is sent with or attached to email communications. While many different types of email viruses work in different ways, there also are a variety of methods used to counteract such challenging cyberattacks.</Text>
        <Text style={styles.dmlH2}>What is Email spam?</Text>
        <Text>Spam refers to unsolicited bulk email (junk email). This usually means that a message with an advertising or even irrelevant content is sent to a multitude of recipients, who never requested it.</Text>
      </View>
    )
  }
}
