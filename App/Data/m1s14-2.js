import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Setting up e mail account on your phone for Android email app (Samsung, Sony,
                    HTC ..)</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Congratulations you set up your e-mail account!</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>You can sign up to all social channels with the e-mail account you created.</Text>
                <Text style={{marginTop:10}}>The next module will explain how to set up your social media accounts.</Text>
            </View>
        )
    }
}
