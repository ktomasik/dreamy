import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Step-by-step funding in project partner countries</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>
                <Text style={styles.dmlH3}>Step 2:</Text>
                <Text>You must attend a promotional day at the Employment Service where the advisor appoints you. There you will fill in a notification on which a commission will decide whether or not you will be accepted in the program.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
