import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S6 extends Component {

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Twitter for Business</Text>
                <Text styl={{marginTop: 10}}>One of the most widely used social media platforms; you can give your voice to your buyers directly via Twitter. Twitter followers are your potential customers. You need to create your own followers to see the benefits of Twitter as a marketing tool. You can create your own Twitter list or join a group that has already been created.
                    {"\n\n"}In order to open a twitter business account, you must first have a Twitter account. As described in Module 2, go to google search; write «twitter» and search; access Twitter’s website by choose «mobile.twitter.com»  click ‘Sign up’ to twitter (for details see Module 2).
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/twitter.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
