import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img1.png')
    }
}];

export default class M8S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Packaging & Marketing</Text>
                <Text style={styles.dmlH3}>Calculating rates</Text>
                <Text>When it comes to classifying freight one of the biggest things to take into consideration is
                    density. Certain items you may hear are density based. Typically the denser the item, the lower the
                    class rule applies. When it comes to freight when you have a low class high density item it will
                    cost less to ship then the opposite an item with a higher class low density (Anonymous 2018c).
                    {"\n\n"}Calculating freight density will also provide with a recommended class for the shipment. The
                    freight class chart below is an abbreviated scale that can be used to help estimate the freight
                    classification for the shipments (Anonymous 2018d).
                </Text>

                <Text style={styles.dmlH3}>Packaging techniques</Text>
                <Text style={{fontWeight: 'bold'}}>Basic packaging method (single box packaging method)</Text>
                <FlatList
                    data={[
                        {key: 'Ship no fragile products like soft goods inside a sturdy outer box.'},
                        {key: 'Use fillers like crumpled newspaper, loose fill peanuts, or air-cellular cushioning material to fill void spaces and prevent movement of goods inside the box during shipping.'},
                        {key: 'Place goods that might be affected by dirt, water, or wet conditions inside a plastic bag.'},
                        {key: 'Consolidate small parts or spillable granular products in a strong sealed container, such as a burlap or siftproof plastic bag, then package in a sturdy outer box.'},
                        {key: 'Use the H taping method for sealing your package (Anonymous 2018e).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{fontWeight: 'bold', marginTop: 5}}>Box-in-box method</Text>
                <FlatList
                    data={[
                        {key: 'Wrap product(s) individually with at least 5-cm (2") thickness of air-cellular cushioning or foam material to fit snugly inside a corrugated box.'},
                        {key: 'Restrict product movement inside the box using filler like crumpled newspaper, loosefill peanuts, or other cushioning material.'},
                        {key: 'Close and tape the inner box using the H taping method. This will help prevent accidental opening.'},
                        {key: 'Use a second box that is at least 15 cm (6") longer, wider, and deeper than the inner box. '},
                        {key: 'Choose the wrap or fill method to cushion the inner box inside the larger sturdy outer box.'},
                        {key: 'Ship fragile products individually, wrapping them in a minimum 8-cm (3") thickness of air-cellular cushioning material.'},
                        {key: 'Wrap the inner box with 8-cm (3") thickness of air cellular cushioning material or use at least 8 cm (3") of loose fill peanuts or other cushioning material to fill the spaces between the inner box and outer box on the top, bottom, and all sides.'},
                        {key: 'Fill any void spaces with more cushioning material.'},
                        {key: 'Use the H taping method for sealing your package (Anonymous 2018e).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
