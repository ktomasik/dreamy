import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Follow the steps below to sell at the best price in the on-line environment</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 2: Search for similar products on the web page</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/OAV9EA0.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
