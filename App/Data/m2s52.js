import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S52 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Use social networking sites</Text>
                <Text style={styles.dmlH2}>Create a Facebook Page</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 8: Add a profile photo from your gallery by clicking ADD A PROFILE
                    PHOTO button, choose photo(s) and click DONE and SAVE.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img105.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img106.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img107.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
