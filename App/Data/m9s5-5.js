import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img12.png')
    }
}];

export default class M9S5_5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Create an Instagram business account</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop:10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 4:</Text> “Continue” to connect to Facebook and Set Up a free Business Profile</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img12.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop:10, textAlign: 'center'}}>Congratulations, you have created Instagram business account
                    {"\n\n"}Start posting content!
                </Text>
            </View>
        )
    }
}
