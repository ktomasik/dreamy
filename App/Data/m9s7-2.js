import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img14.png')
    }
}];

export default class M9S7_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Creating a Twitter business account:</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop:10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 2:</Text> Click the “Edit Profile” button and then complete all necessary areas on your Twitter page, as follows:</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img14.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <FlatList
                    data={[
                        {key: '1. Username: (will be better if it reflects your business name)'},
                        {key: '2. Profile photo (recommended dimensions are 400x400 pixels)'},
                        {key: '3. Explanation about your business (You have 160 characters to let people know what makes your account special)'},
                        {key: '4. Header Image you can use event photos, product photos, promotional information/images, or use it to announce new sales and promotions you are currently running for your business (recommended dimensions are 1500x500 pixels) '},
                        {key: '5. Pinned Tweet: If you always put the most important or newest news at the top, it makes it easier for your visitors to find new ones without having to navigate your entire page.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:5}}>Be sure to complete as much information as possible, also, make sure to use a quality photo or logo for the all social media…</Text>
            </View>
        )
    }
}
