import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz9": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Online marketing is a form of electronic commerce."
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Setting up a Facebook page for business, a page connected with personal Facebook Page must be created."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Twitter",
                    "option2": "b) Facebook",
                    "option3": "c) Website",
                    "option4": "d) Instagram"
                },
                "question": "Which of the following is not social media?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "If a Facebook account has already created, a business page on Instagram can be created with this Facebook account."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "In order to open a Twitter business account, Twitter account should be created initially."
            },
            "question6": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Business or Place",
                    "option2": "b) Company, Organization, or Institution",
                    "option3": "c) Brand or Product",
                    "option4": "d) Cause or Community"
                },
                "question": "When setting up a Facebook page for business, which of the following category will be chosen to create page menu?"
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "By the \"call to action\" buttons, the customers can communicate via email, phone or website and they are able to do shopping"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "By social media, products can be seen by customers easily."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Edit profile",
                    "option2": "b) Switch to Business Profile",
                    "option3": "c) Change Password",
                    "option4": "d) Private Account"
                },
                "question": "Which \"option\" should be chosen to set up an Instagram business account?"
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) To create business accounts",
                    "option2": "b) Upgrade the products continuously",
                    "option3": "c) To be available in various social media ",
                    "option4": "d) All of them"
                },
                "question": "Which of the followings might be effective on e-trade?"
            }
        }
    }
};


export default class M9Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz9;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
