import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

//                   \/
export default class M7S30_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Step-by-step funding in project partner countries</Text>
                <Text style={styles.dmlH2}>For France:</Text>
                <Text style={styles.dmlH3}>If you are not a job seeker: THE AID OF THE STATE - ACCRE</Text>

                <Text style={styles.dmlH3}>Step 2:</Text>
                <Text>In return, it issues you a receipt for registration of your ACCRE application, informs the social
                    organizations of your request,
                    and sends the request to the competent Urssaf within 24 hours. The URSSAF rules on the request
                    within one month.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
