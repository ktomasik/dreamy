import React, { Component } from 'react'
import {Text, View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S20 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Tax Incentives for start up women</Text>
                <Text style={styles.dmlH2}>For France:</Text>
                <Text>Women entrepreneurs are the subject of several specific supports. This public of entrepreneurs is the object of a particular attention, to help them better realize their project of creation or recovery. Women entrepreneurs do face sometimes more complicated personal situations, or a greater external distrust. Accompaniments or specific help exist to help them to advance their business creation project.</Text>
                <Text>In terms of support, specialized networks have been in place for a few years. Les Premières network has set up incubators and business incubators, dedicated to women-led business creation projects. These creative projects must have an innovative side. The interest of this form of accompaniment? The incubator provides a follow-up of the business creator, but also hosting the business project for 1 year. The creative business woman is thus surrounded to start the first year of her business.</Text>
                <Text>The Force Femmes network supports women in the second half of their careers, that is to say over 45 years old. Those with a project of creation or takeover of company are followed during the preparation of their project: validation of the project, formation, realization of the business plan, etc.</Text>
                <Text>Actionelles also supports women in their business creation project. The association offers, in addition to support, including a relationship between creative women and experienced business owners, to break the isolation.</Text>
                <Text>These 3 networks are not necessarily present throughout the national territory.</Text>
                <Text>In terms of access to financing, the Guarantee EGALITE Femmes was set up by France Active. It is a bank guarantee, intended to facilitate the obtaining of bank loans by women entrepreneurs. This guarantee can be mobilized for projects of creation, recovery or business development.</Text>
                <Text>This device is not exclusive of other business creation aids. But it completes the support system for women's creation or takeover projects.</Text>

                <FlatList
                    data={[
                        {key: 'https://les-aides.fr/focus/a5Zi/les-aides-pour-les-femmes-creatrices-ou-repreneuses-d-entreprise.html'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
