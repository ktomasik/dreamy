import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S19 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Tax Incentives for start up women</Text>
                <Text style={styles.dmlH2}>For Greece:</Text>
                <Text>In Greece, incentives are given to enhance innovation as, under a new state law, companies that produce products or provide patent registration services internationally recognized in the name of the business are exempt for three years from income tax.</Text>
                <Text>In particular, it is foreseeable that the profits of an enterprise arising from the sale of products for the production of which a patent is internationally recognized in the name of the enterprise are exempt from income tax for three consecutive years starting from the use in which they were first income from the sale of the products using a patent.</Text>
                <Text>Exemption is also granted when products are produced in third-party installations. The exemption is also granted to profits arising from the provision of services when it concerns a patent which is also internationally recognized.</Text>
            </View>
        )
    }
}
