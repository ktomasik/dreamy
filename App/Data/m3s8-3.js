import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S8_3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Follow steps below to take pictures of the products and upload these on a web</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>Step 19: Now, your product is ready to be sold on the market!</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/28647.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
