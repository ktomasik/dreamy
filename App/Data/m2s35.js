import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S35 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Share from Instagram to other social networks</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 5: It is recommended to add a #hashtag to your description to include
                    the post into #hashtags’ content. It is smart to include #hashtag that already has a lot of
                    followers. Since we post an ardunio shield for robotics we include #arduino and arduinoshield. Both
                    #hashtags are followed by several ten-thousand followers.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img66.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img67.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
