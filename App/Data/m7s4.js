import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S4 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What are business angels?</Text>
                <Text>Business angels (also known as angels or angel investors) are individuals who use their personal wealth to provide capital to start-up and early-stage businesses in return for a share of the company’s equity. The influx of capital can help an idea to develop into a viable company and provide the base to begin producing the product or service proposed.</Text>
                <Text>The definition of “business angel” remains unclear, with the terms “business angel”, “informal investor” and “informal venture capital”.</Text>
                <Text style={styles.dmlH2}>What can they offer?</Text>
                <Text>Business angels are able to offer:
                    {"\n"}-	a cash injection for relatively small amounts that would not otherwise be available through venture capital,
                    {"\n"}-	often follow up with later rounds of financing for the same company,
                    {"\n"}-	are generally interested in becoming involved in the project by acting as a guide or mentor,
                    {"\n"}-	invest their time as well as provide connections to their larger network in order to help guide the entrepreneur in the new business venture
                </Text>
                <Text style={styles.dmlH2}>The Women’s business angel fund</Text>
                <Text>Women’s business angels found is usually part of an association that is a professional business non-profit, non-political organization supporting female participation in the innovation decision making. As business angel investments imply not only providing financial resources, but sharing knowledge, experience, and social capital. Association belief is that women’s involvement in a larger number and in new forms would bring economic benefit to all.</Text>
            </View>
        )
    }
}
