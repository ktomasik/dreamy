import React, { Component } from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S18 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Tax Incentives for start up women</Text>
                <Text style={styles.dmlH2}>For Turkey:</Text>
                <Text>According to Article 9/6 of the Income Tax Law, those who manufacture and sell handmade products in the homes where they live are exempted from tax (Law No. 193, Official Gazette No. 28366 dated 27/07/2012).</Text>
                <Text>To be exempt from tax, needed to obtain "Craft Certificate of Exemption" (Esnaf Vergi Muafiyeti Belgesi).{"\n"}</Text>
                <Text>The following procedure is used to obtain a craft certificate of exemption: </Text>
                <Text>Step 1: Persons wishing to obtain a certificate must apply to the tax office at the place where their residence is located with the petition. </Text>
                <Text>Step 2: The following information shall be written as the type of activity carried out on the certificate of tax exemption: Manufacture and sale of handmade products in the houses they live in (ITL Article: 9/6)</Text>
                <Text>Step 3: Home address is shown as work address</Text>
                <Text>Step 4:  If it is accepted that the conditions stated in Article 9 of the Income Tax Law are met, the tax office will issue a certificate.</Text>
                <Text>Step 5: No fee will be charged for the certificate holder</Text>
                <Text>Step 6: Craft tax exemption certificate is valid for three years from the date of issuance and it is possible to obtain new documents by applying to the tax office at the end of this duration</Text>
            </View>
        )
    }
}
