import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S36 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Share from Instagram to other social networks</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 6: To include this post to other social networks check fails under
                    Share To section. Finally click Share st the top right corner.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img68.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
