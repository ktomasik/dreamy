import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz8": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Shipping costs are one of the highest expenses for big-scale works and businesses"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Self delivery",
                    "option2": "b) Same-day delivery",
                    "option3": "c) In-store pick-up",
                    "option4": "d) Ship from store"
                },
                "question": "Which of the following is not among the shipping options?"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "The shipping and fulfillment strategy is an integral part of the sales profitability."
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "For handcraft materials in small quantities, related cargo companies may transfer the goods by road transfer."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Plastic bags",
                    "option2": "b) Carton boxes",
                    "option3": "c) Air channel packages",
                    "option4": "d) All of them"
                },
                "question": "Which of the following is used for cargo packing?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Offering free shipping is not one of the ways to get the customer’s attention."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Insurance",
                    "option2": "b) Invoice",
                    "option3": "c) Customs declaration",
                    "option4": "d) Package Label"
                },
                "question": "______ is an official document that lists and gives details of goods that are being imported or exported."
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Agreements with a cargo company for continuous work may help to lower the shipping rates due to work capacity."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Branches",
                    "option2": "b) Self-service",
                    "option3": "c) Friends",
                    "option4": "d) Call center"
                },
                "question": "Which of the following is not among the ways to reach cargo companies?"
            },
            "question10": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Sender's name, address, telephone",
                    "option2": "b) Colour of item inside the package",
                    "option3": "c) Receiver’s name, address, telephone",
                    "option4": "d) Payment details(who will pay)"
                },
                "question": "Which of the following is not included on the invoice?"
            }
        }
    }
};

export default class M8Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz8;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
