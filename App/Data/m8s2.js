import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Shipping methods</Text>
                <Text style={{marginTop: 10}}>Based on the area, shipping methods are divided into two groups as domestic and international transportation. Domestic shipping includes transportation inside the country and documentation of the goods consist of the national regulations. Cargo companies take a package and transfer it to the customer.
                    {"\n\n"}Based on type of the transfer vehicle, airfreight, shipping and road transport are available. Airfreight and transportation by sea are common overseas commerce in large-scale. Moreover, road transfer is the most common shipping type between the countries which has boarders. For handcraft materials in small quantities, related cargo companies may transfer the goods by road transfer. Way bill and invoice may be necessary. In some cases, packages needs custom declaration, thus, should be need some more documents. Cargo companies can make recommendation and direct to choose the right way.
                </Text>
                <Text style={styles.dmlH3}>Offer free shipping</Text>
                <Text>Free shipping might be defined with a minimum order amount or minimum number of items.  Offering free shipping is one of the ways to get customer’s attention. Overall, it is clear that displaying ‘Free Shipping’ on the sales provides significant advantage over any competitors in the same field. (Anonymous 2018a).</Text>
                <Text style={styles.dmlH3}>Cheaper shipping rates</Text>
                <Text>Agreements with a cargo company for continuous work may help to lower the shipping rates due to work capacity. Also, choosing the best carrier  individually for each order can cause lower rates. While it may seem time and labor intensive to reassess your shipping choice every time you receive an order, different shipping carriers can offer drastically different rates based on the weight, dimensions, and destination of your package.
                    {"\n\n"}Negotiation with different cargo companies may incentive to offer more competitive pricing. A brief rundown about future work capacity and aims for future might be effective for the rates. On the other hand, as shipping rates are determined by the calculations on weight and dimensions, another thing to help lower your cost of shipping is by using packaging provided by the cargo companies.
                </Text>
            </View>
        )
    }
}
