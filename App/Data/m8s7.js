import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Insurance & Tracking</Text>
                <Text style={{marginTop:10, fontWeight:'bold', color: 'green', textAlign: 'center'}}>What Is Cargo Insurance?</Text>
                <Text>Legally, all carriers must carry a minimum amount of insurance, known as carrier liability. However, carrier liability provides very limited coverage, and anything from natural disasters to vehicle accidents or even acts of war could damage your cargo. Therefore, shippers can request cargo insurance to protect their goods from loss, damage, or theft while in transit. Generally, goods are insured while being stored and while in transit, until they reach the buyer (Robinson 2016).
                    {"\n\n"}There are different types of cargo insurance policies, some going by names such as “all risks,” “broad form,” “legal liability,” and “motor truck freight”. However, most of them are necessary for high capacity shipments over the seas.   Cargo insurance can be taken for international as well as domestic transportation (Robinson 2016).
                </Text>
            </View>
        )
    }
}
