import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S25 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Micro finance and Other Start-Up Funds for women</Text>
                <Text style={styles.dmlH2}>For France:</Text>
                <Text>A loan generally of less than € 25,000 intended for persons wishing to create or take over a business but whose resources are insufficient to qualify for a conventional loan. To benefit from a microcredit, the borrower must be accompanied by a specialized and competent support network such as: “France Active”, “France Initiative”, the “Boutiques de Gestion” or “the Fondation of 2e Chance”. These networks will help him to set up his project, to investigate his request for financing and to develop his activity. The main actor is ADIE (Association for the development of the economic initiative).</Text>

                <Text style={styles.dmlH3}>ADIE</Text>
                <Text>Sensitizes, guides and informs women about business creation, with the organization since 2015 of an annual awareness campaign for women.</Text>
                <Text>Promotes project carrier financing, with accompanied microcredit for businesses that do not have access to bank credit.</Text>
                <Text>Reinforces the support of business creators, with training and awareness modules adapted to their specificities.</Text>
                <FlatList
                    data={[
                        {key: 'https://www.adie.org/nos-actions/pour-les-femmes'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />

                <Text style={styles.dmlH3}>FRANCE ACTIVE</Text>
                <FlatList
                    data={[
                        {key: {val1: 'France Active has supported and financed companies for nearly 30 years and mobilized 270 million euros to service 7,400 companies last year. Much more than a network, France Active is a true movement of committed entrepreneurs whose ambition is to build a more inclusive society. France Active\'s mission is to accelerate entrepreneurs\' success by giving them the means to get involved. "',val2: 'https://www.franceactive.org/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />


                <Text style={styles.dmlH3}>FRANCE INITIATIVE</Text>
                <FlatList
                    data={[
                        {key: {val1: 'After being called France Initiative Network, then France Initiative, the network has redesigned its brand system. Since 1 October 2012, the national association is called Initiative France. Local platforms and regional coordination’s make the same change. It\'s more than just a reversal of words. This brand now highlights the term that is common to all: Initiative, while displaying the name of the territory. It is accompanied by a logo that graphically reflects the strength of a national network and its diversity, linked to its local roots. Finally, it bears a signature that gives full meaning to our collective action: "A network, a spirit".',val2: 'http://www.initiative-france.fr/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>BGE</Text>
                <FlatList
                    data={[
                        {key: {val1: 'For more than 35 years, BGE has been supporting business creation and working to make it a reality accessible to all. By accompanying entrepreneurs at every stage of creation, from emergence to business development, we give everyone who takes the chances of success. As a non-profit association network, BGE is made up of 50 associations established in the territories to open up prospects, secure the entrepreneurs\' path and create lasting solutions for employment and local development.',val2: 'http://www.bge.asso.fr/nous-sommes/notre-engagement.html'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>2nd CHANCE FOUNDATION</Text>
                <FlatList
                    data={[
                        {key: {val1: 'The purpose of the 2nd Chance Foundation is to support people aged between 18 and 62, who have gone through difficult life events and are currently in a very precarious situation, but who have a real desire to bounce back. The 2nd Chance Foundation offers them human and financial support to carry out a realistic and sustainable professional project: qualifying training, creation or takeover of a company.',val2: 'http://www.deuxiemechance.org/fr'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
