import React, { Component } from 'react'
import { Text, View } from 'react-native'
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What is a loan?</Text>
                <Text>A loan is when you borrow money from a bank or other lender. Loan payments are divided into two parts, the principal (main) and the interest.</Text>
                <Text>The principal is the amount you are borrowing and is the major (main) part of the balance of that account.</Text>
                <Text>Interest is the charge for the time you have the loan and is calculated on the principal.</Text>
                <Text style={styles.dmlH2}>What is the loan balance?</Text>
                <Text>A loan balance is an amount left to pay on your loan. Every loan has a loan balance up until the loan is entirely paid off. It changes on a daily basis (interest is added daily).</Text>
                <Text>Loan amortization schedule - The principal and interest are separated, so you can see which part of your monthly payment goes to paying off the principal, and which part is used to pay interest.</Text>
                <Text style={styles.dmlH2}>What is a grant?</Text>
                <Text>Grants are non-repayable funds or products disbursed or gifted by one party, often government department, corporation, foundation or trust, to a recipient. A government grant is a financial award given by the federal, state or local government to an eligible grantee. </Text>
                <Text style={styles.dmlH2}>What is tax incentive?</Text>
                <Text>A tax incentive is a government measure that is intended to encourage individuals and businesses to spend money or to save money by reducing the amount of tax that they have to pay.</Text>
            </View>
        )
    }
}
