import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Receiving payments</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to setting up your business to receive payments:</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>STEP 1: Set Up Your Business</Text>
                <Text>Check the options in advance, in order to set up the right organization and structure. That way you won’t end up with the wrong type of business. In many cases, you can change it later, but it helps to research ahead of time to see what might work best for you, whether that’s a sole proprietor, an LLC, S-Corp, C-Corp, or some other type of organization. An accountant can help you set up your business and take care of the necessary paperwork.
                    {"\n\n"}If you have partners, make sure all of their information is accurate as well.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>STEP 2: Get an Identification Number for your business</Text>
                <Text>Getting an Identification Number is an essential first step because you will need this important number if you expect to open a business bank account.
                    {"\n\n"}You may need more than one identification numbers for your company (e.g. your VAT number when registering to the tax authority, your identification number when registering to the local Chamber, etc.).
                    {"\n\n"}Your company’s Identification Number can also be helpful if you want to receive payments through a processor like PayPal. It has also to appear on all receipts and invoices you issue.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>STEP 3: Open a Business Bank Account</Text>
                <Text>As you begin to receive payments, you need a place to put them.
                    {"\n\n"}Having separate accounts is important as a business owner. First of all, it’s a good idea to keep your business assets separate from your personal assets. Second, it makes record-keeping much easier. When tax time comes around, it’s much easier to take care of everything if it’s all in one place.
                    {"\n\n"}If you mix up your money with that of your company, you may end up having serious problems. You have always to have in mind that the time you receive money from your customers does not coincide with the time you have to pay for instance your rent, your taxes, your suppliers etc. Failing to understand the importance of making this distinction may result in shortage of money when actual payments occur, because the money has been spent to personal expenses.
                    {"\n\n"}Your business bank account is where you should have your income deposited and in general where you should undertake all transactions of your company.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>STEP 4: Set Up to Receive Payments through a Third-Party</Text>
                <Text>It’s important to accept a lot of different payment methods. Your customers and clients have their own payment preferences. One of the best ways to make sure you are accommodating them is to use a third-party processor.
                    {"\n\n"}You can establish a merchant account with a card processor or receive payments through a site like PayPal. When you use this type of processor, credit cards are usually taken care of.
                    {"\n\n"}It’s also possible to use processors to accept physical credit cards from your mobile device. This can be helpful if you have physical items to sell, and you do so in person.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>STEP 5: State Business Requirements</Text>
                <Text>Before you set up to receive payments, make sure you know the requirements for businesses in your state. Your country’s Ministry of Economy (or another competent authority) should have information on what you need to do in order to set up your business and begin doing business.</Text>
            </View>
        )
    }
}
