import React, {Component} from 'react'
import {Text,View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M6S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <FlatList
                    data={[
                        {key: 'https://itunes.apple.com/us/app/invoice-2go-invoice-estimate/id540236748?mt=8'},
                        {key: 'https://itunes.apple.com/us/app/free-invoice/id1368542810?mt=8'},
                        {key: 'https://itunes.apple.com/us/app/invoice-simple/id694831622?mt=8'},
                        {key: 'https://www.waveapps.com/invoice-mobile'},
                        {key: 'https://www.and.co/invoice-app/free-invoice-app-for-iphone'},
                        {key: 'https://techwiser.com/free-invoice-app-for-android/'},
                        {key: 'http://bestandroidapps.com/5-best-invoicing-apps-for-android-simple-minimal-and-powerful/'},
                        {key: 'https://www.makeuseof.com/tag/best-free-ways-create-manage-invoices-online/'},
                        {key: 'https://play.google.com/store/apps/details?id=com.zoho.invoicegenerator&hl=en'},
                        {key: 'https://www.waveapps.com/free-accounting-software/'},
                        {key: 'https://www.techopedia.com/definition/2419/internet'},
                        {key: 'https://www.quora.com/What-is-the-difference-between-the-World-Wide-Web-and-the-Internet'},
                        {key: 'https://www.invoicesimple.com/invoice-template/google-docs-invoice-template'},
                        {key: 'http://www.findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f.aspx'},
                        {key: 'http://www.europarl.europa.eu/RegData/etudes/BRIE/2016/577962/EPRS_BRI%282016%29577962_EN.pdf'},
                        {key: 'https://europa.eu/youreurope/citizens/consumers/shopping/contract-information/index_en.htm'},
                        {key: 'https://quote.simplybusiness.co.uk/q/shop/new_business/about_your_shop/5bdc329728597860d64bf611'},
                        {key: 'https://startups.co.uk/a-guide-to-online-business-insurance/'},
                        {key: 'https://www.entrust.com/wp-content/uploads/2013/05/digsig_transactions.pdf'}
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
