import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S3 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What is brand name registration? Do I need to register my brand (business name)?</Text>
                <Text>The development and launch of a new brand requires the investment of a great deal of financial, mental and emotional capital and it is for this reason that <Text style={{fontWeight: 'bold'}}>brand registration</Text>, or legal protection of the new brand, should be a top priority for any new business venture. This applies whether the new brand is a new company, new product or service, or new online business (Anonymous 2018e).
                    {"\n\n"}Brand registration is another name for <Text style={{fontWeight: 'bold'}}>trademark registration</Text> and this is the only way that a brand owner can get exclusive rights to use the new brand in any given national territory. Neither limited company incorporation nor domain name registration will provide any legal protection for a new brand (Forbes Agency Council 2017).
                    {"\n\n"}Without a trademark registration, there is no way to prevent competitors or “copycats” from using the same brand.
                </Text>
                <Text style={styles.dmlH2}>How to register a trademark for a company name?</Text>
                <Text>Every country has their own bureau or office for registering trademarked name or logo. Also, every country have different procedures according to countries’ laws and regulations. To register a trademark, company or a related person has to pay a registration fee.
                    {"\n\n"}The term of trademark registration can vary, but is usually ten years. It can be renewed indefinitely on payment of additional fees. Trademark rights are private rights and protection is enforced through court orders.
                </Text>
            </View>
        )
    }
}
