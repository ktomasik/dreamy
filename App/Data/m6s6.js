import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Contract information</Text>
                <Text style={{marginTop: 15}}>In December 2015, the EU Commission proposed a directive on contracts for online and other distance sales of goods (online sale of goods directive).The proposed Online Sale of Goods Directive would provide for maximum harmonization, thereby prohibiting Member States from introducing a higher level of consumer protection within the scope of the directive . As already stated above, first of all, where a product or service is purchased in the EU, the trader must provide clear, accurate and understandable information about the product or service before purchasing the product.
                    {"\n\n"}In this context, together with the proposed new arrangements, a typical online shopping contract should include the following elements:
                </Text>
                <FlatList
                    data={[
                        {key: 'the trader\'s identity, address, e-mail and telephone number'},
                        {key: 'professional title and VAT details of the trader (if applicable)'},
                        {key: 'trade register number of the trader'},
                        {key: 'the main product characteristics'},
                        {key: 'the total price inclusive of taxes and all charges'},
                        {key: 'delivery costs, (if applicable) - and any other additional charges'},
                        {key: 'arrangements for payment, delivery or performance'},
                        {key: 'the duration of the contract (if applicable) '},
                        {key: 'any delivery restrictions in certain countries'},
                        {key: 'the right to cancelling order within 14 days'},
                        {key: 'available after-sales services'},
                        {key: 'dispute resolution mechanisms'}
                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:15}}>To apply for insurance to sell online products
                    {"\n\n"}You can be held responsible for the products sold because you sell the products you made under the name of your business. For example, you are an online retailer selling extravagant personalized cakes and delivering them to clients’ doors. If someone blames you for food poisoning soon after eating one of your cakes, you could be liable to pay compensation claims.  In addition, the product can be damaged during cargo operations or your product may be stolen. At this stage, insurance could cover the cost of replacement, based on the cost price. In this respect, contacting an insurance agent will be the right approach to help you understand what type of insurance you really need, such as product liability and commercial liability.
                    {"\n\n"}On the other hand, as seen in the following steps, buying insurance now becomes a very easy and time-saving process. Only the most appropriate offers with your information are prepared as soon as possible. You will be given a proposal after the preliminary review. Therefore, you can buy the insurance you find most suitable.
                </Text>
            </View>
        )
    }
}
