import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";

// Styles

export default class M4S4 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Licenses and permits for a new business</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>For TÜRKİYE</Text>
                <Text>If women want to exempt from tax, needed to oblation ‘Craft Certificate of Exemption’ (Esnaf Vergi Muafiyet Belgesi)
                    {"\n\n"}If women want to be taxpayer for the future growing plan, e-Declaration-System is valid. They can apply this system in electronic platform (online). The records of the documents received and given by the taxpayers are kept in the chambers of the profession to which the taxpayers belong (National Report Turkey, 2018).
                    {"\n\n"}‘Craft Certificate of Exemption’ (Esnaf Vergi Muafiyet Belgesi) (National Report Turkey, 2018);
                </Text>
                <FlatList
                    data={[
                        {key: '1. After getting that certificate, registration should be done to a trade association affiliated to the Confederation of Turkish Tradesmen and Craftsmen.'},
                        {key: '2. For work permission, application to the municipality with a petition should be done.'},
                        {key: '3. For home business, all apartment owners must have an agreement with notary channel.'},
                        {key: '4. Invoice  and declaration are  obligatory.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:15, fontWeight: 'bold'}}>Tax-payers (National Report Turkey, 2018):</Text>
                <FlatList
                    data={[
                        {key: '1. Apply e-Decleration System.'},
                        {key: '2. After approval of tax-payer, should be registered to a association affiliated to the Confederation of Turkish Tradesmen and Craftsmen.'},
                        {key: '3. Apply to the municipality with a petition for work permission.'},
                        {key: '4. The taxpayers will obtain the documents from the chambers or associations.'},
                        {key: '5. Invoice and way bill are obligatory.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For SLOVENIA</Text>
                <Text>To establish a firm, filling of register application CEIDG-1 is required. With this form:</Text>
                <FlatList
                    data={[
                        {key: '1. Getting VAT number is necessary,'},
                        {key: '2. Statement on the selection of the form of taxation with income tax on individuals,'},
                        {key: '3. Notification of declaration of contribution to social security (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:10, fontWeight: 'bold'}}>There are 3 areas that require licenses:</Text>
                <FlatList
                    data={[
                        {key: '1. Real estate and brokerage and property management,'},
                        {key: '2. Performing road transport services,'},
                        {key: '3. Running a work agency, a temporary work agency, an unemployment training institution training for public funds (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For POLAND</Text>
                <Text>To establish a firm, filling of register application CEIDG-1 is required. With this form:</Text>
                <FlatList
                    data={[
                        {key: '1. Getting VAT number is necessary'},
                        {key: '2. Statement on the selection of the form of taxation with income tax on individuals,'},
                        {key: '3. Notification of declaration of contribution to social security (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:10, fontWeight: 'bold'}}>There are 3 areas that require licenses:</Text>
                <FlatList
                    data={[
                        {key: '1. Real estate and brokerage and property management,'},
                        {key: '2. Performing road transport services,'},
                        {key: '3. Running a work agency, a temporary work agency, an unemployment training institution training for public funds (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For GREECE</Text>
                <Text>Steps description:</Text>
                <FlatList
                    data={[
                        {key: '1. Get approval of the company\'s name from Chamber of Commerce and Industry'},
                        {key: '2. File company documents with Athens Bar Association'},
                        {key: '3. Sign Articles of Incorporation before a notary public'},
                        {key: '4. Deposit capital in a bank'},
                        {key: '5. Pay capital tax to the Tax Authority'},
                        {key: '6. Get a stamp from the Lawyers\' Pension Fund'},
                        {key: '7. Get certification by the Lawyer\'s Welfare Fund'},
                        {key: '8. Submit Articles of Incorporation and register with Court secretariat to get a register number'},
                        {key: '9. Submit Articles of Incorporation summary for publication İn Official Gazette (FEK)'},
                        {key: '10. Register at the Chamber of Commerce and Industry'},
                        {key: '11. Register with Self-employed Insurance Organisation (OAEE)'},
                        {key: '12. Agricultural Insurance Organisation (OGA), etc..'},
                        {key: '13. Get a tax number (AFM) for the business'},
                        {key: '14. Commission e vendor to make stamp/seal'},
                        {key: '15. Have the Tax Authority punch company receipt books and accounting log'},
                        {key: '16. Notify Manpower (OAED) within 8 days of hiring a worker'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:10}}>All new business owners will be required to complete the previous steps.
                    {"\n"}Self-employed must complete steps 4 and 10-15.
                    {"\n"}It is not necessary to obtain a specific license or permit to open an online shop for selling crafts (National Report Greece, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For FRANCE</Text>
                <Text>Use INPI when the entrepreneur starts a business (National Report France, 2018).</Text>
                <Text><Text style={{fontWeight: 'bold'}}>What is INPI?</Text>
                    {"\n"}(National Institute of Industrial Property)is a public body under the supervision of the Ministry of Economy, Finance and Foreign Trade of Ministry of Productive Recovery and Minister Delegate for Small and Medium Enterprises, Innovation and Economy (National Report France, 2018).
                    {"\n\n"}Once the name is filled with INPI, the company has a term of 10 years.
                    {"\n"}Legal structure depends on:
                </Text>
                <FlatList
                    data={[
                        {key: 'Self-enterprise,'},
                        {key: 'Sole proprietorship'},
                        {key: 'Company.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>In general for establishing a business, steps to follow as below (National Report France, 2018):</Text>
                <FlatList
                    data={[
                        {key: '1. Apply with requested documents to the CFE (Chamber of Commerce),'},
                        {key: '2. APE code is given based on the main activity (code depends on main activity),'},
                        {key: '3. Tax formalities,'},
                        {key: '4. Social formalities, is carried out by CFE,'},
                        {key: '5. Open a file at the Post Office,'},
                        {key: '6. Open a bank account.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
