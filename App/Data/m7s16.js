import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S16 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Small Business grants for women</Text>
                <Text style={styles.dmlH2}>For Poland:</Text>

                <Text style={styles.dmlH3}>EU funds</Text>

                <FlatList
                    data={[
                        {key: {val1: 'The common informational website about EU funds in Poland. On this site, each entrepreneur or future entrepreneur (but also NGOs, public bodies, etc.) can find interesting information about available funding.',val2: 'https://www.funduszeeuropejskie.gov.pl/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Polish Agency for Enterprise Development</Text>
                <FlatList
                    data={[
                        {key: {val1: 'In Poland, Polish Agency for Enterprise Development offers the loan fund only for women, which aims to support the professional activation of women, improve their situation on the labour market by encouraging women to set up their own business. The Loan Fund for Women should contribute to reducing the problem of unemployment among this group. Women can apply for a preference loan in amount between 5 and 10 thousand € (20 and 40 thousand PLN).',val2:'http://en.parp.gov.pl/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
