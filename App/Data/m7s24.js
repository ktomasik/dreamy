import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S24 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Micro finance and Other Start-Up Funds for women</Text>
                <Text style={styles.dmlH2}>For Greece:</Text>
                <Text style={styles.dmlH3}>The People's Trust</Text>
                <FlatList
                    data={[
                        {key: {val1: 'The People’s Trust offers micro-grants to Greek entrepreneurs who wish to either create a new business, or grow an existing business, but are having difficulties in accessing credit. The grant is up to €10,000 per business provided as starting capital for a new business or working capital for an existing one. This funding program focuses on groups with low access to other forms of financing.',val2: 'http://www.thepeoplestrust.org'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Microfinancing (AFI & Eurobank)</Text>
                <Text>AFI (Action Finance Initiative) is a Civil Non Profit Company. It was set up in Greece in 2014 by ActionAid Hellas and the French organization ADIE, leader in microcredit in Europe.</Text>
                <Text>Eurobank cooperates with AFI to help with microcredit (up to € 15,000) long-term unemployed, people belonging to vulnerable categories of citizens and micro-entrepreneurs without access to bank lending. They offer them the opportunity to create their own job (self-employment) or to develop small business units and create new jobs.</Text>

                <FlatList
                    data={[
                        {key: {val1: 'AFI assumes the preselection, training and mentoring of candidates. Eurobank undertakes credit control and funding.',val2: 'https://www.eurobank.gr/el/business/proionta-kai-upiresies/proionta-upiresies/xrimatodotiseis/anaptuksiaka/easy-afi'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
