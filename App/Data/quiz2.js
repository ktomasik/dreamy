import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View, Alert} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz2": {
            "question1": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Fishing the seafood with large nets",
                    "option2": "b) A technical person who repairing the network system",
                    "option3": "c) A group of people who exchanging the information",
                    "option4": "d) A kind of public service to help people"
                },
                "question": "What is networking?"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Creating  social media accounts differ according to various smartphone brands."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) A group of people watching a soccer game.",
                    "option2": "b) A computer-based technology to build virtual networks and share information.",
                    "option3": "c) A common technology (e.g. TV, radio) where the social news can be gathered.",
                    "option4": "d) A private television channel to watch movies."
                },
                "question": "What is social media?"
            },
            "question4": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Instagram",
                    "option2": "b) YouTube",
                    "option3": "c) Facebook",
                    "option4": "d) Google",

                },
                "question": "Which is the following not a kind of social media?"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Sending advertisements regularly - each day.",
                    "option2": "b) Posting as much content as possible to make channel alive.",
                    "option3": "c) Responding to peoples’ comments.",
                    "option4": "d) Blocking people on social media."
                },
                "question": "What is best practice to maintain an audience?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "On some social media sites, members can't create theme-based pages."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) To have clear specified goals of marketing.",
                    "option2": "b) To have enough money to start a business.",
                    "option3": "c) To ensure good quality product.",
                    "option4": "d) To make advertisement"
                },
                "question": "What is the most important in social marketing and business?"
            },
            "question8": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Provide free web space",
                    "option2": "b) Encourage members to upload content",
                    "option3": "c) Allow live chat",
                    "option4": "d) All of them"
                },
                "question": "Which of the followings is a feature of common social media?"
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Same e-mail addresses can be used for signing up in different social media accounts"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) E-mail address",
                    "option2": "b) Search engine",
                    "option3": "c) Social media",
                    "option4": "d) Spam mail"
                },
                "question": "_______ helps to share product specifications and make advertisement and communicate with people directly on internet."
            }
        }
    }
};

export default class M2Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz2;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                            <AwesomeAlert
                                show={showAlertTrue}
                                showProgress={false}
                                title={I18n.t('_correct')}
                                message={I18n.t('_correctFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="green"
                                onConfirmPressed={() => {
                                    this.hideAlert(); this._next();
                                }}
                                titleStyle={{color: 'green', fontWeight: 'bold'}}
                            />
                            <AwesomeAlert
                                show={showAlertFalse}
                                showProgress={false}
                                title={I18n.t('_incorrect')}
                                message={I18n.t('_incorrectFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="#DD6B55"
                                onConfirmPressed={() => {
                                    this.hideAlert(); this._next();
                                }}
                                titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
