import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S3 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>How to Open a business bank account for Your Start up?</Text>
                <Text style={{marginTop: 15}}>A business bank account allows you to easily keep track of expenses, manage employee pay, convey finances to investors, receive and deposit payment, and plan your budget more accurately. Creating a business bank account requires simple steps to get you working quickly:</Text>
                <FlatList
                    data={[
                        {key: 'Determine What Accounts You Need'},
                        {key: 'Choose your Bank'},
                        {key: 'Obtain Your Business Name'},
                        {key: 'Get Your Paperwork in OrderGet Ready to Accept Payment'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>The Difference Between Credit Card and a Debit Card</Text>
                <Text style={{marginTop: 15}}>A credit card is a card that allows you to borrow money against a line of credit, otherwise known as the card’s credit limit. You use the card to make basic transactions, which are then reflected on your bill. Debit cards draw money directly from your checking account when you make the purchase. It can take a few days for this to happen, and the hold may drop off before the transaction goes through.</Text>
            </View>
        )
    }
}
