import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M7S6 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Credit and support for women in project partner countries</Text>
                <Text>The following section explains how women can obtain funding for each country.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
