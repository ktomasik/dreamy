import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img8.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m9/img9.png')
    }
}];

export default class M9S5_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Create a Instagram business account</Text>

                <Text style={{marginTop:10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 1:</Text> Sign up with your email address or phone number, and then enter a username.</Text>
                <Text style={{textAlign: 'center', color: 'red'}}>OR</Text>
                <Text>If you have a Facebook account, you can log in with the same information and link the accounts. (How to open the Facebook account has been previously described in Module 1)</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop:5}}>Tap Sign up, then enter your email address and touch Next, or touch Sign in with Facebook to sign up with your Facebook account.</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
