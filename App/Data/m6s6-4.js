import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Contract information</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 3:</Text> The last step you need to make for the product you
                    specify is to buy. To buy online, you will need to do the insurance inquiry process.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_7_3319599.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
