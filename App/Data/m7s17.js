import React, { Component } from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S17 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Tax Incentives for start up women</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>
                <Text>Partial exemption from paying contributions up to two years after the first entry. - For 2 years - who opens s. p. for the first time and is based on self-employed included social insurance (only those persons who are included in the pension and invalidity insurance on the basis of self- employed).</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_41_3319603.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
