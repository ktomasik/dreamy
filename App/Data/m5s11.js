import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>PayPal Business Account</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to setting Up a PayPal Business Account
                    {"\n\n"}Follow the guides below to complete your account set-up . You’ll need to confirm your email address, verify your PayPal account and choose a PayPal payment solution.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 1</Text>
                <Text>PayPal sent you an email when you signed up for your PayPal business account. Click the link in the email to confirm your email address. If you can’t find the email, log in to your PayPal account and click Confirm email address in your “To-do list” under the Business Profile icon.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 2</Text>
                <Text>By getting verified, you'll not only gain more credibility with your sellers and buyers, but also remove the withdrawal limit on your account. There are 2 ways to get verified:</Text>
                <FlatList
                    data={[
                        {key: '1. Confirm your UnionPay card (instant verification)\n Add or review your UnionPay card details. Proceed to confirm the card by authorizing China UnionPay to send a verification code via SMS. Enter the code to confirm your card instantly.'},
                        {key: '2. Confirm your credit card\n Add or review your Visa or MasterCard to your PayPal account and proceed to confirm the card. This generates a 4-digit code, which will be reflected in your credit card statement within 2-3 business days. Log in to your PayPal account, enter the code to complete the verification process.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 3</Text>
                <Text>Choose a payment solution to suit your business.</Text>
            </View>
        )
    }
}
