import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M8S1 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Effective shipping and fulfillment strategy</Text>
                <Text style={{marginTop: 10}}>Shipping is one of the crucial steps to provide an efficient product to the customer with ideal costs concerning production of your goods, to reach the customer and to receive the payment. To deliver the product safely with an efficient cost to the customer  there are some factors to be considered such as destination, specification of the content, weight, dimensions etc.
                    {"\n\n"}Shipping costs are one of the highest expenses for small- scale works and businesses, but shipping and fulfillment strategies help entrepreneurs to reach the customers on time with reasonable prices.
                    {"\n\n"}Price calculations for small-scale businesses are based on the amount and frequency to be sent for all shipping companies. If the volume of the shipment increases, price is lowers basically. However, negotiation is possible for many cargo companies. For many companies, membership accounts are available which helps to reduce shipping costs.
                    {"\n\n"}Using packages provided by cargo companies may prevent to face additional dimensional fees. Companies can supply various package types for different products. The most common ones are plastic bags, carton boxes, air channel packages for sensitive products, roller packages for paintings and paper in different sizes and dimensions.
                    {"\n\n"}The shipping and fulfillment strategy is an integral part of the sales profitability. If done correctly, shipping strategy and package can help foster repeat sales and even help acquire new customers.
                </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Production</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Reach the customer</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Agreement & Payment</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Shipping</Text>
                    </View>
                </View>
            </View>
        )
    }
}
