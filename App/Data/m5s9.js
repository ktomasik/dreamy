import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>First order form</Text>
                <Text style={{marginTop:15}}>Follow the directions below to set up your first order form
                    {"\n"}How to Create a Simple Order Form with Online Payments
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>In order to receive orders</Text>: you are going to get an idea on how to create a WordPress order form that will accept credit card and PayPal payments.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 1: Create a Simple Order Form in WordPress</Text>
                <FlatList
                    data={[
                        {key: 'Install and activate the WPForms plugin.'},
                        {key: 'Go to WPForms -> Add New to create a new form.'},
                        {key: 'Name your form and select the Billing/Order form template.'},
                        {key: 'Scroll down to the “Available Items” section in the preview screen on the right and click on it.'},
                        {key: 'This will open up the “Field Options” in the left panel. Here you can rename the field, add or remove order items, and change the prices.'},
                        {key: 'If you want to give people images to choose from when filling out your order form, click on the Use image choices checkbox in the Form Editor.'},
                        {key: 'Lastly, you can add additional fields to your order form by dragging them from the left-hand side to the right-hand side.'},
                        {key: 'Click Save when you’re done.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 2: Configure Your Order Form Notifications</Text>
                <FlatList
                    data={[
                        {key: 'Notifications are a great way to send an email when your form is submitted.'},
                        {key: 'You can send an email notification to yourself, to a member of your team by adding their email to the Send to Email Address field or to a customer to let them know their order has been received.'},
                        {key: 'Click on the Settings tab in the Form Builder and then click Notifications.'},
                        {key: 'Click Show Smart Tags in the Send to Email Address field.'},
                        {key: 'Click on Email'},
                        {key: 'Change your notification’s email subject to be more specific. In addition, you can customize the “From Name”, “From Email”, and “Reply-To” emails.'},
                        {key: 'Include a personalized message if the email is going to anyone but yourself.'},
                        {key: 'Use the {all fields} smart tag, if you want to include all the information found in the form fields of the submitted order form.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 3: Configure Your Order Form Confirmations</Text>
                <Text>Form confirmations are messages that display for customers once they submit an order form.
                    {"\n"}There are three confirmation types you can choose from:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Message. When a customer submits an order form, a simple message confirmation will appear letting them know their form was processed.'},
                        {key: '2. Show Page. This confirmation type will take customers to a specific web page on your site thanking them for their order.'},
                        {key: '3. Go to URL (Redirect). This option is used when you want to send customers to a different website.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Let’s see how to set up a simple form confirmation in WPForms so you can customize the message users will see after submitting their orders.</Text>
                <FlatList
                    data={[
                        {key: 'Click on the Confirmation tab in the Form Editor under Settings.'},
                        {key: 'Select the type of confirmation type you’d like to create.'},
                        {key: 'Customize the confirmation message to your liking and click Save when you’re done.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 4: Configure the Payment Settings</Text>
                <Text>WPForms integrates with PayPal for accepting payments.
                    {"\n"}To configure the payments settings on your order form, you’ll first have to install and activate the right payment addon.
                    {"\n\n"}Once you’ve done that:
                </Text>
                <FlatList
                    data={[
                        {key: 'Click the Payments tab in the Form Editor.'},
                        {key: 'Click PayPal,'},
                        {key: 'Enter your PayPal email address,'},
                        {key: 'Select the Production mode,'},
                        {key: 'Choose Products and Services,'},
                        {key: 'Configure the payment settings,'},
                        {key: 'Click Save to store your changes.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:5}}>Now you’re ready to add your simple order form on your site.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Step 5: Add Your Simple Order Form to Your Site</Text>
                <FlatList
                    data={[
                        {key: 'Create a new post or page in WordPress and then click on the Add Form button.'},
                        {key: '\Select your simple order form from the dropdown menu and click Add Form.'},
                        {key: 'Publish your post or page so your order form will appear on your website.'},
                        {key: 'Go to Appearance -> Widgets and add a WPForms widget to your sidebar.'},
                        {key: '\Select the Billing / Order Form from the drop-down menu.'},
                        {key: 'Click Save.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:10}}>Now you can view your published order form live on your site. Notice when you select items on your form the price changes automatically.
                    {"\n"}You now know how to create a simple order form in WordPress that accepts online payments.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', marginTop: 15}}>If you want to send orders as a business, you may use Google Docs’ order form.</Text>
                <Text>Google Docs allows you to create forms that can be used as an order form. After the data is saved to your spreadsheet in Google Docs, you can handle the billing or ordering from there.</Text>
                <FlatList
                    data={[
                        {key: '1. Open Google Docs and click the "Create" button. Select "Form."'},
                        {key: '2. Fill in the name and description of your order form.'},
                        {key: '3. Break your form up into sections, if relevant.'},
                        {key: '4. Add questions by clicking "Add Item".'},
                        {key: '5. Choose a theme for your form by clicking the "Theme" button beside the "Add Item" button. '},
                        {key: '6. Click the link at the bottom of the form window to view your form in the browser.'},
                        {key: '7. Distribute your form.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
