import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S13_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create a Facebook account</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 11: Wait for verification SMS message and enter the code.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img20.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
