import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'

import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S1 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Introduction</Text>
                <Text>This module aims to explain the various financial opportunities and ways for women who want to sell their handicrafts. Module provides information and links for women to financial support and credit facilities for five countries (Slovenia, France, Turkey, Greece and Poland) in the Dreamy m-learning project.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_48_3319639.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
