import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Setting-up Social Media Accounts for Business</Text>
                <Text styl={{marginTop: 10}}>With the increasing number of users every day, social media has become an
                    integral part of today integrals marketing landscape. The
                    following is a step-by-step guide that lets you set up your social media accounts for your
                    business.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
