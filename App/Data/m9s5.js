import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S5 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Create an Instagram business account</Text>
                <Text>Creating an account: Previously explained how to open an Instagram account in Module 2. As
                    mentioned in module 2, you can download the Instagram application from the App Store for iOS, Google
                    Play store for Android, or Windows Phone Store for Windows Phone.
                    {"\n\n"}Once the application is loaded, touch the icon of the application on your mobile phone to
                    open the Instagram.
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
