import React, {Component} from 'react'
import {Image,Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Contract information</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 2:</Text> After entering your information, these criteria will
                    be evaluated and you will find the most suitable offers. It is possible to make a comparison between the offers by choosing the
                    ones you want. You should review this comparison online in terms of limit, guarantee and price.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_32_3319612.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
