import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S8 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Where to Find the Small Business Loans for Women?</Text>
                <Text style={styles.dmlH2}>For Turkey:</Text>
                <Text style={styles.dmlH3}>Banks</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Akbank (girişimci destek paketi)',val2: 'https://www.akbank.com/tr-tr/urunler/Sayfalar/isim-icin-girisimci-destek-paketi.aspx'}},
                        {key: {val1: 'Garanti Bank, kadın girşimci destek paketi',val2: 'https://www.garanti.com.tr/tr/kobi/kobilere_ozel/kadin-girisimci-bankaciligi/kadin-girisimci-kredi-destek-paketi.page'}},
                        {key: {val1: 'TEB Bank  hazine destekli kadın patron kredisi',val2: 'https://www.teb.com.tr/kadin-patronum/kgf-kadin-patron-destek-kredisi/'}},
                        {key: {val1: 'İşbank Kadın Girişimci Kredisi',val2: 'https://www.isbank.com.tr/TR/kampanyalar/kampanya-ayrintilari/Sayfalar/kampanya-ayrintilari.aspx?CampaignName=kadin-girisimci-kredisi-kampanyasi&IdCampaign=Mzk2-ISB'}},
                        {key: {val1: 'Halkbank kadın girişimclere ilk adım kredisi',val2: 'https://www.halkbankkobi.com.tr/NewsDetail/Kadin-Girisimcilere-Ilk-Adim-Kredisi/215'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
