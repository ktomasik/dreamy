import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S51_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Use social networking sites</Text>
                <Text style={styles.dmlH2}>Create a Facebook Page</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 7: You can type an URL of yours homepage to send your visitors to
                    that page (or skip this step) and than click NEXT.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img104.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
