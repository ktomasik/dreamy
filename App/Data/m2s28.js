import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S28 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create an Instagram account</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 15: We will chose a photo from a gallery and choose one appropriate.
                    During the selection we have to allow Instagram application to have access to ours photo
                    gallery.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img52.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img53.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img54.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
