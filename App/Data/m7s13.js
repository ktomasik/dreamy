import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S13 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Small Business grants for women</Text>
                <Text style={styles.dmlH2}>For Turkey:</Text>
                <Text style={styles.dmlH3}>KOSGEB</Text>

                <FlatList
                    data={[
                        {key: {val1: 'The Small and Medium Enterprises Development Organization (KOSGEB) is the main body responsible for SME policy development, co-ordination and implementation. KOSGEB offers government loan support to female entrepreneurs who want to establish their own business. Within this scope, credit support of 50.000 TL (10.000 Euro) is provided within the framework of KOSGEB loan application. This support provided by the state is completely non-refundable. In addition to this grant support, women are provided with interest-free credit support. By 2018 this loan is 70.000 TL (14.000 Euro). The female entrepreneurs\' first business will also be free from taxes for the first three years.',val2: 'https://www.kosgeb.gov.tr/site/tr/genel/detay/6057/kadin-girisimciligi-women-entrepreneurship'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
