import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S41_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create a Twitter account</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 6: Check Connect with people you know to spread your twits more
                    efficiently and click Next.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img80.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
