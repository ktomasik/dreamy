import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S14 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Small Business grants for women</Text>
                <Text style={styles.dmlH2}>For Greece:</Text>
                <Text style={styles.dmlH3}>Greek community abroad</Text>
                <FlatList
                    data={[
                        {key: {val1: 'The Greek community abroad has also been active, and has created new actions, such as the Envolve Award Greece, that concerns an interest-free loan of up to 500 thousand euros, with a repayment within five years.',val2:'https://envolveglobal.org/el/envolve-awards/envolve-greece'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Seed Capital</Text>
                <Text>Seed Capital is small funding for starting an enterprise usually given to specific population categories, such as young people or the unemployed. The main features of these actions are that the capital is quite small (15 to 50 thousand euros), is prepaid and is intended to cover the operating expenses of the first year of operation of the company to allow time for the business development. Indicatively, there are actions of both the public (OΑED Program) and the private sector (TheOpenFund), for various sectors of the economy (traditional products, information technology, etc.).The process is quite simple, and submission can be done by anyone interested. However, these actions are not open throughout the year, while there are some prerequisites such as seminars and unemployment cards for public programs (OAED).{"\n"}</Text>
                <FlatList
                    data={[
                        {key: {val1: '',val2: 'http://www.digitalplan.gov.gr/portal/resource/Prosklhsh-Ypobolhs-Protasewn-sta-Ergaleia-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Sporas-Seed-ICT-Fund-kai-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Ekkinhshs-Early-Stage-ICT-Fund-gia-epiheirhseis-ston-klado-twn-Tehnologiwn-Plhroforikhs-kai-Epikoinwniwn-ICT-ths-Prwtoboylias-JEREMIE'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Partnership Agreement (PA) 2014-2020 (ESPA)</Text>
                <Text>ESPA is the Greek programme that channels funds from the European Union programme for the elimination of inequalities between the EU regions. Within the framework of ESPA, government distributes funds targeted to trade, processing or primary production. Applications are submitted on time-limited periods, announced by the relevant Ministries and the general secretariat of ESPA. The evaluation of the proposals is made by independent evaluators, the results are published, and then a one to three-year period is given for the implementation of each action. Funding can be a percentage of the investment or tax reduction (typically 40-60%). The expenditure documents are required, and they are examined during on-the-spot checks.</Text>

                <FlatList
                    data={[
                        {key: {val1: 'The great advantage of ESPA is its availability, but as funding follows costs, it is not particularly useful for new businesses. However, it is a particularly attractive option to be combined with other forms of financing (bank loans, venture capital, etc.).',val2:'https://www.espa.gr/en/pages/staticPartnershipAgreement.aspx'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}} onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text>The great advantage of ESPA is its availability, but as funding follows costs, it is not particularly useful for new businesses. However, it is a particularly attractive option to be combined with other forms of financing (bank loans, venture capital, etc.).</Text>

                <Text>Among other the ESPA 2014-2020 programme funds the following initiatives:</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Start-ups & new entrepreneurship',val2:'https://www.efepae.gr/frontend/articles.php?cid=496&t=Neofuis-Epixeirimatikotita'}},
                        {key: {val1: 'Upgrading micro & small businesses to develop their skills in new markets',val2:'https://www.efepae.gr/frontend/articles.php?cid=497&t=Anabathmisi-polu-mikrwn-&-mikrwn-epixeirisewn-gia-tin-anaptuksi-twn-ikanotitwn-tous-stis-nees-agores'}},
                        {key: {val1: 'Doing business abroad',val2:'https://www.efepae.gr/frontend/articles.php?cid=539&t=Epixeiroume-Eksw'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
