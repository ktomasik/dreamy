import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S6_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Searching from Google and Creating an e-mail account from smart phone</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 5: choose “Google Account”</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img5.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
