import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img6.png')
    }
}];

export default class M9S3_9 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Setting up a Facebook Page for business</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 8:</Text> Add a
                    Call-to-Action Button to Your Facebook Page. Thanks to the call to action buttons of your customers
                    can communicate with you via email, phone or website and they able to shopping…
                    {"\n\n"}To add call to action button to your page
                </Text>
                <FlatList
                    data={[
                        {key: 'Under the cover photo of your page, click + Add a Button.'},
                        {key: '\Select a button from the pop-up menu and follow the on-screen instructions.'},
                        {key: 'Click Done.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img6.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
