import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img20.png')
    }
}];

export default class M6S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create electronic signature</Text>
                <Text style={{marginTop: 15}}>New business opportunities have emerged as paper-based transaction systems are moved online. Using your electronic signature in business paper-based processes such as your invoices, contracts, e-mails, etc. is important to improve your business. There are many platforms available to download from mobile phones for the electronic signature. One of them is DocuSign.  DocuSign is a free and convenient platform for electronic signature to be used on mobile phones. It is Available on: iPhone, iPad, Android, and Windows.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img20.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
