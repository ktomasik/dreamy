import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text>As you saw in module 1 you have to make a differences between Internet and the Web:  Internet is a network of global exchanges – including private, public, business, academic and government networks – connected by guided, wireless and fiber-optic technologies
                    {"\n\n"}The Web, or World Wide Web (W3), is basically a system of Internet servers that support specially formatted documents.
                    {"\n\n"}Internet, linking a computer to other computers around the world, is a way of transporting content. You have to access the internet to view the World Wide Web and any of the web pages or other content it contains  the web is the information-sharing portion of the internet. The Web also utilizes browsers, such as Google or Internet to access Web documents called Web pages that are linked to each other via hyperlinks.
                    {"\n\n"}The taxpayers who have the status of Limited Company and/ or a sole trader who sells goods and services over the internet can benefit from electronic invoice application. There is no turnover restriction for the electronic bill transition. Even those with very low turnover can benefit from electronic invoicing.
                    {"\n\n"}A quick Google search will reveal many options. Programs such as Microsoft Office have a customizable template in their software, and you can download the templates and find websites that are available in Office.
                    {"\n\n"}Cash board: Cash board offers a downloadable template that you can open in Microsoft Word. Templates are simple but customizable.
                    {"\n\n"}Invoice berry - Office, Open Office and Excel Templates
                    {"\n\n"}Microsoft - If you don't like the template from Microsoft Office, go to the Microsoft template gallery for more options.
                    {"\n\n"}In addition to Microsoft Office, Google Docs has templates that you can select in the template gallery, and Apple offers options for productivity applications.
                    {"\n\n"}Some websites allow you to create, print, save, and send invoices directly from their site. You don't need any other programs except for a web browser.
                    {"\n\n"}The following section shows some free applications for learning to create electronic invoices.
                </Text>
            </View>
        )
    }
}
