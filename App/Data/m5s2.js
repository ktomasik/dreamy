import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What is A Bank Account?</Text>
                <Text style={{marginTop: 10}}>A bank account  is a safe and useful place of your choice to deposit all your money. You can access your money from any ATM. It also makes it easier to save and invest your money for your future.
                    {"\n\n"}Types of Bank Accounts
                    {"\n"}Most banks and credit unions offer the following types of accounts:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Savings accounts'},
                        {key: '2. Checking accounts'},
                        {key: '3. Money market accounts'},
                        {key: '4. Certificates of deposit (CDs)'},
                        {key: '5. Retirement accounts'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>What is a Checking Account? What is Mobile & Online Banking?</Text>
                <Text style={{marginTop: 10}}>A checking account  offers easy access to your money for your daily transactional needs and helps keep your cash secure. Customers can use a debit card or checks to make purchases or pay bills. Accounts may have different options or packages to help avoid certain monthly service fees. To determine the most economical choice, compare the benefits of different checking packages with the services you actually need.
                    {"\n\n"}Mobile banking allows you to perform many of the same activities as online banking using a smartphone or tablet instead of a desktop computer. Mobile banking’s versatility includes:
                </Text>
                <FlatList
                    data={[
                        {key: 'Logging into a bank’s mobile website'},
                        {key: 'Using a mobile banking app'},
                        {key: 'Text message (SMS) banking'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:15}}>Online banking refers to any banking transaction that can be conducted over the internet, generally through a bank’s website under a private profile, and with a desktop or laptop computer. Online banking is generally defined as having the following characteristics:</Text>
                <FlatList
                    data={[
                        {key: 'Financial transactions through bank’s secure website.'},
                        {key: 'Physical branch locations or only online.'},
                        {key: 'The user must create a login ID and password.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
