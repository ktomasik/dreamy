import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S6 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Transferring documents – customs declaration and forms</Text>
                <Text style={{marginTop:10, fontWeight:'bold', color: 'green', textAlign: 'center'}}>What is a custom declaration?</Text>
                <Text>A customs declaration is an official document that lists and gives details of goods that are being imported or exported.
                    {"\n\n"}In legal terms, a customs declaration is the act whereby a person indicates the wish to place goods under a given customs procedure (European Commission 2018).
                </Text>

                <Text style={{marginTop:10, fontWeight:'bold', color: 'green', textAlign: 'center'}}>Who should lodge a customs declaration?</Text>
                <Text>In general it is the owner of the goods or a person acting on his behalf (a representative).
                    {"\n"}The person having control over the goods may also perform it. These persons may be individuals or companies, as well as in certain cases associations of persons (European Commission 2018).
                </Text>

                <Text style={{marginTop:10, fontWeight:'bold', color: 'green', textAlign: 'center'}}>Where should a customs declaration be lodged?</Text>
                <Text>The declaration should be lodged with the customs office where the goods were or will shortly be presented (European Commission 2018).</Text>

                <Text style={{marginTop:10, fontWeight:'bold', color: 'green', textAlign: 'center'}}>Where should a customs declaration be lodged?</Text>
                <Text>In order to comply with the legal obligations and to place goods under a customs procedure, a customs declaration needs to be lodged (European Commission 2018). This should happen in two cases:</Text>
                <FlatList
                    data={[
                        {key: 'upon importation, when goods are brought into the customs territory, they must be assigned to a customs-approved treatment or use'},
                        {key: 'and goods intended for export - must be placed under the export procedure'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
