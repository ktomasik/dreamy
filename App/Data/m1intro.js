import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1INTRO extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text>Dreamy m- Learning Project’s target group is women with lower educational attainment who produce
                    handicraft from their home. The aim is to help them to acquire mobile- digital and entrepreneurial
                    skills to sell their handicrafts in digital markets by using smart phones efficiently. A mobile
                    application is developed, which is easy to understand, and easy to use; for Android and iOS
                    operating systems, web based interface bridging mobile applications to m-learning portal. According
                    to this aim, modules are produced with a step-by-step approach to the needs of the target group. All
                    of the applications provided in the modules are prepared as free of charge for smartphone mobile
                    applications.</Text>
                <Text style={{marginTop: 10}}>In line with this framework, this module aims to explain how women able to
                    create an e-mail account and to install this account to their smartphones (IOS and Android) at the
                    first stage. In addition, this module is a useful tool for persons who have the same background and
                    who want to learn the relevant topics.</Text>
            </View>
        )
    }
}
