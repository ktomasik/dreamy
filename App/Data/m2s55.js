import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S55 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Use social networking sites</Text>
                <Text style={styles.dmlH2}>Create a Facebook Page</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 12: After you are done with the content click PUBLISH.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img113.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
