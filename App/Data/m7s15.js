import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S15 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Small Business grants for women</Text>
                <Text style={styles.dmlH2}>For France:</Text>
                <Text style={styles.dmlH3}>PRI (Regional Innovation Partnerships)</Text>

                <FlatList
                    data={[
                        {key: {val1: '',val2: 'https://www.bpifrance.fr/Toutes-nos-solutions/Aides-concours-et-labels/Aides-a-l-innovation-projets-individuels/PRI-Faisabilite'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text>This device is open to innovative SMEs being created but it is not their priority target.</Text>
                <Text>It is conducted in partnership only with 5 regions: Grand Est (Alsace, Champagne-Ardenne, Lorraine), Hauts de France (Nord Pas de Calais Picardy), Aquitaine / Poitou Charentes, Pays de la Loire, PACA.</Text>
                <Text>The most innovative projects will be selected and will be awarded a grant of 100,000 to 200,000 euros maximum per project. This assistance will allow you to cover the expenses related to the preliminary studies and the implementation of the project. It is paid in 2 installments (70% and 30%).</Text>
                <Text>Finally the project of the SME must take place over 12 months maximum.</Text>
            </View>
        )
    }
}
