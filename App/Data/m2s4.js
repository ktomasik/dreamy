import React, { Component } from 'react'
import { Text, View, SectionList } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S4 extends Component {
  render () {
    return (
        <View style={styles.section} >
          <Text style={styles.dmlH2noBorder}>Social media marketing for start up women</Text>
          <Text style={styles.dmlH3}>Goals</Text>
          <Text>A really good article about social media strategy was written by Alex York (2018). He pointed out that the most important thing to drive your business is to follow your goals constantly. It even helps if you can write down thous goals to ensure that these are more defined and realistic.
            Goal setting is a staple of all marketing and business strategies. Social media is no exception. Of course, with a range of social capabilities, it can be difficult to determine exactly what your objectives should be. For guid- ance, here are some common social media goals to consider:
          </Text>
          <SectionList
              sections={[
                {title: 'Increase brand awareness:', data: ['To create authentic and lasting brand awareness, avoid a slew of promo- tional messages. Instead, focus on meaningful content and a strong brand personality through your social channels.']},
                {title: 'Higher quality of sales:', data: ['Digging through your social channels is nearly impossible without monitoring or listening to specific keywords, phrases or hashtags. Through more efficient social media targeting, you reach your core audience much faster.']},
                {title: 'Drive in-person sales:', data: ['Some retailers rely on social media marketing efforts to drive in-store sales. Is your brand promoting enough on social to reward those who come to you? What about alerting custom- ers to what’s going on in your stores?']},
                {title: 'Improve ROI:', data: ['There’s not a brand on social media that doesn’t want to increase its return on investment. But on social, this goal is specific to performing a thorough audit of your channels and ensuring cost of labor, advertisements and design stay on track.']},
                {title: 'Create a loyal fanbase:', data: ['Does your brand promote user-generated content? Do your followers react positively without any initiation? Getting to this point takes time and effort with creating a positive brand persona on social.']},
                {title: 'Better pulse on the industry:', data: ['What are your competitors doing that seems to be working? What strategies are they using to drive engagement or sales? Having a pulse on the industry could simply help you improve your efforts and take some tips from those doing well.']}

              ]}
              renderItem={({item}) => <Text style={styles.itemList}>{item}</Text>}
              renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
              keyExtractor={(item, index) => index}
          />
          <Text style={styles.dmlH3}>Audience</Text>
          <Text>We have a lot of opportunities and choices where to post our products. To choose one or several of them we must ask ourselves: ”What is our target group?”. When we have the answer we can follow the statistical demographics data of people using different social media:</Text>
          <SectionList
              sections={[
                {title: 'Facebook’s most popular demographics include:', data:['Women users (89%)','18-29 year olds (88%)', 'Urban- and rural-located users (81% each)','Those earning less than $30,000 (84%)','Users with some college experience (82%)']},
                {title: 'Instagram’s most popular demographics include:', data:['Women users (38%)', '18-29 year olds (59%)', 'Urban-located users (39%)', 'Those earning less than $30,000 (38%)', 'Users with some college experience (37%)']},
                {title: 'Twitter’s most popular demographics include:', data:['Women users (25%)','18-29 year olds (36%)', 'Urban-located users (26%)', 'Those earning $50,000-$74,999 (28%)', 'Users with college experience or more (29%)']}
              ]}
              renderItem={({item}) => <Text style={styles.itemList}>{'\u2022'}{item}</Text>}
              renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
              keyExtractor={(item, index) => index}
          />
          <Text style={styles.dmlH3}>Similar products</Text>
          <Text>Before you start creating content it’s really smart to investigate your competitors. Do this before the content creation process because you often find new ways to look at content by analyzing what’s making your compet- itors successful. The simplest way to find competitors is through a simple Google search. Look up your most valuable keywords, phrases and industry terms to see who shows up.</Text>
          <Text style={styles.dmlH3}>Content</Text>
          <Text>For starters, is recommended to create content that fits to your brand’s identity. This means you should avoid things like reaching out to your unpopular demographics without a complete strategy in place. The content must be relevant and the published page must not be filled up with advertisements. Online shoppers rather believe the video content than only pictures. If it is possible use prepared themes. Keep content format con- sistent and simple for your readers to not confuse them.</Text>
          <Text style={styles.dmlH3}>Do NOT ignore</Text>
          <Text>Social media channels are built as networks. This means their main purpose is to be a space to converse, discuss topics and share content. Your brand can’t forget these core elements of “networking” and it takes effort to ensure conversations or engagement opportunities aren’t left unattended.
            Through social media, you gain respect as a brand by just being present and talking to your audience. That’s why social customer care is so important to brands wanting to increase audience awareness. It’s all about engagement.
          </Text>
        </View>
    )
  }
}
