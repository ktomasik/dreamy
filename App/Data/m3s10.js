import React, {Component} from 'react'
import {Text, View, Image} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S10 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Follow the steps below to create a logo</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 1: Go to internet browser on your mobile phone</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522778-PIXKA7-878.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
