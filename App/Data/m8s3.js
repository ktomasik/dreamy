import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S3 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Customer shipping options</Text>
                <Text style={{marginTop: 10}}>Shipping and delivery is a major sales driver for retail business due to the expectation of the customers  to reach the items quickly and easily. Some of the customer shipping options are indicated as below (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Same-Day Delivery</Text>
                <Text>Same-Day- Delivery is a service that ordering  online and receiving  the delivery in the same day which is a kind of express service. (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>In-Store Pickup</Text>
                <Text>In-store pickup  allows customers to shop for items online, checkout, and pick them up within a set timeframe at a local store or place (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Ship from Store</Text>
                <Text>Ship-from-store is a service that retailers turn their offline locations into fulfillment centers that cater to both in-store and online shoppers (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Scheduled Delivery</Text>
                <Text>Scheduled delivery is a service that shipping companies offer customers the option to schedule their deliveries within a set window for a small fee or for free if they are members of their loyalty program. (Ufford 2018).</Text>

                <Text style={styles.dmlH3}>Offer free shipping</Text>
                <Text>One of the stressful points  at the beginning of sales is to charge to the customers. Except the expenses of the product during production period, there may be some other costs after the production to be considered to get attention by the customers. Shipping costs are evaluated by in this option.
                    {"\n\n"}To get attention to lower the price of the product carrier costs might be stated separately. During the sales, price without shipping cost might be given where the transfer costs will to be covered by the customers. In this point, preference for the cargo company and terms might be defined by the customer and, according to customer need, shipping will be processed. On the other hand, price of the product and shipping cost defined by cargo company might be stated separately.
                    {"\n\n"}Charging shipping costs to sale price may provide competitive prices for product which gets attention of the customers.
                </Text>

                <Text style={styles.dmlH3}>How to charge customers</Text>
                <Text>Offering free shipping (typically only on domestic orders) is a proven method to get customers’ attention and increase conversion. But depending on margins, it can significantly decrease the profits (Anonymous 2018b).
                    {"\n\n"}If free shipping is offered in any capacity in any offer amount and quantity, it should be considered to know how much it actually costs to ship the products, how the competitors handle shipping and the profit margin. That will help to make the right call on whether or not for offering free shipping. Another option is to define minimum order totals above the average order value, or for a minimum number of products per order (Anonymous 2018b).
                    {"\n\n"}Some of the advantages of free shipping are to remain competitive or undercut the competitors' shipping options, improve conversation, increase the average order value and reduce abandoned carts. On the other hand, increased prices to cover the shipping cost should be considered to compare with competitors in the market (Anonymous 2018b).
                </Text>
            </View>
        )
    }
}
