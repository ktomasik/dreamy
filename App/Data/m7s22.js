import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S22 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Micro finance and Other Start-Up Funds for women</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>
                <Text style={styles.dmlH3}>Vstopne točke SPOT (VEM)</Text>
                <Text>Information, basic advice, company registration</Text>

                <Text style={styles.dmlH3}>Podjetniški inkubatorji</Text>
                <Text>Equipped offices, business and other support services</Text>

                <Text style={styles.dmlH3}>Univerzitetni inkubatorji</Text>
                <Text>Equipped offices, business consulting and mentoring, free educational workshops.</Text>
                <Text>Podjetniški inkubator Univerze v Mariboru</Text>
                <Text>Ljubljanski univerzitetni inkubator</Text>
                <Text>Univerzitetni razvojni center in inkubator Primorske</Text>

                <Text style={styles.dmlH3}>Tehnološki parki</Text>
                <Text>Equipped offices, mentoring, consulting, coworking Information,</Text>

                <Text style={styles.dmlH3}>Iniciativa Start:up Slovenija</Text>
                <Text>Networking, organization of the competition</Text>

                <Text style={styles.dmlH3}>Mreža European Enterprise Network (EEN)</Text>
                <Text>Search for business partners, information, counselling</Text>

                <Text style={styles.dmlH3}>Coworking prostori Coworking MB Hekovnik</Text>
                <Text>Business start-up, networking, search for business partners, information, counseling, training</Text>

                <Text style={styles.dmlH3}>Gospodarska zbornica Slovenije</Text>
                <Text>Counseling, training, assistance with internationalization</Text>

                <Text style={styles.dmlH3}>Obrtno podjetniška zbornica Slovenije</Text>
                <Text>Counseling, training, issuing craft licenses, EU certificates, certificates for occasional craft activities in the Republic of Slovenia, licenses for carrying out transport</Text>

                <Text style={styles.dmlH3}>Program Erasmus za mlade podjetnike</Text>
                <Text>Co-financing of entrepreneurship training - exchange of experience among entrepreneurs within the EU</Text>
            </View>
        )
    }
}
