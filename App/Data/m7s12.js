import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S12 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Small Business grants for women</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>
                <Text style={styles.dmlH3}>Slovenski podjetniški sklad (Slovene Enterprise Fund)</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Start-up funds for newly born innovative companies (P2A and P2B): start-up capital for new innovative companies, more favorable sources of funding for development investment firms (subsidies, guarantees), promotion of private investment (equity, loans, guarantees).',val2: 'https://www.podjetniskisklad.si/en'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Agencija RS za kmetijske trge in razvoj podeželja</Text>
                <Text>Support for the creation and development of micro-enterprises.</Text>
                <Text style={styles.dmlH3}>Zavod RS za zaposlovanje</Text>
                <Text>Self-employment subsidies / occasional provision of grants for self-employment.</Text>
                <Text style={styles.dmlH3}>Slovenski regionalno razvojni sklad</Text>
                <Text>Financial incentives, especially in the form of returnable funds, for initial investments in the field of entrepreneurship, agriculture, regional development, financial investments in regional guarantee schemes, pre-financing projects with approved European funds.</Text>
                <Text style={styles.dmlH3}>SID banka</Text>
                <Text>Providing favorable financial resources for companies, export insurance business.</Text>
                <Text style={styles.dmlH3}>Eko sklad </Text>
                <Text>Providing favorable financial resources for investing in environmentally-oriented projects and energy efficiency.</Text>

            </View>
        )
    }
}
