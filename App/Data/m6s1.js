import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S1 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text style={{marginTop: 10}}>Dreamy m- Learning Project’s target group is women with lower educational attainment who produce handicraft from their home. The aim is to help them to acquire mobile- digital and entrepreneurial skills to sell their handicrafts in digital markets by using smart phones efficiently. According to this aim, we will develop a mobile application, which is easy to understand, and easy to use; for Android and iOS operating systems, web based interface bridging mobile applications to m-learning portal. In the training program we prepared, we will explain to women how to proceed step by step with free and common mobile applications.
                    {"\n\n"}In line with this framework, this module aims to explain issues such as creating invoices, establishing contracts for buyer-supplier relationships for Internet shopping, applying for insurance to sell online products, and creating electronic signatures for women who want to sell their handicrafts on the internet.
                </Text>
            </View>
        )
    }
}
