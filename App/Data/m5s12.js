import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Accepting payments by Credit Card</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to accept payments by Credit Card
                    {"\n"}For accepting payments by Credit Card, you will need:
                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Merchant Account:',
                                val2: 'A type of bank account where credit and debit card payments get deposited'
                            }
                        },
                        {
                            key: {
                                val1: 'Virtual Terminal:',
                                val2: 'Like a digital credit card swipe machine, this system allows you to input credit card information on your computer.'
                            }
                        },
                        {
                            key: {
                                val1: 'Gateway:',
                                val2: 'The connector between your online store and the bank, which sends payment information securely to be approved or declined.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />

                <Text style={{marginTop: 10}}>How It Works Step-by-Step</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Step 1:',
                                val2: 'When a client contacts you, he/she logs in to your virtual terminal and select the products or services he/she wishes to buy. He/she then has to enter his/her credit card information.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 2:',
                                val2: 'The payment information passes through the secure payment gateway and is transmitted to the authorization source.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 3:',
                                val2: 'The bank that issued the credit card receives the transaction information, checks if the funds are available, and approves or declines the release of payment.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 4:',
                                val2: 'The payment gateway “tells” you if the payment was accepted or declined.'
                            }
                        },
                        {
                            key: {
                                val1: 'Step 5:',
                                val2: 'If approved, funds are deposited into your bank account in 2-3 days.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />
            </View>
        )
    }
}
