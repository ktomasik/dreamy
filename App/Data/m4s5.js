import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S5 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What is tax identification number?</Text>
                <Text>A Taxpayer Identification Number (TIN) is an identifying number used for tax purposes in the country (Anonymous 2018f).
                    {"\n"}All legal entities, unincorporated entities and individuals must obtain a tax identification number.
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For TÜRKİYE</Text>
                <Text>There is e-Declaration system is valid. Actually, now people’s ID number is used as Tax ID Number (TIN). ID must be notarized or approved by tax office officers. So, they can get the Tax ID Number (National Report Turkey, 2018).</Text>
                <FlatList
                    data={[
                        {key: 'TIN in order to undertake professional or business activities in Türkiye.'},
                        {key: 'As of 1 July 2006, the National Identity Number is used as the unique identification number for Turkish citizens and all TINs for citizens were matched with their National Identification Number in tax database system.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For SLOVENIA</Text>
                <Text>Upon entry of the required data into the tax register.
                    {"\n"}Additional alpha or numeric characters to the tax number can be required.
                    {"\n"}If an individual registers in the tax register as a personal entrepreneur, the financial administration doesn’t assign a new TIN (National Report Slovenia, 2018).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For POLAND</Text>
                <Text>Each adult has to have tax identification number to account for the tax office.
                    {"\n"}If an adult becomes an individual entrepreneur, he/she has the same tax identification number to use (National Report Poland, 2018).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For GREECE</Text>
                <Text>Each adult must have a tax ID Number.
                    {"\n"}In the case when an adult wants to be an individual entrepreneur, he/she must register the business to the tax authority.
                    {"\n"}With this authorization, he/she can use personal Tax ID Number in business purposes (National Report Greece, 2018).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For FRANCE</Text>
                <Text>The French tax authorities issue a tax identification number to all natural people who have the obligation to declare taxes in France.
                    {"\n"}The TIN is assigned when the person registers in the tax authorities' databases.
                    {"\n"}It is assigned to all people created in the registration system of the Directorate-General of Public Finances (DGFiP) (referential PERS) for all taxes. It is a unique, non-significant, reliable and permanent identification number.
                    {"\n"}This tax number is indicated on the pre-printed income tax declaration form and on income tax and property tax notices.
                    {"\n"}The TIN that must be obtained by the account or contract holder, or the holder of the asset or the beneficiary of the income (National Report France, 2018).
                </Text>
            </View>
        )
    }
}
