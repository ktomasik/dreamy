import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz5": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "A checking account doesn't offer access to your money for your daily transactional needs"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Automatic bill payments are irregular payments made from a banking, brokerage or mutual fund account to vendors. "
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Pay online with revealing own credit card details",
                    "option2": "b) Pay cash",
                    "option3": "c) Pay to a bank account",
                    "option4": "d) Pay an invoice"
                },
                "question": "Which one is NOT a way to make an online payment?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Online banking refers to any banking transaction that can be conducted over the internet."
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Choose the best bank that's best for the purpose.",
                    "option2": "b) Visit the bank and ask to open an account.",
                    "option3": "c) Keep the personal information secret from the bank.",
                    "option4": "d) Keep the account documents received secure."
                },
                "question": "Which of the following directions to open a bank account is FALSE?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "Debit cards draw money directly from the checking account when the purchased is accounted immediately."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Get an Identification Number for the business.",
                    "option2": "b) Set Up to Receive Payments through a Third-Party",
                    "option3": "c) Open a Business Bank Account.",
                    "option4": "d) Create a social media account"
                },
                "question": "Which of the following steps is not required to receive payments?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) True",
                    "option2": "b) False"
                },
                "question": "The credit card issuer sends the billing statement about once a month."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Create a simple order form on facebook",
                    "option2": "b) Configure your order form notifications",
                    "option3": "c) Configure your order form confirmations",
                    "option4": "d) Configure the payment settings"
                },
                "question": "Which of the following statements is FALSE? In order to receive orders, you have to:"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Merchant Account",
                    "option2": "b) Virtual Terminal",
                    "option3": "c) Cash Money",
                    "option4": "d) Gateway"
                },
                "question": "For accepting payments by Credit Card, which of the followings is not necessary?"
            }
        }
    }
};

export default class M5Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz5;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
