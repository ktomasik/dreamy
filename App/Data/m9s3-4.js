import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S3_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Setting up a Facebook Page for business</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 3:</Text> click on
                    the “Pages” tab in the “Explore section” of the left-hand sidebar of your profile home page. From
                    the drop down, click “Create Page.”
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m9/img1.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
