import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M1S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Searching from Google and Creating an e-mail account from smart phone</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 6: Go to the "Google Account" Creation page</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img6.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
