import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create a Facebook account</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 8: Now you must setup your Facebook account. First you can ALLOW that
                    Facebook can send and view your SMS messages.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img17.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
