import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S3 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Common Social Media Features</Text>
                <Text>Today, there are scores of social media site. They provide different services, have different fan followings and enjoy very distinct identity. Yet, all of them share some common characteristics (Sunil 2017). Here are some key features of social media sites:</Text>
                <Text style={styles.dmlH3}>Provide free web space:</Text>
                <Text>Members of these sites don’t need to own or share web servers. They can publish their content on the free space provided by these sites.</Text>
                <Text style={styles.dmlH3}>Provide free web address:</Text>
                <Text>Members are allotted a unique web address that becomes the web identity of an individual or a business. It can be used to identify, connect and share content.</Text>
                <Text style={styles.dmlH3}>Ask members to build profiles:</Text>
                <Text>These sites require members to build their profiles. Information entered in the profiles is used to connect friends and contacts, and build networks that connect people with similar likes and interests across the world.</Text>
                <Text style={styles.dmlH3}>Encourage members to upload content:</Text>
                <Text>These sites allow members to upload text messages, photographs, audio and video files. All posts are published in in descending order with the last post coming first. Most important, all content is published in real time, and can be read, viewed or shared instantly.</Text>
                <Text style={styles.dmlH3}>Allow members to build conversations</Text>
                <Text>Members can browse content and comment upon it. By doing so, social media sites allow members to engage in conversations that increase engagement.</Text>
                <Text style={styles.dmlH3}>Allow live chats:</Text>
                <Text>Several social media sites have chat clients that enable members to chat with each other in real time.</Text>
                <Text style={styles.dmlH3}>Direct Messaging facility:</Text>
                <Text>Several social media sites provide direct messaging facility to their members. This allows members to send private messages, which can be read or viewed only by those for whom the message is intended.</Text>
                <Text style={styles.dmlH3}>Provide tagging alerts:</Text>
                <Text>Most social media sites alert members through e-mail or in site notifications whenever they are tagged in a message or in a photograph.</Text>
                <Text style={styles.dmlH3}>Enable members to create unique pages:</Text>
                <Text>On some social media sites, members can create theme-based pages. The pages can then be used to post articles or photographs related to a theme. The pages can also be used to promote businesses (user accounts; profile pages; friends, followers, groups, so on).</Text>
            </View>
        )
    }
}
