import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_2 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Follow the steps below to sell your products on Instagram</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>Step 2: Use your already taken Instagram account name or
                    take a new business account name for your products’ commercialization</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m3/img37.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
