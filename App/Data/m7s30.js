import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Step-by-step funding in project partner countries</Text>
                <Text style={styles.dmlH2}>For France:</Text>
                <Text style={styles.dmlH3}>If you are not a job seeker: THE AID OF THE STATE - ACCRE</Text>

                <Text style={styles.dmlH3}>Step 1:</Text>
                <Text>Your application must be filed with the competent enterprise formalities center (CFE) either when
                    your business is created or resumed or within 45 days of it. You must attach to your request:</Text>
                <Text>- The declaration form of the company at the CFE or its copy.</Text>
                <Text>- The specific form of the aid application form, which is valid on the honour of not having
                    benefited from this aid for 3 years.</Text>
                <Text>- Proof that you belong to one of the categories benefiting from the ACCRE.</Text>
                <Text>Other documents may be requested by your CFE, I advise you to contact them.</Text>

            </View>
        )
    }
}
