import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S43_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Create a Twitter account</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 11: And click ALLOW to set application permissions.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img86.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
