import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Registration to professional and occupational associations</Text>
                <Text style={{marginTop: 10}}>Follow the directions to registration for professional and occupational associations:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Tax office will be direct you to related{"\n"}chamber that can apply for professional{"\n"}and occupational associations</Text>
                    </View>
                </View>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 320}}
                        source={require('../Images/images/30321.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
