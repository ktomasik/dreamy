import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Follow the steps below to sell at the best price in the on-line environment</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 3: Put a price lower than if you saw a product similar to yours, on the page</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmarks.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
