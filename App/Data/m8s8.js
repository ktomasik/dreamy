import React, {Component} from 'react'
import {FlatList, Image, Linking, Modal, StyleSheet, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img9.png')
    }
}];

export default class M8S8 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Sending the cargo</Text>
                <Text styl={{marginTop: 10}}>Follow the steps indicated below to send the cargo:</Text>
                <Text style={styles.dmlH3}>STEP 1: PACKAGE YOUR PRODUCT WITH THE PROPER CONDITIONS</Text>
                <View style={styles2.border}>
                    <Text>Tips to make suitable cargo package:</Text>
                    <FlatList
                        data={[
                            {key: 'Choose the proper package type,'},
                            {key: 'Choose the proper package size,'},
                            {key: 'Cover sensitive products with protective soft materials,'},
                            {key: 'If any need, use some caution labels such as «fragile»,'},
                            {key: 'If any need, mark the handle angle.'}
                        ]}
                        renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                    />
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m8/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
