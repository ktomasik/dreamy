import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Where to Find the Small Business Loans for Women?</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>
                <Text style={styles.dmlH3}>Banks</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Simple and fast loan up to 7000 EUR without approval costs.',val2: 'http://www.hipkredit.si/'}},
                        {key: {val1: 'Microcredits with lower interest rate and costs where a favourable fixed monthly cost is allocated throughout the repayment period of the loan.', val2: 'http://www.intesasanpaolobank.si/'}},
                        {key: {val1: 'Attractive offer for s.p.',val2:'http://www.sparkasse.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Financial services</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Loan of value 500 or 1000 EUR.',val2:'http://www.skupina8.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Regional Chamber of Commerce and Industry</Text>
                <FlatList
                    data={[
                    {key: {val1: 'Is granting loans with a subsidy of a portion of the interest rate on short-term loans for members.',val2:'http://www.eng.gzs.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Enterprise Investment Center</Text>
                <FlatList
                        data={[
                        {key: {val1: 'Fast online loan from 1000 EUR to 30000 EUR.',val2:'http://www.pnc.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                       onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
