import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S1 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text style={{marginTop: 10}}>This module refers to commonly done in practice accounting procedures for start up women entrepreneurs. Specifically refers to the opening of bank accounts, their types, the methods of banking transactions and information about e-commerce and online (or through credit card) payment system. It also contains directions for their implementation.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 280, height: 340}}
                        source={require('../Images/images/6931.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
