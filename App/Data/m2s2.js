import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Popular networking websites of the Social media</Text>
                <Text>It has been estimated that some 81% of Americans used social media as of 2017, and increasingly so. Over one- fifth of an individual’s online time is spent on social media, according to one estimate. In 2005, the percentage of adults using social media was around 5%. Globally, there are roughly 1.96 billion social media users. That number is expected to rise to 2.5 billion by the end of 2018. Other estimates are even higher (Silver, n.d.). According to the Pew Research Center (2018), social media users tend to be younger (some 90% of people ages 18 to 29 used at least one form of social media), better educated and relatively wealthy (earning over $75,000 per year). The United States and China lead the list of social media usage (‘Leading Global Social Networks 2018
                    | Statistic’ 2018): Facebook (2.167 billion users as of January 2018), YouTube (1.5B), WhatsApp (1.3B), Facebook Messenger (1.3B), WeChat (980M), QQ (843M), Instagram (800M), Tumblr (794M), QZone (568M), Sina Weibo (376M), Twitter (330M), Baidu Tieba (300M), Skype (300M), LinkedIn (260M), Viber (260M), Snapchat (255M), Reddit (250M), LINE (203M), Pinterest (200M), YY (117M).
                    {"\n\n"}Different websites and applications that serves for social media are dedicated to different types of communic- ation and different content. A short description is available in next sections for most popular social medias (Rouse 2016).
                </Text>
                <Text style={styles.dmlH3}>Facebook</Text>
                <Text>is a popular free social networking website that allows registered users to create profiles, upload photos and video, send messages and keep in touch with friends, family and colleagues. According to statistics from the Nielsen Group, Internet users within the United States spend more time on Facebook than any other website.</Text>
                <Text style={styles.dmlH3}>Twitter</Text>
                <Text>is a free microblogging service that allows registered members to broadcast short posts called tweets. Twitter members can broadcast tweets and follow other users’ tweets by using multiple platforms and devices</Text>
                <Text style={styles.dmlH3}>Google+</Text>
                <Text>is Google’s social networking project, designed to replicate the way people interact offline more closely than is the case in other social networking services. The project’s slogan is “Real-life sharing rethought for the web.”</Text>
                <Text style={styles.dmlH3}>Wikipedia</Text>
                <Text>is a free, open content online encyclopedia created through the collaborative effort of a community of users known as Wikipedians. Anyone registered on the site can create an article for publication; registration is not required to edit articles. Wikipedia was founded in January of 2001.</Text>
                <Text style={styles.dmlH3}>LinkedIn</Text>
                <Text>is a social networking site designed specifically for the business community. The goal of the site is to allow registered members to establish and document networks of people they know and trust professionally.</Text>
                <Text style={styles.dmlH3}>Reddit</Text>
                <Text>is a social news website and forum where stories are socially curated and promoted by site members. The site is composed of hundreds of sub-communities, known as “subreddits.” Each subreddit has a specific topic such as technology, politics or music. Reddit site members, also known as, “redditors,” submit content which is then voted upon by other members. The goal is to send well-regarded stories to the top of the site’s main thread page.</Text>
                <Text style={styles.dmlH3}>Pinterest</Text>
                <Text>is a social curation website for sharing and categorizing images found online. Pinterest requires brief descrip- tions but the main focus of the site is visual. Clicking on an image will take you to the original source, so, for
                    example, if you click on a picture of a pair of shoes, you might be taken to a site where you can purchase them. An image of blueberry pancakes might take you to the recipe; a picture of a whimsical birdhouse might take you to the instructions.
                </Text>
            </View>
        )
    }
}
