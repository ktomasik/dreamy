import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S6 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Tax basics</Text>
                <Text>Tax liability is the amount of money an individual owe to tax authorities (Cameron 2017).
                    {"\n"}The government uses tax payments to fund social programs and administrative roles.
                    {"\n"}Basically, a tax liability is usually a certain percentage of one's income and varies according to income.
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For TÜRKİYE</Text>
                <FlatList
                    data={[
                        {key: 'It is necessary to make an address declaration. A home address can be an invoice address.'},
                        {key: 'During the application, simple taxation should be decelerated.'},
                        {key: 'After the evaluation of tax office, office will send tax card to tax payer.'},
                        {key: 'Registration should be done to ‘Confederation of Turkish Tradesmen and Craftsmen.'},
                        {key: 'Application should be done to municipality with a petition for work permission.'},
                        {key: 'The taxpayers who are subject to the small business taxation will obtain the documents from the chambers or associations.'},
                        {key: 'The taxpayers must declare their income by annual declaration.'},
                        {key: 'The declaration will be done to the registered tax office. As a result, the taxpayer will pay the tax based on the invoice (National Report Turkey, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For SLOVENIA</Text>
                <FlatList
                    data={[
                        {key: 'VAT (value added tax)'},
                        {key: 'Corporate Income Tax'},
                        {key: 'Personal Income Tax'},
                        {key: 'Social Security Contributions'},
                        {key: 'Immovable Property Transfer Tax'},
                        {key: 'Capital Gains Tax'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Choose business form:</Text>
                <FlatList
                    data={[
                        {key: 'Personal entrepreneurs, taxations is an income from an activity, which is derived from the performance any entreprenurial agricultural, forestry, occupational or other independent self-employed activity.'},
                        {key: 'Limited liability company, is a legal personality and taxed with Corporate Income Tax.'},
                        {key: 'E-Sellers taxes, who is taxable person, is identified for VAT purposes, has to calculate VAT from the delivery of the product to the customer in Slovania (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For POLAND</Text>
                <Text>For individuals who want to conduct their own business, tax liabilities are paid on the basis of a tax on income, as is the case for full time employees. An individual must choose one:</Text>
                <FlatList
                    data={[
                        {key: 'General rules tax,'},
                        {key: '19% tax (flat tax),'},
                        {key: 'Lump-sum from registered income,'},
                        {key: 'Tax card (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For GREECE</Text>
                <FlatList
                    data={[
                        {key: 'Individuals and businesses must submit an electronic tax declaration through online system of the Independent Authority of Public Revenue, based on which they will be taxed.'},
                        {key: 'At the end of each month, they submit the list of invoices and VAT.'},
                        {key: 'For individuals who want to conduct their own business, tax liabilities are paid on the basis of a tax on income.'},
                        {key: 'As is the case for full-time emplooyes- without the tax deduction is occurred.'},
                        {key: 'Irrespective of profits all Greek entities are taxed with rate of 29%.'},
                        {key: 'Shares are taxed with a rate of 15% (National Report Greece, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For FRANCE</Text>
                <Text>Taxation of profits depends on the legal structure of the business. Entities may subject to Income Tax (IR) or Corporate Tax(CI).</Text>
                <FlatList
                    data={[
                        {key: 'Companies are subject to'},
                        {key: 'Taxation of its profits,'},
                        {key: 'Territorial economic contribution (CET),'},
                        {key: 'VAT,'},
                        {key: 'Individual companies (craftsmen, tradesmen), the liberal professions and EURL (one-man limited liability company) must pay the IR.'},
                        {key: 'Partners are taxed personally in respect of income tax only on the salaries or dividends (National Report France, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
