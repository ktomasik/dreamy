import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";
// Styles

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m4/img1.png')
    }
}];

export default class M4S2 extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>What is the difference between a business name, a trading name and a legal name?</Text>
                <Text>A business name is; the official name of the person or entity that owns the company.
                {"\n"}It is business’s legal name. A business name is used on government forms and applications (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>As an example:</Text> Enterpreuner’s name is John Smith, and he owns an insurance business. Companies’ legal name can be John Smith Insurance (Anonymous 2018b).
                {"\n\n"}Business owners can use a trade name for advertising and sales purposes. The trade name is the name the public sees, like on signs and the internet (Cameron 2017).
                {"\n"}Business name and trading name can be different. A trade name does not need to include LLC, Corp, or other legal endings used for tax entity (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>As an example:</Text> McDonald’s is a trade name. The company’s legal business name is McDonald’s Corporation (Anonymous 2018c).
                </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m4/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop:10}}>
                    A legal name of a business is; the name of the person or entity that owns a business. If the business is a partnership, the legal name is the name given in the partnership agreement or the last names of the partners.
                    {"\n\n"}For limited liability companies (LLCs) and corporations, the business' legal name is the one that was registered with the state government. These names will often have a “legal ending” such as LLC, Inc. or LLP (Fishman 2015).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10}}>When should a legal name or trade name be used?</Text>
                <Text>A legal name should be used when communicating with the government or other businesses.
                    {"\n"}For example, the business’ legal name should be used when filing tax returns, buying property, or writing checks.
                    {"\n"}A company may use a trade name for advertising and trade purposes. It is often the name the general public sees on signs, the internet, and advertisements (Anonymous 2018d).
                    {"\n\n"}Basically:
                    {"\n"}Legal name is for government procedures,
                    {"\n"}Trade name is for public relations.
                </Text>
            </View>
        )
    }
}
