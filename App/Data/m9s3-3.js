import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3_3 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Setting-up Social Media Accounts for Business</Text>
                <Text style={styles.dmlH3}>Setting up a Facebook Page for business</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Step 2:</Text> click the “Home” button
                    next to your name
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
