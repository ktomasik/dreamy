import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";


export default class M4S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Registration of business brand name</Text>
                <Text style={{marginTop: 10}}>Follow the directions to register business brand name:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Decide business brand name</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Register to an Institue</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Pay a registration fee</Text>
                    </View>
                </View>
            </View>
        )
    }
}
