import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img1.png')
    }
}];

export default class M6S3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Invoicing on Mobile</Text>
                <Text style={styles.dmlH3}>For iOS</Text>
                <Text>The free version of Invoice Simple is available on iPhone, iPad, and iPod Touch and can be used to
                    create 3 free invoices or estimates on your mobile device.</Text>
                <Text style={{fontSize: 18, marginTop: 10}}><Text style={{fontWeight: 'bold'}}>Step 1:</Text> Go App Store and search “invoice” (see modules 1
                    and 2 to see how to go and how to search)</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
