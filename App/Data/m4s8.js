import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S8 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Contract formation issue for e-trade</Text>
                <Text style={{marginTop: 10}}>For all countries, the main steps are described as below:</Text>
                <FlatList
                    data={[
                        {key: 'Step 1: Establishing the offer and acceptance procedure'},
                        {key: 'Step 2: Completing the order form'},
                        {key: 'Step 3: Incorporating the terms and conditions)'},
                        {key: 'Step 4: Taking the consumer\'s credit card details on-line'},
                        {key: 'Step 5: Acknowledging receipt of the order'},
                        {key: 'Step 6: Providing confirmation of the information provided and the right to cancel'},
                        {key: 'Step 7: Delivery'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For TÜRKİYE</Text>
                <Text>The E-Commerce Regulations require that all commercial web sites make the following information directly and permanently available to consumers via the website:</Text>
                <FlatList
                    data={[
                        {key: 'the company\'s name, postal address (and registered office address if this is different) and email address;'},
                        {key: 'the company\'s registration number;'},
                        {key: 'any Trade or Professional Association memberships;'},
                        {key: 'the company\'s VAT number.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>All of these data must be included regardless of whether the site sells on-line. In addition, any commercial communication such as e-mail or SMS text service used in providing an "Information Society Service" must display this information.
                    {"\n"}The E-Commerce Regulations also require that all prices must be clear, and web sites must state whether the prices are inclusive of taxes and delivery costs (National Report Turkey, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For SLOVENIA</Text>
                <Text>Register the company to the Public Payments Administration of the Republic of Slovenia.
                    {"\n\n"}Obtain access to using the portal for issuing invoices. (before a digital certificate must be obtained).
                    {"\n\n"}Website of the Public Payments Administration of the Republic of Slovenia can be accessed (National Report Slovenia, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For POLAND</Text>
                <Text>Since 2018, each entrepreneur has to prepare VAT.
                    {"\n\n"}As a result, entrepreneur must prepare the invoice (preparing e-invoice is given not only by the computer programs, but also by some banks via bank accounts.)
                    {"\n\n"}All required documents related to social security of entrepreneur have to be seen, in electronic form via dedicated program prepared by SII (Social Insurance Institution-called PLATNIK) (National Report Poland, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For GREECE</Text>
                <Text>E- invoicing was partly introduced in Greece, 2006.
                    {"\n\n"}But the electronic system is still not fully operating, will be completed till the end of 2019.
                    {"\n\n"}Aiming that, as soon as an invoice is issued, to notify in real time the client’s accounting system to accept the charge, at the same time tax office can collect the tax (National Report Greece, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>For FRANCE</Text>
                <Text>Commercial sides which collect personal info (name, e-mail..)and constitute files of customers and prospect, must make a simplified declaration to the National Commission for Informatics and Liberties.
                    {"\n\n"}Online trade sites generally fall under Simplified Standards 48 (National Report France, 2018).
                </Text>
            </View>
        )
    }
}
