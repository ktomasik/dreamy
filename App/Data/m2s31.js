import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S31 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Share from Instagram to other social networks</Text>
                <Text style={{marginTop: 10}}>In next exercise we will show:</Text>
                <FlatList
                    data={[
                        {key: 'how to post a content'},
                        {key: 'how to share the content with other social networks and'},
                        {key: 'how to add a content into relevant subject.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 3, marginLeft: 15}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Step 1: To post new content press camera sign at the bottom of the
                    screen.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img59.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
