import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wprowadzenie</Text>
                <Text>Moduł ten ma na celu wyjaśnienie różnych możliwości finansowych i sposobów dla kobiet, które chcą
                    sprzedawać swoje wyroby rzemieślnicze. Moduł zapewnia informacje i linki do informacji o wsparciu
                    finansowym i kredytach w pięciu krajach (Słowenii, Francji, Turcji, Grecji i Polsce) w projekcie
                    Dreamy m-learning.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_48_3319639.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
