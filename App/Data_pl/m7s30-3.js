import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text style={styles.dmlH3}>Jeśli nie jesteś poszukującym pracy POMOC PAŃSTWA ACCRE</Text>
                <Text style={styles.dmlH3}>Krok 3:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'W przypadku pozytywnej reakcji Urssaf wyda ci zaświadczenie o przyjęciu. Jeśli nie, podaje uzasadnienie i powiadamia o swojej decyzji o odrzuceniu. Jeśli nie ma odpowiedzi w ciągu jednego miesiąca, ACCRE jest uznawane za udzielone.',
                                val2: 'https://www.legalstart.fr/fiches-pratiques/aides-creation-entreprise/aide-creation-entreprise-femmes/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
