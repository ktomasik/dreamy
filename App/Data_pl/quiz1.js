import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz1": {
            "question1": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Nazwy użytkownika",
                    "option2": "b) Hasła",
                    "option3": "c) Danych kontaktowych",
                    "option4": "d) Adresu domowego"
                },
                "question": "Aby utworzyć konto e-mail, nie jest wymagane podawanie?"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Spam i mail z wirusem znaczą to samo"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Mogą występować różnice w ustawianiu konta e-mail dla różnych marek smartfonów"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) World wide web",
                    "option2": "b) Internet",
                    "option3": "c) Mail",
                    "option4": "d) Spam"
                },
                "question": "________ jest tworzony z milionów powiązanych ze sobą stron internetowych"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Konieczne jest posiadanie konta by móc korzystać z poczty e-mail."
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Konta bankowego",
                    "option2": "b) Konta e-mail",
                    "option3": "c) Adresu domowego",
                    "option4": "d) Numeru telefonu"
                },
                "question": "Aby wysłać maila, niezbędnym jest posiadanie:"
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Nazwa medium społecznościowego",
                    "option2": "b) Konto e-mail",
                    "option3": "c) Oprogramowanie przeznaczone do wyszukiwania informacji w Internecie",
                    "option4": "d) Strona internetowa"
                },
                "question": "“Wyszukiwarka” to:"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Email użytkownika może być dodany do ustawień smartfona."
            },
            "question9": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Do wyszukiwania w Internecie",
                    "option2": "b) Do niszczenia wirusów",
                    "option3": "c) Do przesyłania danych ",
                    "option4": "d) Aby oddzielić nazwę użytkownika od adresu internetowego użytkownika"
                },
                "question": "Do czego służy @?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Wyszukiwarka ",
                    "option2": "b) Adres e-mail",
                    "option3": "c) Rodzaj wirusa",
                    "option4": "d) Spam"
                },
                "question": "Google to:"
            }
        }
    }
};

export default class M1Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz1;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
        /*console.log('countCheck: ' + this.state.countCheck);
        console.log('punkty: ' + this.score)*/
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
