import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie konta e-mail w telefonie komórkowym dla aplikacji pocztowej
                    Android (Samsung, Sony, HTC ..)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 5: wprowadź swoje hasło i kliknij dalej</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
