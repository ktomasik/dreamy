import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Co to jest Internet?</Text>
                <Text>Internet to globalna sieć – zawierająca sieci prywatne, publiczne, biznesowe, akademickie i
                    rządowe - połączone za pomocą technologii przewodowych, bezprzewodowych i światłowodowych.</Text>
                <Text style={styles.dmlH2}>Krótka historia Internetu</Text>
                <Text>Internet miał swoje korzenie w latach sześćdziesiątych jako projekt Departamentu Obrony rządu
                    Stanów Zjednoczonych, który stworzył niescentralizowaną sieć. Projekt ten nazwano ARPANET (Advanced
                    Research Projects Agency Network). Aby uczynić sieć bardziej globalną, opracowano nowy wyrafinowany
                    i standardowy protokół IP (Internet Protocol), który określał, w jaki sposób wiadomości
                    elektroniczne były pakowane, adresowane i przesyłane przez sieć.</Text>
                <Text style={styles.dmlH2}>Co to jest Sieć - (World Wide Web)</Text>
                <Text>Sieć lub World Wide Web (W3) to system serwerów internetowych obsługujących specjalnie
                    sformatowane dokumenty.</Text>
                <Text style={styles.dmlH2}>Różnice między Internetem a Siecią</Text>
                <Text>Internet, łączący komputer z innymi komputerami na całym świecie, jest sposobem transportu treści.
                    Musisz uzyskać dostęp do Sieci, aby przeglądać sieć WWW i dowolne strony internetowe lub inne
                    treści, które zawiera. Sieć to część Internetu, która udostępnia informacje. Sieć wykorzystuje
                    również przeglądarki, takie jak Google lub Internet Explorer, do uzyskiwania dostępu do dokumentów
                    sieci Web zwanych stronami internetowymi, które są ze sobą połączone za pomocą hiperłączy.</Text>
            </View>
        )
    }
}
