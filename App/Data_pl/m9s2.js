import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Co oznaczają zakupy online?</Text>
                <Text>Zakupy online to forma handlu elektronicznego, która umożliwia konsumentom bezpośredni zakup
                    towarów lub usług od sprzedawcy przez Internet za pomocą przeglądarki internetowej.</Text>

                <Text style={styles.dmlH2}>Co oznacza marketing internetowy?</Text>
                <Text>Marketing internetowy to zestaw narzędzi i metodologii wykorzystywanych do promowania produktów i
                    usług przez Internet . Marketing internetowy obejmuje szerszy zakres elementów marketingowych niż
                    tradycyjny marketing biznesowy ze względu na dodatkowe kanały i mechanizmy marketingowe dostępne w
                    Internecie.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmark2.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
