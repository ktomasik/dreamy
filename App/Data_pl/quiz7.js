import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {

        "quiz7": {
            "question1": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Im niższe są odsetki, tym tańsza jest pożyczka."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) W banku.",
                    "option2": "b) W regionalnej izbie przemysłowo-handlowej",
                    "option3": "c) W firmach świadczących usługi finansowe",
                    "option4": "d) W centrum przedsiębiorczości"
                },
                "question": "Gdzie można znaleźć najlepszą ofertę pożyczki dla małego biznesu?"
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Granty to fundusze, które muszą być spłacone"
            },
            "question4": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Zachęty dla start-upów mają charakter niefinansowy"
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Promowanie inwestycji prywatnych",
                    "option2": "b) Wsparcie dla tworzenia i rozwoju mikroprzedsiębiorstw",
                    "option3": "c) Zapewnienie korzystnych środków finansowych dla formy",
                    "option4": "d) Zachęty finansowe w regionalnych programach gwarancyjnych"
                },
                "question": "Jaka jest najbardziej typowa forma wsparcia bankowego dla biznesu kobiet?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Zachęta podatkowa to sposób rządu na zachęcenie osób do oszczędzania poprzez zmniejszenie kwoty ich opodatkowania."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) mikrokredytem",
                    "option2": "b) parasolem finansowym",
                    "option3": "c) mikropożyczką",
                    "option4": "d) żadnym z powyższych"
                },
                "question": "Mikrofinansowanie jest ściśle związane z:"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Mikrofinansowanie i kapitał społeczny mają znaczący wpływ na wzmocnienie przedsiębiorczości."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Anioły biznesu są zainteresowane zaangażowaniem w projekt poprzez działania jako przewodnik lub mentor."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Kredyt",
                    "option2": "b) Saldo",
                    "option3": "c) Dotacja",
                    "option4": "d) Zakup"
                },
                "question": "____________ jest wtedy kiedy pożyczasz pieniądze od banku."
            }
        }
    }
};

export default class M7Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz7;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
