import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Dotacje dla małych firm prowadzonych przez kobiety</Text>
                <Text style={styles.dmlH2}>Dla Turcji:</Text>
                <Text style={styles.dmlH3}>KOSGEB</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Organizacja Rozwoju Małych i Średnich Przedsiębiorstw (KOSGEB) jest głównym organem odpowiedzialnym za rozwój, koordynację i wdrożenie polityki MŚP. KOSGEB oferuje wsparcie pożyczek rządowych dla kobiet-przedsiębiorców, które chcą założyć własną firmę. W tym zakresie wsparcie kredytowe w wysokości 50 000 TL (10.000 Euro) jest udzielane w ramach wniosku o pożyczkę KOSGEB. Wsparcie udzielane przez państwo jest całkowicie bezzwrotne. Oprócz tego wsparcia dotacyjnego, kobiety otrzymują nieoprocentowane wsparcie kredytowe. Do 2018 r. pożyczka ta wynosi 70 000 TL (14 000 euro). Pierwsza firma dla kobiet prowadzących działalność gospodarczą będzie również wolna od podatków przez pierwsze trzy lata.',
                                val2: 'https://www.kosgeb.gov.tr/site/tr/genel/detay/6057/kadin-girisimciligi-women-entrepreneurship'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
