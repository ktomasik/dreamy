import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen4 = [{
    url: '',
    props: {
        source: require('../Images/m3/img28.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m3/img29.png')
    }
}];

export default class M3S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe czynności, aby zrobić zdjęcia produktów i umieścić je w
                    sieci</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 13: Dodaj zdjęcia swoich produktów, ile chcesz, które chcesz sprzedać:</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen4}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img28.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img29.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
