import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Turcji:</Text>
                <Text style={styles.dmlH3}>Krok 1:</Text>
                <Text>Potencjalni przedsiębiorcy, którzy chcą uzyskać wsparcie państwa, muszą wziąć udział w szkoleniu z
                    zakresu przedsiębiorczości prowadzonym przez KOSGEB. Około 45% wszystkich szkoleń KOSGEB zajmują
                    kobiety.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
