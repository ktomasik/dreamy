import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S22 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Mikrofinansowanie i inny fundusz początkowy dla kobiet</Text>
                <Text style={styles.dmlH2}>Dla Słowenii:</Text>
                <Text style={styles.dmlH3}>Vstopne točke SPOT (VEM)</Text>
                <Text>Informacje, podstawowe porady, rejestracja firmy</Text>

                <Text style={styles.dmlH3}>Podjetniški inkubatorji</Text>
                <Text>Wyposażone biura, firmy i inne usługi wsparcia</Text>

                <Text style={styles.dmlH3}>Univerzitetni inkubatorji</Text>
                <Text>Wyposażone biura, doradztwo biznesowe i mentoring, bezpłatne warsztaty edukacyjne.</Text>
                <Text>Podjetniški inkubator Univerze v Mariboru</Text>
                <Text>Ljubljanski univerzitetni inkubator</Text>
                <Text>Univerzitetni razvojni center in inkubator Primorske</Text>

                <Text style={styles.dmlH3}>Tehnološki parki</Text>
                <Text>Wyposażone biura, mentoring, doradztwo, coworking Informacje</Text>

                <Text style={styles.dmlH3}>Iniciativa Start:up Slovenija</Text>
                <Text>Networking, organizacja zawodów</Text>

                <Text style={styles.dmlH3}>Mreža European Enterprise Network (EEN)</Text>
                <Text>Wyszukiwanie partnerów biznesowych, informacje, porady</Text>

                <Text style={styles.dmlH3}>Coworking prostori Coworking MB Hekovnik</Text>
                <Text>Rozpoczęcie działalności, tworzenie sieci, poszukiwanie partnerów biznesowych, informacje, doradztwo, szkolenia</Text>

                <Text style={styles.dmlH3}>Gospodarska zbornica Slovenije</Text>
                <Text>Doradztwo, szkolenia, pomoc w internacjonalizacji</Text>

                <Text style={styles.dmlH3}>Obrtno podjetniška zbornica Slovenije</Text>
                <Text>Doradztwo, szkolenia, wydawanie licencji rzemieślniczych, certyfikaty UE, certyfikaty dla okazjonalnych zajęć rzemieślniczych w Republice Słowenii, licencje na wykonywanie transportu</Text>

                <Text style={styles.dmlH3}>Program Erasmus za mlade podjetnike</Text>
                <Text>Współfinansowanie szkoleń z zakresu przedsiębiorczości - wymiana doświadczeń między przedsiębiorcami w UE.</Text>
            </View>
        )
    }
}
