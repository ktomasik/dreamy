import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym są anioły biznesu?</Text>
                <Text>Anioły biznesu (znane również jako inwestorzy aniołów) to osoby, które wykorzystują swój osobisty
                    majątek, aby zapewnić kapitał przedsiębiorstwom rozpoczynającym działalność i znajdującym się na
                    wczesnym etapie działalności, w zamian za udział w kapitale spółki. Napływ kapitału może pomóc
                    rozwinąć się w rentowną firmę i zapewnić podstawy do rozpoczęcia produkcji proponowanego produktu
                    lub usługi.</Text>
                <Text>Definicja "anioła biznesu" pozostaje niejasna, podobnie jak terminy, "nieformalny inwestor" i
                    "nieformalny kapitał wysokiego ryzyka".</Text>
                <Text style={styles.dmlH2}>Co mogą zaoferować?</Text>
                <Text>Anioły biznesu są w stanie zaoferować:
                    {"\n"}- zastrzyk gotówki za stosunkowo niewielkie kwoty, które w innym przypadku nie byłyby dostępne
                    za pośrednictwem kapitału podwyższonego ryzyka,
                    {"\n"}- często finansują dlasze etapy tej samej firmy,
                    {"\n"}- są ogólnie zainteresowani zaangażowaniem się w projekt, działając jako przewodnik lub
                    mentor,
                    {"\n"}- inwestują swój czas, a także udostępniają połączenia z większą siecią, aby pomóc kierować
                    przedsiębiorstwem w nowym przedsięwzięciu
                </Text>
                <Text style={styles.dmlH2}>Fundusz aniołów biznesu dla kobiet</Text>
                <Text>Fundusz aniołów biznesu dla kobiet jest zazwyczaj częścią stowarzyszenia, które jest profesjonalną
                    organizacją non-profit, wspierającą udział kobiet w podejmowaniu decyzji dotyczących innowacji.
                    Inwestycja w anioły biznesu oznacza nie tylko zapewnienie środków finansowych, ale także dzielenie
                    się wiedzą, doświadczeniem i kapitałem społecznym. W przekonaniu stowarzyszenia udział kobiet w
                    większej liczbie i w nowych formach przyniósłby korzyści ekonomiczne wszystkim.</Text>
            </View>
        )
    }
}
