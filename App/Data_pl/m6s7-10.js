import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img30.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m6/img31.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m6/img32.png')
    }
}];

export default class M6S7_10 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Tworzenie podpisu elektronicznego</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 9:</Text> W polu
                    ODBIORCY wprowadź nazwę odbiorcy i adres e-mail.</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img30.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img31.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginBottom: 5}}>Po zakończeniu kliknij przycisk Dalej, a następnie Dodaj pola
                    podpisywania</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img32.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginBottom: 5}}>To wszystko!</Text>
            </View>
        )
    }
}
