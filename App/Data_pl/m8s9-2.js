import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img13.png')
    }
}];

export default class M8S9_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Wysyłanie ładunku</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>KROK 6: ZAPYTAJ O FAKTURĘ</Text>
                <Text>Faktura zawiera następujące informacje:</Text>
                <FlatList
                    data={[
                        {key: 'imię i nazwisko nadawcy, adres, telefon itp.,'},
                        {key: 'Nazwa, adres, telefon itp. Odbiorcy,'},
                        {key: 'Dane dotyczące rozmiaru i rodzaju ładunku, takie jak ilość, wymiary itd.,'},
                        {key: 'Szczegóły płatności (odbiorca / nadawca),'},
                        {key: 'Data, szczegóły faktury i numer śledzenia.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img13.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
