import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wyszukiwanie z Google i Tworzenie konta e-mail ze smartfona</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 3: napisz słowo lub zdanie, które chcesz wyszukać i
                    wybierz z wyników to, które Cię interesuje</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img2.png')}
                        resizeMode="contain"/>
                    <Text style={{marginTop: 10}}/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m1/img3.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
