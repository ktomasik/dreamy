import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img8.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m9/img9.png')
    }
}];

export default class M9S5_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Konto biznesowe na Instagramie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok
                    1:</Text> Zarejestruj się, używając swojego adresu e-mail lub numeru telefonu, a następnie wprowadź
                    nazwę użytkownika.</Text>
                <Text style={{textAlign: 'center', color: 'red'}}>LUB</Text>
                <Text>Jeśli masz już konto Facebook, możesz zalogować się tą samą nazwą użytkownika</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Kliknij Zarejestruj się, następnie wprowadź swój adres e-mail i dotknij
                    Dalej lub dotknij Zaloguj się przez Facebooka, aby zarejestrować się przy użyciu konta
                    Facebook.</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
