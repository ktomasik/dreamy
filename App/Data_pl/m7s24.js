import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S24 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Mikrofinansowanie i inny fundusz początkowy dla kobiet</Text>
                <Text style={styles.dmlH2}>Dla Grecji:</Text>
                <Text style={styles.dmlH3}>The People's Trust</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'The People\'s Trust oferuje mikrogranty dla greckich przedsiębiorców, którzy chcą albo utworzyć nowy biznes, albo rozwinąć istniejący biznes, ale mają trudności z dostępem do kredytu. Dotacja wynosi maksymalnie 10 000 EUR na firmę, która stanowi kapitał początkowy dla nowej działalności lub kapitału obrotowego dla istniejącej. Ten program finansowania koncentruje się na grupach o niskim dostępie do innych form finansowania.',
                                val2: 'http://www.thepeoplestrust.org'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Microfinancing (AFI & Eurobank)</Text>
                <Text>AFI (Action Finance Initiative) jest spółką cywilną non profit. Została założona w Grecji w 2014 r. przez ActionAid Hellas i francuską organizację ADIE, lidera mikrokredytów w Europie.
                    {"\n"}Eurobank współpracuje z AFI w zakresie udzielania pomocy mikrokredytom (do 15 000 EUR) osobom długotrwale bezrobotnym, osobom należącym do wrażliwych kategorii obywateli i mikroprzedsiębiorców bez dostępu do pożyczek bankowych. Oferują im możliwość tworzenia własnej pracy (samozatrudnienia) lub tworzenia małych jednostek biznesowych i tworzenia nowych miejsc pracy.
                    {"\n"}AFI zakłada preselekcję, szkolenie i mentoring kandydatów. Eurobank podejmuje kontrolę kredytową i finansowanie.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'AFI assumes the preselection, training and mentoring of candidates. Eurobank undertakes credit control and funding.',
                                val2: 'https://www.eurobank.gr/el/business/proionta-kai-upiresies/proionta-upiresies/xrimatodotiseis/anaptuksiaka/easy-afi'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
