import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_4 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe kroki, aby sprzedać swoje produkty na Instagramie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 4: Napisz tekst związany z produktem, wyjaśnienie i
                    cenę, podaj swój prywatny adres e-mail w celu komunikacji z klientem.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522778-PIXKA7-878.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
