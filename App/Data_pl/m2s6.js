import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Facebooku</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 2: W oknie wyszukiwania wpisz "Facebook" i kliknij
                    przycisk ZAINSTALUJ. Następnie poczekaj na zakończenie instalacji.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img4.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img5.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img6.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
