import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S42_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Twitterze</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 9: Wypełnij (ustaw) hasło do konta Twitter i kliknij next.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img84.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
