import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1INTRO extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Wprowadzenie</Text>
                <Text>Grupą docelową projektu Dreamy m- Learning są kobiety o niższym poziomie wykształcenia, które
                    pracują w domu wytwarzając rękodzieło. Celem projektu jest pomoc w nabywaniu umiejętności związanych
                    z mobilnością cyfrową i przedsiębiorczością, w celu sprzedawania swoich wyrobów rękodzielniczych na
                    rynkach cyfrowych dzięki sprawnemu wykorzystaniu smartfonów. Opracujemy aplikację mobilną, która
                    jest łatwa do zrozumienia i łatwa w użyciu; dla systemów operacyjnych Android i iOS, mającą
                    interfejs internetowy łączący aplikacje mobilne z portalem m-learning. Zgodnie z tym celem,
                    przygotowaliśmy moduły dopasowane do potrzeb grupy docelowej. Wszystkie mobilne aplikacje
                    udostępnione w przygotowanych przez nas modułach są dostępne bezpłatnie.</Text>
                <Text style={{marginTop: 10}}>Zgodnie z powyższym planem, moduł ten ma na celu wyjaśnienie, w jaki
                    sposób kobiety mogą stworzyć konto e-mail i skonfigurować to konto na swoich smartfonach (IOS i
                    Android). Ponadto moduł ten będzie przydatnym narzędziem dla osób, które chcą uczyć się podobnych
                    tematów.</Text>
            </View>
        )
    }
}
