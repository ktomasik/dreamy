import React, {Component} from 'react'
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M6S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz umowę kupna-sprzedaży dla zakupów internetowych</Text>
                <Text style={{marginTop: 15}}>Ponieważ zakupy internetowe różnią się od tradycyjnych zakupów, w
                    szczególności transakcje dokonywane za pośrednictwem Internetu podlegają rozporządzeniu w sprawie
                    umów zawieranych na odległość. Umowa zawarta między przedsiębiorcą a konsumentem, gdy nie są razem,
                    która jest negocjowana i uzgadniana przez jeden lub więcej zorganizowanych środków porozumiewania
                    się na odległość - na przykład przez telefon, pocztę lub przez Internet</Text>
                <View style={styles2.border}>
                    <Text>W większości przypadków ogólne zasady dotyczące kształtowania stosunku umownego dotyczącego
                        zakupów internetowych można stosować po następujących ogólnych działaniach między sprzedawcą a
                        nabywcą:</Text>
                    <FlatList
                        data={[
                            {key: 'zaproszenie do zawarcia umowy: wystawianie towarów na sprzedaż w witrynie e-commerce jest zazwyczaj postrzegane jako zaproszenie do negocjacji, a nie oferta, która jest podobna do tradycyjnego sklepu fizycznego. Ofertę wypełniają raczej działania klienta odwiedzającego witrynę, a nie sprzedającego;'},
                            {key: 'klient komunikuje swoją ofertę: ponieważ reklama towarów lub usług na stronie internetowej nie jest ofertą, impulsem jest wówczas, aby kupujący złożył ofertę, a klient przekaże ofertę drogą elektroniczną - co można zrobić na stronie internetowej - oferowanie zakupu reklamowanego produktu;'},
                            {key: 'sprzedawca akceptuje ofertę:  po złożeniu oferty przez klienta, od sprzedawcy zależy zaakceptowanie oferty w sposób jednoznaczny i bezwarunkowy poprzez przekazanie jej akceptacji klientowi.'}
                        ]}
                        renderItem={({item}) => <Text
                            style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                    />
                    <Text style={{marginTop: 15}}>Po zaakceptowaniu przez sprzedawcę oferty klienta, wystąpił stosunek
                        umowny</Text>
                    <Text><Text style={{fontWeight: 'bold'}}>Źródło: </Text><Text style={{color: 'blue'}}
                                                                                  onPress={() => Linking.openURL('http://www.findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f.aspx')}>findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f</Text></Text>
                </View>
                <Text style={{marginTop: 15}}>Przed zawarciem umowy między konsumentem a sprzedawcą w Internecie,
                    usługodawca lub dostawca towarów w kwestiach określonych w rozporządzeniu musi poinformować
                    konsumenta. Niektóre informacje, które sprzedający powinien przekazać konsumentowi w ramach
                    rozporządzenia przed zawarciem umowy, są następujące:</Text>
                <FlatList
                    data={[
                        {key: 'nazwisko, tytuł, adres, telefon i inne informacje dostępowe dostawcy lub dostawcy,'},
                        {key: 'podstawowe cechy towarów lub usług objętych umową,'},
                        {key: 'cenę sprzedaży towarów lub usług, w tym wszystkie podatki,'},
                        {key: 'jeśli istnieją jakiekolwiek koszty dostawy,'},
                        {key: 'informacje na temat płatności i dostawy lub wykonania;'},
                        {key: 'warunki korzystania z prawa odstąpienia.'}
                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Te wstępne informacje muszą zostać potwierdzone przez konsumenta przed
                    dokonaniem zakupu przez Internet, w przeciwnym razie stosunek umowny nie zostanie ustalony.</Text>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',
        padding: 10
    }
});
