import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img4.png')
    }
}];

export default class M9S3_7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Tworzenie strony Facebook dla firm</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 6:</Text> Dodaj
                    zdjęcia: Prześlij profil i obraz nagłówkowy do swojej strony na Facebooku. Dodatkowo możesz przesłać
                    swoje logo firmy. Jeśli nie masz logo, umieść zdjęcie jednej z twoich ostatnich kreacji i zaplanuj
                    aktualizację. Ważne jest, aby stworzyć dobre wizualne wrażenie (Zobacz ten temat Moduł 3).
                    {"\n\n"}Z drugiej strony ważna jest również wielkość obrazu. Rozmiar zdjęcia na okładkę Facebooka
                    wielkości 563 x 315 pikseli pojawia się poprawnie na urządzeniach mobilnych.
                </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img4.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
