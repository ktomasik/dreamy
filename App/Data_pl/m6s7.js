import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img20.png')
    }
}];

export default class M6S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Tworzenie podpisu elektronicznego</Text>
                <Text style={{marginTop: 15}}>Pojawiły się nowe możliwości biznesowe, ponieważ transakcyjne systemy
                    papierowe są przenoszone online. Używanie Twojego podpisu elektronicznego w procesach opartych na
                    papierach firmowych, takich jak faktury, kontrakty, wiadomości e-mail itp., jest ważne dla poprawy
                    procesów biznesowych Twojej firmy. Istnieje wiele platform dostępnych dla telefonów komórkowych do
                    podpisu elektronicznego. Jednym z nich jest DocuSign. DocuSign to bezpłatna i wygodna platforma do
                    podpisu elektronicznego do wykorzystania w telefonach komórkowych. Jest dostępny na: iPhone, iPad,
                    Android i Windows.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img20.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
