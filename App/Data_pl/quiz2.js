import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz2": {
            "question1": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Łowieniem ryb za pomocą sieci",
                    "option2": "b) To osoba zajmująca się naprawianiem systemów sieciowych",
                    "option3": "c) To grupa ludzi, którzy wymieniają informacje",
                    "option4": "d) Rodzajem pomocy publicznej dla ludzi"
                },
                "question": "Czym jest networking?"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Tworzenie kont w mediach społecznościowych różni się w zależności od używanej marki smartfona."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Grupą ludzi oglądających mecz piłki nożnej",
                    "option2": "b) Komputerową technologią do budowania wirtualnych sieci i udostępniania informacji",
                    "option3": "c) Wspólnotą technologii (np. telewizja, radio), w której można gromadzić wiadomości społecznościowe",
                    "option4": "d) Prywatnym kanałem telewizyjnym do oglądania filmów"
                },
                "question": "Czym są media społecznościowe?"
            },
            "question4": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Instagram",
                    "option2": "b) YouTube",
                    "option3": "c) Facebook",
                    "option4": "d) Google",

                },
                "question": "Do mediów społecznościowych nie należy:"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Regularne wysyłanie reklam – każdego dnia",
                    "option2": "b) Publikowanie jak największej ilości treści aby kanał był „żywy”",
                    "option3": "c) Odpowiadanie na komentarze",
                    "option4": "d) Blokowanie osób w mediach społecznościowych"
                },
                "question": "Jaka jest najlepsza praktyka na utrzymanie odbiorców?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "W niektórych serwisach społecznościowych członkowie nie mogą tworzyć stron tematycznych."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Posiadanie jasno określonych celów marketingowych",
                    "option2": "b) Posiadanie wystarczającej ilości pieniędzy do rozpoczęcia działalności",
                    "option3": "c) Zapewnienie produkty dobrej jakości",
                    "option4": "d) Tworzenie reklam"
                },
                "question": "Co jest najważniejsze w marketingu społecznościowym i biznesie?"
            },
            "question8": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Zapewnianie wolnej przestrzeni internetowej",
                    "option2": "b) Zachęcanie członków do przesyłania treści",
                    "option3": "c) Chat na żywo",
                    "option4": "d) Wszystkie wymienione"
                },
                "question": "Która cecha jest wspólna dla mediów społecznościowych?"
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Tych samych adresów e-mail można używać do rejestrowania się na różnych kontach w mediach społecznościowych."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Adres e-mail",
                    "option2": "b) Wyszukiwarka",
                    "option3": "c) Media społecznościowe",
                    "option4": "d) Spam"
                },
                "question": "_______ pomaga udostępniać specyfikacje produktów, reklamować i komunikować się z ludźmi bezpośrednio w Internecie."
            }
        }
    }
};

export default class M2Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz2;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                            <AwesomeAlert
                                show={showAlertTrue}
                                showProgress={false}
                                title={I18n.t('_correct')}
                                message={I18n.t('_correctFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="green"
                                onConfirmPressed={() => {
                                    this.hideAlert();
                                    this._next();
                                }}
                                titleStyle={{color: 'green', fontWeight: 'bold'}}
                            />
                            <AwesomeAlert
                                show={showAlertFalse}
                                showProgress={false}
                                title={I18n.t('_incorrect')}
                                message={I18n.t('_incorrectFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="#DD6B55"
                                onConfirmPressed={() => {
                                    this.hideAlert();
                                    this._next();
                                }}
                                titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
