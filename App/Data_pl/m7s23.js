import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S23 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Mikrofinansowanie i inny fundusz początkowy dla kobiet</Text>
                <Text style={styles.dmlH2}>Dla Turcji:</Text>
                <Text style={styles.dmlH3}>Kredi Garanti Fonu (The Credit Guarantee Fund- KGF)</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'KGF jest spółką nienastawioną na zysk i działającą jako poręczyciel dla MŚP i przedsiębiorstw innych niż MŚP, które nie mogą uzyskać pożyczki z powodu niewystarczającego zabezpieczenia. W ten sposób KGF wspiera MŚP i przedsiębiorstwa niebędące MŚP w dostępie do finansowania. Pożyczka wspiera kobiety-przedsiębiorców w rozwijaniu działalności gospodarczej lub wspieraniu ich z trudnej sytuacji.',
                                val2: 'http://www.kgf.com.tr/index.php/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Turkish Grameen Microfinance Program</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Turecki Program Mikrofinansowy Grameen (TGMP) to ekonomiczna fundacja non-profit. Zamiast tradycyjnych darowizn i "dobroczynności", TGMP oferuje usługi "mikrokredytów" pomagające zmniejszyć ubóstwo w Turcji. Celem systemu mikrokredytów jest pomoc kobietom o niskich dochodach w podejmowaniu zrównoważonych działań generujących dochód i przyczynianiu się do ich budżetów rodzinnych. W przeciwieństwie do formalnego (komercyjnego) sektora bankowego, kredyty mikrokredytowe są oferowane bez wymogu zabezpieczenia i bez żadnej dokumentacji innej niż turecki dowód osobisty danej osoby. Niektóre produkty mikrokredytu są wymienione poniżej.',
                                val2: 'http://www.tgmp.net/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Basic Loan</Text>
                <Text>Kredyt podstawowy jest pierwszym rodzajem pożyczki dla starych i nowych członków. Nowi członkowie
                    mogą otrzymać pożyczkę od 100 TL do 1000 TL, a spłaty pożyczek są dokonywane przez 46
                    tygodni.</Text>

                <Text style={styles.dmlH3}>Entrepreneurial Loan</Text>
                <Text>Rodzaj pożyczki dla przedsiębiorcy, który może uzyskać pożyczkę od 1 000 TL do 5 000 TL, a spłaty
                    pożyczek są dokonywane w ciągu 46 tygodni.</Text>

                <Text style={styles.dmlH3}>Digital Divide Loan</Text>
                <Text>Tego rodzaju pożyczka ma na celu zapewnienie rozwoju technologicznego dla członków
                    przedsiębiorstwa. Dzięki temu kredytowi członkowie mogą mieć smartfony wykorzystujące dzisiejszą
                    technologię. Spłaty pożyczek dokonywane są przez 46 tygodni.</Text>
            </View>
        )
    }
}
