import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S33 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe czynności, aby udostępnić informacje z serwisu Instagram
                    innym sieciom społecznościowym</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 3: Jeśli chcesz zmienić wygląd zdjęcia, możesz
                    zastosować do niego filtr lub edytować go ręcznie. Gdy skończysz, naciśnij next.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img63.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img64.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
