import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S16 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Dotacje dla małych firm prowadzonych przez kobiety</Text>
                <Text style={styles.dmlH2}>Dla Polski:</Text>
                <Text style={styles.dmlH3}>EU funds</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Wspólna strona informacyjna o funduszach UE w Polsce. Na tej stronie każdy przedsiębiorca lub przyszły przedsiębiorca, (ale także organizacje pozarządowe, organy publiczne itp.) może znaleźć interesujące informacje na temat dostępnych funduszy.',
                                val2: 'https://www.funduszeeuropejskie.gov.pl/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Polish Agency for Enterprise Development</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'W Polsce, Polska Agencja Rozwoju Przedsiębiorczości oferuje fundusz pożyczkowy wyłącznie dla kobiet, którego celem jest wspieranie aktywizacji zawodowej kobiet, poprawa ich sytuacji na rynku pracy poprzez zachęcanie kobiet do zakładania własnych firm. Fundusz pożyczkowy dla kobiet powinien przyczynić się do zmniejszenia problemu bezrobocia wśród tej grupy. Kobiety mogą ubiegać się o pożyczkę preferencyjną w wysokości od 5 do 10 tys. € (20 i 40 tys. zł).',
                                val2: 'http://en.parp.gov.pl/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
