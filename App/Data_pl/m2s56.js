import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S56 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Korzystanie z serwisów społecznościowych</Text>
                <Text style={styles.dmlH2}>Skonfiguruj blog</Text>
                <Text style={{marginTop: 10}}>Zanim zaczniesz blogowanie, musisz wybrać platformę, na której chcesz
                    opublikować swoją witrynę. Istnieje kilka darmowych, hostowanych opcji, takich jak bardzo popularny
                    WordPress i Blogger. Obie te platformy mają aplikacje, które pozwolą użytkownikom tworzyć, edytować
                    i publikować posty.</Text>
                <Text style={{marginTop: 10}}>Kluczową różnicą między Bloggerem i WordPressem jest to, że podczas gdy
                    Blogger jest nieco prostszy w konfiguracji i obsłudze, WordPress jest łatwiejszy do dostosowania i
                    ostatecznie przekształcenia w stronę hostowaną, gdy przekroczysz granice darmowych usług.
                    Niezależnie od wyboru, na główne platformy mobilne dostępne są oficjalne aplikacje. (Bozzo
                    2014)</Text>
                <Text style={{marginTop: 10}}>W tym samouczku będziemy używać Bloggera ze względu na prostotę ...</Text>
            </View>
        )
    }
}
