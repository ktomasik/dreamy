import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Przetwarzanie kart kredytowych w punkcie sprzedaży</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie ze wskazówkami poniżej do przetwarzania kart kredytowych
                    w punkcie sprzedaży:</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1. Najpierw
                    wybierz odpowiedni system punktu sprzedaży dla Twojej firmy.</Text>
                <Text>Istnieje wiele czynników przy wyborze systemu punktu sprzedaży (POS). Najpierw pomyśl, jak chcesz
                    akceptować płatności. Czy będziesz sprzedawał więcej fizycznie, online czy na oba sposoby? Czy
                    chcesz mieć możliwość dokonywania płatności poza biurem? Jaki jest twój budżet na zakup sprzętu POS,
                    oprogramowania i urządzeń peryferyjnych, takich jak skanery kodów kreskowych i drukarki paragonów?
                    Jakie są twoje plany rozszerzenia działalności w przyszłości? Odpowiedzi na wszystkie te pytania
                    pomogą Ci wybrać system odpowiedni dla Twojej firmy.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2. Wybierz zgodny
                    procesor płatności lub bramkę płatności</Text>
                <Text>Musisz także zdecydować, z którym procesorem płatności lub bramką płatności pracować. Bramka
                    płatności jest zewnętrznym hostowanym rozwiązaniem płatności, które umożliwia łączenie się z
                    wybranym procesorem płatności, o ile bramka działa z tym procesorem.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3. Upewnij się,
                    że Twoje rozwiązanie zawiera niezbędne funkcje bezpieczeństwa, aby zapewnić bezpieczeństwo
                    transakcji i chronić klientów oraz firmę.</Text>
                <Text>Naruszenie bezpieczeństwa danych jest nie tylko złe dla biznesu, ale może być katastrofalne.
                    Dlatego w twoim najlepszym interesie jest zrobić wszystko, aby zapobiec naruszeniu bezpieczenstwa
                    danych. Bezpieczeństwo płatności wymaga wielostronnego podejścia obejmującego zarówno rozwiązania
                    technologiczne, jak i najlepsze praktyki. Najlepiej jest to osiągnąć we współpracy z dostawcą usług
                    płatniczych.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>4. Wyznacz
                    priorytetowe funkcje, które chcesz dodać do swojego rozwiązania płatniczego.</Text>
                <Text>Twoja firma jest wyjątkowa i jako taka będzie miała unikalne potrzeby, jeśli chodzi o
                    przetwarzanie płatności.
                    {"\n\n"}Aby w pełni wykorzystać swoje rozwiązanie POS, warto zastanowić się nad funkcjami, które
                    chcesz,
                    oraz ocenić dostępność i koszty oferowane przez różnych dostawców.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>5. Nie zapomnij
                    zapytać o pomoc przy instalacji i szkoleniu personelu.</Text>
                <Text>Najlepsze rozwiązanie płatnicze na świecie ma niską wartość dla Twojej firmy, jeśli jest zbyt
                    trudne do zainstalowania lub nauczenia się korzystania z niego. Poproś o prezentację produktu, abyś
                    mógł wypróbować rozwiązanie w czasie rzeczywistym. Pamiętaj, aby zapytać o konfigurację i integrację
                    z istniejącymi systemami. Dowiedz się o protokoły i czy konieczne aktualizacje są automatycznie
                    wykonywane lub jeśli musisz je wdrożyć samodzielnie. No i oczywiście zapytaj o podstawy
                    przetwarzania. Dowiedz się, jak uruchomić różne typy płatności.</Text>
            </View>
        )
    }
}
