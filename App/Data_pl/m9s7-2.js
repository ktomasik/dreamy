import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img14.png')
    }
}];

export default class M9S7_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Tworzenie konta biznesowego Twitter:</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 2:</Text> Kliknij
                    przycisk "Edytuj profil", a następnie uzupełnij wszystkie niezbędne obszary na stronie Twitter w
                    następujący sposób:</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img14.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <FlatList
                    data={[
                        {key: '1. Nazwa użytkownika: (będzie lepiej, jeśli będzie odzwierciedlać nazwę Twojej firmy)'},
                        {key: '2. Zdjęcie profilowe (zalecane wymiary to 400 x 400 pikseli)'},
                        {key: '3. Opis Twojej firmy (masz 160 znaków, aby inni wiedzieli, co powoduje, że Twoje konto jest wyjątkowe)'},
                        {key: '4. Obraz nagłówka, umożliwia wysłanie zdjęć z wydarzenia, zdjęć produktów, informacji promocyjnych / obrazów lub ogłaszanie nowych sprzedaży i promocji, które obecnie prowadzisz dla swojej firmy (zalecane wymiary to 1500 x 500 pikseli)'},
                        {key: '5. Przypięty tweet: jeśli zawsze umieszczasz najważniejsze lub najnowsze wiadomości na górze, ułatwisz odwiedzającym znalezienie nowych informacji bez konieczności szukania po całej stronie.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Koniecznie uzupełnij jak najwięcej informacji, a także upewnij się, że
                    używasz wysokiej jakości zdjęcia lub logo dla wszystkich mediów społecznościowych!</Text>
            </View>
        )
    }
}
