import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S19 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Ulgi podatkowe dla kobiet rozpoczynających działalność</Text>
                <Text style={styles.dmlH2}>Dla Grecji:</Text>
                <Text>W Grecji wprowadzono zachęty w celu poprawy innowacyjności, ponieważ zgodnie z nowym prawem
                    państwowym firmy wytwarzające produkty lub świadczące usługi rejestracji patentów na całym świecie
                    uznane w imieniu przedsiębiorstwa są zwolnione z podatku dochodowego przez trzy lata.</Text>
                <Text>W szczególności można przewidzieć, że zyski przedsiębiorstwa wynikające ze sprzedaży produktów,
                    których patent jest międzynarodowo uznawany w imieniu przedsiębiorstwa, są zwolnione z podatku
                    dochodowego przez trzy kolejne lata, począwszy od czasu, w którym były pierwszymi dochodami ze
                    sprzedaży produktów przy użyciu patentu.</Text>
                <Text>Zwolnienie przyznawane jest również w przypadku, gdy produkty są wytwarzane w przez strony
                    trzecie. Zwolnienie przyznawane jest również w odniesieniu do zysków wynikających ze świadczenia
                    usług w przypadku patentu, który jest również uznawany na całym świecie.</Text>
            </View>
        )
    }
}
