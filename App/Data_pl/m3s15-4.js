import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe kroki, aby sprzedać po najlepszej cenie w środowisku on-line</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 4: Wspomnij o rabatach przy zakupie 2 lub więcej produktów w tym samym czasie, podej wartość redukcji.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/24911.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
