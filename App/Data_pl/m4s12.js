import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Zastosowanie lokalnych licencji i pozwoleń</Text>
                <Text style={{marginTop: 10}}>Postępuj zgodnie ze wskazówkami, aby ubiegać się o licencje i zezwolenia:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Uzyskaj zgodę od właścicieli pomieszczeń{"\n"}(jeśli to konieczne)</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Zgłoś się do urzędu skarbowego{"\n"}(w miejscu zamieszkania)</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Podaj fakturę i rachunek za dostawę</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Uzyskaj dokumenty od izb lub stowarzyszeń</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Złóż wniosek do gminy o pozwolenie na pracę</Text>
                    </View>
                </View>
            </View>
        )
    }
}
