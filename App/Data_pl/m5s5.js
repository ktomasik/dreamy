import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>System płatności e-commerce</Text>
                <Text style={{marginTop: 15}}>System płatności e-commerce to sposób dokonywania transakcji lub płacenia
                    za towary i usługi za pośrednictwem medium elektronicznego, bez korzystania z czeków lub
                    gotówki.</Text>
                <Text style={styles.dmlH3}>Co to jest konfiguracja płatności automatycznych?</Text>
                <Text>Automatyczna zapłata za rachunki to przelew pieniężny zaplanowany na określoną datę, aby zapłacić
                    powtarzający się rachunek. Automatyczne płatności za rachunki to rutynowe płatności dokonywane z
                    konta bankowego, maklerskiego lub funduszu inwestycyjnego na rzecz dostawców. Zazwyczaj są one
                    konfigurowane w firmie otrzymującej płatność, ale możliwe jest również planowanie płatności
                    automatycznych za pośrednictwem usługi płatności online rachunku.</Text>
                <Text style={styles.dmlH3}>Akceptowanie płatności online za pomocą karty kredytowej, debetowej lub
                    PayPal</Text>
                <Text>Umożliwienie klientom płacenia kartą kredytową w witrynie jest najbardziej podstawowym sposobem
                    akceptowania płatności online. Aby zaoferować tę funkcję swoim klientom, musisz zdecydować, czy
                    chcesz mieć własne dedykowane konto sprzedawcy, czy użyć pośredniczącego rachunku.
                    {"\n\n"}Małe firmy lub organizacje, które chcą akceptować płatności kartą kredytową online za
                    usługi, subskrypcje lub produkty sprzedawane na stronie internetowej, mogą dodawać przyciski PayPal
                    do dowolnej witryny.
                </Text>
                <Text style={styles.dmlH3}>Wymagany bezpieczny system płatności online</Text>
                <Text>Bezpieczeństwo w Internecie jest czymś, co dotyczy nas wszystkich jako konsumentów. Dla
                    właściciela firmy jest jeszcze ważniejsze. Przyjmując płatności online, bierzesz odpowiedzialność za
                    ochronę danych swoich klientów, a bezpieczne zarządzanie nimi może być kosztownym obciążeniem.
                    Możesz jednak ułatwić sobie korzystanie z rozwiązania płatniczego zgodnego z PCI. Zgodność PCI
                    dotyczy zasad i przepisów regulujących ochronę danych.</Text>
                <Text style={styles.dmlH3}>Internetowe rozwiązania płatnicze z kartą, fakturą i bankiem</Text>
                <Text>Płatności online są dokonywane natychmiast, więc jest to wygodne i oszczędza dużo czasu.
                    {"\n"}Tak zwane "portfele online" pozwalają swoim klientom:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Zapłacić online, ujawniając dane swojej karty kredytowej'},
                        {key: '2. Zapłacić fakturę'},
                        {key: '3. Zapłacić na konto bankowe'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH3}>Co to jest wirtualny punkt sprzedaży (POS)?</Text>
                <Text>Wirtualny terminal POS. Jest to bramka płatności, która pozwala sprzedawcom internetowym i
                    sprzedawcom usług ręcznie autoryzować transakcje kartowe zainicjowane przez kupującego. Proces ten
                    znacznie rozszerza źródła płatności i skraca czas procesu płatności, dodając dodatkowe
                    zabezpieczenia. Integracja wirtualnego terminala POS jest możliwa dzięki licznym platformom
                    e-commerce.</Text>
            </View>
        )
    }
}
