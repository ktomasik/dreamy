import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S31 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe czynności, aby udostępnić informacje z serwisu Instagram
                    innym sieciom społecznościowym</Text>
                <Text style={{marginTop: 10}}>W następnym ćwiczeniu pokażemy:</Text>
                <FlatList
                    data={[
                        {key: 'jak publikować treści'},
                        {key: 'jak udostępniać zawartość innym sieciom społecznościowym i'},
                        {key: 'jak dodać treść do odpowiedniego tematu.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 3, marginLeft: 15}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Krok 1: Aby opublikować nową zawartość, naciśnij znak kamery u dołu
                    ekranu.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img59.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
