import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wyszukiwanie z Google i Tworzenie konta e-mail ze smartfona</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 2: otwórz "Google Chrome" z przeglądarki</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex:1, width: 320, height: 400}}
                        source={require('../Images/m1/img1.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
