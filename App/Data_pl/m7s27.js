import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Słowenii:</Text>
                <Text style={styles.dmlH3}>Krok 1:</Text>
                <Text>W służbie zatrudnienia w Słowenii zarejestruj się jako bezrobotny. Natychmiast poinformuj swojego
                    osobistego doradcę na pierwszym spotkaniu, że chcesz otrzymać dotację. Osobisty doradca przygotuje
                    plan rekrutacji i deklarację włączenia do programu pomocy na rzecz samozatrudnienia.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
