import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";
// Styles

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m4/img1.png')
    }
}];

export default class M4S2 extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Jaka jest różnica między nazwą firmy, nazwą handlową i legalną nazwą?</Text>
                <Text>Nazwa firmy to oficjalna nazwa osoby lub podmiotu, który jest właścicielem firmy.
                    {"\n"}Jest to prawna nazwa firmy. Nazwa firmy jest używana w formularzach rządowych i aplikacjach
                    (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Na przykład:</Text> Imię I nazwisko przedsiębiorcy to
                    John Smith. Jest on właścicielem firmy ubezpieczeniowej. Prawną nazwą firmy może być John Smith
                    Insurance (Anonymous 2018b).
                    {"\n\n"}Właściciele firm mogą używać nazwy handlowej w celach reklamowych i sprzedaży. Nazwa
                    handlowa to nazwa, którą widzi publiczność, jak na znakach i w Internecie (Cameron 2017).
                    {"\n"}Nazwa firmy i nazwa handlowa mogą być różne. Nazwa handlowa nie musi obejmować LLC, Corp lub
                    innych zakończeń prawnych stosowanych dla podmiotu podatkowego (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Na przykład:</Text> McDonald's jest nazwą handlową.
                    Legalna nazwa firmy to McDonald's Corporation (Anonymous 2018c).
                </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m4/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 10}}>
                    Prawna nazwa firmy to imię i nazwisko osoby lub podmiotu, który jest właścicielem firmy. Jeśli firma
                    jest spółką osobową, nazwa prawna jest nazwą podaną w umowie partnerskiej.
                    {"\n\n"}W przypadku spółek z ograniczoną odpowiedzialnością (LLC) i korporacji nazwa prawna tej
                    firmy jest zarejestrowana przez rząd stanowy. Nazwy te często mają "prawne zakończenie", takie jak
                    LLC, Inc. lub LLP (Fishman 2015).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10}}>Kiedy należy użyć
                    nazwy prawnej lub nazwy handlowej?</Text>
                <Text>Przy komunikowaniu się z rządem lub innymi firmami należy użyć nazwy prawnej.
                    {"\n"}Na przykład, podczas składania deklaracji podatkowych, zakupu nieruchomości lub sporządzania
                    czeków należy użyć nazwy prawnej firmy.
                    {"\n"}Firma może używać nazwy handlowej w celach reklamowych i handlowych. Często jest to nazwa,
                    którą ogół społeczeństwa widzi na znakach, w Internecie i reklamach (Anonimous 2018d).
                    {"\n\n"}Ogólnie:
                    {"\n"}Nazwa prawna dotyczy procedur rządowych,
                    {"\n"}Nazwa handlowa służy do public relations.
                </Text>
            </View>
        )
    }
}
