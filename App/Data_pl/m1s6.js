import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wyszukiwanie z Google i Tworzenie konta e-mail ze smartfona</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 4: następnie wpisz: "Chcę utworzyć konto e-mail" w
                    twoim własnym języku</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img4.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
