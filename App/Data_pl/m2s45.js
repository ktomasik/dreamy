import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S45 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Twitterze</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 14: Możesz zezwolić tej aplikacji na dostęp do danych
                    GPS (Twojej lokalizacji), klikając OK i ZEZWALAJ.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img90.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img91.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
