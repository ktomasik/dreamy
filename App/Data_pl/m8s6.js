import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Przesyłanie dokumentów - zgłoszenie celne i formularze</Text>
                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Co to jest
                    deklaracja celna?</Text>
                <Text>Zgłoszenie celne jest urzędowym dokumentem, który zawiera wykaz i podaje dane dotyczące towarów,
                    które są przywożone lub wywożone.Pod względem prawnym zgłoszenie celne jest aktem, w którym dana
                    osoba wskazuje na chęć objęcia towarów określoną procedurą celną (Komisja Europejska 2018).</Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Kto powinien
                    złożyć zgłoszenie celne?</Text>
                <Text>Zasadniczo jest to właściciel towarów lub osoba działająca w jego imieniu (przedstawiciel). Osoba
                    sprawująca kontrolę nad towarami może również go wykonać. Osoby te mogą być osobami fizycznymi lub
                    firmami, a także w niektórych przypadkach stowarzyszeniami osób (Komisja Europejska 2018).</Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Gdzie należy
                    złożyć zgłoszenie celne?</Text>
                <Text>Zgłoszenie należy złożyć w urzędzie celnym, w którym towary zostały lub zostaną wkrótce
                    przedstawione (Komisja Europejska 2018).</Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Gdzie należy
                    złożyć zgłoszenie celne?</Text>
                <Text>Aby spełnić wymogi prawne i objąć towary procedurą celną, należy złożyć zgłoszenie celne (Komisja
                    Europejska 2018). Tak powinno się stać w dwóch przypadkach:</Text>
                <FlatList
                    data={[
                        {key: 'w momencie przywozu, gdy towary są wprowadzane na obszar celny, muszą zostać przypisane do przeznaczenia celnego'},
                        {key: 'towary przeznaczone na eksport - muszą zostać objęte procedurą wywozu'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
