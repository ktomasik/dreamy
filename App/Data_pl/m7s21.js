import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S21 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Ulgi podatkowe dla kobiet rozpoczynających działalność</Text>
                <Text style={styles.dmlH2}>Dla Polski:</Text>
                <Text>W Polsce nie ma oddzielnych ulg podatkowych dla kobiet zakładających firmę.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_24_3319620.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
