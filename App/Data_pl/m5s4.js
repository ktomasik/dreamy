import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m5/img1.png')
    }
}];

export default class M5S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Rzeczy, które powinieneś wiedzieć zanim otrzymasz pierwszą kartę kredytową
                    (np. Co to jest wyciąg z karty kredytowej, jak obliczane są odsetki kart kredytowych, jak określa
                    się minimalne płatności)</Text>
                <Text style={{marginTop: 15}}>Wyciąg bankowy to okresowe zestawienie zawierające wszystkie zakupy,
                    płatności i inne obciążenia i kredyty przekazane na rachunek karty kredytowej w cyklu
                    rozliczeniowym. Wydawca karty kredytowej przesyła rozliczenie raz w miesiącu.
                    {"\n\n"}Co znajduje sie na wyciągu bankowym?
                    {"\n"}Na wyciągu znajdziesz wszystko, co musisz wiedzieć o transakcjach. Obejmuje on:
                </Text>
                <FlatList
                    data={[
                        {key: 'Saldo z poprzedniego cyklu rozliczeniowego'},
                        {key: 'Minimalną należną płatność'},
                        {key: 'Termin płatności'},
                        {key: 'Opłatę, która zostanie naliczona, jeśli się spóźnisz'},
                        {key: 'Podsumowanie i szczegółowy wykaz płatności, kredytów, zakupów, transferów salda, zaliczek gotówkowych, opłat, odsetek i innych obciążeń Twojego konta'},
                        {key: 'Podział rodzajów sald na koncie oraz oprocentowanie i odsetki dla każdego z nich'},
                        {key: 'Twój limit kredytowy i dostępny kredyt'},
                        {key: 'Liczbę dni w Twoim okresie rozliczeniowym'},
                        {key: 'Łączną kwota odsetek i opłat zapłaconych rok do roku'},
                        {key: 'Dane kontaktowe wystawcy karty kredytowej'},
                        {key: 'Nagrody/punkty zarobione lub wykorzystane, jeśli dotyczy'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 10}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m5/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 10}}>Wyciąg z karty kredytowej będzie zawierał informację o minimalnej
                    płatności, określającą czas potrzebny na spłatę salda, jeśli dokonasz tylko minimalnej płatności i
                    całkowitej kwoty, którą zapłacisz. Obejmuje to również comiesięczną opłatę, jeśli chcesz spłacić
                    saldo za trzy lata. Informacje te są pomocne w określeniu najlepszego sposobu na spłacenie salda
                    karty kredytowej.
                    {"\n\n"}Oświadczenie rozliczeniowe Twojej karty kredytowej zawiera również ostrzeżenie o
                    opóźnieniach w płatnościach z informacjami o wpływie opóźnień w płatnościach na podwyższenie stawek
                    spłaty.
                    {"\n\n"}Minimalna kwota płatności kartą kredytową jest najmniejszą kwotą, którą możesz zapłacić w
                    stosunku do salda karty kredytowej, nie ponosząc za to żadnych strat z powodu opóźnionej opłaty i
                    możliwego wzrostu stopy procentowej.
                    {"\n\n"}Jeśli zwracasz uwagę na swoje rozliczenie miesięcznie, prawdopodobnie zauważyłeś, że
                    minimalna płatność może się zmieniać z miesiąca na miesiąc. Niektórzy wystawcy kart kredytowych
                    obliczają minimalną płatność jako procent salda, zwykle od 2% do 5%, pod koniec cyklu
                    rozliczeniowego.
                    {"\n\n"}Twoja minimalna płatność może być również obliczona na podstawie procentu salda na koniec
                    cyklu rozliczeniowego i dodania miesięcznego obciążenia finansowego.
                    {"\n\n"}Możesz dowiedzieć się, jakiej metody używa twój wystawca karty kredytowej, czytając umowę o
                    karcie kredytowej. Poszukaj sekcji zatytułowanej "Jak obliczana jest minimalna płatność" lub
                    "Dokonywanie płatności".
                </Text>
            </View>
        )
    }
}
