import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ubezpieczenia i śledzenie</Text>
                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Co to jest
                    ubezpieczenie ładunku?</Text>
                <Text>Zgodnie z prawem wszyscy przewoźnicy muszą posiadać minimalną kwotę ubezpieczenia, zwaną
                    odpowiedzialnością przewoźnika. Jednak odpowiedzialność przewoźnika zapewnia bardzo ograniczony
                    zasięg, a różne wydarzenia losowe od klęsk żywiołowych po wypadki samochodowe, a nawet działania
                    wojenne, może spowodować uszkodzenie ładunku. Dlatego spedytorzy mogą zażądać ubezpieczenia ładunku,
                    aby chronić towary przed utratą, uszkodzeniem lub kradzieżą podczas transportu. Zasadniczo towary są
                    ubezpieczone podczas przechowywania i podczas transportu, dopóki nie dotrą do kupującego (Robinson
                    2016).
                    {"\n\n"}Istnieją różne rodzaje polis ubezpieczeniowych ładunków, niektóre z nich obejmują takie
                    nazwy, jak "wszystkie rodzaje ryzyka", "szeroka odpowiedzialność", "odpowiedzialność prawna" i
                    "przewóz samochodów ciężarowych". Jednak większość z nich jest niezbędna w przypadku przesyłek o
                    dużej przepustowości na morzach. Ubezpieczenie cargo może być stosowane zarówno w transporcie
                    międzynarodowym, jak i krajowym (Robinson 2016).
                </Text>
            </View>
        )
    }
}
