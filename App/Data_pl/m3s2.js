import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Dlaczego zdjęcia produktów są ważne?</Text>
                <Text>Wiek, w którym żyjemy, jest taki, że wizualność ma wielkie znaczenie. Kiedy ludzie myślą lub coś
                    robią, zawsze projektują kształt w swoim umyśle. Ponieważ ludzki umysł działa tak, aby przekształcić
                    coś abstrakcyjnego w coś konkretnego. Dlatego rzeczy wizualne są ważne.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 120}}
                    source={require('../Images/m3/img10.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img11.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 130}}
                    source={require('../Images/m3/img12.png')}
                    resizeMode="contain"/>

                <Text style={styles.dmlH2}>Jak sprzedawać swoje produkty na Instagramie?</Text>
                <Text>Sprzedaż produktów na swojej stronie internetowej nie jest już jedynym sposobem na osiągnięcie
                    zysku. Wraz z rosnącą popularnością handlu społecznościowego nigdy nie było więcej sposobów na
                    sprzedaż online i promowanie swojej marki. Z ponad 700 milionami użytkowników, Instagram szybko
                    staje się potęgą handlu społecznego. Około 80% Instagrammerów śledzi firmę na Instagramie, a 60%
                    twierdzi, że używa jej do odkrywania nowych produktów.</Text>
                <Image
                    style={{flex: 1, width: undefined}}
                    source={require('../Images/m3/img13.png')}
                    resizeMode="contain"/>
                <Text style={{marginTop: 10}}>Aby sprzedać swój produkt na Instagramie, musisz skorzystać z kilku wskazówek:</Text>
                <FlatList
                    data={[
                        {key: 'Upewnij się, że masz konto firmowe.'},
                        {key: 'Wysyłaj wysokiej jakości zdjęcia i filmy.'},
                        {key: 'Skoncentruj się na opowiadaniu historii, aby zwiększyć zaangażowanie.'},
                        {key: 'Stwórz atrakcyjne reklamy na Instagramie.'},
                        {key: 'Zaufaj ambasadorom marek i autorytetom, aby szerzyć świadomość.'},
                        {key: 'Rozmawiaj z klientami.'},
                        {key: 'Znajdź nowych klientów poprzez odkrywanie hashtagów.'},
                        {key: 'Często aktualizuj link do swojego profilu.'},
                        {key: 'Zaoferuj specjalne zniżki dla obserwujących na Instagramie.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 5, marginLeft: 12}}>{item.key}</Text>}
                />
                <Image
                    style={{flex: 1, width: 300, height: 480}}
                    source={require('../Images/m3/img14.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 400}}
                    source={require('../Images/m3/img15.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
