import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S26 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Mikrofinansowanie i inny fundusz początkowy dla kobiet</Text>
                <Text style={styles.dmlH2}>Dla Polski:</Text>
                <Text style={styles.dmlH3}>STARTUP ACADEMY</Text>
                <Text>szkolenia, mentoring, innowacyjne metody budowania startupów, programy akceleracyjne</Text>

                <Text style={styles.dmlH3}>TWÓJ STARTUP</Text>
                <Text>preinkubacja, doradztwo prawne i księgowe, konsultacje w zakresie IT i marketingu, szkolenia</Text>

                <Text style={styles.dmlH3}>Inkubator Technologiczny Podkarpckiego Parku Naukowo-Technologicznego</Text>
                <Text>pomieszczenia biurowe, usługi doradcze, wsparcie rozwojowe</Text>

                <Text style={styles.dmlH3}>Przedsiębiorcze kobiety 2.0</Text>
                <Text>Projekt jest skierowany do kobiet niepracujących, aby pomóc im w założeniu własnej firmy</Text>

                <Text style={styles.dmlH3}>AIP</Text>
                <Text>Doradztwo biznesowe, doradztwo i coaching, usługi księgowe, pomoc prawna, szkolenie dla początkujących</Text>
            </View>
        )
    }
}
