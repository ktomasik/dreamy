import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S37 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe czynności, aby udostępnić informacje z serwisu Instagram
                    innym sieciom społecznościowym</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 8: ... a także dołączona do wybranej sekcji #hashtag.
                    Możesz to sprawdzić, klikając link #hashtag.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img70.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img71.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img72.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
