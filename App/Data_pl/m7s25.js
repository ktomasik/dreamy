import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Mikrofinansowanie i inny fundusz początkowy dla kobiet</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text>Pożyczka na ogół poniżej 25 000 € przeznaczona dla osób, które chcą założyć lub przejąć firmę, ale
                    których zasoby są niewystarczające, aby kwalifikować się do pożyczki konwencjonalnej. Aby skorzystać
                    z mikrokredytu, pożyczkobiorcy musi towarzyszyć wyspecjalizowana i kompetentna sieć wsparcia, taka
                    jak: "France Active", "France Initiative", "Boutiques de Gestion" lub "Fondation of 2e Chance".
                    Sieci te pomogą mu w przygotowaniu projektu, zbadaniu jego wniosku o finansowanie i rozwoju jego
                    działalności. Głównym aktorem jest ADIE (Stowarzyszenie na rzecz rozwoju inicjatywy
                    gospodarczej).</Text>

                <Text style={styles.dmlH3}>ADIE</Text>
                <Text>Uwrażliwia, prowadzi i informuje kobiety podczas tworzenia biznesu, a także organizuje od 2015 r.
                    coroczną kampanię informacyjną dla kobiet.</Text>
                <Text>Promuje finansowanie carier wraz z mikrokredytem dla przedsiębiorstw, które nie mają dostępu do
                    kredytu bankowego.</Text>
                <Text>Wzmacnia wsparcie twórców biznesowych dzięki modułom szkoleń i świadomości dostosowanym do ich
                    specyfiki.</Text>
                <FlatList
                    data={[
                        {key: 'https://www.adie.org/nos-actions/pour-les-femmes'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}}
                                                  onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />

                <Text style={styles.dmlH3}>FRANCE ACTIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'France Active wspiera i finansuje firmy od prawie 30 lat i zmobilizowała 270 milionów euro do obsługi 7400 firm w zeszłym roku. To znacznie więcej niż sieć, France Active jest prawdziwym ruchem zaangażowanych przedsiębiorców, których ambicją jest budowa bardziej zintegrowanego społeczeństwa. Misją France Active jest przyspieszenie sukcesu przedsiębiorców poprzez zapewnienie im środków do rozpoczęcia działalności.',
                                val2: 'https://www.franceactive.org/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />


                <Text style={styles.dmlH3}>INICJATYWA FRANCJI</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Zaczynając pod nazwą France Initiative Network, a następnie France Initiative, sieć przeprojektowała system marki. Od 1 października 2012 roku to krajowe stowarzyszenie nosi nazwę Inicjatywa Francja. Lokalne platformy i koordynacja regionalna wprowadzają tę samą zmianę. To coś więcej niż odwrócenie słów. Marka ta podkreśla teraz termin wspólny dla wszystkich: Inicjatywa. Towarzyszy temu logo, które graficznie odzwierciedla siłę sieci krajowej i jej różnorodność, związaną z lokalnymi korzeniami. Wreszcie, posiada slogan, który nadaje zbiorowemu działaniu pełny sens: "Sieć, duch"',
                                val2: 'http://www.initiative-france.fr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>BGE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Od ponad 35 lat BGE wspiera tworzenie biznesu i działa, aby stał się on dostępny dla wszystkich. Towarzysząc przedsiębiorcom na każdym etapie tworzenia, od powstania do rozwoju biznesu, pomaga każdemu, kto ma szanse na sukces. Jako sieć stowarzyszeń typu non-profit, BGE składa się z 50 stowarzyszeń założonych w różnych miejscachw celu otwarcia perspektyw, zabezpieczenia ścieżki przedsiębiorców i stworzenia trwałych rozwiązań dla zatrudnienia i rozwoju lokalnego.',
                                val2: 'http://www.bge.asso.fr/nous-sommes/notre-engagement.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Fundacja Drugiej Szansy</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Celem Fundacji drugiej szansy jest wspieranie osób w wieku od 18 do 62 lat, które przeszły trudne wydarzenia życiowe i obecnie znajdują się w bardzo niepewnej sytuacji, ale mają prawdziwe pragnienie odbicia się. Fundacja drugiej szansy oferuje im wsparcie finansowe w  realizacji realistycznego i zrównoważonego projektu zawodowego: kwalifikowanie szkolenia, tworzenie lub przejmowanie firmy.',
                                val2: 'http://www.deuxiemechance.org/fr'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
