import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";


export default class M4S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uzyskanie numeru identyfikacji podatkowej</Text>
                <Text style={{marginTop: 10}}>Postępuj zgodnie ze wskazówkami, aby uzyskać numer identyfikacji podatkowej:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Zgłoś się do urzędu skarbowego{"\n"}(w miejscu zamieszkania)</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Zadeklaruj swój adres domowy</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Wskaż sposób opodatkowania</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Dostarcz fakturę{"\n"}(nie musisz tego robić samodzielnie)</Text>
                    </View>
                </View>
            </View>
        )
    }
}
