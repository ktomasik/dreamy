import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S17 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Ulgi podatkowe dla kobiet rozpoczynających działalność</Text>
                <Text style={styles.dmlH2}>Dla Słowenii:</Text>
                <Text>Częściowe zwolnienie z opłacania składek do dwóch lat po pierwszej działalności. Osoby
                    samozatrudnione przez 2 lata od otworzenia firmy płacą jedynie ubezpieczenie społeczne (tylko te
                    osoby, które są objęte ubezpieczeniem emerytalnym i inwalidzkim na podstawie
                    samozatrudnienia).</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_41_3319603.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
