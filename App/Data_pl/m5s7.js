import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Uruchomienie bankowości mobilnej i internetowej</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie ze wskazówkami poniżej, aby uruchomić Mobile & Online Banking:</Text>
                <Text style={{fontWeight: 'bold'}}>Co będziesz potrzebował?</Text>
                <FlatList
                    data={[
                        {key: 'Ważną kartę debetowa lub kredytową'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop:10}}>Co robisz</Text>
                <Text>Na urządzeniu mobilnym:
                    {"\n"}Pobierz i otwórz aplikację Mobile Banking na urządzeniu z Androidem lub Apple.
                    {"\n\n"}Wykonaj poniższe kroki:
                    {"\n"}Z komputera:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Krok 1',
                                val2: 'Odwiedź stronę główną banku'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 2',
                                val2: 'Wybierz "Zarejestruj się"'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 3',
                                val2: 'Wprowadź numer karty i datę ważności, a następnie wybierz "Kontynuuj"'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 4',
                                val2: 'Powiedz, jak chcesz otrzymać kod weryfikacyjny'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />

                <Text style={{marginTop:15}}>Niektóre banki zapewniają również następujące usługi:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Krok 1',
                                val2: 'Po przeprowadzeniu transakcji poufnych kod weryfikacyjny zabezpiecza Cię dodatkową warstwą zabezpieczeń. Możesz otrzymać 6-cyfrowy kod weryfikacyjny za pomocą wiadomości SMS, e-maila lub połączenia głosowego. Wpisz kod, aby dokończyć transakcję.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 2',
                                val2: 'Niektóre banki mogą wysyłać jednorazowe kody weryfikacyjne do osobistych lub bezpłatnych usług e-mail.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 3',
                                val2: 'Wprowadź kod weryfikacyjny w polu Weryfikacja tożsamości i wybierz "Kontynuuj".'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 4',
                                val2: 'Wybierz hasło'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 5',
                                val2: 'Powtórz hasło'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 6',
                                val2: 'Jesteś prawie gotowy! Przeczytaj umowę o dostępie elektronicznym i zaznacz to pole, aby potwierdzić, że ją przeczytałeś.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 7',
                                val2: 'Zaloguj się i rozpocznij bankowość'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 8',
                                val2: 'Po zarejestrowaniu możesz dokonać przelewu bankowego z dowolnego komputera, smartfona lub tabletu.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 9',
                                val2: 'Jeśli Twoje urządzenie obsługuje funkcję logowania odcisków palców, istnieją aplikacje banku, w których możesz użyć czytnika odcisków palców, aby zweryfikować swoją tożsamość i uzyskać dostęp do swoich rachunków jednym dotknięciem.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
