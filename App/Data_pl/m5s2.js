import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Co to jest konto bankowe?</Text>
                <Text style={{marginTop: 10}}>Konto bankowe jest bezpiecznym i przydatnym miejscem do zdeponowania
                    pieniędzy. Dostęp do pieniędzy możliwy jest z każdego bankomatu. Posiadanie konta ułatwia
                    oszczędzanie inwestowanie na przyszłość.
                    {"\n\n"}Rodzaje rachunków bankowych
                    {"\n"}Większość banków oferuje następujące rodzaje rachunków:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Konta oszczędnościowe'},
                        {key: '2. Rachunki czekowe'},
                        {key: '3. Rachunki rynku pieniężnego'},
                        {key: '4. Certyfikaty depozytowe (CD)'},
                        {key: '5. Konta emerytalne'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>Co to jest rachunek czekowy? Co to jest bankowość mobilna i
                    internetowa?</Text>
                <Text style={{marginTop: 10}}>Rachunek czekowy zapewnia łatwy dostęp do pieniędzy na codzienne potrzeby
                    transakcyjne i pomaga utrzymać bezpieczeństwo gotówki. Klienci mogą korzystać z karty debetowej lub
                    czeków, aby dokonywać zakupów lub płacić rachunki. Konta mogą mieć różne opcje lub pakiety, aby
                    uniknąć niektórych miesięcznych opłat za usługi. Aby dokonać najbardziej ekonomicznego wyboru,
                    porównaj zalety różnych pakietów z usługami, których faktycznie potrzebujesz.
                    {"\n\n"}Bankowość mobilna pozwala wykonywać wiele takich samych czynności, jak bankowość internetowa
                    za pomocą smartfona lub tabletu zamiast komputera stacjonarnego. Elastyczność bankowości mobilnej
                    obejmuje:
                </Text>
                <FlatList
                    data={[
                        {key: 'Logowanie do mobilnej witryny banku'},
                        {key: 'Korzystanie z aplikacji bankowości mobilnej'},
                        {key: 'Bankowość SMS'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Bankowość internetowa oznacza dowolną transakcję bankową, którą można
                    przeprowadzić przez Internet, zazwyczaj za pośrednictwem strony internetowej banku na profile
                    prywatnym oraz za pomocą komputera stacjonarnego lub laptopa. Bankowość internetowa jest ogólnie
                    definiowana jako mająca następujące cechy:</Text>
                <FlatList
                    data={[
                        {key: 'Transakcje finansowe za pośrednictwem bezpiecznej witryny internetowej banku.'},
                        {key: 'Lokalizacje oddziałów fizycznych lub tylko online.'},
                        {key: 'Użytkownik musi utworzyć identyfikator logowania i hasło.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
