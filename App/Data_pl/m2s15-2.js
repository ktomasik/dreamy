import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S15_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Facebooku</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 15: Teraz twoje podstawowe ustawienia konta na
                    Facebooku są kompletne. Powinieneś zobaczyć swoją stronę profilu.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img26.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
