import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Dotacje dla małych firm prowadzonych przez kobiety</Text>
                <Text style={styles.dmlH2}>Dla Grecji:</Text>
                <Text style={styles.dmlH3}>Seed Capital</Text>
                <Text>Kapitał zalążkowy to niewielkie fundusze na założenie przedsiębiorstwa zwykle przydzielane
                    konkretnym kategoriom ludności, takim jak ludzie młodzi lub bezrobotni. Głównymi cechami tych
                    działań jest to, że kapitał jest dość mały (od 15 do 50 tysięcy euro), jest opłacony z góry i ma
                    pokryć koszty operacyjne pierwszego roku funkcjonowania firmy, aby dać jej czas na rozwój
                    działalności. Ogólnie istnieją działania zarówno publiczne (program OED), jak i w sektorze prywatnym
                    (TheOpenFund), dla różnych sektorów gospodarki (produkty tradycyjne, technologia informacyjna itp.).
                    Proces ubiegania się jest dość prosty, a wnioski składać może dowolny zainteresowany. Działania te
                    nie są jednak otwarte przez cały rok, a dodatkowo istnieją pewne warunki wstępne, takie jak
                    seminaria i karty bezrobocia dla programów publicznych (OAED).</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: '',
                                val2: 'http://www.digitalplan.gov.gr/portal/resource/Prosklhsh-Ypobolhs-Protasewn-sta-Ergaleia-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Sporas-Seed-ICT-Fund-kai-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Ekkinhshs-Early-Stage-ICT-Fund-gia-epiheirhseis-ston-klado-twn-Tehnologiwn-Plhroforikhs-kai-Epikoinwniwn-ICT-ths-Prwtoboylias-JEREMIE'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Partnership Agreement (PA) 2014-2020 (ESPA)</Text>
                <Text>ESPA to grecki program, który przekazuje środki z unijnego programu na rzecz eliminacji
                    nierówności między regionami UE. W ramach ESPA rząd dystrybuuje fundusze przeznaczone na handel,
                    przetwórstwo lub produkcję pierwotną. Wnioski są składane w określonych terminach, ogłaszane przez
                    odpowiednie ministerstwa i sekretariat generalny ESPA. Oceny wniosków dokonują niezależni
                    oceniający, wyniki są publikowane, a następnie daje się okres od jednego roku do trzech lat na
                    realizację każdego działania. Finansowanie może pokrywać procent inwestycji lub obniżać podatek
                    (zazwyczaj 40-60%). Dokumenty wydatków są wymagane i są sprawdzane podczas kontroli na
                    miejscu.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Ogromną zaletą ESPA jest jej dostępność, ale ponieważ finansowanie pokrywa się z kosztami, nie jest szczególnie przydatne w przypadku nowych firm. Jednak jest to szczególnie atrakcyjna opcja, którą można łączyć z innymi formami finansowania (pożyczki bankowe, kapitał podwyższonego ryzyka itp.).',
                                val2: 'https://www.espa.gr/en/pages/staticPartnershipAgreement.aspx'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text>Między innymi z programu ESPA 2014-2020 finansowane są następujące inicjatywy:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Start-upy i nowa przedsiębiorczość',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=496&t=Neofuis-Epixeirimatikotita'
                            }
                        },
                        {
                            key: {
                                val1: 'Modernizacja mikroprzedsiębiorstw i małych firm w celu rozwijania ich umiejętności na nowych rynkach',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=497&t=Anabathmisi-polu-mikrwn-&-mikrwn-epixeirisewn-gia-tin-anaptuksi-twn-ikanotitwn-tous-stis-nees-agores'
                            }
                        },
                        {
                            key: {
                                val1: 'Prowadzenie działalności za granicą',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=539&t=Epixeiroume-Eksw'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Grecka społeczność za granicą</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Społeczność grecka za granicą również jest aktywna i stworzyła nowe działania, takie jak Nagroda Envolve Grecja, która dotyczy nieoprocentowanej pożyczki w wysokości do 500 tysięcy euro, której spłata następuje w ciągu pięciu lat.',
                                val2: 'https://envolveglobal.org/el/envolve-awards/envolve-greece'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
