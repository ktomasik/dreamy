import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Co to jest pożyczka?</Text>
                <Text>Pożyczka to pożyczanie pieniądzy od banku lub innego pożyczkodawcy. Pożyczki są podzielone na dwie
                    części, główną i odsetki.</Text>
                <Text>Kwota główna jest kwotą, którą pożyczasz i jest główną częścią salda tego konta.</Text>
                <Text>Odsetki to opłata za czas pożyczki i jest obliczana na podstawie kwoty głównej.</Text>
                <Text style={styles.dmlH2}>Czym jest saldo pożyczki?</Text>
                <Text>Saldo pożyczki to kwota pozostała do spłacenia pożyczki. Każda pożyczka ma saldo kredytu do
                    momentu całkowitego spłacenia pożyczki. Zmienia się codziennie (odsetki dodawane są
                    codziennie).</Text>
                <Text>Harmonogram amortyzacji pożyczki; Kwota główna i odsetki są rozdzielone, dzięki czemu można
                    zobaczyć, która część miesięcznej płatności przeznaczona jest na spłatę kwoty głównej, a która służy
                    do spłaty odsetek.</Text>
                <Text style={styles.dmlH2}>Co to jest grant?</Text>
                <Text>Dotacje są bezzwrotnymi funduszami lub produktami wydatkowanymi lub obdarowanymi przez jedną ze
                    stron, często rządową, korporacyjną, fundację lub fundusz zaufania. Dotacja rządowa to finansowa
                    nagroda przyznana przez federalny, stanowy lub lokalny rząd uprawnionemu beneficjentowi.</Text>
                <Text style={styles.dmlH2}>Co to jest zachęta podatkowa?</Text>
                <Text>Zachęta podatkowa to rządowy środek, który ma zachęcać osoby prywatne i firmy do wydawania
                    pieniędzy lub do oszczędzania poprzez zmniejszenie kwoty podatku, który muszą zapłacić.</Text>
            </View>
        )
    }
}
