import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Informacje o umowie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 2:</Text> Po
                    wprowadzeniu informacji, kryteria te zostaną ocenione, a Ty znajdziesz najbardziej odpowiednie
                    oferty. Można dokonać porównania ofert, wybierając te, które chcesz. Powinieneś zapoznać się z tym
                    porównaniem online pod względem limitu, gwarancji i ceny.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_32_3319612.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
