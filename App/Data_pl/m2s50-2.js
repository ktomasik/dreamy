import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S50_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Korzystanie z serwisów społecznościowych</Text>
                <Text style={styles.dmlH2}>Stwórz stronę na Facebooku</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 5: Wpisz odpowiednią nazwę strony i kliknij DALEJ.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img102.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
