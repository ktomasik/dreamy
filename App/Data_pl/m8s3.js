import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Opcje wysyłki dla klienta</Text>
                <Text style={{marginTop: 10}}>Wysyłka i dostawa to główny czynnik sprzedaży dla biznesu detalicznego,
                    ponieważ oczekuje się, że produkty szybko i łatwo dotrą do klientów. Niektóre opcje wysyłki dla
                    klienta są wskazane poniżej (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Dostawa tego samego dnia</Text>
                <Text>Dostawa tego samego dnia to usługa gdzie zamówienie online i dostawa odbywa sie w tym samym dniu,
                    co jest rodzajem usługi ekspresowej. (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Odbiór w sklepie</Text>
                <Text>Odbiór w sklepie umożliwia klientom dokonywanie zakupów w Internecie, dokonywanie transakcji i
                    odbiór w wyznaczonym terminie w lokalnym sklepie lub miejscu (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Wysyłka ze sklepu</Text>
                <Text>Ship-from-store to usługa, dzięki której detaliści zamieniają swoje lokalizacje w trybie offline w
                    centra realizacji, które obsługują zarówno kupujących w sklepie, jak i klientów online (Ufford
                    2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Zaplanowana dostawa</Text>
                <Text>Zaplanowane dostawy to usługa, za pomocą której firmy spedycyjne oferują klientom opcję planowania
                    dostaw w określonym oknie czasowym za niewielką opłatą lub bezpłatnie, jeśli są członkami programu
                    lojalnościowego. (Ufford 2018).</Text>

                <Text style={styles.dmlH3}>Oferowanie bezpłatnej wysyłki</Text>
                <Text>Oferowanie bezpłatnej wysyłki (zazwyczaj tylko na zamówienia krajowe) to sprawdzona metoda
                    przyciągnięcia uwagi klientów i zwiększenia konwersji. Ale w zależności od marży może znacznie
                    zmniejszyć zyski (Anonim 2018b).
                    {"\n\n"}Jeśli bezpłatna dostawa jest oferowana dla produktu w dowolnej ofercie i ilości, należy
                    wziąć pod uwagę fakt, ile faktycznie kosztuje wysłanie produktów, jak konkurenci obsługują wysyłkę i
                    z jaką marżą. Pomoże to dokonać właściwego wyboru, czy oferować bezpłatną wysyłkę, czy nie. Inną
                    opcją jest zdefiniowanie minimalnych sum zamówienia powyżej średniej wartości zamówienia lub
                    minimalnej liczby produktów na zamówienie (Anonimowy 2018b).
                    {"\n\n"}Niektóre zalety bezpłatnej wysyłki to zwiekszenie konkurencyjności lub ograniczenie opcji
                    wysyłki u konkurentów, zwiększenie stopnia konwersji, zwiększanie średniej wartości zamówienia i
                    zmniejszanie liczby porzuconych koszyków. Z drugiej strony należy rozważyć podniesienie cen w celu
                    pokrycia kosztów wysyłki w porównaniu z konkurentami na rynku (Anonim 2018b).
                </Text>

                <Text style={styles.dmlH3}>Płatnosci od klientów</Text>
                <Text>Jednym ze stresujących elementów na początku sprzedaży są płatnosci. Z wyjątkiem wydatków
                    związanych z produktem w okresie jego produkcji, po wyprodukowaniu mogą pojawić się inne koszty,
                    niezbędne by zwrócić uwagę. Koszty wysyłki są szacowane według tej opcji.
                    {"\n\n"}Aby zwrócić uwagę na obniżenie ceny produktu, koszty przewoźnika można określić osobno.
                    Podczas sprzedaży można podać cenę bez kosztów wysyłki, jeżeli koszty przelewu zostaną pokryte przez
                    klientów. W tym miejscu preferencje dla firmy przewozowej i warunków mogą być określone przez
                    klienta i, zgodnie z potrzebami klienta, wysyłka zostanie przetworzona. Z drugiej strony cenę
                    produktu i koszty wysyłki określone przez firmę przewozową można określić osobno.
                    {"\n\n"}Sumowanie kosztów wysyłki do ceny koncowej może zapewnić konkurencyjne ceny produktu, który
                    przyciąga uwagę klientów.
                </Text>
            </View>
        )
    }
}
