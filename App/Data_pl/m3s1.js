import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym jest e-biznes i e-commerce?</Text>
                <Text>Biznes elektroniczny (e-biznes) odnosi się do korzystania z Internetu, intranetu, ekstranetu lub
                    ich kombinacji, do prowadzenia działalności.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img1.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img2.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>Jak i gdzie sprzedawać ręcznie robione przedmioty online?</Text>
                <Text>Internet to wyjątkowe miejsce marketingowe promujące i sprzedające ręcznie robione produkty
                    klientom na całym świecie.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img3.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>Najlepsze strony internetowe do sprzedaży rzemiosła</Text>
                <Text style={styles.dmlH3}>Etsy</Text>
                <Text>Etsy (<Text style={{color: 'blue'}}
                                  onPress={() => Linking.openURL('https://www.etsy.com/')}>etsy.com</Text>) to
                    dynamiczna społeczność 30 milionów kreatywnych firm zarejestrowanych na stronie. Na Etsy sprzedawana
                    jest szeroka gama produktów, w tym artykuły artystyczne, ręcznie robione produkty i elementy.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img4.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Amazon</Text>
                <Text>Umożliwia zakupy online z największych na świecie zbiorów książek, czasopism, muzyki, płyt DVD,
                    wideo, elektroniki, komputerów, oprogramowania, odzieży i akcesoriów, obuwia i jest dobrą platformą
                    do komercjalizacji produktów. (<Text style={{color: 'blue'}}
                                                         onPress={() => Linking.openURL('https://www.amazon.com/')}>amazon.com</Text>)</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img5.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Bonanza</Text>
                <Text>Bonanza (<Text style={{color: 'blue'}}
                                     onPress={() => Linking.openURL('https://www.bonanza.com/')}>bonanza.com</Text>)
                    oferuje najprostszą nawigację i oferuje atrakcyjną stronę, która sprawi, że będziesz cieszyć się
                    sprzedażą ręcznie robionych przedmiotów dla konsumentów. Za pomocą kilku prostych kliknięć łatwo
                    stwórz swój profil, prześlij aukcje i zacznij sprzedawać.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img6.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>eBay</Text>
                <Text><Text style={{color: 'blue'}}
                            onPress={() => Linking.openURL('https://www.ebay.com/')}>eBay</Text> Serwis eBay jest jedną
                    z tych witryn, które dosłownie zmieniły świat e-commerce. Z ponad 100 milionami kupujących serwis
                    eBay jest jednym z najbardziej rozpoznawalnych rynków internetowych na całym świecie.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img7.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>ArtFire</Text>
                <Text>ArtFire (<Text style={{color: 'blue'}}
                                     onPress={() => Linking.openURL('https://www.artfire.com/')}>artfire.com</Text>) to
                    szybko rozwijający się rynek internetowy do sprzedaży ręcznie robionych produktów zaprojektowanych
                    przez rzemieślników na całym świecie. Z ponad 30 000 zarejestrowanych aktywnie sprzedawców, ArFire
                    oferuje łatwą w obsłudze platformę, która umożliwia również opcje dostosowywania.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img8.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>DaWanda</Text>
                <Text>DaWanda (<Text style={{color: 'blue'}}
                                     onPress={() => Linking.openURL('http://en.dawanda.com/')}>dawanda.com</Text>) jest
                    jednym z wiodących rynków online do sprzedaży unikatowych i ręcznie robionych przedmiotów. Ponad 280
                    000 sprzedawców aktywnych w DaWandzie to dobre podejście do przyciągania kupujących.</Text>
                <Text style={styles.dmlH3}>Zibbet</Text>
                <Text><Text style={{color: 'blue'}}
                            onPress={() => Linking.openURL('https://www.zibbet.com/')}>Zibbet</Text> Zibbet to
                    internetowy sklep internetowy, w którym można sprzedawać ręcznie robione artykuły online - od dzieł
                    sztuki i fotografii po artykuły vintage i artykuły rzemieślnicze.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img9.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
