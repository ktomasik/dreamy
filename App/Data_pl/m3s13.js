import React, {Component} from 'react'
import {Image, Linking, Modal, Text, TouchableHighlight, View} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen10 = [{
    url: '',
    props: {
        source: require('../Images/m3/img56.png')
    }
}];

export default class M3S13 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Jeśli chcesz skonfigurować stronę, postępuj zgodnie z instrukcjami</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 1: Przejdź do strony <Text style={{color: 'blue'}}
                                                                                            onPress={() => Linking.openURL('https://www.wix.com/')}>wix.com</Text></Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen10}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img56.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
