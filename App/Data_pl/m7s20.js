import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S20 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Ulgi podatkowe dla kobiet rozpoczynających działalność</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text>Kobiety-przedsiębiorcy są przedmiotem kilku konkretnych rodzajów wsparcia. Ta część
                    przedsiębiorców jest przedmiotem szczególnej uwagi. Kobiety-przedsiębiorcy borykają się czasami z
                    bardziej skomplikowanymi sytuacjami osobistymi lub napotykają na większą zewnętrzną nieufność.
                    Dlatego istnieje specjalna pomoc w rozwijaniu firm przez kobiety.</Text>
                <Text>Pod względem wsparcia od kilku lat funkcjonują wyspecjalizowane sieci. Sieć Les Premières
                    utworzyła inkubatory przedsiębiorczości poświęcone prowadzonym przez kobiety projektom biznesowym,
                    które jednak muszą mieć innowacyjną stronę. Inkubator zapewnia kontynuację i organizację projektu
                    biznesowego na okres 1 roku. Kreatywna kobieta biznesu jest wspierana podczas pierwszego roku
                    działalności. Sieć Force Femmes wspiera kobiety w drugiej połowie kariery zawodowej, czyli mające
                    ponad 45 lat. Osoby z projektem stworzenia lub przejęcia firmy są wspierane podczas przygotowywania
                    swojego projektu, a obejmuje to: zatwierdzenie projektu, sformułowanie, realizację planu
                    biznesowego, itp. Actionelles wspiera również kobiety w tworzeniu przez nie firm. Stowarzyszenie
                    oferuje, oprócz wsparcia, także relacje między kreatywnymi kobietami i doświadczonymi właścicielami
                    firm.</Text>
                <Text>Wyżej wymienione 3 sieci niekoniecznie są obecne na terytorium całego kraju. </Text>
                <Text>Wsparcie dostępu do finansowania dla kobiet-przedsiębiorców.</Text>
                <Text>W zakresie dostępu do finansowania, France Active ustanowiło gwarancję EGALITE Femmes. Jest to
                    gwarancja bankowa, mająca na celu ułatwienie uzyskania kredytu bankowego przez
                    kobiety-przedsiębiorców. Gwarancja ta może zostać uruchomiona na projekty związane z tworzeniem,
                    odzyskiem lub rozwojem biznesu.</Text>
                <Text>Ten instrument uzupełnia system wsparcia dla projektów tworzenia firm przez kobiety.</Text>

                <FlatList
                    data={[
                        {key: 'https://les-aides.fr/focus/a5Zi/les-aides-pour-les-femmes-creatrices-ou-repreneuses-d-entreprise.html'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}}
                                                  onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
