import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Przepisy dotyczące biznesu online (e-handlu)</Text>
                <Text style={{marginTop: 10}}>W przypadku handlu elektronicznego na stronie internetowej muszą być
                    dostępne następujące informacje dla użytkowników:</Text>
                <Text style={{marginLeft: 10, marginTop: 5}}>Informacje:</Text>
                <FlatList
                    data={[
                        {key: 'Tytuł komercyjny, adres handlowy, numer ewidencji podatkowej lub handlowej, adres e-mail, numer telefonu i nazwiska administratora (ów) strony internetowej (oraz, w przypadku prowadzenia giełdy online, oficjalne informacje komunikacyjne sprzedających / dostawców );'},
                        {key: 'Czy strona działa na podstawie licencji lub zezwolenia agencji rządowej i odpowiedniej agencji.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 25}}>{item.key}</Text>}
                />
                <Text>Warunki odwiedzania i korzystania ze strony internetowej</Text>
                <FlatList
                    data={[
                        {key: 'Polityka prywatności.'},
                        {key: 'Podpis elektroniczny jest obowiązkowy.'},
                        {key: 'Umowa użytkownika (jeśli wymagane jest członkostwo).'},
                        {key: 'Umowa na odległość, która zostanie sporządzona zgodnie z rozporządzeniem w sprawie umów zawieranych na odległość (jeżeli strona internetowa będzie sprzedawać konsumentom jakiekolwiek towary lub usługi) zgodnie z ustawą o ochronie konsumentów nr 6502.'},
                        {key: 'Aby skoordynować przepływ pieniędzy między konsumentami a przedsiębiorstwem internetowym, firma internetowa musi współpracować z bankiem lub dostawcą usług płatniczych (Ustawa o usługach płatniczych nr 6493) (Dora i wsp. 2018).'}
                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
