import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text styl={{marginTop: 10}}>Wraz z rosnącą liczbą użytkowników, media społecznościowe stały się
                    integralną częścią dzisiejszego krajobrazu marketingu. Poniżej znajduje się przewodnik krok po
                    kroku, który pozwala skonfigurować konta w mediach społecznościowych dla Twojej firmy.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
