import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Wprowadzenie</Text>
                <Text style={{marginTop: 10}}>Grupą docelową Dreamy m- Learning Project są kobiety o niższym poziomie
                    wykształcenia, które wytwarzają rękodzieła w domu. Celem projektu jest pomoc w nabywaniu
                    umiejętności związanych z mobilnością cyfrową i przedsiębiorczością, aby sprzedawać swoje wyroby
                    rękodzielnicze na rynkach cyfrowych dzięki sprawnemu wykorzystaniu inteligentnych telefonów. Zgodnie
                    z tym celem opracujemy aplikację mobilną, która jest łatwa do zrozumienia i łatwa w użyciu; dla
                    systemów operacyjnych Android i iOS, interfejs internetowy łączący aplikacje mobilne z portalem
                    m-learning. W przygotowanym programie szkoleniowym wyjaśniamy kobietom, jak postępować krok po kroku
                    z darmowymi i popularnymi aplikacjami mobilnymi.
                    {"\n\n"}Zgodnie z tymi ramami moduł ten ma na celu wyjaśnienie zagadnień, takich jak tworzenie
                    faktur, zawieranie umów pomiędzy kupującym a dostawcą internetowym, ubieganie się o ubezpieczenie
                    sprzedaży produktów online i tworzenie podpisów elektronicznych dla kobiet, które chcą sprzedawać
                    swoje wyroby rzemieślnicze online.
                </Text>
            </View>
        )
    }
}
