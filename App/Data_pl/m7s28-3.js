import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Turcji:</Text>
                <Text style={styles.dmlH3}>Krok 3:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Daty rozpoczęcia szkoleń z zakresu przedsiębiorczości są również publikowane na oficjalnej stronie internetowej KOSGEB. Te szkolenia z zakresu przedsiębiorczości są całkowicie bezpłatne.',
                                val2: 'http://www.kosgeb.gov.tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
