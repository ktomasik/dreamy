import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Akceptacja płatności kartą kredytową</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie ze wskazówkami poniżej, aby akceptować płatności kartą kredytową
                    {"\n"}Do przyjmowania płatności kartą kredytową potrzebne będą:
                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Konto sprzedawcy:',
                                val2: 'rodzaj konta bankowego, na którym zdeponowane są płatności kartą kredytową i debetową'
                            }
                        },
                        {
                            key: {
                                val1: 'Wirtualny terminal:',
                                val2: 'Podobnie jak w przypadku fizycznej zapłaty kartą kredytową, system ten umożliwia wprowadzanie danych karty kredytowej na komputerze.'
                            }
                        },
                        {
                            key: {
                                val1: 'Bramka:',
                                val2: 'łącznik między Twoim sklepem internetowym a bankiem, który bezpiecznie przesyła informacje o płatnościach, aby je zatwierdzić lub odrzucić.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />

                <Text style={{marginTop: 10}}>Jak to działa krok po kroku</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Krok 1:',
                                val2: 'Kiedy klient kontaktuje się z Tobą, loguje się do wirtualnego terminalu i wybiera produkty lub usługi, które chce kupić. Następnie musi podać swoje dane karty kredytowej.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 2:',
                                val2: 'Informacje o płatnościach przechodzą przez bramkę bezpiecznych płatności i są przesyłane do źródła autoryzacji.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 3:',
                                val2: 'Bank, który wydał kartę kredytową, otrzymuje informacje o transakcji, sprawdza, czy środki są dostępne i zatwierdza lub odrzuca wykonanie płatności.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 4:',
                                val2: 'Brama płatności "mówi" czy płatność została zaakceptowana lub odrzucona.'
                            }
                        },
                        {
                            key: {
                                val1: 'Krok 5:',
                                val2: 'W przypadku zatwierdzenia środki są wpłacane na konto bankowe w ciągu 2-3 dni.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />
            </View>
        )
    }
}
