import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie konta e-mail w telefonie komórkowym dla aplikacji pocztowej
                    Android (Samsung, Sony, HTC ..)</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Gratulacje, założyłeś
                    swoje konto e-mail!</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Możesz zarejestrować się
                    we wszystkich kanałach społecznościowych za pomocą utworzonego konta e-mail.</Text>
                <Text style={{marginTop: 10}}>Następny moduł wyjaśni, jak skonfigurować konta w mediach
                    społecznościowych.</Text>
            </View>
        )
    }
}
