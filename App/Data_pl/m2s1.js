import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym jest networking? Co to dla ciebie oznacza?</Text>
                <Text>Według The Oxford Dictionary ("Słownik języka angielskiego, Słownik tezaurusa
                    i pomoc dla gramatyki | Słowniki oksfordzkie" 2018), Networking to "grupa ludzi, którzy wymieniają
                    informacje, kontakty i doświadczenia dla celów zawodowych lub społecznych". Jeśli jednak zapyta się
                    dziesięć różnych osób co to jest networking, można uzyskać aż dziesięć różnych odpowiedzi. Definicja
                    networkingu prawdopodobnie zależy od tego, w jaki sposób wykorzystuje się tę ważną osobistą i
                    zawodową aktywność. Niezależnie od tego, czy tworzysz sieć kontaktów, aby nawiązać nowe znajomości,
                    znaleźć nową pracę, rozwinąć swoją obecną karierę, poznać nowe możliwości kariery, uzyskać
                    referencje lub prowadzić sprzedaż lub po prostu poszerzyć swoje horyzonty zawodowe, ważne jest, aby
                    skupić się na tworzeniu sieci jako wymianie informacji, kontaktów lub doświadczenia ("WHAT is
                    Networking", nd). W każdej branży lub na poziomie kariery networking pomaga nawiązywać kontakty w
                    sposób osobisty i budować relacje wsparcia i szacunku dla odkrywania i tworzenia wzajemnych
                    korzyści. Jest to zestaw umiejętności, którego żadna poważna kobieta chcąca rozwinąć karierę
                    zawodową w XXI wieku nie może nie mieć.
                    {"\n\n"}Bez względu na branżę, w której się znajdujesz, istnieją odpowiednie osoby, z którymi
                    znajomość może się opłacić. Kiedy jest to dobrze zrobione, tworzenie sieci może doprowadzić do
                    wzrostu liczby klientów, większego zaangażowania i możliwości rozwoju kariery zawodowej. A dla osób
                    szukających pracy sieci naprawdę są dobrym rozwiązaniem bo umożliwiają wiele kontaktów. ("Co to jest
                    Networking? | Reed.co.uk", n.d.).</Text>
                <Text style={styles.dmlH2}>Czym są media społecznościowe?</Text>
                <Text>Media społecznościowe to komputerowa technologia ułatwiająca dzielenie się pomysłami i
                    informacjami oraz tworzenie wirtualnych sieci i społeczności. Z założenia media społecznościowe są
                    oparte na Internecie i oferują użytkownikom łatwą, elektroniczną komunikację danych osobowych i
                    innych treści, takich jak filmy i zdjęcia. Użytkownicy korzystają z mediów społecznościowych za
                    pośrednictwem komputera, tabletu lub smartfona, za pośrednictwem oprogramowania sieciowego lub
                    aplikacji internetowej, często wykorzystując je do przesyłania wiadomości.
                    {"\n\n"}Media społecznościowe powstały jako narzędzie, dzięki któremu ludzie kontaktowali się ze
                    znajomymi i rodziną, ale później zostały zaadoptowane przez firmy, które chciały skorzystać z
                    popularnej i nowej metody komunikacji, aby dotrzeć do klientów. Potęgą mediów społecznościowych jest
                    możliwość łączenia się i dzielenia informacjami z kimkolwiek na Ziemi (lub mnóstwem ludzi), o ile
                    używają również mediów społecznościowych (Silver, n.d.).</Text>
                <Text style={styles.dmlH2}>Krótka historia mediów społecznościowych</Text>
                <Text>Media społecznościowe mają historię sięgającą lat 70. (Ries 2016). ARPANET, który po raz pierwszy
                    pojawił się w Internecie w 1969 roku, pod koniec lat siedemdziesiątych opracował bogatą kulturową
                    wymianę pozarządowych/biznesowych pomysłów, co zostało zarejestrowane przez artukuł ARPANET #Zasady
                    i etykieta "1982 podręcznik informatyki w MIT's AI Lab zawiera odniesienie do etykiety sieci" oraz w
                    pełni spełnia obecną definicję terminu "media społecznościowe". Usenet, który pojawił się w 1979 r.,
                    został pokonany przez prekursora elektronicznego systemu tablic ogłoszeniowych (BBS), znanego jako
                    Community Memory w 1973 r. Prawdziwe elektroniczne systemy tablic ogłoszeniowych powstały wraz z
                    systemem Computer Bulletin Board w Chicago, który po raz pierwszy pojawił się w Internecie 16 lutego
                    1978 r. Wkrótce większość większych miast miała więcej niż jeden BBS działający na TRS-80, Apple II,
                    Atari, IBM PC, Commodore 64, Sinclair i podobnych komputerach osobistych ("Social Media -
                    Wikipedia", n.d.).
                    {"\n\n"}IBM PC został wprowadzony w 1981 roku, a kolejne modele komputerów Mac i PC były używane w
                    latach 80-tych. Wiele modemów wraz ze specjalistycznym sprzętem telekomunikacyjnym, umożliwiał wielu
                    użytkownikom jednoczesny dostęp do Internetu. Compuserve, Prodigy i AOL były trzema największymi
                    firmami BBS i jako pierwsze w latach 90. przeprowadziły migrację do Internetu. Od połowy lat 80. do
                    połowy lat 90. XX wieku BBS liczył się w dziesiątkach tysięcy tylko w Ameryce Północnej (Edvards
                    2016). Fora dyskusyjne (specyficzna struktura mediów społecznościowych) powstały wraz ze zjawiskiem
                    BBS w latach osiemdziesiątych i na początku lat dziewięćdziesiątych. Kiedy w połowie lat 90.
                    Internet rozprzestrzenił się, fora komunikacyjne migrowały online, stając się forami internetowymi,
                    głównie ze względu na tańszy dostęp do osób, a także możliwość obsługi znacznie większej liczby osób
                    jednocześnie niż banki modemów telekomunikacyjnych ("Media społecznościowe - Wikipedia", n.d.).
                    {"\n\n"}GeoCities był jednym z pierwszych portali społecznościowych w Internecie, który pojawił się
                    w listopadzie 1994 r., a po mim Classmates w grudniu 1995 r., następnie Six Degrees w maju 1997 r.,
                    Open Diary w październiku 1998 r., LiveJournal w kwietniu 1999 r., Ryze w październiku 2001 r.,
                    Friendster w marcu 2002 r., LinkedIn w maju 2003, hi5 w czerwcu 2003, MySpace w sierpniu 2003, Orkut
                    w styczniu 2004, Facebook w lutym 2004, Yahoo! 360° w marcu 2005 r., Bebo w lipcu 2005 r., Twitter w
                    lipcu 2006 r., Tumblr w lutym 2007 r. i Google+ w lipcu 2010 r. ("Grudzień 1995: Koledzy z klasy -
                    teraz i dziś: historia portali społecznościowych - Zdjęcia - Wiadomości Cbs", nd; "Historia i różne
                    typy mediów społecznościowych", oraz Ortutay 2012).</Text>
            </View>
        )
    }
}
