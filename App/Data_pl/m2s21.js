import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S21 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Instagramie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 8: Wpisz swoje imię i hasło. Następnie kliknij next. To
                    będzie ostatni etap tworzenia konta na Instagramie przed skonfigurowaniem profilu ...</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img37.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img38.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img39.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
