import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Rejestracja do stowarzyszeń zawodowych</Text>
                <Text style={{marginTop: 10}}>Postępuj zgodnie ze wskazówkami dotyczącymi rejestracji w stowarzyszeniach zawodowych:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Urząd skarbowy skieruje Cię do {"\n"}odpowiedniej izby, która może ubiegać{"\n"}się o stowarzyszenia zawodowe</Text>
                    </View>
                </View>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 320}}
                        source={require('../Images/images/30321.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
