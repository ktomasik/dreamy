import React, {Component} from 'react'
import {SectionList, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Marketing w mediach społecznościowych dla kobiet rozpoczynających
                    działalność</Text>
                <Text style={styles.dmlH3}>Cele</Text>
                <Text>Naprawdę dobry artykuł na temat strategii mediów społecznościowych napisał Alex York (2018).
                    Zaznaczył, że najważniejszą rzeczą do prowadzenia firmy jest ciągłe podążanie za swoimi celami.
                    Pomocnym jest jeśli zapiszesz te cele, aby były bardziej zdefiniowane i realistyczne.
                    Ustawienie celu jest podstawą wszystkich strategii marketingowych i biznesowych. Media
                    społecznościowe nie są wyjątkiem. Oczywiście, z szerokim zakresem możliwości społecznych, może być
                    trudno dokładnie określić, jakie powinny być Twoje cele. Aby uzyskać wskazówki, oto kilka typowych
                    celów mediów społecznościowych, które należy wziąć pod uwagę:</Text>
                <SectionList
                    sections={[
                        {
                            title: 'Zwiększanie świadomości marki:',
                            data: ['aby tworzyć autentyczną i trwałą świadomość marki, unikaj mnóstwa wiadomości promocyjnych. Zamiast tego skup się na sensownej treści i silnej osobowości marki za pośrednictwem kanałów społecznościowych.']
                        },
                        {
                            title: 'Wyższa jakość sprzedaży:',
                            data: ['przedzieranie się przez kanały społecznościowe jest prawie niemożliwe bez monitorowania konkretnych słów kluczowych, fraz lub hashtagów. Dzięki skuteczniejszemu kierowaniu w mediach społecznościowych szybciej docierasz do głównych odbiorców.']
                        },
                        {
                            title: 'Zwiększanie sprzedaży osobowej:',
                            data: ['niektórzy sprzedawcy detaliczni polegają na działaniach marketingowych w mediach społecznościowych w celu zwiększenia sprzedaży w sklepie. Czy Twoja marka promuje na tyle społecznie, aby nagradzać tych, którzy do ciebie przychodzą? A co z alarmowaniem klientów o tym, co dzieje się w twoich sklepach?']
                        },
                        {
                            title: 'Zwiększenie zwrot z inwestycji:',
                            data: ['w mediach społecznościowych nie ma marki, która nie chce zwiększyć zwrotu z inwestycji. Jednak w aspekcie społecznym cel ten jest charakterystyczny dla dokładnego audytu Twoich kanałów i zapewnienia, że koszty pracy, reklamy i projektu pozostają na dobrej drodze.']
                        },
                        {
                            title: 'Stworzenie lojalnej grupy fanów:',
                            data: ['Czy Twoja marka promuje treści generowane przez użytkowników? Czy człoklowie reagują pozytywnie, bez żadnej inicjacji? Dotarcie do tego punktu wymaga czasu i wysiłku, aby stworzyć pozytywną markę na płaszczyźnie społecznej.']
                        },
                        {
                            title: 'Lepszy impuls w branży:',
                            data: ['Co robią twoi konkurenci? Jakie strategie wykorzystują do zwiększenia zaangażowania lub sprzedaży? Posiadanie impulsu w branży może po prostu pomóc ulepszyć Twoje wysiłki i wyciągnąć kilka wskazówek od dobrze funkcjonujących osób.']
                        }

                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Publiczność</Text>
                <Text>Mamy wiele możliwości i opcji, w których można publikować nasze produkty. Aby wybrać jedną lub
                    kilka z nich, musimy zadać sobie pytanie: "Jaka jest nasza grupa docelowa?". Po otrzymaniu
                    odpowiedzi możemy śledzić dane demograficzne osób korzystających z różnych mediów
                    społecznościowych:</Text>
                <SectionList
                    sections={[
                        {
                            title: 'Najbardziej popularne dane demograficzne Facebooka to:',
                            data: ['Użytkownicy, kobiety (89%)', 'Użytkownicy w wieku 18-29 lat (88%)', 'Użytkownicy zlokalizowani w miastach i na wsi (po 81%)', 'Osoby zarabiające mniej niż 30 000 USD (84%)', 'Użytkownicy z doświadczeniem akademickim (82%)']
                        },
                        {
                            title: 'Najpopularniejsze demograficzne serwisy Instagrama obejmują:',
                            data: ['Użytkownicy, kobiety (38%)', 'Użytkownicy w wieku 18-29 lat (59%)', 'Użytkownicy miejscy (39%)', 'Osoby zarabiające mniej niż 30 000 USD (38%)', 'Użytkownicy z doświadczeniem akademickim (37%)']
                        },
                        {
                            title: 'Najbardziej popularne dane demograficzne na Twitterze obejmują:',
                            data: ['Użytkownicy, kobiety (25%)', 'Użytkownicy w wieku 18-29 lat (36%)', 'Użytkownicy zlokalizowani w miastach (26%)', 'Ci zarabiający 50 000 $ - 74.999 $ (28%)', 'Użytkownicy z wykształceniem wyższym (29%)']
                        }
                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{'\u2022'}{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Podobne produkty</Text>
                <Text>Zanim zaczniesz tworzyć treść, bardzo mądrze będzie zbadać konkurencję. Zrób to przed procesem
                    tworzenia treści, ponieważ często znajdujesz nowe sposoby patrzenia na treści, analizując, co czyni
                    Twoją konkurencję sukcesem. Najprostszym sposobem na znalezienie konkurentów jest proste
                    wyszukiwanie w Google. Sprawdź najcenniejsze słowa kluczowe, wyrażenia i terminy branżowe, aby
                    zobaczyć, kto się pojawi.</Text>
                <Text style={styles.dmlH3}>Zawartość</Text>
                <Text>Na początek zaleca się tworzenie treści pasujących do tożsamości Twojej marki. Oznacza to, że
                    należy unikać takich sytuacji, jak dotarcie do niepopularnych danych demograficznych bez pełnej
                    strategii. Treść musi być odpowiednia, a opublikowana strona nie może być wypełniona reklamami.
                    Kupujący w Internecie raczej wierzą w treści wideo niż tylko w obrazy. Jeśli to możliwe, użyj
                    przygotowanych tematów. Utrzymuj spójny i prosty format treści, aby czytelnicy nie mylili
                    ich.</Text>
                <Text style={styles.dmlH3}>Nie ignoruj</Text>
                <Text>Kanały mediów społecznościowych budowane są jako sieci. Oznacza to, że ich głównym celem jest
                    bycie miejscem do dyskusji, omawiania tematów i udostępniania treści. Twoja marka nie może zapomnieć
                    o podstawowych elementach "networkingu" i wymaga wysiłku, aby rozmowy i możliwości zaangażowania nie
                    zostały pozostawione bez opieki.
                    Poprzez media społecznościowe zyskujesz szacunek jako marka, po prostu będąc obecnym i rozmawiając z
                    publicznością. Dlatego obsługa klientów społecznościowych jest tak ważna dla marek, które chcą
                    podnieść świadomość odbiorców. Chodzi o zaangażowanie.</Text>
            </View>
        )
    }
}
