import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Jak otworzyć konto bankowe ald Twojej nowej firmy?</Text>
                <Text style={{marginTop: 15}}>Rachunek bankowy firmy umożliwia łatwe śledzenie wydatków, zarządzanie
                    wynagrodzeniem pracowników, przekazywanie środków finansowych inwestorom, otrzymywanie i
                    przekazywanie płatności oraz dokładniejsze planowanie budżetu. Utworzenie konta w banku biznesowym
                    wymaga prostych kroków, które są następujące:</Text>
                <FlatList
                    data={[
                        {key: 'Ustal, jakie konta są potrzebne'},
                        {key: 'Wybierz swój bank'},
                        {key: 'Uzyskaj nazwę swojej firmy'},
                        {key: 'Zdobądź potrzebne dokumenty, które pozwolą Ci na przyjęcie płatności.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>Różnica miedzy kartą kredytową a kartą debetową</Text>
                <Text style={{marginTop: 15}}>Karta kredytowa to karta, która pozwala na pożyczenie pieniędzy z linii
                    kredytowej, zwanej inaczej limitem karty kredytowej. Za pomocą karty dokonuje się podstawowych
                    transakcji, które są następnie odzwierciedlane na rachunku. Karty debetowe pobierają pieniądze
                    bezpośrednio z konta po dokonaniu zakupu. W zależności od banku i miejsca zakupu, może to potrwać
                    kilka dni.</Text>
            </View>
        )
    }
}
