import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M8S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Skuteczna strategia wysyłki i realizacji</Text>
                <Text style={{marginTop: 10}}>Wysyłka to jeden z kluczowych kroków, aby zapewnić klientowi efektywny
                    produkt przy idealnych kosztach związanych z produkcją towarów, dotarciem do klienta i otrzymaniem
                    płatności. Aby zapewnić bezpieczne dostarczenie produktu przy efektywnym koszcie dla klienta, należy
                    wziąć pod uwagę pewne czynniki, takie jak miejsce docelowe, specyfikacja zawartości, waga, wymiary
                    itp.
                    {"\n\n"}Koszty wysyłki są jednym z najwyższych wydatków małej firmy, ale strategie wysyłki i
                    realizacji pomagają przedsiębiorcom dotrzeć do klientów punktualnie w rozsądnych cenach. Obliczenia
                    cen dla małych firm opierają się na ilości i częstotliwości wysyłanych do wszystkich firm
                    wysyłkowych. Jeśli ilość przesyłek wzrasta, cena obniża się. Negocjacje są możliwe w przypadku wielu
                    firm typu cargo. Często dostępne są konta członkowskie, które pomagają zmniejszyć koszty wysyłki.
                    {"\n\n"}Korzystanie z kartonów dostarczanych przez firmy transportowe może zapobiegać ponoszeniu
                    dodatkowych opłat wymiarowych. Firmy mogą dostarczać różne rodzaje opakowań dla różnych produktów.
                    Najczęściej spotykane są torby plastikowe, pudełka kartonowe, paczki z kanałami powietrznymi dla
                    produktów wrażliwych, opakowania rolkowe do obrazów i papier o różnych rozmiarach.
                    {"\n\n"}Strategia wysyłki i realizacji jest integralną częścią rentowności sprzedaży. Jeśli zostanie
                    to zrobione poprawnie, strategia wysyłki i pakowania mogą pomóc w zwiększeniu liczby powtórnych
                    sprzedaży, a nawet w pozyskaniu nowych klientów.
                </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Produkcja</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Dotarcie do klienta</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Umowa i płatność</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Wysyłka</Text>
                    </View>
                </View>
            </View>
        )
    }
}
