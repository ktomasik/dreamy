import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym jest rejestracja marki? Czy muszę zarejestrować moją markę (nazwę
                    firmy)?</Text>
                <Text>Opracowanie i wprowadzenie na rynek nowej marki wymaga zainwestowania dużej ilości kapitału
                    finansowego, umysłowego i emocjonalnego, dlatego też rejestracja marki lub ochrona prawna nowej
                    marki powinna być priorytetem dla każdego nowego przedsięwzięcia biznesowego. Ma to zastosowanie bez
                    względu na to, czy nowa marka jest nową firmą, nowym produktem lub usługą, czy nową firmą
                    internetową (Anonymous 2018e).
                    {"\n\n"}Rejestracja marki jest inną nazwą rejestracji znaku towarowego i jest to jedyny sposób, w
                    jaki właściciel marki może uzyskać wyłączne prawa do korzystania z nowej marki na dowolnym
                    terytorium kraju. Ani rejestracja spółki, ani rejestracja nazwy domeny nie zapewni żadnej ochrony
                    prawnej nowej marki (Forbes Agency Council 2017).
                    {"\n\n"}Bez rejestracji znaku towarowego nie ma sposobu, aby uniemożliwić konkurentom lub
                    "naśladowcom" korzystanie z tej samej marki.
                </Text>
                <Text style={styles.dmlH2}>Jak zarejestrować znak towarowy dla nazwy firmy?</Text>
                <Text>Każdy kraj ma własne biuro do rejestracji znaku towarowego lub logo. Ponadto każdy kraj ma inne
                    procedury zgodnie z przepisami i regulacjami krajów. Aby zarejestrować znak towarowy, firmę lub
                    powiązaną osobę, należy uiścić opłatę rejestracyjną. Termin rejestracji znaku towarowego może być
                    różny, ale zwykle wynosi dziesięć lat. Rejestracja może być przedłużana na czas nieokreślony po
                    uiszczeniu dodatkowych opłat.
                    {"\n\n"}Prawa do znaków towarowych są prywatnymi prawami, a ochrona jest egzekwowana na podstawie
                    nakazów sądowych.
                </Text>
            </View>
        )
    }
}
