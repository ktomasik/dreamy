import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Facebooku</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 10: Wpisz hasło i zapamiętaj je. Następnie kliknij
                    przycisk OK.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img19.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
