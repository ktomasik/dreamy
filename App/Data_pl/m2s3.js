import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Typowe funkcje mediów społecznościowych</Text>
                <Text>Dzisiaj jest wiele serwisów społecznościowych. Zapewniają różne usługi, mają różnych fanów i
                    cieszą się bardzo wyraźną tożsamością. Jednak wszystkie mają wspólną cechę (Sunil 2017). Oto
                    niektóre kluczowe cechy serwisów społecznościowych:</Text>
                <Text style={styles.dmlH3}>Zapewniają bezpłatną przestrzeń internetową:</Text>
                <Text>Członkowie tych stron nie muszą posiadać ani udostępniać serwerów internetowych. Mogą publikować
                    swoje treści na wolnej przestrzeni udostępnianej przez te witryny.</Text>
                <Text style={styles.dmlH3}>Zapewniają bezpłatny adres internetowy:</Text>
                <Text>Członkom przydzielany jest unikalny adres internetowy, który staje się tożsamością internetową
                    osoby lub firmy. Może służyć do identyfikacji, łączenia i udostępniania treści.</Text>
                <Text style={styles.dmlH3}>Proszą członków o tworzenie profili:</Text>
                <Text>Te witryny wymagają od członków tworzenia profili. Informacje wprowadzone w profilach służą do
                    łączenia znajomych i kontaktów oraz budowania sieci łączących ludzi o podobnych zainteresowaniach na
                    całym świecie.</Text>
                <Text style={styles.dmlH3}>Zachęcają użytkowników do przesyłania treści:</Text>
                <Text>Witryny te umożliwiają członkom przesyłanie wiadomości tekstowych, zdjęć, plików audio i wideo.
                    Wszystkie posty są publikowane w porządku malejącym, a ostatni post przychodzi pierwszy. Co
                    najważniejsze, cała zawartość jest publikowana w czasie rzeczywistym i może być odczytywana,
                    przeglądana lub udostępniana natychmiast.</Text>
                <Text style={styles.dmlH3}>Zezwalają członkom na budowanie konwersacji</Text>
                <Text>Członkowie mogą przeglądać zawartość i komentować. Dzięki temu witryny społecznościowe pozwalają
                    członkom angażować się w konwersacje, które zwiększają zaangażowanie.</Text>
                <Text style={styles.dmlH3}>Zezwalają na czaty na żywo:</Text>
                <Text>Wiele serwisów społecznościowych umożliwia członkom czatowanie ze sobą w czasie
                    rzeczywistym.</Text>
                <Text style={styles.dmlH3}>Bezpośrednie udostępnianie wiadomości:</Text>
                <Text>Wiele serwisów społecznościowych udostępnia swoim członkom bezpośrednie narzędzie do przesyłania
                    wiadomości. Pozwala to członkom wysyłać prywatne wiadomości, które mogą być odczytywane lub
                    przeglądane tylko przez tych, dla których wiadomość jest przeznaczona.</Text>
                <Text style={styles.dmlH3}>Zapewniają alerty dotyczące tagowania:</Text>
                <Text>Większość serwisów społecznościowych powiadamia członków za pośrednictwem wiadomości e-mail lub
                    powiadomień na stronie, gdy są oni oznaczeni w wiadomości lub na zdjęciu.</Text>
                <Text style={styles.dmlH3}>Umożliwiają członkom tworzenie unikalnych stron:</Text>
                <Text>W niektórych serwisach społecznościowych członkowie mogą tworzyć strony tematyczne. Strony mogą
                    być następnie wykorzystywane do publikowania artykułów lub fotografii związanych z tematem. Strony
                    mogą być również używane do promowania firm (kont użytkowników, stron profilu, przyjaciół,
                    obserwujących, grup itd.).</Text>
            </View>
        )
    }
}
