import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img11.png')
    }
}];

export default class M8S8_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Wysyłanie ładunku</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>KROK 4: PRZEKAŻ PACZKĘ FIRMIE WYSYŁKOWEJ</Text>
                <Text>Niektóre sposoby dotarcia do firm przesyłkowych:</Text>
                <FlatList
                    data={[
                        {key: 'Lokalne oddziały,'},
                        {key: 'Oddziały mobilne,'},
                        {key: 'Strona internetowa,'},
                        {key: 'Aplikacje mobilne,'},
                        {key: 'Sms,'},
                        {key: 'Samoobsługa,'},
                        {key: 'Centrum telefoniczne..'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img11.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
