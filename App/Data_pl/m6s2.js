import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text>Jak zobaczyłeś w module 1, musisz dokonać rozróżnienia między Internetem a stroną internetową:
                    Internet to globalna sieć komputerowa łącząca sieci prywatne, publiczne, biznesowe, akademickie i
                    rządowe - połączonych za pomocą technologii przewodowych, bezprzewodowych i światłowodowych.
                    Internet lub World Wide Web (W3) to w zasadzie system serwerów internetowych obsługujących
                    specjalnie sformatowane dokumenty.
                    {"\n\n"}Internet, jako metoda łączenia komputerów na całym świecie, jest sposobem na transport
                    treści. Musisz podłączyć się do Internetu, aby przeglądać World Wide Web i dowolne inne strony
                    internetowe lub inne treści, które zawiera Internet. Internet wykorzystuje również przeglądarki,
                    takie jak Google lub Internet, do uzyskiwania dostępu do dokumentów sieci Web zwanych stronami
                    internetowymi, które są ze sobą połączone za pomocą hiperłączy.
                    {"\n\n"}Podatnicy, którzy mają status spółki z ograniczoną odpowiedzialnością lub jednoosobowego
                    przedsiębiorcy sprzedającego towary i usługi przez Internet, mogą korzystać z aplikacji faktur
                    elektronicznych. Nie ma ograniczeń obrotu dla elektronicznego. Nawet osoby o bardzo niskich obrotach
                    mogą skorzystać z fakturowania elektronicznego.
                    {"\n\n"}Szybkie wyszukiwanie w Google ujawni wiele opcji. Programy takie jak Microsoft Office mają w
                    swoim oprogramowaniu konfigurowalne szablony i możesz pobrać szablony oraz znaleźć strony
                    internetowe dostępne w pakiecie Office.
                    {"\n\n"}Szablon tablica gotówkowa: Tablica gotówkowa oferuje szablon do pobrania, który można
                    otworzyć w programie Microsoft Word. Szablony są proste, ale można je dostosować.
                    {"\n\n"}Szablony Invoice Berry - szablony Office, Open Office i Excel - Jeśli nie podoba ci się ten
                    szablon z pakietu Microsoft Office, przejdź do galerii szablonów Microsoft, aby uzyskać więcej
                    opcji.
                    {"\n\n"}Oprócz pakietu Microsoft Office, Dokumenty Google mają szablony, które można wybrać w
                    galerii szablonów, a Apple także oferuje opcje dla aplikacji.
                    {"\n\n"}Niektóre witryny umożliwiają tworzenie, drukowanie, zapisywanie i wysyłanie faktur
                    bezpośrednio z ich witryny. Nie potrzebujesz żadnych innych programów poza przeglądarką internetową.
                    {"\n\n"}W poniższej sekcji przedstawiono niektóre bezpłatne aplikacje do nauki tworzenia faktur
                    elektronicznych.
                </Text>
            </View>
        )
    }
}
