import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S53 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Korzystanie z serwisów społecznościowych</Text>
                <Text style={styles.dmlH2}>Stwórz stronę na Facebooku</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 9: Kliknij przycisk Dalej, aby kontynuować i kliknij
                    ODWIEDŹ STRONĘ, aby przejść do strony FaceBook.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img108.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img109.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
