import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements'
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz4": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Logo",
                    "option2": "b) Marka",
                    "option3": "c) Patent",
                    "option4": "d) Identyfikator podatkowy"
                },
                "question": "_______ to nazwa, logo, znak i kształt które pojedynczo lub w połączeniu umożliwiają konsumentowi odróżnienie produktu lub usługi od innych na rynku."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Marka i znak towarowy muszą być takie same."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Do lokalnej gminy",
                    "option2": "b) Do Urzędu skarbowego",
                    "option3": "c) Spółdzielnia",
                    "option4": "d) Izba Przemysłowo-Handlowa"
                },
                "question": "Gdzie trzeba się udać aby otrzymać licencję i pozwolenie na pracę w domu?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Nazwy prawnej używa się podczas procedur rządowych, nazwa marketingowa zaś ma zastosowanie w działaniach PR."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Znak towarowy jest prawnym środkiem ochrony marki, przyznawanym przez Urząd Patentowy."
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Złożenie wniosku do Urzędu skarbowego",
                    "option2": "b) Zapytanie w spółdzielni",
                    "option3": "c) Zapytanie w Izbie Przemysłowo-Handlowej",
                    "option4": "d) Wystawienie rachunku"
                },
                "question": "Jaki jest pierwszy krok do zostania podatnikiem?"
            },
            "question7": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Nazwa handlowa",
                    "option2": "b) Faktura",
                    "option3": "c) Członkostwo w izbie",
                    "option4": "d) Numer identyfikacji podatkowej"
                },
                "question": "Do zastosowania system e-deklaracji potrzeba:"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Nazwa firmy nie może być używana w formularzach rządowych i aplikacjach."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Zgody użytkownika",
                    "option2": "b) Polityki prywatności",
                    "option3": "c) Pozwolenia właścicieli lokalu",
                    "option4": "d) Opatentowanego logo"
                },
                "question": "W przypadku e-handlu nie wymaga się:"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Bez zarejestrowania znaku towarowego, nie ma sposobu aby uniemożliwić konkurentom lub “naśladowcom” korzystania z tej samej marki."
            }
        }
    }
};

export default class M4Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz4;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
