import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Grecji:</Text>
                <Text style={styles.dmlH3}>Krok 2:</Text>
                <Text>Pierwsze spotkanie odbywa się w celu dokładniejszego przedstawienia planu biznesowego. Celem
                    spotkania jest potwierdzenie spełnienia kryteriów oceny i identyfikacji potrzeb finansowych, a także
                    odpowiedniego instrumentu finansowego dla nich.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
