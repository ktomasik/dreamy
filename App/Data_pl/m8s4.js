import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img1.png')
    }
}];

export default class M8S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Pakowanie i marketing</Text>
                <Text style={styles.dmlH3}>Obliczanie stawek</Text>
                <Text>Jeśli chodzi o klasyfikację ładunków, jedną z najistotniejszych rzeczy, które należy wziąć pod
                    uwagę, jest gęstość. Niektóre przedmioty, które możesz usłyszeć, są oparte na gęstości. Zwykle im
                    gęstszy przedmiot, tym niższa obowiązuje klasa. Jeśli chodzi o fracht, produkt niskiej klasy o
                    wysokiej gęstości, będzie go kosztować mniej niż wysyłka przeciwnie niż produkt o wyższej klasie
                    niskiej gęstości (Anonymous 2018c).
                    {"\n\n"}Obliczanie gęstości ładunku zapewni również zalecaną klasę dla przesyłki. Tabela klas
                    przewozowych jest skróconą skalą, która może posłużyć do oszacowania klasyfikacji frachtu dla
                    przesyłek (Anonimowy 2018d).
                </Text>

                <Text style={styles.dmlH3}>Techniki pakowania</Text>
                <Text style={{fontWeight: 'bold'}}>Podstawowa metoda pakowania (metoda pojedynczego opakowania)</Text>
                <FlatList
                    data={[
                        {key: 'Wysyłaj produkty miękkie lub delikatne w wytrzymałych pudełkach zewnętrznych.'},
                        {key: 'Stosuj wypełniacze, takie jak zmięte gazety lub powietrzny materiał amortyzujący, aby wypełnić puste przestrzenie i zapobiec przemieszczaniu się towarów w pudełku podczas transportu.'},
                        {key: 'Umieszczaj w plastikowych torbach towary, które mogą być zanieczyszczone brudem, wodą w niekorzystnych warunkach.'},
                        {key: 'Konsoliduj małe części lub granulowane produkty w silnym zamkniętym pojemniku, takim jak juta lub torby sift-proof, a następnie zapakuj je w mocne opakowanie zewnętrzne.'},
                        {key: 'Użyj metody pakowania H do uszczelnienia opakowania (Anonymous 2018e).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{fontWeight: 'bold', marginTop: 5}}>Metoda "pudełko w pudełku"</Text>
                <FlatList
                    data={[
                        {key: 'Owiń produkty pojedynczo przy grubości izolacji co najmniej 5 cm (2") aby pasował ciasno do pudła z tektury falistej.'},
                        {key: 'Ogranicz ruch produktu w pudełku za pomocą wypełniacza, np. pogniecionej gazety, porowatej kruszonki lub innego materiału amortyzującego.'},
                        {key: 'Zamknij i przyklej taśmę wewnętrzną za pomocą metody taśmowania H. Pomoże to zapobiec przypadkowemu otwarciu.'},
                        {key: 'Użyj drugiego opakowania, które jest co najmniej 15 cm (6") dłuższe, szersze i głębsze niż wewnętrzne pudełko.'},
                        {key: 'Wybierz metodę owijania lub wypełniania w celu wyściełania wewnętrznej skrzynki wewnątrz większego, wytrzymałego pudełka zewnętrznego.'},
                        {key: 'Wysyłaj osobno produkty delikatne, owijając je grubością minimalnie 8 cm (3") powietrzno-komórkowych materiałów amortyzujących.'},
                        {key: 'Owiń pudełko wewnętrzne materiałem amortyzującym z poduszkę powietrzną o grubości 8 cm (3 cale) lub użyj co najmniej 8 cm (3 ") materiału amortyzującego, aby wypełnić przestrzenie między pudełkiem wewnętrznym a pudełkiem zewnętrznym z każdej strony.'},
                        {key: 'Wypełnij wszystkie puste przestrzenie bardziej materiałem amortyzującym.'},
                        {key: 'Użyj metody taśmowania H do uszczelnienia opakowania (Anonymous 2018e).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
