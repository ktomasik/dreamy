import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz8": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Koszty wysyłki to największe wydatki dla firmy"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Samodzielna dostawa",
                    "option2": "b) Dostawa tego samego dnia",
                    "option3": "c) Odbiór w sklepie",
                    "option4": "d) Wysyłka ze sklepu"
                },
                "question": "Opcją wysyłki nie jest:"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Strategia wysyłki i realizacji zamówień jest integralną częścią rentowności sprzedaży."
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "W przypadku materiałów rzemieślniczych wysyłanych w małych ilościach powiązane przedsiębiorstwa transportowe mogą przekazywać towary drogą lądową."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Plastikowych toreb",
                    "option2": "b) Kartonowych pudeł",
                    "option3": "c) Opakowań z kanałami powietrznymi",
                    "option4": "d) Wszystkie wymienione"
                },
                "question": "Do transport towaru używa się:"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Oferowanie darmowej wysyłki to kiepski sposób na zainteresowanie klienta:"
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Ubezpieczenie",
                    "option2": "b) Faktura",
                    "option3": "c) Deklaracja celna",
                    "option4": "d) Etykieta pakunku"
                },
                "question": "______ jest urzędowym dokumentem zawierającym wykaz i dane dotyczące towarów, które są importowane lub eksportowane."
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Umowa z firmą transportową w celu pracy ciągłej może przyczynić się do obniżenia kosztów dostawy."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Filie",
                    "option2": "b) Samoobsługa",
                    "option3": "c) Przyjaciele",
                    "option4": "d) Call center"
                },
                "question": "Sposobem na zwiększenie zasięgu firmy transportowej nie są:"
            },
            "question10": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Imienia, nazwiska, adresu nadawcy",
                    "option2": "b) Koloru przedmiotu w paczce ",
                    "option3": "c) Imienia, nazwiska, adresu odbiorcy",
                    "option4": "d) Szczegółów płatności"
                },
                "question": "Na fakturze nie uwzględnia się:"
            }
        }
    }
};

export default class M8Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz8;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
