import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Słowenii:</Text>
                <Text style={styles.dmlH3}>Krok 5:</Text>
                <Text>Zarejestruj firmę prowadzącą działalność na własny rachunek. Złóż wniosek o obowiązkowe
                    ubezpieczenie społeczne. Prześlij pełny wniosek do urzędu pracy w ciągu 30 dni od zatwierdzonego
                    biznesplanu.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
