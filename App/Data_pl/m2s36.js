import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S36 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe czynności, aby udostępnić informacje z serwisu Instagram
                    innym sieciom społecznościowym</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 6: Aby uwzględnić ten wpis w innych sieciach
                    społecznościowych, sprawdź fails w sekcji Udostępnij. Na koniec kliknij Udostępnij w prawym górnym
                    rogu.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img68.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
