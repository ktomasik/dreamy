import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S35 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe czynności, aby udostępnić informacje z serwisu Instagram
                    innym sieciom społecznościowym</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 5: Zaleca się dodanie #hashtag do opisu, aby zamieścić
                    post w treści #hashtags. Sprytnie jest włączyć #hashtag, który ma już wielu obserwatorów. Ponieważ
                    umieszczamy tarczę arduino dla robotyki, dołączamy #arduino i #arduinoshield. Oba #hashtags są
                    śledzone przez kilka tysięcy followers.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img66.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img67.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
