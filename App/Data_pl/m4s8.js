import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Problem formowania umowy w przypadku handlu elektronicznego</Text>
                <Text style={{marginTop: 10}}>W przypadku wszystkich krajów główne kroki opisano poniżej:</Text>
                <FlatList
                    data={[
                        {key: 'Krok 1: Ustanowienie oferty i procedura akceptacji'},
                        {key: 'Krok 2: Wypełnianie formularza zamówienia'},
                        {key: 'Krok 3: Uwzględnienie warunków'},
                        {key: 'Krok 4: Przejęcie danych karty kredytowej konsumenta przez Internet'},
                        {key: 'Krok 5: Potwierdzenie otrzymania zamówienia'},
                        {key: 'Krok 6: Dostarczenie potwierdzenia informacji i prawo do anulowania'},
                        {key: 'Krok 7: Dostawa'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla TURCJI</Text>
                <Text>Przepisy dotyczące handlu elektronicznego wymagają, aby wszystkie komercyjne strony internetowe
                    bezpośrednio i trwale udostępniały konsumentom następujące informacje za pośrednictwem strony
                    internetowej:</Text>
                <FlatList
                    data={[
                        {key: 'nazwa firmy, adres pocztowy (i adres siedziby, jeśli jest inny) i adres e-mail;'},
                        {key: 'numer rejestracyjny firmy;'},
                        {key: 'jakiekolwiek członkostwo w Stowarzyszeniach Trade lub Professional'},
                        {key: 'numer VAT firmy.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Wszystkie te dane muszą być uwzględnione niezależnie od tego, czy witryna
                    sprzedaje on-line. Ponadto każda informacja handlowa, taka jak usługa tekstowa e-mail lub SMS,
                    używana w celu świadczenia "Usługi społeczeństwa informacyjnego", musi wyświetlać te informacje.
                    {"\n"}Przepisy dotyczące handlu elektronicznego wymagają również, aby wszystkie ceny były jasne, a
                    strony internetowe muszą określać, czy ceny zawierają podatki i koszty dostawy (National Report
                    Turkey, 2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla SŁOWENII</Text>
                <Text>Zarejestruj firmę w Administracji Płatności Publicznych Republiki Słowenii.
                    {"\n\n"}Uzyskaj dostęp do korzystania z portalu w celu wystawiania faktur (przed uzyskaniem
                    cyfrowego certyfikatu).
                    {"\n\n"}Dostęp do strony internetowej Administracji Płatności Publicznych Republiki Słowenii może
                    zostać udostępniony (National Report Slovenia, 2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla POLSKI</Text>
                <Text>Od 2018 roku każdy przedsiębiorca musi przygotowywać deklarację VAT.
                    {"\n\n"}W rezultacie przedsiębiorca musi przygotować fakturę (przygotowanie e-faktury jest
                    przekazywane nie tylko przez programy komputerowe, ale także przez niektóre banki za pośrednictwem
                    rachunków bankowych).
                    {"\n\n"}Wszystkie wymagane dokumenty związane z ubezpieczeniem społecznym przedsiębiorcy należy
                    przekazywać w formie elektronicznej za pośrednictwem dedykowanego programu przygotowanego przez SII
                    (ZUS-PŁATNIK) (National Report Poland, 2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla GRECJI</Text>
                <Text>E-fakturowanie zostało częściowo wprowadzone w Grecji w roku 2006 r.ale system elektroniczny nadal
                    nie działa w pełni, zostanie zakończony do końca 2019 r.
                    {"\n\n"}Dąży się do tego, aby zaraz po wystawieniu faktury powiadomić w czasie rzeczywistym system
                    rachunkowości klienta o akceptacji opłaty, a jednocześnie urząd skarbowy może pobrać podatek (Raport
                    krajowy Grecja, 2018 r.).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla FRANCJI</Text>
                <Text>Komercyjne strony, które zbierają dane osobowe (imię i nazwisko, e-mail ..) i stanowią pliki
                    klientów i potencjalnych klientów, muszą złożyć uproszczoną deklarację do Krajowej Komisji ds.
                    Informatyki i Wolności.
                    {"\n\n"}Internetowe witryny handlowe zwykle podlegają uproszczonym standardom 48 (National Report
                    France, 2018).
                </Text>
            </View>
        )
    }
}
