import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Gdzie znaleźć pożyczki dla małych firm prowadzonych przez kobiety?</Text>
                <Text style={styles.dmlH2}>Dla Słowenii:</Text>
                <Text style={styles.dmlH3}>Banki</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Prosta i szybka pożyczka online do 7000 EUR bez kosztów zatwierdzenia.',
                                val2: 'http://www.hipkredit.si/'
                            }
                        },
                        {
                            key: {
                                val1: 'Mikrokredyty o niższym oprocentowaniu i kosztach, w których stały miesięczny koszt jest alokowany w całym okresie spłaty pożyczki.',
                                val2: 'http://www.intesasanpaolobank.si/'
                            }
                        },
                        {key: {val1: 'Atrakcyjna oferta dla s.p.', val2: 'http://www.sparkasse.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Usługi finansowe</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Pożyczka o wartości 500 lub 1000 EUR.', val2: 'http://www.skupina8.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Regionalna Izba Handlowo-Przemysłowa</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Udziela pożyczek z dopłatą do części oprocentowania krótkoterminowych pożyczek dla członków.',
                                val2: 'http://www.eng.gzs.si/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Enterprise Investment Center</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Szybka pożyczka online od 1000 EUR do 30000 EUR.', val2: 'http://www.pnc.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
