import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym jest mikrofinansowanie?</Text>
                <Text>Mikrofinansowanie to pojęcie zbiorcze używane do oznaczania produktów i usług finansowych dla osób
                    wyłączonych z tradycyjnego obiegu bankowego. Mikrofinansowanie pozwala takim ludziom finansować
                    środki do życia, oszczędzać, zapewniać byt rodzinom i chronić się przed codziennymi zagrożeniami
                    życia.</Text>
                <Text style={styles.dmlH2}>Jak działa mikrofinansowanie?</Text>
                <Text>Instytucje mikrofinansowe oferują kredytobiorcom "mikrokredyty" wraz z pomocą (finansowaniem nowej
                    firmy lub planem ekspansji, płaceniem za pilne potrzeby rodziny, ułatwianiem mobilności w celu
                    uzyskania pracy itp.), mimo że ci kredytobiorcy nie oferują solidnej gwarancji spłaty. Dochody
                    generowane przez działalność gospodarczą kredytobiorców mikrokredytów umożliwiają im spłatę salda
                    kredytu.</Text>
                <Text style={styles.dmlH2}>W jaki sposób wkład mikrofinansowania wzmacnia pozycję
                    kobiet-przedsiębiorców?</Text>
                <Text>Wkład mikrofinansów ma pomóc ludziom znajdującym się w trudnej sytuacji finansowej, w tym
                    kobietom, w znalezieniu zatrudnienia, zwiększeniu zaufania, poprawie umiejętności komunikacyjnych
                    oraz w innych aspektach. Dzięki zaangażowaniu w programy mikrofinansowania, kobiety mogą uzyskać
                    większą kontrolę nad zasobami materialnymi, intelektualnymi, takimi jak wiedza, informacje, pomysły,
                    a także podejmowanie decyzji w domu, społeczności, społeczeństwie i narodzie.</Text>
                <Text>Mikrofinansowanie jest ważnym narzędziem służącym umocnieniu przedsiębiorczości kobiet w
                    perspektywie zasobów. Pożyczki grupowe, które zostały ogłoszone jako ważne narzędzie łagodzenia
                    ubóstwa, przynoszą korzyści członkom poprzez tworzenie sieci kontaktów.</Text>
            </View>
        )
    }
}
