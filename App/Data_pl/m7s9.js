import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Gdzie znaleźć pożyczki dla małych firm prowadzonych przez kobiety?</Text>
                <Text style={styles.dmlH2}>Dla Grecji:</Text>
                <Text style={styles.dmlH3}>Banki</Text>
                <Text>Kiedy banki finansują nowe przedsiębiorstwa, proszą o zapoznanie się z biznesplanem. Aplikacja
                    jest badana przez kilka grup sprawdzających różne obszary, szczególnie ważne jest, aby biznesplan
                    był jak najbardziej kompletny, aby uniknąć opóźnień.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
