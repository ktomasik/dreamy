import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Podstawy podatkowe</Text>
                <Text>Obowiązek podatkowy to kwota, którą osoba fizyczna jest winna organom podatkowym (Cameron 2017).
                    {"\n"}Rząd wykorzystuje podatki do finansowania programów społecznych i ról administracyjnych.
                    {"\n"}Zasadniczo zobowiązanie podatkowe stanowi zazwyczaj pewien procent dochodu i zmienia się w
                    zależności od niego.
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>For TÜRKİYE</Text>
                <FlatList
                    data={[
                        {key: 'Należy sporządzić deklarację. Adres domowy może być adresem faktury.'},
                        {key: 'Podczas składania wniosku opodatkowanie powinno zostać wybrane.'},
                        {key: 'Po dokonaniu oceny przez urząd skarboy, biuro prześle kartę podatkową do podatnika.'},
                        {key: 'Rejestracja powinna odbywać się w "Konfederacji tureckich rzemieślników i handlowców".'},
                        {key: 'Wniosek należy złożyć do gminy z prośbą o pozwolenie na pracę.'},
                        {key: 'Podatnicy podlegający opodatkowaniu w małych przedsiębiorstwach otrzymają dokumenty od izb lub stowarzyszeń.'},
                        {key: 'Podatnicy muszą deklarować dochody poprzez deklarację roczną.'},
                        {key: 'Oświadczenie zostanie złożone w zarejestrowanym urzędzie skarbowym. W rezultacie podatnik zapłaci podatek na podstawie faktury (National Report Turkey, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla SŁOWENII</Text>
                <FlatList
                    data={[
                        {key: 'VAT (podatek od wartości dodanej)'},
                        {key: 'Podatek dochodowy od osób prawnych'},
                        {key: 'Podatek dochodowy od osób fizycznych'},
                        {key: 'Składki na ubezpieczenie społeczne'},
                        {key: 'Podatek od nieruchomości'},
                        {key: 'Podatek od zysków kapitałowych'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Wybierz formę biznesową:</Text>
                <FlatList
                    data={[
                        {key: 'Prywatni przedsiębiorcy, podatki to dochód z działalności, który wywodzi się z działalności gospodarczej prowadzonej niezależnie od rolnictwa, leśnictwa, pracy lub innej niezależnej działalności na własny rachunek.'},
                        {key: 'Spółka z ograniczoną odpowiedzialnością, ma osobowość prawną i jest opodatkowana podatkiem dochodowym od osób prawnych.'},
                        {key: 'Podatki osób sprzedających w internecie, które są identyfikowane dla celów podatku VAT, muszą obliczyć podatek VAT od dostawy produktu do klienta w Słowenii (raport krajowy Słowenia, 2018 r.).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla POLSKI</Text>
                <Text>Dla osób, które chcą prowadzić własną działalność, zobowiązania podatkowe są płacone na podstawie
                    podatku dochodowego, tak jak ma to miejsce w przypadku pracowników zatrudnionych w pełnym wymiarze
                    godzin. Osoba musi wybrać jedną z zasad podatkowych:</Text>
                <FlatList
                    data={[
                        {key: 'Ogólne zasady podatkowe,'},
                        {key: '19% podatku (podatek liniowy),'},
                        {key: 'Ryczałt z zarejestrowanych dochodów,'},
                        {key: 'Karta podatkowa (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla GRECJI</Text>
                <FlatList
                    data={[
                        {key: 'Osoby i firmy muszą złożyć elektroniczną deklarację podatkową za pośrednictwem internetowego systemu Niezależnego Urzędu Dochodów Publicznych, na podstawie którego będą opodatkowane.'},
                        {key: 'Pod koniec każdego miesiąca przesyłają listę faktur i VAT.'},
                        {key: 'Dla osób, które chcą prowadzić własną działalność, zobowiązania podatkowe są płacone na podstawie podatku dochodowego.'},
                        {key: 'Podobnie jak w przypadku pełnoetatowych pracowników - bez potrącenia podatku.'},
                        {key: 'Niezależnie od zysków wszystkie greckie podmioty są opodatkowane stawką 29%.'},
                        {key: 'Akcje są opodatkowane stawką 15% (Raport krajowy Grecja, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla FRANCJI</Text>
                <Text>Opodatkowanie zysków zależy od prawnej struktury działalności. Podmioty mogą podlegać podatkowi
                    dochodowemu (IR) lub podatkowi od osób prawnych (CI).
                    {"\n"}Firmy podlegają:
                </Text>
                <FlatList
                    data={[
                        {key: 'podatkowi dochodowemu'},
                        {key: 'terytorialnemu podatkowi ekonomicznemu (CET),'},
                        {key: 'VAT,'},
                        {key: 'Poszczególne spółki (rzemieślnicy, handlowcy), wolne zawody i EURL (jednoosobowa spółka z ograniczoną odpowiedzialnością) muszą płacić podatek dochodowy.'},
                        {key: 'Partnerzy są opodatkowywani osobiście z tytułu podatku dochodowego tylko od wynagrodzeń lub dywidend (Raport krajowy Francja, 2018 r.).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
