import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Strony internetowe</Text>
                <Text>Strona internetowa to dokument odpowiedni dla sieci WWW i przeglądarek internetowych.</Text>
                <Text style={styles.dmlH2}>Witryna internetowa</Text>
                <Text>Witryna internetowa to zbiór powiązanych stron internetowych, w tym treści multimedialnych zwykle
                    identyfikowanych za pomocą wspólnej nazwy domeny i publikowanych na co najmniej jednym serwerze
                    internetowym.</Text>
                <Text style={styles.dmlH2}>Serwer internetowy</Text>
                <Text>Serwer internetowy to system dostarczający treści lub usługi użytkownikom końcowym przez
                    Internet.</Text>
                <Text style={styles.dmlH2}>Wyszukiwarka</Text>
                <Text>Wyszukiwarka internetowa to oprogramowanie przeznaczone do wyszukiwania informacji w sieci
                    WWW.</Text>
                <Text style={styles.dmlH2}>Wyszukiwanie w Internecie</Text>
                <Text>Wyszukiwanie w Internecie to proces wyszukiwania informacji w sieci za pomocą wyszukiwarek takich
                    jak Google lub Internet Explorer.</Text>
                <Text style={styles.dmlH2}>System e-mail</Text>
                <Text>E-mail to system wysyłania pisemnych wiadomości w formie elektronicznej z jednego komputera na
                    drugi.</Text>
                <Text style={styles.dmlH2}>Co to jest adres e-mail?</Text>
                <Text>Adres e-mail to adres elektronicznej skrzynki pocztowej, która może odbierać (i wysyłać)
                    wiadomości e-mail w sieci.</Text>
                <Text style={styles.dmlH2}>Co znaczy@?</Text>
                <Text>Jest symbolem w adresie e-mail, który oddziela nazwę użytkownika od adresu internetowego
                    użytkownika.</Text>
                <Text style={styles.dmlH2}>Co to jest wirus e-mail?</Text>
                <Text>Wirus e-mail to wirus wysyłany z wiadomością e-mail lub dołączony do niej. Podczas gdy wiele
                    różnych typów wirusów e-mailowych działa na różne sposoby, istnieje również wiele metod stosowanych
                    w celu przeciwdziałania cyberatakom.</Text>
                <Text style={styles.dmlH2}>Co to jest spam e-mailowy?</Text>
                <Text>Spam dotyczy niechcianych masowych wiadomości e-mail (wiadomości-śmieci). Zwykle oznacza to, że
                    wiadomość z treścią reklamową lub jakąś nieistotną jest wysyłana do wielu odbiorców, którzy nigdy o
                    nią nie poprosili.</Text>
            </View>
        )
    }
}
