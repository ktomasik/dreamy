import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text style={styles.dmlH3}>Jeśli nie jesteś poszukującym pracy POMOC PAŃSTWA ACCRE</Text>
                <Text style={styles.dmlH3}>Krok 2:</Text>
                <Text>CFE wystawia pokwitowanie rejestracji twojej aplikacji ACCRE, informuje organizacje społeczne o
                    Twojej prośbie i wysyła żądanie do właściwego URSSAF w ciągu 24 godzin. Zasady URSSAF na żądanie w
                    ciągu jednego miesiąca.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
