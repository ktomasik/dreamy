import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Gdzie znaleźć pożyczki dla małych firm prowadzonych przez kobiety?</Text>
                <Text style={styles.dmlH2}>Dla Turcji:</Text>
                <Text style={styles.dmlH3}>Banki</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Pakiet wspierający przedsiębiorców',
                                val2: 'https://www.akbank.com/tr-tr/urunler/Sayfalar/isim-icin-girisimci-destek-paketi.aspx'
                            }
                        },
                        {
                            key: {
                                val1: 'Pakiet wspierający przedsiębiorczość dla kobiet',
                                val2: 'https://www.garanti.com.tr/tr/kobi/kobilere_ozel/kadin-girisimci-bankaciligi/kadin-girisimci-kredi-destek-paketi.page'
                            }
                        },
                        {
                            key: {
                                val1: 'Kredyt udzielany przez TEB bank, wspierający kobiety',
                                val2: 'https://www.teb.com.tr/kadin-patronum/kgf-kadin-patron-destek-kredisi/'
                            }
                        },
                        {
                            key: {
                                val1: 'Kredyt dla kobiet prowadzących działalność',
                                val2: 'https://www.isbank.com.tr/TR/kampanyalar/kampanya-ayrintilari/Sayfalar/kampanya-ayrintilari.aspx?CampaignName=kadin-girisimci-kredisi-kampanyasi&IdCampaign=Mzk2-ISB'
                            }
                        },
                        {
                            key: {
                                val1: 'Ułatwia zrobienie pierwszych kroków kobietom przedsiębiorcom',
                                val2: 'https://www.halkbankkobi.com.tr/NewsDetail/Kadin-Girisimcilere-Ilk-Adim-Kredisi/215'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
