import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym jest logo marki?</Text>
                <Text>Logo jest znakiem graficznym, godłem lub symbolem używanym do wspierania i promowania
                    rozpoznawalności. Może mieć wzór abstrakcyjny lub graficzny lub zawierać tekst nazwy, którą
                    reprezentuje w logotypie lub znaku słownym.</Text>
                <Text>Marka to każda interakcja i praktyka marketingowa nazwy lub projektu, który identyfikuje i
                    różnicuje jedną firmę, produkt lub usługę od innej.</Text>

                <Text style={styles.dmlH2}>Jak wybrać odpowiedni projekt logo?</Text>
                <Text>Podejmując decyzję o projekcie logo, musisz pamiętać o kilku rzeczach, które pomogą nadać Twojej
                    firmie tożsamość, na jaką zasługuje. Jeśli chcesz mieć kreatywne logo dla swojej firmy, spróbuj
                    najpierw znaleźć inspirację logo, patrząc na koncepcje odnoszących sukcesy marek.</Text>

                <Text style={styles.dmlH2}>Strategia cenowa dla handlu elektronicznego (ustalanie ceny za
                    produkt)</Text>
                <Text>W środowisku zakupów online najlepiej jest zobaczyć cenę razem z produktem. Klientom będzie
                    łatwiej kupić produkt, gdy zobaczy dobry projekt i jego koszt na platformie.</Text>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img16.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={styles.dmlH2}>Różne rodzaje strategii ustalania cen</Text>
                <Text style={styles.dmlH3}>Oparte na kosztach ceny e-commerce</Text>
                <Text>Oparta na kosztach wycena może być najpopularniejszym modelem cenowym w branży detalicznej,
                    zapewniającym minimalny zwrot z każdego sprzedanego produktu.</Text>
                <Text style={styles.dmlH3}>Konkurencyjne ceny e-commerce</Text>
                <Text>Dzięki strategii cenowej opartej na konkurencji, po prostu monitorujesz, ile płacą twoi
                    bezpośredni konkurenci za konkretny produkt, i ustalasz cenę w stosunku do ich ceny.</Text>
                <Text style={styles.dmlH3}>Ceny oparte na wartości e-commerce</Text>
                <Text>Jeśli koncentrujesz się na wartości, którą możesz dostarczyć klientowi, ustalając ceny w oparciu o
                    to, jak postrzegasz kupującego - w podanym segmencie branży – ile zapłaci za konkretny produkt w
                    określonym czasie, zastosuj podejście oparte na wartości lub zoptymalizowanej wartości do wyceny
                    e-commerce (Roggio, 2017).</Text>
            </View>
        )
    }
}
