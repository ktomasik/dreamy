import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";


const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img9.png')
    }
}];

export default class M6S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Jak utworzyć fakturę w Dokumentach Google</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 1:</Text> Wyszukuj w
                    Google "Szablony faktur Google Docs", aby uzyskać dostęp do menu Szablony, otworzy się następna
                    strona</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
