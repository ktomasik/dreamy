import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Tworzenie strony Facebook dla firm</Text>
                <Text>Aby sprzedawać na Facebooku, najpierw trzeba mieć stronę na Facebooku (Zobacz Moduł 2).
                    {"\n\n"}Wewnątrz Twojej strony na Facebooku użytkownicy zobaczą kartę sklepu i będą mogli uzyskać
                    dostęp do Twoich produktów.
                </Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 1:</Text> zaloguj
                    się do swojego profilu na Facebooku
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
