import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym jest marka?</Text>
                <Text>Marka to nazwa, logo, znak lub kształt, które pojedynczo lub w połączeniu umożliwiają konsumentowi
                    odróżnienie produktu lub usługi od innych na rynku (Shim 2009).</Text>
                <Text style={styles.dmlH2}>Czym jest nazwa marki?</Text>
                <Text>Nazwa marki to słowo lub liczby w niektórych kombinacjach, które można wyrazić słowami (Shim
                    2009). Przykładowo: 3M, Google, XEROX itp.</Text>
                <Text style={styles.dmlH2}>Jaka jest różnica między marką a znakiem towarowym?</Text>
                <Text>Nazwa biznesu / handlu / firmy to sposób na identyfikację firmy, podmiotu lub osoby. Jest to
                    oficjalna nazwa, pod którą dany podmiot lub osoba fizyczna decyduje się na prowadzenie działalności
                    gospodarczej (Cameron 2017).
                    {"\n\n"}Znakiem towarowym jest: słowo, fraza, logo, symbol, wzór, kolor lub połączenie jednego lub
                    więcej z tych elementów, które odróżniają produkty / usługi jednej firmy od usług innej (Cameron
                    2017, Shravani 2017).
                    {"\n\n"}Różnice między marką a znakiem towarowym są następujące (Anonymous 2018a, Shravani 2017):
                </Text>
                <FlatList
                    data={[
                        {key: 'Marki i znaki towarowe są cennymi zasobami dla firmy. Często marka lub znak towarowy stają się synonimem produktu. Błędem jest używanie zamiennie terminów "znak towarowy" i "marka", ponieważ mają one bardzo ważne różnice.'},
                        {key: 'Podczas gdy marka reprezentuje reputację i biznes w oczach opinii publicznej, znak towarowy prawnie chroni te aspekty marki, które są unikalne i specyficzne dla firmy.'},
                        {key: 'Znak towarowy jest prawną ochroną marki przyznawaną przez Znak towarowy i Urząd Patentowy.'},
                        {key: 'Wszystkie znaki towarowe są markami, a nie wszystkie marki są znakami towarowymi.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}><Text style={{fontWeight: 'bold'}}>Przykład:</Text>
                    {"\n"}Coco Chanel jest doskonałym przykładem nazwy, która jest znakiem towarowym. Słynna
                    projektantka Coco Chanel zbudowała swoje udane imperium mody, używając swojego imienia. Ludzie
                    wiedzieli, że jeśli kupią produkt Coco Chanel, otrzymają wysokiej jakości produkt. Dzięki reputacji
                    jej imię stało się rozpoznawalne na całym świecie. Nazwa Coco Chanel jest uznawana za znak towarowy,
                    nazwisko Chanel jest uważane za markę (Husbey 2016).</Text>
            </View>
        )
    }
}
