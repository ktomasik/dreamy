import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Rejestracja do stowarzyszeń zawodowych</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>Dla
                    TURCJI</Text>
                <FlatList
                    data={[
                        {key: 'KOSGEB (Organizacja Rozwoju Małych i Średnich Przedsiębiorstw)'},
                        {key: 'Generalna Dyrekcja Statusu Kobiet (KSGM)'},
                        {key: 'Tureckie Stowarzyszenie Biznesu (İŞKUR)'},
                        {key: 'Turcecki Związek Izb i Giełd Wspólnoty (TOBB) Wsparcie Ministerstwa '},
                        {key: 'Republiki Turcji i Polityki Społecznej'},
                        {key: 'Turecki program mikrofinansowania Grameen'},
                        {key: 'Kredyty bankowe dla kobiet'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla SŁOWENII</Text>
                <Text style={{fontWeight: 'bold'}}>Wsparcie pozafinansowe państwa</Text>
                <FlatList
                    data={[
                        {key: 'Punkty VEM'},
                        {key: 'Inkubatory przedsiębiorczości'},
                        {key: 'Inkubatory uniwersyteckie'},
                        {key: 'Parki technologiczne'},
                        {key: 'Inicjatywa start-up Słowenia'},
                        {key: 'Europejska sieć przedsiębiorstw'},
                        {key: 'SPIRIT Słowenia'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Wsparcie finansowe w Słowenii</Text>
                <FlatList
                    data={[
                        {key: 'Słoweński Fundusz Przedsiębiorczości'},
                        {key: 'Służba zatrudnienia w Słowenii'},
                        {key: 'Słoweński Fundusz Rozwoju Regionalnego'},
                        {key: 'Kredyty bankowe'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Inne wsparcie</Text>
                <FlatList
                    data={[
                        {key: 'Izba Przemysłowo-Handlowa'},
                        {key: 'Izba Rzemiosła i Małych Przedsiębiorstw w Słowenii'},
                        {key: 'Biznesowi Aniołowie w Słowenii'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla POLSKI</Text>
                <FlatList
                    data={[
                        {key: 'Fundusze Unii Europejskiej dla kobiet-przedsiębiorców (organizacje pozarządowe, organy publiczne itp.)'},
                        {key: 'Europejski Fundusz Społeczny (POWER - Program Operacyjny Wiedza Edukacja Rozwój)'},
                        {key: 'Źródła spoza UE (rządowe, prywatne itp.)'},
                        {key: 'Polska Agencja Rozwoju Przedsiębiorczości'},
                        {key: 'Fundusz pożyczkowy dla kobiet'},
                        {key: 'Anioły biznesu'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla GRECJI</Text>
                <FlatList
                    data={[
                        {key: 'Kapitał zalążkowy (niewielkie fundusze dla określonej populacji, np. młodych ludzi, bezrobotnych)'},
                        {key: 'Program OAED (dla ogółu społeczeństwa)'},
                        {key: 'Otwarty fundusz (dla sektora prywatnego)'},
                        {key: 'Kredyty bankowe'},
                        {key: 'Umowa partnerska (PA) 2014-2020 (ESPA)'},
                        {key: 'Społeczność grecka za granicą'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla FRANCJI</Text>
                <FlatList
                    data={[
                        {key: 'CFE'},
                        {key: 'Rejestr handlu i firm (RFS)'},
                        {key: 'ACCRE - to narzędzie zostało stworzone, aby pomóc osobom poszukującym pracy i ułatwić tworzenie przez nich firm.'},
                        {key: 'BPI France (publiczny bank inwestycyjny)'},
                        {key: 'PRI (regionalne partnerstwo innowacji)'},
                        {key: 'Anioly biznesu'},
                        {key: 'Entreprendre au Feminin'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
