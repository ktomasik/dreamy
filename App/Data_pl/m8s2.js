import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Metody wysyłki</Text>
                <Text style={{marginTop: 10}}>W zależności od obszaru, metody transportu są podzielone na dwie grupy
                    jako transport krajowy i międzynarodowy. Wysyłka krajowa obejmuje transport wewnątrz kraju, a
                    dokumentacja odpowiada przepisom krajowym. Firmy cargo przyjmują paczkę i przekazują ją klientowi.
                    {"\n\n"}W zależności od rodzaju transportu dostępne są: transport lotniczy, transport morski i
                    drogowy. Transport lotniczy i transport morski jest powszechny w handlu zagranicznym na dużą skalę.
                    Transport drogowy jest najczęstszym typem transportu między krajami, które mają granice lądowe. W
                    przypadku materiałów rzemieślniczych w małych ilościach, powiązane firmy przewozowe mogą przesyłać
                    towary drogą lądową. Potrzebne mogą być listy przewozowe i faktury. W niektórych przypadkach paczki
                    wymagają niestandardowych deklaracji, mogą być potrzebne dodatkowe dokumenty. Firmy przewozowe mogą
                    wydawać zalecenia i pomagać w wyborze niezbędnych dokumentów.
                </Text>
                <Text style={styles.dmlH3}>Oferta bezpłatnej wysyłki</Text>
                <Text>Bezpłatną wysyłkę można zdefiniować dla minimalnej wartości zamówienia lub minimalnej liczbie
                    pozycji. Oferowanie bezpłatnej wysyłki to jeden ze sposobów na zwrócenie uwagi klienta. Ogólnie
                    rzecz biorąc, oferowanie bezpłatnej wysyłki przy sprzedaży zapewnia znaczną przewagę nad
                    konkurentami w tej samej dziedzinie. (Anonimowy 2018a).</Text>
                <Text style={styles.dmlH3}>Niższe stawki za wysyłkę</Text>
                <Text>Umowy z firmami przewozowymi mogą pomóc obniżyć stawki za wysyłkę ze względu na ciągłość
                    współpracy. Wybór najlepszego operatora indywidualnie dla każdego zamówienia również może spowodować
                    niższe stawki. Chociaż może się wydawać, że czas i nakład pracy wymagany do każdorazowej oceny
                    wyboru wysyłki za każdym razem, gdy otrzymujesz zamówienie, różni przewoźnicy mogą oferować
                    drastycznie różne stawki w zależności od wagi, wymiarów i miejsca docelowego Twojej przesyłki.
                    {"\n\n"}Negocjacje z różnymi firmami przewozowymi mogą motywować do oferowania bardziej
                    konkurencyjnych cen. Krótki przegląd przyszłych możliwości pracy i celów na przyszłość może być
                    skuteczny w przypadku stawek. Z drugiej strony, ponieważ stawki za wysyłkę są określane przez
                    obliczenia masy i wymiarów, inną rzeczą, która pomaga obniżyć koszty wysyłki, jest użycie opakowań
                    dostarczanych przez firmy transportowe.
                </Text>
            </View>
        )
    }
}
