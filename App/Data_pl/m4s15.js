import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M4S15 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Anonymous, 2018a. 22.09.2018.Trademark vs. Brand: Everything You Need to Know',
                                val2: 'https://www.upcounsel.com/trademark-vs-brand'
                            }
                        },
                        {
                            key: {
                                val1: 'Anonymous, 2018b. 22.09.2018.',
                                val2: 'https://www.smallbusiness.wa.gov.au/business-advice/starting-your-business/business-names'
                            }
                        },
                        {
                            key: {
                                val1: 'Anonymous, 2018c. 22.09.2018.',
                                val2: 'https://www.slideshare.net/TahminaSharmin/mc-donalds-strategic-management-analysis'
                            }
                        },
                        {
                            key: {
                                val1: 'Anonymous, 2018d. 14.08.2018. Business name, trading names& legal names.',
                                val2: 'https://www.business.gov.au/registrations/register-a-business-name/business-name-trading-names-legal-names'
                            }
                        },
                        {
                            key: {
                                val1: 'Anonymous, 2018e. 22.09.2018. What is Brand Registration?',
                                val2: 'http://www.uktrademarkregistration.co.uk/brand-registration/What-is-Brand-Registration.aspx'
                            }
                        },
                        {
                            key: {
                                val1: 'Anonymous, 2018f. 22.09.2018.',
                                val2: 'https://en.wikipedia.org/wiki/Taxpayer_Identification_Number'
                            }
                        },
                        {
                            key: {
                                val1: 'Cameron, A. 2017. Business Name vs. Trading Name, Do You Know the Difference?.',
                                val2: 'https://www.patriotsoftware.com/accounting/training/blog/business-name-or-trade-name-difference/'
                            }
                        },
                        {
                            key: {
                                val1: 'Dora, E.K., Ozer, C. and Korkmaz, T. 2018. Digital business in Turkey: overview.',
                                val2: 'https://uk.practicallaw.thomsonreuters.com/5-618-1186?transitionType=Default&contextData=(sc.Default)&firstPage=true&bhcp=1'
                            }
                        },
                        {
                            key: {
                                val1: 'Fishman, S. 2015. What’s in a Name? Choosing and Protecting Your Business Name. ',
                                val2: 'https://www.mileiq.com/blog/whats-in-a-name-choosing-and-protecting-your-business-name/'
                            }
                        },
                        {
                            key: {
                                val1: 'Forbes Agency Council. 2017. 24.10.2017.',
                                val2: 'https://www.forbes.com/sites/forbesagencycouncil/2017/10/24/18-steps-to-take-before-you-launch-a-product-or-service/#5772b0bf19cf'
                            }
                        },
                        {
                            key: {
                                val1: 'Hubsey, A. 2016. Unit 9-Commercial Aspects of Engineering Organizations.',
                                val2: 'https://www.coursehero.com/file/14108141/Unit-9-Commercial-Aspects-of-Engineering-Organizations/'
                            }
                        },
                        {
                            key: {
                                val1: 'National Report Turkey 2018. National Report created by Gazi University Partner in Dreamy M-Learning Project.',
                                val2: ''
                            }
                        },
                        {
                            key: {
                                val1: 'National Report Slovenia 2018. National Report created by Ljubljana University Partner in Dreamy M-Learning Project.',
                                val2: ''
                            }
                        },
                        {
                            key: {
                                val1: 'National Report Poland 2018. National Report created by Danmar Computers Partner in Dreamy M-Learning Project. ',
                                val2: ''
                            }
                        },
                        {
                            key: {
                                val1: 'National Report Greece 2018. National Report created by IDEC Partner in Dreamy M-Learning Project.',
                                val2: ''
                            }
                        },
                        {
                            key: {
                                val1: 'National Report France 2018. National Report created by GUIMEL Partner in Dreamy M-Learning Project.',
                                val2: ''
                            }
                        },
                        {
                            key: {
                                val1: 'Shravani, P.T. 2017. Understanding the Difference Between Company Name and Trademark.',
                                val2: 'http://www.iamwire.com/2017/08/difference-company-name-trademark/156468'
                            }
                        },
                        {
                            key: {
                                val1: 'Shim, J.K. 2009. The Pocket MBA: Concepts and Strategies. Delta Publishing Company, Los Alamitos, California.',
                                val2: ''
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
