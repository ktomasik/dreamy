import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S48 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Korzystanie z serwisów społecznościowych</Text>
                <Text style={{marginTop: 10}}>Informowanie ludzi o postępie lub produktach Twojej firmy jest bardzo
                    ważną pracą. Wiele osób decyduje się na to przez kilka serwisów społecznościowych lub platform.
                    Zdecydowanie najpopularniejszym jest Facebook, którego nie trzeba przedstawiać w dzisiejszych
                    czasach ...</Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 300, height: 300}}
                        source={require('../Images/images/1016.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}

