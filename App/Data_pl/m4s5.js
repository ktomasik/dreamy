import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Czym jest numer identyfikacji podatkowej?</Text>
                <Text>Numer identyfikacji podatkowej (TIN) to numer identyfikacyjny używany do celów podatkowych w danym
                    kraju (Anonimous 2018f).
                    {"\n"}Wszystkie osoby prawne, podmioty nieposiadające osobowości prawnej i osoby fizyczne muszą
                    uzyskać numer identyfikacji podatkowej.
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla TURCJI</Text>
                <Text>Musi istnieć w systemie e-Deklaracji. W rzeczywistości numer identyfikacyjny osoby jest używany
                    jako numer identyfikacji podatkowej (TIN).
                    {"\n"}Identyfikator musi być potwierdzony notarialnie lub zatwierdzony przez urzędników podatkowych.
                    (National Report Turkey, 2018).</Text>
                <FlatList
                    data={[
                        {key: 'TIN musi być uzyskany w celu podjęcia działalności zawodowej lub handlowej w Turcji.'},
                        {key: 'Od 1 lipca 2006 r. jako numer identyfikacyjny dla obywateli tureckich stosowany jest krajowy numer identyfikacyjny, a wszystkie numery NIP dla obywateli zostały dopasowane do ich krajowego numeru identyfikacyjnego w systemie baz danych podatkowych.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla SŁOWENII</Text>
                <Text>Po wpisaniu wymaganych danych do rejestru podatkowego.
                    {"\n"}Mogą być wymagane dodatkowe znaki alfanumeryczne lub numeryczne do numeru identyfikacji
                    podatkowej.
                    {"\n"}Jeżeli dana osoba wpisuje się do rejestru podatkowego jako osobisty przedsiębiorca,
                    administracja finansowa nie przydziela nowego NIP (National Report Slovenia, 2018).
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla POLSKI</Text>
                <Text>Każda osoba dorosła musi mieć numer identyfikacji podatkowej na potrzeby urzędu skarbowego.
                    {"\n"}Jeżeli osoba dorosła staje się indywidualnym przedsiębiorcą, ma ten sam numer identyfikacji
                    podatkowej, którego należy użyć (National Report Poland, 2018).</Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla GRECJI</Text>
                <Text>Każda osoba dorosła musi mieć numer identyfikacji podatkowej.
                    {"\n"}W przypadku, gdy osoba dorosła chce być indywidualnym przedsiębiorcą, musi zarejestrować firmę
                    w organie podatkowym.
                    {"\n"}Za pomocą tego upoważnienia może ona używać osobistego numeru identyfikacji podatkowej w
                    celach biznesowych (National Report Greece, 2018).
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla FRANCJI</Text>
                <Text>Francuskie organy podatkowe wystawiają numer identyfikacji podatkowej wszystkim osobom fizycznym,
                    które mają obowiązek zgłaszania podatków we Francji.
                    {"\n"}NIP jest przypisywany, gdy osoba rejestruje się w bazach danych organów podatkowych. Jest on
                    przypisany do wszystkich osób utworzonych w systemie rejestracji Dyrekcji Generalnej ds. Finansów
                    publicznych (DGFiP) (referencyjna PERS) dla wszystkich podatków. Jest to unikalny, nieistotny,
                    niezawodny i stały numer identyfikacyjny.
                    {"\n"}Ten numer podatkowy jest wskazany we wcześniej wydrukowanym formularzu deklaracji podatku
                    dochodowego oraz w informacjach dotyczących podatku dochodowego i podatku od nieruchomości. NIP musi
                    uzyskać zleceniobiorca, lub posiadacz aktywów lub beneficjent dochodu (National Report France,
                    2018).
                </Text>
            </View>
        )
    }
}
