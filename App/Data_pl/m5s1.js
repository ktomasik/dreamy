import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Wprowadzenie</Text>
                <Text style={{marginTop: 10}}>Moduł ten odnosi się do powszechnie wykonywanych w praktyce procedur
                    księgowych dla początkujących kobiet-przedsiębiorców. W szczególności odnosi się do otwierania
                    rachunków bankowych, ich rodzajów, metod transakcji bankowych i informacji o handlu elektronicznym i
                    systemie płatności online (lub za pośrednictwem karty kredytowej). Zawiera również wskazówki
                    dotyczące ich realizacji.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 280, height: 340}}
                        source={require('../Images/images/6931.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
