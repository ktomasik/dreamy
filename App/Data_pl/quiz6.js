import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz6": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Z Google play",
                    "option2": "b) Z App store",
                    "option3": "c) Z żadnego z wymienionych",
                    "option4": "d) Z obu z wymienionych"
                },
                "question": "Skąd mogę pobrać szablon faktury na smartfon Apple?"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "App Store może być użyty to tworzenia faktur dla telefonów z systemem android."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Fresh books",
                    "option2": "b) Moon Invoice",
                    "option3": "c) Wave",
                    "option4": "d) Pinterest"
                },
                "question": "Która z wymienionych aplikacji pozwala na tworzenie faktur zarówno dla smartfonów Apple jak i android."
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Umieszczanie logo na fakturze nie jest konieczne, ale lepiej to zrobić."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Nazwa i tytuł",
                    "option2": "b) Dane kontaktowe, adres, telefon itp. ",
                    "option3": "c) Podatek",
                    "option4": "d) Wszystkie wyżej wymienione"
                },
                "question": "Która z informacji musi się pojawić na fakturze?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Tożsamości sprzedawcy",
                    "option2": "b) Konta w social mediach ",
                    "option3": "c) Podatku VAT",
                    "options": "d) Informacji kontaktowych"
                },
                "question": "Typowa umowa zakupu online nie wymaga podawania:"
            },
            "question7": {
                "correctoption": "option2",
                "options": {
                    "option1": "a)Prawda",
                    "option2": "b)Fałsz"
                },
                "question": "Handlowiec nie ponosi odpowiedzialności za produkty sprzedawane pod nazwą swojej firmy."
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Nie warto ubezpieczać swoich produktów handlowych."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Miejscem do klasyfikacji dokumentów",
                    "option2": "b) Bezpłatną i wygodną platformą do elektrycznego podpisu, której można używać przy pomocy smartfonów",
                    "option3": "c) Dokumentem, który podpisuje się przed każdą sprzedażą",
                    "option4": "d) Żadnym z powyższych"
                },
                "question": "Czym jest DocuSign?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Cash board",
                    "option2": "b) DocuSign",
                    "option3": "c) DropBox",
                    "option4": "d) Pinterest"
                },
                "question": "_______ oferuje szablon do pobrania, który można otworzyć w programie Microsoft Word."
            }
        }
    }
};

export default class M6Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz6;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>

                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
