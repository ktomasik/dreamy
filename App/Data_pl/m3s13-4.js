import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen10 = [{
    url: '',
    props: {
        source: require('../Images/m3/img59.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m3/img60.png')
    }
}];

export default class M3S13_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Jeśli chcesz skonfigurować stronę, postępuj zgodnie z instrukcjami</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen10}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 4: Naciśnij "Zarejestruj się" i rozpocznij demonstrację
                    swojej strony internetowej, najpierw odpowiedz na pytania na ekranie, a następnie naciśnij przycisk
                    "Rozpocznij teraz" poniżej:</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img59.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img60.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
