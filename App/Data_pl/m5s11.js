import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konto biznesowe PayPal</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie z instrukcjami poniżej, aby skonfigurować konto biznesowe
                    PayPal.
                    {"\n\n"}Postępuj zgodnie z instrukcjami poniżej, aby dokończyć konfigurację konta. Musisz
                    potwierdzić swój
                    adres e-mail, zweryfikować konto PayPal i wybrać rozwiązanie płatności PayPal.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 1</Text>
                <Text>Firma PayPal wysłała Ci wiadomość e-mail, gdy założyłeś konto firmowe PayPal. Kliknij link w
                    wiadomości e-mail, aby potwierdzić swój adres e-mail. Jeśli nie możesz znaleźć wiadomości e-mail,
                    zaloguj się na swoje konto PayPal i kliknij Potwierdź adres e-mail na "liście spraw do dokończenia"
                    pod ikoną profilu działalności.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 2</Text>
                <Text>Po zweryfikowaniu nie tylko zyskasz większą wiarygodność u sprzedawców i kupujących, ale także
                    usuniesz limit wypłat na swoim koncie. Istnieją dwa sposoby uzyskania weryfikacji:</Text>
                <FlatList
                    data={[
                        {key: '1. Potwierdź swoją kartę UnionPay (natychmiastowa weryfikacja)\n Dodaj lub przejrzyj szczegóły swojej karty UnionPay. Potwierdź kartę, upoważniając UnionPay do wysłania SMS-a z kodem weryfikacyjnym. Wprowadź kod, aby natychmiast potwierdzić swoją kartę.'},
                        {key: '2. Potwierdź swoją kartę kredytową\n Dodaj lub sprawdź swoją kartę Visa lub MasterCard na swoim koncie PayPal i przejdź do potwierdzenia karty. Generuje to 4-cyfrowy kod, który zostanie odzwierciedlony w wyciągu z karty kredytowej w ciągu 2-3 dni roboczych. Zaloguj się na swoje konto PayPal, wprowadź kod, aby zakończyć proces weryfikacji.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 3</Text>
                <Text>Wybierz rozwiązanie płatności dostosowane do potrzeb Twojej firmy.</Text>
            </View>
        )
    }
}
