import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S15 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Dotacje dla małych firm prowadzonych przez kobiety</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text style={styles.dmlH3}>PRI (regionalne partnerstwa innowacyjne)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: '',
                                val2: 'https://www.bpifrance.fr/Toutes-nos-solutions/Aides-concours-et-labels/Aides-a-l-innovation-projets-individuels/PRI-Faisabilite'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text>Ten instrument jest otwarty dla powstających innowacyjnych MŚP, których priorytetowym celem nie
                    jest innowacyjność.</Text>
                <Text>Jest prowadzony we współpracy tylko z 5 regionami: Grand Est (Alzacja, Szampania-Ardeny,
                    Lotaryngia), Hauts de France (Nord Pas de Calais Pikardia), Akwitania / Poitou Charentes, Kraj
                    Loary, PACA.</Text>
                <Text>Wybierane są najbardziej innowacyjne projekty, którym przyznany zostaje grant w wysokości od 100
                    000 do 200 000 EUR na projekt. Ta pomoc pozwala pokryć wydatki związane ze wstępnymi studiami i
                    realizacją projektu. Jest wypłacana w 2 ratach (70% i 30%).</Text>
                <Text>Projekt MŚP musi trwać maksymalnie 12 miesięcy.</Text>
            </View>
        )
    }
}
