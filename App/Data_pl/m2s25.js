import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Instagramie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 12: Zaleca się także obserwowanie znajomych ze swoich
                    kontaktów. W tym celu należy zezwolić aplikacji Instagram na dostęp do informacji
                    kontaktowych..</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img46.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
