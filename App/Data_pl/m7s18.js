import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S18 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Ulgi podatkowe dla kobiet rozpoczynających działalność</Text>
                <Text style={styles.dmlH2}>Dla Turcji:</Text>
                <Text>Zgodnie z art. 9/6 ustawy o podatku dochodowym ci, którzy wytwarzają i sprzedają ręcznie robione
                    produkty w domach, w których mieszkają, są zwolnieni z podatku (ustawa nr 193, Dziennik Urzędowy nr
                    28366 z 27 lipca 2012 r.).</Text>
                <Text>Aby zwolnić się z podatku, konieczne jest uzyskanie "świadectwa zwolnienia dla jednostek" (Esnaf
                    Vergi Muafiyeti Belgesi)</Text>
                <Text>Aby uzyskać certyfikat zwolnienia dla jednostki, stosuje się następującą procedurę:</Text>
                <Text>Krok 1: Osoby pragnące uzyskać zaświadczenie muszą zwrócić się z petycjś do urzędu podatkowego w
                    miejscu, w którym znajduje się ich miejsce zamieszkania.</Text>
                <Text>Krok 2: Następujące informacje powinny być zapisane jako rodzaj rowadzonej działalności na
                    świadectwie zwolnienia podatkowego: Produkcja i sprzedaż wyrobów ręcznie robionych we własnym domu
                    (artykuł ITL: 9/6)</Text>
                <Text>Krok 3: Adres domowy jest wyświetlany jako adres służbowy</Text>
                <Text>Krok 4: Jeśli zostanie zaakceptowane spełnienie warunków określonych w art. 9 ustawy o podatku
                    dochodowym, urząd skarbowy wyda certyfikat</Text>
                <Text>Krok 5: Posiadacz certyfikatu nie będzie obciążany żadną opłatą</Text>
                <Text>Krok 6: Świadectwo zwolnienia z podatku od pracy jest ważne przez trzy lata od daty wydania i
                    możliwe jest uzyskanie nowych dokumentów po złożeniu wniosku do urzędu skarbowego na koniec tego
                    okresu.</Text>
            </View>
        )
    }
}
