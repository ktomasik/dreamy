import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S19 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Instagramie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 5: Kliknij Otwórz, aby otworzyć program Instagram.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img32.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
