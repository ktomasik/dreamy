import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {Button} from 'react-native-elements';
import {quizStyles} from '../Containers/Styles/general';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz3": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz",
                },
                "question": "E-commerce zaczął wkraczać w nasze życie od lat 50. XX wieku, kiedy zaczęto używać komputerów."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz",
                },
                "question": "Konto Instagram Business nie udostępnia informacji o marce, a także nie jest narzędziem analitycznym do mierzenia sukcesu."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Etsy nie pobiera opłat za stworzenie sklepu",
                    "option2": "b) Bonanza pozwala na wysyłanie przedmiotów za darmo",
                    "option3": "c) W Zibbet, możesz sprzedawać pojazdy elektryczne",
                    "option4": "d) Otwieranie sklepu w DaWanda jest bezpłatne"
                },
                "question": "Które z poniższych stwierdzeń jest fałszywe?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz",
                },
                "question": "E-commerce to kupowanie i sprzedawanie towarów i usług w platformie online."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Ludzki umysł usiłuje ucieleśnić coś co jest abstrakcyjne",
                    "option2": "b) Zdjęcia produktów w wysokiej jakości mają kluczowy wpływ na sprzedaż sklepu",
                    "option3": "c) Badania pokazują, że klienci sklepu przykuwają uwagę najpierw do wizualnych elementów",
                    "option4": "d) Nie ma powiązania pomiędzy marką a logo."
                },
                "question": "Które z poniższych stwierdzeń na temat znaczenia zdjęć produktów jest fałszywe?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz",
                },
                "question": "Wyszukiwanie hasztagów na Instagramie pomaga produktom częściej wyświetlać się na stronie głównej."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Skoncentruj się na opowiedzeniu historii aby zaangażować klientów",
                    "option2": "b) Utwórz konto na Instagramie",
                    "option3": "c) Utwórz fałszywe konta z których będziesz wychwalał produkt",
                    "option4": "d) Przekształć oryginalny obraz za pomocą edycji ",
                },
                "question": "Aby zwiększyć sprzedaż na Instagramie:"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Sprzedając rękodzieło online, wykorzystanie prawdziwego zdjęcia produktu nie jest tak ważne jak cena."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Prawda",
                    "option2": "b) Fałsz"
                },
                "question": "Twoje logo powinno odzwierciedlać twój produkt i filozofię biznesu."
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Ponad 60% kupujących online na całym świecie uważa, że cena w handle internetowym jest pierwszym kryterium wpływającym na decyzje zakupowe",
                    "option2": "b) Najważniejszą zaletą strategii cenowej w handle internetowym jest prostota",
                    "option3": "c) Konkurencja cenowa w e-commerce może doprowadzić do tego, że sprzedasz swój produkt taniej, niż zakładałeś",
                    "option4": "d) Handel internetowy oparty na cenie może przynieść krótkoterminowe zyski",
                },
                "question": "Które z poniższych stwierdzeń jest fałszywe?"
            }
        }
    }
};

export default class M3Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz3;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
