import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";

// Styles

export default class M4S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Licencje i zezwolenia dla nowej firmy</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>Dla
                    TURCJI</Text>
                <Text>Jeśli kobiety chcą zwolnić się z podatku, potrzebna jest rezygnacja z "Craft Certificate of
                    Exemption" (Esnaf Vergi Muafiyet Belgesi). Jeśli kobiety chcą być podatnikiem dla przyszłego planu
                    rozwoju, obowiązuje System e-Deklaracja. System ten może być stosowany za pomocą platformy
                    elektronicznej (online). Zapisy dokumentów otrzymanych i przekazanych przez podatników są
                    przechowywane w izbach rzemieśniczych, do których należą podatnicy (National Report Turkey, 2018).
                    {"\n\n"}"Craft Certificate of Exemption" (Esnaf Vergi Muafiyet Belgesi) (National Report Turkey,
                    2018);
                </Text>
                <FlatList
                    data={[
                        {key: '1. Po uzyskaniu tego certyfikatu należy dokonać rejestracji w stowarzyszeniu handlowym stowarzyszonym z Konfederacją Tureckich Handlowców i Rzemieślników.'},
                        {key: '2. W celu uzyskania zezwolenia na pracę należy złożyć wniosek do gminy wraz z petycją.'},
                        {key: '3. W przypadku biznesu domowego wszyscy właściciele mieszkań muszą zawrzeć umowę  notarialną.'},
                        {key: '4. Faktura i deklaracja są obowiązkowe.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15, fontWeight: 'bold'}}>Podatnicy (National Report Turkey, 2018):</Text>
                <FlatList
                    data={[
                        {key: '1. Należy zastosować system e-Decleration.'},
                        {key: '2. Po zatwierdzeniu podatnika, powinien on zostać zarejestrowany w stowarzyszeniu stowarzyszonym z Konfederacją Tureckich Handlowców i Rzemieślników.'},
                        {key: '3. Należy złożyć wniosek do gminy z wnioskiem o pozwolenie na pracę.'},
                        {key: '4. Podatnicy otrzymają dokumenty od izb lub stowarzyszeń.'},
                        {key: '5. Faktura i rachunek są obowiązkowe.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla SŁOWENI</Text>
                <Text>Do założenia firmy wymagane jest wypełnienie wniosku rejestracyjnego CEIDG-1. W tym formularzu
                    konieczne jest:</Text>
                <FlatList
                    data={[
                        {key: '1. Uzyskanie numeru VAT,'},
                        {key: '2. Oświadczenie o wyborze formy opodatkowania podatkiem dochodowym od osób fizycznych,'},
                        {key: '3. Powiadomienie o deklaracji składki na ubezpieczenie społeczne (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>Istnieją 3 obszary wymagające licencji:</Text>
                <FlatList
                    data={[
                        {key: '1. Zarządzanie nieruchomościami i pośrednictwo oraz zarządzanie nieruchomościami,'},
                        {key: '2. Wykonywanie usług transportu drogowego,'},
                        {key: '3. Prowadzenie agencji pracy, agencji pracy tymczasowej, instytucji szkolącej bezrobotnych w zakresie funduszy publicznych (National Report Slovenia, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla POLSKI</Text>
                <Text>Do założenia firmy wymagane jest wypełnienie wniosku rejestracyjnego CEIDG-1. W tym formularzu
                    konieczne jest:</Text>
                <FlatList
                    data={[
                        {key: '1. Uzyskanie numeru VAT,'},
                        {key: '2. Oświadczenie o wyborze formy opodatkowania podatkiem dochodowym od osób fizycznych,'},
                        {key: '3. Zawiadomienie o deklaracji składki na ubezpieczenie społeczne (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>Istnieją 3 obszary wymagające licencji:</Text>
                <FlatList
                    data={[
                        {key: '1. Zarządzanie nieruchomościami i pośrednictwo oraz zarządzanie nieruchomościami,'},
                        {key: '2. Wykonywanie usług transportu drogowego,'},
                        {key: '3. Prowadzenie agencji pracy, agencji pracy tymczasowej, instytucji szkolącej bezrobotnych w zakresie funduszy publicznych (National Report Poland, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla GRECJI</Text>
                <Text>Opis kroków:</Text>
                <FlatList
                    data={[
                        {key: '1. Uzyskaj zgodę na nazwę firmy od Izby Przemysłowo-Handlowej'},
                        {key: '2. Złóż dokumenty firmowe do Stowarzyszenia Adwokatów w Atenach'},
                        {key: '3. Podpisz akt założycielski przed notariuszem'},
                        {key: '4. Wpłać kapitał w banku'},
                        {key: '5. Zapłać podatek kapitałowy do organu podatkowego'},
                        {key: '6. Uzyskaj znaczek z funduszu emerytalnego prawników'},
                        {key: '7. Uzyskaj certyfikat wydany przez prawniczy fundusz opieki społecznej'},
                        {key: '8. Prześlij statut i zarejestruj się w sekretariacie sądu, aby uzyskać numer rejestracyjny'},
                        {key: '9. Prześlij podsumowanie Statutu do publikacji w Dzienniku Urzędowym (FEK)'},
                        {key: '10. Zarejestruj się w Izbie Przemysłowo-Handlowej'},
                        {key: '11. Zarejestruj się w organizacji ubezpieczenia (OAEE)'},
                        {key: '12. Organizacji Ubezpieczeń Rolnych (OGA), itp.'},
                        {key: '13. Uzyskaj numer podatkowy (AFM) dla firmy'},
                        {key: '14. Uzyskaj stempel / pieczęć'},
                        {key: '15. Poproś urząd skarbowy o ustalenie firmowych zestawień pokwitowań i dziennika księgowego'},
                        {key: '16. Powiadom Manpower (OAED) w ciągu 8 dni od zatrudnienia pracownika'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Wszyscy nowi właściciele firm będą musieli ukończyć poprzednie kroki.
                    {"\n"}Osoby prowadzące działalność na własny rachunek muszą wykonać kroki 4 i 10-15.
                    {"\n"}Nie jest konieczne uzyskanie określonej licencji lub zezwolenia na otwarcie sklepu
                    internetowego w celu sprzedaży rzemiosła (National Report Greece, 2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Dla FRANCJI</Text>
                <Text>Użycie INPI, gdy przedsiębiorca rozpoczyna działalność gospodarczą (National Report France,
                    2018).</Text>
                <Text><Text style={{fontWeight: 'bold'}}>Czym jest INPI?</Text>
                    {"\n"}(Narodowy Instytut Własności Przemysłowej) jest organem publicznym pod nadzorem Ministerstwa
                    Gospodarki, Finansów i Handlu Zagranicznego Ministerstwa Odzyskania Produktywności oraz Ministra
                    Delegata ds. Małych i Średnich Przedsiębiorstw, Innowacji i Gospodarki (Raport Krajowy Francja, 2018
                    r.).
                    {"\n\n"}Gdy nazwa zostanie zarejestrowana w INPI, firma jest ważna przez 10 lat.
                    {"\n"}Struktura prawna:
                </Text>
                <FlatList
                    data={[
                        {key: 'Samozatrudnienie'},
                        {key: 'Wyłączna własność'},
                        {key: 'Firma'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Ogólnie rzecz biorąc w celu ustanowienia działalności gospodarczej, należy postępować zgodnie z poniższymi wskazówkami (Raport krajowy Francja, 2018 r.):</Text>
                <FlatList
                    data={[
                        {key: '1. Wniosek wraz z żądanymi dokumentami do CFE (Chamber of Commerce),'},
                        {key: '2. Kod APE ustalony na podstawie głównej aktywności (kod zależy od głównej aktywności),'},
                        {key: '3. Formalności podatkowe,'},
                        {key: '4. Formalności socjalne realizowane przez CFE,'},
                        {key: '5. Otworzenie folderu w biurze pocztowym,'},
                        {key: '6. Otworzenie konta bankowego.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
