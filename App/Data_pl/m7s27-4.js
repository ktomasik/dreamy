import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Słowenii:</Text>
                <Text style={styles.dmlH3}>Krok 4:</Text>
                <Text>Po pomyślnym ukończeniu szkolenia i zatwierdzonym planie przedsiębiorczości oraz podpisaniu planu
                    zatrudnienia w Urzędzie ds. Zatrudnienia, musisz samozatrudnić się w ciągu 30 dni.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
