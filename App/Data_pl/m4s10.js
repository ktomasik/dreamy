import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Obowiązki podatkowe</Text>
                <Text style={{marginTop: 10}}>Postępuj zgodnie ze wskazówkami, aby spełnić zobowiązania podatkowe:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text style={{padding: 5}}>Powiadom urząd podatkowy (w miejscu zamieszkania)</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Uzyskaj certyfikat zwolnienia podatkowego</Text>
                    </View>
                </View>
            </View>
        )
    }
}
