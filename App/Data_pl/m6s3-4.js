import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S3_4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Fakturowanie na urządzeniach mobilnych</Text>
                <Text style={styles.dmlH3}>Dla Androida</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 2:</Text> Kliknij
                    Pobierz dla wybranego szablonu, a następnie wprowadź hasło do Google</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_20_3319624.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
