import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S62_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Korzystanie z serwisów społecznościowych</Text>
                <Text style={styles.dmlH2}>Skonfiguruj blog</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 15: Podaj tytuł swojego bloga, na przykład: Blog Davida
                    i URL Twojego bloga, na przykład: davidsblog.blogspot.com.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img132.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
