import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Gdzie znaleźć pożyczki dla małych firm prowadzonych przez kobiety?</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text style={styles.dmlH3}>Fundusz Gwarancyjny dla kobiet (GIF)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Cel: Ułatwienie pozyskiwania kredytów bankowych na pokrycie wymogów kapitału obrotowego i inwestycji w fazie tworzenia, odzyskiwania lub rozwoju firmy',
                                val2: 'https://www.afecreation.fr/pid14855/appuis-pour-les-femmes.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Sieci</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Poniższe sieci krajowe informują i towarzyszą przedsiębiorcom podczas prowadzenia działalności. Niektóre są poświęcone twórcom kobietom, inne są skierowane do wszystkich, ale mają konkretne działania dla kobiet.',
                                val2: 'http://www.ellesentreprennent.fr/pid14416/les-reseaux-au-service-des-creatrices.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>BPI France (Public Investment Bank):</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'BPI jest organizacją podlegającą nadzorowi państwa. Pomaga w finansowaniu i pomocy rozwojowej. Oferuje rozwiązania dotyczące uzyskania gwarancji bankowej.',
                                val2: 'http://www.bpifrance.com/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
