import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Odbieranie płatności</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie ze wskazówkami poniżej, aby skonfigurować firmę do
                    otrzymywania płatności:</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>KROK 1:
                    Skonfiguruj swoją firmę</Text>
                <Text>Sprawdź opcje z wyprzedzeniem, aby ustalić odpowiednią organizację i structure firmy. W ten sposób
                    nie zakończy się to niewłaściwym rodzajem działalności. W wielu przypadkach możesz zmienić to
                    później, ale łatwiej jest sprawdzić jakie opcje sa najlepsze niezależnie od tego, czy jesteś jedynym
                    właścicielem, spółką, korporacją czy inna organizacją. Księgowy może pomóc w założeniu firmy i
                    załatwieniu niezbędnych formalności.
                    {"\n\n"}Jeśli masz partnerów, upewnij się, że wszystkie informacje są dokładne.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>KROK 2: Uzyskaj
                    numer identyfikacyjny swojej firmy</Text>
                <Text>Uzyskanie numeru identyfikacyjnego jest niezbędnym pierwszym krokiem, ponieważ będziesz
                    potrzebować tego ważnego numeru, jeśli planujesz otworzyć konto bankowe.
                    {"\n\n"}Możesz potrzebować więcej niż jednego numeru identyfikacyjnego dla swojej firmy (np. Numeru
                    VAT podczas rejestracji w urzędzie podatkowym, numeru identyfikacyjnego przy rejestracji w lokalnej
                    izbie itp.).
                    {"\n\n"}Numer identyfikacyjny Twojej firmy może być również przydatny, jeśli chcesz otrzymywać
                    płatności za pośrednictwem procesora takiego jak PayPal. Musi się również pojawić na wszystkich
                    paragonach i fakturach, które wystawiasz.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>KROK 3: Otwórz
                    firmowe konto bankowe</Text>
                <Text>Gdy zaczniesz otrzymywać płatności, będziesz potrzebował miejsca, do którego płatności będą
                    kierowane.
                    {"\n\n"}Posiadanie osobnych kont jest ważne dla właściciela firmy. Przede wszystkim warto
                    przechowywać aktywa firmy oddzielnie od zasobów osobistych. Po drugie, utrzymanie zapisów
                    finansowych jest w ten sposób łatwiejsze. Kiedy przychodzi czas podatkowy, o wiele łatwiej jest
                    zająć się wszystkim, jeśli wszystko jest na swoim miejscu.Jeśli pomiesziesz swoje pieniądze z
                    pieniędzmi firmy, możesz mieć poważne problemy.
                    {"\n\n"}Zawsze musisz mieć na uwadze, że czas otrzymywania pieniędzy od klientów nie pokrywa się z
                    czasem, w którym Ty sam musisz zapłacić, na przykład, z czynszem, podatkami, dostawcami itp.
                    Niezrozumienie znaczenia tego rozróżnienia może skutkować niedoborem pieniędzy w przypadku
                    faktycznych płatności, ponieważ pieniądze zostały wydane na wydatki osobiste.
                    {"\n\n"}Twoje konto bankowe jest miejscem, gdzie powinieneś mieć swoje depozyty i ogólnie, gdzie
                    powinieneś podejmować wszystkie transakcje swojej firmy.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>KROK 4: Ustaw
                    odbieranie płatności za pośrednictwem strony trzeciej</Text>
                <Text>Ważne jest, aby akceptować wiele różnych metod płatności. Twoi klienci mają własne preferencje
                    dotyczące płatności. Jednym z najlepszych sposobów upewnienia się, że je dostosowujesz, jest użycie
                    procesora innej firmy.
                    {"\n\n"}Możesz założyć konto sprzedawcy za pomocą procesora karty lub otrzymywać płatności za
                    pośrednictwem witryny takiej jak PayPal. Podczas korzystania z tego typu procesora zwykle podawane
                    są karty kredytowe.
                    {"\n\n"}Możliwe jest także używanie procesorów do akceptowania fizycznych kart kredytowych z
                    urządzenia przenośnego. Może to być pomocne, jeśli masz fizyczne przedmioty do sprzedania i robisz
                    to osobiście.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>KROK 5: Wymagania
                    biznesowe państwa</Text>
                <Text>Przed otrzymywaniem płatności upewnij się, że znasz wymagania dla firm w swoim kraju. Ministerstwo
                    Gospodarki Twojego kraju (lub inny właściwy organ) powinno mieć informacje o tym, co musisz zrobić,
                    aby założyć firmę i zacząć prowadzić interesy.</Text>
            </View>
        )
    }
}
