import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S5 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Konto biznesowe na Instagramie</Text>
                <Text>Utworzenie konta: Wcześniej wyjaśniono, jak otworzyć konto Instagram w Module 2. Jak wspomniano w
                    module 2, można pobrać aplikację Instagram ze sklepu App Store na iOS, sklepu Google Play na system
                    Android lub Windows Phone Store na Windows Phone.
                    {"\n\n"}Po załadowaniu aplikacji dotknij ikony aplikacji na telefonie komórkowym, aby otworzyć
                    Instagram.
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
