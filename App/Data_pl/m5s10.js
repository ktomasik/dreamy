import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Akceptowanie płatności za pomocą kont bankowych</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie ze wskazówkami poniżej, aby akceptować płatności za
                    pomocą kont bankowych.
                    {"\n\n"}Przelew bankowy działa w następujący sposób:
                </Text>
                <FlatList
                    data={[
                        {key: 'a. Podmiot, który chce dokonać transferu przekazuje bankowi zlecenie przekazania określonej kwoty pieniędzy.  Podawane są również kody IBAN i BIC, więc bank wie, gdzie należy wysłać pieniądze.'},
                        {key: 'b. Bank wysyłający przesyła komunikat, za pośrednictwem bezpiecznego systemu, do banku odbiorczego, żądając, aby dokonywał płatności zgodnie z podanymi instrukcjami.'},
                        {key: 'c. Komunikat zawiera również instrukcje rozliczeniowe. Rzeczywisty transfer nie jest natychmiastowy: przesunięcie środków z konta nadawcy na konto odbiorcy może potrwać kilka godzin lub nawet kilka dni.'},
                        {key: 'd. Zaangażowane banki muszą posiadać wzajemne konto lub płatność musi zostać wysłana do banku posiadającego takie konto, banku korespondującego, w celu przekazania funduszy dla ostatecznego odbiorcy.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Przed otrzymaniem płatności międzynarodowej musisz podać nadawcy kilka
                    informacji, w tym:</Text>
                <FlatList
                    data={[
                        {key: 'Twój międzynarodowy numer rachunku bankowego (IBAN)'},
                        {key: 'Twój kod sortowania'},
                        {key: 'Numer konta'},
                        {key: 'Twoje pełne imię i nazwisko'},
                        {key: 'Twój adres'},
                        {key: 'Kwota i waluta, w której chcesz otrzymać płatność.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
