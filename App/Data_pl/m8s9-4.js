import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img15.png')
    }
}];

export default class M8S9_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Wysyłanie ładunku</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>KROK 8: ŚLEDZENIE</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img15.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Wskazówki przy pracy z firmami przewozowymi:</Text>
                <FlatList
                    data={[
                        {key: 'Zwróć się do klienta z pytaniem o preferencje (firma, pracownik itp.) może być dobrym pomysłem. Może on / ona mieć specjalne wymagania.'},
                        {key: 'Produkt powinien pasować do opakowania.'},
                        {key: 'Odpowiednie opakowanie może obniżyć koszty.'},
                        {key: 'Podpisanie umowy z firmą przewozową może obniżyć koszty.'},
                        {key: 'Umowa może zapewnić pewne korzyści, takie jak otrzymywanie ładunku na twój adres bez żadnych kosztów.'},
                        {key: 'Przed dostawą, firma przewozowa powinna sprawdzić planowaną drogę dostawy.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

            </View>
        )
    }
}
