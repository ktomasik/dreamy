import React, {Component} from 'react'
import {FlatList, Image, Modal, StyleSheet, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img9.png')
    }
}];

export default class M8S8 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Wysyłanie ładunku</Text>
                <Text styl={{marginTop: 10}}>Wykonaj kroki wskazane poniżej, aby wysłać ładunek:</Text>
                <Text style={styles.dmlH3}>KROK 1: ODPOWIEDNIO ZAPAKUJ SWÓJ PRODUKT</Text>
                <View style={styles2.border}>
                    <Text>Wskazówki, jak przygotować przesyłkę:</Text>
                    <FlatList
                        data={[
                            {key: 'Wybierz odpowiedni rodzaj paczki,'},
                            {key: 'Wybierz odpowiedni rozmiar paczki,'},
                            {key: 'Zabezpiecz produkty wrażliwe miękkimi materiałami,'},
                            {key: 'W razie potrzeby użyj ostrożnych etykiet, takich jak "delikatne",'},
                            {key: 'W razie potrzeby zaznacz uchwyty'}
                        ]}
                        renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                    />
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
