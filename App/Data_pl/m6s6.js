import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Informacje o umowie</Text>
                <Text style={{marginTop: 15}}>W grudniu 2015 r. Komisja UE zaproponowała dyrektywę w sprawie umów
                    dotyczących sprzedaży towarów i usług drogą internetową na odległość (dyrektywa w sprawie sprzedaży
                    internetowej towarów). Proponowana dyrektywa w sprawie sprzedaży internetowej towarów przewiduje
                    maksymalną harmonizację, a tym samym zakazuje państwom członkowskim wprowadzania wyższego poziomu
                    ochrony konsumenta w zakresie objętym dyrektywą . Jak już wspomniano powyżej, przede wszystkim w
                    przypadku zakupu produktu lub usługi w UE przedsiębiorca musi przed przekazaniem produktu
                    przedstawić jasne, dokładne i zrozumiałe informacje o produkcie lub usłudze.
                    {"\n\n"}W tym kontekście, wraz z proponowanymi nowymi umowami, typowa umowa na zakupy online powinna
                    zawierać następujące elementy:
                </Text>
                <FlatList
                    data={[
                        {key: 'tożsamość handlowca, adres, e-mail i numer telefonu'},
                        {key: 'tytuł zawodowy i dane dotyczące podatku VAT podmiotu gospodarczego (jeśli dotyczy)'},
                        {key: 'numer handlowego rejestru przedsiębiorcy'},
                        {key: 'główne cechy produktu'},
                        {key: 'łączna cena zawierająca podatki i wszystkie opłaty'},
                        {key: 'koszty dostawy (jeśli dotyczy) - i wszelkie inne dodatkowe opłaty'},
                        {key: 'ustalenia dotyczące płatności, dostawy lub wykonania'},
                        {key: 'czas trwania umowy (jeśli dotyczy)'},
                        {key: 'wszelkie ograniczenia dostawy w niektórych krajach'},
                        {key: 'prawo do anulowania zamówienia w ciągu 14 dni'},
                        {key: 'dostępne usługi posprzedażne'},
                        {key: 'mechanizmy rozstrzygania sporów'}
                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Aby ubiegać się o ubezpieczenie w celu sprzedaży produktów online
                    {"\n\n"}Możesz ponosić odpowiedzialność za sprzedane produkty, ponieważ sprzedajesz produkty, które
                    wykonałeś pod nazwą swojej firmy. Na przykład jesteś sprzedawcą internetowym, który sprzedaje
                    ekstrawaganckie, spersonalizowane ciasta i dostarcza je do drzwi klientów. Jeśli ktoś obwinia cię o
                    zatrucie pokarmowe wkrótce po zjedzeniu jednego z twoich ciastek, możesz być zobowiązany do wypłaty
                    odszkodowania. Ponadto produkt może zostać uszkodzony podczas operacji ładunkowych lub produkt może
                    zostać skradziony. Na tym etapie ubezpieczenie może pokryć koszty wymiany w oparciu o cenę nabycia.
                    Pod tym względem skontaktowanie się z agentem ubezpieczeniowym będzie właściwym sposobem, aby pomóc
                    ci zrozumieć, jakiego rodzaju ubezpieczenie naprawdę potrzebujesz, takie jak odpowiedzialność za
                    produkt i odpowiedzialność handlową.
                    {"\n\n"}Z drugiej strony, jak widać w poniższych krokach, wykupienie ubezpieczenia staje się teraz
                    bardzo łatwym i szybkim procesem. Odpowiednie oferty z Twoimi danymi są przygotowywane tak szybko,
                    jak to możliwe. Otrzymasz propozycję po wstępnej ocenie. Dlatego możesz kupić ubezpieczenie, które
                    uważasz za najbardziej odpowiednie.
                </Text>
            </View>
        )
    }
}
