import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_2 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Wykonaj poniższe kroki, aby sprzedać swoje produkty na Instagramie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 2: Użyj już podanej nazwy konta na Instagramie lub
                    podaj nową nazwę konta biznesowego do komercjalizacji swoich produktów</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m3/img37.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
