import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Konto biznesowe na Instagramie</Text>
                <Text styl={{marginTop: 10}}>Jeśli już otworzyłeś konto biznesowe na Facebooku, z biznesowego punktu
                    widzenia, Instagram jest prostszy i mniej czasochłonny niż inne witryny.,
                    {"\n\n"}Jeśli chcesz, możesz rozszerzyć swoją sieć klientów za pomocą Facebooka i Instagrama.
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
