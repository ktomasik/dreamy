import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utwórz konto na Instagramie</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 14: kliknij dodaj zdjęcie, aby dodać swoje zdjęcie
                    profilowe. Wybierz źródło swojego zdjęcia profilowego. Możesz go pobrać z Facebooka, możesz zrobić
                    zdjęcie aparatem telefonu lub wybrać zdjęcie z biblioteki.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img50.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img51.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
