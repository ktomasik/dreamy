import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img8.png')
    }
}];

export default class M8S5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Pakowanie i marketing</Text>
                <Text style={styles.dmlH3}>Etykiety nadawcze</Text>
                <Text>SEtykiety wysyłkowe i manipulacyjne - etykiety są wstępnie drukowanymi etykietami, które określają
                    właściwą obsługę, a w niektórych przypadkach informacje dotyczące miejsca przeznaczenia. Etykiety te
                    mogą wskazywać zawartość opakowania, na przykład łatwopalność lub delikatne, mogą zawierać wskazówki
                    dotyczące postępowania (Anonimowy 2018j). Etykiety wysyłkowe i obsługowe zawierają informacje na
                    temat obsługi każdej konkretnej przesyłki. Istnieje wiele typów etykiet wysyłki i obsługi, jak
                    pokazano poniżej.
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Mailery i małe etykiety </Text>na paczkach służą do
                    ostrzegania personelu o konkretnych instrukcjach obsługi małych paczek wysyłanych pocztą (Anonimowy
                    2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Etykiety produkcji i małych opakowań </Text>zawierają
                    specyficzne informacje jak obsługiwać poszczególne paczki w procesie transportu. (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Międzynarodowe oznaczenia obrazowe </Text>są
                    wykorzystywane do wysyłki międzynarodowej. Etykiety z obrazkami zmniejszają ryzyko błędu, ponieważ
                    personel w innych krajach może nie rozumieć języka kraju pochodzenia paczki, ale zaleca się używanie
                    dodatkowych etykiet w języku miejsca przeznaczenia przesyłki w celu zapewnienia prawidłowej obsługi.
                    Etykiety powinny być stosowane w celu zapewnienia zgodności z normami ochrony środowiska i
                    bezpieczeństwa oraz identyfikacji materiałów niebezpiecznych (Anonimowy 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Etykiety ze strzałkami </Text>pomagają chronić delikatne
                    opakowania, aby bezpiecznie dotarły. Etykiety ze strzałkami są częścią międzynarodowych etykiet
                    obrazkowych (Anonimowy 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img2.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Etykiety Delikatne / Szklane </Text>zwracają
                    uwagę na zawartość opakowania i odpowiednie instrukcje postępowania, takie jak "tutaj otwierać"
                    (Anonimowy 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img3.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Etykiety pośpiechu </Text>są używane do
                    opakowań zależnych od temperatury lub medycznych. Łatwo psujące się leki, które muszą być
                    przechowywane w lodówce i są potrzebne w sytuacjach ratowania życia, często używają etykiet
                    pośpiechu (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img4.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img5.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Etykiety ostrzegawcze dotyczące
                    temperatury </Text>informują personel spedycyjny i obsługowy o wymogach dotyczących temperatury
                    właściwej dla opakowania. Etykiety te są używane do zamrożonych i / lub łatwo psujących się towarów
                    (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img6.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img7.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Etykiety zagrożenia </Text>identyfikują
                    paczki z towarami niebezpiecznymi. Ostrzegają personel spedycyjny i obsługujący o specjalnych
                    potrzebach magazynowania i segregacji podczas transportu. Istnieją surowe przepisy dotyczące
                    transportu towarów niebezpiecznych i często wymagana jest specjalna dokumentacja, np. dane
                    chemiczne. Etykiety o niebezpieczeństwie powinny być stosowane w materiałach wybuchowych, gazach,
                    łatwopalnych cieczach, łatwopalnych ciałach stałych, materiałach samozapalnych lub reagujących z
                    wodą, trujących substancjach i materiałach korozyjnych (Anonim 2018j).</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Zamknij okno</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Biohazardowe etykiety </Text>identyfikują
                    opakowania produktów biologicznie niebezpiecznych, takich jak bioodpady, próbki ludzkie lub
                    zwierzęce oraz zużyty sprzęt laboratoryjny (Anonimowy 2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Specjalne etykiety </Text>do obsługi
                    obejmują wszystkie specjalne instrukcje dotyczące transportu i obsługi. Etykiety mogą być
                    zaprojektowane na indywidualne zamówienie z konkretnymi instrukcjami. Często są to etykiety
                    ostrzegawcze lub instrukcje wysyłkowe (Anonymous 2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Wskazówka </Text>Etykiety należy
                    naklejać na trzy strony opakowania, najlepiej z boku i / lub na górze i na górze (Anonymous 2018j).
                    Jeśli towary wymagają specjalnej obsługi lub przechowywania, pakiet wysyłkowy powinien być tak
                    oznaczony, a informacja ta powinna również pojawić się na liście przewozowym (Anonimowy 2018j).
                </Text>
            </View>
        )
    }
}
