import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S5 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Niektóre fundusze dla kobiet-założycieli</Text>
                <Text>500 kobiet: Finansowanie kobiet założycieli</Text>
                <Text>Astia: Sieć oferująca dostęp do kapitału i szkolenia / wsparcie dla kobiet-przedsiębiorców</Text>
                <Text>BBG (Built By Girls) Ventures: Inwestuje w start-upy konsumenckie w Internecie z co najmniej jedną założycielką </Text>
                <Text>Chloe Capital: koncentruje się na firmach prowadzonych przez kobiety</Text>
                <Text>Female Founders Fund: inwestuje w kobiety prowadzące działalność w zakresie start-upów w handlu elektronicznym, na platformach i serwisach internetowych </Text>
                <Text>Golden Seeds: sieć inwestora i inwestowanie w fundusze dla kobiet-przedsiębiorców</Text>
                <Text>Intel Capital Diversity Fund: Fundusz, który inwestuje w start-upy kobiet i mniejszości</Text>
                <Text>Mergelane: Akcelerator i inwestor firm prowadzonych przez kobiety (Boulder, CO) </Text>
                <Text>Next Wave Impact: innowacyjny fundusz wczesnego rozwoju oparty na uczeniu się przez działanie</Text>
                <Text>Pipeline Fellowship: Kobiety inwestujące w kobiety prowadzące przedsiębiorstwa społeczne </Text>
                <Text>Valor Ventures: Fundusz z siedzibą w Atlancie finansuje kobiety, które założyły własne firmy </Text>
                <Text>Women's Venture Fund: pomaga przedsiębiorcom poprzez kursy, doradztwo, kredyty i wiele innych</Text>
            </View>
        )
    }
}
