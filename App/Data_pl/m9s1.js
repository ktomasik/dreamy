import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Wprowadzenie</Text>
                <Text>W poprzednich modułach nauczyłeś się tworzyć konta e-mail i konfigurować je na smartfonach. Poza
                    tym nauczyłeś się zakładać konta w mediach społecznościowych. Z drugiej strony nauczyłeś się, jak
                    robić zdjęcia swoich produktów i przesyłać je na te platformy. Celem tego modułu jest wyjaśnienie,
                    jak skonfigurować istniejące konta w mediach społecznościowych dla swojej firmy.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
