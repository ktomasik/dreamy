import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Popularne serwisy społecznościowe mediów społecznościowych</Text>
                <Text>Szacuje się, że około 81% Amerykanów korzystało z mediów społecznościowych od 2017 roku i robi to
                    coraz częściej. Według jednego z szacunków ponad jedna piąta czasu spędzonego w Internecie spędzana
                    jest w mediach społecznościowych. W 2005 r. odsetek osób dorosłych korzystających z mediów
                    społecznościowych wyniósł około 5%. Na całym świecie jest około 1,96 miliarda użytkowników mediów
                    społecznościowych. Oczekuje się, że liczba ta wzrośnie do 2,5 miliarda na koniec 2018 r. Inne
                    szacunki są jeszcze wyższe (Silver, n.d.). Według Pew Research Center (2018) użytkownicy mediów
                    społecznościowych są zazwyczaj młodsi (około 90% osób w wieku od 18 do 29 lat korzystali z co
                    najmniej jednej formy mediów społecznościowych), lepiej wykształceni i stosunkowo bogaci
                    (zarabiający ponad 75 000 USD rocznie). Stany Zjednoczone i Chiny prowadzą na liście mediów
                    społecznościowych ("Wiodące globalne sieci społecznościowe 2018 |Statistic’ 2018): Facebook (2.167
                    billion users as of January 2018), YouTube (1.5B), WhatsApp (1.3B), Facebook Messenger (1.3B),
                    WeChat (980M), QQ (843M), Instagram (800M), Tumblr (794M), QZone (568M), Sina Weibo (376M), Twitter
                    (330M), Baidu Tieba (300M), Skype (300M), LinkedIn (260M), Viber (260M), Snapchat (255M), Reddit
                    (250M), LINE (203M), Pinterest (200M), YY (117M).
                    {"\n\n"}Różne witryny i aplikacje służące do obsługi mediów społecznościowych są poświęcone różnym
                    rodzajom komunikacji i różnym treściom. Krótki opis jest dostępny w następnych sekcjach dla
                    najpopularniejszych mediów społecznościowych (Rouse 2016).
                </Text>
                <Text style={styles.dmlH3}>Facebook</Text>
                <Text>to popularny, darmowy serwis społecznościowy, który umożliwia zarejestrowanym użytkownikom
                    tworzenie profili, przesyłanie zdjęć i filmów, wysyłanie wiadomości oraz utrzymywanie kontaktu z
                    przyjaciółmi, rodziną i współpracownikami. Według statystyk Nielsen Group, użytkownicy Internetu w
                    Stanach Zjednoczonych spędzają więcej czasu na Facebooku niż na jakiejkolwiek innej stronie
                    internetowej.</Text>
                <Text style={styles.dmlH3}>Twitter</Text>
                <Text>to darmowa usługa mikroblogowania, która pozwala zarejestrowanym użytkownikom na nadawanie
                    krótkich postów zwanych tweetami. Członkowie Twittera mogą wysyłać tweety i śledzić tweety innych
                    użytkowników za pomocą wielu platform i urządzeń.</Text>
                <Text style={styles.dmlH3}>Google+</Text>
                <Text>to projekt sieci społecznościowych Google, mający na celu odtworzenie sposobu, w jaki ludzie
                    wchodzą w interakcję w trybie offline bardziej niż w przypadku innych serwisów społecznościowych.
                    Hasło projektu brzmi: "Prawdziwe życie ponownie przemyślane w Internecie".</Text>
                <Text style={styles.dmlH3}>Wikipedia</Text>
                <Text>to darmowa, otwarta encyklopedia internetowa stworzona dzięki wspólnemu wysiłkowi społeczności
                    użytkowników zwanej Wikipedystami. Każdy zarejestrowany na stronie może stworzyć artykuł do
                    publikacji; rejestracja nie jest wymagana do edytowania artykułów. Wikipedia została założona w
                    styczniu 2001 roku.</Text>
                <Text style={styles.dmlH3}>LinkedIn</Text>
                <Text>to serwis społecznościowy zaprojektowany specjalnie dla społeczności biznesowej. Celem witryny
                    jest umożliwienie zarejestrowanym użytkownikom tworzenia i dokumentowania sieci osób, które znają i
                    którym ufają zawodowo.</Text>
                <Text style={styles.dmlH3}>Reddit</Text>
                <Text>to serwis społecznościowy i forum, na którym historie są społecznie promowane przez członków
                    witryny. Witryna składa się z setek pod-społeczności, znanych jako "subreddits". Każdy subreddit ma
                    określony temat, taki jak technologia, polityka lub muzyka. Członkowie witryny Reddit, znani również
                    jako "redditors", przesyłają treści, które są następnie głosowane przez innych członków. Celem jest
                    wysłanie dobrze przemyślanych artykułów na górę głównej strony wątku witryny.</Text>
                <Text style={styles.dmlH3}>Pinterest</Text>
                <Text>to serwis społecznościowy, w którym można udostępniać i kategoryzować obrazy znalezione w
                    Internecie. Pinterest wymaga krótkich opisów, ale główny nacisk strony jest wizualny. Kliknięcie
                    obrazu przeniesie Cię do oryginalnego źródła, więc na przykład, jeśli klikniesz zdjęcie pary butów,
                    możesz zostać przeniesiony na stronę, gdzie możesz je kupić. Obraz naleśników z jagodami może
                    doprowadzić Cię do przepisu; zdjęcie domku dla ptaków może zabrać Cię do instrukcji.
                </Text>
            </View>
        )
    }
}
