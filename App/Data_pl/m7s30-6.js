import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text style={styles.dmlH3}>Jeśli jesteś poszukującym pracy</Text>
                <Text style={styles.dmlH3}>Krok 3:</Text>
                <Text>Wypełnij wniosek o dotację.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
