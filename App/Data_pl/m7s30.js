import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Francji:</Text>
                <Text style={styles.dmlH3}>Jeśli nie jesteś poszukującym pracy POMOC PAŃSTWA ACCRE</Text>
                <Text>Twój wniosek musi zostać złożony w kompetentnym centrum ds. Formalności biznesowych (CFE), zarówno
                    gdy twoja firma zostanie założona lub wznowiona, lub w ciągu 45 dni od jej utworzenia.
                    {"\n"}Natępnie:
                </Text>
                <Text style={styles.dmlH3}>Krok 1:</Text>
                <Text>Wniosek należy złożyć w odpowiednim ośrodku ds. Formalności zawodowych (CFE) w momencie utworzenia
                    lub wznowienia działalności lub w ciągu 45 dni od jej złożenia. Musisz dołączyć do swojej
                    prośby:</Text>
                <Text>- Formularz deklaracji spółki w CFE lub jej kopii.</Text>
                <Text>- Konkretna forma wniosku o przyznanie pomocy, ważna z uwagi na brak korzystania z tej pomocy
                    przez 3 lata.</Text>
                <Text>- Dowód, że należysz do jednej z kategorii korzystających z ACCRE.</Text>
                <Text>Inne dokumenty mogą być wymagane przez CFE, w tej sprawie należy skontaktować się z nimi.</Text>
            </View>
        )
    }
}
