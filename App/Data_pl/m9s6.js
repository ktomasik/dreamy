import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Twitter dla firm</Text>
                <Text styl={{marginTop: 10}}>Jedna z najczęściej używanych platform mediów społecznościowych;
                    umożliwiająca bezpośrednią komunikację z potencjalnymi klientami to Twitter. Obserwujący na
                    Twitterze to Twoi potencjalni klienci. Musisz zgromadzić własnych obserwatorów, aby zobaczyć zalety
                    Twittera jako narzędzia marketingowego. Możesz stworzyć własną listę twitterów lub dołączyć do
                    grupy, która już została utworzona.
                    {"\n\n"}Aby otworzyć konto firmowe Twitter, musisz najpierw mieć konto na Twitterze. Jak opisano w
                    Module 2, przejdź do wyszukiwania Google; wpisz «twitter» i szukaj; wejdź na stronę Twittera,
                    wybierz «mobile.twitter.com» kliknij "Zarejestruj się", aby otworzyć twitter (szczegóły - patrz
                    Moduł 2)
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/twitter.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
