import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Formularz pierwszego zamówienia</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie ze wskazówkami poniżej, aby skonfigurować formularz
                    pierwszego zamówienia.
                    {"\n"}Jak utworzyć prosty formularz zamówienia z płatnościami online
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Aby otrzymywać zamówienia</Text>: poniżej otrzymasz
                    podpowiedź, jak utworzyć formularz zamówienia WordPress, który będzie akceptował płatności kartą
                    kredytową i PayPal.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 1: Utwórz
                    prosty formularz zamówienia w WordPress</Text>
                <FlatList
                    data={[
                        {key: 'Zainstaluj i aktywuj wtyczkę WPForms.'},
                        {key: 'Przejdź do WPForms -> Dodaj nowy, aby utworzyć nowy formularz.'},
                        {key: 'Nazwij swój formularz i wybierz szablon formularza Rozliczanie / Zamówienie.'},
                        {key: 'Przewiń w dół do sekcji "Dostępne elementy" na ekranie podglądu po prawej stronie i kliknij na nią'},
                        {key: 'Otworzy się sekcja "Opcje” na lewym panelu. Tutaj możesz zmienić nazwę pola, dodać lub usunąć elementy zamówienia i zmienić ceny'},
                        {key: 'Jeśli chcesz podać ludziom zdjęcia do wyboru podczas wypełniania formularza zamówienia, kliknij pole wyboru Użyj opcji wyboru obrazu w Edytorze formularzy.'},
                        {key: 'Na koniec możesz dodać dodatkowe pola do formularza zamówienia, przeciągając je z lewej strony na prawą stronę'},
                        {key: 'Kliknij Zapisz, gdy skończysz'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 2:
                    Skonfiguruj powiadomienia formularza zamówienia</Text>
                <FlatList
                    data={[
                        {key: 'Powiadomienia to świetny sposób na wysłanie e-maila po przesłaniu formularza.'},
                        {key: 'Możesz przesłać powiadomienie e-mail do siebie, do członka zespołu, dodając swoją wiadomość e-mail do pola Wyślij do adresu e-mail lub do klienta, aby powiadomić ich o otrzymaniu zamówienia.'},
                        {key: 'Kliknij kartę Ustawienia w Konstruktorze formularzy, a następnie kliknij Powiadomienia.'},
                        {key: 'Kliknij opcję Pokaż znaczniki w polu Wyślij na adres e-mail.'},
                        {key: 'Kliknij Email'},
                        {key: 'Zmień adres e-mail powiadomienia, aby był bardziej szczegółowy. Ponadto możesz dostosować "Od", "Od e-maili" i "Odpowiedz" do "e-maili".'},
                        {key: 'Dołącz spersonalizowaną wiadomość, jeśli wiadomość e-mail trafia do kogoś innego niż Ty.'},
                        {key: 'Użyj tagów {wszystkie pola}, jeśli chcesz uwzględnić wszystkie informacje znalezione w polach formularza złożonego zamówienia.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 3:
                    Skonfiguruj potwierdzenia zamówienia</Text>
                <Text>Potwierdzenia formularza to wiadomości wyświetlane klientom po przesłaniu formularza
                    zamówienia.Dostępne są trzy rodzaje potwierdzeń:</Text>
                <FlatList
                    data={[
                        {key: '1. Wiadomość. Gdy klient prześle formularz zamówienia, pojawi się proste potwierdzenie wiadomości, informujące o tym, że formularz został przetworzony.'},
                        {key: '2. Pokaż stronę. Ten rodzaj potwierdzenia przenosi klientów do określonej strony internetowej w witrynie, dziękując im za zamówienie.'},
                        {key: '3. Przejdź do adresu URL (Przekierowanie). Ta opcja jest używana, gdy chcesz wysłać klientów do innej witryny.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Zobaczmy, jak skonfigurować proste potwierdzenie formularza w WPForms,
                    dzięki czemu można dostosować wiadomość, którą zobaczą użytkownicy po złożeniu zamówienia.</Text>
                <FlatList
                    data={[
                        {key: 'Kliknij zakładkę Potwierdzenie w Edytorze formularzy w Ustawieniach.'},
                        {key: 'Wybierz typ potwierdzenia, które chcesz utworzyć.'},
                        {key: 'Dostosuj wiadomość z potwierdzeniem do swoich potrzeb i kliknij przycisk Zapisz, gdy skończysz.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 4:
                    Skonfiguruj ustawienia płatności</Text>
                <Text>WPForms integruje się z PayPalem w celu przyjmowania płatności.
                    {"\n"}Aby skonfigurować ustawienia płatności w formularzu zamówienia, musisz najpierw zainstalować i
                    aktywować odpowiedni dodatek do płatności.
                    {"\n\n"}Kiedy to zrobisz:
                </Text>
                <FlatList
                    data={[
                        {key: 'Kliknij kartę Płatności w Edytorze formularzy.'},
                        {key: 'Kliknij PayPal,'},
                        {key: 'Wprowadź swój adres e-mail w systemie PayPal,'},
                        {key: 'Wybierz tryb Produkcji,'},
                        {key: 'Wybierz Produkty i Usługi,'},
                        {key: 'Skonfiguruj ustawienia płatności,'},
                        {key: 'Kliknij Zapisz, aby zapisać zmiany.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Teraz możesz dodać prosty formularz zamówienia do swojej witryny.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Krok 5: Utwórz
                    nowy post lub stronę w WordPress, a następnie kliknij przycisk Dodaj formularz.</Text>
                <FlatList
                    data={[
                        {key: 'Utwórz nowy post lub stronę w WordPress, a następnie kliknij przycisk Dodaj formularz.'},
                        {key: 'Wybierz prosty formularz zamówienia z rozwijanego menu i kliknij Dodaj formularz.'},
                        {key: 'Opublikuj swój wpis lub stronę, aby formularz zamówienia pojawił się w Twojej witrynie.'},
                        {key: 'Przejdź do Wygląd »Widgets i dodaj WPForms do paska bocznego.'},
                        {key: 'Wybierz opcję Rozliczenia / Zamówienia z menu rozwijanego.'},
                        {key: 'Kliknij Zapisz.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Teraz możesz zobaczyć opublikowany formularz zamówienia na żywo w swojej
                    witrynie.
                    {"\n"}Zauważ, że po wybraniu pozycji w formularzu cena zmienia się automatycznie.
                    {"\n"}Teraz wiesz, jak utworzyć prosty formularz zamówienia w WordPress, który akceptuje płatności
                    online.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', marginTop: 15}}>Jeśli chcesz wysyłać zamówienia
                    jako firma, możesz skorzystać z formularza zamówień Dokumentów Google.</Text>
                <Text>Dokumenty Google umożliwiają tworzenie formularzy, które mogą być używane jako formularz
                    zamówienia. Po zapisaniu danych w arkuszu kalkulacyjnym w Dokumentach Google możesz zarządzać
                    rozliczeniami lub zamawianiem z tego miejsca.</Text>
                <FlatList
                    data={[
                        {key: '1. Otwórz Dokumenty Google i kliknij przycisk "Utwórz". Wybierz "Formularz".'},
                        {key: '2. Wpisz nazwę i opis swojego formularza zamówienia.'},
                        {key: '3. W razie potrzeby podziel formularz na sekcje.'},
                        {key: '4. Dodaj pytania, klikając "Dodaj przedmiot".'},
                        {key: '5. Wybierz motyw dla swojego formularza, klikając przycisk "Motyw" obok przycisku "Dodaj przedmiot".'},
                        {key: '6. Kliknij łącze u dołu okna formularza, aby wyświetlić formularz w przeglądarce.'},
                        {key: '7. Rozprowadź swój formularz.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
