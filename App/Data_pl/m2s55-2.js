import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S55_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Korzystanie z serwisów społecznościowych</Text>
                <Text style={styles.dmlH2}>Stwórz stronę na Facebooku</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 13: Twoja treść jest publikowana na stronie FaceBook.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img114.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img115.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
