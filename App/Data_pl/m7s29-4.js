import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M7S29_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Finansowanie krok po kroku w krajach partnerskich projektu</Text>
                <Text style={styles.dmlH2}>Dla Grecji:</Text>
                <Text style={styles.dmlH3}>Krok 4:</Text>
                <Text>Ostateczny biznes plan jest oceniany przez właściwą komisję, a jeśli zostanie zatwierdzony, można
                    kontynuować udzielania mikrofinansowania do kwoty 10 000 EUR i wsparcie doradcze, aby rozpocząć jego
                    wdrażanie.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
