import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S61_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Korzystanie z serwisów społecznościowych</Text>
                <Text style={styles.dmlH2}>Skonfiguruj blog</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 12: Ponownie wybierz swoje konto Google, wpisz swoje *
                    hasło * i kliknij Dalej.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img128.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img129.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
