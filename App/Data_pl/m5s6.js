import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S6 extends Component {
    render() {
        return (
            <View style={styles.section}>

                <Text style={styles.dmlH2}>Otwieranie konta bankowego</Text>
                <Text style={{marginTop: 15}}>Postępuj zgodnie ze wskazówkami poniżej, aby otworzyć konto bankowe
                    {"\n"}Rozważ swoje opcje
                    {"\n"}Po określeniu swoich potrzeb oceń dostępne opcje:
                </Text>
                <FlatList
                    data={[
                        {key: 'Konto czekowe'},
                        {key: 'Konto oszczędnościowe'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5, fontWeight: 'bold'}}>{item.key}</Text>}
                />
                <Text style={{marginTop:10}}>Możliwe, że wybrałeś konto bankowe, które najbardziej Ci odpowiada, ale musisz także upewnić się, że możesz otworzyć konto. Zanim przejdziesz do banku, musisz sprawdzić, czy spełniasz wszystkie kryteria otwarcia konta.
                    {"\n\n"}Co do zasady banki wymagają:
                    {"\n"}Prawidłowej identyfikacji. W niektórych krajach może być potrzebny również numer ubezpieczenia społecznego. Minimalnej kwoty pieniędzy na otwarcie konta. Może się to różnić w zależności od wybranego banku i konta. Na przykład rachunek oszczędnościowy przeciętnego banku wymaga minimalnego depozytu w wysokości 300 €.
                    {"\n\n"}Wybierz bank, który jest dla Ciebie najlepszy. Skontaktuj się z oddziałem banku w Twojej okolicy, aby przedyskutować, co dokładnie otrzymasz, jeśli otworzysz konto podstawowe. Chociaż banki są różne, można je ogólnie podzielić na dwie ogólne kategorie: banki należące do dużej sieci i mniejsze banki lokalne
                    {"\n\n"}Banki należące do dużej sieci: duże banki mają zwykle oddziały w większości miast w całym kraju. Możesz uniknąć opłat, które będziesz musiał zapłacić za korzystanie z usług innych banków (takich jak opłaty bankomatowe itp.). Duże banki oferują również usługi, takie jak całodobowe linie pomocy dla swoich klientów. Ponadto banki te mają zazwyczaj stabilną, zaufaną reputację.
                    {"\n\n"}Mniejsze banki lokalne: małe banki oferują bardziej osobiste, przyjazne i ludzkie doświadczenia. Mniejsze banki zazwyczaj pobierają mniejsze opłaty za korzystanie z ich usług. Mniejsze banki często inwestują swoje pieniądze w lokalną społeczność.  Z drugiej strony mniejsze banki upadają częściej niż duże banki (jest to jednak bardzo rzadkie).
                    {"\n\n"}Dodatkową opcją są unie kredytowe. SKOK-i to instytucje finansowe typu non-profit, często z misją "zorientowaną na społeczność" i "służącą ludziom, a nie zyskom." SKOK-i z powodzeniem udoskonaliły swoje usługi, współpracując z innymi spółdzielczymi bankami, oferując wspólną bankowość oddziałową i bankomaty.
                    {"\n\n"}Odwiedź swój bank i poproś o otwarcie konta. Otwarcie konta osobiście jest zwykle najlepszą opcją dla posiadaczy kont po raz pierwszy. Możesz poprosić kasjera o odpowiedzi na wszystkie pytania i wątpliwości, które masz. Również proces otwierania konta jest zwykle szybszy osobiście.
                    {"\n\n"}Zadaj wszystkie ważne pytania, zanim sfinalizujesz swoje konto. Poproś o wyjaśnienie wszelkich problemów dotyczących Twojego konta.
                    {"\n\n"}Podaj niezbędne informacje, aby utworzyć konto. Otwarcie rachunku wymaga kilku podstawowych informacji osobistych. Ogólnie rzecz biorąc, dobrze jest mieć: Dowód, że jesteś tym, za kogo się podajesz: miej ze sobą document tożsamości ze zdjęciem (prawo jazdy lub paszport może również wystarczyć).
                    {"\n\n"}Potwierdzenie adresu: rachunek telefoniczny, prawo jazdy lub jakikolwiek inny oficjalny dokument z Twoim imieniem i nazwiskiem.
                    {"\n\n"}Dowód, że jesteś zarejestrowanym obywatelem: bank poprosi Cię o podanie numeru ubezpieczenia społecznego, numeru identyfikacyjnego podatnika lub numeru identyfikacyjnego pracodawcy.
                    {"\n\n"}Przechowuj bezpiecznie otrzymane dokumenty konta. Po zakończeniu zakładania konta otrzymasz dokumenty zawierające ważne informacje o koncie. Przechowuj je w bezpiecznym miejscu. Dobrym pomysłem jest zapamiętanie następujących informacji, abyś nie musiał polegać na dokumentach w przyszłości:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Twój czterocyfrowy numer PIN',
                                val2: 'będzie potrzebny do użyia karty do zakupów.'
                            }
                        },
                        {
                            key: {
                                val1: 'Twój numer konta bankowego',
                                val2: 'potrzebujesz go do zadań finansowych, takich jak bezpośrednie wpłaty'
                            }
                        },
                        {
                            key: {
                                val1: 'Twój numer ubezpieczenia społecznego',
                                val2: 'potrzebujesz go do różnych czynności podatkowych i finansowych w przyszłości'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
