import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie konta e-mail w telefonie komórkowym na urządzeniu z systemem
                    iOS (iPhone, iPad lub iPod touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Krok 2: Naciśnij, aby wybrać "Konta i Menu"</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img9-2.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
