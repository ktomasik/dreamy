import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S3_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Konfigurowanie kont mediów społecznościowych dla firm</Text>
                <Text style={styles.dmlH3}>Tworzenie strony Facebook dla firm</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Krok 3:</Text> kliknij
                    kartę "Strony" w sekcji "Eksploruj" na lewym pasku bocznym na stronie głównej profilu. Z menu
                    wybierz "Utwórz stronę".</Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m9/img1.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
