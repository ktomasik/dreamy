import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Dotacje dla małych firm prowadzonych przez kobiety</Text>
                <Text style={styles.dmlH2}>Dla Słowenii:</Text>
                <Text style={styles.dmlH3}>Slovenski podjetniški sklad (Slovene Enterprise Fund)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Fundusze początkowe dla nowopowstających innowacyjnych przedsiębiorstw (P2A i P2B): kapitał początkowy dla nowych innowacyjnych przedsiębiorstw, bardziej korzystne źródła finansowania dla inwestycyjnych firm (dotacje, gwarancje), promocja inwestycji prywatnych (kapitał własny, pożyczki, gwarancje).',
                                val2: 'https://www.podjetniskisklad.si/en'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Agencija RS za kmetijske trge in razvoj podeželja</Text>
                <Text>Wsparcie dla tworzenia i rozwoju mikroprzedsiębiorstw.</Text>
                <Text style={styles.dmlH3}>Zavod RS za zaposlovanje</Text>
                <Text>Subwencje na samozatrudnienie / okazjonalne udzielanie dotacji na samozatrudnienie.</Text>
                <Text style={styles.dmlH3}>Slovenski regionalno razvojni sklad</Text>
                <Text>Zachęty finansowe, zwłaszcza w formie funduszy zwrotnych, na początkowe inwestycje w dziedzinie
                    przedsiębiorczości, rolnictwa, rozwoju regionalnego, inwestycji finansowych w regionalne programy
                    gwarancyjne, prefinansowania projektów z zatwierdzonymi funduszami europejskimi.</Text>
                <Text style={styles.dmlH3}>SID banka</Text>
                <Text>Zapewnienie korzystnych zasobów finansowych dla firm, działalność ubezpieczeniowa w zakresie
                    eksportu.</Text>
                <Text style={styles.dmlH3}>Eko sklad </Text>
                <Text>Zapewnienie korzystnych środków finansowych na inwestycje w projekty proekologiczne i efektywność
                    energetyczną.</Text>

            </View>
        )
    }
}
