import {createStackNavigator} from 'react-navigation'
import React from 'react';
import {TouchableOpacity} from 'react-native';
import LaunchScreen from '../Containers/LaunchScreen'
import ModuleContent_en from '../Containers/ModuleContent'
import ModuleContent_pl from '../Containers/ModuleContent_pl'
import ModuleContent_tr from '../Containers/ModuleContent_tr'
import ModuleContent_gr from '../Containers/ModuleContent_gr'
import ModuleContent_sl from '../Containers/ModuleContent_sl'
import ModuleContent_fr from '../Containers/ModuleContent_fr'
import TypeContentScreen from '../Containers/TypeContentScreen'
import Playquiz_en from '../Containers/Playquiz'
import Playquiz_pl from '../Containers/Playquiz_pl'
import Playquiz_tr from '../Containers/Playquiz_tr'
import Playquiz_gr from '../Containers/Playquiz_gr'
import Playquiz_sl from '../Containers/Playquiz_sl'
import Playquiz_fr from '../Containers/Playquiz_fr'
import LangMenu from '../Containers/LangMenu'
import ContentTableScreen from "../Containers/ContentTableScreen";
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from "../Services/I18n";
import AboutProject from "../Containers/AboutProject";

function getTitle() {
    var lang = I18n.currentLocale();
    var title;
    switch (lang) {
        case "en":
            title = "About the project";
            break;
        case "pl":
            title = "O projekcie";
            break;
        case "fr":
            title = "À propos du projet";
            break;
        case "gr":
            title = "Σχετικά με το έργο";
            break;
        case "sl":
            title = "O projektu";
            break;
        case "tr":
            title = "Proje hakkında";
            break;
        default:
            title = "About the project";
            break;
    }
    return title;
}

// Manifest of possible screens
const PrimaryNav = createStackNavigator({

    LaunchScreen: {
        screen: LaunchScreen, navigationOptions: ({navigation}) => ({
            title: 'DREAMY M-LEARNING',
            headerBackTitle: null,
            headerStyle: {borderBottomColor: 'black', borderBottomWidth: 2},
            //headerRight: NavIcons.settingsButton(navigation.navigate),
            headerRight: (
                <TouchableOpacity onPress={() => navigation.navigate("LangMenu", {
                    moduleNo: 5
                })}>
                    <Icon name='md-globe' size={32} style={{marginRight: 10}}/>
                </TouchableOpacity>
            )
        }),
    },
    ModuleContent_en: {
        screen: ModuleContent_en, navigationOptions: () => ({}),
    },
    ModuleContent_pl: {
        screen: ModuleContent_pl, navigationOptions: () => ({}),
    },
    ModuleContent_tr: {
        screen: ModuleContent_tr, navigationOptions: () => ({}),
    },
    ModuleContent_gr: {
        screen: ModuleContent_gr, navigationOptions: () => ({}),
    },
    ModuleContent_sl: {
        screen: ModuleContent_sl, navigationOptions: () => ({}),
    },
    ModuleContent_fr: {
        screen: ModuleContent_fr, navigationOptions: () => ({}),
    },
    TypeContentScreen: {
        screen: TypeContentScreen, navigationOptions: () => ({}),
    },
    Playquiz_en: {
        screen: Playquiz_en, navigationOptions: () => ({}),
    },
    Playquiz_pl: {
        screen: Playquiz_pl, navigationOptions: () => ({}),
    },
    Playquiz_tr: {
        screen: Playquiz_tr, navigationOptions: () => ({}),
    },
    Playquiz_gr: {
        screen: Playquiz_gr, navigationOptions: () => ({}),
    },
    Playquiz_sl: {
        screen: Playquiz_sl, navigationOptions: () => ({}),
    },
    Playquiz_fr: {
        screen: Playquiz_fr, navigationOptions: () => ({}),
    },
    ContentTableScreen: {
        screen: ContentTableScreen, navigationOptions: () => ({}),
    },
    LangMenu: {
        screen: LangMenu, navigationOptions: () => ({
            headerBackTitle: null,
        }),
    },
    AboutProject: {
        screen: AboutProject, navigationOptions: () => ({}),
    }

}, {
    // Default config for all screens
    headerMode: 'float',
    initialRouteName: 'LaunchScreen',

});

export default PrimaryNav
