import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S44 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Twitter hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 12: Takip etmekten hoşlanacağınız insanları seçin… ve
                    İleri yi tıklayın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img87.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img88.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
