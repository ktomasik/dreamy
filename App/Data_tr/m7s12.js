import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Hibeleri</Text>
                <Text style={styles.dmlH2}>Slovenya:</Text>
                <Text style={styles.dmlH3}>Slovenski podjetniški sklad (Slovene Enterprise Fund)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Yeni kurulan  yenilikçi şirketler için başlangıç fonları (P2A ve P2B): yeni kurulmuş yenilikçi şirketler için başlangıç sermayesi, kalkınma yatırım firmaları için daha uygun finansman kaynakları (sübvansiyonlar, garantiler), özel yatırımın teşvik edilmesi (hisse senedi, krediler, garantiler).',
                                val2: 'https://www.podjetniskisklad.si/en'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Agencija RS za kmetijske trge in razvoj podeželja</Text>
                <Text>Mikro işletmelerin kurulması ve geliştirilmesi için destek.</Text>
                <Text style={styles.dmlH3}>Zavod RS za zaposlovanje</Text>
                <Text>Serbest meslek sübvansiyonları / serbest meslek için şartlı hibe verilmesi.</Text>
                <Text style={styles.dmlH3}>Slovenski regionalno razvojni sklad</Text>
                <Text>Girişimcilik, tarım, bölgesel kalkınmada başlangıç yatırımları için özellikle de geri dönüşebilir
                    fonlar biçimindeki mali teşvikler, bölgesel garanti programlarına, onaylı Avrupa fonlarıyla ön
                    finansman projelerine finansal yatırımlar,</Text>
                <Text style={styles.dmlH3}>SID banka</Text>
                <Text>Şirketler için uygun finansal kaynaklar sağlamak, ihracat sigortacılığı işletmesi</Text>
                <Text style={styles.dmlH3}>Eko sklad </Text>
                <Text>Çevreye yönelik projelere ve enerji verimliliğine yatırım yapmak için uygun finansal kaynaklar
                    sağlamak.</Text>

            </View>
        )
    }
}
