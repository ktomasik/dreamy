import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S3_2 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Cepte Faturalandırma</Text>
                <Text style={styles.dmlH3}>IOS için</Text>
                <Text style={{marginTop: 10,fontSize: 18}}><Text style={{fontWeight:'bold'}}>Adım 2:</Text> Seçilen şablonu yüklemek için ‘indir’e tıkla sonra Apple Kimliği için şifreyi gir.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_20_3319624.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
