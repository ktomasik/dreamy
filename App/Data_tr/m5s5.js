import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>E-Ticaret Ödeme Sistemi</Text>
                <Text style={{marginTop: 15}}>E-Ticaret Ödeme sistemi, çek ya da nakit kullanmadan elektronik ortamda
                    işlem yapma veya mal ve hizmetler için ödeme yapma yoludur. </Text>
                <Text style={styles.dmlH3}>Otomatik Ödeme Ayarlaması Nedir?</Text>
                <Text>Otomatik fatura ödemesi yinelenen bir faturanın ödenmesi için önceden belirlenmiş bir tarihte
                    yapılan para transferidir. Otomatik fatura ödemeleri banka, aracı kurum veya yatırım fonu hesabından
                    satıcılara yapılan rutin ödemelerdir. Genellikle ödemeyi alan firma ile yapılmasına rağmen çek
                    hesabının internetten ödeme hizmet ile de otomatik ödeme planlamak mümkündür.</Text>
                <Text style={styles.dmlH3}>Kredi kartı, Banka Kartı ya da Pay Pal ile internetten ödeme kabul
                    etme</Text>
                <Text>Müşterilere kredi kartlarıyla web sitesinde ödeme yapabilmelerini sağlamak, eletronik ödemeleri
                    kabul etmenin en temel yoludur. Bu özelliği müşterilere sunmak için, satıcının kendi özel ticari
                    hesabına ya da ara bekleme hesabına sahip olup olmayacağına karar vermesi istenir.
                    {"\n\n"}Websitesinde satılan ürünler, abonelik ve hizmetler için elektronik kredi kartı ödemesi
                    Kabul etmeyi isteyen küçük işletmeler ya da kurumlar tarafından herhangi bir websitesine PayPal
                    düğmeleri eklenebilinir.
                </Text>
                <Text style={styles.dmlH3}>Güvenli internet ödeme sistemlerinin şartları</Text>
                <Text>İnternet güvenliği tüketici olarak herkesi ilgilendiren bir husustur. Hatta işletme sahipleri
                    için daha önemlidir. Elektronik ödeme alarak, müşteri veri korunması sorumluluğu alınmış olur, ve
                    bunun güvenli yönetilmesi masraflı bir yük olabilir. Ancak PCI uyumlu ödeme çözümü kullanılmasıyla,
                    kolaylık sağlanabilir. PCI uyumluluğu veri korumasını yöneten kurallar ve yönetmelikleri ifade
                    eder.</Text>
                <Text style={styles.dmlH3}>Kredi kartı, fatura ve banka ile elektronik ödeme çözümleri</Text>
                <Text>Elektronik ödemeler 13 anında yapılır, bundan dolayı uygundur ve çok zaman kazandırır.
                    {"\n"}“Elektronik cüzdanlar” olarak adlandırılanlar müşterilere aşağıdakileri yapmalarına izin
                    verir:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Kredi kart bilgilerini vererek elektronik ödeme'},
                        {key: '2. Fatura ödeme'},
                        {key: '3. Banka hesabına ödeme '}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH3}>Sanal Satış Noktası Nedir (POS)?</Text>
                <Text>Sanal POS Terminali. İnternet ortamı satıcıları ve hizmet satıcılarının, alıcılar tarafından
                    başlatılan kart işlemlerini manuel olarak yetkilendirmesini sağlayan ödeme yöntemidir. Bu süreç ek
                    güvenlik sağlarken ödeme kaynaklarını büyük ölçüde genişletir ve ödeme süreci süresini azaltır.
                    Sanal POS terminalinin entegrasyonu çok sayıda e-ticaret platformunda mümkündür.</Text>
            </View>
        )
    }
}
