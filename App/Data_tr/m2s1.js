import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Networking nedir? Bu size ne anlam ifade ediyor?</Text>
                <Text>Oxford Sözlüğü’ne göre (‘İngilizce Sözlük, Thesaurus, & Grammar Help – Oxford Dictionaries’ 2018)
                    Ağ İletişimi (Networking) “profesyonel veya sosyal amaçlarla bilgi, bağlantı ve deneyim paylaşan bir
                    grup insan”dır. Bununla birlikte on farklı insana ağ iletişimi nedir sorusu yönelttiğinizde on
                    farklı cevap almanız mümkündür. Ağ iletişimi nedir sorusuna bir insanın vereceği cevap büyük
                    ihtimalle onun bu önemli kişisel ve profesyonel faaliyeti kullanımı ile ilgilidir. Fakat bu networkü
                    ne için kullanıyor olursanız olun (arkadaş edinmek, yeni iş bulmak, mevcut kariyerinizi geliştirmek,
                    yeni kariyer seçenekleri bulmak, satış ilanları vermek ya da sadece profesyonel ufku geliştirmek),
                    önemli olan ağ iletişiminin bilgi, bağlantı ya da deneyim değişimi olduğuna odaklanmaktır. Hangi
                    sektör ve kariyer basamağında olursa olsun, ağ iletişimi kurmak (networking) kişisel olarak
                    bağlantılar kurmanızda, destekleyici ve karşılıklı yarar sağlayacağınız ilişkiler geliştirmenizde ve
                    yeni şeyler keşfetmenizde size yardımcı olacaktır. Bu 21. yüzyılın profesyonel kadınlarında olmazsa
                    olmaz bir beceri setidir.
                    {"\n\n"}Hangi sektörde olursanız olun diğer doğru kişileri tanımak önemlidir. Bu iyi bir şekilde
                    yapıldığında, ağ kurma (networking) kariyer basamaklarını tırmanmakta daha çok imkan tanır, daha çok
                    müşteri edinmenizi sağlar ve daha geniş bir alana yayılmanıza yardımcı olur. Ve iş arayanlar için,
                    networking, sizi reklamınızı yapmadan gerekli rollere sokar ya da roller için karar verici olanlarla
                    iletişim kurmanızı sağlar (‘What Is Networking? | Reed.co.uk’, n.d.).
                </Text>
                <Text style={styles.dmlH2}>Sosyal Medya Nedir?</Text>
                <Text>Sosyal medya, fikirlerin ve bilgilerin paylaşımını ve sanal ağların ve toplulukların kurulmasını
                    kolaylaştıran bilgisayar tabanlı bir teknolojidir. Tasarım gereği sosyal medya internet tabanlıdır
                    ve kullanıcılara kişisel bilgiler, videolar ve fotoğraflar gibi diğer içeriklerin kolayca elektronik
                    olarak iletilmesini sağlar. Kullanıcılar sosyal medyaya web tabanlı yazılım veya web uygulaması
                    aracılığı ile bilgisayar, tablet veya akıllı telefon kullanarak katılırlar ve bunu genellikle
                    mesajlaşmak için yaparlar.
                    {"\n\n"}Sosyal medya, insanların arkadaşlarıyla ve aileleriyle etkileşime girdikleri bir araç olarak
                    ortaya çıkmıştır ancak daha sonra popüler yeni bir iletişim yönteminden yararlanmak isteyen
                    işletmeler tarafından müşterilere ulaşmak amacıyla benimsenmiştir. Sosyal medyanın gücü dünya
                    üzerinde yaşayan herhangi biriyle (ya da birçok insanla) onların da sosyal medya kullanmaları
                    halinde iletişim kurma ve bilgi paylaşımı olanağı sağlamasından kaynaklanır (Silver, n.d.).
                </Text>
                <Text style={styles.dmlH2}>Sosyal Medyanın Kısa Tarihçesi</Text>
                <Text>Sosyal medyanın tarihi 1970’lere dayanmaktadır (Ries 2016). ARPANET 1969 yılında ilk defa
                    çevrimiçi olarak ortaya çıkmış ve 1970’lerin sonuna kadar gayriresmi/iş fikirlerinin ve iletişimin
                    zengin kültürel değişimini geliştirmiştir. Bu ARPANET# kurallar ve görgü kuralları “1982- MIT AI Lab
                    ağ oluşturma görgü kuralları el kitapçığı” kitabında açıkça görülmektedir ve “sosyal medya” nın
                    günümüzdeki tanımıı burada bulunan bir makale ile yer almaktadır. 1979'da ortaya çıkan Usenet,
                    1973'te Topluluk Belleği olarak bilinen elektronik bülten panosu sisteminin (BBS) bir öncüsü
                    tarafından ortadan kaldırılmıştır. İlk defa 16 Şubat 1978’de çevrimiçi olarak ortaya çıkan ve
                    Bilgisayar Bülten Panosu Sistemi ile birlikte gelen gerçek elektronik bülten pano sistemleri
                    Şikago’da kullanılmıştır. Çok geçmeden çoğu ana şehirde birden fazla BBS TRS-80, Apple II, Atari,
                    IBM PC, Commodore 64, Sinclair, ve benzeri kişisel bilgisayarlarda kullanılıyordu (‘Social Media -
                    Wikipedia’, n.d.).
                    {"\n\n"}IBM bilgisayarı 1981 yılında tanıtıldı ve Mac modelleri ve bilgisayarlar 1980’li yıllar
                    boyunca kullanıldı. Özel telekomünikasyon donanımı tarafından takip edilen çoklu modemler, birçok
                    kullanıcının aynı anda çevrimiçi olmasına izin verdi. En büyük BBS şirketlerinden üçü olan
                    Compuserve, Prodigy ve AOL, 1990'larda internete ilk girenlerdi. 1980 ortaları ile 1990 ortaları
                    arasında, BBS’ler sadece Kuzey Amerika’da on binlere ulaştı (Edvards 2016). Mesaj forumları (sosyal
                    medyanın özel bir yapısı) 1980'lerde ve 1990'ların başlarında BBS fenomeniyle ortaya çıktı. İnternet
                    1990'lı yılların ortalarında çoğaldığında, mesaj forumları çevrimiçine taşındı ve internet forumları
                    haline geldi; bu, özellikle kişi başına daha ucuz erişimin yanı sıra, telco modem bankalarından çok
                    daha fazla kişiyi aynı anda idare edebilme özelliği nedeniyle ilgi gördü (‘Social Media -
                    Wikipedia’, n.d.).
                    {"\n\n"}GeoCities Kasım 1994’te ortaya çıkmış internetin ilk sosyal medya ağ iletişimi
                    websitelerinden biridir; bunu Aralık 1995’te Classmates, Mayıs 1997’de Six Degrees, Ekim 1998’de
                    Open Diary, Nisan 1999’da LiveJournal, Ekim 2001’de Ryze, Mart 2002’de Friendster, Mayıs 2003’te
                    LinkedIn, Haziran 2003’te hi5, Ağustos 2003’te MySpace, Haziran 2004’te Orkut, Şubat 2004’te
                    Facebook, Mart 2005’te Yahoo! 360°, Temmuz 2005’te Bebo, Temmuz 2006’da Twitter, Şubat 2007’de
                    Tumblr, ve Temmuz 2010’da Google+ takip etmiştir (‘December 1995: Classmates - Then and Now: A
                    History of Social Networking Sites - Pictures - Cbs News’, n.d.; ‘History and Different Types of
                    Social Media’, n.d.; Ortutay 2012).
                </Text>
            </View>
        )
    }
}
