import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme Instagram hesabı</Text>
                <Text styl={{marginTop: 10}}>Yukarıdaki talimat doğrultusunda Facebook’ta işletme hesabı açmışsanız
                    işletme açısından Instagram daha kolaydır ve diğer sitelerde daha az vakit geçirme olanağı sağlar.
                    İsterseniz hem Facebook hem Instagram hesabını kullanarak müşteri ağınızı
                    genişletebilirsiniz.</Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
