import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S13 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Hibeleri</Text>
                <Text style={styles.dmlH2}>Türkiye:</Text>
                <Text style={styles.dmlH3}>KOSGEB</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Küçük ve Orta Ölçekli İşletmeler Geliştirme Organizasyonu (KOSGEB), KOBİ politikalarının geliştirilmesi, koordinasyonu ve uygulanmasından sorumlu ana organdır. KOSGEB kendi işini kurmak isteyen kadın girişimcilere devlet kredisi desteği vermektedir. Bu kapsamda KOSGEB kredisi uygulaması kapsamında 50.000 TL kredi desteği sağlanmaktadır. Devlet tarafından verilen bu destek tamamen geri ödemesizdir.  Bu hibe desteğine yanısıra, kadınlara faizsiz  kredi desteği verilmektedir. 2018 itibariyle bu kredi 100.000 TL\'dir. Kadın girişimcilerin ilk işleri  ilk üç yıl boyunca faizden muaf olur.',val2: 'https://www.kosgeb.gov.tr/site/tr/genel/detay/6057/kadin-girisimciligi-women-entrepreneurship'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
