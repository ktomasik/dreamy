import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kredi Kartıyla Ödeme Kabul etme</Text>
                <Text style={{marginTop: 15}}>Kredi Kartı ile ödeme kabul etmek için aşağıdaki talimatları izleyin
                    {"\n"}Kredi Kartı ile ödeme kabul etmek için gerekli olanlar :
                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Satıcı Hesabı:',
                                val2: 'Kredi ve banka kartı ödemelerinin yatırıldığı bir banka hesabı türü'
                            }
                        },
                        {
                            key: {
                                val1: 'Sanal Terminal:',
                                val2: 'Dijital kredi kartı pos makinesi gibi, bu sistem bilgisayarınızda kredi kartı bilgilerini girmenizi sağlar'
                            }
                        },
                        {
                            key: {
                                val1: 'Ağ Geçidi:',
                                val2: 'Ödeme bilgilerini güvenli bir şekilde onaylayan veya reddedilen olarak gönderen internet mağazası ile banka arasındaki bağlayıcı.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />

                <Text style={{marginTop: 10}}>Adım Adım Nasıl Çalışır?</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Adım 1:',
                                val2: 'Bir müşteri size ulaştığında, sanal terminalinize giriş yapar ve satın almak istediği ürünleri veya hizmetleri seçer. Daha sonra kredi kartı bilgilerini girmelidir.'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 2:',
                                val2: 'Ödeme bilgileri güvenli ödeme ağ geçidinden geçer ve yetki kaynağına iletilir.'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 3:',
                                val2: 'Kredi kartını veren banka işlem bilgilerini alır, fonların kullanılabilir olup olmadığını kontrol eder ve ödemenin serbest bırakılmasını onaylar veya reddeder.'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 4:',
                                val2: 'Ödeme kabul edildiğinde veya reddedildiğinde ödeme ağ geçidi size “söyler”.'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 5:',
                                val2: 'Onaylanırsa, para 2-3 gün içinde banka hesabınıza yatırılır.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />
            </View>
        )
    }
}
