import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Giriş</Text>
                <Text>Bu modüle kadar e-posta hesabı açma ve akıllı telefonlarda kurulumunu öğrenmiş bulunmaktasınız.
                    Bunun haricinde sosyal medya hesabı açmak da öğrendikleriniz arasında yer almaktadır. Diğer yandan
                    el işlerinizin fotoğrafını çekmeyi ve çeşitli platformlarda yayınlamayı da öğrenmiş bulunmaktasınız.
                    Bu modülün amacı mevcut sosyal medya hesaplarını işletmeniz için nasıl ayarlayacağınızı
                    açıklamaktır.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
