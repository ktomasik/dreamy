import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Çevrimiçi çevrede en iyi fiyattan satış yapmak istiyorsanız aşağıdaki adımları izleyin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 4: 2 ya da daha fazla ürün alınması halinde indirim olacağını belirtin ve indirim değerini yazın</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/24911.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
