import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Facebook hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 3: Facebook uygulamasını açın ve YENİ FACEBOOK HESABI
                    OLUŞTUR a tıklayın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 400}}
                        source={require('../Images/m2/img7.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
