import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";


const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img18.png')
    }
}];

export default class M6S4_9 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Google Docs’da fatura oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 9:</Text> Bu sizi
                    şablonlar sayfasına getirir. E çizelgeye metin ekleyebilir, düzenleyebilir ya da
                    biçimlendirebilirsiniz. Ayrıca dosya ve klasörleri insanlarla paylaşabilir ve bunları görüntüleyip,
                    düzenleyip ya da üzerinde yorum yapıp yapamacaklarını seçebilirsiniz.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 280}}
                        source={require('../Images/m6/img18.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
