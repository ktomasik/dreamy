import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sözleşme Bilgileri</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım
                    3:</Text> Belirttiğiniz ürün için yapmanız gereken son adım satın almaktır. İnternetten satın almak
                    için sigorta sorgulama işlemini yapmanız gerekir.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_7_3319599.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
