import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S26 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Kadınlar için Mikro Finans ve Diğer Girişimci Fonları</Text>
                <Text style={styles.dmlH2}>Polonya:</Text>
                <Text style={styles.dmlH3}>STARTUP ACADEMY</Text>
                <Text>Eğitim, mentorluk, yenilikçi girişimcilik yöntemleri, hızlanma programları</Text>

                <Text style={styles.dmlH3}>TWÓJ STARTUP</Text>
                <Text>kuluçka öncesi, hukuk ve muhasebe danışmanlığı, BT ve pazarlama danışmanlığı, eğitim</Text>

                <Text style={styles.dmlH3}>Inkubator Technologiczny Podkarpckiego Parku Naukowo-Technologicznego</Text>
                <Text>ofis odaları, danışmanlık hizmetleri, geliştirme desteği</Text>

                <Text style={styles.dmlH3}>Przedsiębiorcze kobiety 2.0</Text>
                <Text>Proje, çalışmayan kadınlara kendi işlerini kurmalarına yardımcı olmayı ele almaktadır.</Text>

                <Text style={styles.dmlH3}>AIP</Text>
                <Text>İş Danışmanlığı, Mentorluk ve koçluk, Muhasebe hizmeti, Yasal destek, Girişimci Eğitimi</Text>
            </View>
        )
    }
}
