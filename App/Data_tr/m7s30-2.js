import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

//                   \/
export default class M7S30_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Proje ortağı ülkelerde adım adım finansman</Text>
                <Text style={styles.dmlH2}>Fransa:</Text>
                <Text style={styles.dmlH3}>Eğer iş aramıyorsanız DEVLET YARDIMI ACCRE</Text>

                <Text style={styles.dmlH3}>Adım 2:</Text>
                <Text>Buna karşılık, ACCRE başvurunuzun tescili için bir makbuz düzenler, isteğiniz sosyal
                    organizasyonlarını bilgilendirir ve talebi 24 saat içinde yetkili Urssaf'a gönderir. URSSAF talep
                    üzerine 1 ay içerisinde karara verir.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
