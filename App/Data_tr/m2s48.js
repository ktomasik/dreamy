import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S48 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sosyal ağ iletişim sitelerini kullanma</Text>
                <Text style={{marginTop: 10}}>İnsanları, işinizin gelişiminden veya ürünlerinizden haberdar etmek çok
                    önemli bir iştirç Çoğu insan bunu çeşitli sosyal ağ siteleri ya da platformlar aracılığı ile yapmayı
                    tercih etmektedir. Bunların arasında en popüleri, günümüzde anlatılmasına bile gerek olmayan
                    Facebook’tur.</Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 300, height: 300}}
                        source={require('../Images/images/1016.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}

