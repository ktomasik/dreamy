import React, {Component} from 'react'
import {Text, Image, View, Linking, TouchableHighlight, Modal} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen10 = [{
    url: '',
    props: {
        source: require('../Images/m3/img59.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m3/img60.png')
    }
}];

export default class M3S13_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Bir web sitesi kurmak istiyorsanız aşağıdaki adımları takip edin</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen10}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 4: ”Kaydol” a basın ve web sitenizin demonstrasyonuna başlayın, ilk önce ekrandaki soruları yanıtlayın ve daha sonra “Şimdi Başla”ya basın:</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img59.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img60.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
