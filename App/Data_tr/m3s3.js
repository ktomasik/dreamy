import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Marka logosu nedir?</Text>
                <Text>Bir logo, kamu farkındalığını desteklemek ve yükseltmek için kullanılan grafik işareti, amblem ya
                    da semboldür. Soyut veya figüratif bir tasarıma sahip olabilir ya da temsil ettiği ismi logo şekli
                    ya da kelime ile ifade eder.</Text>
                <Text>Bir marka, bir işi, ürünü ya da hizmeti tanımlama ve diğerlerinden farklılaştırmayı sağlayan isim
                    ya da tasarımın pazarlama uygulamasıdır.</Text>

                <Text style={styles.dmlH2}>Doğru logo tasarımı nasıl seçilir</Text>
                <Text>Logo tasarımına karar verirken, işinize hakettiği marka kimliğini verecek bazı şeyleri aklınızda
                    bulundurmanız gerekir. Eğer işiniz için yaratıcı bir logo istiyorsanız, öncelikle başarılı
                    markaların konseptlerine bakarak ilham alabilirsiniz.</Text>

                <Text style={styles.dmlH2}>E-ticaret için fiyatlandırma stratejisi (ürününüzü fiyatlandırma)</Text>
                <Text>Çevrimiçi alışveriş ortamında, ürünle fiyatı birlikte görmek en iyisidir. Müşteri için, iyi dizayn
                    edilmiş bir platformda ürünü fiyatı ile birlikte görmek ürünü satın almasını kolaylaştırır.</Text>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img16.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={styles.dmlH2}>Farklı fiyatlandırma stratejileri</Text>
                <Text style={styles.dmlH3}>Maliyet tabanlı e-ticaret fiyatlandırması</Text>
                <Text>Maliyet tabanlı fiyatlandırma, parekende sektöründeki belki de en popüler fiyatlandırma modelidir.
                    Satılan her ürün için minimum getiri olduğundan emin olunur.</Text>
                <Text style={styles.dmlH3}>Rakip tabanlı e-ticaret fiyatlandırması </Text>
                <Text>Rakip tabanlı fiyatlandırma stratejisinde belli bir ürün için doğrudan rakiplerinizin
                    fiyatlandırma stratejilerini takip eder ve fiyatınızı onlarınkine göre belirlersiniz. </Text>
                <Text style={styles.dmlH3}>Değer tabanlı e-ticaret fiyatlandırması</Text>
                <Text>Eğer müşteriye teslim edebileceğiniz değere odaklanırsanız, fiyatlar alış veriş yapanı –
                    bulunduğunuz sanayi segmentine göre- nasıl algıladığınıza göre şekillenir. Ödeme belli ürün için
                    belli zamanda yapılır. Bu durumda e-ticaret fiyatlandırmasında değer tabanlı ya da değer
                    optimizasyonlu yaklaşım kullanılır. (Roggio, 2017).</Text>
            </View>
        )
    }
}
