import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M8S8_3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Kargo gönderisi</Text>
                <Text style={styles.dmlH3}>ADIM 3: KARGO ŞEKLİ VE ŞİRKETİNİ BELİRLEYİN</Text>
                <Text>Ulusal ve uluslarası anlamda gönderi yapabilen Türkiye’deki bazı kargo firmaları ve web adresleri aşağıdaki gibidir:</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Aras Kargo', val2: 'https://www.araskargo.com.tr'}},
                        {key: {val1: 'Fedex Kargo', val2: 'http://www.fedex.com/tr/'}},
                        {key: {val1: 'Mng Kargo', val2: 'http://www.mngkargo.com.tr/tr'}},
                        {key: {val1: 'Ptt Kargo', val2: 'http://www.ptt.gov.tr/ptt'}},
                        {key: {val1: 'Sürat Kargo', val2: 'http://www.suratkargo.com.tr'}},
                        {key: {val1: 'TNT Kargo', val2: 'https://www.tnt.com'}},
                        {key: {val1: 'UPS Kargo', val2: 'http://www.ups.com.tr'}},
                        {key: {val1: 'Yurtiçi Kargo', val2: 'http://www.yurticikargo.com'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key.val1}: <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}

