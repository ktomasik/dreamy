import React, {Component} from 'react'
import {SectionList, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Yeni başlayan kadınlar için sosyal medya pazarlaması</Text>
                <Text style={styles.dmlH3}>Hedefler</Text>
                <Text>Sosyal medya stratejisi hakkında iyi bir makale Alex York (2018) tarafından yazılmıştır. Alex York
                    işletmenizi geliştirmek için en önemli şeyin istikrarlı bir şekilde hedeflerinizi takip etmek
                    olduğunu vurgulamıştır. Bu hedefleri yazmak, hedeflerin tanımlanmış ve gerçekçi olmasına yardımcı
                    olacaktır.{"\n"}
                    Hedef belirleme, tüm pazarlama ve işletme stratejilerinin temelini oluşturur. Sosyal medya istisna
                    değildir. Tabii ki, bir dizi sosyal yetenekle, hedeflerinizin tam olarak ne olması gerektiğini
                    belirlemek zor olabilir. Rehberlik etmesi açısından aşağıda bazı sosyal medya hedefleri
                    belirtilmiştir:
                </Text>
                <SectionList
                    sections={[
                        {
                            title: 'Marka bilinirliğini arttırma:',
                            data: ['Otantik ve kalıcı bir marka bilinirliği oluşturmak için, bir çok promosyon mesajından kaçının. Bunun yerine, sosyal medya kanallarınız aracılığıyla güçlü bir marka kişiliğine ve anlamlı içeriğe odaklanın. sosyal kanallarınız üzerinden odaklanın.']
                        },
                        {
                            title: 'Yüksek satış kalitesi:',
                            data: ['Belirli anahtar kelimeleri, cümleleri veya hashtagleri izlemeden veya dinlemeden sosyal kanallarınız arasında gezinmek neredeyse imkansızdır. Daha etkili sosyal medya hedeflemesi sayesinde, temel hedef kitlenize çok daha hızlı bir şekilde ulaşırsınız.']
                        },
                        {
                            title: 'Kişisel satışları arttırma:',
                            data: ['Bazı perakendeciler, mağaza içi satışları artırmak için sosyal medya pazarlama çabalarına güvenir. Markanızın tanıtımını yeterince yapıyor musunuz? Müşterilerinizi mağazanızda neler olup bittiği konusunda bilgilendirmeye ne dersiniz?']
                        },
                        {
                            title: 'Yatırım getirisini geliştirme:',
                            data: ['Her marka sosyal medyada yatırım getirisini arttırmak ister. Fakat sosyal medyada bu hedef, işçilik, reklam ve tasarım maliyetlerinin istendiği gibi olduğundan emin olmak, ve kanalların muhasebe denetiminin gerçekleştirilmesi için özeldir.']
                        },
                        {
                            title: 'Sadık bir hayran kitlesi yaratmak:',
                            data: ['Markanız kullanıcı katkılı içeriği destekliyor mu? Takipçileriniz herhangi bir uyarım almadan olumlu tepki veriyor mu? Sosyal olarak pozitif marka kişiliği oluşturmak zaman ve çaba gerektirir.']
                        },
                        {
                            title: 'Sektörde nabzını tutmak:',
                            data: ['Rakipleriniz işe yarar görünen neler yapıyor? Katılım ya da satışlarını arttıran hangi stratejileri kullanıyorlar? Sektörün nabzını tutmak çabalarınızı geliştirmek ve ipucu edinmek için size yardımcı olur.']
                        }

                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Kitle</Text>
                <Text>Ürünlerimizi nerede yayınlayacağımız ile ilgili birçok seçeneğimiz vardır. Bir veya birkaçını
                    seçerken kendimize mutlama şu soruyu sormalıyız: “Hedef grubumuz kimlerdir?”. Cevabı bulduğumuzda
                    farklı sosyal medyaları kullanan insanların istatiksel demografik verilerini takip
                    edebiliriz::</Text>
                <SectionList
                    sections={[
                        {
                            title: 'Facebook’un en popüler demografisi:',
                            data: ['Kadın kullanıcılar (%89)', '18-29 yaş arası kullanıcılar (%88)', 'Kentsel ve kırsal alanda yerleşik kullanıcılar (her biri % 81)', '30,000 $’da az kazananlar (%84)', 'Kolej deneyimine sahip kullanıcılar (%82)']
                        },
                        {
                            title: 'Instagram’ın en popüler demografisi:',
                            data: ['Kadın kullanıcılar (%38)', '18-29 yaş arası kullanıcılar (%59)', 'Kentsel ve kırsal alanda yerleşik kullanıcılar (%39)', '30,000 $’da az kazananlar (%38)', 'Kolej deneyimine sahip kullanıcılar (%37)']
                        },
                        {
                            title: 'Twitter’ın en popüler demografisi:',
                            data: ['Kadın kullanıcılar (%25)', '18-29 yaş arası kullanıcılar (%36)', 'Kentsel alanda yerleşik kullanıcılar (%26)', '50,000-74,999 $ arası kazanan kullanıcılar (%28)', 'Kolej deneyimine veya daha fazlasına sahip kullanıcılar (%29)']
                        }
                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{'\u2022'}{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Benzer ürünler</Text>
                <Text>İçerik oluşturmaya başlamadan önce rakiplerinizi araştırmak akıllıca olacaktır. Bunu içerik
                    oluşturma sürecinden önce yapın çünkü rakiplerinizi başarılı yapan şeyleri analiz etmek içeriğinize
                    yeni yollardan bakmanızı sağlar. Rakiplerinizi bulmanın en kolay yolu basit bir Google araması
                    yapmaktır. Kimlerin olduğunu bulmak için en değerli anahtar kelimeleri, kelime gruplarını ve sanayi
                    terimleri için arama yapın.</Text>
                <Text style={styles.dmlH3}>İçerik</Text>
                <Text>Yeni başlayanlar için, marka kimliğinizle uyumlu bir içerik oluşturmanız tavsiye edilir. Bu tam
                    bir strateji oluşturmadan popüler olmayan demografik gruba ulaşmadan kaçınmak anlamına gelir. İçerik
                    mutlaka ilgili olmalıdır ve yayınlanan sayfa reklamlarla dolu olmamalıdır. Çevrimiçi alışveriş
                    yapanlar video içeriklerine sadece resimlerin olduğu içeriklerden daha çok inanmaktadırlar. Eğer
                    mümkünse hazırlanmış temalar kullanın. İçerik formatını, okuyucularınızın kafasını karıştırmamak
                    için tutarlı ve basit tutun.</Text>
                <Text style={styles.dmlH3}>Göz ardı ETMEYİN</Text>
                <Text>Sosyal medya kanalları ağ iletişimi üzerine kurulur. Bu, amaçlarının sohbet etmek, konu tartışmak
                    ve içerik tartışmak için alan oluşturmak anlamına gelir. Markanız “ağ iletişiminin” bu temelini
                    unutmamalıdır ve konuşmaların ya da katılım fırsatlarının gözetimsiz bırakılmaması çaba gerektirir.
                    {"\n"}Sosyal medya aracılığıyla, marka olarak sadece varolarak ve topluluğunuzla iletişim kurarak
                    saygı kazanırsınız. Bu nedenle sosyal müşteri hizmetleri, kitle bilincini arttırmak isteyen markalar
                    için çok önemlidir. Herşey katılım ile ilgilidir.
                </Text>
            </View>
        )
    }
}
