import React, {Component} from 'react'
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M6S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>3. İnternet alışverişinde alıcı-tedarikçi ilişkilerinde sözleşme
                    oluşturma</Text>
                <Text style={{marginTop: 15}}>İnternetten alışveriş geleneksel alışverişten farklı olduğu için,
                    özellikle internet üzerinden yapılan işlemler Mesafeli Sözleşme Yönetmeliğine tabidir. Bir arada
                    olmayan bir tüccar ve tüketici arasında yapılan bu sözleşme, internet üzerinden, posta ya da telefon
                    gibi bir ya da daha fazla organize uzaktan iletişim aracıyla görüşülür ve kabul edilir.</Text>
                <View style={styles2.border}>
                    <Text>Çoğunlukla, internetten alımlarla ilgili bir sözleşme ilişkisinin oluşturulmasındaki genel
                        ilkeler, satıcı ve alıcı arasındaki genel eylemlerden sonra uygulanabilir:</Text>
                    <FlatList
                        data={[
                            {key: 'Birşey almaya davet: E-ticaret sitesinde satılık malları göstermek genellikle teklif etmek yerine satın almaya davet olarak görünür. Bu durum geleneksel tuğla ve harç mağazasına benzer. Teklif satıcıdan ziyade siteyi ziyaret eden müşterinin eylemleriyle gerçekleşir.'},
                            {key: 'müşteri teklifi bildirir : websitesindeki mal ve hizmetlerin reklamı teklif olmadığından, teklif etme uyarısı alıcıdadır ve müşteri teklifi elektronik ortamda websitesinde yapabilir reklamı yapılan ürünü satın almak istediğini bildirerek'},
                            {key: 'Satıcı teklifi kabul eder: müşteri tarafından teklif yapılır yapılmaz, kabul ettiğini müşteriye bildirerek teklifi şartsız ve koşulsuz kabul etmek satıcıya bağlıdır.'}
                        ]}
                        renderItem={({item}) => <Text
                            style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                    />
                    <Text style={{marginTop: 15}}>Bir müşterinin teklifinin satıcı tarafından kabul edilmesi üzerine,
                        sözleşme ilişkisi oluşturulmuştur.</Text>
                    <Text><Text style={{fontWeight: 'bold'}}>Kaynak: </Text><Text style={{color: 'blue'}}
                                                                                  onPress={() => Linking.openURL('http://www.findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f.aspx')}>findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f</Text></Text>
                </View>
                <Text style={{marginTop: 15}}>Tüketici ve satıcı arasında internette bir sözleşme oluşturulmadan önce,
                    mal ve hizmetlerin tedarikçisi yönetmelikte yeralan konular hakkında tüketiciyi bilgilendirmelidir.
                    Sözleşmenin kurulmasından önce yönetmelik kapsamında satıcı tarafından tüketiciye verilmesi gereken
                    bazı bilgiler aşağıdaki gibidir:</Text>
                <FlatList
                    data={[
                        {key: 'Satıcı ya da tedarikçinin  isim, ünvan, adres, telefon ve diğer erişim bilgileri'},
                        {key: 'Sözleşmeye konu olan mal ve hizmetlerin temel özellikleri'},
                        {key: 'Tüm vergiler dahil olmak üzere mal ve hizmetlerin satış fiyatları'},
                        {key: 'Teslim bedeli  varsa'},
                        {key: 'ödeme ve teslimat veya işi yapma bilgileri'},
                        {key: 'cayma hakkının kullanım koşulları'}
                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Bu ön bilgiler, tüketici tarafından internette işlem yapılmadan önce
                    onaylanmalıdır, aksi takdirde sözleşme ilişkisi kurulmamış olur.Bu ön bilgiler, tüketici tarafından
                    internette işlem yapılmadan önce onaylanmalıdır, aksi takdirde sözleşme ilişkisi kurulmamış
                    olur.</Text>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
