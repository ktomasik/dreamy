import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Satış Kredi Kartı İşlem Noktası</Text>
                <Text style={{marginTop: 15}}>Satış kredi kartı işlem noktası için aşağıdaki talimatları izleyin:</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1. Öncelikle,
                    işiniz için doğru satış noktası sistemini seçin.</Text>
                <Text>Bir satış noktası (POS) sistemi seçiminde birçok husus vardır. Öncelikle, ödemeleri nasıl kabul
                    etmek istediğiniz hakkında düşünmek istersiniz. İşinizin çoğunu mağazada veya internette yoksa her
                    ikisinin bir kombinasyonunu mu yapacaksınız? Ödemeyi site dışında almak ister misiniz? POS donanımı,
                    yazılımı ve barkod tarayıcıları ve makbuz yazıcıları gibi elektronik ekipmanlar satın almak için
                    bütçeniz nedir? Gelecekte işinizi büyütme planlarınız neler?
                    {"\n\n"}Tüm bu soruların cevapları işletmenize en uygun olan sisteme yönlendirmede yardımcı
                    olacaktır.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2. Uyumlu bir
                    ödeme işlemcisi veya ödeme ağ geçidi seçin</Text>
                <Text>Aynı zamanda hangi ödeme işlemcisi veya ödeme ağ geçidiyle birlikte çalışacağınıza da karar
                    vermelisiniz. Ödeme ağ geçidi, ağ geçidi bu işlemci ile çalıştığı sürece, seçtiğiniz ödeme
                    işlemcisine bağlanmanızı sağlayan üçüncü tarafın barındırdığı ödeme çözümüdür.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3. Çözümünüzün
                    işlemleri güvenli tutmak ve müşterilerinizi ve işinizi bir ihlalden korumak için gerekli güvenlik
                    özelliklerini içerdiğinden emin olun.</Text>
                <Text>Bir veri güvenliği ihlali sadece iş için kötü değil, yıkıcı olabilir. Bu nedenle, ilk etapta bir
                    ihlalin gerçekleşmesini önlemek için elinizden gelenin en iyisini yapmak sizin çıkarınıza olacaktır.
                    {"\n"}Ödeme güvenliği, hem teknoloji çözümlerini hem de en iyi uygulamaları kapsayan çok yönlü bir
                    yaklaşımı gerektiriyor. Ödeme güvenliği en iyi bir ödeme işleme sağlayıcısı ile ortaklık içinde
                    gerçekleştirilir.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>4. Ödeme
                    çözümünüzde istediğiniz katma değer özelliklerine öncelik verin.</Text>
                <Text>İşletmeniz benzersizdir ve bu nedenle, ödemeleri işleme koyma konusunda benzersiz gereksinimlere
                    sahip olacaktır. POS çözümünüzden en iyi şekilde yararlanmak için, istediğiniz özellikleri düşünmeye
                    ve çeşitli sağlayıcılar tarafından sunulan kullanılabilirliği ve maliyetleri değerlendirmeye değer.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>5. Kurulum
                    yardımı ve personel eğitimi hakkında soru sormayı unutmayın.</Text>
                <Text>Kurulumu veya nasıl kullanılacağını öğrenmek çok zorsa, dünyanın en iyi ödeme çözümü, işinize çok
                    az değer katar. Ürün tanıtımını isteyin, böylece çözümü gerçek zamanlı olarak deneyebilirsiniz.
                    Mevcut sistemlere kurulum ve entegrasyon hakkında soru sorduğunuzdan emin olun. Dolandırıcılık
                    protokolleri, gerekli güncellemelerin otomatik olarak gerçekleştirilip gerçekleştirilmeyeceğini veya
                    bunları kendiniz uygulamanız gerekip gerekmediğini öğrenin. Ve tabi ki, işlemenin temelleri var.
                    Çeşitli ödeme türlerini nasıl çalıştıracağınızı öğrenin.</Text>
            </View>
        )
    }
}
