import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img8.png')
    }
},
    {
        url: '',
    props: {
            source: require('../Images/m8/img8_tr.png')
        }
    }];

export default class M8S5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Paketleme & Pazarlama</Text>
                <Text style={styles.dmlH3}>Özel taşıma etiketleri</Text>
                <Text>Nakliye ve taşıma etiketleri uygun taşıma talimatlarını ve bazı durumlarda teslimat yeri
                    bilgilerini içeren kullanıma hazır etiketlerdir. Bu etiketler yanıcı veya kırılır gibi kargo
                    içeriğine yönelik bilgiler vermekte veya taşımaya yönelik uyarılar içermektedir (Anonymous 2018j).
                    Nakliye ve taşıma etiketleri yetkili personeli taşıma talimatlarına ve ihtiyaçlarına göre
                    uyarmaktadır. Aşağıda yaygın olarak kullanılmakta olan etiketlere yönelik bilgiler yer almaktadır.
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Posta ve küçük paket etiketleri </Text>postayla
                    gönderilmekte olan küçük paketler için yetkiliyi uyarmak için kullanılmaktadır (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Üretim ve küçük paket etiketleri </Text>üretim ve taşıma
                    süresince dikkat uyarılarını içermektedir (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Uluslararası resimli etiketlemeler </Text>uluslararası
                    nakliyede kullanılmaktadır. Farklı ülkelerdeki kişi ve yetkililerin üretim ülkesindeki dili anlamama
                    ihtimali düşünülerek resimli etiketler hata riskini düşürmektedir. Uygun taşıma şartlarına uyulması
                    için ürünün gideceği ülke dilinde yazının hazırlanması da uygun olmaktadır. Etiketler çevresel ve
                    güvenlik standartları göz önünde bulundurularak hazırlanmalı ve tehlikeli maddeler tanımlanmalıdır
                    (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Üst ok etiketleri </Text>kırılabilir ürünlerin uygun
                    olarak taşınmasını sağlayarak güvenli şekilde teslim edilmesini hedeflemektedir. Üst ok etiketleri
                    uluslararası resimli etiketlerin parçasıdır (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img2_tr.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Kırılır/Cam etiketleri </Text>paket
                    içeriği ve uygun taşıma yöntemlerine yönelik bilgi vermektedir (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img3_tr.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Bozulabilir etiketleri </Text>sıcaklık
                    bağlantılı veya tıbbi tedarik paketlerinde kullanılmaktadır. Soğukta depolanması ve hayat kurtarıcı
                    durumlarda gerekli olan bozulabilir ilaçların taşınması sırasında kullanılabilir (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img4_tr.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Sıcaklık uyarı etiketleri </Text>ürünün
                    taşıma ve nakliyesi sırasında gereken sıcaklık durumunu bildirmek için kullanılmaktadır. Bu
                    etiketler dondurulmuş ve/veya bozulabilir gıdalar için de kullanılmaktadır (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img6.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img7.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Tehlike etiketleri </Text>tehlikeli
                    ürünlere karşı uyarı sağlamaktadır. Nakliye süresince ürünün özel depolama ve ayrı tutulma gibi
                    gereksinimlerine yönelik nakliye ve taşıma personelini uyarmayı hedeflemektedir. Tehlikeli malların
                    nakliyesi için katı düzenlemeler vardır ve kimyasal veriler gibi özel belgeler genellikle
                    gereklidir. Patlayıcı maddeler, gazlar, yanıcı sıvılar, yanıcı katılar, kendiliğinden yanabilen veya
                    suya reaktif maddeler, zehirli maddeler ve aşındırıcı maddeler için tehlike etiketleri
                    kullanılmalıdır (Anonim 2018j).</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img8_tr.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Biyozararlı etiketleri </Text>biyo
                    atıkları, insan veya hayvan örnekleri ve kullanılmış laboratuar ekipmanları gibi biyo tehlikeli mal
                    paketlerini tanımlamada kullanılmaktadır (Anonymous 2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Özel taşıma etiketleri </Text>tüm özel
                    taşıma ve nakliye bilgilerini içermektedir. Etiketler spesifik talimatları içerir özellikte
                    oluşturulabilir. Uyarı etiketleri veya taşıma talimatlarından oluşabilmektedir (Anonymous
                    2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Tasarı ipuçları </Text>paketin üç yüzüne
                    tercihen yan, son ve üst kısmına etiket olarak uygulanmaktadır (Anonymous 2018j).
                    {"\n\n"}Ürünün özel bir taşıma veya saklama gereksinimi varsa, paketin üzerinde belirtilmeli ve bu
                    bilgiler taşıma irsaliyesi üzerinde belirtilmelidir (Anonymous 2018j).
                </Text>
            </View>
        )
    }
}
