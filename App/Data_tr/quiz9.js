import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz9": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Online pazarlama bir elektronik ticaret şeklidir."
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "İşletme Facebook sayfası oluşturmak için, öncelikle Facebook sayfasını bağlayacak kişisel bir sayfanın olması gerekmektedir."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Twitter",
                    "option2": "b) Facebook",
                    "option3": "c) Websitesi",
                    "option4": "d) Instagram"
                },
                "question": "Aşağıdakilerden hangisi sosyal medya değildir?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Facebook hesabınız varsa, bu hesapla Instagram’da işletme sayfası oluşturabilirsiniz."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "İşletme Twitter hesabı açmak için, Twitter hesabınız olmalıdır."
            },
            "question6": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) İş veya Yer",
                    "option2": "b) Şirket, Organizasyon veya Enstitü",
                    "option3": "c) Marka veya Ürün",
                    "option4": "d) Neden veya Topluluk"
                },
                "question": "İşletme Facebook sayfası oluştururken sayfa menüsü oluşturmak için aşağıdaki kategorilerden hangisi seçilmelidir?"
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "“call to action” butonu sayesinde e-posta, telefon veya websitesi aracılığıyla müşteriler size kolayca ulaşabilecek ve alış veriş yapabileceklerdir."
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Sosyal medya sayesinde ürünler, müşteriler tarafından kolayca görüntülenebilir."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Profili düzenle",
                    "option2": "b) İşletme profiline geçiş yap",
                    "option3": "c) Şifre değiştir",
                    "option4": "d) Özel hesap"
                },
                "question": "İşletmeniz için İnstagram hesabı oluştururken hangi “özelliği” seçmelisiniz?"
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Ticari hesaplar oluşturmak",
                    "option2": "b) Ürünleri sürekli geliştirmek",
                    "option3": "c) Farklı sosyal medyalarda yer almak",
                    "option4": "d) Hepsi"
                },
                "question": "Aşağıdakilerden hangisi e-ticarette etkili olabilir?"
            }
        }
    }
};


export default class M9Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz9;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
