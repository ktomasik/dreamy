import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View, Alert} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz2": {
            "question1": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Büyük ağlarla deniz ürünleri balıkçılığı",
                    "option2": "b) Ağ sistemini onaran teknik personel",
                    "option3": "c) Bilgi alışverişinde bulunan bir grup insan",
                    "option4": "d) İnsanlara yardımcı olan bir tür kamu hizmeti"
                },
                "question": "Ağ iletişimi nedir?"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Sosyal medya hesabı oluşturmak, akıllı telefon markalarına göre değişebilir."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Futbol maçı izleyen bir grup insan",
                    "option2": "b) A computer-based technology to build virtual networks and share information",
                    "option3": "c) Sosyal haberlerin toplanabileceği ortak bir teknoloji (örneğin, TV, radyo)",
                    "option4": "d) Film izlemek için özel bir televizyon kanalı"
                },
                "question": "Sosyal Medya nedir?"
            },
            "question4": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Instagram",
                    "option2": "b) YouTube",
                    "option3": "c) Facebook",
                    "option4": "d) Google",

                },
                "question": "Aşağıdakilerden hangisi bir tür sosyal medya değildir?"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Her gün- düzenli olarak reklam göndermek.",
                    "option2": "b) Kanalı canlı tutmak için mümkün oldukça fazla  içerik yayınlamak.",
                    "option3": "c) İnsanların yorumlarına cevap vermek.",
                    "option4": "d) Sosyal medyada insanları engellemek."
                },
                "question": "Takipçi kitleyi korumak için en iyi uygulama nedir?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Bazı sosyal medya sitelerinde, üyeler konu tabanlı sayfalar oluşturamaz."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Net belirlenmiş pazarlama hedeflerine sahip olmak",
                    "option2": "b) Bir işe başlamak için yeterli paraya sahip olmak.",
                    "option3": "c) Kaliteli ürün sağlamak.",
                    "option4": "d) Reklam yapmak."
                },
                "question": "Sosyal pazarlama ve işletmede en önemli olan nedir?"
            },
            "question8": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Ücretsiz web alanı sağlamak",
                    "option2": "b) İçeriği yüklemek için üyeleri teşvik etmek",
                    "option3": "c) Canlı sohbete izin vermek",
                    "option4": "d) Hepsi"
                },
                "question": "Aşağıdakilerden hangisi yaygın sosyal medyanın bir özelliğidir?"
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Aynı e-posta adresi farklı sosyal medya hesaplarına üye olmada kullanılabilir"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) E-posta adresi",
                    "option2": "b) Arama motoru",
                    "option3": "c) Sosyal medya",
                    "option4": "d) Spam posta"
                },
                "question": "_______ insanlarla doğrudan iletişim kurmaya, reklam yapmaya ve ürün özelliklerini paylaşmaya yardımcı olur."
            }
        }
    }
};

export default class M2Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz2;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                            <AwesomeAlert
                                show={showAlertTrue}
                                showProgress={false}
                                title={I18n.t('_correct')}
                                message={I18n.t('_correctFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="green"
                                onConfirmPressed={() => {
                                    this.hideAlert(); this._next();
                                }}
                                titleStyle={{color: 'green', fontWeight: 'bold'}}
                            />
                            <AwesomeAlert
                                show={showAlertFalse}
                                showProgress={false}
                                title={I18n.t('_incorrect')}
                                message={I18n.t('_incorrectFeedback')}
                                closeOnTouchOutside={false}
                                closeOnHardwareBackPress={false}
                                showCancelButton={false}
                                showConfirmButton={true}
                                confirmText="OK"
                                confirmButtonColor="#DD6B55"
                                onConfirmPressed={() => {
                                    this.hideAlert(); this._next();
                                }}
                                titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
