import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sigorta & Takip</Text>
                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Kargo sigortası
                    ne demek?</Text>
                <Text>Yasal olarak tüm kargo şirketleri sorumlulukları olarak bilinen minimum düzeyde sigorta
                    yatırmalıdır Taşıyıcı sorumluluğun kapsamı çok sınırlı olduğu halde ve doğal afetlerden taşıt
                    kazalarına veya hatta savaşa kadar her şey yükünüze zarar verebilir. Bu nedenle, nakliyeciler
                    mallarını nakliye sırasında kaybolma, hasar veya hırsızlığa karşı koruması için kargo sigortası
                    talep edebilirler. Genellikle mallar alıcıya ulaşana kadar, depolanırken ve nakliye sırasında
                    sigortalanır (Robinson 2016).
                    {"\n\n"}Sıklıkla tercih edilen kargo çeşitleri arasında “tüm riskler”, “geniş kapsamlı”, “yasal
                    sorumluluk” ve “motorlu yük taşımacılığı ” sigortası gibi kapsamlar yer almaktadır. Bu sigortalardan
                    birçoğu kapsamlı ve denizaşırı iş yapan firmalar tarafından tercih edilmektedir. Uluslarası işlemler
                    için uygulanabileceği gibi yerel satışlardaki transferlerde de geçerli olabilmektedir (Robinson
                    2016).
                </Text>
            </View>
        )
    }
}
