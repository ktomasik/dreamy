import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S5 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Kadın kurucular için bazı fonlar</Text>
                <Text>500 Women:  Mükemmel Kadın Kurucular Fonu</Text>
                <Text>Astia: Kadın girişimcilere sermaye ve eğitim/destek erişimi sağlayan ağ</Text>
                <Text>BBG (Built By Girls) Ventures: En az bir kadın kurucu ile tüketici internet girişimlerine yatırım yapma</Text>
                <Text>Chloe Capital: Kadınların yönettiği şirketlere odaklanmış tohum evre girişim sermaye firmaları</Text>
                <Text>Female Founders Fund: E-ticaret, platformlar ve web özellikli hizmetlerdeki kadınların yönetimindeki girişimlere yatırım</Text>
                <Text>Golden Seeds: Kadın girişimcilere yatırım yapan Melek Yatırımcı ağı ve fonu</Text>
                <Text>Intel Capital Diversity Fund: Kadın ve azınlık girişimcilere yatırım yapan fonlar</Text>
                <Text>Mergelane: Kadınların yönetimindeki girişimlerin yatırımcı ve harekete geçiricisi (Boulder, CO)</Text>
                <Text>Next Wave Impact: Yenilikçi yaparak öğrenme erken dönem girişim sermayesi</Text>
                <Text>Pipeline Fellowship: Kadınların yönettiği sosyal girişimlere yatırım yapan kadın yatırımcılar</Text>
                <Text>Valor Ventures: Kadın kuruculara finansman sağlayan Atlanta merkezli fon</Text>
                <Text>Women’s Venture Fund: Kurslar, danışmanlık, kredi ve daha fazlasıyla girişimcilere yardımcı olur</Text>
            </View>
        )
    }
}
