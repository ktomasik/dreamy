import React, {Component} from 'react'
import {Text, Image, View, TouchableHighlight, Modal} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen8 = [{
    url: '',
    props: {
        source: require('../Images/m3/img47.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m3/img48.png')
    }
}];

export default class M3S11_5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Logo tasarlamak için aşağıdaki adımları takip edino</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen8}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible)}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 10: Aşağıdaki tabloyu doldurduktan sonra, kartvizitiniz hazır olacaktır:</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img47.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img48.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
