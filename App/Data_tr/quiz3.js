import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {Button} from 'react-native-elements';
import {quizStyles} from '../Containers/Styles/general';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz3": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış",
                },
                "question": "E-ticaret, bilgisayarların kullanılmaya başlandığı 1950’lerden bu yana hayatımıza girmeye başladı."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış",
                },
                "question": "İnstagram işletme hesapları, başarıyı ölçmek için analitik araç olmadığı gibi marka hakkında da bilgi sağlamaz."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Etsy, dükkan oluşturmak için ücret almaz.",
                    "option2": "b) Bonanza, ücretsiz listeleme sağlar.",
                    "option3": "c) Zibbet’de, elektronik araç satmaya izin verilir.",
                    "option4": "d) DaWanda da dükkan açmak için ücret talep edilmez."
                },
                "question": "Aşağıdaki ifadelerden hangisi yanlıştır?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış",
                },
                "question": "E-ticaret, internet üzerinden mal ve hizmet  alım ve satımıdır."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) İnsan zihni soyut olan bir şeyi somutlaştırmaya eğilimlidir.",
                    "option2": "b) Kaliteli ürün görselleri mağazaya bağımlılıkta temel etkenlerdir.",
                    "option3": "c) Göz izleme çalışmaları, mağaza ziyaretçilerinin önce görsel unsurlarla meşgul olduklarını göstermektedir.",
                    "option4": "d) Logo ve marka arasındaki bağlantı kaçınılmaz değildir."
                },
                "question": "Ürün resimlerinin önemi ile ilgili aşağıdaki ifadelerden hangisi yanlıştır?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış",
                },
                "question": "Instagramdaki Hashtag keşfi, ürünlerin kullanıcıların ana sayfasında daha sık görünmesine yardımcı olur."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Bağlılığı artırmak için hikaye anlatıcılığına odaklanın",
                    "option2": "b) İnstagramda hesap açma",
                    "option3": "c) Ürünü övmek için sahte hesaplar açmak",
                    "option4": "d) Orijinal resmi bir düzenleme ile dönüştürme",
                },
                "question": "Aşağıdakilerden hangisi İnstagramda başarılı bir satış yapma yollarından biridir?"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "El sanatlarının internetten satışında, gerçek görsel kullanımı ürün fiyatı kadar önemli değildir."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Logo tasarımınız ürününüzü ve iş felsefenizi temsil etmelidir."
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Dünya çapında internetten alışveriş yapanlardan % 60'ından fazlası e-ticaret fiyatlandırmasını satın alma kararlarını etkileyen ilk ölçüt olarak görmektedir.",
                    "option2": "b) Maliyete Dayalı E-Ticaret Fiyatlandırma Stratejisinin en büyük avantajı basitliğidir.",
                    "option3": "c) Rakip tabanlı E-ticaret Fiyatlandırması, ürününüzü istediğinizden daha düşük bir fiyatla satmanıza neden olabilir.",
                    "option4": "d) Değer tabanlı E-ticaret fiyatlandırması kısa süreli kazanca işaret eder. ",
                },
                "question": "Aşağıdaki ifadelerden hangisi yanlıştır?"
            }
        }
    }
};

export default class M3Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz3;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
