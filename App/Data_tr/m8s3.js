import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nakliye seçenekleri</Text>
                <Text style={{marginTop: 10}}>Nakliye ve teslimat, müşterilerin ürünlere hızlı ve kolay bir şekilde
                    ulaşma beklentileri nedeniyle perakende satış işinde önemli bir satış faktörüdür. Müşteri sunulan
                    nakliye seçeneklerinden bazıları aşağıda belirtilmiştir (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Aynı gün teslim</Text>
                <Text>Aynı gün teslim şekli genellikle online verilen siparişlerin aynı gün müşteriye teslim edilmesine
                    yönelik dağıtım şeklidir. Bir çeşit express servistir (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Mağazadan teslim</Text>
                <Text>Müşterilerin çoğunlukla online olarak satın alıp yerel mağaza veya yerden kendilerinin teslim
                    almasını sağlayan bir seçenektir (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Mağazadan sevkiyat</Text>
                <Text>Mağazadan gönderim, perakendecilerin, çevrimdışı konumlarını mağaza içi ve çevrimiçi alışveriş
                    yapanların karşıladığı yerine getirme merkezlerine dönüştürdüğü bir hizmettir
                    (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Planlı dağıtım</Text>
                <Text>Planlı dağıtım şekliyle kargo şirketleri müşterilerine sadakat programlarına üye olmaları
                    durumunda ufak bir ücret karşılığında veya ücretsiz olarak teslim zamanı planlamasına müsade
                    etmektedir (Ufford 2018).</Text>

                <Text style={styles.dmlH3}>Ücretsiz kargo önerisi</Text>
                <Text>Özellikle yurt içi satışlarda müşterinin ilgisini çekmek ve dönüşümü artırmak için tercih
                    edilmektedir. Ancak marjlara bağlı olarak,kar oranını düşürmektedir (Anonymous 2018b).
                    {"\n\n"}Ücretsiz kargo önerilecekse belirli bir satış miktarı veya adedi bildirmek faydalı
                    olacaktır. Bunun için rakiplerin nakliye ve kar marjlarına yönelik nasıl bir tutum sergilediğini ve
                    ürünlerin gönderilmesinin maliyetinin ne kadar olacağını bilmek gerekir. Bu, ücretsiz gönderim
                    teklifinde bulunup bulunmamak konusunda doğru bir karar vermeye yardımcı olacaktır. Diğer bir
                    seçenek ise ortalama sipariş değerinin üzerinde minimum sipariş toplamı veya sipariş başına minimum
                    ürün sayısı tanımlaması yapılarak ücretsiz kargo seçeneği sunulabilir (Anonymous 2018b).
                    {"\n\n"}Ücretsiz sevkıyatın avantajlarından bazıları rekabetçi kalmak veya rakiplerin sevkıyat
                    seçeneklerini azaltmak, iletişimi geliştirmek, ortalama sipariş değerini artırmak ve terkedilmiş
                    arabaları azaltmaktır. Öte yandan, nakliye maliyetlerini karşılamak için artan fiyatların pazardaki
                    rakiplerle karşılaştırılması düşünülmelidir (Anonim 2018b).
                </Text>

                <Text style={styles.dmlH3}>Müşteriler nasıl ücretlendirilir</Text>
                <Text>Satışların başında stresli noktalardan biri de müşterilerin nasıl ücretlendirileceğidir. Ürünün
                    üretim dönemi içindeki giderleri dışında, üretimden sonra müşterilerin dikkatini çekeceği düşünülen
                    başka maliyetler de olabilir. Nakliye ücretleri bu kapsamda değerlendirilebilmektedir.
                    {"\n\n"}Ürünün fiyatını düşük göstermek için kargo fiyata dahil edilmeyebilir. Kargo fiyatı dahil
                    edilmeden ürün fiyatı bildirilir ve bu masrafların müşteriye ait olduğu notu düşülebilir Bu noktada,
                    kargo şirketi tercihleri ve şartları müşteri tarafından belirlenebilir ve müşteri ihtiyacına göre
                    nakliye işlenir. Öte yandan, kargo firması tarafından belirlenen ürünün fiyatı ve nakliye bedeli
                    ayrı olarak belirtilebilir.
                    {"\n\n"}Bazı durumlarda kargo dahil ürün fiyatı vermek diğer rakiplere göre avantaj sağlamaktadır.
                </Text>
            </View>
        )
    }
}
