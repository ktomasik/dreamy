import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";

// Styles

export default class M4S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Yeni iş kurma sürecindeki lisanslar ve izinler</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>TÜRKİYE’deki
                    uygulamalar</Text>
                <Text>Bir kadın iş hayatında vergiden muaf olmak istiyorsa Esnaf Vergi Muafiyet Belgesi alması
                    gerekebilir.
                    {"\n\n"}Kadınlar gelecekteki büyüme planlarında vergi mükellefi olmak isterlerse, e-Bildirge Sistemi
                    geçerlidir. Bu sisteme elektronik platformda başvuru yapabilirler. Vergi mükellefleri tarafından
                    alınan ve verilen belgelerin kayıtları, bağlı ait oldukları meslek odalarında tutulmaktadır.
                    (Türkiye Ulusal Raporu, 2018).
                    {"\n\n"}Esnaf Vergi Muafiyet Belgesi (Türkiye Ulusal Raporu, 2018);
                </Text>
                <FlatList
                    data={[
                        {key: '1. Belge alındıktan sonra, Türkiye Esnaf ve Sanatkarla Konfedarasyonuna bağlı bir birliğe kayıt yapılmalıdır.'},
                        {key: '2. Çalışma izni için belediyeye dilekçeyle başvuru yapılmalıdır. '},
                        {key: '3. Evde yürütülecek işler için, tüm apartman sakinleri noter kanalıyla onay vermelidir.'},
                        {key: '4. Fatura ve irsaliye zorunluluğu vardır.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15, fontWeight: 'bold'}}>Vergi mükellefleri için (Türkiye Ulusal Raporu,
                    2018):</Text>
                <FlatList
                    data={[
                        {key: '1. E-Beyan(Elektornik Beyan) sistemine başvurulur.'},
                        {key: '2. Vergi mükellefi olduktan sonra Esnaf ve Sanatkarlar Konfedarasyonuna bağlı bir birliğe kayıt yapılmalıdır.'},
                        {key: '3. Çalışma izni için belediyeye dilekçeyle başvuru yapılmalıdır.'},
                        {key: '4. Vergi mükellefleri dökümanları oda veya birliklerden teslim alabilirler.'},
                        {key: '5. Fatura ve irsaliye zorunluluğu vardır.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>SLOVENYA’ daki uygulamalar</Text>
                <Text>Bir şirket kurmak için CEIDG-1 başvuru kaydı yapılmalıdır. Bu kayıtta:</Text>
                <FlatList
                    data={[
                        {key: '1. Vergi numarası almak gereklidir'},
                        {key: '2. Bireyler üzerindeki  gelir vergisinin vergilendirme türü seçiminin belirtilmesi'},
                        {key: '3. Sosyal güvenlik katkısının beyan edildiğine yönelik bilgilendirme olmalıdır (Slovenya Ulusal Raporu, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>3 alanda faaliyet gösteren firmalar için lisans
                    gereklidir:</Text>
                <FlatList
                    data={[
                        {key: '1. Emlakçılık, brokerlık ve gayrimenkul yönetimi,'},
                        {key: '2. Yol taşıma hizmetleri,'},
                        {key: '3. Çalışma ajansı, süreli çalışma ajansı ve kamu fonları için işsizlik eğitimi kurumu işletmek (Slovenya Ulusal Raporu 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>POLONYA’ daki uygulamalar</Text>
                <Text>Bir şirket kurmak için CEIDG-1 başvuru kaydı yapılmalıdır. Bu kayıtta:</Text>
                <FlatList
                    data={[
                        {key: '1. Vergi numarası almak gereklidir'},
                        {key: '2. Bireyler üzerindeki  gelir vergisinin vergilendirme türü seçiminin belirtilmesi'},
                        {key: '3. Sosyal güvenlik katkısının beyan edildiğine yönelik bilgilendirme olmalıdır (Polonya Ulusal Raporu  2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>3 alanda faaliyet gösteren firmalar için lisans
                    gereklidir:</Text>
                <FlatList
                    data={[
                        {key: '1. Emlakçılık, brokerlık ve gayrimenkul yönetimi,'},
                        {key: '2. Yol taşıma hizmetleri,'},
                        {key: '3. Çalışma* ajansı, süreli çalışma ajansı ve kamu fonları için işsizlik eğitimi kurumu işletmek (Polonya Ulusal Raporu 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>YUNANİSTAN’daki uygulamalar</Text>
                <Text>Adimlar açiklama:</Text>
                <FlatList
                    data={[
                        {key: '1. Ticaret ve Sanayi Odasın’dan şirket adı için onay alın'},
                        {key: '2. Atina Barosu’yla şirket evraklarını doldurun'},
                        {key: '3. Noterden once Ana sözleşmeyi imzalayın'},
                        {key: '4. Bankaya sermaye  yatırın'},
                        {key: '5. Vergi Dairesi’ne sermaye vergisini yatırın'},
                        {key: '6. Avukatlar Emeklilik Fonu’ndan pul alın'},
                        {key: '7. Avukatlar Sosyal Yardım Fonu’ndan belge alın'},
                        {key: '8. Kayıt numarası almak için Ana Sözleşmeyi gönderin ve Mahkeme genel sekreterliğine  kaydolun.'},
                        {key: '9. Resmi Gazete’de yayınlanması için ana  sözleşme özetini gönder I'},
                        {key: '10. Ticaret ve Sanayi Odası kayıt yaptırın'},
                        {key: '11. Bağkur’a, Tarım Sigorta Organizasyonuna vb  kayıt yaptırın'},
                        {key: '12. İşletme için Vergi numarası (AFM) alın'},
                        {key: '13. Kaşe ve mühür  yaptırın'},
                        {key: '14. Vergi Dairesinde şirket makbuz defterlerini ve muhasebe kayıtlarını mühürlettirin'},
                        {key: '15. İşçinin işe alınmasından sonraki 8 gün içinde İşgücüne (OAED) bildirimde bulunun'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Tüm yeni iş sahipleri yukarıdaki adımları takip etmelidir.
                    {"\n"}Serbest Meslek sahipleri 4 ve 10-15 adımlarını tamamlamalıdır.
                    {"\n"}El işlerini satmak için online dükkan açmak isteyen meslek serbest sahiplerinin özel bir izin
                    veya belge almalarına gerek yoktur (Yunanistan Ulusal Raporu, 2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>FRANSADA’daki uygulamalar</Text>
                <Text>Bir markayı korumak için INPI doğrultusunda hareket etmek gerekir (Fransa Ulusal Raporu,
                    2018).</Text>
                <Text>
                    {"\n"}Ulusal Sınai Mülkiyet Enstitüsü (USME) Ekonomi, Maliye ve Dış Ticaret Bakanlığı, Verimli
                    İyileştirme Bakanlığı ve Küçük ve Orta Ölçekli İşletmeler, Yenilikçilik ve Ekonomi Bakanlıkları
                    Delegesi gözetiminde bir kamu organıdır (Fransa Ulusal Raporu, 2018).
                    {"\n\n"}İsim için INPI’ye bir kez başvurulduğunda, şirketin 10 yıllık bir süresi vardır.
                    {"\n"}Yasal yapı değişebilir:
                </Text>
                <FlatList
                    data={[
                        {key: 'Kendi işini yapan'},
                        {key: 'Şahıs İşletmesi'},
                        {key: 'Şirket'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Bir iş kurmanın en basit yolu serbest meslek statüsüdür: aşağıdaki
                    adımları takip edin (Fransa Ulusal Raporu, 2018):</Text>
                <Text>Basit beyanname (PO AE formu) mikro girişimci olmak için yeterli olmaktadır.</Text>
                <Text>Beyanname, faaliyete göre internet üzerinden (1 Ocak 2016 tarihinden itibaren zorunlu) yetkili
                    Ticari İşlemler Merkezine (CFE) gönderilmelidir:</Text>
                <FlatList
                    data={[
                        {key: 'Ticari faaliyetler için Ticaret ve Sanayi Odası (CCI)'},
                        {key: 'Esnaf ve ticari faaliyetler için Esnaf ve Sanaatkarlar Odası (CMA)'},
                        {key: 'Serbest faaliyetler için URSSAF'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text>Yetkin CFE takip edilecek adımlar veya ilave usüllere yönelik bilgilendirmeler yapmaktadır.
                    Sonucunda şunlar elde edilecektir:</Text>
                <FlatList
                    data={[
                        {key: '1. Siret(Kurumlar Dizin Tanımlama Kodu) sayısı'},
                        {key: '2. Kazanç konusunda hangi vergi rejimine(BIC veya BNC – mikro girişimci sistemi)  tabi olunacağı hakkında  ve KDV (temel muafiyet) bilgilendirme yapılır.'},
                        {key: '3. Özellikle kazançlar ve CFE kendi işini yapan girişimciler üzerinden vergisini beyan etmek ve ödemek için irtibat kurmanız gereken vergi muhataplarının iletişim bilgileri'},
                        {key: '4. Kullanabileceğiniz istisnalar ve vergi indirimleri hakkında bilgi vermek üzere atanmış yetkilinin iletişim bilgileri'},
                        {key: '5. Ticaret, esnaflık veya endüstriyel faaliyet yürüten  serbest meslek sahibi girişimciler için Ticaret ve Şirketler Kaydı\'na (RCS) zorunlu kayıt, 2015\'te tanıtılan ilave bir adımdır. Serbest çalışan bir KBIS(mikro-işletmenin kayıt işlemini onaylayacak belge) almanızı sağlayacaktır.'},
                        {key: '6. Küçük bir girişimci olarak, otomatik olarak "mikro-sosyal" rejimine tabi tutulunur'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text>İş Kurma Destekleri{"\n\n"}Bireysel girişimci olarak, çeşitli iş kurma desteklerinden
                    faydalanılabilir:</Text>
                <FlatList
                    data={[
                        {key: 'İş arayanlar veya belirli kriterlere (yaş, cinsiyet vb) uyanlar bireysel girişimci olarak ACCRE’ye başvurabilirler. ACCRE onayı alanlar, NACRE’ye başvurarak projeyi yazma ve şirketi geliştirme konularında destek alabilirler.'},
                        {key: 'Yurtdışı bölümlerde çalışılması durumunda esnaf ve ticari meslek sahipleri 24 aylık sigorta piriminden muaf tutulmaktadır.'},
                        {key: 'Kısmi işsizlik maaş desteği gibi Pôle emploi yardımcıları da mevcuttur.'},
                        {key: 've otomatik olarak mikro girişimin vergi rejiminden faydalanılabilinir'},
                        {key: 'https://www.legalstart.fr/fiches-pratiques/autoentrepreneur/statut-auto-entrepreneur-quelles-demarches/'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
