import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Proje ortağı ülkelerde adım adım finansman</Text>
                <Text style={styles.dmlH2}>Yunanistan:</Text>
                <Text style={styles.dmlH3}>Adım 2:</Text>
                <Text>İş planına daha detaylı bir genel bakış sağlamak için ilk toplantı yapılacaktır. Toplantının
                    amacı, değerlendirme kriterlerinin yerine getirildiğini ve finansman ihtiyaçlarının tanımlanmasının
                    yanı sıra finansal araçların uygun olduğunun doğrulanmasıdır.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
