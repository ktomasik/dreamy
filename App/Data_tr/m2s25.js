import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İnstagram Hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 12: Ayrıca arkadaşlarınızı kişilerinizden takip etmeniz önerilir. Bunu yapmak için Instagram uygulamasının iletişim bilgilerinize erişmesine izin vermelisiniz.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img46.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
