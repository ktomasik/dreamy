import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M8S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Etkin nakliye ve ikmal stratejileri</Text>
                <Text style={{marginTop: 10}}>Nakliye, müşteriye uygun üretim maliyetleri olan verimli bir ürün sunmak,
                    müşteriye ulaşmak ve ödemeyi almak için atılan çok önemli adımlardan biridir. Ürünü müşteriye
                    güvenli bir şekilde uygun bir maliyetle ulaştırmak için varış yeri, içeriğin özelliği, ağırlık,
                    boyutlar vb. gibi faktörlerin göz önünde bulundurulması gerekir.
                    {"\n\n"}Nakliye giderleri ufak çaplı işlerde ve şirketlerde en yüksek giderlerden biridir. Fakat
                    çeşitli nakliye ve ikmal stratejileri kullanılarak müşterilere zamanında uygun fiyatla kargo
                    göndermek mümkün olmaktadır.
                    {"\n\n"}Küçük çaplı işlerde kargo fiyat hesaplamalarında çoğunlukla gönderi sıklığı ve miktarı
                    dikkate alınmaktadır. Temel olarak gönderi hacmi arttıkça fiyat düşmektedir.Bununla birlikte kargo
                    şirketleriyle pazarlık yapılarak fiyatları düşürmek mümkündür. Bazı kargo şirketlerinin
                    aboneliklerine üye olmak da fiyatı düşürmekte fayda sağlayan diğer yoldur.
                    {"\n\n"}Kargo şirketlerinin tedarik etmiş olduğu poşet veya ambalajlar boyut kaynaklı ekstre
                    maliyetlerin önüne geçmek için kullanılabilmektedir. Kargo şirketleri farklı ürünlere değişik
                    özelliklerde ambalaj/paket tedariği yapabilmektedir. Farklı boyutlar ve büyüklüklerdeki plastik
                    poşetler, karton kutular, hassas ürünler için hava kanallı paketler, tablo veya kağıt gibi ürünler
                    için rulo paketler en yaygın kullanılan ambalajlar arasındadır.
                    {"\n\n"}Nakliye ve ikmal startejileri satış karlılığının bir parçası olarak tanımlanmaktadır. Eğer
                    doğru şekilde kullanılırsa nakliye stratejisi satışların tekrarını sağlamakta ve hatta yeni
                    müştelerilere ulaşma konusunda artı değer katmaktadır.
                </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Üretim</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Müşteriye ulaşım</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Satış & Ödeme</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Nakliye - sevk</Text>
                    </View>
                </View>
            </View>
        )
    }
}
