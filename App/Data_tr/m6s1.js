import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Giriş</Text>
                <Text style={{marginTop: 10}}>Dreamy m-Learning Projesi’nin hedef kitlesi evlerinde el sanatları üreten,
                    düşük eğitime sahip kadınlardır. Projenin amacı akıllı telefonlarını verimli kullanarak digital
                    pazarlarda el sanatlarını satabilmeleri için mobil dijital ve girişimcilik becerileri kazanmalarına
                    yardımcı olmaktır. Bu amaç doğrultusunda, m-learning portalına cep uygulamasını köprüleyen web
                    tabanlı arayüzlü Android ve iOS işletim sistemleri için anlaşılması ve kullanılması kolay bir cep
                    telefonu uygulaması geşitirilecektir. Hazırlanan eğitim programlarında, ortak ve ücretsiz cep
                    uygulamalarıyla kadınlara adım adım nasıl ilerleneceği açıklanacaktır.
                    {"\n\n"}Bu çerçevede, modülde fatura oluşturma, internet alışverişi için alıcı-tedarikçi ilişkileri
                    için sözleşmeler oluşturma, internette ürün satışı için sigorta başvurusu yapma ve el sanatı
                    ürünlerini internette satmak isteyen kadınlar için elektronik imza oluşturma gibi konuların
                    açıklanması hedeflemektedir.
                </Text>
            </View>
        )
    }
}
