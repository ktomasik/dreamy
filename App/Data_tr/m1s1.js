import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İnternet Nedir?</Text>
                <Text>İnternet kablolu, kablosuz ve fiber optik teknolojileri ile bağlanılabilen küresel değişimler için
                    iletişim ağıdır- özel, kamu, iş, akademik ve devlet iletişim ağlarını da içeren.</Text>
                <Text style={styles.dmlH2}>İnternetin kısa tarihçesi</Text>
                <Text>İnternetin kökleri 1960’lı yıllara dayanmaktadır. Birleşik Devletler Hükümet, Savunma Birimi’nin
                    merkezi olmayan iletişim ağı yaratma projesi olarak başlamıştır. Bu proje ARPANET (Gelişmiş
                    Araştırma Projeleri Ajansı İletişim Ağı) olarak adlandırılmıştır. . Elektronik mesajların nasıl
                    paketleneceği, adresleneceği ve iletişim ağı üzerinden gönderilmesinin tanımlandığı daha küresel
                    yeni sofistike ve standart protokol oluşturmak için IP (İnternet Protokolü) teknolojisi yaratmak
                    için bu proje geliştirilmiştir. </Text>
                <Text style={styles.dmlH2}>Ağ (Web) nedir – (Dünya Çapında Ağ)</Text>
                <Text>Ağ, ya da Dünya Çapında Ağ (3W), temelde özel formatlı dökümanları destekleyen Internet sunucuları
                    sistemidir.</Text>
                <Text style={styles.dmlH2}>İnternet ve Ağ arasındaki farklar</Text>
                <Text>İnternet, bir bilgisayarı dünya çapında diğer bilgisayarlara bağlayan, taşıma yoludur. Dünya
                    çapında ağı ve herhangi bir web sayfasını ya da içeriğini görüntülemek için internete erişiminiz
                    olması gereklidir. Web internetin bilgi paylaşım kısmıdır. Web, Web sayfası denilen ve birbirlerine
                    hiperlinklerle bağlı olan Web dökümanlarına erişim için Google ya da İnternet gibi tarayıcılar
                    kullanır. </Text>
            </View>
        )
    }
}
