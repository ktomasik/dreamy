import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Facebook hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 5: Adınızı soyadınızı, doğum tarihinizi yazın…</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img11.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img12.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
