import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz1": {
            "question1": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Kullanıcı adı",
                    "option2": "b) Şifre",
                    "option3": "c) İletişim bilgileri",
                    "option4": "d) Ev adresi"
                },
                "question": "Bir e-posta hesabı açmak için aşağıdakilerden hangisi gerekli değildir?"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "E-posta spam ve e-mail virüsü aynı anlama sahiptir."
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Çeşitli akıllı telefon markalarında e-posta hesabı oluşturmada farklılıklar olabilir"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) WWW(internet sunucuları ağı)",
                    "option2": "b) İnternet",
                    "option3": "c) E-posta hesabı",
                    "option4": "d) Spam posta"
                },
                "question": "________ birbirine bağlı milyonlarca web sayfasından oluşur."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "e- posta kullanmak için bir hesaba sahip olmak gereklidir."
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Banka hesabı",
                    "option2": "b) E-posta hesabı",
                    "option3": "c) Ev adresi",
                    "option4": "d) Telefon numarası"
                },
                "question": "İnternet üzerinden e-posta göndermek için aşağıdakilerden hangisi gereklidir?"
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Sosyal medya ismi",
                    "option2": "b) E-posta hesabı",
                    "option3": "c) İnternette bilgi aramaya yarayan bir yazılım sistemi",
                    "option4": "d) Web sitesi"
                },
                "question": "Arama motoru nedir?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Kullanıcı e-posta hesabı akıllı telefona hesap olarak uygulanabilir."
            },
            "question9": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) İnternette arama yapmak",
                    "option2": "b) Virüs temizlemek",
                    "option3": "c) İnternette veri transferi yapmak",
                    "option4": "d) Kullanıcı adını, kullanıcının internet adresini ayırmak"
                },
                "question": "@ ne amaçla kullanılır?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Arama motoru",
                    "option2": "b) E-posta adresi",
                    "option3": "c) Bir çeşit virüs",
                    "option4": "d) Spam posta"
                },
                "question": "Google nedir?"
            }
        }
    }
};

export default class M1Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz1;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
        /*console.log('countCheck: ' + this.state.countCheck);
        console.log('punkty: ' + this.score)*/
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
