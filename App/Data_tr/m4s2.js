import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";
// Styles

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m4/img1.png')
    }
}];

export default class M4S2 extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Şirket adı, ticari isim ve yasal isim arasındaki fark nedir? </Text>
                <Text>Şirket adı şirket sahibi olan kişi veya kuruluşun resmi adıdır.
                    {"\n"}Şirketin resmi adıdır ve devlet işlemleri ve uygulamalarında kullanılmaktadır (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Örneğin:</Text> Girişimcinin adı John Smith’dir ve
                    sigorta işi yapmaktadır. Şirketin resmi adı John Smith Sigorta olarak geçmektedir (Anonymous 2018b).
                    {"\n\n"}Şirket sahipleri reklam vermek ve satış yapmak amacıyla ticari adını kullanabilmekteler.
                    Ticari marka, internette ve levhlarda halkın gördüğü isimdir. (Cameron 2017).
                    {"\n"}Şirket adı ve marka adı birbirinden farklı olabilir. Marka adında Ltd, A.Ş. gibi ibareler yer
                    almamaktadır (Cameron 2017).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Örneğin:</Text> McDonald’s ticari bir markadır. Şirketin
                    adı ise McDonald’s Corporation olarak geçmektedir (Anonymous 2018c).
                </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m4/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 10}}>
                    Bir şirketin yasal ismi şirket sahibi olan kişi veya kuruluşların adıdır.. Şirket ortaklıysa, yasal
                    isim ortaklık sözleşmesiyle verilmiştir veya ortakların soyadlarından oluşturulmaktadır.
                    {"\n\n"}Sınırlı sorumlu şirketlerde (Limited şirketlerde (Ltd.Şti.))-ve kuruluşlarda, şirketin yasal
                    adı devlette kayıtlı olandır. Bu isimlerin çoğu LLC, Inc. or LLP gibi “yasal ekler” kullanmaktadır
                    (Fishman 2015).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10}}>Yasal ad veya ticari
                    ad ne zaman kullanılmalıdır?</Text>
                <Text>Yasal isim devlet veya diğer şirketlerle iletişim kurulurken kullanılmalıdır.
                    {"\n"}Örneğin, vergi iadesini doldururken, mülk satın alırken veya çek yazarken, işletmenin yasal
                    adı kullanılmalıdır.
                    {"\n"}Ticari isim şirketin reklamlarında ve iş amaçlı kullanılmaktadır. Genellikle tabelalarda,
                    internette ve reklamlarda halkın gördüğü adder. (Anonymous 2018d).
                    {"\n\n"}Temel olarak:
                    {"\n"}Yasal isim devlet prosedürlerinde,
                    {"\n"}Ticari marka halka olan ilişkilerden kullanılmaktadır.
                </Text>
            </View>
        )
    }
}
