import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S9 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Online ticaret (e-ticaret) kuralları</Text>
                <Text style={{marginTop: 10}}>E-ticaret için aşağıdaki bilgilerin web sitesinde bulunması gerekmektedir:</Text>
                <Text style={{marginLeft: 10, marginTop:5}}>Bilgiler şu şekilde olmalıdır:</Text>
                <FlatList
                    data={[
                        {key: 'Ticaret ünvanı, ticaret adresi, vergi ve ticaret sicil numarası, e-posta adresi, telefon numarası ve web sitesinin yöneticilerinin adları(ve online Pazar işlettikleri yerde, satıcı/tedarikçilerin resmi iletişim bilgileri); '},
                        {key: 'Websitesinin bir devlet kurumunun ve ilgili kurumun lisansı veya izni altında faaliyet gösterip göstermediği'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 25}}>{item.key}</Text>}
                />
                <FlatList
                    data={[
                        {key: 'Web sitesi kullanım veya ziyaretine yönelik şart ve koşullar.'},
                        {key: 'Gizlilik politikası.'},
                        {key: 'Elektronik imza zorunludur.'},
                        {key: 'Kullanıcı sözleşmesi (eğer üyelik gerekliyse).'},
                        {key: '6502 sayılı Tüketici Koruma Kanunu uyarınca (web sitesinde  tüketicilere mal veya hizmet satışacaksa) Uzaktan Sözleşmeler Yönetmeliğine göre hazırlanacak mesafe sözleşmesi hazırlanmalı'},
                        {key: 'Müşteriler ve online ticaret girişimcileri arasındaki para akışını düzenlemek üzere online ticaret banka veya ödeme servis yöneticisi tarafından gerçekleştirilmelidir (Ödeme Servisleri Kanunu No.6493) (Dora et al. 2018).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
