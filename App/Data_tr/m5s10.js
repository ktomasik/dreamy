import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Banka Hesaplarıyla Ödeme Kabul etme</Text>
                <Text style={{marginTop: 15}}>Banka Hesapları ile ödeme kabul etmek için aşağıdaki talimatları izleyin
                    {"\n\n"}Banka havalesi şu şekilde gerçekleşir:
                </Text>
                <FlatList
                    data={[
                        {key: 'Transfer yapmak isteyen taraf bir bankayla görüşür ve bankaya belirli bir miktar para transfer etme emri verir. IBAN ve BIC kodları da verildiği için banka paranın nereye gönderilmesi gerektiğini biliyor. '},
                        {key: 'Gönderen banka, güvenli bir sistem üzerinden alıcı bankaya bir mesaj ileterek, verilen talimatlara göre ödeme yapmasını talep eder.'},
                        {key: 'Mesaj ayrıca ödeme talimatlarını da içerir. Gerçek transfer hemen gerçekleşmez: Paranın gönderenin hesabından alıcının hesabına geçmesi birkaç saat hatta günler sürebilir.'},
                        {key: 'İlgili bankaların birbirleriyle karşılıklı bir hesapları bulunmalı veya ödeme, nihai alıcıya daha fazla fayda sağlamak için muhabir banka gibi böyle bir hesap sahip bankaya gönderilmelidir.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop:15}}>Uluslararası bir ödeme alabilmeniz için, gönderene aşağıdakileri de içeren bazı ayrıntılar vermeniz gerekir:</Text>
                <FlatList
                    data={[
                        {key: 'Uluslararası Banka Hesap Numarası (IBAN) '},
                        {key: 'Tasnik kodu '},
                        {key: 'Hesap numarası '},
                        {key: 'Tam isim	'},
                        {key: 'Adres'},
                        {key: 'Ödeme almak isteğiniz tutar ve para birimi.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
