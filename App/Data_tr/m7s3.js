import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Mikro Kredi Nedir ?</Text>
                <Text>Mikrofinans, geleneksel bankacılık sistemine erişemeyen insanlar için finansal ürün ve hizmetleri
                    belirlemede kullanılan bir şemsiye terimdir. Mikrofinans, bu insanların geçim kaynaklarını finanse
                    etmelerine, tasarruf etmelerine, ailelerinin geçimini sağlamalarına ve onları hayatın günlük
                    risklerinden korumalarına izin verir.</Text>
                <Text style={styles.dmlH2}>Mikro Finans Nasıl Çalışır?</Text>
                <Text>Mikrofinans kurumları, borçluların sağlam bir geri ödeme garantisi sunmamasına rağmen, yardım(yeni
                    bir iş veya büyüme planını finanse etmek, acil aile ihtiyaçlarını karşılamak, bir iş elde etmek için
                    hareketliliği kolaylaştırmak, vb) ile birlikte bu borçlulara mikro kredi verir.</Text>
                <Text style={styles.dmlH2}>Mikrofinans kadın girişimcileri güçlendirmeye nasıl katkıda bulunur ?</Text>
                <Text>Mikrofinansın katkısı, kadınlar da dahil yoksul insanların, iş bulma, güven arttırma, iletişim
                    becerilerini geliştirme ve diğer yönleriyle yardımcı olmalarıdır .Kadınlar, bu mikro finansman
                    programlarına katılımlarıyla, maddi mal varlığı gibi kaynaklar, bilgi, malumat, fikir gibi fikri
                    kaynaklar, ve evde, toplumda, topluluklarda ve ülkede karar verme süreci üzerinde daha fazla kontrol
                    sahibi olurlar.</Text>
                <Text>Mikrofinans, kadınların kaynak perspektifinde girişimciliğin güçlendirilmesine yönelik önemli bir
                    araçtır. Yoksulluğun azaltılmasında önemli bir araç olarak kabul edilen grup kredileri, ağ oluşturma
                    yoluyla üyelere fayda sağlamaktadır.</Text>
            </View>
        )
    }
}
