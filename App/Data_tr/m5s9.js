import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İlk Sipariş Formu</Text>
                <Text style={{marginTop: 15}}>İlk Sipariş Formunu düzenlemek için aşağıdaki talimatları izleyin
                    {"\n"}İnternet ödemeleriyle Basit Sipariş Formu Nasıl Oluşturulur
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Sipariş almak içins</Text>: Kredi Kartı ve PayPal ödemesi
                    kabul edecek WordPress sipariş formunun nasıl oluşturulacağı hakkında fikir sahibi olunmalı
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 1: Word
                    Press’te Basit Sipariş Formu Oluşturma</Text>
                <FlatList
                    data={[
                        {key: 'Yeni bir form oluşturmak için WPForms ‘a Git » Yeni Ekle'},
                        {key: 'Forma isim verin ve Fatura /Sipariş Form şablonunu seçin'},
                        {key: 'Sağdaki önizleme ekranında “Kullanılabilir Öğeler” bölümüne ilerleyin ve üzerine tıklayın'},
                        {key: 'Bu Sol paneldeki “Saha Seçenekler” açacaktır. Burada alanı yeniden adlandırabilir, siparişleri ekleyebilir ve çıkartabilir ve fiyatları değiştirebilirsiniz.'},
                        {key: 'İnsanlara sipariş formunu doldururken seçebilecekleri resimler vermek isterseniz, Form Editör\'de Resim tercihlerini kullan onay kutusunu işaretleyin.'},
                        {key: 'Son olarak, sipraiş formunuza soldan sağa tarafa sürükleyerek yeni alanlar ekleyebilirsiniz'},
                        {key: 'İşlem bittikten sonra Kaydet’i tıklayın'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 2: Sipariş
                    Formu Bildirimlerinizi Yapılandırın</Text>
                <FlatList
                    data={[
                        {key: 'Bildirimler, formunuz gönderildiğinde bir e-posta göndermenin harika bir yoludur'},
                        {key: 'E-posta adres alanına eposta adreslerini ekleyerek, kendinize ve takımınızdaki bir üyeye ya da müşterinize siparişini aldığınızı gösteren e-posta bildirimi gönderebilirsiniz '},
                        {key: 'Form Oluşturucu’daki Ayarlar sekmesine ve ardından Bildirimlere tıklayın'},
                        {key: 'E-posta adresine gönder alanındaki Akıllı Etiketleri Göster ‘e tıklayın'},
                        {key: 'E-posta’ya tıklayın'},
                        {key: 'Daha özel olması için bildirimin e-posta konusunu değiştirin. Bununla birlikte “Gönderen Adı”,”E-postadan” ve “Yanıtla” e-postalarını özelleştirebilirsiniz'},
                        {key: 'E-posta kendinizden başka birine giderse kişileştirilmiş bir mesaj ekleyin'},
                        {key: 'Gönderilen sipariş formunun alanlarında bulunan tüm bilgileri dail etmek istiyorsanız, {tüm alanlar} akıllı etiketini kullanın'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 3: Sipariş
                    Formu Onaylarınızı Yapılandırın</Text>
                <Text>Form onayları, müşteriler sipariş formunu gönderdikten sonra görüntülenen mesajlardır.
                    {"\n"}Seçebileceğiniz üç onay tipi vardır:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Mesaj. Bir müşteri sipariş formu gönderdiğinde, formlarının işleme alındığını bilmelerini sağlayan basit bir mesaj onayı görünür.'},
                        {key: '2. Sayfayı Göster. Bu onay türü, müşterilerinizi sitenizdeki siparişleri için teşekkür eden belirli bir web sayfasına yönledirecektir. '},
                        {key: '3. (URL)’ye git(Yönlendirme) Bu seçenek, müşterileri farklı bir web sitesine göndermek istediğinizde kullanılır..'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Kullanıcıların siparişlerini gönderdikten sonra görecekleri mesajı
                    özelleştirebilmeniz için WPForms'ta basit bir form onayının nasıl ayarlanacağını görelim.</Text>
                <FlatList
                    data={[
                        {key: 'Ayarlar altındaki Form Düzenleyici\'deki Onay sekmesine tıklayın.'},
                        {key: 'Oluşturmak istediğiniz onay tipi türünü seçin.'},
                        {key: 'Onay mesajını beğeninize göre özelleştirin ve işiniz bittiğinde Kaydet\'i tıklayın.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 4: Ödeme
                    Ayarlarını Yapılandırın</Text>
                <Text>WPForms, ödemeleri kabul etmek için PayPal ile birleşir
                    {"\n"}Sipariş formundaki ödeme ayarlarını yapılandırmak için öncelikle doğru ödeme eklentisini
                    yüklemeniz ve etkinleştirmeniz gerekir.
                    {"\n"}Bunu yaptıktan sonra:
                </Text>
                <FlatList
                    data={[
                        {key: '• Form Düzenleyici\'deki Ödemeler sekmesini tıklayın.'},
                        {key: '• PayPal\'a tıklayın'},
                        {key: '• PayPal e-posta adresinizi girin,'},
                        {key: '• Üretim modunu seçin,'},
                        {key: '• Ürünler ve Hizmetler\'i seçin,'},
                        {key: '• Ödeme ayarlarını yapılandırın,'},
                        {key: '• Değişikliklerinizi saklamak için Kaydet\'e tıklayın.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Artık sitenize basit sipariş formunuzu eklemeye hazırsınız.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 5: Basit
                    Sipariş Formunu Sitenize ekleyin</Text>
                <FlatList
                    data={[
                        {key: 'WordPress\'de yeni bir yazı veya sayfa oluşturun ve ardından Form Ekle düğmesine tıklayın.'},
                        {key: 'Açılır menüden basit sipariş formunuzu seçin ve Form Ekle\'ye tıklayın. '},
                        {key: 'Gönderinizi veya sayfanızı yayınlayın, böylece sipariş formunuz web sitenizde görünecektir. '},
                        {key: 'Görünüm »Ekran Aracı\'lara gidin ve kenar çubuğunuza bir WPForms ekran aracını ekleyin. '},
                        {key: 'Açılır menüden Fatura/Sipariş Formunu seçin.'},
                        {key: 'Kaydet’i tıklayın.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Artık yayınlanan sipariş formunuzu sitenizde canlı olarak
                    görüntüleyebilirsiniz. Formunuzdaki öğeleri seçtiğinizde fiyat otomatik olarak değişir.
                    {"\n"}Artık internette ödeme kabul eden WordPress'te basit bir sipariş formu nasıl oluşturulacağını
                    biliyorsunuz.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', marginTop: 15}}>Siparişleri bir işletme olarak
                    göndermek istiyorsanız, Google Dokümanlar'ın sipariş formunu kullanabilirsiniz</Text>
                <Text>Google Dokümanlar, sipariş formu olarak kullanılabilecek formlar oluşturmanıza olanak tanır.
                    Veriler Google Dokümanlar’da e-tablonuza kaydedildikten sonra faturalandırmayı veya oradan sipariş
                    verebilirsiniz.</Text>
                <FlatList
                    data={[
                        {key: '1. Google Dokümanlar\'ı açın ve "Oluştur" düğmesini tıklayın. "Form" u seçin.'},
                        {key: '2. Sipariş formunuzun adını ve açıklamasını doldurun.'},
                        {key: '3. Uygun ise, formunuzu bölümlere ayırın.'},
                        {key: '4. "Öğe Ekle" ye tıklayarak soru ekleyin'},
                        {key: '5. "Öğe Ekle" düğmesinin yanındaki "Tema" düğmesine tıklayarak formunuz için bir tema seçin.'},
                        {key: '6. Formunuzu tarayıcıda görüntülemek için form penceresinin altındaki bağlantıya tıklayın.'},
                        {key: '7. Formunuzu dağıtın19.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
