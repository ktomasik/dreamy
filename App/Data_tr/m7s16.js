import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S16 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Hibeleri</Text>
                <Text style={styles.dmlH2}>Polonya:</Text>

                <Text style={styles.dmlH3}>AB Fonları</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Polonya\'da AB fonları hakkında ortak bilgilendirme sitesi. Bu sitede, her girişimci veya gelecekteki girişimci (aynı zamanda STK\'lar, kamu kurumları vb.) mevcut fonlar hakkında ilginç bilgiler bulabilir.',val2: 'https://www.funduszeeuropejskie.gov.pl/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Polonya Kurumsal Kalkınma Ajansı</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Polonya\'da, Polonya Kurumsal Gelişim Ajansı, kadınların profesyonel faaliyetlerini desteklemeyi, kendi işlerini kurmalarını teşvik ederek iş piyasasında durumlarını iyileştirmelerini amaçlayan, sadece kadınlara yönelik kredi verir. Kadınlara yönelik Kredi Fonu, bu gruptaki işsizlik problemlerini azaltmaya katkıda bulunmalıdır. Kadınlar 5 ila 10 bin € (20 and 40 bin PLN) arasında tercihli krediye başvurabilir.',val2:'http://en.parp.gov.pl/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
