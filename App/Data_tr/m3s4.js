import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ürünlerin fotoğrafını çekmek ve bunları ağa yüklemek için aşağıdaki adımları
                    izleyin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 1: Cep telefonunuzdan “kamera” ya gidin</Text>
                <View style={{alignItems: 'center'}}>
                <Image
                    style={{flex: 1, width: 300, height:350}}
                    source={require('../Images/m3/img17.png')}
                    resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
