import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S31 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Instagram'dan diğer sosyal ağlara paylaş</Text>
                <Text style={{marginTop: 10}}>Gelecek adımlarda aşağıdakileri göstereceğiz:</Text>
                <FlatList
                    data={[
                        {key: 'nasıl içerik paylaşılır'},
                        {key: 'diğer sosyal ağlarda nasıl içerik paylaşılır ve'},
                        {key: 'ilgili konuya nasıl içerik eklenir.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 3, marginLeft: 15}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Adım 1: Yeni içerik paylaşmak için ekranın altındaki kamera işaretine basın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img59.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
