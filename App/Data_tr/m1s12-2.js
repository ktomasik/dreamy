import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S12_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Android işletim sistemini içeren mobil telefonlarda e-posta hesabı oluşturma (Samsung, Sony, HTC…) </Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 2: Hesaba gidin Hesap Ekle’yi seçin sonra</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img13.png')}
                        resizeMode="contain"/>

                </View>
            </View>
        )
    }
}
