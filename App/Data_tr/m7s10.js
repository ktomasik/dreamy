import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S10 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Kredileri nereden bulunur?</Text>
                <Text style={styles.dmlH2}>Fransa:</Text>
                <Text style={styles.dmlH3}>Women-Initiated Guarantee Fund (FGIF)</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Amaç: İşletme sermayesi gereksinimlerini ve / veya kuruluş aşamasındaki yatırımları, bir şirketin geri kazanımını veya geliştirilmesini kapsayacak şekilde banka kredilerinin elde edilmesini kolaylaştırmak',val2: 'https://www.afecreation.fr/pid14855/appuis-pour-les-femmes.html'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Ağlar</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Aşağıda listelenen ulusal ağlar, kadınları karşılamak,  bilgilendirmek ve projelerinin düzenlenmesinde eşlik etmek üzere hizmet verirler. Bazıları kadın yaratıcılara tahsis edilmiştir, diğerleri herkesi  hedef almıştır ancak kadınlara özgü faaliyetleri vardır, diğerleri finans ettikleri şirketleri takip ederler.',val2:'http://www.ellesentreprennent.fr/pid14416/les-reseaux-au-service-des-creatrices.html'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>The BPI France (Kamu Yatırım Bankası)</Text>
                <FlatList
                    data={[
                        {key: {val1: 'BPI, Devletin gözetiminde olan bir organizasyondur. Finansman ve kalkınma  yardımlarında birlikte hareket ederler. Bunun yerine, bankanızı projelerinizde sizi takip etmeye ikna etmek için birleştirme ve garanti çözümleri sunar.',val2:'http://www.bpifrance.com/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
