import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Hibeleri</Text>
                <Text style={styles.dmlH2}>Yunanistan:</Text>
                <Text style={styles.dmlH3}>Seed Capital</Text>
                <Text>Tohum Sermayesi, genellikle gençler veya işsizler gibi belirli nüfus grubuna verilen bir işletmeyi
                    başlatmak için küçük fon kaynağıdır. Bunların başlıca özellikleri, iş geliştirmeye zaman ayıran
                    işletmenin ilk yıl operasyon işletme masraflarını karşılamayı hedefleyen, ön ödemeli ve (15-50 bin
                    EURO) oldukça küçük sermayeler olmasıdır. Gösterge olarak, ekonominin çeşitli sektörleri (geleneksel
                    ürünler, bilgi teknolojileri, vb.) için hem kamu (OEDED Program) hem de özel sektörün (TheOpenFund)
                    faaliyetleri vardır. Süreç oldukça basittir ve başvuru ilgilenen herkes tarafından yapılabilir.
                    Ancak bu işler yıl boyunca açık değildir. Kamu programları için seminerler, işsizlik kartları gibi
                    bazı önkoşullar vardır.</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: '',
                                val2: 'http://www.digitalplan.gov.gr/portal/resource/Prosklhsh-Ypobolhs-Protasewn-sta-Ergaleia-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Sporas-Seed-ICT-Fund-kai-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Ekkinhshs-Early-Stage-ICT-Fund-gia-epiheirhseis-ston-klado-twn-Tehnologiwn-Plhroforikhs-kai-Epikoinwniwn-ICT-ths-Prwtoboylias-JEREMIE'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Partnership Agreement (PA) 2014-2020 (ESPA)</Text>
                <Text>ESPA, AB bölgeleri arasındaki eşitsizliklerin giderilmesi için Avrupa Birliği programından fon
                    sağlayan bir Yunan programıdır. ESPA çerçevesinde hükümet ticaret, işleme veya birincil üretime
                    yönelik fonları dağıtır. Başvurular ilgili Bakanlıklar ve ESPA genel sekreterliği tarafından ilan
                    edilen sınırlı süreler içerisinde sunulur. Tekliflerin değerlendirilmesi bağımsız değerlendiriciler
                    tarafından yapılır, sonuçlar yayınlanır ve daha sonra her bir eylemin uygulanması için bir ile üç
                    yıllık bir süre verilir. Finansman, yatırımın veya vergi indiriminin (genellikle% 40-60) bir yüzdesi
                    olabilir. Harcama belgeleri gereklidir ve yerinde kontroller sırasında incelenir.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'ESPA\'nın büyük avantajı ulaşılabilirliğidir, ancak finansman maliyetleri takip ettikçe, özellikle yeni işletmeler için yararlı değildir. Ancak, diğer finansman türleriyle (banka kredileri, girişim sermayesi, vb.) birleştirilmesi özellikle cazip bir seçenektir.',
                                val2: 'https://www.espa.gr/en/pages/staticPartnershipAgreement.aspx'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text>Diğerleri arasında ESPA 2014-2020 programı aşağıdaki girişimleri finanse eder:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Girişimciler ve Yeni girişimcilik',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=496&t=Neofuis-Epixeirimatikotita'
                            }
                        },
                        {
                            key: {
                                val1: 'Yeni pazarlarda yeteneklerini geliştirmek için mikro ve küçük işletmelerin iyileştirilmesi',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=497&t=Anabathmisi-polu-mikrwn-&-mikrwn-epixeirisewn-gia-tin-anaptuksi-twn-ikanotitwn-tous-stis-nees-agores'
                            }
                        },
                        {
                            key: {
                                val1: 'Yurtdışında iş yapmak',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=539&t=Epixeiroume-Eksw'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
