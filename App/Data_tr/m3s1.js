import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>E- İş ve E- Ticaret nedir?</Text>
                <Text>Elektronik iş (e-iş) iş yapmak için web, internet, intranet, extranet ya da bazı kombinasyonların
                    kullanılmasına karşılık gelmektedir.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img1.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img2.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>El yapımı eşyalarınızı çevrimiçi olarak nasıl ve nerede satarsınız?</Text>
                <Text>İnternet dünyanın dört bir yanındaki müşterilere el yapımı ürünleri tanıtmak ve satmak için eşsiz
                    bir pazarlama alanıdır. </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img3.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>El yapımı ürünler için en iyi web siteleri</Text>
                <Text style={styles.dmlH3}>Etsy</Text>
                <Text>Etsy (<Text style={{color: 'blue'}}
                                  onPress={() => Linking.openURL('https://www.etsy.com/')}>etsy.com</Text>) 30 milyondan
                    fazla yaratıcı işin web sitesinde kayıtlı olduğu canlı bir topluluktur. Etsy’de sanat eserleri, el
                    yapımı ürünler ve parçalar dahil çok çeşitli ürünler satılmaktadır.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img4.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Amazon</Text>
                <Text>Dünyanın en büyük kitap, dergi, müzik, DVD, video, elektronik, bilgisayar, yazılım, giyim ve
                    aksesuar, ayakkabı çevrimiçi alışveriş sitesidir. Ürünlerin ticarileşmesi için de iyi bir
                    platformdur. (<Text style={{color: 'blue'}}
                                        onPress={() => Linking.openURL('https://www.amazon.com/')}>amazon.com</Text>)</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img5.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Bonanza</Text>
                <Text>Bonanza (<Text style={{color: 'blue'}}
                                     onPress={() => Linking.openURL('https://www.bonanza.com/')}>bonanza.com</Text>)
                    el yapımı ürünlerinizi müşterilere satarken eğleneceğiniz en basit yönlendirmelere sahip çekici bir
                    sitedir. Birkaç basit tıklamayla kolayca profilinizi oluşturabilir, listeler yükleyebilir ve satış
                    yapmaya başlayabilirsiniz.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img6.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>eBay</Text>
                <Text><Text style={{color: 'blue'}}
                            onPress={() => Linking.openURL('https://www.ebay.com/')}>eBay</Text> eBay e-ticaret
                    dünyasını tam anlamıyla değiştiren sitelerden biridir. 100 milyondan fazla alıcıyla eBay, dünyanın
                    en tanınmış çevrimiçi pazarlarından biridir.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img7.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>ArtFire</Text>
                <Text>ArtFire (<Text style={{color: 'blue'}}
                                     onPress={() => Linking.openURL('https://www.artfire.com/')}>artfire.com</Text>)
                    dünya genelinden zanaatkarlar tarafından tasarlanmış hızla büyüyen çevrimiçi ticaret ortamıdır.
                    30,000’den fazla kayıtlı satıcısı ile ArtFire özelleştirme seçeneklerine de izin veren kullanımı
                    kolay bir platforma sahiptir.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img8.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>DaWanda</Text>
                <Text>DaWanda (<Text style={{color: 'blue'}}
                                     onPress={() => Linking.openURL('http://en.dawanda.com/')}>dawanda.com</Text>)
                    benzersiz ve el yapımı ürünlerin satıldığı lider çevrimiçi ticaret ortamlarından biridir. DaWanda’da
                    280,000 satıcı aktiftir, alıcıları çekmek için mükemmel bir yaklaşımdır.</Text>
                <Text style={styles.dmlH3}>Zibbet</Text>
                <Text><Text style={{color: 'blue'}}
                            onPress={() => Linking.openURL('https://www.zibbet.com/')}>Zibbet</Text> güzel sanatlar ve
                    fotoğraflardan zanaat ürünlerine kadar elyapımı ürünlerin satıldığı çevrim içi bir ticaret
                    ortamıdır.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img9.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
