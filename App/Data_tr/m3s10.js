import React, {Component} from 'react'
import {Text, View, Image} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S10 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Logo tasarlamak için aşağıdaki adımları takip edino</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 1: Cep telefonunuzdan internet tarayıcısı açın</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522778-PIXKA7-878.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
