import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sevkiyat dökümanları – gümrük beyannamesi ve formları</Text>
                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Gümrük
                    beyannamesi nedir?</Text>
                <Text>Gümrük beyannamesi ithalat ve ihracat yapılan ürünlerin içeriğini ve özelliğini gösteren resmi
                    belgedir.
                    {"\n\n"}Yasal anlamda gümrük beyannamesi kişinin gümrük prosedürü doğrultusunda ürünleri gönderme
                    işlemidir (European Commission 2018).
                </Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Gümrük
                    beyannamesi kimler için gerekli?</Text>
                <Text>Genel anlamda ithalat veya ihracat yapan kişiler veya bu ürünleri üreten kişiler bu evrakı
                    düzenlemekle yükümlüdür. Ürünlerin kontrolünden sorumlu olan kişi de bu belgeyi düzenleyebilir.
                    İşlemi yapan kişi veya şirketler olabileceği gibi bazı durumlarda kuruluşlar da olabilir (European
                    Commission 2018).</Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Gümrük
                    beyannamesi nereye verilir?</Text>
                <Text>Ürünlerin bildirileceği veya bildirildiği gümrük kapılarına sunulmalıdır (European Commission
                    2018).</Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Neden gümrük
                    beyannamesi verilmelidir?</Text>
                <Text>Yasal zorunluluklara uymak ve malları gümrük prosedürüne sokmak için gümrük beyannamesi
                    verilmelidir (Avrupa Komisyonu 2018) Aşağıdaki İki durumda bu işlem uygulanmaktadır:</Text>
                <FlatList
                    data={[
                        {key: 'İthalatta, mallar gümrük bölgesine getirildiğinde, gümrük onaylı bir muamele veya kullanıma tahsis edilmesi gerekir.'},
                        {key: 'ihracata gönderilecek ürünler – ihracat işlemlerine uygun  yerleştirilmelidir'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
