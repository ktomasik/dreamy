import React, {Component} from 'react'
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {Row, Rows, Table} from 'react-native-table-component';
// Styles

export default class M4S5 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['Format', 'Açıklama', 'Not'],
            tableData: [
                ['99 99 999 999 999', 'Vergi kimlik numarası(TIN) 13 basamaktan oluşur', 'TIN’in ilk karakteri "0", "1", "2" veya "3" olmalıdır. Bilgisayar üzerinden NIF boşluksuz 13 basamak olarak yazılır.']
            ],
            tableHead2: ['Numara', 'Konu', 'Şekli', 'Hangi yönetici verir?', 'Numara'],
            tableData2: [
                ['Siren(İş Rehberi Tanımlama Kodu)', 'Yönetimlerle her şirkete özgü tanımlama.', '9 basamak', 'Insee(Ulusal İstatistik Enstitüsü) CFE aracılığıyla', 'Siren'],
                ['Siret(Kurumlar Dizin Tanımlama Kodu)', 'Sosyal ve mali kurumlarıyla aynı şirketin her kadrosunun tanımlanması.\nÇalışan maaş bordrosunda görünmeli..', '14 basamak = 9 Siren basamak + 5 basamak her enstitüye ayrı', 'Insee(Ulusal İstatistik Enstitüsü) CFE aracılığıyla', 'Siret'],
                ['APE kodu (yürütülen temel faaliyet) veya NAF kodu', 'İşletmenin veya serbest meslek faaliyet dalının tanımlanması.\nUygulanabilir kolektif sözleşmede tanımlamak üzere kullanılır.\nÇalışan maaş bordrosunda bulunmalı.', '4 basamak + 1 harf, Fransız ulusal istatiski sınıflandırmasında yer alacak faaliyet (Nacre 2, itibariyle yürürlükte)', 'CFE aracılığıyla\nYanlış olması durumunda düzenlenebilir', 'APE kodu (yürütülen temel faaliyet) veya NAF kodu'],
                ['RCS (Ticaret ve Şirketler Kaydı)', 'Tüccarların ve ticari şirketlerin RCS\'ye kaydı.\nK veya Kbis((mikro-işletmenin kayıt işlemini onaylayacak belge) özetinde şekillendirilmiştir.', 'RCS + kayıt şehri + Siren numarası', 'CFE aracılığıyla ticari mahkemeye kayıt', 'RCS (Ticaret ve Şirketler Kaydı)'],
                ['Ticaret rehberi (RM)', '10’dan fazla çalışanı olmayan esnaf şirketleri ve esnafların yer aldığı ticaret rehberine zorunlu kayıt.\nD1 özetinde şekillendirilmiştir', 'ticaret odası ve ilgili el sanatları tanımlayan No. Siren + RM + basamaklar', 'CFE aracılığıyla Esnaf ve Sanatkarlar Odası', 'Ticaret rehberi (RM)'],
                ['Topluluk içi KDV', 'KDV mükellefi herhangi bir şirketin vergi numarası\nFatura ve KDV iadesinde bulunmalı', 'FR + 2 basamak + Siren numarası', 'CFE aracılığıyla Ticaret Vergi Hizmeti (SIE)', 'Topluluk içi KDV'],
            ]
        }
    }

    render() {
        const state = this.state;
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Vergi kimlik numarası nedir?</Text>
                <Text>Vergi kimlik numarası (TIN) ülke içerisinde vergi amaçlı oluşturulan kimlik numarasıdır (Anonymous
                    2018f).
                    {"\n"}Tüm tüzel kişilikler, Kurumsal olmayan kuruluşlar ve bireyler vergi kimlik numarası almak
                    zorundadır.
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>TÜRKİYE’deki uygulamalar</Text>
                <Text>E-Beyan sistemi mevcuttur. Aslında günümüzde kişilerin kimlik numaraları da vergi numarası olarak
                    kullanılabilmektedir. Kimlik numarası noter onaylanmalı veya vergi dairesi memurları tarafından
                    onaylanmalıdır. Böylece vergi kimlik numarası alınabilir (Türkiye Ulusal Raporu, 2018).</Text>
                <FlatList
                    data={[
                        {key: 'Vergi kimlik numarası Türkiye’deki mesleki veya iş faaliyetleri kapsamında kullanılmaktadır.'},
                        {key: '1 Temmuz 2006 itibariyle TC kimlik numarası Türk vatandaşları için tek bir tanımlama numarası olarak kullanılmaktadır ve vatandaşların vergi numaraları vergi kayıt sisteminde kimlik numaralarıyla eşleştirilmektedir.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>SLOVENYA’daki uygulamalar</Text>
                <Text>Vergi kaydı için gerekli bilgilerin girişi
                    {"\n"}Ayrıca alfa ve numeric karakterlerin de vergi numarasına eklenmesi gerekebilir.
                    {"\n"}Bireysel girişimci olarak şahıs vergi dairesine başvurursa, mali yönetin yeni bir vergi kayıt
                    numarası (TIN) oluşturmaz (Slovenya Ulusal Raporu, 2018).
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Polonyada’daki uygulamalar</Text>
                <Text>Her yetişkin bireyin vergi dairesine sorumlu olması için vergi numarasına sahip olması
                    gerekmektedir.
                    {"\n"}Yetişkin bir birey girişimci olmak isterse aynı vergi kayıt numarasını kullanabilmektedir
                    (Polonya Ulusal Raporu, 2018).
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Yunanistan’daki uygulamalar</Text>
                <Text>Her yetişkin bireyin vergi kayıt numarası olması gerekmektedir.
                    {"\n"}Yetişkin bir birey girişimci olmak isterse vergi dairesine iş amaçlı kayıt olması
                    gerekmektedir.
                    {"\n"}Bu yetkiyle birlikte kendi kimlik numarasını ticari amaçlı vergi kayıt numarası olarak
                    kullanabilmektedir (Yunanistan Ulusal Raporu, 2018).
                </Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Fransa’daki uygulamalar</Text>
                <Text>NIF yapısı</Text>
                <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                    <Row data={state.tableHead} style={styles2.head} textStyle={styles2.text}/>
                    <Rows data={state.tableData} textStyle={styles2.text}/>
                </Table>
                <Text>
                    Vergi Kimlik Numarası (TIN) açıklaması
                    {"\n\n"}Fransız vergi makamları, Fransa’da vergi bildirme zorunluluğu bulunan tüm gerçek kişilere
                    vergi kimlik numarası verir. Birey vergi kayıt sistemine dahil olduğunda TIN tanımlaması yapılır.
                    “Referans vergi numarası” (veya “SPI”, Basitleştirilmiş Vergilendirme Prosedürü) olarak da
                    tanımlanmaktadır. Tüm vergilendirmelerde DGFIG (referansiyel PERS) çerçevesinde değerlendirilir Tüm
                    vergilendirme için DGFiP'nin (referans PERS) kişilerin referans çerçevesinde oluşturulan kişiye
                    bağlanır. Benzersiz, özelliksiz, güvenilir ve dayanıklıdır.
                    {"\n\n"}NIF nerede bulunur?
                    {"\n"}Referans vergi numarası, önceden doldurulmuş gelir vergisi beyannamesi ve gelir vergisi, konut
                    vergisi ve emlak vergisi bildirimlerinde görünür. Bir belge birden fazla kişi tarafından
                    paylaşılıyorsa, ilgili vergi mükelleflerinin her birinin vergi numarası, önceden doldurulmuş vergi
                    beyannamesi "Medeni durum" bölümündeki bilgilere göre belirtilir. Tahsil edilmesi gereken TIN,
                    duruma bağlı olarak, hesap sahibinin veya sözleşme sahibinin NIF'i, mülkün sahibinin TIN'i veya
                    gelirin yararlanıcısının TIN'ıdır. Birden fazla kişi dahil ise (örneğin, iki kişi tarafından
                    düzenlenen ortak hesap durumunda), her bir kişinin TIN'i alınmalıdır.
                </Text>
                <Text>
                    Ulusal NIF website: <Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.impots.gouv.fr/portail/')}>impots.gouv.fr/portail</Text>
                    {"\n"}Kaynak: <Text style={{color: 'blue'}}
                                        onPress={() => Linking.openURL('https://ec.europa.eu/taxation_customs/tin/pdf/fr/TIN_-_country_sheet_FR_fr.pdf')}>ec.europa.eu/taxation_customs/tin/pdf/fr/TIN_-_country_sheet_FR_fr.pdf</Text>
                </Text>
                <Text>
                    Sirene rehberi tüm şirketleri ve kuruluşlarını, yapılanmaları ve faaliyet alanlarına bakmaksızın kayıt altında tutmaktadır. Fransa’da faaliyet gösteren yabancı şirketler ve
                    temsilcilikleri de listelenmektedir.
                </Text>
                <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                    <Row data={state.tableHead2} style={styles2.head} textStyle={styles2.text}/>
                    <Rows data={state.tableData2} textStyle={styles2.text}/>
                </Table>
            </View>
        )
    }
}
const styles2 = StyleSheet.create({
    head: {minHeight: 40, backgroundColor: '#f1f8ff'},
    text: {margin: 6}
});
