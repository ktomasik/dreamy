import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img8.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m9/img9.png')
    }
}];

export default class M9S5_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme Instagram hesabı oluşturma</Text>

                <Text style={{marginTop:10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 1:</Text> E-posta adresi veya cep telefon numarasıyla giriş yapın ve kullanıcı adı girin.</Text>
                <Text style={{textAlign: 'center', color: 'red'}}>VEYA</Text>
                <Text>Eğer bir Facebook hesabınız varsa, aynı bilgilerle ve linkle giriş yapılabilir (Modül 1’de nasıl Facebook hesabı açıldığı anlatılmıştır).</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop:5}}>“Üye ol” üzerine gelin ve e-posta adresinizi girin. “Sonraki”ne basın veya Facebook hesabınızla giriş yapmak için “Facebook la giriş yapın”a dokunun. </Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
