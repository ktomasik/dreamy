import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S23 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Mikro Finans ve Diğer Girişimci Fonları</Text>
                <Text style={styles.dmlH2}>Türkiye:</Text>
                <Text style={styles.dmlH3}>Kredi Garanti Fonu (The Credit Guarantee Fund- KGF)</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'KGF, kar amacı gütmeyen bir anonim şirkettir ve yetersiz teminat nedeniyle borçlanmayan KOBİ\'ler ve KOBİ olmayan işletmeler için garantör olarak hareket etmektedir. Bu nedenle KGF, KOBİ\'leri ve KOBİ olmayan işletmeleri finansmana erişim konusunda desteklemektedir. Kredi, kadın girişimcilerin işlerini büyütmelerini ya da işlerini zor durumlardan uzaklaştırmalarını destekler.',
                                val2: 'http://www.kgf.com.tr/index.php/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Türkiye Grameen Mikrofinans Programı</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Türkiye Grameen Mikrofinans Programı (TGMP) kar amacı gütmeyen ekonomik bir kuruluştur. Geleneksel bağışlar ve “yardım” yerine, TGMP Türkiye\'de yoksulluğun azaltılmasına yardımcı olmak için “mikrokredi” hizmetleri sunmaktadır. Mikro kredi sisteminin amacı düşük gelirli kadınların sürdürülebilir gelir getirici faaliyetlerde bulunmalarına ve aile bütçelerine katkıda bulunmalarına yardımcı olmaktır. Resmi (ticari) bankacılık sektöründen farklı olarak , mikrokredi kredileri, teminat veya bir kişinin Türk ulusal kimlik kartı dışındaki herhangi bir belgeye ihtiyaç duymadan verilir. Bazı mikro kredi ürünleri aşağıda listelenmiştir;',
                                val2: 'http://www.tgmp.net/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Temel Kredi</Text>
                <Text>Temel Kredi eski ve yeni üyeler için ilk kredi türüdür. Yeni üyelere 100 TL'den 1.000 TL'ye kadar
                    kredi verilebilir ve 46 haftada kredi geri ödemesi yapılır.</Text>

                <Text style={styles.dmlH3}>Girişimci Kredisi</Text>
                <Text>Girişimci ve başarılı üyeler için 1.000 TL'den 5.000 TL'ye kadar verilebilinen kredi türüdür ve 46
                    hafta boyunca kredilerin geri ödemesi yapılabilir.</Text>

                <Text style={styles.dmlH3}>Dijital Bölünme (Eşitsizlik) Kredi</Text>
                <Text>Üyelerce alınmış kredilere ek olarak, üyelerin teknolojik gelişmelerini sağlamaya yönelik kredi
                    türüdür. Bu kredi sayesinde üyeler bugünün teknolojisi ile kullanılan akıllı telefonlara sahip
                    olabilirler. Kredi geri ödemeleri 46 haftada yapılır.</Text>
            </View>
        )
    }
}
