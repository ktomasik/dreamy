import React, {Component} from 'react'
import {FlatList, Image, Linking, Modal, StyleSheet, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img9.png')
    }
}];

export default class M8S8 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Kargo gönderisi</Text>
                <Text styl={{marginTop: 10}}>Kargo gönderisi yapmak için aşağıdaki adımları takip edin:</Text>
                <Text style={styles.dmlH3}>ADIM 1: ÜRÜNLERİ UYGUN ŞEKİLDE PAKETLEYİN</Text>
                <View style={styles2.border}>
                    <Text>Uygun kargo paketi yapmak için tavsiyeler:</Text>
                    <FlatList
                        data={[
                            {key: 'Uygun paket çeşidini seçin,'},
                            {key: 'Uygun paket büyüklüğünü seçin,'},
                            {key: 'Hassas ürünleri koruyucu yumuşak materyallerle sarın,'},
                            {key: 'Eğer gerekliyse paketin üstüne "kırılır" gibi uyarı etiketleri kullanın,'},
                            {key: 'Eğer gerekliyse taşıma yönünü bildirecek işaretleme yapın'}
                        ]}
                        renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                    />
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m8/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
