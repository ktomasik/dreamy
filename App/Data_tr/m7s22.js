import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S22 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Mikro Finans ve Diğer Girişimci Fonları</Text>
                <Text style={styles.dmlH2}>Slovenya:</Text>
                <Text style={styles.dmlH3}>Vstopne točke SPOT (VEM)</Text>
                <Text>Bilgi, temel danışmanlık, şirket kaydı</Text>

                <Text style={styles.dmlH3}>Podjetniški inkubatorji</Text>
                <Text>Donanımlı ofisler, iş ve diğer destek hizmetleri</Text>

                <Text style={styles.dmlH3}>Univerzitetni inkubatorji</Text>
                <Text>Donanımlı ofisler, iş danışmanlığı ve mentorluk, ücretsiz eğitim atölyeleri</Text>
                <Text>Podjetniški inkubator Univerze v Mariboru</Text>
                <Text>Ljubljanski univerzitetni inkubator</Text>
                <Text>Univerzitetni razvojni center in inkubator Primorske</Text>

                <Text style={styles.dmlH3}>Tehnološki parki</Text>
                <Text>Donanımlı ofisler, mentorluk, danışmanlık, ortak çalışma,</Text>

                <Text style={styles.dmlH3}>Iniciativa Start:up Slovenija</Text>
                <Text>Ağ oluşturma, rekabetin organizasyonu</Text>

                <Text style={styles.dmlH3}>Mreža European Enterprise Network (EEN)</Text>
                <Text>İş ortakları, bilgi, danışma için araştırma yapma</Text>

                <Text style={styles.dmlH3}>Coworking prostori Coworking MB Hekovnik</Text>
                <Text>İş kurma, ağ kurma, iş ortaklarını arama, bilgi, danışma, eğitim</Text>

                <Text style={styles.dmlH3}>Gospodarska zbornica Slovenije</Text>
                <Text>Danışmanlık, eğitim, uluslararasılaşma konusunda yardım</Text>

                <Text style={styles.dmlH3}>Obrtno podjetniška zbornica Slovenije</Text>
                <Text>Danışmanlık, eğitim, zanaat lisansı verilmesi, AB sertifikaları, Slovenya Cumhuriyeti'ndeki
                    sürekli yapılmayan el işi faaliyetleri için sertifikalar, taşımacılık yapma lisansları</Text>

                <Text style={styles.dmlH3}>Program Erasmus za mlade podjetnike</Text>
                <Text>Girişimcilik eğitiminin eş finansmanı - AB'deki girişimciler arasında deneyim değişimi</Text>
            </View>
        )
    }
}
