import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Online alış veriş ne demektir?</Text>
                <Text>Çevrimiçi alışveriş, tüketicilerin bir satıcıdan Internet üzerinden bir tarayıcı kullanarak
                    doğrudan ürün veya hizmet satın almalarını sağlayan bir elektronik ticaret şeklidir.</Text>

                <Text style={styles.dmlH2}>Online pazarlama ne demektir?</Text>
                <Text>Online pazarlama internet üzerinden ürün ve servislerin geliştirilmesi için kullanılmakta olan
                    araçlar ve yöntemlerdir. Online pazarlama internet üzerindeki ekstra kanallar ve pazarlama
                    mekanizmaları vasıtasıyla yapılan geleneksel iş pazarlama yöntemlerini daha geniş kapsamda
                    içermektedir.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmark2.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
