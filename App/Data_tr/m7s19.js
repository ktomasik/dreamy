import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S19 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadın Girişimciler için Vergi Teşvikleri</Text>
                <Text style={styles.dmlH2}>Yunanistan:</Text>
                <Text>Yunanistanda, yenilikçiliği artımak için yeni devlet kanunu kapsamında teşvikler verilir. Üretim
                    yapan ya da İşletmenin adına uluslararası kabul görmüş patent tescil hizmetleri sağlayan şirketler 3
                    yıl süreyle gelir vergisinden muaftır.</Text>
                <Text>Özellikle, patentli bir ürünün satışından elde edilen ilk gelirin kullanımından itibaren ard arda
                    3 yıl gelir vergisinden muaf olan adına uluslararası kabul görmüş olan patenti olan firmanın
                    ürettiği ürünün satışından firma kazancının artacağı öngörülebilinir.</Text>
                <Text>Ürünler üçüncü taraf kurulumlarında üretildiğinde de muafiyet verilir. Uluslararası kabul görmüş
                    patentli hizmetlerin sunulmasından doğan kazançlarda da muafiyet uygulanır.</Text>
            </View>
        )
    }
}
