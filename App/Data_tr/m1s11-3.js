import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S11_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İOS işletim sistemini kullanan cihazlarda (iPhone, iPad ya da iPod) e-posta hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 6: Doğrulama kodunun mesajla mı yoksa e-posta ile mi size ulaştırılmasını istediğinizi seçin</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex:1, width: 320, height: 350}}
                        source={require('../Images/images/483352-PGP3GE-251.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
