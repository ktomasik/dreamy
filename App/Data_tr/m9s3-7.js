import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img4.png')
    }
}];

export default class M9S3_7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme için Facebook sayfası oluşturma</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 6:</Text> Resimler
                    ekle : Profilini yükle ve Facebook Sayası’nda kapak resimleri ekle. Buna ek olarak, iş logosu
                    eklenebilir. Eğer logonuz yoksa, yaptığınız ürünlerden eklenebilir ve sonrasında güncellenebilir.
                    İlk izlenim için güzel bir intiba bırakmak önemlidir (Bakınız Modül 3).
                    {"\n\n"}Diğer yandan resmin büyüklüğü önemlidir. Facebook kapak sayfasındaki resmin ebatları
                    563x315px oranında olması cep telefonlarında görünürlüğe fayda sağlamaktadır.
                </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img4.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
