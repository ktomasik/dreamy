import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {

        "quiz7": {
            "question1": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Faiz  düşük olduğunda, borçlanmak da ucuz olur."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Banka",
                    "option2": "b) Bölgesel ticaret ve sanayi odası",
                    "option3": "c) Finansal hizmetler",
                    "option4": "d) Kurumsal yatırım merkezi"
                },
                "question": "Küçük işletme kredisi için en iyi anlaşma nerede bulunur?"
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Hibeler genellikle bir devlet departmanı tarafından tahsil edilen ödenebilir fonlardır."
            },
            "question4": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Girişimciler için devlet teşvikleri sadece finansal olmayan tiptedir."
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Özel yatırım teşviki.",
                    "option2": "b) Mikro işletmelerin yaratılması ve geliştirilmesi için destek.",
                    "option3": "c) Şirketler için uygun finansal kaynaklar sağlamak.",
                    "option4": "d) Bölgesel garanti programlarından finansal teşvikler."
                },
                "question": "Kadınlar için en özgün banka desteği türü olan küçük işletme hibesi nedir?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Vergi teşviki, bireylerin ödemek zorunda oldukları vergi miktarını azaltarak para biriktirmelerini teşvik eden bir hükümet önlemidir."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Mikrokredi",
                    "option2": "b) Finans Şemsiyesi",
                    "option3": "c) Mikro borç",
                    "option4": "d) Hiçbiri"
                },
                "question": "Microfinans ile ilişkili olan hangisidir?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Mikrofinans ve sosyal sermaye, girişimciliğin güçlendirilmesi ile önemli bir ilişkiye sahiptir."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "İş melekleri genellikle bir rehber veya mentor olarak hareket ederek projeye dahil olmakla ilgilenirler."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Kredi",
                    "option2": "b) Bakiye",
                    "option3": "c) Borç",
                    "option4": "d) Mikrokredi"
                },
                "question": "____________ bir bankadan veya başka bir borç verenden para almadır."
            }
        }
    }
};

export default class M7Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz7;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
