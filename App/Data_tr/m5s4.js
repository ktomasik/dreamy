import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m5/img1.png')
    }
}];

export default class M5S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İlk Kredi Kartı Alımından Önce Bilinmesi Gerekenler (örneğin, kredi kartı
                    ekstresi nedir? kredi kartı faizi nasıl hesaplanır?, minimum ödemeler nasıl belirlenir?</Text>
                <Text style={{marginTop: 15}}>Ödeme Ekstre, faturalandırma dönemlerinde kredi kartı hesabına yansıtılan
                    tüm satın alımları, ödemeleri, diğer borçları ve kredileri listeleyen dönemsel belgedir. Kredi kartı
                    veren kurum, ekstreyi ayda bir kez gönderir.
                    {"\n\n"}Ekstrede neler yer alır?
                    {"\n"}Ekstere, kredi kartı hesabı hakkında bilinmesi gereken herşeyi listeler. İçeriğinde şunlar yer
                    alır:
                </Text>
                <FlatList
                    data={[
                        {key: 'Önceki fatura dönemi bakiyesi'},
                        {key: 'Asgari ödeme tutarı'},
                        {key: 'Ödeme Dönemi'},
                        {key: 'Geç ödeme durumunda alınacak gecikme bedeli'},
                        {key: 'Özet, ve hesaba yansıtılan ödemeler, krediler, satın alımlar, bakiye transferi, nakit avans, harçlar, faizler ve diğer borçların detaylı listesi'},
                        {key: 'Hesaptaki bakiye türlerinin dağılımı ve faiz oranları ve herbiri için uygulanan faizler'},
                        {key: 'Kredi limiti ve mevcut kredi'},
                        {key: 'Faturalandırma dönemindeki gün sayısı'},
                        {key: 'Yıldan yıla ödenen toplam faiz ve ücret tutarı'},
                        {key: 'Kredi kartı veren kurumun iletişim bilgileri'},
                        {key: 'Varsa, kazanılan ve geri alınan ödüller '}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 10}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m5/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 10}}>Kredi kartı ekstresi, yalnızca minimum ödeme yaparsanız ve ödenmesi
                    gereken toplam tutarı ödüyorsanız, bakiyenizin ödenmesi için gereken süreyi detaylandıran asgari
                    ödeme açıklamasını içerecektir. Ayrıca, üç yıl içinde bakiye ödenecekse, yapılacak aylık ödemeyi de
                    içerir. Bu bilgi, kredi kartı bakiyesini ödemenin en iyi yolunu bulmada yardımcı olur.
                    {"\n\n"}Kredi kartı ekstresi ayrıca, geç ödemenin etkilerini (geç ödeme ve ceza oranı artışı)
                    bildiren geç ödeme uyarısını da içerir.
                    {"\n\n"}Kredi kartı asgari ödemesi, gecikme faizi ve olası faiz artışı ile cezalandırılmadan kredi
                    kartı bakiyesinde ödenebilecek asgari tutardır. Eğer kart ekstresine dikkat ediliyorsa, bir aydan
                    diğerine asgari ödeme tutarının değiştiği görülecektir.
                    {"\n\n"}Bazı kredi kartı veren kurumlar, fatura dönemi sonunda asgari ödemeyi bakiyenin belli bir
                    yüzdesi, genellikle %2 ve %5 arasında, olarak hesaplar.
                    {"\n\n"}Asgari ödeme, fatura döneminin sonunda bakiyenin yüzdesi alınarak ve aylık toplam kredi
                    maliyeti de eklenerek hesaplanabilir.
                    {"\n\n"}Kredi kartı veren kurumun hangi yöntemi kullandığı, kredi kartı sözleşmesinden
                    öğrenilebilinir. “Asgari ödemeniz nasıl hesaplanır” veya “Ödeme Yapma” başlıklı bölümlerde açıklama
                    bulunacaktır.
                </Text>
            </View>
        )
    }
}
