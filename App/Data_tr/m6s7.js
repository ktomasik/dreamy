import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img20.png')
    }
}];

export default class M6S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Elektronik imza oluşturma</Text>
                <Text style={{marginTop: 15}}>Kağıt tabanlı işlem sistemleri internete taşınınca yeni iş fırsatları
                    ortaya çıkmıştır. Faturalar, sözleşmeler, e-postaları vb. gibi iş amaçlı kağıt tabanlı işlemlerde
                    elektronik imzanın kullanılması işletmenin geliştirilmesinde önemlidir. Cep telefonlarından
                    eletronik imza indirilebilecek çok sayıda platform vardır. DocuSign bunlardan biridir. DocuSign
                    ücretsiz ve cep telefonlarında elektronik imza kullanmak için uygun platformdur. iphone,ipad,
                    Android ve Windows’da mevcuttur.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img20.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
