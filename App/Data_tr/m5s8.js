import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ödemeleri Alma</Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>ADIM 1: İşinizi
                    kurun Kurulan işte ödemeyi almak için aşağıdaki talimatları takip edin</Text>
                <Text>Doğru organizasyon ve yapıyı kurmak için seçenekleri önceden kontrol edin. Bu şekilde yanlış iş
                    türü seçilmemiş olacaktır. Birçok durumda daha sonra değiştirilebilinir ancak ister tek mülkiyet,
                    ister limitli sorumlu, ister şahıs şirketi, ister anonim şirket ya da farklı tür bir şirket olsun,
                    en uygununun hangisi olduğunu görmek için önceden araştırma yapmaya yardımcı olur. Bir muhasebeci,
                    işin kurulmasına yardımcı olabilir ve gerekli evraklarla ilgilenebilir.
                    {"\n\n"}Eğer ortaklık söz konusu ise, tüm bilgilerin doğru olduğundan emin olunmalıdır.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>ADIM 2: İşletme
                    için Kimlik Numarası Alın</Text>
                <Text>İşletme banka hesabı açılması düşünülüyorsa bu önemli numaraya ihtiyaç olacağından, kimlik
                    numarası almak ilk gerekli adımdır.
                    {"\n\n"}İşletme için birden fazla kimlik numarasına ihtiyaç olunabilir (örn. Vergi dairesine
                    kaydolunurken KDV numarası, yerel odaya kaydolunurken kimlik numarasına, vb)
                    {"\n\n"}PayPal gibi bir işlemciden ödeme almak isteniyorsa, şirket kimlik numarası da yardımcı
                    olabilir. Ayrıca verilen tüm makbuz ve faturalar da görünmek zorundadır.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>ADIM 3: İşletme
                    Banka Hesabı Açma</Text>
                <Text>Ödeme alınmaya başlandığında, bunların muhafaza edileceği bir yere ihtiyaç doğar.
                    {"\n\n"}İşletme sahibi olarak ayrı bir hesaba sahip olmak önemlidir. İlk olarak, şahsi varlıkların
                    şirket varlıklarından ayrı tutulması iyi bir fikirdir. İkincisi, kayıt tutmayı daha kolaylaştırır.
                    Vergi zamanı geldiğinde, herşey tek bir yerde ise tümüyle ilgilenmek daha kolaydır.
                    {"\n\n"}Şahsi paralar, şirket paralarıyla karıştırılırsa ciddi sonuçlar ortaya çıkabilir.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>ADIM 4: Üçüncü
                    Şahıslardan Ödeme Almaya Hazırlanma</Text>
                <Text>Çok sayıda ödeme yöntemini kabul etmek önemlidir. Müşterilerin ve alıcıların kendi ödeme
                    tercihleri vardır. Onları temin etmenin en iyi yolu üçüncü şahıs işlemci kullanmaktır.
                    {"\n\n"}Kart işlemcisi ile satıcı hesabı oluşturulabilinir veya Pay Pal gibi bir site üzerinden
                    ödeme alınabilinir. Bu tür bir işlemci kullanıldığında, kredi kartları genellikle dikkate alınır.
                    {"\n\n"}Mobil cihazdan fiziksel kredi kartlarını kabul etmek için de işlemci kullanılabilinir. Bu
                    işlemci, satılacak öğelerin fiziksel olduğu ve satışın yüz yüze yapıldığında yardımcı olacaktır.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>ADIM 5: İşletme
                    Şartlarını Bildirmek</Text>
                <Text>Ödeme alımlarını ayarlamadan önce, ülkedeki işletmeler için gerekli şartlardan emin olunmalıdır.
                    Ülkenin Ekonomi Bakanlığı (ya da diğer yetkili makamlar)’nın iş kurma ve işi yapmaya başlamak için
                    gerekenler hakkında bilgi sahibi olmalıdır.</Text>
            </View>
        )
    }
}
