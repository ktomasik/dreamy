import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S5 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme Instagram hesabı oluşturma</Text>
                <Text>Hesap oluşturma : Modül 2’de nasıl Instagram hesabı oluşturulacağı anlatılmıştır. Modül 2’de de
                    belirtildiği üzere, Instagram yüklemesi iOS App Store’dan, Android için Google Play Store’dan veya
                    Windows Telefon için Windows Phone Store’dan yapılabilmektedir.
                    {"\n\n"}Bu uygulama bir kez yüklendiğinde, Instagram’I açmak için cep telefonu üzerindeki uygulama
                    ikonuna basılması gerekmektedir.
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
