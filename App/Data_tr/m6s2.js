import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text>Modül 1 ‘de görüldüğü üzere İnternet ve Web arasında ayrım yapılması zorunludur:
                    İnternet, rehberli, kablosuz ve fiber-optik teknolojilerle birbirine bağlanmış özel, kamu, iş ve
                    akademi ve devlet ağlarını da içeren küresel değişim ağıdır
                    {"\n\n"}Ağ(web) ya da İnternet sunucuları ağı (WWW), temel olarak özel biçimlendirilmiş belgeleri
                    destekleyen İnternet sunucuları sistemidir.
                    {"\n\n"}Bir bilgisayarı dünyadaki diğer bilgisayarlara bağlayan internet, içerik taşımanın bir
                    yoludur. WWWın, herhangi bir web sayfasının ya da internetin bilgi paylaşma parçasını kapsayan diğer
                    içeriklerin2 gözden geçirilmesi için internet erişimi olmalıdır. Ağ ayrıca birbirine hyperlinks adı
                    verilen köprüler aracılığıyla bağlanan Ağ sayfası denilen Ağ dökümanlarına erişim sağlayan Google ya
                    da İnternet gibi tarayıcıları kullanır.
                    {"\n\n"}İnternet üzerinden mal ve hizmetleri satan Limited Şirket ve/veya serbest tüccar statüsüne
                    sahip vergi mükellefleri, elektronik fatura uygulanmasından faydalanabilirler. Elektronik fatura
                    işlemleri için ciro sınırlaması yoktur. Çok düşük ciroya sahip olanlar bile elektronik
                    faturalandırmadan yararlanabilir.
                    {"\n\n"}Hızlı bir Google araması pek çok seçeneği ortaya çıkaracaktır. Microsoft Office gibi
                    programların kendi yazılımlarında ayarlanabilir şablonları vardır ve bu şablonlar indirilebilir ve
                    Office’de bulunan websiteleri bulunabilir.
                    {"\n\n"}Cashboard : Cashboard, Microsoft Word'de açabileceğiniz indirilebilir bir şablon sunar.
                    Şablonlar basit ama özelleştirilebilir.
                    {"\n\n"}İnvoice berry: Office, Açık Office and Excel Şablonları
                    {"\n\n"}Microsoft- Microsoft Office’teki şablonları beğenilmezse, Microsoft şablon galerisinde daha
                    fazla seçeneğe başvurulur.
                    {"\n\n"}Microsoft Office ek olarak, Google Docs’ın da galerisinden seçebileceğiniz şablonları vardır
                    ve Apple verimlilik uygulamaları için seçenekler sunar.
                    {"\n\n"}Bazı websiteleri, fatura oluşturmaya, çıktı almaya, kaydetmeye, kendi sitelerinden doğrudan
                    fatura gönderilmesine izin verir. Web tarayıcıları dışında başka programlara ihtiyaç yoktur.
                    {"\n\n"}Aşağıdaki bölümde, elektronik fatura oluşturmanın öğrenilebileceği bazı ücretsiz uygulamalar
                    gösterilmektedir.
                </Text>
            </View>
        )
    }
}
