import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import { Button } from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz8": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Nakliye masrafları büyük çaplı işlerde en büyük giderlerden biridir"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Kendi teslim etme",
                    "option2": "b) Aynı gün teslimat",
                    "option3": "c) Mağazadan alma",
                    "option4": "d) Mağazadan yükleme"
                },
                "question": "Aşağıdakilerden hangisi nakliye seçeneklerinden değildir?"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Nakliye ve ikmal stratejisi satışlardaki karlılığı etkileyen bir unsurdur."
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Ufak çaplı el işlerinin ulusal nakliyesinde kargo firmaları çoğunlukla karayolunu tercih etmektedir."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Plastik poşetler",
                    "option2": "b) Karton kutular",
                    "option3": "c) Hava kanalları olan paketler",
                    "option4": "d) Hepsi"
                },
                "question": "Aşağıdakilerden hangisi kargo paketlemede kullanılmaktadır?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Ücretsiz kargo teklifi müşterinin ilgisini çeken etkenlerden biri değildir."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Sigorta",
                    "option2": "b) Fatura",
                    "option3": "c) Gümrük beyannamesi",
                    "option4": "d) Paket etiketi"
                },
                "question": "______ ihracat veya ithalat edilecek ürünlerin listesi ve detaylarını içeren resmi dökümandır."
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Kargo şirketleriyle sürekli çalışmak üzere sözleşme imzalamak nakliye ücretlerini düşürmeye fayda sağlamaktadır."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Şubeler",
                    "option2": "b) Kendinin teslim etmesi",
                    "option3": "c) Arkadaşlar",
                    "option4": "d) Çağrı merkezleri"
                },
                "question": "Aşağıdakilerden hangisi kargo şirketlerine ulaşma yollarından biri değildir?"
            },
            "question10": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Gönderici adı, adresi, telefonu",
                    "option2": "b) Paketin içindeki ürünün rengi ",
                    "option3": "c) Alıcı adı, adresi, telefonu ",
                    "option4": "d) Ödeme bilgisi (kim kargoyu ödeyecek)"
                },
                "question": "Aşağıdakilerden hangisi kargo faturasında yer almamaktadır?"
            }
        }
    }
};

export default class M8Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz8;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
