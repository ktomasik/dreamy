import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İnstagram Hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 14: Profil fotoğrafınızı eklemek için fotoğraf ekle ye
                    tıklayın ve profil fotoğrafınızın hangi kaynaktan ekleyeceğinizi seçin. Fotoğrafınızı Facebook’tan
                    indirebilir, telefonunuzun kamerası ile fotoğraf çekebilir ya da galerinizden fotoğraf
                    seçebilirsiniz.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img50.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img51.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
