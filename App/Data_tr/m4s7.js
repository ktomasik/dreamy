import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Meslek birliklerine kayıt</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>TÜRKİYE’deki uygulamalar</Text>
                <FlatList
                    data={[
                        {key: 'Küçük ve Orrta Ölçekli İşletmeleri Geliştirme ve Destekleme İdaresi  (KOSGEB) '},
                        {key: 'Kadın Statüsü Genel Müdürlüğü (KSGM)'},
                        {key: 'Türkiye İş Kurumu (İŞKUR)'},
                        {key: 'Türkiye Odalar ve Borsalar Birliği (TOBB)'},
                        {key: 'Türkiye Cumhuriyeti Aile ve Sosyal Politikalar Bakanlığı Destekleri'},
                        {key: 'Türk Grameen Mikrofinans programları'},
                        {key: 'Kadınlara özgü banka kredileri'}

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>Slovenya’daki uygulamalar</Text>
                <Text style={{fontWeight: 'bold'}}>Mali Olmayan Destekler</Text>
                <FlatList
                    data={[
                        {key: 'VEM Noktaları'},
                        {key: 'İş inkübatörleri '},
                        {key: 'Üniversite inkübatörleri'},
                        {key: 'Teknoloji parkları'},
                        {key: 'Slovenya ilk yenilikçi girişim'},
                        {key: 'Avrupa İşletme Ağı '},
                        {key: 'SPIRIT Slovenya	'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Mali Destekler</Text>
                <FlatList
                    data={[
                        {key: 'Slovenya İşletme Fonları '},
                        {key: 'Slovenya İşveren Hizmetleri '},
                        {key: 'Slovenya Bölgesel Gelişim Fonları '},
                        {key: 'Banka kredileri'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Diğer Destekler</Text>
                <FlatList
                    data={[
                        {key: 'Ticaret& Sanayi Odası'},
                        {key: 'Slovenya Elişleri ve Küçük İşletmeler Odası'},
                        {key: 'Slovenya İş Melekleri'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>Polonya’daki uygulamalar</Text>
                <FlatList
                    data={[
                        {key: 'Kadın girişimciler için Avrupa Sosyal Fonları (STK, Kamu Kurumları,..etc)'},
                        {key: 'Avrupa Sosyal Fonları (PO WER- Operasyonel Program Bilgi Eğitim Gelişim)'},
                        {key: 'AB kaynaklı olmayan destekler (devlet, özel, vb)'},
                        {key: 'Polonya İş Geliştirme Ajansı'},
                        {key: 'Kadınlar için krediler'},
                        {key: 'İş Melekleri '},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>Yunanistan’daki uygulamalar</Text>
                <FlatList
                    data={[
                        {key: 'Çekirdek Sermaye (gençler veya işsizler gibi belirli gruplara özgü ufak çaplı fonlardır)'},
                        {key: 'OAED Program (halka yönelik)'},
                        {key: 'Açık uçlu fonlar (özel sektöre yönelik)'},
                        {key: 'Banka Kredileri'},
                        {key: 'Ortaklık anlaşmaları (PA) 2014-2020 (ESPA)'},
                        {key: 'Ülke dışındaki Yunan toplulukları'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop:20, fontSize: 17}}>Fransa’daki uygulamalar</Text>
                <FlatList
                    data={[
                        {key: 'CFE (Ticari İşlemler Merkezi)'},
                        {key: 'Ticaret ve Şirketler Kaydı  (RFS)'},
                        {key: 'ACCRE- iş arayanlara yardımcı olmak ve kendi işletmelerinin kuruluşunu kolaylaştırmak için oluşturulmuştur'},
                        {key: 'The BPI France (KAmu Yatırım Bankası)'},
                        {key: 'Bölgesel İnovasyon Ortaklığı (PRI )'},
                        {key: 'İş Melekleri '},
                        {key: 'Entreprendre au Feminin(Kadınlara projelerini gerçekleştirmek için yardım eder)'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop:5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
