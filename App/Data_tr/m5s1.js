import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Giriş</Text>
                <Text style={{marginTop: 10}}>Bu modül, başlangıç (start-up) kadın girişimciler için pratikte yaygın
                    kullanılan muhasebe ilkeleriyle ilgilidir. Özellikle, banka hesaplarının açılması, hesap türleri,
                    bankacılık işlem yolları ve e-ticaret ve internetten (ya da kredi kartı yoluyla) ödeme sisteminden
                    bahsedilir. Ayrıca bunların uygulaması için yönergeleri de içerir.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 280, height: 340}}
                        source={require('../Images/images/6931.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
