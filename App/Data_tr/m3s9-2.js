import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_2 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ürünlerinizi İnstagram’da satmak için aşağıdaki adımları takip edin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 2: Çoktan almış olduğunuz Instagram hesabınızı kullanın
                    ya da ürünlerinizin ticarileşmesi için yeni bir iş ismi alın</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m3/img37.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
