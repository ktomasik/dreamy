import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen6 = [{
    url: '',
    props: {
        source: require('../Images/m3/img38.png')
    }
}];

export default class M3S9_3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ürünlerinizi İnstagram’da satmak için aşağıdaki adımları takip edin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 3: Ürünlerinizin resimlerini instagramda ya da
                    hikayeler kısmında paylaşın,</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen6}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img38.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
