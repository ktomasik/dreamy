import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Mobil ve İnternet Bankacılığına Başlama</Text>
                <Text style={{marginTop: 15}}>Mobil ve İnternet Bankacılığına başlamak için aşağıdaki talimatlar  takip edilmelidir:</Text>
                <Text style={{fontWeight: 'bold'}}>Gerekli olan</Text>
                <FlatList
                    data={[
                        {key: 'Geçerli bir banka ya da kredi kartı'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop:10}}>Yapılacak olan</Text>
                <Text>Mobil cihazda: Android veya Apple cihazınızda Mobil Bankacılık uygulamasını indirin ve açın. Aşağıdaki adımları takip edin.
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Adım 1',
                                val2: 'Bankanın internet anasayfasına gidin'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 2',
                                val2: '“kayıt ol” ‘düğmesini tıklayın'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 3',
                                val2: 'kart numarası ve son kullanma tarihini girin daha sonra “devam et düğmesini tıklayın'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 4',
                                val2: 'Doğrulama kodunu nasıl almak  istenildiğini  belirtin'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />

                <Text style={{marginTop:15}}>Bazı bankalar ayrıca asağıdaki hizmetleri de sunarlar:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Adım 1',
                                val2: 'Hassas işlemleri tamamladığınızda, doğrulama kodu sizi ekstra bir güvenlik katmanı ile korur. Kısa mesaj,  e-posta veya sesli arama ile 6 basamaklı doğrulama kodu gönderebilinir. İşlemi tamamlamak için bu kod girilmelidir.'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 2',
                                val2: 'Kişisel ve ücretsiz e-postalara bir kerelik doğrulama kodları gönderen bazı hizmetler de mevcuttur.'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 3',
                                val2: 'Kimlik doğrulama kutucuğuna doğrulama kodunu girin ve “devam” düğmesini tıklayın'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 4',
                                val2: 'Şifre seçin'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 5',
                                val2: 'Ek bilgileri gözden geçirin'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 6',
                                val2: 'Neredeyse bitti! Elektronik Erişim Anlaşmasını okuyun ve okuduğunuzu onaylamak için kutuyu işaretleyin'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 7',
                                val2: 'Oturumu açın ve bankacılığa başlayın'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 8',
                                val2: 'Şimdi kayıt oldunuz, bankacılık işlemlerini herhangi bir masaüstü bilgisayar, akıllı telefon ya da tabletten yapabilirsiniz.'
                            }
                        },
                        {
                            key: {
                                val1: 'Adım 9',
                                val2: 'Cihazınız parmak izi oturum açmayı destekliyorsa, kimliğinizi doğrulamak ve bir dokunuşta hesaplarınıza erişmek için parmak izi okuyucusunu kullanabileceğiniz bazı Banka uygulamaları vardır'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
