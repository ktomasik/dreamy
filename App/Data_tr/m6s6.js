import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sözleşme Bilgileri</Text>
                <Text style={{marginTop: 15}}>2015 Aralık ayında, AB komisyonu malların uzaktan ve internet üzerinden
                    satışlarındaki sözleşmeler(internette malların satış yönergesi) için yönetmelik önermiştir. Önerilen
                    İnternetten Mal Satış yönetmeliği en üst düzeyde uyum sağlamakta olduğundan Üye devletlerin
                    yönetmelik kapsamında yüksek düzeyde tüketici korumasını ileri sürmesini önlemektedir. Yukarıda
                    ifade edildiği üzere, herşeyden önce, mal ve hizmetlerin AB’de satın alındığı yerde, tüccar ürün
                    satın alınmadan önce ürün ve hizmetler hakkında açık, doğru ve anlaşılabilir bilgiler vermelidir.
                    {"\n\n"}Bu bağlamda, önerilen yeni düzenlemelerle, tipik bir internet alışveriş sözleşmesi aşağıdaki
                    unsurları içermelidir:
                </Text>
                <FlatList
                    data={[
                        {key: 'tüccarın kimliği, adresi, e-postası ve telefon numarası'},
                        {key: 'profesyonel ünvan and tüccarın KDV detayları (varsa)'},
                        {key: 'tüccarın ticaret sicil numarası'},
                        {key: 'ana ürünün özellikleri'},
                        {key: 'tüm vergi ve harçları içeren toplam'},
                        {key: 'teslimat bedeli (mevcut ise) – ve diğer ek harçlar'},
                        {key: 'Ödemenin, teslimat ve işi yapmanın düzenlenmesi'},
                        {key: 'Sözleşmenin süresi (mevcut ise)'},
                        {key: 'Belli ülkelerdeki teslimat sınırlaması'},
                        {key: '14 gün içersinde sipariş iptal etme hakkı'},
                        {key: 'Satış sonrası hizmetlerin mevcudiyeti'},
                        {key: 'Anlaşmazlıkların çözüm mekanizması'}
                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>İnternetten ürün satışında sigorta yaptırılması
                    {"\n\n"}Üretilen ürünler işletmelerin adı altında satıldığından, işletme sahibi sattığı ürünlerden
                    sorumlu tutulabilinir. İnternette kişiye özel yapım kekler satan ve müşterilerin kaplarına dağıtan
                    perakendeciyi örnek alalım. Bu kekleri yedikten sonra müşteri gıda zehirlenmesi sebebiyle satıcıyı
                    suçlarsa, satıcı tazminat taleplerini ödemeye maruz kalabilir. Bunun yanında, kargo aşamasında ürün
                    hasar görebilir ya da çalınabilir. Bu aşamada sigorta, maliyet bedeli nispetinde hasarı yerine koyma
                    maliyetini karşılar. Bu bağlamda, ürün sorumluluğu ve ticari sorumluluk gibi gerçekten ne tür bir
                    sigortaya ihtiyaç olunduğunu anlamaya yardımcı olması için sigorta acentesiyle görüşmek doğru olur.
                    {"\n\n"}Diğer yandan, aşağıdaki adımlarda görüldüğü üzere, günümüzde sigorta yaptırmak oldukça kolay
                    zaman kazandıran işlemdir.En uygun teklifler sadece verilen doğru bilgilerle en kısa zamanda
                    hazırlanır. Ön incelemeden sonra teklif verilir. Böylece en uygun bulunan sigorta satın
                    alınabilinir.

                </Text>
            </View>
        )
    }
}
