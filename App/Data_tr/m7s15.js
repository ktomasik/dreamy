import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S15 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Hibeleri</Text>
                <Text style={styles.dmlH2}>Fransa:</Text>
                <Text style={styles.dmlH3}>PRI (Bölgesel Yenilikçilik Ortaklıkları)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: '',
                                val2: 'https://www.bpifrance.fr/Toutes-nos-solutions/Aides-concours-et-labels/Aides-a-l-innovation-projets-individuels/PRI-Faisabilite'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text>Bu yol, kurulan yenilikçi KOBİ'lere açıktır ancak öncelikli hedefleri değildir.</Text>
                <Text>Sadece 5 bölge ile ortaklık halinde yürütülmektedir:: Grand Est (Alsace, Champagne-Ardenne,
                    Lorraine), Hauts de France (Nord Pas de Calais Picardy), Aquitaine / Poitou Charentes, Pays de la
                    Loire, PACA.</Text>
                <Text>En yenilikçi projeler seçilir ve proje başına maksimum 100.000 ila 200.000 Euro hibe verilir. Bu
                    yardım, projenin ön çalışmaları ve uygulanması ile ilgili masrafların karşılnamasını sağlar. 2
                    taksitte ödenir(%70 ve %30).</Text>
                <Text>Son olarak KOBİ projesi maximum 12 ay içinde gerçekleştirilmelidir.</Text>
            </View>
        )
    }
}
