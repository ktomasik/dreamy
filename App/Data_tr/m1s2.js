import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Web sayfaları</Text>
                <Text>Ağ sayfası (web sayfası) Dünya Çapında Ağ’a ve ağ tarayıcılarına uygun dökümandır.</Text>
                <Text style={styles.dmlH2}>Websitesi</Text>
                <Text>Websitesi, en az bir web sunucusunda yayınlanmış ilgili web sitelerinin, ortak alan adı ile
                    tanımlanan multimedya içeriği de dahil, koleksiyonudur.</Text>
                <Text style={styles.dmlH2}>Web sunucusu</Text>
                <Text>Web sunucusu içeriği ya da hizmetleri internet üzerinden son kullanıcıya ulaştıran
                    sistemdir.</Text>
                <Text style={styles.dmlH2}>Arama motoru</Text>
                <Text>Web arama motoru, Dünya çapında ağda bilgi aramak için tasarlanmış bir yazılım sistemidir.</Text>
                <Text style={styles.dmlH2}>İnternette arama yapma</Text>
                <Text>İnternette arama yapma, bilgiye ulaşmak için Google veya İnternet Explorer gibi arama motorları
                    kullanılarak İnterneti keşfetme sürecidir.</Text>
                <Text style={styles.dmlH2}>E-posta’nın anlamı</Text>
                <Text>E- posta yazılmış mesajları elektronik olarak bir bilgisayardan diğerine gönderme
                    sistemidir.</Text>
                <Text style={styles.dmlH2}>E-posta Adresi nedir?</Text>
                <Text>E-posta adresi iletişim ağı üzerinden e-postaların alınabildiği (ve gönderildiği) elektronik posta
                    kutusu adresidir.</Text>
                <Text style={styles.dmlH2}>“@” ne demektir?</Text>
                <Text>Kullanıcı İnternet adresinden kullanıcının adını ayıran semboldür.</Text>
                <Text style={styles.dmlH2}>Eposta Virüsü nedir?</Text>
                <Text>E-mail virüsü e-posta iletişimleriyle gönderilen veya e-posta iletişimlerine eklenen bir virüstür.
                    Farklı e-posta virüsleri farklı şekillerde çalışsa da, bu tür siber saldırılara karşı koymak için
                    çeşitli yöntemler kullanılmaktadır.</Text>
                <Text style={styles.dmlH2}>İstenmeyen E-posta nedir?</Text>
                <Text>Spam, istenmeyen toptan ileti (önemsiz e-posta) anlamına gelir. Bu genellikle reklam içeren bir
                    mesajın hatta ilgisiz bir içeriğin talep edilmeden birçok alıcıya gönderilmesi demektir.</Text>
            </View>
        )
    }
}
