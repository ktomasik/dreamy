import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S18 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadın Girişimciler için Vergi Teşvikleri</Text>
                <Text style={styles.dmlH2}>Türkiye:</Text>
                <Text>Gelir Vergisi Kanunu'nun 9/6 maddesine göre, ikametgahlarında el yapımı ürünler üreten ve satanlar
                    vergiden muaftır (193 Sayılı Kanun, 27/07/2012 tarihli ve 28366 sayılı Resmi Gazete).</Text>
                <Text>Vergiden muaf olmak için Esnaf Vergi Muafiyeti Belgesi alınmalıdır.</Text>
                <Text>Esnaf muafiyet belgesi almak için aşağıdaki süreç takip edilir.</Text>
                <Text>Adım 1: Belge almak isteyenler ikamet ettikleri yerdeki vergi dairesine dilekçe ile
                    başvurmalıdır.</Text>
                <Text>Adım 2: Aşağıdaki bilgiler, yapılmakta olan işin türü olarak vergi muafiyet belgesine
                    yazılmalıdır: ikametgahlarında el yapımı ürünler üreten ve satanlar(GVK Madde:9/6)</Text>
                <Text>Adım 3: Ev adresi iş adresi olarak gösterilir</Text>
                <Text>Adım 4: Gelir Vergisi Kanunu'nun 9. maddesinde belirtilen şartların yerine getirildiği kabul
                    edilirse, vergi dairesi belgeyi verecektir.</Text>
                <Text>Adım 5: Sertifika sahiplerinden ücret alınmaz</Text>
                <Text>Adım 6: Esnaf vergi muafiyeti belgesi, düzenlenme tarihinden itibaren üç yıl süreyle geçerli olup,
                    bu sürenin sonunda vergi dairesine başvurarak yeni belgelerin alınması mümkündür.</Text>
            </View>
        )
    }
}
