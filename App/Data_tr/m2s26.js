import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S26 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İnstagram Hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 13: Yayınladıkları içeriği takip etmek için ilgili kişileri seçin. İşiniz bittiğinde ileri tuşuna basın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img47.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img48.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop:15}}
                        source={require('../Images/m2/img49.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
