import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Giriş</Text>
                <Text>Bu modül, el sanatlarını satmak isteyen kadınlara çeşitli finansal fırsatları ve yolları
                    açıklamayı amaçlamaktadır. Modül, Dreamy m-Learning projesinde yer alan beş ülke kadınlarına
                    (Slovenya, Fransa, Türkiye, Yunanistan ve Polonya) mali destek ve kredi imkânları için bilgi ve
                    bağlantılar sağlar.
                </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_48_3319639.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
