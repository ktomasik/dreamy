import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img12.png')
    }
}];

export default class M9S5_5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme Instagram hesabı oluşturma</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop:10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 4:</Text> Facebook’a bağlanmak ve ücretsiz işletme profili oluşturmak için “Devam” seçin.</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m9/img12.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop:10, textAlign: 'center'}}>Tebrikler, Instagram işletme hesabınız başarıyla oluşturuldu.
                    {"\n\n"}İçerik paylaşmaya devam edin!
                </Text>
            </View>
        )
    }
}
