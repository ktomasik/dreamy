import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Kredileri nereden bulunur?</Text>
                <Text style={styles.dmlH2}>Slovenya:</Text>
                <Text style={styles.dmlH3}>Bankalar</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Onay ücretsiz 7000 Euro’ya kadar basit ve hzılı internetten kredi',val2: 'http://www.hipkredit.si/'}},
                        {key: {val1: 'kredinin geri ödeme süresince uygun sabit aylık maliyetin tahsis edildiği daha düşük faiz oranına ve maliyete sahip mikro krediler', val2: 'http://www.intesasanpaolobank.si/'}},
                        {key: {val1: 'S.P. cazip teklif',val2:'http://www.sparkasse.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Finansal Hizmetler</Text>
                <FlatList
                    data={[
                        {key: {val1: '500 ya da 1000 EURO değerinde kredi.',val2:'http://www.skupina8.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Bölgesel Ticaret ve Sanayi Odası</Text>
                <FlatList
                    data={[
                    {key: {val1: 'Üyelerin kısa vadeli kredi faiz oranının bir kısmını destekleyen kredi verir',val2:'http://www.eng.gzs.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Girişim Yatırım Merkezi</Text>
                <FlatList
                        data={[
                        {key: {val1: '1000 EURO ile 30000 EURO arasında hızlı internet kredisi',val2:'http://www.pnc.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                       onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
