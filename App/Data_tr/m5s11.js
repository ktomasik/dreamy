import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Banka Hesaplarıyla Ödeme Kabul etme</Text>
                <Text style={{marginTop: 15}}>PayPal İşletme Hesabı kurmak için aşağıdaki talimatları izleyin.
                    {"\n\n"}Hesap kurulumunuzu tamamlamak için aşağıdaki kılavuzları izleyin20. E-posta adresinizi doğrulamanız, PayPal hesabınızı doğrulamanız ve bir PayPal ödeme çözümü seçmeniz gerekir.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 1</Text>
                <Text>PayPal, PayPal iş hesabına kaydolduğunuzda size bir e-posta gönderir E-posta adresinizi onaylamak için e-postadaki bağlantıyı tıklayın. E-postayı bulamıyorsanız PayPal hesabınıza giriş yapın ve İş Profili simgesinin altındaki “Yapılacaklar” listenizde E-posta adresini onayla'yı tıklayın.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 2</Text>
                <Text>Onaylanmış olarak, yalnızca satıcılarınız ve alıcılarınızla daha fazla güvenilirlik kazanmakla kalmaz, aynı zamanda hesabınızdaki para çekme sınırını da kaldırırsınız. Onaylama için 2 yol vardır:</Text>
                <FlatList
                    data={[
                        {key: '1. UnionPay kartınızı onaylayın (anlık onay). UnionPay kart bilgilerinizi ekleyin veya inceleyin. China UnionPay\'e SMS yoluyla bir onay kodu göndermesi için yetki vererek kartı onaylayın. Kartınızı anında onaylamak için kodu girin.'},
                        {key: '2.\tKredi kartınızı onaylayın. PayPal hesabınıza Visa veya MasterCard ekleyin veya inceleyin ve kartı onaylamaya devam edin. Bu, kredi kartı ekstrenize 2-3 iş günü içinde yansıtılacak olan 4 haneli bir kod oluşturur. PayPal hesabınıza giriş yapın, doğrulama işlemini tamamlamak için kodu girin.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Adım 3</Text>
                <Text>İşletmenize uygun bir ödeme çözümü seçin.</Text>
            </View>
        )
    }
}
