import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ürün görselleri neden önemlidir?</Text>
                <Text>Yaşadığımız çağda görsellik büyük önem taşımaktadır. İnsanlar bir şey düşündüğünde ya da
                    yaptığında, zihinlerinde her zaman bir şekil tasarlarlar. Çünkü insan zihni soyut birşeyi
                    somutlaştırmak için çalışır. Bu yüzden el yapımı ürünlerin satışında görsel şeylerin büyük etkisi
                    vardır.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 120}}
                    source={require('../Images/m3/img10.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img11.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 130}}
                    source={require('../Images/m3/img12.png')}
                    resizeMode="contain"/>

                <Text style={styles.dmlH2}>Ürünlerinizi İnstagram’da nasıl satarsınız?</Text>
                <Text>Ürünlerinizi web siteniz üzerinden satmak kar elde etmek için tek yol değildir. Sosya ticaretin
                    gelişen popülaritesi ile ürünlerinizi satmak ve markanızı güçlendirmek için birçok yol vardır. 700
                    milyondan fazla kullanıcısı ile İnstagram hızla sosyal ticaret için santral durumuna gelmiştir.
                    İnstagram kullanıcılarının %80’i Instagram üzerinden bir işi takip etmekte ve %60’ı yeni ürünler
                    keşfetmek için instagramı kullandıklarını söylemektedir.</Text>
                <Image
                    style={{flex: 1, width: undefined}}
                    source={require('../Images/m3/img13.png')}
                    resizeMode="contain"/>
                <Text style={{marginTop: 10}}>İnstagram üzerinden ürünlerinizi satmak için birkaç ipucunu kullanmalısınız:</Text>
                <FlatList
                    data={[
                        {key: 'İş hesabınız olduğundan emin olun.'},
                        {key: 'Yüksek kalitede fotoğraflar ve videolar kullanın.'},
                        {key: 'İlişkilendirmeyi güçlendirmek için hikaye anlatımına odaklanın.'},
                        {key: 'İlginç İnstagram reklamları yaratın.'},
                        {key: 'Farkındalığı yaygınlaştırmak için marka elçilerine ve etkileyicilerine güvenin.'},
                        {key: 'Müşterilerle sohbet edin.'},
                        {key: 'Hashtag keşfi ile yeni müşteriler bulun.'},
                        {key: 'Profil bağlantınızı sık sık güncelleyin.'},
                        {key: 'İnstagram takipçileri için özel indirimler sunun.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 5, marginLeft: 12}}>{item.key}</Text>}
                />
                <Image
                    style={{flex: 1, width: 300, height: 480}}
                    source={require('../Images/m3/img14.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 400}}
                    source={require('../Images/m3/img15.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
