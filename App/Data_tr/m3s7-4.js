import React, {Component} from 'react'
import {Text, Image, View, TouchableHighlight, Modal} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen4 = [{
    url: '',
    props: {
        source: require('../Images/m3/img33.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m3/img34.png')
    }
}];

export default class M3S7_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen4}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH2}>Ürünlerin fotoğrafını çekmek ve bunları ağa yüklemek için aşağıdaki adımları
                    izleyin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 16: e-posta adresinizi doğrulamanız gerekmektedir, e-posta adresinize bir mesaj gelecektir ve bunu doğrulamanız gerekir:</Text>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img33.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img34.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
