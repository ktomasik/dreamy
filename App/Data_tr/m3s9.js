import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ürünlerinizi İnstagram’da satmak için aşağıdaki adımları takip edin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 1: Cep telefonunuzdan “Instagram” a gidin </Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/278998-P5UHEU-627.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
