import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S35 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Instagram'dan diğer sosyal ağlara paylaş</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 5: Paylaşımı #hashtag içeriğine dahil etmek için
                    açıklamanıza #hashtag eklemeniz önerilir. Mevcutta çok fazla takipçisi olan #hashtag’a dahil olmak
                    oldukça akıllıcadır. Robotik için ardunio kalkanı paylaşıldığında #arduino and arduinoshield
                    hashtagları eklenmelidir. Her iki #hashtag on binlerce takipçi tarafından takip edilir. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img66.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img67.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
