import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme için Facebook sayfası oluşturma</Text>
                <Text>Facebook üzerinden satış için, Facebook üzerinden bir sayfasının oluşturulması gerekmektedir
                    (Bakınız Modül 2).
                    {"\n\n"}Facebook sayfanız içerisinde ziyaretçiler mağazanızı görebilir ve ürünlerinize erişim
                    sağlayabilir.
                </Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 1:</Text> Facebook
                    profiline gir</Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
