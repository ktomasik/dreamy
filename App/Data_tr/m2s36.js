import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S36 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Instagram'dan diğer sosyal ağlara paylaş</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 6: Bu gönderiyi diğer sosyal ağlara eklemek için Paylaş
                    bölümü altındaki sekmeleri kontrol edin ve etkinleştirin. En son sağ üst köşedeki Paylaş a
                    tıklayın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img68.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
