import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_4 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ürünlerinizi İnstagram’da satmak için aşağıdaki adımları takip edin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 4: Ürünlerinizle ilgili bilgileri yazın, açıklamalar ve
                    fiyat, müşteriler ile iletişim kurmak için özel e-posta adresinizi verin</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522778-PIXKA7-878.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
