import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S51 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sosyal ağ iletişim sitelerini kullanma</Text>
                <Text style={styles.dmlH2}>Facebook sayfası oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 6: İlgili kategorileri seçin ve İLERİ ye tıklayın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img103.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
