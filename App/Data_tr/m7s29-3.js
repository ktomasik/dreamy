import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Proje ortağı ülkelerde adım adım finansman</Text>
                <Text style={styles.dmlH2}>Yunanistan:</Text>
                <Text style={styles.dmlH3}>Adım 3:</Text>
                <Text>İlk toplantıdan sonra, iş planının oluşturulması ve sonuçlandırılması yetkili bir danışmanla
                    işbirliği içinde gerçekleşir. Bu aşama, ilgili işadamının hazır olma derecesine ve işinin
                    ihtiyaçlarına bağlı olarak birden fazla toplantı gerektirebilir.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
