import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img1.png')
    }
}];

export default class M6S3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Cepte Faturalandırma</Text>
                <Text style={styles.dmlH3}>IOS için</Text>
                <Text>Invoice Simple uygulamasının iPhone, iPad, and iPod Touch’ta ücretsiz sürümü mevcuttur ve cep
                    telefonunda 3 ücretsiz fatura ya da hesaplar oluşturulabilinir.</Text>
                <Text style={{fontSize: 18, marginTop: 10}}><Text style={{fontWeight: 'bold'}}>Adım 1:</Text> App
                    Mağazasına(App Store)’a git and “invoice” arattır (nasıl gidileceğini ve arama yapılacağını görmek
                    için modül 1 and 2’e git)</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
