import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6_3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sözleşme Bilgileri</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 2:</Text> Bilgiler
                    girildikten sonra, bu kriterler değerlendirilecek ve en uygun teklifler bulunulacaktır. İstenilenler
                    seçilerek aralarında karşılaştırma yapılabilinir. Bu karşılaştırma limit, teminat ve fiyat açısından
                    internet üzerinden gözden geçirilmelidir.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_32_3319612.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
