import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Başlangıçta İşletme banka hesabı nasıl açılır?</Text>
                <Text style={{marginTop: 15}}>İşletme banka hesabı, masrafların kolayca takip edilmesine, çalışanların
                    ödemelerinin yönetilmesine, yatırımcılara paranın iletilmesine, ödemenin alınıp, verilmesine ve
                    bütçenin daha doğru bir şekilde planlanmasına olanak sağlar. Hızlı bir şekilde çalışabilmek için
                    İşletme banka hesabını açmada takip edilecek basit adımlar:</Text>
                <FlatList
                    data={[
                        {key: 'İhtiyaç olan hesaba karar ver'},
                        {key: 'Bankayı seç'},
                        {key: 'İşletme adı al'},
                        {key: 'Ödemeleri Kabul etmeye hazır olmak için evrakları al'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>Kredi Kartı ile Banka Kartı arasındaki fark</Text>
                <Text style={{marginTop: 15}}>Kredi kartı, ayrıca kart kredi limiti olarak da bilinen kredi limitine
                    karşılık borç almanızı sağlayan bir karttır. Kredi kartı sonradan hesaba yansıtılan basit işlemleri
                    yapmak için kullanılır. Banka kartı satın alımlarda doğrudan mevduat hesabından para çeker. Bunun
                    gerçekleşmesi birkaç gün sürebilir ve işlem gerçekleşmeden beklemeye bırakılabilir.</Text>
            </View>
        )
    }
}
