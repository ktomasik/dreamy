import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Küçük İşletme Kredileri nereden bulunur?</Text>
                <Text style={styles.dmlH2}>Yunanistan:</Text>
                <Text style={styles.dmlH3}>Bankalar</Text>
                <Text>Bankalar yeni girişimleri finanse ederken, iş planını görmek isterler. Başvuru farklı konuları
                    kontrol eden çeşitli gruplar tarafından incelenecektir, özellikle iş planının gecikmeden kaçınmak
                    için mümkün olduğu kadar eksiksiz olması önemlidir.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
