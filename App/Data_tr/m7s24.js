import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S24 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Mikro Finans ve Diğer Girişimci Fonları</Text>
                <Text style={styles.dmlH2}>Yunanistan:</Text>
                <Text style={styles.dmlH3}>The People’s Trust</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'The People’s Trust yeni bir işletme kurmak veya mevcut bir işi büyütmek isteyen ancak krediye erişimde zorluk çeken Yunan girişimcilere mikro hibeler verir. Mevcut bir işletme için çalışma sermayesi ya da yeni bir işletme için başlangıç sermayesi olarak verilen hibe işletme başına 10.000 Avro\'ya kadardır. Bu finansman programı, diğer finansman türlerine az erişimi olan gruplara odaklanmaktadır.',
                                val2: 'http://www.thepeoplestrust.org'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Microfinancing (AFI & Eurobank)</Text>
                <Text>AFI (Eylem Finans Girişimi) Sivil Kar Amacı Gütmeyen bir Şirkettir. 2014 yılında Yunanistan'da
                    ActionAid Hellas ve Avrupa'da mikrokredide lider olan Fransız kuruluşu ADIE tarafından
                    kurulmuştur.</Text>
                <Text>Eurobank, uzun vadeli işsiz, savunmasız vatandaşlar kategorisine mensuplar, banka kredisine
                    erişimi olmayan mikro-girişimcilere (15.000 Avro'ya kadar) mikro kredi ile yardım etmek için AFI ile
                    işbirliği yapmaktadır. Onlara kendi işlerini kurma (kendi işini kurma) veya küçük işletme birimleri
                    geliştirme ve yeni işler yaratma fırsatı sunar.</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'AFI, adayların ön seçimi, eğitimi ve mentorluğunu üstlenir. Eurobank, kredi kontrolü ve finansmanı üstlenmektedir.',
                                val2: 'https://www.eurobank.gr/el/business/proionta-kai-upiresies/proionta-upiresies/xrimatodotiseis/anaptuksiaka/easy-afi'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
