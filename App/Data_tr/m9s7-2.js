import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img14.png')
    }
}];

export default class M9S7_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme Twitter hesabı oluşturma</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 2:</Text> “Profili
                    düzenle” butonuna tıklanır ve Twitter sayfasındaki gerekli kısımlar aşağıdaki şekilde
                    doldurulur:</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img14.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <FlatList
                    data={[
                        {key: '1. Kullanıcı adı	:(işletme adını yansıtması daha iyi olacaktır)'},
                        {key: '2. Profil resmi (tavsiye edilen ebatlar 400x400 piksel şeklindedir)'},
                        {key: '3. İşletme ilgili açıklama (Kullanıcıların hesabınızı özel kılan şeyleri bilmelerini sağlamak üzere 160 karakter kullanabilirsiniz)'},
                        {key: '4. Baş resim olarak etkinlik fotoğrafları, ürün fotoğrafları, promosyon bilgisi/görüntüsü kullanılabilirinr ya da  yeni satış ve promosyonlara yönelik duyurular yer alabilir (tavsiye edilen ebatlar 1500x500 pikseldir)6'},
                        {key: '5. Sabitlenmiş Tweet: Her zaman en önemli veya en yeni haberleri her zaman en üste çıkarırsanız, tüm sayfanızda gezinmek zorunda kalmadan ziyaretçilerinizin yenilerini bulmasını kolaylaştırır.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Mümkün olduğunca fazla bilgi doldurduğunuzdan ve tüm sosyal medya için
                    kaliteli bir fotoğraf veya logo kullandığınızdan emin olun…</Text>
            </View>
        )
    }
}
