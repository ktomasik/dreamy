import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S45_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Twitter hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 15: Şimdi hesabınız tweet atmanız için hazır. f
                    simgesine tıklayarak ilk tweetinizi yazabilirsiniz. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img92.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img93.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
