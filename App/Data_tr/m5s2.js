import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Banka Hesabı Nedir?</Text>
                <Text style={{marginTop: 10}}>Banka hesabı1, tüm paranın yatırılması için seçilen güvenli ve yararlı bir
                    yerdir. Yatırılan paraya herhangi bir ATM’den ulaşılabilir. Ayrıca gelecek için paranın tasarrufu ve
                    yatırımını da kolaylaştırır.
                    {"\n\n"}Banka Hesabı Türleri
                    {"\n"}Çoğu banka ve kredi birliği aşağıdaki hesap türlerini sunar:
                </Text>
                <FlatList
                    data={[
                        {key: '1. Tasarruf Hesapları'},
                        {key: '2. Vadesiz Mevduat Hesapları'},
                        {key: '3. Para Piyasası Hesapları'},
                        {key: '4. Mevduat Sertifikaları (CDs)'},
                        {key: '5. Emeklilik Hesapları'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>Vadesiz Mevduat Hesabı Nedir? Mobile ve İnternet Bankacılığı Nedir?</Text>
                <Text style={{marginTop: 10}}>Vadesiz mevduat hesabı3 günlük işlem ihtiyaçları için paraya kolay erişim
                    sağlar ve nakit güvenliğini korumaya yardımcı olur. Müşteriler, banka kartı ya da çek kullanarak
                    satın alma veya fatura ödemesi yapabilirler. Belirli aylık hizmet ücretlerini ödememek için hesaplar
                    farklı seçenek ve paketlere sahip olabilir. En ekonomik seçeneğe karar verebilmek için, gerçekten
                    ihtiyaç duyulan hizmetleri içeren farklı mevduat paketlerinin faydaları karşılaştırılmalıdır.
                    {"\n\n"}Mobil bankacılık, masaüstü bilgisayar yerine akıllı telefon ve tablet kullanılarak internet
                    bankacılığıyla aynı etkinliklerin çoğunu gerçekleştirmeyi sağlar. Mobil Bankacılığın çok yönlü
                    kapsamı:
                </Text>
                <FlatList
                    data={[
                        {key: 'Bir bankanın mobil web sitesine giriş'},
                        {key: 'Mobil bankacılık uygulamasını kullanma'},
                        {key: 'Kısa mesaj (SMS) bankacılığı'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>İnternet bankacılığı, genellikle masaüstü veya diziüstü bilgisayarla bir
                    bankanın web sitesinde kişiye özel profil altında internet üzerinden gerçekleştirilebilen herhangi
                    bankacılık işlemini ifade eder. İnternet bankacılığı genellikle aşağıdaki özellikleriyle
                    tanımlanır:</Text>
                <FlatList
                    data={[
                        {key: 'Bankanın güvenli web sitesi aracılığıyla yapılan parasal işlemler '},
                        {key: 'Fiziki banka şube konumları ya da sadece internet '},
                        {key: 'Kullanıcı, bir giriş kimliği ve şifre oluşturmalıdır'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
