import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Vergi Temeli</Text>
                <Text>Vergi yükümlülüğü vergi mercilerine kişinin vermesi gereken para miktarıdır (Cameron 2017). Devlet
                    vergi ödemelerini sosyal programlar ve yönetim faaliyetlerinde kullanır. Temel olarak, vergi
                    yükümlülüğü genellikle gelirin belli bir yüzdesidir ve gelire göre değişir.</Text>
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>TÜRKİYE’deki uygulamalar</Text>
                <FlatList
                    data={[
                        {key: 'Adres bildirimi gereklidir. Ev adresi fatura adresi olarak kullanılabilir.'},
                        {key: 'Başvuru sırasında, basit vergilendirme beyanı yapılmalıdır'},
                        {key: 'Vergi dairesinin değerlendirmesinden sonra, daire mükellefe vergi kartını yönlendirir.'},
                        {key: 'Kayıt Esnaf ve Sanatkarlar Odasına yapılmalıdır.'},
                        {key: 'Başvuru çalışma izni için diklekçeyle belediyeye yapılır.'},
                        {key: 'Basit vergilendirmeye tabii tutulan mükellefler belgelerini oda veya birliklerden alır.'},
                        {key: 'Vergi mükellefleri gelirlerini yıllık olarak bildirmekle yükümlüdür.'},
                        {key: 'Bildirim kayıtlı olunan vergi dairesine yapılır. Vergi mükellefi fatura bazında vergi öder (Türkiye Ulusal Raporu, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Slovenya’daki uygulamalar</Text>
                <FlatList
                    data={[
                        {key: 'Katma Değer Vergisi (VAT)'},
                        {key: 'Kurumlar Vergisi'},
                        {key: 'Gelir Vergisi'},
                        {key: 'Sosyal Sigorta Pirimi'},
                        {key: 'Taşınmaz Mal Transfer Vergisi'},
                        {key: 'Sermaye Kazanç Vergisi'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>İşletme şekline göre:</Text>
                <FlatList
                    data={[
                        {key: 'Kişisel girişimciler, vergilendirmeler, girişimcilik amaçlı tarımsal, ormancılık, mesleki veya diğer bağımsız serbest meslek faaliyetlerinin performansından elde edilen bir faaliyetten elde edilen gelirdir.'},
                        {key: 'Limited şirket, tüzel kişiliktir ve Kurumlar Vergisi ile vergilendirilir'},
                        {key: 'KDV amacıyla belirlenmiş olan vergilendirilebilir kişi E-Satış vergileri, Slovenya’da müşteriye ürünün tesliminden KDV’yi hesaplamak zorundadır.(Slovenya Ulusal Raporu, 2018)'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Polonya’daki uygulamalar</Text>
                <Text>Kendi işini yürütmek isteyen bireyler için vergi yükümlülükleri, tam zamanlı çalışanlarda olduğu gibi, gelir vergisi temelinde ödenmektedir. Bireyler aşağıdakilerden birini seçmelidir:</Text>
                <FlatList
                    data={[
                        {key: 'Genel kurallar vergisi'},
                        {key: '%19 vergi (sabit vergi),'},
                        {key: 'Kayıtlı gelirin toptan ödemesi,'},
                        {key: 'Vergi kartı (Polonya Ulusal Raporu, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Yunanistan’daki uygulamalar</Text>
                <FlatList
                    data={[
                        {key: 'Bireyler ve işletmeler, Vergilendirilecekleri Bağımsız Gelir İdaresi\'nin online sistemi aracılığıyla elektronik vergi beyanı sunmalıdır.'},
                        {key: 'Her ayın sonunda, fatura listesi ve katma değer vergisi girişi yapılır.'},
                        {key: 'Kendi işini yürütmek isteyen bireyler için vergi yükümlülükleri, gelir vergisi temelinde ödenmektedir.'},
                        {key: 'Tam zamanlı çalışanlar için olduğu gibi vergi indrimi olmadan gerçekleşir.'},
                        {key: 'Kazanca bakılmaksızın Tüm Yunan şirketler %29 oranında vergiye tabii tutulur.'},
                        {key: 'Hisseler %15 oranında vergilendirilir (Yunanistan Ulusal Raporu, 2018).'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>Fransa’daki uygulamalar</Text>
                <Text>Kazancın vergilendirilmesi işletmenin yasal yapısına bağlıdır. İşletmeler Gelir Vergisi  veya Kurumlar Vergisine (CI) tabii tutulmaktadır.</Text>
                <Text>Şirketler şunlara tabidir:</Text>
                <FlatList
                    data={[
                        {key: 'o\tKazancın vergilendirilmesi'},
                        {key: 'o\tBölgesel ekonomik katkı (CET),'},
                        {key: 'o\tKatma Değer Vergisi (VAT),'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text>Şahıs şirketleri (el işleri yapanlar, esnaflar), serbest meslek sahipleri ve EURL (tek kişilik bir limited şirket) IR ‘yi ödemek zorundadır.
                    {"\n"}Ortaklar, yalnızca maaşlar veya temettüler üzerinden gelir vergisi açısından kişisel olarak vergilendirilir (Fransa Ulusal Raporu, 2018).
                </Text>
            </View>
        )
    }
}
