import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Popüler Sosyal Medya Web Siteleri</Text>
                <Text>Amerikalıların % 81'inin sosyal medyayı 2017'den beri kullandığı ve kullanıcı sayısının giderek
                    arttığı tahmin edilmektedir. Bir tahmine göre bir birey çevrimiçi zamanının beşte birinden fazlasını
                    sosyal medyada harcamaktadır. 2005’te sosyal medyayı kullanan yetişkin yüzdesi 5’ti. Tüm dünyada
                    yaklaşık 1.96 milyar sosya medya kullanıcısı bulunmaktadır. 2018 yılı sonunda bu rakamın 2.5 milyara
                    yükselmesi beklenmektedir. Diğer tahminler daha da yüksektir (Silver, n.d.). Pew Araştırma
                    Merkezi'ne göre (2018), sosyal medya kullanıcıları daha genç olma eğilimindedir (18-29 yaşları
                    arasındaki kişilerin % 90'ı en az bir sosyal medya türü kullanmaktadır), daha iyi eğitime sahip ve
                    nispeten zengindir (yılda 75.000 dolardan fazla kazanmaktadır). Birleşik Devletler ve Çin sosyal
                    medya kullanımında liderdir (‘Leading Global Social Networks 2018 | Statistic’ 2018): Facebook (Ocak
                    2018 itibariyle 2.167 milyar kullanıcı ), YouTube (1.5 milyar), WhatsApp (1.3 milyar), Facebook
                    Messenger (1.3 milyar), WeChat (980 milyon), QQ (843 milyon), Instagram (800 milyon), Tumblr (794
                    milyon), QZone (568 milyon), Sina Weibo (376 milyon), Twitter (330 milyon), Baidu Tieba (300
                    milyon), Skype (300 milyon), LinkedIn (260 milyon), Viber (260 milyon), Snapchat (255 milyon),
                    Reddit (250 milyon), LINE (203 milyon), Pinterest (200 milyon), YY (117 milyon).
                    {"\n\n"}Sosyal medyaya hizmet eden farklı web siteleri ve uygulamalar, farklı iletişim türlerine ve
                    farklı içeriklere adanmıştır. En popüler sosyal ortamlar için sonraki bölümlerde kısa bir açıklama
                    bulunmaktadır (Rouse 2016).
                </Text>
                <Text style={styles.dmlH3}>Facebook</Text>
                <Text>Facebook popüler ücretsiz sosyal ağ iletişimi (network) web sitesidir. Kayıtlı kullanıcılara
                    profil oluşturma, fotoğraf ve video yükleme, mesaj gönderme, aile, arkadaş ve iş arkadaşları ile
                    iletişim halinde olma imkanı tanır. Nielsen grup istatistiklerine göre, Birleşik Devletler’deki
                    internet kullanıcıları Facebook’ta diğer web sitelerinde geçirdikleri zamandan daha çok vakit
                    geçirmektedir.</Text>
                <Text style={styles.dmlH3}>Twitter</Text>
                <Text>Twitter kayıtlı üyelerine tweet adı verilen kısa paylaşımlar yayınlamalarına olanak tanıtan
                    ücretsiz mikro blog hizmetidir. Twitter üyeleri tweetler yayınlayabilir ve çeşitli platformlar ve
                    cihazlar kullanarak diğer kullanıcıların tweetlerini takip edebilir.</Text>
                <Text style={styles.dmlH3}>Google+</Text>
                <Text>Google +, Google’ın diğer sosyal ağ hizmetlerinde olduğu gibi insanların çevrimdışı olarak
                    etkileşim kurma şeklini çeşitlendirmek için tasarladığı sosyal ağ projesidir. “Gerçek yaşam
                    paylaşımı web için yeniden düşünüldü” projenin sloganıdır.</Text>
                <Text style={styles.dmlH3}>Wikipedia</Text>
                <Text>Wikipedi, Wikipediyenler olarak bilinen bir kullanıcı topluluğunun işbirliği çabasıyla oluşturulan
                    ücretsiz, açık içerikli bir çevrimiçi ansiklopedidir. Sitede kayıtlı herkes yayın için bir makale
                    oluşturabilir; makaleleri düzenlemek için kayıt olmak gerekmez. Wikipedia Ocak 2001'de
                    kurulmuştur.</Text>
                <Text style={styles.dmlH3}>LinkedIn</Text>
                <Text>Özellikle iş dünyası için tasarlanmış bir sosyal paylaşım sitesidir. Sitenin amacı, kayıtlı
                    üyelerin profesyonel olarak tanıdıkları ve güvendikleri kişi ve belge ağlarını kurmalarını
                    sağlamaktır.</Text>
                <Text style={styles.dmlH3}>Reddit</Text>
                <Text>Reddit, hikayelerin site üyeleri tarafından sosyal olarak düzenlendiği ve tanıtıldığı bir sosyal
                    haber sitesi ve forumudur. Site yüzlerce “subreddit” adı verilen alt topluluktan oluşur. Her
                    subreddit teknoloji, politika ya da müzik gibi spesifik konuya sahiptir. “Redditors” olarak da
                    bilinen Reddit site üyeleri, içerik sunarlar ve daha sonra bu içerik diğer üyeler tarafından
                    oylanır. Amaç, saygın hikayeleri sitenin ana sayfasının en üstüne çıkarmaktır.</Text>
                <Text style={styles.dmlH3}>Pinterest</Text>
                <Text>Pinterest, çevrimiçi bulunan görüntüleri paylaşmak ve kategorilere ayırmak için kullanılan bir
                    sosyal paylaşım sitesidir. Pinterest kısa açıklamalar gerektirir ancak sitenin ana odağı görseldir.
                    Bir görüntünün üzerine tıklamak sizi orijinal kaynağa götürecektir, yani, örneğin, bir çift
                    ayakkabının resmini tıklarsanız, onları satın alabileceğiniz bir siteye yönlendirilebilirsiniz.
                    Yaban mersinli krep resmi sizi tarife, garip bir kuş evi resmi size talimatlara götürebilir.
                </Text>
            </View>
        )
    }
}
