import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Marka nedir?</Text>
                <Text>Marka bir isim, logo, işaret veya şekil ve/veya bunların kombinasyonlarıdır. Marka müşterilerin
                    ürün veya servisin diğer ürün veya servislerden ayrılmasına fayda sağlamaktadır (Shim 2009).</Text>
                <Text style={styles.dmlH2}>Marka adı nedir?</Text>
                <Text>Marka adı kombinasyonlar halinde sözlü olarak ifade edilen kelime veya sayılardır (Shim 2009).
                    {"\n\n"}Örneğin; 3M, Google, XEROX, gibi.
                </Text>
                <Text style={styles.dmlH2}>Marka adı & ticari marka arasındaki fark nedir?</Text>
                <Text>Şirket/ iş/ işletme adı bir işi, bir oluşumu veya bir bireyi tanımlamaya yarayan isim veya yoldur.
                    Birey veya kuruluşun iş yapmak için seçtiğiğı resmi isimdir.(Cameron 2017).
                    {"\n\n"}Ticari marka isim, ifade, logo, sembol, tasarım, renk veya bunların bir /birkaçının
                    kombinasyonu olup bir firmanın ürün/hizmetini diğerinden ayırt etmek için kullanılmaktadır (Cameron
                    2017, Shravani 2017).
                    {"\n\n"}Marka adı ve ticari marka arasındaki farklar şu şekildedir (Anonymous 2018a, Shravani 2017):
                </Text>
                <FlatList
                    data={[
                        {key: 'Marka adları ve ticari markalar işletmenin değerli varlıklarıdır. Marka veya ticari marka ürünle özdeşleşmektedir. “Marka” ve “ticari marka”yı karıştırmamak gerekir, çünkü aralarında büyük farklılıklar vardır.'},
                        {key: 'Marka halk tarafından itibarı ve işi temsil ederken, ticari marka , markanın işletmeye özgü ve özel yönlerini yasal olarak korur.'},
                        {key: 'Ticari marka, Marka ve Patent Ofisi tarafından verilen markanın yasal korunmasıdır'},
                        {key: 'Tüm ticari markalar marka kapsamındadır, ancak tüm markalar ticari marka değildir.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}><Text style={{fontWeight: 'bold'}}>Örneğin:</Text>
                    {"\n"}Coco Chanel ticari marka için güzel bir örnektir. Ünlü tasarımcı Coco Chanel kendi adını
                    kullanarak başarılı moda işi yapmaktadır. İnsananlar Coco Chanel ürünü aldıklarında kaliteli
                    olduğunu bilmekteler. Bu bilinirlik dünya çağında geçerli olmaktadır. Coco Chanel ismi ticari marka
                    olurken, soy ismi olan Chanel marka olarak kullanılmaktadır (Husbey 2016).</Text>
            </View>
        )
    }
}
