import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements'
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz4": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Logo",
                    "option2": "b) Marka",
                    "option3": "c) Patent",
                    "option4": "d) Vergi numarası"
                },
                "question": "_______ tüketicinin ürünü veya hizmeti pazardaki diğerlerinden ayırt etmesini sağlayan, tekil olarak veya kombinasyon halinde bir isim, logo, işaret veya şekildir"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a)Doğru",
                    "option2": "b)Yanlış"
                },
                "question": "Marka Adı ve Ticari isim aynı olmak zorundadır."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Yerel belediyeler",
                    "option2": "b) Vergi Dairesi",
                    "option3": "c) Apartman sakinleri",
                    "option4": "d) Oda"
                },
                "question": "Evden çalışmak için yerel lisans ve izinlerin alınmasında aşağıdakilerden hangisi ilk basamaktır?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Yasal ad devlet prosedürleri içindir, ticari ad halkla ilişkiler içindir."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Ticari marka, Marka ve Patent Ofisi tarafından verilen markanın yasal korumasıdır."
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vergi Dairesine başvurmak",
                    "option2": "b) Apatman sakinlerine sormak",
                    "option3": "c) Odaya gitmek",
                    "option4": "d) Tedarik faturası"
                },
                "question": "Vergi mükellefi olmak için atılması gereken ilk adım hangisidir?"
            },
            "question7": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Marka adı",
                    "option2": "b) Fatura",
                    "option3": "c) Oda üyeliği",
                    "option4": "d) Vergi kimlik numarası"
                },
                "question": "E-beyan sistemine başvurmak için hangisi gereklidir?"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "İşletme adı devlet başvuru ve formlarında kullanılamaz."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Kullanıcı Sözleşmesi",
                    "option2": "b) Gizlilik Politikası",
                    "option3": "c) Apartman sakinlerinin izni",
                    "option4": "d) Patentli Logo"
                },
                "question": "Web sitesinde e-ticaret için hangisi gerekli değildir?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Ticari marka tescili olmadan, rakiplerin veya taklitçilerin aynı markayı kullanmasını engellemenin yolu yoktur."
            }
        }
    }
};

export default class M4Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz4;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
