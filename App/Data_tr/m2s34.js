import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S34 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Instagram'dan diğer sosyal ağlara paylaş</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 4: Açıklama yazın bölümüne tıklayın ve fotoğrafınızın
                    hakkında kısa bir tanım yazın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img65.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
