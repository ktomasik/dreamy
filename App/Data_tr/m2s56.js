import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S56 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sosyal ağ iletişim sitelerini kullanma</Text>
                <Text style={styles.dmlH2}>Blog Kurma</Text>
                <Text style={{marginTop: 10}}>Blog oluşturmaya başlamadan önce, sitenizi yayınlayacağınız bir platform
                    seçmeniz gerekir. WordPress ve Blogger gibi çok popüler olan ücretsiz seçenekler mevcuttur. Bu
                    platformların her ikisinde de kullanıcıların hareket halindeyken yayınlarını oluşturmalarına,
                    düzenlemelerine ve yayınlamalarına izin verecek uygulamalar vardır.</Text>
                <Text style={{marginTop: 10}}>Blogger ve WordPress arasındaki temel fark, Blogger’ın yapılandırılması ve
                    kullanılması biraz daha basit olsa da, WordPress’in özelleştirilmesi ve ücretsiz tekliflerinin
                    limiti aşıldığında özel bir siteye geçiş yapmak daha kolaydır. Hangisini seçerseniz seçin, ikisinde
                    de ana mobil platformlar için resmi uygulamalar mevcuttur. (Bozzo 2014)</Text>
                <Text style={{marginTop: 10}}>Bu öğretici bölümde basitliği nedeniyle Blogger üzerinden gideceğiz.</Text>
            </View>
        )
    }
}
