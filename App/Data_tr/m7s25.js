import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadınlar için Mikro Finans ve Diğer Girişimci Fonları</Text>
                <Text style={styles.dmlH2}>Fransa:</Text>
                <Text>Genellikle bir iş kurmak veya devralmak isteyen ancak kaynakları bilinen krediye hak kazanmak için
                    yeterli olmayan kişilere yönelik olan 25.000 Avro'dan daha düşük kredi.Bir mikro krediden
                    yararlanmak için, borçluya “France Active”, “France Initiative”, “Boutiques de Gestion” veya “2e
                    Chance Fondation” gibi özel ve yetkili destek ağı eşlik etmelidir. Bu ağlar, projenin ayarlanmasına,
                    finansman talebinin araştırılmasına ve faaliyetlerin geliştirilmesine yardımcı olacaktır. Baş aktör
                    ADIE'dir (ekonomik girişimin geliştirilmesi Derneği).</Text>

                <Text style={styles.dmlH3}>ADIE</Text>
                <Text>2015 yılından bu yana kadınlar için yıllık farkındalık kampanyası organizasyonuyla Kadınları iş
                    yaratma konusunda duyarlılaştırma, yönlendirme ve bilgilendirme</Text>
                <Text>Banka kredisine erişimi olmayan işletmeler için mikro kredi ile birlikte proje taşıyıcı finansmanını teşvik eder.</Text>
                <Text>Özelliğine göre uyarlanmış eğitim ve farkındalık modülleri ile iş yaratıcılarının desteğini güçlendirir.</Text>
                <FlatList
                    data={[
                        {key: 'https://www.adie.org/nos-actions/pour-les-femmes'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}}
                                                  onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />

                <Text style={styles.dmlH3}>FRANCE ACTIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'France Active, yaklaşık 30 yıl boyunca şirketleri destekledi ve finanse etti ve geçen yıl 7.400 şirkete hizmet vermek için 270 milyon avro harekete geçirmiştir. France Active, bir ağdan çok öte, tutkusu daha kapsamlı bir toplum inşa etmek isteyen kendini bu işe adamış girişimcilerin  gerçek hareketidir. France Active\'in misyonu, onlara müdahil olacakları araçlar sağlayarak girişimcilerin başarısını hızlandırmaktır.',
                                val2: 'https://www.franceactive.org/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />


                <Text style={styles.dmlH3}>FRANCE INITIATIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Fransa İnitiative Ağı, ardından Fransa İnitiative olarak adlandırıldıktan sonra, ağ marka sistemini yeniden tasarladı. 1 Ekim 2012 tarihinden itibaren ulusal kurum İnitiative France olarak adlandırıldı. Yerel platformlar ve bölgesel koordinasyon aynı değişikliği yapıyor. Bu sadece kelimelerin tersine çevrilmesinden daha fazlasıdır. Bu marka artık herkes için ortak olan terimi vurgulamakta: Girişim, bölgenin adını gösterirken. Ulusal ağın gücünü ve yerel kökleriyle bağlantılı çeşitliliğini grafiksel olarak gösteren bir logo eşlik eder. Sonunda, ortak eyleme tam anlam kazandıran bir imza taşır: “Bir ağ, bir ruh”',
                                val2: 'http://www.initiative-france.fr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>BGE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: '35 yıldan fazla bir süredir BGE, iş yaratmayı desteklemekte ve bunu herkes için erişilebilir hale getirmek için çalışmaktadır. Kuruluşun her aşamasında girişimcilere eşlik ederek, ortaya çıkışından iş gelişimine kadar, başarı şansını yakalayan herkese verilir. Kâr amacı gütmeyen birlik ağı olarak BGE, potansiyel müşterileri açmak, girişimcilerin yolunu güvence altına almak ve istihdam ve yerel kalkınma için kalıcı çözümler oluşturmak için bölgelerde kurulan 50 dernekten oluşur.',
                                val2: 'http://www.bge.asso.fr/nous-sommes/notre-engagement.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>2nd CHANCE FOUNDATION</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: '2. Şans Vakfının amacı, 18-62 yaşları arasındaki, zorlu yaşam koşullarını tecrübe eden ve şu anda çok güvencesiz bir durumda olan, ancak durumunu iyileştirme arzusunda olan insanları desteklemektir. 2. Şans Vakfı, gerçekçi ve sürdürülebilir bir profesyonel proje yürütmeleri için insani ve finansal destek sunar: nitelikli bir eğitim, bir şirketin kurulması veya devralınması gibi.',
                                val2: 'http://www.deuxiemechance.org/fr'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
