import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Vergi Mükellefiyeti</Text>
                <Text style={{marginTop: 10}}>Vergi Mükellefiyeti  Için aşağıdaki adımları takip edin:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Yaşadığınız yerdeki vergi dairesine başvurun</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Vergi muafiyeti belgenizi alın
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
