import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz6": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Sadece Google play",
                    "option2": "b) Sadece Apple store",
                    "option3": "c) Hiçbiri",
                    "option4": "d) Her ikisi"
                },
                "question": "Apple cep telefonu için fatura şablonu nereden indirilebilir?"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Android işletim sisteminde fatura oluşturmak için Apple Store kullanılabilir?"
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Fresh books",
                    "option2": "b) Moon Invoice",
                    "option3": "c) Wave",
                    "option4": "d) Pinterest"
                },
                "question": "Aşağıdaki uygulamalardan hangisi hem apple and android ‘de fatura oluşturmaya izin verir?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Faturada logonun bulunması kesinlikle zorunlu değildir, ancak kullanılması iyi olur."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) İsim ve ünvan",
                    "option2": "b) İletişim bilgi,adres, telefon vb. ",
                    "option3": "c) Malzemenin vergisi",
                    "option4": "d) Hepsi"
                },
                "question": "Fatura üzerinde hangi bilgiler yer almalıdır?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Tüccarın kimliği",
                    "option2": "b) Sosyal medya hesabı",
                    "option3": "c) KDV detayları",
                    "options": "d) İletişim detayları"
                },
                "question": "Tipik bir internet alışveriş sözleşmesi için aşağıdaki unsurlardan hangisi gerekli değildir?"
            },
            "question7": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "İşletme sahibi, kendi işletmesinin adı altında satılan ürünlerden sorumlu tutulamaz."
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Doğru",
                    "option2": "b) Yanlış"
                },
                "question": "Ticari malzemeler için sigorta yaptırmak faydalı değildir."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Dökümanların sınıflandırıldığı yer",
                    "option2": "b) Cep telefonlarında elektronik imza kullanmak için ücretsiz ve uygun platformdur",
                    "option3": "c) Her satışta imzalanacak belge",
                    "option4": "d) Hiçbiri"
                },
                "question": "DocuSign nedir?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Cash board",
                    "option2": "b) DocuSign",
                    "option3": "c) DropBox",
                    "option4": "d) Pinterest"
                },
                "question": "_______ Microsoft Word'de açabileceğiniz indirilebilir bir şablon sunar."
            }
        }
    }
};

export default class M6Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz6;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>

                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
