import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img15.png')
    }
}];

export default class M8S9_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Kargo gönderisi</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>ADIM 8: GÖNDERİN</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img15.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Kargo şirketleriyle çalışmak üzere tavsiyeler:</Text>
                <FlatList
                    data={[
                        {key: 'Nakliye öncesinde müşteriye kargo tercihi (kargo firması, şubesi vb.) sormak faydalı olmaktadır. Müşterinin bu konuda özel bir talebi olabilir.'},
                        {key: 'Paket büyüklüğü ürüne göre seçilmelidir. Uygun paket büyüklüğü nakliye bedelini etkilemektedir.'},
                        {key: 'Kargo şirketleriyle sözleşme imzalamak nakliye tutarlarını düşürmeye yardımcı olabilmektedir.'},
                        {key: 'Kargo şirketleriyle sözleşme imzalamak ürünün adresten ücretsiz teslim gibi avantajlar sağlamaktadır.'},
                        {key: 'Nakliye işlemi öncesi, teslim şubesi ve teslim süresini öğrenmek faydalı olmaktadır.'},
                        {key: 'Takip numarasını öğrenmek ve müşteriyle paylaşmak teslim süresinin doğruluğu açısından oldukça önemlidir.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

            </View>
        )
    }
}
