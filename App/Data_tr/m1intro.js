import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1INTRO extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Giriş</Text>
                <Text>Dreamy m- Learning Projesi’nin hedef grubu evlerinden el işi üreten düşük eğitim seviyesine sahip
                    kadınlardır. Amaç, hedef grubun mobil-dijital ve girişimcilik yeteneklerinin gelişimlerine yardım
                    ederek, ürettikleri el işlerini akıllı telefonları etkili bir şekilde kullanarak dijital pazarlarda
                    satmalarına yardımcı olmaktır. Android ve İOS işletim sistemleri için anlaması ve kullanması kolay
                    bir mobil uygulama, m-öğrenme portalı ve mobil uygulama arasında köprü oluşturan ağ tabanlı bir
                    arayüz geliştireceğiz. Bu amacı gerçekleştirmek için, hedef grubun ihtiyaçlarına yönelik adım adım
                    yaklaşımıyla modüller hazırlanmıştır. Modüllerdeki tüm uygulamalar için ücretsiz akıllı telefon
                    uygulamaları seçilmiştir.</Text>
                <Text style={{marginTop: 10}}>Bu çerçevede, bu modül, ilk aşamada kadınların e-posta hesabı oluşturması
                    ve bu hesabı akıllı telefonlarına (İOS ve Android) yüklemesini amaçlamaktadır. Ayrıca, bu modül aynı
                    özgeçmişe sahip ilgili konuları öğrenmek isteyen kişiler için de yararlı bir araç olacaktır.</Text>
            </View>
        )
    }
}
