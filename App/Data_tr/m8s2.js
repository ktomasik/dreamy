import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Nakliye metodları</Text>
                <Text style={{marginTop: 10}}>Bölgeye bağlı olarak nakliye ulusal ve uluslararası olmak üzere iki gruba
                    ayrılır. Yerel bazlı yapılan gönderiler aynı ülke içerisinde gerçekleştirilir ve nakliye için
                    gerekli dökümanlar ülke kuralları göz önünde tutularak gerçekleştirilir. Çoğunlukla kargo şirketleri
                    ürünü alır ve müşteriye teslim eder.
                    {"\n\n"}Sevkiyat aracı açısından hava, deniz ve kara yolu taşımacılığı mümkün olmaktadır. Büyük
                    çaplı işlerde veya uluslar arası sevkiyatlarda çoğunlukla hava ve deniz taşımacılığı tercih
                    edilebilmektedir. Sınır ülkelerindeki sevkiyatlarda kara yolu tercih edilmektedir. Küçük çaplı el
                    işlerinin sevkiyatlarında ise ülke içerisinde kargo şirketleri karayolu üzerinden işlem görmektedir.
                    Sevkiyat için fatura ve irsaliye gerekebilmektedir. Ülkeler arası sevkiyatlarda ise gümrük
                    beyannamesi gibi farklı dökümanlar da gerekebilir. Bu konuda nakliye şirketleri yönlendirme
                    yapabilmektedir.
                </Text>
                <Text style={styles.dmlH3}>Müşteriye ücretsiz kargo önerisi</Text>
                <Text>Ücretsiz gönderim, minimum sipariş miktarı veya minimum ürün sayısıyla tanımlanabilir. Ücretsiz
                    kargo müşterinin ilgisini çekmeye fayda sağlamaktadır. Satışlarda “Ücretsiz kargo” ibaresinin
                    geçmesi aynı alanda iş yapmakta olan rekabetçilere oranla ciddi avantaj sağlamaktadır (Anonymous
                    2018a).</Text>
                <Text style={styles.dmlH3}>İndirimli nakliye fiyatları</Text>
                <Text>Sürekli bir iş için bir kargo şirketi ile yapılan anlaşmalar, iş kapasitesi nedeniyle nakliye
                    ücretlerinin düşürülmesine yardımcı olabilir. Ayrıca, her sipariş için ayrı ayrı en iyi taşıyıcıyı
                    seçmek daha düşük fiyatlara neden olabilir. Her sipariş aldığınızda gönderim seçiminizi yeniden
                    değerlendirmek zaman ve emek yoğun gibi görünse de, farklı nakliye gemileri paketinizin ağırlığına,
                    boyutlarına ve hedefine bağlı olarak büyük ölçüde farklı fiyatlar sunabilir.
                    {"\n\n"}Farklı kargo şirketleriyle görüşmek rekabetçi fiyatlara neden olabilmektedir. Satış
                    hedefleri ve çalışma kapasitesine yönelik bilgiler kargo şirketlerinin daha özenli fiyat vermesine
                    neden olabilmektedir. Diğer yandan ürüne yönelik kargo fiyat hesaplamaları ağırlık ve ebata göre
                    yapıldığından kargo şirketlerinin tedarik edeceği ambalajları kullanmak daha indirimli rakamları
                    verebilmektedir.
                </Text>
            </View>
        )
    }
}
