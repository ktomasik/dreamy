import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


const screen2 = [{
    url: '',
    props: {
        source: require('../Images/m3/img20.png')
    }
}];

export default class M3S5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ürünlerin fotoğrafını çekmek ve bunları ağa yüklemek için aşağıdaki adımları
                    izleyin</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 5: Sayfada alta gidin ve “Etsy ‘de satışa başlayın” a
                    tıklayın</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen2}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img20.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
