import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28_6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Proje ortağı ülkelerde adım adım finansman</Text>
                <Text style={styles.dmlH2}>Türkiye:</Text>
                <Text style={styles.dmlH3}>Adım 6: </Text>
                <Text>Bu aşamada, kadın girişimci iş planını KOSGEB'e sunar (KOSGEB eğitimi sırasında iş planı eğitimine
                    hazırlık yapılır).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
