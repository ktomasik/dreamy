import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img1.png')
    }
}];

export default class M8S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Paketleme & Pazarlama</Text>
                <Text style={styles.dmlH3}>Kargo paket hesaplaması</Text>
                <Text>Nakliyenin sınıflandırılması söz konusu olduğunda, göz önünde bulundurulması gereken en önemli
                    şeylerden biri yoğunluktur. ve çoğunlukla bu terim kullanılmaktadır. Tipik olarak ürün ne kadar
                    yoğun olursa, sınıfı kadar düşük olur kuralı uygulanır. Düşük sınıflı yüksek yoğunluklu bir öğeye
                    sahip olunan nakliyede, daha yüksek sınıf düşük yoğunluğa sahip bir öğenin tersine gönderilmesi daha
                    düşük maliyetli olacaktır (Anonim 2018c).
                    {"\n\n"}Nakliye yoğunluğunun hesaplanması, sevkıyat için önerilen bir sınıfı da sağlayacaktır.
                </Text>

                <Text style={styles.dmlH3}>Paketleme teknikleri</Text>
                <Text style={{fontWeight: 'bold'}}>Basit paketleme yöntemi (tekli paketleme)</Text>
                <FlatList
                    data={[
                        {key: 'Kırılır özellikte olmayan yumuşak ürünler harici bir kutuya konulur.'},
                        {key: 'Nakliye sırasında ürünün ambalaj içerisinde hareketini önlemek üzere burultulmuş gazete kağıdı, dolgu malzemeleri veya havalı köpükler kullanılır.'},
                        {key: 'Toz, su veya nemli şartlardan korunması gereken ürünler plastik özellikli poşetlere konulur.'},
                        {key: 'Küçük parçalar veya dağılabilir granüler ürünlerin etrafı sağlamlaştırılarak iyi kapanmış paketlere konulması gerekmektedir. Sonrasında ise dayanıklı bir kutuya konulmalıdır.'},
                        {key: 'Paket H yöntemiyle sıkıca kapatılmalıdır (Anonymous 2018e).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{fontWeight: 'bold', marginTop: 5}}>Kutu içinde kutu yöntemi</Text>
                <FlatList
                    data={[
                        {key: 'Ürün veya ürünler ayrı ayrı 5cm (2”) kalınlığında hava kanallı veya köpük materyalle doldurarak paket içine destekli şekilde konulur.'},
                        {key: 'Nakliye sırasında ürünün ambalaj içerisinde hareketini önlemek üzere burultulmuş gazete kağıdı, dolgu malzemeleri veya havalı köpükler kullanılır.'},
                        {key: 'İç kutu H yöntemiyle sıkıca kapatılmalı ve bantlanmalıdır. Böylece taşıma sırasında kolaylıkla açılmaz'},
                        {key: 'En az 15 cm (6") uzun, geniş ve derin özellikli ikinci bir paket kullanılır.'},
                        {key: 'Sarma veya doldurma yöntemiyle kaplanan iç kutu başka bir kutunun içine yerleştirilir.'},
                        {key: 'Kırılabilir ürünler 8cm (3”) kalınlığındaki hava kanallı materyallerle kaplanarak ayrı yarı gönderin'},
                        {key: 'İç kutuyu 8 cm (3 ") kalınlıkta hava hücresel yastıklama malzemesi ile sarın veya iç kutu ile dış kutu arasındaki üst, alt ve her iki taraf boşlukları doldurmak için en az 8 cm (3") gevşek dolgu veya başka bir yastıklama malzemesi kullanın.'},
                        {key: 'Boşluklar uygun materyallerle doldurulur.'},
                        {key: 'Paket H yöntemiyle sıkıca kapatılmalıdır (Anonymous 2018e).'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
