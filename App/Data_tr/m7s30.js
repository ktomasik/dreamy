import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Proje ortağı ülkelerde adım adım finansman</Text>
                <Text style={styles.dmlH2}>Fransa:</Text>
                <Text style={styles.dmlH3}>Eğer iş aramıyorsanız DEVLET YARDIMI ACCRE</Text>

                <Text style={styles.dmlH3}>Adım 1:</Text>
                <Text>Başvuru, iş kurulurken ya da devam ettirildiğinde ya da 45 gün içinde yetkili Ticari İşlemler Merkezi (CFE) ‘nde kayda geçirilmelidir. Talep eklenmelidir:</Text>
                <Text>- CFE’de şirketin beyan formu veya kopyası,</Text>
                <Text>- Bu yardımdan 3 yıl boyunca faydalanmadığına izafeten geçerli olan yardım başvuru formunun   spesifik formu</Text>
                <Text>- ACCRE'den yararlananlar kategorileriden birine ait olduğunun kanıtı</Text>
                <Text>Farklı  belgeler CFE tarafından talep edilebilir, onlarla bağlantı kurulması tavsiye edilir.</Text>

            </View>
        )
    }
}
