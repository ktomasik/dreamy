import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Proje ortağı ülkelerde adım adım finansman</Text>
                <Text style={styles.dmlH2}>Fransa:</Text>
                <Text style={styles.dmlH3}>İş arıyorsanız</Text>
                <Text style={styles.dmlH3}>Adım 4:</Text>
                <Text>İş Planı yaz</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Başvuru olumlu bir puan alıncaya kadar bekle',
                                val2: 'https://www.petite-entreprise.net/P-226-88-G1-les-aides-et-subventions-pour-createur-d-entreprise.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
