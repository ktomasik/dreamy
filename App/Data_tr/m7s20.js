import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S20 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadın Girişimciler için Vergi Teşvikleri</Text>
                <Text style={styles.dmlH2}>Fransa:</Text>
                <Text>Kadın girişimciler çeşitli özel desteğe konu olurlar. Bu girişimciler , kurma veya kurtarma
                    projelerini daha iyi gerçekleştirmelerine yardımcı olmak için özel bir dikkatin hedefidir. Kadın
                    girişimciler bazen daha karışık kişisel durumlarla ya da daha büyük dış güvensizlikle yüz yüze
                    gelebilir. İş kurma projelerini ilerletmelerine yardımcı olmak üzere refakatçı veya özel yardımlar
                    mevcuttur.</Text>
                <Text>Destek konusunda birkaç yıldan beri özel ağlar mevcuttur.</Text>
                <Text>Les Premières ağı kadınların liderliğindeki iş yaratma projelerine tahsis edilmiş kuluçka ve iş
                    kuluçka merkezleri oluşturmuştur. Bu yaratıcı projelerin yenilikçi yanı olmalıdır. Kuluçka
                    merkezleri, iş yaratıcısının takibini sağlar, ancak aynı zamanda iş projesine 1 yıl boyunca ev
                    sahipliği yapar. Yaratıcı iş kadını bu nedenle işinin ilk yılına başlayacak şekilde
                    çevrelenmiştir.</Text>
                <Text>The Force Femmes ağı, kadınları kariyerlerinin ikinci yarısında yani 45 üzerinde destekler. Oluşum
                    veya şirket devralma projesine sahip olanlar, projelerinin hazırlanması sürecinde takip edilirler:
                    projenin onaylanması, oluşturulması, iş planının gerçekleştirilmesi, vb</Text>
                <Text>Actionelles diğerleri gibi iş kurma projelerini destekler. Dernek teklifleri, şirketin yaratılması
                    veya devralınması projesine sahip olanlara ek olarak yaratıcı kadınlar ve deneyimli iş sahipleri
                    arasındaki bir ilişki de dahil olan destekleyici projelerinin hazırlanması sürecinde izolasyonu
                    kırmak üzere takip edilmektedir.</Text>
                <Text>Bu 3 ağ ülke genelinde tam olarak mevcut değildir.</Text>
                <Text>Support access to financing for women entrepreneurs</Text>
                <Text>Finansmana erişim açısından, EGALITE Femmes Garantisi France Active tarafından kurulmuştur. Kadın
                    girişimciler tarafından banka kredisi alınmasını kolaylaştırmayı amaçlayan bir banka garantisidir.
                    Bu garanti, yaratma, kurtarma veya iş geliştirme projeleri için harekete geçirilebilinir.</Text>
                <Text>Bu yol diğer iş yaratma yardımcılarına özgü değildir. Ancak kadınların yaratıcılığı veya devralma
                    projeleri için destek sistemini tamamlar.</Text>

                <FlatList
                    data={[
                        {key: 'https://les-aides.fr/focus/a5Zi/les-aides-pour-les-femmes-creatrices-ou-repreneuses-d-entreprise.html'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}}
                                                  onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
