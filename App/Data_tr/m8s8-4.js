import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img11.png')
    }
}];

export default class M8S8_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Kargo gönderisi</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>ADIM 4: KARGO FİRMASINA ULAŞIN VE ÜRÜNÜ TESLİM EDİN</Text>
                <Text>Kargo firmalarına ürün teslim etme yollarından bazıları aşağıdaki gibidir:</Text>
                <FlatList
                    data={[
                        {key: 'Şubeler,'},
                        {key: 'Mobil şubeler,'},
                        {key: 'Internet web siteleri,'},
                        {key: 'Mobil uygulamalar,'},
                        {key: 'Kısa mesaj,'},
                        {key: 'Kendi teslim etme,'},
                        {key: 'Çağrı merkezleri.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img11.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
