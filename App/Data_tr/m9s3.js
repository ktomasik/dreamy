import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text styl={{marginTop: 10}}>Her gün artan kullanıcı sayısıyla sosyal medya pazarlama sürecinin bir
                    parçası haline gelmiştir Aşağıda, sosyal medya hesaplarınızı işletmenize göre ayarlamanızı sağlayan
                    adım adım bir kılavuz verilmiştir.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
