import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Proje ortağı ülkelerde adım adım finansman</Text>
                <Text style={styles.dmlH2}>Slovenya:</Text>
                <Text style={styles.dmlH3}>Adım 1:</Text>
                <Text>Slovenya İstihdam Bürosunda işsizlik kaydı yaptırın. Kişisel danışmanınıza hemen ilk toplantınızda
                    destek istediğinizi söyleyin. Kişisel danışman işe alım planı ve serbest meslek yardım programına
                    dahil edilme beyanı hazırlayacaktır.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
