import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Android işletim sistemini içeren mobil telefonlarda e-posta hesabı oluşturma (Samsung, Sony, HTC…) </Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Tebrikler, e-posta hesabınızı oluşturdunuz!</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Oluşturduğunuz e-posta hesabı ile tüm sosyal kanallara giriş yapabilirsiniz.</Text>
                <Text style={{marginTop:10}}>Bir sonraki modülde sosyal medya hesaplarınızı nasıl oluşturacağınız açıklanacaktır.</Text>
            </View>
        )
    }
}
