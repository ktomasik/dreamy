import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İOS işletim sistemini kullanan cihazlarda (iPhone, iPad ya da iPod) e-posta hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 1: ayarlara gidin</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img9.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
