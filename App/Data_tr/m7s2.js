import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Kredi Nedir?</Text>
                <Text>Bir banka ya da başka bir borç verenden aldığınız para kredidir. Kredi ödemeleri, ana para ve faiz
                    olmak üzere ikiye ayrılır.</Text>
                <Text>Ana para, borç alınan miktardır ve bu hesap bakiyesinin ana bölümüdür.</Text>
                <Text>Faiz, kredi alındığındaki bedelidir ve anapara üzerinden hesaplanır.</Text>
                <Text style={styles.dmlH2}>Kredi Bakiyesi Nedir?</Text>
                <Text>Kredi bakiyesi, kredi ödemesinde kalan tutar Her kredinin, tamamı ödenene kadar kredi bakiyesi
                    vardır. Günlük olarak değişir (faiz günlük olarak eklenir).</Text>
                <Text>Kredi İtfa Planı; Anapara ve faiz ayrılır, böylece aylık ödemenin hangi kısmının ana para ve hangi
                    kısmının faizi ödemek için kullanıldığı görülebilinir.</Text>
                <Text style={styles.dmlH2}>Hibe Nedir?</Text>
                <Text>Hibeler , bir alıcıya genellikle devlet dairesi, şirket, vakıf veya tröst tarafından ödünç veya
                    hediye olarak verilen geri ödemesiz fonlar veya ürünlerdir.</Text>
                <Text style={styles.dmlH2}>Vergi Teşviki Nedir?</Text>
                <Text>Vergi Teşviki, bireylerin ve işletmelerin ödemek zorunda oldukları vergi miktarının azaltılarak
                    onların para harcamalarını ya da tasarruf yapmalarını teşvik etmek için kullanılan bir devlet
                    önlemidir.</Text>
            </View>
        )
    }
}
