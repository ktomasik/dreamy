import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Marka adı kaydı ne demektir? Markamı (şirket adı) kaydettirmem gerekli
                    mi?</Text>
                <Text>YYeni bir markanın gelişimi ve lansmanı, büyük miktarda finansal, zihinsel ve duygusal sermayenin
                    yatırımını gerektirir ve bu nedenle marka tescili veya yeni markanın yasal koruması, herhangi bir
                    yeni iş girişimi için birinci öncelik olmalıdır.
                    {"\n"}Bu, yeni markanın yeni bir şirket, yeni ürün veya hizmet ya da yeni bir çevrimiçi işletme olup
                    olmadığı için geçerlidir. (Anonymous 2018e).
                    {"\n\n"}Marka kaydı ticarimarka kaydının diğer adıdır. Bu işlem, marka sahibinin yeni markayı
                    herhangi bir ulusal bölgede kullanma hakkına sahip olabileceği tek yol budur. Ne limited şirketler
                    ne alan isim kaydı yeni marka için oluşturmak yasal bir koruma sağlamaz (Forbes Agency Council
                    2017).
                    {"\n\n"}Ticari marka kaydı olmaksızın rakiplerin veya taklitçilerin aynı veya benzer markayı
                    kullanmasını engellemek mümkün değildir.
                </Text>
                <Text style={styles.dmlH2}>Şirket adı için ticari marka kaydı nasıl yapılır? </Text>
                <Text>Ticari markalı isim veya logo kaydı için her ülkenin kendine ait büro veya ofisleri vardır. Ayrıca
                    her ülke kendi kanun ve yönetmeliklerine göre farklı düzenlemeleri vardır. Ticari marka kaydı yapmak
                    için şirket ya da ilgili kişi kayıt ücreti ödemekle yükümlüdür.
                    {"\n\n"}Ticari marka kayıt süresi farklılık gösterebilir ancak genellikle on yıldır. Ek ücretler
                    ödenerek süresiz olarak yenilenebilinir. Ticari marka hakları özel haklardır ve koruması mahkeme
                    kanalınca yapılmaktadır.
                </Text>
            </View>
        )
    }
}
