import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme Twitter</Text>
                <Text styl={{marginTop: 10}}>En yaygın kullanılan sosyal medya hesaplarından ve sesinizi musterilere
                    direk duyurabileceğiniz platformlardan bir tanesi Twitter’dır. Twitter takipçileri potansiyel
                    müşteridir. Twitter'ın faydalarını bir pazarlama aracı olarak görmek için kendi takipçilerinizi
                    yaratmanız gerekir. Kendi Twitter listenizi oluşturabilir veya oluşturulmuş bir gruba dahil
                    olabilirsiniz.
                    {"\n\n"}İşletme Twitter hesabı açmak için ilk önce Twitter hesabı olmalıdır.. Modül 2’de anlatıldığı
                    üzere, google aramaya gidin; "twitter" yazın ve aratın, (mobile.twitter.com)I seçerek Twitter web
                        sitesine ulaşılır, Twitter’a “giriş yap” tıklanır (Detaylar için Modül 2’ye bakınız).
                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/twitter.png')}
                        resizeMode="contain"/>
                </View>
            </View>
    )
    }
    }
