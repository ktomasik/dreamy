import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S3_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme için Facebook sayfası oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 3:</Text> cProfil
                    ana sayfasındaki sol taraftaki çubukta yer alan “Ara bölümü”ndeki “sayfa”lar etiketine tıklayın.
                    Açılır menüden “Sayfa Oluştur” u tıklayın.</Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m9/img1.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
