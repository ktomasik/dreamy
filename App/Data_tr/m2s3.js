import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Yaygın Sosyal Medya Özellikleri</Text>
                <Text>Günümüzde sosyal medya sitesi puanları vardır. Farklı hizmetler sunmakta, farklı takipçileri
                    bulunmakta ve farklı kimliklerle eğlenme olanağı sağlamaktadır. Bununla birlikte bazı ortak
                    özellikleri paylaşmaktadırlar (Sunil 2017). Aşağıda sosya medya sitelerinin bazı önemli
                    özelliklerini bulabilirsiniz:</Text>
                <Text style={styles.dmlH3}>Ücretsiz web alanı sunmak:</Text>
                <Text>Bu sitelerin üyeleri web sunucusuna sahip olmak ya da web sunucusunu paylaşmak zorunda değildir.
                    İçeriklerini bu siteler tarafından sunulan ücretsiz alanlarda yayınlayabilirler.</Text>
                <Text style={styles.dmlH3}>Ücretsiz web adresi sunmak:</Text>
                <Text>Üyelere, bir bireyin veya bir işletmenin web kimliği olan benzersiz bir web adresi verilir.
                    İçeriği tanımlamak, bağlamak ve paylaşmak için kullanılabilir.</Text>
                <Text style={styles.dmlH3}>Üyelerde profil oluşturmaları istenir:</Text>
                <Text>Bu sitelerde üyelerin profil oluşturması istenir. Profillerde verilen bilgiler arkadaşlarla ve
                    kişilerle bağlanmak ve dünya genelinde benzer beğenileri olan insanlarla network kurmak için
                    kullanılır.</Text>
                <Text style={styles.dmlH3}>Üyeleri içerik yüklemeye teşvik etmek:</Text>
                <Text>Bu siteler üyelerin kısa mesaj, fotoğraf, ses ve video dosyası yüklemelerine olanak sağlar. Tüm
                    gönderiler sondan başa doğru sıralanır ve son gönderiler önce gelir. En önemlisi, tüm içerik gerçek
                    zamanlı olarak yayınlanır ve anında okunabilir, görüntülenebilir veya paylaşılabilir.</Text>
                <Text style={styles.dmlH3}>Üyelerin sohbetler oluşturmasına olanak tanır</Text>
                <Text>Üyeler içeriğe göz atabilir ve içerikle ilgili yorum yapabilir. Bunu yaparak, sosyal medya
                    siteleri, katılımı arttırıcı bir yol olarak üyelerin sohbetlere katılımını teşvik eder.</Text>
                <Text style={styles.dmlH3}>Canlı sohbetlere olanak tanır:</Text>
                <Text>Bazı sosyal medya siteleri kullanıcıların gerçek zamanlı olarak sohbet etmelerini sağlayan yapıya
                    sahiptir.</Text>
                <Text style={styles.dmlH3}>Doğrudan Mesajlaşma aracı:</Text>
                <Text>Çeşitli sosyal medya siteleri, üyelerine doğrudan mesajlaşma imkanı sağlar. Bu, üyelerin yalnızca
                    mesajın amaçlandığı kişiler tarafından okunabilen veya görüntülenebilen özel mesajlar göndermesini
                    sağlar.</Text>
                <Text style={styles.dmlH3}>Etiket uyarısı:</Text>
                <Text>Çoğu sosyal medya sitesi, bir mesajda veya fotoğrafta etiketlendiklerinde e-posta veya site
                    bildirimleri yoluyla üyelere uyarıda bulunur.</Text>
                <Text style={styles.dmlH3}>Üyelere benzersiz sayfalar oluşturma imkanı:</Text>
                <Text>Bazı sosyal medya sitelerinde üyeler tema tabanlı sayfalar oluşturabilir. Sayfalar daha sonra bir
                    temayla ilgili makale veya fotoğraf göndermek için kullanılabilir. Sayfalar ayrıca işletmeleri
                    tanıtmak için de kullanılabilir (kullanıcı hesapları; profil sayfaları; arkadaşlar, takipçiler,
                    gruplar vb.).</Text>
            </View>
        )
    }
}
