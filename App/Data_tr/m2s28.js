import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S28 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İnstagram Hesabı oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 15: Galeriden uygun bir fotoğraf seçeceğiz. Seçim
                    sırasında İnstagram uygulamasına fotoğraf galerimize erişim için izin vermeliyiz.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img52.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img53.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img54.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
