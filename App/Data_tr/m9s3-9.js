import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img6.png')
    }
}];

export default class M9S3_9 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İşletme için Sosyal medya hesaplarının oluşturulması</Text>
                <Text style={styles.dmlH3}>İşletme için Facebook sayfası oluşturma</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Adım 8:</Text> Facebook
                    Sayfasına “call-to-action” butonu ekleyin. Ekleyeceğiniz call to action butonu sayesinde e-posta,
                    telefon veya websitesi sayesinde müşterileriniz size kolayca ulaşabilecek ve alış veriş
                    yapabileceklerdir.
                    {"\n\n"}Sayfanıza “call to action” butonu eklemek için:
                </Text>
                <FlatList
                    data={[
                        {key: 'Sayfanızın kapak resmi altında, Buton ekleye (+Ekle) tıklayın'},
                        {key: 'Açılan menüden bir buton seçin ve ekrandaki talimatları takip edin. '},
                        {key: '“Tamamlandı”ya tıklayın.'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img6.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
