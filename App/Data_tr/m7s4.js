import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>İş Melekleri Kimdir ?</Text>
                <Text>İş Melekleri(melek veya melek yatırımcılar olarak da bilinir), şirket hissesi karşılığında
                    girişimcilere veya erken aşamadaki işletmelere sermaye sağlamak için kişisel varlıklarını kullanan
                    bireylerdir. Sermaye akışı, sürdürülebilir bir şirkete dönüşme fikrine yardımcı olabilir ve önerilen
                    ürün veya hizmeti üretmeye başlamak için temel sağlar.</Text>
                <Text>“İş meleği”, “gayri resmi yatırımcı” ve “gayri resmi girişim sermayesi” terimleriyle “İş meleği”
                    tanımı belirsizdir.</Text>
                <Text style={styles.dmlH2}>Ne tür imkanlar sunarlar ?</Text>
                <Text>İş melekleri şu imkanları sunarlar:
                    {"\n"}- Girişim sermayesi yoluyla elde edilemeyecek nispeten küçük miktarlar için nakit enjeksiyonu,
                    {"\n"}- Aynı şirket için sık sık daha sonraki finansmanları takip eder,
                    {"\n"}- Genellikle rehber ya da mentor olarak projeye dahil olmakla ilgilenir,
                    {"\n"}- Girişimciye yeni girişimlerinde hem rehberlik etmek için daha geniş ağlara bağlantı sağlar
                    hem de zamanını harcar
                </Text>
                <Text style={styles.dmlH2}>Kadınlar İş Meleği Fonu</Text>
                <Text>Kadınların iş meleği, genellikle, inovasyon karar verme sürecine kadın katılımını destekleyen, kar
                    amacı gütmeyen, politik olmayan bir kurumun parçasıdır. İş meleği yatırımları yalnızca maddi kaynak
                    sağlamakla kalmayıp bilgi, deneyim ve sosyal sermaye paylaşımını da kapsar. Ortak inanç, kadınların
                    çok sayıda ve yeni biçimlerde katılımının herkese ekonomik fayda getireceğidir.</Text>
            </View>
        )
    }
}
