import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S6 extends Component {
    render() {
        return (
            <View style={styles.section}>

                <Text style={styles.dmlH2}>Banka hesabı açmak</Text>
                <Text style={{marginTop: 15}}>Banka hesabı açmak için aşağıdaki yönelgeler takip edilmelidir
                    {"\n"}Seçenekleri gözden geçirin
                    {"\n"}İhtiyaçlar belirlendiğinde, seçenekler değerlendirilmelidir:
                </Text>
                <FlatList
                    data={[
                        {key: 'Vadesiz hesap'},
                        {key: 'Vadeli hesap'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5, fontWeight: 'bold'}}>{item.key}</Text>}
                />
                <Text style={{marginTop:10}}>Hangi banka hesabının uygun olduğu seçilmiş olabilir ancak hesabı açmaya uygun olup olunmadığından da emin olunmalıdır. Bankaya gitmeden önce, banka hesabı açma kriterlerinin hepsinin karşılanıp karşılanmadığının kontrol edilmesi gerekir.
                    {"\n\n"}Genel bir kural olarak, Bankalar aşağıdakileri talep eder:
                    {"\n\n"}Geçerli Kimlik. Bazı ülkelerde Sosyal Güvenlik numarası da istenilebilinir.
                    {"\n\n"}Banka hesabı açmak için asgari miktarda para. Bu seçilen banka ve hesaba göre değişebilir. Örneğin, orta düzeyde bir bankanın vadeli hesabı için asgari 300 Euro depozito istenir.
                    {"\n\n"}En uygun bankanın seçimi. Temel bir hesap açtırıldıysa tam olarak ne alınacağının görüşülmesi için bölgedeki banka şubesine başvurulmalıdır. Tüm bankalar farklı olsa da genel olarak iki genel kategoride toplanırlar: büyük zincir bankalar ve daha küçük yerel olanlar.
                    {"\n\n"}Büyük Zincir Bankalar: Büyük bankalar genellikle ülke çapında birçok kasaba ve şehirde şubelere sahiptir.  Diğer bankaların hizmetlerini (ATM bedeli, gibi) kullanmak  için ödemek zorunda kalınan bedellerden kaçınılabilinir. Büyük bankalar, müşterileri için 24 saat yardım hatları gibi hizmetler de sunmaktadır.  Bunlara ek olarak, bu bankalar istikrarlı ve güvenilir itibara sahip olma eğilimindedir.
                    {"\n\n"}Küçük Yerel Bankalar; Küçük bankalar daha kişisel, arkadaşça ve insan deneyimi sunar.  Küçük bankalar genellikle hizmetlerini kullananlardan daha az ücretler alırlar. Paralarını genellikle yerel topluluğa yatırım yaparlar. Diğer yandan, küçük bankalar büyük bankalara göre daha sık başarısız olurlar (gerçi , bu oldukça nadirdir).
                    {"\n\n"}Ayrıca, kredi kooperatifleri banka işlemleri için diğer bir seçenektir. Kredi kooperatifleri, kar amacı gütmeyen finans kurumlarıdır, çoğu zaman “toplum odaklı” ve “kar amacı gütmeyen insanlara hizmet etmek” misyonuyla hareket eder. Kredi kooperatifleri, ATM’ler ve ortak şube bankacılığı hizmeti sunmak için diğer kredi kooperatifleriyle ortaklık kurarak hizmetlerini daha erişilebilir hale getirmiştir.
                    {"\n\n"}Bankanın ziyaret edilmesi ve hesap açılmasının talep edilmesi. Bir hesabı kişisel olarak açmak, ilk kez hesap sahibi olacaklar için genellikle en iyi seçenektir. Banka memuruna tüm sorular ve endişeler sorulabilir ve anında cevap alınabilinir. Ayrıca, kişisel olarak banka hesabı açma süreci genellikle daha hızlıdır.
                    {"\n\n"}Hesap açma işlemini tamamlamadan önce tüm önemli sorular sorulmalıdır. Hesaba ilişkin tüm konularda açıklama istenebilinir.
                    {"\n\n"}Hesabı oluşturulması için gerekli bilgiler sağlanmalıdır. Vadesiz hesap açtırmak, birkaç temel kişisel bilgiyi gerektirir.
                    {"\n\n"}Kimliğin kanıtlanması: Fotoğraflı resmi bir kimlik belgesi (ehliyet ya da pasaportta yeterli olabilir)
                    {"\n\n"}Adres Belgesi: Telefon faturası, ehliyet ya da üzerinde ad ve adresin yazılı olduğu resmi bir belge
                    {"\n\n"}Vatandaşlık kayıt belgesi: Banka, başvuru sahibinin devlet kaydının olduğundan emin olmak için Sosyal Güvenlik Numarası, Vergi Kimlik Numarası, İşveren Kimlik Numarasını isteyecektir
                    {"\n\n"}Alınan Belgeler Güvenli Şekilde Saklanmalı. Hesap açma işlemi tamamlandıktan sonra, hesapla ilgili önemli bilgileri içeren belgeler verilir. Bu evraklar güvenli bir yerde saklanmalı. Mümkün olursa aşağıda yer alan bilgilerin bellekte saklanması mantıklı bir fikirdir böylece gelecekte bu belgelere bağımlı kalınmamış olunur:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Dört haneli PIN Numarası',
                                val2: 'Satın alımlarda banka kartı kullanmak için gereklidir.'
                            }
                        },
                        {
                            key: {
                                val1: 'Banka Hesap Numarası',
                                val2: 'Doğrudan para yatırmak gibi mali işler için gereklidir'
                            }
                        },
                        {
                            key: {
                                val1: 'Sosyal Güvenlik Numaras',
                                val2: 'Gelecekteki farklı vergi ve mali işler için gereklidir'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
