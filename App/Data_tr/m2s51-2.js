import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S51_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sosyal ağ iletişim sitelerini kullanma</Text>
                <Text style={styles.dmlH2}>Facebook sayfası oluşturma</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Adım 7: Ziyaretçilerinizi ana sayfanıza göndermek için ana
                    sayfanızın URL’sini yazabilirsiniz (ya da bu adımı atlayın) ve sonar İLERİ ye tıklayın.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img104.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
