import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S17 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Kadın Girişimciler için Vergi Teşvikleri</Text>
                <Text style={styles.dmlH2}>Slovenya:</Text>
                <Text>İlk girişten iki yıla kadar katkı paylarından kısmi muafiyet.- 2 yıl süresince, ilk defa girişimde
                    bulunanlar ve serbest meslek sahipleri sosyal sigortası olan( sadece emeklilik ve maluliyet
                    sigortasına dahil olan serbest meslek sahibi olarak).</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_41_3319603.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
