import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>E-ticaret için sözleşme taslağı</Text>
                <Text style={{marginTop: 10}}>Tüm ülkelerde yapılması gerekli ana adımlar aşağıdaki gibidir:</Text>
                <FlatList
                    data={[
                        {key: '1.Adım: Teklif ve onay prosedürünün gerçekleştirilmesi'},
                        {key: '2.Adım: Teklif formunun tamamlanması'},
                        {key: '3.Adım: Şart ve koşulların oluşturulması'},
                        {key: '4.Adım: On-line olarak müşteri kart bilgilerinin alınması'},
                        {key: '5.Adım: Sipariş onayının gelmesi'},
                        {key: '6.Adım: Alınan bilginin ve iptal etme haklarının onaylanması'},
                        {key: '7.Adım: Dağıtım'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>TÜRKİYE’deki uygulama</Text>
                <Text>E-Ticaret Düzenlemeleri, tüm ticari web sitelerinin, web sitesinden tüketicilere doğrudan ve
                    kalıcı olarak aşağıdaki bilgileri sağlamasını gerektirir:</Text>
                <FlatList
                    data={[
                        {key: 'Şirket adı, posta adresi  (ve farklı olması durumunda kayıtlı şirket adresi) ve e-posta adresi;'},
                        {key: 'Şirket kayıt numarası,'},
                        {key: 'Herhangi bir Ticaret veya Meslek Birliği Üyeliği varsa;'},
                        {key: 'Şirketin vergi numarası.'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Bu verilerin tümü, sitenin çevrimiçi satılıp satılmadığına bakılmaksızın
                    dahil edilmelidir. Ayrıca, bir "Bilgi Toplumu Hizmeti" sağlamada kullanılan e-posta veya SMS metin
                    servisi gibi herhangi bir ticari iletişim bu bilgiyi göstermelidir.
                    {"\n"}E-Ticaret Yönetmeliği ayrıca, tüm fiyatların açık olmasını ve web sitelerinin fiyatların vergi
                    ve teslimat maliyetlerini içerip içermediğini belirtmesini zorunlu kılar (Türkiye Ulusal Raporu,
                    2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>SLOVENYA’daki uygulamalar</Text>
                <Text>Slovenya Cumhuriyeti Kamu Ödemeler İdaresi'ne Şirket kaydı yapılmalıdır.
                    {"\n\n"}Fatura için portal erişim sağlanmalı (öncesinde dijital sertifika alınmalı).
                    {"\n\n"}Slovenya Cumhuriyeti Kamu Ödemeler İdaresi web sitesine erişelibilmesi (Slovenya Ulusal
                    Raporu, 2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>POLONYA’daki uygulamalar</Text>
                <Text>2018 itibariyle, her girişimci vergi numarası hazırlamalı.
                    {"\n\n"}Sonrasında, fatura oluşturmalı (e-fatura hazırlığı yalnızca sadece bilgisayar programları
                    tarafından değil, bazı bankalar tarafından da banka hesapları aracılığıyla verilmektedir).
                    {"\n\n"}Girişimcinin sosyal güvenliğine ilişkin tüm gerekli belgeler SII (PLATNIK adlı Sosyal
                    Sigorta Kurumu) tarafından hazırlanan özel bir program aracılığıyla elektronik olarak görülmek
                    zorundadır.(Polonya Ulusal Raporu).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>YUNANİSTAN için</Text>
                <Text>E-fatura Yunanistan’da 2006 yılında başlamıştır. Ancak elektronik sistem tam anlamıyla
                    oturmamıştır, 2019 sonu itibariyle tamamlanacaktır.
                    {"\n\n"}Fatura oluşturulur oluşturulmaz müşterinin muhasebe sistemine masrafı kabul ettiğini eş
                    zamanlı bildirilir ve aynı zamanda vergi dairesinin vergiyi alabilir (Yunanistan Ulusal Raporu,
                    2018).
                </Text>

                <Text style={{
                    color: 'green',
                    textAlign: 'center',
                    fontWeight: 'bold',
                    marginBottom: 10,
                    marginTop: 20,
                    fontSize: 17
                }}>FRANSA için</Text>
                <Text>Kişisel bilgi toplayan (isim, e-posta ..) ve müşteri ve potansiyel müşterinin dosyalarını
                    oluşturan ticari taraflar, Ulusal Bilişim ve Özgürlükler Komisyonu'na basitleştirilmiş bir beyanda
                    bulunmalıdır.
                    {"\n\n"}Online ticaret siteleri Kolaylaştırılmış Standards 48 altında yönetilmektedir (Fransa Ulusal
                    Raporu, 2018).
                </Text>
            </View>
        )
    }
}
