import React, {Component} from 'react'
import {ScrollView, Text, TouchableOpacity} from 'react-native'
import GestureRecognizer from 'react-native-swipe-gestures'
import {HeaderBackButton} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
// module 1
import M1INTRO from "../Data_tr/m1intro";
import M1S1 from "../Data_tr/m1s1";
import M1S2 from "../Data_tr/m1s2";
import M1S3 from "../Data_tr/m1s3";
import M1S4 from "../Data_tr/m1s4";
import M1S5 from "../Data_tr/m1s5";
import M1S6 from "../Data_tr/m1s6";
import M1S6_2 from "../Data_tr/m1s6-2";
import M1S7 from "../Data_tr/m1s7";
import M1S8 from "../Data_tr/m1s8";
import M1S9 from "../Data_tr/m1s9";
import M1S10 from "../Data_tr/m1s10";
import M1S10_2 from "../Data_tr/m1s10-2";
import M1S11 from "../Data_tr/m1s11";
import M1S11_2 from "../Data_tr/m1s11-2";
import M1S11_3 from "../Data_tr/m1s11-3";
import M1S11_4 from "../Data_tr/m1s11-4";
import M1S11_5 from "../Data_tr/m1s11-5";
import M1S12 from "../Data_tr/m1s12";
import M1S12_2 from "../Data_tr/m1s12-2";
import M1S12_3 from "../Data_tr/m1s12-3";
import M1S13 from "../Data_tr/m1s13";
import M1S14 from "../Data_tr/m1s14";
import M1S14_2 from "../Data_tr/m1s14-2";
import M1S15 from "../Data_tr/m1s15";
// module 2
import M2S1 from "../Data_tr/m2s1";
import M2S2 from "../Data_tr/m2s2";
import M2S3 from "../Data_tr/m2s3";
import M2S4 from "../Data_tr/m2s4";
import M2S5 from "../Data_tr/m2s5";
import M2S6 from "../Data_tr/m2s6";
import M2S7 from "../Data_tr/m2s7";
import M2S8 from "../Data_tr/m2s8";
import M2S9 from "../Data_tr/m2s9";
import M2S10 from "../Data_tr/m2s10";
import M2S11 from "../Data_tr/m2s11";
import M2S12 from "../Data_tr/m2s12";
import M2S12_2 from "../Data_tr/m2s12-2";
import M2S13 from "../Data_tr/m2s13";
import M2S13_2 from "../Data_tr/m2s13-2";
import M2S13_3 from "../Data_tr/m2s13-3";
import M2S14 from "../Data_tr/m2s14";
import M2S15 from "../Data_tr/m2s15";
import M2S15_2 from "../Data_tr/m2s15-2";
import M2S16 from "../Data_tr/m2s16";
import M2S17 from "../Data_tr/m2s17";
import M2S17_2 from "../Data_tr/m2s17-2";
import M2S18 from "../Data_tr/m2s18";
import M2S19 from "../Data_tr/m2s19";
import M2S19_2 from "../Data_tr/m2s19-2";
import M2S20 from "../Data_tr/m2s20";
import M2S21 from "../Data_tr/m2s21";
import M2S22 from "../Data_tr/m2s22";
import M2S23 from "../Data_tr/m2s23";
import M2S24 from "../Data_tr/m2s24";
import M2S25 from "../Data_tr/m2s25";
import M2S26 from "../Data_tr/m2s26";
import M2S27 from "../Data_tr/m2s27";
import M2S28 from "../Data_tr/m2s28";
import M2S29 from "../Data_tr/m2s29";
import M2S30 from "../Data_tr/m2s30";
import M2S31 from "../Data_tr/m2s31";
import M2S32 from "../Data_tr/m2s32";
import M2S33 from "../Data_tr/m2s33";
import M2S34 from "../Data_tr/m2s34";
import M2S35 from "../Data_tr/m2s35";
import M2S36 from "../Data_tr/m2s36";
import M2S36_2 from "../Data_tr/m2s36-2";
import M2S37 from "../Data_tr/m2s37";
import M2S38 from "../Data_tr/m2s38";
import M2S39 from "../Data_tr/m2s39";
import M2S39_2 from "../Data_tr/m2s39-2";
import M2S40 from "../Data_tr/m2s40";
import M2S41 from "../Data_tr/m2s41";
import M2S41_2 from "../Data_tr/m2s41-2";
import M2S41_3 from "../Data_tr/m2s41-3";
import M2S42 from "../Data_tr/m2s42";
import M2S42_2 from "../Data_tr/m2s42-2";
import M2S43 from "../Data_tr/m2s43";
import M2S43_2 from "../Data_tr/m2s43-2";
import M2S44 from "../Data_tr/m2s44";
import M2S44_2 from "../Data_tr/m2s44-2";
import M2S45 from "../Data_tr/m2s45";
import M2S45_2 from "../Data_tr/m2s45-2";
import M2S46 from "../Data_tr/m2s46";
import M2S47 from "../Data_tr/m2s47";
import M2S48 from "../Data_tr/m2s48";
import M2S49 from "../Data_tr/m2s49";
import M2S49_2 from "../Data_tr/m2s49-2";
import M2S49_3 from "../Data_tr/m2s49-3";
import M2S50 from "../Data_tr/m2s50";
import M2S50_2 from "../Data_tr/m2s50-2";
import M2S51 from "../Data_tr/m2s51";
import M2S51_2 from "../Data_tr/m2s51-2";
import M2S52 from "../Data_tr/m2s52";
import M2S53 from "../Data_tr/m2s53";
import M2S54 from "../Data_tr/m2s54";
import M2S54_2 from "../Data_tr/m2s54-2";
import M2S55 from "../Data_tr/m2s55";
import M2S55_2 from "../Data_tr/m2s55-2";
import M2S56 from "../Data_tr/m2s56";
import M2S57 from "../Data_tr/m2s57";
import M2S57_2 from "../Data_tr/m2s57-2";
import M2S58 from "../Data_tr/m2s58";
import M2S58_2 from "../Data_tr/m2s58-2";
import M2S58_3 from "../Data_tr/m2s58-3";
import M2S59 from "../Data_tr/m2s59";
import M2S59_2 from "../Data_tr/m2s59-2";
import M2S59_3 from "../Data_tr/m2s59-3";
import M2S60 from "../Data_tr/m2s60";
import M2S60_2 from "../Data_tr/m2s60-2";
import M2S61 from "../Data_tr/m2s61";
import M2S61_2 from "../Data_tr/m2s61-2";
import M2S62 from "../Data_tr/m2s62";
import M2S62_2 from "../Data_tr/m2s62-2";
import M2S62_3 from "../Data_tr/m2s62-3";
import M2S63 from "../Data_tr/m2s63";
import M2S63_2 from "../Data_tr/m2s63-2";
import M2S63_3 from "../Data_tr/m2s63-3";
import M2S64 from "../Data_tr/m2s64";
import M2S64_2 from "../Data_tr/m2s64-2";
import M2S65 from "../Data_tr/m2s65";
import M2S65_2 from "../Data_tr/m2s65-2";
import M2S66 from "../Data_tr/m2s66";
import M2S66_2 from "../Data_tr/m2s66-2";
import M2S67 from "../Data_tr/m2s67";
// module 3
import M3S1 from "../Data_tr/m3s1";
import M3S2 from "../Data_tr/m3s2";
import M3S3 from "../Data_tr/m3s3";
import M3S4 from "../Data_tr/m3s4";
import M3S4_2 from "../Data_tr/m3s4-2";
import M3S4_3 from "../Data_tr/m3s4-3";
import M3S4_4 from "../Data_tr/m3s4-4";
import M3S5 from "../Data_tr/m3s5";
import M3S5_2 from "../Data_tr/m3s5-2";
import M3S5_3 from "../Data_tr/m3s5-3";
import M3S5_4 from "../Data_tr/m3s5-4";
import M3S6 from "../Data_tr/m3s6";
import M3S6_2 from "../Data_tr/m3s6-2";
import M3S6_3 from "../Data_tr/m3s6-3";
import M3S6_4 from "../Data_tr/m3s6-4";
import M3S7 from "../Data_tr/m3s7";
import M3S7_2 from "../Data_tr/m3s7-2";
import M3S7_3 from "../Data_tr/m3s7-3";
import M3S7_4 from "../Data_tr/m3s7-4";
import M3S8 from "../Data_tr/m3s8";
import M3S8_2 from "../Data_tr/m3s8-2";
import M3S8_3 from "../Data_tr/m3s8-3";
import M3S9 from "../Data_tr/m3s9";
import M3S9_2 from "../Data_tr/m3s9-2";
import M3S9_3 from "../Data_tr/m3s9-3";
import M3S9_4 from "../Data_tr/m3s9-4";
import M3S10 from "../Data_tr/m3s10";
import M3S10_2 from "../Data_tr/m3s10-2";
import M3S10_3 from "../Data_tr/m3s10-3";
import M3S10_4 from "../Data_tr/m3s10-4";
import M3S10_5 from "../Data_tr/m3s10-5";
import M3S11 from "../Data_tr/m3s11";
import M3S11_2 from "../Data_tr/m3s11-2";
import M3S11_3 from "../Data_tr/m3s11-3";
import M3S11_4 from "../Data_tr/m3s11-4";
import M3S11_5 from "../Data_tr/m3s11-5";
import M3S12 from "../Data_tr/m3s12";
import M3S12_2 from "../Data_tr/m3s12-2";
import M3S12_3 from "../Data_tr/m3s12-3";
import M3S12_4 from "../Data_tr/m3s12-4";
import M3S12_5 from "../Data_tr/m3s12-5";
import M3S12_6 from "../Data_tr/m3s12-6";
import M3S12_7 from "../Data_tr/m3s12-7";
import M3S13 from "../Data_tr/m3s13";
import M3S13_2 from "../Data_tr/m3s13-2";
import M3S13_3 from "../Data_tr/m3s13-3";
import M3S13_4 from "../Data_tr/m3s13-4";
import M3S13_5 from "../Data_tr/m3s13-5";
import M3S14 from "../Data_tr/m3s14";
import M3S14_2 from "../Data_tr/m3s14-2";
import M3S14_3 from "../Data_tr/m3s14-3";
import M3S14_4 from "../Data_tr/m3s14-4";
import M3S14_5 from "../Data_tr/m3s14-5";
import M3S15 from "../Data_tr/m3s15";
import M3S15_2 from "../Data_tr/m3s15-2";
import M3S15_3 from "../Data_tr/m3s15-3";
import M3S15_4 from "../Data_tr/m3s15-4";
import M3S16 from "../Data_tr/m3s16";
//module4
import M4S1 from "../Data_tr/m4s1";
import M4S2 from "../Data_tr/m4s2";
import M4S3 from "../Data_tr/m4s3";
import M4S4 from "../Data_tr/m4s4";
import M4S5 from "../Data_tr/m4s5";
import M4S6 from "../Data_tr/m4s6";
import M4S7 from "../Data_tr/m4s7";
import M4S8 from "../Data_tr/m4s8";
import M4S9 from "../Data_tr/m4s9";
import M4S10 from "../Data_tr/m4s10";
import M4S11 from "../Data_tr/m4s11";
import M4S12 from "../Data_tr/m4s12";
import M4S13 from "../Data_tr/m4s13";
import M4S14 from "../Data_tr/m4s14";
import M4S15 from "../Data_tr/m4s15";
//module 5
import M5S1 from "../Data_tr/m5s1";
import M5S2 from "../Data_tr/m5s2";
import M5S3 from "../Data_tr/m5s3";
import M5S4 from "../Data_tr/m5s4";
import M5S5 from "../Data_tr/m5s5";
import M5S6 from "../Data_tr/m5s6";
import M5S7 from "../Data_tr/m5s7";
import M5S8 from "../Data_tr/m5s8";
import M5S9 from "../Data_tr/m5s9";
import M5S10 from "../Data_tr/m5s10";
import M5S11 from "../Data_tr/m5s11";
import M5S12 from "../Data_tr/m5s12";
import M5S13 from "../Data_tr/m5s13";
import M5S14 from "../Data_tr/m5s14";
//module 6
import M6S1 from "../Data_tr/m6s1";
import M6S2 from "../Data_tr/m6s2";
import M6S3 from "../Data_tr/m6s3";
import M6S3_2 from "../Data_tr/m6s3-2";
import M6S3_3 from "../Data_tr/m6s3-3";
import M6S3_4 from "../Data_tr/m6s3-4";
import M6S3_5 from "../Data_tr/m6s3-5";
import M6S3_6 from "../Data_tr/m6s3-6";
import M6S3_7 from "../Data_tr/m6s3-7";
import M6S3_8 from "../Data_tr/m6s3-8";
import M6S3_9 from "../Data_tr/m6s3-9";
import M6S3_10 from "../Data_tr/m6s3-10";
import M6S4 from "../Data_tr/m6s4";
import M6S4_2 from "../Data_tr/m6s4-2";
import M6S4_3 from "../Data_tr/m6s4-3";
import M6S4_4 from "../Data_tr/m6s4-4";
import M6S4_5 from "../Data_tr/m6s4-5";
import M6S4_6 from "../Data_tr/m6s4-6";
import M6S4_7 from "../Data_tr/m6s4-7";
import M6S4_8 from "../Data_tr/m6s4-8";
import M6S4_9 from "../Data_tr/m6s4-9";
import M6S5 from "../Data_tr/m6s5";
import M6S6 from "../Data_tr/m6s6";
import M6S6_2 from "../Data_tr/m6s6-2";
import M6S6_3 from "../Data_tr/m6s6-3";
import M6S6_4 from "../Data_tr/m6s6-4";
import M6S7 from "../Data_tr/m6s7";
import M6S7_2 from "../Data_tr/m6s7-2";
import M6S7_3 from "../Data_tr/m6s7-3";
import M6S7_4 from "../Data_tr/m6s7-4";
import M6S7_5 from "../Data_tr/m6s7-5";
import M6S7_6 from "../Data_tr/m6s7-6";
import M6S7_7 from "../Data_tr/m6s7-7";
import M6S7_8 from "../Data_tr/m6s7-8";
import M6S7_9 from "../Data_tr/m6s7-9";
import M6S7_10 from "../Data_tr/m6s7-10";
import M6S8 from "../Data_tr/m6s8";
//module 7
import M7S1 from "../Data_tr/m7s1";
import M7S2 from "../Data_tr/m7s2";
import M7S3 from "../Data_tr/m7s3";
import M7S4 from "../Data_tr/m7s4";
import M7S5 from "../Data_tr/m7s5";
import M7S6 from "../Data_tr/m7s6";
import M7S7 from "../Data_tr/m7s7";
import M7S8 from "../Data_tr/m7s8";
import M7S9 from "../Data_tr/m7s9";
import M7S10 from "../Data_tr/m7s10";
import M7S11 from "../Data_tr/m7s11";
import M7S12 from "../Data_tr/m7s12";
import M7S13 from "../Data_tr/m7s13";
import M7S14 from "../Data_tr/m7s14";
import M7S15 from "../Data_tr/m7s15";
import M7S16 from "../Data_tr/m7s16";
import M7S17 from "../Data_tr/m7s17";
import M7S18 from "../Data_tr/m7s18";
import M7S19 from "../Data_tr/m7s19";
import M7S20 from "../Data_tr/m7s20";
import M7S21 from "../Data_tr/m7s21";
import M7S22 from "../Data_tr/m7s22";
import M7S23 from "../Data_tr/m7s23";
import M7S24 from "../Data_tr/m7s24";
import M7S25 from "../Data_tr/m7s25";
import M7S26 from "../Data_tr/m7s26";
import M7S27 from "../Data_tr/m7s27";
import M7S27_2 from "../Data_tr/m7s27-2";
import M7S27_3 from "../Data_tr/m7s27-3";
import M7S27_4 from "../Data_tr/m7s27-4";
import M7S27_5 from "../Data_tr/m7s27-5";
import M7S28 from "../Data_tr/m7s28";
import M7S28_2 from "../Data_tr/m7s28-2";
import M7S28_3 from "../Data_tr/m7s28-3";
import M7S28_4 from "../Data_tr/m7s28-4";
import M7S28_5 from "../Data_tr/m7s28-5";
import M7S28_6 from "../Data_tr/m7s28-6";
import M7S28_7 from "../Data_tr/m7s28-7";
import M7S28_8 from "../Data_tr/m7s28-8";
import M7S29 from "../Data_tr/m7s29";
import M7S29_2 from "../Data_tr/m7s29-2";
import M7S29_3 from "../Data_tr/m7s29-3";
import M7S29_4 from "../Data_tr/m7s29-4";
import M7S30 from "../Data_tr/m7s30";
import M7S30_2 from "../Data_tr/m7s30-2";
import M7S30_3 from "../Data_tr/m7s30-3";
import M7S30_4 from "../Data_tr/m7s30-4";
import M7S30_5 from "../Data_tr/m7s30-5";
import M7S30_6 from "../Data_tr/m7s30-6";
import M7S30_7 from "../Data_tr/m7s30-7";
import M7S31 from "../Data_tr/m7s31";
import M7S31_2 from "../Data_tr/m7s31-2";
import M7S31_3 from "../Data_tr/m7s31-3";
import M7S31_4 from "../Data_tr/m7s31-4";
import M7S31_5 from "../Data_tr/m7s31-5";
import M7S31_6 from "../Data_tr/m7s31-6";
import M7S31_7 from "../Data_tr/m7s31-7";
import M7S32 from "../Data_tr/m7s32";
// module 8
import M8S1 from "../Data_tr/m8s1";
import M8S2 from "../Data_tr/m8s2";
import M8S3 from "../Data_tr/m8s3";
import M8S4 from "../Data_tr/m8s4";
import M8S5 from "../Data_tr/m8s5";
import M8S6 from "../Data_tr/m8s6";
import M8S7 from "../Data_tr/m8s7";
import M8S8 from "../Data_tr/m8s8";
import M8S8_2 from "../Data_tr/m8s8-2";
import M8S8_3 from "../Data_tr/m8s8-3";
import M8S8_4 from "../Data_tr/m8s8-4";
import M8S9 from "../Data_tr/m8s9";
import M8S9_2 from "../Data_tr/m8s9-2";
import M8S9_3 from "../Data_tr/m8s9-3";
import M8S9_4 from "../Data_tr/m8s9-4";
import M8S10 from "../Data_tr/m8s10";
// module 9
import M9S1 from "../Data_tr/m9s1";
import M9S2 from "../Data_tr/m9s2";
import M9S3 from "../Data_tr/m9s3";
import M9S3_2 from "../Data_tr/m9s3-2";
import M9S3_3 from "../Data_tr/m9s3-3";
import M9S3_4 from "../Data_tr/m9s3-4";
import M9S3_5 from "../Data_tr/m9s3-5";
import M9S3_6 from "../Data_tr/m9s3-6";
import M9S3_7 from "../Data_tr/m9s3-7";
import M9S3_8 from "../Data_tr/m9s3-8";
import M9S3_9 from "../Data_tr/m9s3-9";
import M9S3_10 from "../Data_tr/m9s3-10";
import M9S4 from "../Data_tr/m9s4";
import M9S5 from "../Data_tr/m9s5";
import M9S5_2 from "../Data_tr/m9s5-2";
import M9S5_3 from "../Data_tr/m9s5-3";
import M9S5_4 from "../Data_tr/m9s5-4";
import M9S5_5 from "../Data_tr/m9s5-5";
import M9S6 from "../Data_tr/m9s6";
import M9S7 from "../Data_tr/m9s7";
import M9S7_2 from "../Data_tr/m9s7-2";
import M9S8 from "../Data_tr/m9s8";
//others


export default class ModuleContent_tr extends Component {

    static navigationOptions = ({navigation}) => {
        const screenNo = navigation.getParam('screenNo', 1);
        const moduleNo = navigation.getParam('moduleNo', 1);
        const partNo = navigation.getParam('partNo', 1);


        let navContentTableIcon =
            <TouchableOpacity onPress={() => navigation.navigate("ContentTableScreen", {
              moduleNo: moduleNo
            })}>
              <Icon name='md-menu' size={32} style={{marginRight:10}}/>
            </TouchableOpacity>;

        // if not active learning
        if (partNo !== 2) {
            navContentTableIcon = null;
        }

        return {
            title: 'M'+navigation.getParam('moduleNo')+': '+navigation.getParam('navTitle') + ' (' + screenNo + '/' + Object.keys(componentList['M' + moduleNo]['P' + partNo]).length + ')',
            headerLeft: (
                <HeaderBackButton onPress={() => navigation.navigate("TypeContentScreen", {
                    screen: "TypeContentScreen",
                    moduleNo: moduleNo
                })}/>
            ),
            headerTitle: ({ style, children : title }) => {
              return (
                <Text style={style} numberOfLines={2}>{title}</Text>
              )
            },
            headerRight: navContentTableIcon,
            headerStyle: {borderBottomColor: 'black', borderBottomWidth: 2},
            headerTitleStyle: {fontSize: 17}
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            loadScreen: false,
        }
    }

    onSwipeLeft(moduleNo, partNo, screenNo) {
        if (screenNo >= Object.keys(componentList['M' + moduleNo]['P' + partNo]).length) {
            return;
        }
        this.props.navigation.navigate('ModuleContent_tr', {moduleNo: moduleNo, partNo: partNo, screenNo: screenNo + 1})

    };

    onSwipeRight(moduleNo, partNo, screenNo) {
        if (screenNo <= 1) {
            return;
        }
        this.props.navigation.navigate('ModuleContent_tr', {moduleNo: moduleNo, partNo: partNo, screenNo: screenNo - 1})
    };

    render() {
        const {navigation} = this.props;
        const screenNo = navigation.getParam('screenNo', 1);
        const moduleNo = navigation.getParam('moduleNo', 1);
        const partNo = navigation.getParam('partNo', 1);
        const LoadComponent = componentList['M' + moduleNo]['P' + partNo]['S' + screenNo];
        return (
                <GestureRecognizer
                    onSwipeLeft={() => {
                        this.onSwipeLeft(moduleNo, partNo, screenNo);
                        this.setState({loadScreen: true})
                    }}
                    onSwipeRight={() => {
                        this.onSwipeRight(moduleNo, partNo, screenNo);
                        this.setState({loadScreen: true})
                    }}
                    style={{
                        flex: 1,
                    }}
                >
                    <ScrollView>
                        <LoadComponent/>
                    </ScrollView>
                </GestureRecognizer>
        )
    }
}

const componentList = {
    M1: {
        P1: {
            S1: M1INTRO,
            S2: M1S1,
            S3: M1S2,
        },
        P2: {
            S1: M1S3, // 1
            S2: M1S4,
            S3: M1S5,
            S4: M1S6,
            S5: M1S6_2,
            S6: M1S7,
            S7: M1S8,
            S8: M1S9,
            S9: M1S10,
            S10: M1S10_2,
            S11: M1S11,
            S12: M1S11_2,
            S13: M1S11_3,
            S14: M1S11_4,
            S15: M1S11_5,
            S16: M1S12,
            S17: M1S12_2,
            S18: M1S12_3,
            S19: M1S13,
            S20: M1S14,
            S21: M1S14_2
        },
        P4: {
            S1: M1S15
        }
    },
    M2: {
        P1: {
            S1: M2S1,
            S2: M2S2,
            S3: M2S3,
            S4: M2S4
        },
        P2: {
            S1: M2S5, //
            S2: M2S6,
            S3: M2S7,
            S4: M2S8,
            S5: M2S9,
            S6: M2S10,
            S7: M2S11,
            S8: M2S12,
            S9: M2S12_2,
            S10: M2S13,
            S11: M2S13_2,
            S12: M2S13_3,
            S13: M2S14,
            S14: M2S15,
            S15: M2S15_2,
            S16: M2S16, //
            S17: M2S17,
            S18: M2S17_2,
            S19: M2S18,
            S20: M2S19,
            S21: M2S19_2,
            S22: M2S20,
            S23: M2S21,
            S24: M2S22,
            S25: M2S23,
            S26: M2S24,
            S27: M2S25,
            S28: M2S26,
            S29: M2S27,
            S30: M2S28,
            S31: M2S29,
            S32: M2S30,
            S33: M2S31, //
            S34: M2S32,
            S35: M2S33,
            S36: M2S34,
            S37: M2S35,
            S38: M2S36,
            S39: M2S36_2,
            S40: M2S37,
            S41: M2S38, //
            S42: M2S39,
            S43: M2S39_2,
            S44: M2S40,
            S45: M2S41,
            S46: M2S41_2,
            S47: M2S41_3,
            S48: M2S42,
            S49: M2S42_2,
            S50: M2S43,
            S51: M2S43_2,
            S52: M2S44,
            S53: M2S44_2,
            S54: M2S45,
            S55: M2S45_2,
            S56: M2S46,
            S57: M2S47,
            S58: M2S48,
            S59: M2S49, //
            S60: M2S49_2,
            S61: M2S49_3,
            S62: M2S50,
            S63: M2S50_2,
            S64: M2S51,
            S65: M2S51_2,
            S66: M2S52,
            S67: M2S53,
            S68: M2S54,
            S69: M2S54_2,
            S70: M2S55,
            S71: M2S55_2,
            S72: M2S56, //
            S73: M2S57,
            S74: M2S57_2,
            S75: M2S58,
            S76: M2S58_2,
            S77: M2S58_3,
            S78: M2S59,
            S79: M2S59_2,
            S80: M2S59_3,
            S81: M2S60,
            S82: M2S60_2,
            S83: M2S61,
            S84: M2S61_2,
            S85: M2S62,
            S86: M2S62_2,
            S87: M2S62_3,
            S88: M2S63,
            S89: M2S63_2,
            S90: M2S63_3,
            S91: M2S64,
            S92: M2S64_2,
            S93: M2S65,
            S94: M2S65_2,
            S95: M2S66,
            S96: M2S66_2
        },
        P4: {
            S1: M2S67
        }
    },
    M3: {
        P1: {
            S1: M3S1,
            S2: M3S2,
            S3: M3S3
        },
        P2: {
            S1: M3S4, //
            S2: M3S4_2,
            S3: M3S4_3,
            S4: M3S4_4,
            S5: M3S5,
            S6: M3S5_2,
            S7: M3S5_3,
            S8: M3S5_4,
            S9: M3S6,
            S10: M3S6_2,
            S11: M3S6_3,
            S12: M3S6_4,
            S13: M3S7,
            S14: M3S7_2,
            S15: M3S7_3,
            S16: M3S7_4,
            S17: M3S8,
            S18: M3S8_2,
            S19: M3S8_3,
            S20: M3S9, //
            S21: M3S9_2,
            S22: M3S9_3,
            S23: M3S9_4,
            S24: M3S10, //
            S25: M3S10_2,
            S26: M3S10_3,
            S27: M3S10_4,
            S28: M3S10_5,
            S29: M3S11,
            S30: M3S11_2,
            S31: M3S11_3,
            S32: M3S11_4,
            S33: M3S11_5,
            S34: M3S12, //
            S35: M3S12_2,
            S36: M3S12_3,
            S37: M3S12_4,
            S38: M3S12_5,
            S39: M3S12_6,
            S40: M3S12_7,
            S41: M3S13, //
            S42: M3S13_2,
            S43: M3S13_3,
            S44: M3S13_4,
            S45: M3S13_5,
            S46: M3S14,
            S47: M3S14_2,
            S48: M3S14_3,
            S49: M3S14_4,
            S50: M3S14_5,
            S51: M3S15, //
            S52: M3S15_2,
            S53: M3S15_3,
            S54: M3S15_4
        },
        P4: {
            S1: M3S16
        },
    },
    M4: {
        P1: {
            S1: M4S1,
            S2: M4S2,
            S3: M4S3,
            S4: M4S4,
            S5: M4S5,
            S6: M4S6,
            S7: M4S7,
            S8: M4S8,
            S9: M4S9
        },
        P2: {
            S1: M4S10,
            S2: M4S11,
            S3: M4S12,
            S4: M4S13,
            S5: M4S14
        },
        P4: {
            S1: M4S15
        }
    },
    M5: {
        P1: {
            S1: M5S1,
            S2: M5S2,
            S3: M5S3,
            S4: M5S4,
            S5: M5S5
        },
        P2: {
            S1: M5S6,
            S2: M5S7,
            S3: M5S8,
            S4: M5S9,
            S5: M5S10,
            S6: M5S11,
            S7: M5S12,
            S8: M5S13
        },
        P4: {
            S1: M5S14
        }
    },
    M6: {
        P1: {
            S1: M6S1,
            S2: M6S2
        },
        P2: {
            S1: M6S3,
            S2: M6S3_2,
            S3: M6S3_3,
            S4: M6S3_4,
            S5: M6S3_5,
            S6: M6S3_6,
            S7: M6S3_7,
            S8: M6S3_8,
            S9: M6S3_9,
            S10: M6S3_10,
            S11: M6S4, //
            S12: M6S4_2,
            S13: M6S4_3,
            S14: M6S4_4,
            S15: M6S4_5,
            S16: M6S4_6,
            S17: M6S4_7,
            S18: M6S4_8,
            S19: M6S4_9,
            S20: M6S5, //
            S21: M6S6, //
            S22: M6S6_2,
            S23: M6S6_3,
            S24: M6S6_4,
            S25: M6S7, //
            S26: M6S7_2,
            S27: M6S7_3,
            S28: M6S7_4,
            S29: M6S7_5,
            S30: M6S7_6,
            S31: M6S7_7,
            S32: M6S7_8,
            S33: M6S7_9,
            S34: M6S7_10
        },
        P4: {
            S1: M6S8
        }
    },
    M7: {
        P1: {
            S1: M7S1,
            S2: M7S2,
            S3: M7S3,
            S4: M7S4,
            S5: M7S5
        },
        P2: {
            S1: M7S6,
            S2: M7S7, //
            S3: M7S8,
            S4: M7S9,
            S5: M7S10,
            S6: M7S11,
            S7: M7S12, //
            S8: M7S13,
            S9: M7S14,
            S10: M7S15,
            S11: M7S16,
            S12: M7S17, //
            S13: M7S18,
            S14: M7S19,
            S15: M7S20,
            S16: M7S21,
            S17: M7S22, //
            S18: M7S23,
            S19: M7S24,
            S20: M7S25,
            S21: M7S26,
            S22: M7S27, //
            S23: M7S27_2,
            S24: M7S27_3,
            S25: M7S27_4,
            S26: M7S27_5,
            S27: M7S28, //
            S28: M7S28_2,
            S29: M7S28_3,
            S30: M7S28_4,
            S31: M7S28_5,
            S32: M7S28_6,
            S33: M7S28_7,
            S34: M7S28_8,
            S35: M7S29, //
            S36: M7S29_2,
            S37: M7S29_3,
            S38: M7S29_4,
            S39: M7S30, //
            S40: M7S30_2,
            S41: M7S30_3,
            S42: M7S30_4,
            S43: M7S30_5,
            S44: M7S30_6,
            S45: M7S30_7,
            S46: M7S31, //
            S47: M7S31_2,
            S48: M7S31_3,
            S49: M7S31_4,
            S50: M7S31_5,
            S51: M7S31_6,
            S52: M7S31_7
        },
        P4: {
            S1: M7S32
        }
    },
    M8: {
        P1: {
            S1: M8S1,
            S2: M8S2,
            S3: M8S3,
            S4: M8S4,
            S5: M8S5,
            S6: M8S6,
            S7: M8S7
        },
        P2: {
            S1: M8S8,
            S2: M8S8_2,
            S3: M8S8_3,
            S4: M8S8_4,
            S5: M8S9,
            S6: M8S9_2,
            S7: M8S9_3,
            S8: M8S9_4
        },
        P4: {
            S1: M8S10
        }
    },
    M9: {
        P1: {
            S1: M9S1,
            S2: M9S2
        },
        P2: {
            S1: M9S3,
            S2: M9S3_2, //
            S3: M9S3_3,
            S4: M9S3_4,
            S5: M9S3_5,
            S6: M9S3_6,
            S7: M9S3_7,
            S8: M9S3_8,
            S9: M9S3_9,
            S10: M9S3_10,
            S11: M9S4, //
            S12: M9S5,
            S13: M9S5_2,
            S14: M9S5_3,
            S15: M9S5_4,
            S16: M9S5_5,
            S17: M9S6, //
            S18: M9S7,
            S19: M9S7_2,
        },
        P4: {
            S1: M9S8,
        }
    }
};

