import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    content: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    RectangleShapeView: {
        marginTop: 20,
        width: 150 * 2,
        height: 65,
        borderWidth: 2,
        borderRadius:5,
        borderColor: 'blue',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    SquareView: {
        marginTop: 15,
        width: 30,
        height: 30,
        backgroundColor: 'blue',
    },
    TriangleView: {
        width: 0,
        height: 0,
        borderLeftWidth: 25,
        borderRightWidth: 25,
        borderBottomWidth: 35,
        borderStyle: 'solid',
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'blue',
        transform: [
            {rotate: '180deg'}
        ]
    }
})
