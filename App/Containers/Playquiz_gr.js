import React, {Component} from 'react'
import {StyleSheet, Text, View} from 'react-native'
import {HeaderBackButton} from 'react-navigation';

import M1Q1 from "../Data_gr/quiz1";
import M2Q1 from "../Data_gr/quiz2";
import M3Q1 from "../Data_gr/quiz3";
import M4Q1 from "../Data_gr/quiz4";
import M5Q1 from "../Data_gr/quiz5";
import M6Q1 from "../Data_gr/quiz6";
import M7Q1 from "../Data_gr/quiz7";
import M8Q1 from "../Data_gr/quiz8";
import M9Q1 from "../Data_gr/quiz9";
import styles from "./Styles/LaunchScreenStyles";

import {playQuizStyles} from './Styles/general';
import I18n from "../Services/I18n";


export default class Playquiz_gr extends Component {
    static navigationOptions = ({navigation}) => {
        const moduleNo = navigation.getParam('moduleNo', 1);
        return {
            title: "M"+navigation.getParam('moduleNo')+': ' + navigation.getParam('navTitle'),
            headerLeft: (
                <HeaderBackButton onPress={() => navigation.navigate("TypeContentScreen", {
                    screen: "TypeContentScreen",
                    moduleNo: moduleNo
                })}/>
            ),
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            quizFinish: false,
            score: 0
        }
    }

    _quizFinish(score) {
        this.setState({quizFinish: true, score: Math.round(score)});
    }

    _scoreMessage(score) {
        if (score <= 30) {
            return (
                <View style={playQuizStyles.innerContainer}>
                    <Text style={playQuizStyles.score}>{I18n.t('_resultMessage1')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_scoreMessage')}: {score}%</Text>
                </View>)
        } else if (score > 30 && score < 50) {
            return (
                <View style={playQuizStyles.innerContainer}>
                    <Text style={playQuizStyles.score}>{I18n.t('_resultMessage2')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_scoreMessage')}: {score}%</Text>
                </View>)
        } else if (score >= 50 && score < 75) {
            return (
                <View style={playQuizStyles.innerContainer}>
                    <Text style={playQuizStyles.score}>{I18n.t('_resultMessage3')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_congrats')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_scoreMessage')}:</Text>
                    <Text style={playQuizStyles.score}>{score}%</Text>
                </View>)
        } else if (score >= 75 && score < 90) {
            return (
                <View style={playQuizStyles.innerContainer}>
                    <Text style={playQuizStyles.score}>{I18n.t('_resultMessage3')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_congrats')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_scoreMessage')}:</Text>
                    <Text style={playQuizStyles.score}>{score}%</Text>
                </View>)
        } else if (score >= 90) {
            return (
                <View style={playQuizStyles.innerContainer}>
                    <Text style={playQuizStyles.score}>{I18n.t('_resultMessage5')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_congrats')}</Text>
                    <Text style={playQuizStyles.score}>{I18n.t('_scoreMessage')}:</Text>
                    <Text style={playQuizStyles.score}>{score}%!</Text>
                </View>)
        }
    }


    render() {
        const {navigation} = this.props;
        const moduleNo = navigation.getParam('moduleNo', 1);
        const testNo = navigation.getParam('partNo', 3);
        const Quiz = componentList['M' + moduleNo]['P' + testNo];
        return (
            <View style={styles.mainContainer}>
                {this.state.quizFinish ?
                    <View style={playQuizStyles.container}>
                        <View style={playQuizStyles.circle}>
                            {this._scoreMessage(this.state.score)}
                        </View>
                    </View> : <Quiz quizFinish={(score) => this._quizFinish(score)}/>
                }

            </View>
        )
    }
}
const componentList = {
    M1: {
        P3: M1Q1
    },
    M2: {
        P3: M2Q1
    },
    M3: {
        P3: M3Q1
    },
    M4: {
        P3: M4Q1
    },
    M5: {
        P3: M5Q1
    },
    M6: {
        P3: M6Q1
    },
    M7: {
        P3: M7Q1
    },
    M8: {
        P3: M8Q1
    },

    M9: {
        P3: M9Q1
    }
};

const scoreCircleSize = 300;
const styles2 = StyleSheet.create({
    score: {
        color: "white",
        fontSize: 20,
        fontStyle: 'italic',
        padding: 5,
        fontWeight: 'bold'
    },
    circle: {
        justifyContent: 'center',
        alignItems: 'center',
        width: scoreCircleSize,
        height: scoreCircleSize,
        borderRadius: scoreCircleSize / 2,
        backgroundColor: "#f09609"
    },
    innerContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
    }
});
