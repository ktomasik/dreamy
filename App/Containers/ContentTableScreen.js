import React, {Component} from 'react';
import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import I18n from "../Services/I18n";
import Flag from 'react-native-flags';

export default class ContentTableScreen extends Component {
    constructor() {
        super();
        this.state = {
            language: I18n.currentLocale(),
        }
    }

    static navigationOptions = ({navigation}) => ({
        title: I18n.t('_toc')
    });

    render() {

        const {navigation} = this.props;
        const moduleNo = navigation.getParam('moduleNo');

        const module1 = [
            [I18n.t('_toc1_1'), 1],
            [I18n.t('_toc1_2'), 8],
            [I18n.t('_toc1_3'), 16]
        ];

        var module2;

        if (this.state.language == "fr") {
            module2 = [
                [I18n.t('_toc2_1'), 1],
                [I18n.t('_toc2_2'), 11],
                [I18n.t('_toc2_3'), 18],
                [I18n.t('_toc2_4'), 26],
                [I18n.t('_toc2_5'), 44],
                [I18n.t('_toc2_6'), 57]
            ]
        }

        else {

            module2 = [
                [I18n.t('_toc2_1'), 1],
                [I18n.t('_toc2_2'), 16],
                [I18n.t('_toc2_3'), 33],
                [I18n.t('_toc2_4'), 41],
                [I18n.t('_toc2_5'), 58],
                [I18n.t('_toc2_6'), 72]
            ]
        }

        const module3 = [
            [I18n.t('_toc3_1'), 1],
            [I18n.t('_toc3_2'), 20],
            [I18n.t('_toc3_3'), 24],
            [I18n.t('_toc3_4'), 34],
            [I18n.t('_toc3_5'), 41],
            [I18n.t('_toc3_6'), 51]
        ];

        const module4 = [
            [I18n.t('_toc4_1'), 1],
            [I18n.t('_toc4_2'), 2],
            [I18n.t('_toc4_3'), 3],
            [I18n.t('_toc4_4'), 4],
            [I18n.t('_toc4_5'), 5]
        ];

        const module5 = [
            [I18n.t('_toc5_1'), 1],
            [I18n.t('_toc5_2'), 2],
            [I18n.t('_toc5_3'), 3],
            [I18n.t('_toc5_4'), 4],
            [I18n.t('_toc5_5'), 5],
            [I18n.t('_toc5_6'), 6],
            [I18n.t('_toc5_7'), 7],
            [I18n.t('_toc5_8'), 8]
        ];

        const module6 = [
            [I18n.t('_toc6_1'), 1],
            [I18n.t('_toc6_2'), 11],
            [I18n.t('_toc6_3'), 20],
            [I18n.t('_toc6_4'), 21],
            [I18n.t('_toc6_5'), 25],
        ];

        const sl = <Flag code="SI" size={48}/>;
        const tr = <Flag code="TR" size={48}/>;
        const gr = <Flag code="GR" size={48}/>;
        const fr = <Flag code="FR" size={48}/>;
        const pl = <Flag code="PL" size={48}/>;
        const module7 = [
            [I18n.t('_toc7_1'), 2],
            [I18n.t('_toc7_2'), 7],
            [I18n.t('_toc7_3'), 12],
            [I18n.t('_toc7_4'), 17],
            [I18n.t('_toc7_5'), 22, sl],
            [I18n.t('_toc7_6'), 27, tr],
            [I18n.t('_toc7_7'), 35, gr],
            [I18n.t('_toc7_8'), 39, fr],
            [I18n.t('_toc7_9'), 46, pl]
        ];

        const module8 = [
            [I18n.t('_toc8_1'), 1]
        ];

        const module9 = [
            [I18n.t('_toc9_1'), 2],
            [I18n.t('_toc9_2'), 11],
            [I18n.t('_toc9_3'), 17]
        ];


        const linkList = {
            m1: module1,
            m2: module2,
            m3: module3,
            m4: module4,
            m5: module5,
            m6: module6,
            m7: module7,
            m8: module8,
            m9: module9
        };

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={styles.container}>
                    <View style={styles.linkContainer}>
                        {linkList['m' + moduleNo].map((x, y) => {
                            return (
                                <TouchableOpacity key={y} style={styles.linkText}
                                                  onPress={() => this.onLinkClick(moduleNo, x[1])}>

                                    <Text style={styles.singleRow}>
                                        <Icon name='md-arrow-dropright-circle' size={24} style={styles.icon}/> {x[0]}{' '}{x[2]} - <Text
                                        style={{fontWeight: 'bold', color: 'blue'}}>{I18n.t('_tocPage')}: {x[1]}</Text>
                                    </Text>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </ScrollView>

            </View>

        )
    }


    onLinkClick(moduleNo, screenNo) {
        const moduleLang = this.state.language;
        this.props.navigation.navigate("ModuleContent_" + moduleLang, {
            screen: "ModuleContent_" + moduleLang,
            moduleNo: moduleNo,
            partNo: 2,
            screenNo: screenNo,
            navTitle: I18n.t('_titleActiveLearning'),
        })
    }
}


const styles = StyleSheet.create({

    linkContainer: {
        marginTop: 20,
        marginRight: 40,
        marginLeft: 40
    },
    linkText: {
        marginBottom: 15,
        paddingBottom: 10,
        borderBottomColor: '#a200ff',
        borderBottomWidth: 2,
    },
    singleRow: {
        fontWeight: 'bold',
        fontSize: 16,
    },
    icon: {
        color: '#a200ff',
        marginRight: 10
    }
});
