import React, {Component} from 'react'
import {Image, ScrollView, Text, View} from 'react-native'
import {Button} from 'react-native-elements'
import {Images} from '../Themes'
import I18n from '../Services/I18n';

// Styles
import styles from './Styles/LaunchScreenStyles'
import {HeaderBackButton, NavigationActions, StackActions} from 'react-navigation';
import ModuleContent from "./ModuleContent";

export default class TypeContentScreen extends Component {
    static navigationOptions = ({navigation}) => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'LaunchScreen'})]
        });

        return {
            title: "M" + navigation.getParam('moduleNo') + ': ' + navigation.getParam('moduleTitles'),
            headerLeft: (
                <HeaderBackButton onPress={() => navigation.dispatch(resetAction)}/>
            ),
            headerStyle: {borderBottomColor: 'black', borderBottomWidth: 2}
        }
    };

    render() {
        const {navigation} = this.props;
        const moduleNo = navigation.getParam('moduleNo', 1);
        const buttonHeight = 55;
        const moduleLang = navigation.getParam('moduleLang');
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={styles.container}>
                    <View style={styles.centered}>
                        <Image source={Images.logo} style={styles.logo}/>
                    </View>
                    <View style={styles.section}>
                        <Button title={I18n.t("_titleBasicConcepts")} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#00aba9', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('ModuleContent_'+moduleLang, {
                                        screenNo: 1,
                                        partNo: 1,
                                        moduleNo: moduleNo,
                                        navTitle: I18n.t("_titleBasicConcepts")
                                    })
                                }}/>
                        <Button title={I18n.t("_titleActiveLearning")} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#ff0097', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('ModuleContent_'+moduleLang, {
                                        screenNo: 1,
                                        partNo: 2,
                                        moduleNo: moduleNo,
                                        navTitle: I18n.t("_titleActiveLearning")
                                    })
                                }}/>
                        <Button title={I18n.t("_titleQuiz")} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#a200ff', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('Playquiz_'+moduleLang, {
                                        moduleNo: moduleNo,
                                        partNo: 3,
                                        navTitle: I18n.t("_titleQuiz")
                                    })
                                }}/>
                        <Button title={I18n.t("_titleReferences")} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#f09609', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('ModuleContent_'+moduleLang, {
                                        screenNo: 1,
                                        partNo: 4,
                                        moduleNo: moduleNo,
                                        navTitle: I18n.t("_titleReferences")
                                    })
                                }}/>
                    </View>

                </ScrollView>
            </View>
        );
    }
}
