import React, {Component} from 'react';
import I18n from "../Services/I18n";
import styles from "./Styles/LaunchScreenStyles";
import {HeaderBackButton, NavigationActions, StackActions} from "react-navigation";
import {Image, Linking, ScrollView, StyleSheet, Text, View} from 'react-native';
import {Button} from "react-native-elements";
import Metrics from "../Themes/Metrics";


export default class AboutProject extends Component {

    static navigationOptions = ({navigation}) => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'LaunchScreen'})]
        });

        return {
            title: I18n.t('_aboutTitle'),
            headerLeft: (
                <HeaderBackButton onPress={() => navigation.dispatch(resetAction)}/>
            ),
            headerStyle: {borderBottomColor: 'black', borderBottomWidth: 2}
        }
    };

    render() {
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={styles.container}>
                    <View style={styles.section}>
                        <Text style={styles2.h2}>{I18n.t('_aboutProjectTitle')}</Text>
                        <Text style={styles2.h4}>{I18n.t('_aboutProjectNumber')}: 2017-1-TR01-KA204-045864</Text>
                        <View style={{alignItems: "center", marginTop: 15, marginBottom: 15}}>
                        <Button title={I18n.t('_aboutProjectWebsite')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#28a745', height: 55}}
                                onPress={() => Linking.openURL('https://www.dreamy-m-learning.org/')}/>
                        </View>
                        <Text style={{fontSize: 16 }}>{I18n.t('_aboutDescription')}</Text>
                        <View style={{flexDirection: "row", flexWrap: "wrap", marginTop: 25}}>
                            <Image
                                style={{width: 75, height: 80}}
                                source={require('../Images/logos/turkish.jpg')}
                                resizeMode="contain"
                                />
                                <Image
                                    style={{width: 100, height: 80}}
                                    source={require('../Images/logos/turkish_NA.jpg')}
                                    resizeMode="contain"
                                />
                                <Image
                                    style={{width: 155, height: 80}}
                                    source={require('../Images/logos/EU_logo.jpg')}
                                    resizeMode="contain"
                                />
                        </View>
                        <Text style={{fontSize: 16 }}>{I18n.t('_aboutDisclaimer')}</Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
const styles2 = StyleSheet.create({
    h2: {
        fontSize: 20,
        lineHeight: 26,
        fontWeight: "bold",
        marginTop: Metrics.smallMargin,
        textAlign: "center",
    },
    h4: {
        fontSize: 17,
        textAlign: "center",
        marginTop: 10,
        fontWeight: "bold",
    }
});
