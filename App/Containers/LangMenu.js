import React, {Component} from 'react';
import I18n, {switchLanguage} from "../Services/I18n";
import styles from "./Styles/LaunchScreenStyles";
import {NavigationActions, StackActions} from "react-navigation";
import {ScrollView, StyleSheet, TouchableOpacity, View, Text} from 'react-native';
import Flag from 'react-native-flags';


export default class LangMenu extends Component {

    constructor() {
        super();
        this.state = {
            language: I18n.currentLocale(),
        }
    }

    _rootComponent = () => {
        return this;
    };
    static navigationOptions = ({navigation}) => ({
        title: I18n.t('_lang')
    });

    render() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({routeName: 'LaunchScreen'})]
        });
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={styles.container}>
                    <View style={styles.section}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>
                            <TouchableOpacity onPress={() => {
                                this.setState({language: 'en'});
                                switchLanguage('en', this._rootComponent());
                                this.props.navigation.dispatch(resetAction);
                            }}>
                                <Flag
                                    code="GB"
                                    size={48}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                this.setState({language: 'pl'});
                                switchLanguage('pl', this._rootComponent());
                                this.props.navigation.dispatch(resetAction);
                            }}>
                                <Flag
                                    code="PL"
                                    size={48}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                this.setState({language: 'tr'});
                                switchLanguage('tr', this._rootComponent());
                                this.props.navigation.dispatch(resetAction);
                            }}>
                                <Flag
                                    code="TR"
                                    size={48}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                this.setState({language: 'gr'});
                                switchLanguage('gr', this._rootComponent());
                                this.props.navigation.dispatch(resetAction);
                            }}>
                                <Flag
                                    code="GR"
                                    size={48}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                this.setState({language: 'sl'});
                                switchLanguage('sl', this._rootComponent());
                                this.props.navigation.dispatch(resetAction);
                            }}>
                                <Flag
                                    code="SI"
                                    size={48}
                                />
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => {
                                this.setState({language: 'fr'});
                                switchLanguage('fr', this._rootComponent());
                                this.props.navigation.dispatch(resetAction);
                            }}>
                                <Flag
                                    code="FR"
                                    size={48}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    langButtons: {
        textAlign: 'center',
    }
});
