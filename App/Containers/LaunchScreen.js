import React, {Component} from 'react';
import {ScrollView, View} from 'react-native';
import {Button} from 'react-native-elements';

import I18n, {switchLanguage, updateLanguage} from '../Services/I18n'
// Styles
import styles from './Styles/LaunchScreenStyles'


export default class LaunchScreen extends Component {

    constructor() {
        super();
        let language = I18n.currentLocale().slice(0,2);
        language = ['en', 'pl', 'tr', 'sl', 'fr', 'gr'].includes(language) ? language : 'en';
        updateLanguage(language);

        this.state = {
            language: language,
        }
    }

    render() {
        const buttonHeight = 45;
        const moduleLang = this.state.language;
        return (
            <View style={styles.mainContainer}>
                <ScrollView style={styles.container}>
                    <View style={styles.section}>
                        <Button title={"M1 - " + I18n.t('_titleM1')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#00aba9', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 1,
                                        moduleTitles: I18n.t('_titleM1'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M2 - " + I18n.t('_titleM2')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#ff0097', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 2,
                                        moduleTitles: I18n.t('_titleM2'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M3 - " + I18n.t('_titleM3')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#a200ff', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 3,
                                        moduleTitles: I18n.t('_titleM3'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M4 - " + I18n.t('_titleM4')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#1ba1e2', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 4,
                                        moduleTitles: I18n.t('_titleM4'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M5 - " + I18n.t('_titleM5')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#f09609', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 5,
                                        moduleTitles: I18n.t('_titleM5'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M6 - " + I18n.t('_titleM6')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#00aba9', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 6,
                                        moduleTitles: I18n.t('_titleM6'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M7 - " + I18n.t('_titleM7')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#ff0097', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 7,
                                        moduleTitles: I18n.t('_titleM7'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M8 - " + I18n.t('_titleM8')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#a200ff', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 8,
                                        moduleTitles: I18n.t('_titleM8'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={"M9 - " + I18n.t('_titleM9')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#1ba1e2', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => {
                                    this.props.navigation.navigate('TypeContentScreen', {
                                        moduleNo: 9,
                                        moduleTitles: I18n.t('_titleM9'),
                                        moduleLang: moduleLang
                                    })
                                }}/>
                        <Button title={I18n.t('_aboutTitle')} textStyle={{textAlign: 'center'}}
                                buttonStyle={{backgroundColor: '#28a745', height: buttonHeight}}
                                containerViewStyle={{marginTop: 5, marginBottom: 5, height: buttonHeight}}
                                onPress={() => this.props.navigation.navigate('AboutProject')}/>
                    </View>
                </ScrollView>
            </View>
        )
    }
}
