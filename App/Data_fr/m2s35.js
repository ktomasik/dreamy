import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S35 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour partager à partir d'Instagram vers d'autres réseaux sociaux</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 5: Il est recommandé d’ajouter un #hashtag à votre description pour inclure la publication dans le contenu de #hashtags. Il est
                    judicieux d'inclure #hashtag qui compte déjà de nombreux adeptes. Depuis que nous publions un bouclier ardunio pour la robotique, nous incluons #arduino et arduinoshield. Les deux
                    #hashtags sont suivis par plusieurs dizaines de milliers de followers.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img66.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img67.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
