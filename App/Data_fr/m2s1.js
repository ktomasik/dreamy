import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu'est-ce que le réseautage? Qu'est-ce que cela signifie pour vous?</Text>
                <Text>Selon The Oxford Dictionnaire («Dictionnaire anglais, thésaurus, aide de grammaire | dictionnaires Oxford» 2018), la mise en réseau est «un groupe de personnes qui échangent des
                    informations, des contacts et des expériences à des fins professionnelles ou sociales». Toutefois, si vous représentez dix personnes différentes en réseau, vous pouvez obtenir
                    jusqu'à dix réponses différentes. La définition de la mise en réseau d’une personne dépend probablement de son utilisation de cette activité personnelle et professionnelle
                    importante. Toutefois, que vous souhaitiez créer un réseau pour vous faire de nouveaux amis, trouver un nouvel emploi, développer votre carrière actuelle, explorer de nouvelles
                    options de carrière, obtenir des références ou des prospects, ou simplement élargir vos horizons professionnels, il est important de vous concentrer sur la mise en réseau en tant
                    qu’échange d’informations, de contacts ou d’expériences (‘Qu'est-ce que le réseautage, n.d.). Dans n'importe quel secteur ou carrière, la mise en réseau vous aide à établir des
                    liens de manière personnelle et à établir des relations de soutien et de respect afin de découvrir et de créer des avantages mutuels. C'est un ensemble de compétences dont aucune
                    femme professionnelle sérieuse du 21ème siècle ne peut se dispenser.
                    {"\n\n"}Peu importe l’industrie dans laquelle vous vous trouvez, connaître d’autres bonnes personnes peut rapporter. Lorsque tout est bien fait, le réseautage peut conduire à plus
                    de clients, plus de visibilité et plus d'opportunités pour progresser dans la carrière. Et pour les demandeurs d'emploi, la mise en réseau prend vraiment tout son sens, vous
                    plaçant dans le cadre de rôles non annoncés ou vous mettant en contact avec le décideur pour des jobs non annoncés ou vous mettant en contact avec le décideur pour des jobs qui
                    existent ('Qu'est-ce que le réseautage? | Reed.co.uk’, n.d.).

                </Text>
                <Text style={styles.dmlH2}>Qu'est-ce qu'un média social?</Text>
                <Text>Les médias sociaux sont une technologie informatique facilitant le partage d’idées et d’informations, ainsi que la création de réseaux et de communautés virtuels. De par leur
                    conception, les médias sociaux sont basés sur Internet et offrent aux utilisateurs une communication électronique aisée d'informations personnelles et d'autres contenus, tels que
                    des vidéos et des photos. Les utilisateurs utilisent les médias sociaux via un ordinateur, une tablette ou un smartphone via un logiciel ou une application Web, et les utilisent
                    souvent pour la messagerie.
                    {"\n\n"}À l'origine, les médias sociaux étaient un outil que les gens utilisaient pour interagir avec leurs amis et leur famille, mais ils ont ensuite été adoptés par les
                    entreprises qui souhaitaient tirer parti d'une nouvelle méthode de communication populaire pour atteindre leurs clients. Le pouvoir des médias sociaux réside dans la capacité de se
                    connecter et de partager des informations avec n'importe qui sur Terre (ou des multitudes de personnes), à condition qu'ils utilisent également les médias sociaux (Silver, n.d.).

                </Text>
                <Text style={styles.dmlH2}>Une brève histoire des médias sociaux</Text>
                <Text>Les médias sociaux ont une histoire qui remonte aux années 1970 (Ries 2016). ARPANET, qui a été mis en ligne pour la première fois en 1969, avait développé un riche échange culturel d'idées non gouvernementales / commerciales et de communication à la fin des années 1970, comme le prouve clairement le règlement ARPANET # ARPANET# Règles et étiquettes “Un manuel de 1982 sur l’informatique publié par AI Lab du MIT énonçait ce qu’il convenait de savoir s’appliquer aux réseaux »et correspondait parfaitement à la définition actuelle du terme« médias sociaux »contenue dans cet article. Usenet, arrivé en 1979, a été battu par un précurseur du système de babillard électronique (BBS) connu sous le nom de Community Memory en 1973. De véritables systèmes de babillard électronique sont arrivés avec le système de babillard électronique à Chicago, qui a été mis en ligne le 16 février 1978. Peu de temps après, la plupart des grandes villes avaient plus d’un BBS fonctionnant sur les ordinateurs personnels TRS-80, Apple II, Atari, IBM PC, Commodore 64, Sinclair et similaires (‘Social Media - Wikipédia’, n.d.)
                    {"\n\n"}Le PC IBM a été introduit en 1981 et les modèles suivants d’ordinateurs Mac et de PC ont été utilisés tout au long des années 1980. Plusieurs modems, suivis par du matériel de télécommunication spécialisé, ont permis à de nombreux utilisateurs d’être en ligne simultanément. Compuserve, Prodigy et AOL étaient trois des plus grandes entreprises de BBS et ont été les premiers à migrer vers Internet dans les années 1990. Entre le milieu des années 80 et le milieu des années 90, le nombre de BBS s'est élevé à des dizaines de milliers en Amérique du Nord (Edvards 2016). Des forums de messagerie (une structure spécifique de médias sociaux) ont vu le jour au cours des années 1980 et au début des années 1990. Lorsque l'Internet a proliféré au milieu des années 90, les forums de messages ont migré en ligne, devenant des forums Internet, principalement en raison d'un accès par personne moins coûteux et de la capacité de gérer beaucoup plus de personnes simultanément que les banques de modem téléphonique (‘Social Media - Wikipédia’, n.d.).
                    {"\n\n"}GeoCities a été l’un des premiers sites de réseautage social sur Internet, apparaissant en novembre 1994, suivi de Classmates en décembre 1995, Six Degrees en mai 1997, Open Diary en octobre 1998, LiveJournal en avril 1999, Ryze en octobre 2001, Friendster en mars 2002, LinkedIn en mai 2003, hi5 en juin 2003, MySpace en août 2003, Orkut en janvier 2004, Facebook en février 2004, Yahoo! 360 ° en mars 2005, Bebo en juillet 2005, Twitter en juillet 2006, Tumblr en février 2007 et Google+ en juillet 2010 ('Décembre 1995: Les camarades de classe - Avant et maintenant: une histoire de sites de réseautage social - Images - Actualités Cbs', «Histoire et différents types de médias sociaux»,n.d.; Ortutay 2012).
                </Text>
            </View>
        )
    }
}
