import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Acceptation des paiements par carte de crédit </Text>
                <Text style={{marginTop: 15}}>Suivez les instructions ci-dessous pour accepter les paiements par carte de crédit.
                    {"\n\n"}Pour accepter les paiements par carte de crédit, vous aurez besoin de :

                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Compte marchand: ',
                                val2: 'type de compte bancaire dans lequel les paiements par carte de crédit et de débit sont déposés'
                            }
                        },
                        {
                            key: {
                                val1: 'Terminal virtuel: ',
                                val2: 'À l\'instar d\'une machine à cartes de crédit numérique, ce système vous permet de saisir des informations de carte de crédit sur votre ordinateur.'
                            }
                        },
                        {
                            key: {
                                val1: 'Passerelle: ',
                                val2: 'Connecteur entre votre boutique en ligne et la banque, qui envoie les informations de paiement en toute sécurité pour approbation ou refus.'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />

                <Text style={{marginTop: 10}}>Comment ça marche pas à pas</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Étape 1:',
                                val2: "Lorsqu'un client contacte vous, il/elle se logue à votre terminal virtuel et sélectionne les produits ou services qu'il/elle souhaite acheter. Il/elle doit ensuite saisir ses informations de carte de crédit."

                            }
                        },
                        {
                            key: {
                                val1: 'Étape 2:',
                                val2: "Les informations de paiement transitent par la passerelle de paiement sécurisée et sont transmises à la source d'autorisation."

                            }
                        },
                        {
                            key: {
                                val1: 'Étape 3:',
                                val2: "La banque qui a émis la carte de crédit reçoit les informations sur la transaction, vérifie si les fonds sont disponibles et approuve ou refuse la libération du paiement."

                            }
                        },
                        {
                            key: {
                                val1: 'Étape 4:',
                                val2: "La passerelle de paiement vous \"indique\" si le paiement a été accepté ou refusé."
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 5:',
                                val2: "Si approuvé, les fonds sont déposés sur votre compte bancaire dans 2-3 jours. "
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight:'bold'}}>{item.key.val1}</Text> {item.key.val2}</Text>}
                />
            </View>
        )
    }
}
