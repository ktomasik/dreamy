import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz2": {
            "question1": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Pêcher les fruits de mer avec de grands filets.",
                    "option2": "b) Un technicien qui répare le système de réseau.",
                    "option3": "c) Un groupe de personnes qui échangent des informations",
                    "option4": "d) Une sorte de service public pour aider les gens"
                },
                "question": "Qu'est-ce que le réseautage?"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "La création de comptes de médias sociaux diffère selon les marques de smartphones."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Un groupe de personnes regardant un match de football.",
                    "option2": "b) Une technologie informatique pour construire des réseaux virtuels et partager des informations.",
                    "option3": "c) Une technologie commune (par exemple   Télévision, radio) où les informations sociales peuvent être rassemblées.",
                    "option4": "d) Une chaîne de télévision privée pour regarder des films."
                },
                "question": "Qu'est-ce qu'un média social?"
            },
            "question4": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Instagram",
                    "option2": "b) YouTube",
                    "option3": "c) Facebook",
                    "option4": "d) Google",

                },
                "question": "Qu’est-ce qui n’est pas un média social?"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Envoi de publicités régulièrement - chaque jour.",
                    "option2": "b) Publier autant de contenu que possible pour rendre la chaîne vivante.",
                    "option3": "c) Répondre aux commentaires des gens.",
                    "option4": "d) Bloquer les gens sur les médias sociaux."
                },
                "question": "Quelle est la meilleure pratique pour maintenir un public?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Sur certains sites de médias sociaux, les membres ne peuvent pas créer de pages thématiques."
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Avoir des objectifs clairs de marketing.",
                    "option2": "b) Avoir assez d'argent pour démarrer une entreprise.",
                    "option3": "c) Assurer un produit de bonne qualité.",
                    "option4": "d) Faire de la publicité"
                },
                "question": "Quel est le plus important dans le marketing social et les affaires?"
            },
            "question8": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Fournir un espace web gratuit",
                    "option2": "b) Encourager les membres à télécharger du contenu",
                    "option3": "c) Autoriser le chat en direct",
                    "option4": "d) Tous"
                },
                "question": "Lequel des éléments suivants est une caractéristique des médias sociaux courants?"
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Vous pouvez utiliser les mêmes adresses électroniques pour vous connecter à différents comptes de médias sociaux"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Adresse électronique",
                    "option2": "b) Moteur de recherche",
                    "option3": "c) Des médias sociaux",
                    "option4": "d) Courrier indésirable"
                },
                "question": "_______ permet de partager les spécifications des produits, de faire de la publicité et de communiquer avec les gens directement sur Internet.."
            }
        }
    }
};

export default class M1Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz2;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
        /*console.log('countCheck: ' + this.state.countCheck);
        console.log('punkty: ' + this.score)*/
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
