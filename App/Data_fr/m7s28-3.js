import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Turquie:</Text>

                <Text style={styles.dmlH3}>Étape 3:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Les dates de début des formations à l\'entreprenariat sont également publiées sur le site officiel du KOSGEB. Ces formations à l\'entreprenariat sont totalement gratuites  ',
                                val2: 'http://www.kosgeb.gov.tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
