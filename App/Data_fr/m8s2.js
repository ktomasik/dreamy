import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Méthodes de livraison</Text>
                <Text style={{marginTop: 10}}>Selon la région, les méthodes d’expédition sont divisées en deux groupes: transport intérieur et international. Le transport intérieur comprend le
                    transport à l'intérieur du pays et la documentation de la marchandise est constituée par la réglementation nationale. Les entreprises de transport prennent un colis et le transfert
                    au client.
                    {"\n\n"}Selon le type de véhicule de transfert, le fret aérien, l'expédition et le transport routier sont disponibles. Le fret aérien et le transport maritime constituent un
                    commerce d'outre-mer commun. De plus, le transfert routier est le type de transport le plus courant entre les pays frontaliers. Pour les matériaux artisanaux en petites quantités,
                    les entreprises de transport liées peuvent transférer les marchandises par route. Une facture et un bon peuvent être nécessaires. Dans certains cas, les paquetages nécessitent une
                    déclaration personnalisée et doivent donc contenir davantage de documents. Les entreprises de transport de marchandises peuvent faire des recommandations et choisir le bon chemin.
                </Text>
                <Text style={styles.dmlH3}> Offrir la livraison gratuite </Text>
                <Text>La livraison gratuite peut être définie avec un montant minimum de commande ou un nombre minimum d'articles. Offrir la livraison gratuite est l'un des moyens d'attirer
                    l'attention du client. Dans l’ensemble, il est clair que l’affichage «Livraison gratuite» sur les ventes offre un avantage significatif par rapport à tous les concurrents du même
                    domaine. (Anonymous 2018a). </Text>
                <Text style={styles.dmlH3}> Frais de livraison moins chers </Text>
                <Text>Des accords de travail continu avec une entreprise de transport peuvent aider à réduire les frais d’expédition en raison de la capacité de travail. De plus, choisir le meilleur
                    transporteur individuellement pour chaque commande peut entraîner des taux plus bas. Bien qu'il puisse sembler long et laborieux de réévaluer votre choix d'expédition chaque fois
                    que vous recevez une commande, différents transporteurs peuvent proposer des tarifs extrêmement différents en fonction du poids, des dimensions et de la destination de votre colis.
                    {"\n\n"}La négociation avec différentes entreprises de fret peut inciter à offrir des prix plus compétitifs. Un bref aperçu de la capacité de travail future et des objectifs pour
                    l’avenir pourrait être efficace pour les taux. D'autre part, comme les tarifs d'expédition sont déterminés par les calculs de poids et de dimensions, un autre moyen d'aider à
                    réduire vos coûts d'expédition consiste à utiliser les emballages fournis par les sociétés de transport.

                </Text>
            </View>
        )
    }
}
