import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S12_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration d'un compte de messagerie sur un téléphone mobile pour une application de messagerie Android (Samsung, Sony, HTC...)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Etape 2: Allez sur votre compte puis sélectionnez un add compte (ajouter un compte)</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img13.png')}
                        resizeMode="contain"/>

                </View>
            </View>
        )
    }
}
