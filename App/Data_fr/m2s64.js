import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S64 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utiliser les sites de réseaux sociaux</Text>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour configurer un blog</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 19: si vous souhaitez publier le même contenu également dans un blog Google+, cliquez sur Partager.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img136.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
