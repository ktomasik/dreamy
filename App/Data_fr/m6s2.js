import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S2 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text>Comme vous l'avez vu dans le module 1, vous devez faire une différence entre Internet et le Web:   Internet est un réseau d’échanges mondiaux - comprenant des réseaux privés, publics, commerciaux, universitaires et gouvernementaux - reliés par des technologies guidées, filaires et à fibres optiques
                    {"\n\n"}Le Web, ou World Wide Web (W3), est fondamentalement un système de serveurs Internet prenant en charge des documents spécialement formatés.
                    {"\n\n"}Internet, relier un ordinateur à d’autres ordinateurs du monde entier, est un moyen de transporter du contenu. Vous devez accéder à Internet pour voir le Web  À l'échelle mondiale    et tout le contenu Web  qu'il contient   the Le Web est la partie Internet de partage d’informations. Le Web utilise également des navigateurs, tels que Google ou Internet, pour accéder à des documents Web, appelés pages Web, reliés entre eux par des hyperliens.
                    {"\n\n"}Les contribuables qui ont le statut de société à responsabilité limitée et / ou un commerçant individuel qui vend des biens et des services sur Internet peuvent bénéficier d’une application de facturation électronique. Il n'y a pas de restriction de chiffre d'affaires pour la transition de facture électronique. Même ceux dont le chiffre d'affaires est très faible peuvent bénéficier de la facturation électronique.
                    {"\n\n"}Une recherche rapide sur Google révélera de nombreuses options. Des programmes tels que Microsoft Office ont un modèle personnalisable dans leur logiciel, et vous pouvez télécharger les modèles et trouver des sites Web disponibles dans Office.
                    {"\n\n"}Cash board: Cash Board propose un modèle téléchargeable que vous pouvez ouvrir dans Microsoft Word. Les modèles sont simples mais personnalisables.
                    {"\n\n"}Invoice berry - Modèles Office, Open Office et Excel
                    {"\n\n"}Microsoft - Si vous n'aimez pas le modèle de Microsoft Office, accédez à la galerie de modèles de Microsoft pour plus d'options.
                    {"\n\n"}En plus de Microsoft Office, Google Docs propose des modèles que vous pouvez sélectionner dans la galerie de modèles et Apple propose des options pour les applications de productivité.
                    {"\n\n"}Certains sites Web vous permettent de créer, d’imprimer, de sauvegarder et d’envoyer des factures directement à partir de leur site. Vous n'avez besoin d'aucun autre programme, à l'exception d'un navigateur Web.
                    {"\n\n"}La section suivante présente quelques applications gratuites pour apprendre à créer des factures électroniques.

                </Text>
            </View>
        )
    }
}
