import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S41_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer un compte Twitter</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 6: Cochez la case Connectez-vous avec des personnes de votre choix pour diffuser vos twits plus efficacement, puis cliquez sur
                    Suivant.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img80.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
