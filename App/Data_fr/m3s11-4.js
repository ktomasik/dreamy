import React, {Component} from 'react'
import {Text, Image, View, TouchableHighlight, Modal} from 'react-native'
import ImageViewer from "react-native-image-zoom-viewer"

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen8 = [{
    url: '',
    props: {
        source: require('../Images/m3/img46.png')
    }
}];

export default class M3S11_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer un logo</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen8}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible)
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 9: Vous pouvez enregistrer ou modifier votre choix. Une fois la sauvegarde effectuée, vous aurez également la possibilité d’avoir
                    votre carte de visite:</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img46.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
