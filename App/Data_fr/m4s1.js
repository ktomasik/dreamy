import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu’est-ce qu’une marque?</Text>
                <Text>Une marque est un nom, un logo, un signe ou une forme qui, individuellement ou conjointement, permet au consommateur de différencier le produit ou le service des autres sur le
                    marché (Shim 2009). </Text>
                <Text style={styles.dmlH2}> Qu’est-ce qu’un nom de marque?</Text>
                <Text>Un nom de marque est un mot ou des chiffres dans une combinaison qui peut être exprimée verbalement (Shim 2009).
                    {"\n\n"}À titre d'exemple ; 3M, Google, XEROX, etc.

                </Text>
                <Text style={styles.dmlH2}> Quelle est la différence entre un nom de marque et une marque?</Text>
                <Text>Le nom d’une entreprise / commerce / affaire est un nom ou un moyen d'aider à identifier une entreprise, une entité ou un individu. C'est le nom officiel sous lequel ladite
                    entité ou cet individu choisit de faire affaire (Cameron 2017).
                    {"\n\n"}Une marque est un mot, une phrase, un logo, un symbole, un dessin, une couleur ou une combinaison d'un ou plusieurs de ces éléments qui distingue les produits / services
                    d'une entreprise de ceux d'une autre (Cameron 2017, Shravani 2017).
                    {"\n\n"}Les différences entre un nom de marque et une marque sont les suivantes (Anonymous 2018a, Shravani 2017):

                </Text>
                <FlatList
                    data={[
                        {key: "Les noms de marque et les marques de commerce sont des atouts précieux pour une entreprise. Souvent, une marque ou une marque déposée devient synonyme du produit. C'est une erreur d'utiliser les termes «marque déposée» et «marque» de manière interchangeable, car ils présentent des différences très importantes"},
                        {key: "Alors que la marque représente la réputation et les affaires aux yeux du public, une marque déposée protège juridiquement les aspects de la marque qui sont uniques et spécifiques à la société."},
                        {key: "Une marque déposée est une protection juridique de la marque, accordée par le Bureau des marques et des brevets."},
                        {key: "Toutes les marques déposées sont des marques, alors que toutes les marques ne sont pas des marques déposées."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}><Text style={{fontWeight: 'bold'}}>À titre d'exemple: </Text>
                    {"\n"}Coco Chanel est l'exemple parfait d'un nom qui est une marque déposée. La célèbre créatrice Coco Chanel a construit son empire de la mode en utilisant son nom. Les gens savaient
                    que s'ils achetaient un produit Coco Chanel, ils recevraient un travail de qualité. Grâce à sa réputation d'excellent goût, son nom est devenu reconnaissable dans le monde entier.
                    Coco Chanel, son nom, est considéré comme une marque déposée, le nom Chanel est considéré comme une marque (Husbey 2016).</Text>
            </View>
        )
    }
}
