import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'


// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Où Trouver la Petite entreprise de prêts pour Femmes?</Text>
                <Text style={styles.dmlH2}>Pour la Grèce:</Text>
                <Text style={styles.dmlH3}>Banks</Text>
                <Text>Lorsque les banques financent de nouvelles entreprises, elles demandent à voir le business plan. L’application sera étudiée par plusieurs groupes vérifiant différents sujets. Il
                    est donc particulièrement important que le business plan soit aussi complet que possible pour éviter tout retard.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
