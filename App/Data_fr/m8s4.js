import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img1.png')
    }
}];

export default class M8S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Emballage & commercialisation</Text>
                <Text style={styles.dmlH3}>Calcul des taux</Text>
                <Text>Lorsqu’il s’agit de classer le fret, l’un des principaux facteurs à prendre en compte est la densité. Certains éléments que vous pouvez entendre sont basés sur la densité.
                    Généralement, plus l'élément est dense, plus la règle de classement est basse. En ce qui concerne le fret lorsque vous avez un article de faible classe à haute densité, le coût
                    d’expédition est inférieur à celui d’un article à faible densité (Anonymous 2018c).
                    {"\n\n"}Le calcul de la densité de fret fournira également une classe recommandée pour l'envoi. Le tableau de classe de fret ci-dessous est une échelle abrégée qui peut être
                    utilisée pour aider à estimer la classification du fret pour les envois (Anonymous 2018d).

                </Text>

                <Text style={styles.dmlH3}> Techniques d'emballage</Text>
                <Text style={{fontWeight: 'bold'}}>Méthode d'emballage de base (méthode d'emballage de boîte)</Text>
                <FlatList
                    data={[
                        {key: "N'expédiez pas de produits fragiles comme des produits non durables à l'intérieur d'une boîte extérieure solide."},
                        {key: "Utilisez des charges telles que du papier journal froissé, des cacahuètes en vrac ou un matériau de rembourrage air-cellulaire pour combler les espaces vides et empêcher le mouvement des marchandises à l'intérieur de la boîte pendant l'expédition"},
                        {key: "Placez les marchandises susceptibles d'être affectées par la saleté, l'eau ou les conditions humides à l'intérieur d'un sac en plastique."},
                        {key: "Consolidez les petites pièces ou les produits granulaires renversables dans un contenant solide et étanche, tel qu’un sac de jute ou un sac en plastique indélébile, puis emballez-le dans un carton extérieur robuste"},
                        {key: "Utilisez la méthode de bande H pour sceller votre colis (Anonymous 2018e)."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{fontWeight: 'bold', marginTop: 5}}> Méthode de la boîte dans la boîte</Text>
                <FlatList
                    data={[
                        {key: "Enveloppez le (s) produit (s) individuellement avec au moins 5 cm (2″) d’épaisseur de mousse ou de matériau alvéolaire à cellules alvéolaires pour s’ajuster parfaitement à l’intérieur d’une boîte en carton ondulé."},
                        {key: "Limitez le mouvement du produit à l’intérieur de la boîte en utilisant un enduit comme du papier journal froissé, des cacahuètes en vrac ou tout autre matériau de rembourrage."},
                        {key: "Fermez et collez le carton intérieur à l’aide de la méthode de ruban H. Cela aidera à prévenir l'ouverture accidentelle."},
                        {key: "Utilisez une deuxième boîte d’au moins 15 cm (6 po) plus longue, plus large et plus profonde que la boîte intérieure."},
                        {key: "Choisissez la méthode d’emballage ou de remplissage pour protéger la boîte intérieure à l’intérieur de la boîte extérieure plus grande et robuste. • Expédiez les produits fragiles individuellement, en les enveloppant dans une épaisseur minimale de 8 cm (3″) de matériau de rembourrage air-cell."},
                        {key: "Enveloppez la boîte intérieure avec un matériau de rembourrage cellulaire de 8 cm (3″) d'épaisseur ou utilisez au moins 8 cm (3″) d'arachides en vrac ou autre matériau de rembourrage pour remplir les espaces entre le haut et le bas. , en bas et de tous les côtés. • Remplissez les espaces vides avec plus de matériau amortissant."},
                        {key: "Utilisez la méthode de bande H pour sceller votre colis (Anonymous 2018e)."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
