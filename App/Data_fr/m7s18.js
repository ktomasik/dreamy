import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S18 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Incitations fiscales pour Start-up de femmes</Text>
                <Text style={styles.dmlH2}>Pour la Turquie:</Text>
                <Text>Conformément à l'article 9/6 de la loi relative à l'impôt sur le revenu, les personnes qui fabriquent et vendent des produits artisanaux dans les maisons où elles habitent sont
                    exemptées de l'impôt (loi n ° 193, Journal officiel n ° 28366 du 27/07/2012). </Text>
                <Text>Pour être exonéré d’impôts, il est nécessaire d’obtenir un "Certificat d’exemption de l’artisanat" (Esnaf Vergi Muafiyeti Belgesi).{"\n"}</Text>
                <Text>La procédure suivante est utilisée pour obtenir un certificat d’exemption artisanale: </Text>
                <Text>Étape 1: Les personnes souhaitant obtenir un certificat doivent en faire la demande au bureau des impôts du lieu où se trouve leur résidence.
                    {"\n\n"}Étape 2: Les informations ci-après correspondent au type d'activité exercée sur le certificat d'exonération fiscale: Fabrication et vente de produits artisanaux dans les maisons où elles habitent (article ITL: 9/6).
                    {"\n\n"}Étape 3: L'adresse du domicile est indiquée comme adresse professionnelle
                    {"\n\n"}Étape 4: S'il est accepté, que les conditions énoncées à l'article 9 de la loi relative à l'impôt sur le revenu sont remplies, le bureau des impôts délivrera un certificat.
                    {"\n\n"}Étape 5: Aucun frais ne sera facturé pour le titulaire du certificat
                    {"\n\n"}Étape 6: Le certificat d'exonération de la taxe artisanale est valable trois ans à compter de la date d'émission et il est possible d'obtenir de nouveaux documents en s'adressant au bureau des impôts à la fin de cette durée
                </Text>
            </View>
        )
    }
}
