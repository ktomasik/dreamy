import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu'est-ce que l'internet?</Text>
                <Text>Internet est un réseau d’échanges mondiaux - comprenant des réseaux privés, publics, commerciaux, universitaires et gouvernementaux - reliés par des technologies guidées,
                    filaires et à fibres optiquesl</Text>
                <Text style={styles.dmlH2}>Petite histoire d'Internet</Text>
                <Text>Internet a pris racine dans les années 1960 en tant que projet du ministère de la Défense des États-Unis, visant à créer un réseau non centralisé. Ce projet a été appelé ARPANET
                    (Réseau d’agences de projets de recherche avancée) . Afin de mondialiser davantage le réseau, une nouvelle technologie IP (Internet Protocol) développée par un protocole standard
                    et sophistiqué définit la manière dont les messages électroniques sont empaquetés, adressés et envoyés sur le réseau.</Text>
                <Text style={styles.dmlH2}>Qu'est-ce que le Web - (World Wide Web)</Text>
                <Text>Le Web, ou World Wide Web (W3), est fondamentalement un système de serveurs Internet prenant en charge des documents spécialement formatés.</Text>
                <Text style={styles.dmlH2}>Les différences entre Internet et le Web</Text>
                <Text>Internet, qui relie un ordinateur à d’autres ordinateurs du monde entier, est un moyen de transporter du contenu. Vous devez accéder à Internet pour voir le Web à l'échelle
                    mondiale et tout ce que le Web ou tout autre contenu contient . Le Web est la partie Internet de partage d’informations. Le Web utilise également des navigateurs, tels que Google
                    ou Internet, pour accéder à des documents Web, appelés pages Web, reliés entre eux par des hyperliens.</Text>
            </View>
        )
    }
}
