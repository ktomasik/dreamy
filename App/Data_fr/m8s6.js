import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Transfert de documents - déclaration en douane et formulaires </Text>
                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Qu'est-ce qu'une déclaration en douane?</Text>
                <Text>Une déclaration en douane est un document officiel qui répertorie et donne les détails des marchandises importées ou exportées.
                    {"\n\n"}En termes juridiques, une déclaration en douane est l'acte par lequel une personne exprime le souhait de placer des marchandises sous un régime douanier donné (Commission
                    Européenne 2018).

                </Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Qui devrait déposer une déclaration en douane?</Text>
                <Text>En général, il s’agit du propriétaire du bien ou d’une personne agissant en son nom (un représentant).
                    {"\n\n"}La personne qui contrôle les marchandises peut également les effectuer. Ces personnes peuvent être des particuliers ou des sociétés, ainsi que dans certains cas des
                    associations de personnes (Commission Européenne 2018).

                </Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Où une déclaration en douane doit-elle être déposée?</Text>
                <Text>La déclaration doit être déposée auprès du bureau de douane où les marchandises ont été ou seront prochainement présentées (Commission Européenne 2018).</Text>

                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Où une déclaration en douane doit-elle être déposée?</Text>
                <Text>Afin de respecter les obligations légales et de placer les marchandises sous un régime douanier, une déclaration en douane doit être déposée (Commission Européenne 2018). Cela
                    devrait se produire dans deux cas:</Text>
                <FlatList
                    data={[
                        {key: 'au moment de l\'importation, lorsque les marchandises entrent sur le territoire douanier, elles doivent être affectées à un traitement ou à un usage approuvé par les douanes'},
                        {key: 'et les marchandises destinées à l\'exportation - doivent être placées sous le régime de l\'exportation'}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
