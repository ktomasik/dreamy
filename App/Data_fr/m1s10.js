import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration d'un compte de messagerie sur un téléphone mobile pour un appareil iOS (iPhone, iPad ou iPod touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 2: Appuyez  pour sélectionner le menu "Comptes et mots de passe"</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img9-2.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
