import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Assurance & suivi</Text>
                <Text style={{marginTop: 10, fontWeight: 'bold', color: 'green', textAlign: 'center'}}>Qu'est-ce qu'une assurance de fret?</Text>
                <Text>Légalement, tous les transporteurs doivent souscrire un montant minimal d'assurance, appelé responsabilité du transporteur. Toutefois, la responsabilité du transporteur offre une
                    couverture très limitée et tout, des catastrophes naturelles aux accidents de véhicules, voire aux actes de guerre, peut endommager votre chargement. Par conséquent, les
                    expéditeurs peuvent demander une assurance cargaison pour protéger leurs marchandises perte, dommage ou vol en transit. En règle générale, les marchandises sont assurées
                    lorsqu'elles sont stockées et en transit jusqu'à ce qu'elles parviennent à l'acheteur (Robinson 2016).
                    {"\n\n"}Il existe différents types de polices d’assurance fret, dont certaines portent des noms tels que «tous risques», «forme générale», «responsabilité civile» et «fret
                    routier». Cependant, la plupart d’entre eux sont nécessaires pour les expéditions de grande capacité au-dessus des mers. Une assurance cargaison peut être souscrite pour le
                    transport international aussi bien que national (Robinson 2016).
                </Text>
            </View>
        )
    }
}
