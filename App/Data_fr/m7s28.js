import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Turquie:</Text>
                <Text style={styles.dmlH3}>Étape 1:</Text>
                <Text>Les entrepreneurs potentiels souhaitant obtenir le soutien de l'État doivent participer à la formation en entrepreneuriat donnée par KOSGEB. Quelque 45% de toutes les formations
                    KOSGEB sont suivies par des femmes. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
