import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img14.png')
    }
}];

export default class M9S7_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Création d'un compte professionnel Twitter:</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Étape 2:</Text> Cliquez sur le bouton «Modifier le profil», puis remplissez toutes les zones nécessaires
                    sur votre page Twitter, comme suit:</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img14.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <FlatList
                    data={[
                        {key: "Nom d'utilisateur: (sera préférable s'il correspond à votre nom d'entreprise)"},
                        {key: "Photo de profil (dimensions recommandées: 400 x 400 pixels)"},
                        {key: "Explication sur votre entreprise (vous avez 160 caractères pour que les gens sachent ce qui rend votre compte si spécial)"},
                        {key: "Image d'en-tête   vous pouvez utiliser des photos d'événement, des photos de produits, des informations / images promotionnelles, ou l'utiliser pour annoncer les nouvelles ventes et promotions en cours pour votre entreprise (les dimensions recommandées sont 1 500 x 500 pixels)"},
                        {key: "Tweet épinglé: Si vous mettez toujours les nouvelles les plus importantes ou les plus récentes en haut, vos visiteurs pourront plus facilement en trouver de nouvelles sans avoir à parcourir toute la page."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Assurez-vous de compléter autant d'informations que possible, et assurez-vous d'utiliser une photo ou un logo de qualité pour tous les médias sociaux…</Text>
            </View>
        )
    }
}
