import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

//                   \/
export default class M7S30_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la France:</Text>
                <Text style={styles.dmlH3}>Si vous n'êtes pas un demandeur d'emploi: L'AIDE DE L'ETAT - ACCRE</Text>

                <Text style={styles.dmlH3}>Étape 2:</Text>
                <Text>En retour, il vous délivre un reçu pour l’enregistrement de votre demande ACCRE, informe les organisations sociales de votre demande et l’envoie à l’Urssaf compétente dans les 24
                    heures. L'URSSAF statue sur la demande dans un délai d'un mois.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
