import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Où Trouver la Petite entreprise de prêts pour Femmes?</Text>
                <Text style={styles.dmlH2}>Pour la France:</Text>
                <Text style={styles.dmlH3}>Fonds de garantie à l'initiative des femmes (FGIF)</Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Objet: Faciliter l\'obtention de prêts bancaires pour couvrir les besoins en fonds de roulement et / ou les investissements en phase de création, de redressement ou de développement d\'une entreprise.',
                                val2: 'https://www.afecreation.fr/pid14855/appuis-pour-les-femmes.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Les réseaux</Text>
                <FlatList
                    data={[
                        {
                            key:
                                {
                                    val1: "Les réseaux nationaux listés ci-dessous sont à votre disposition pour vous accueillir, vous informer et vous accompagner dans la rédaction de votre projet. Certaines sont dédiées aux femmes créatrices, d'autres s'adressent à tous mais comportent des actions spécifiques en faveur des femmes, d'autres suivent toujours les entreprises qu'elles ont financées",
                                    val2: 'http://www.ellesentreprennent.fr/pid14416/les-reseaux-au-service-des-creatrices.html'
                                }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>La BPI France (Banque publique d'investissement):</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Le BPI est une organisation sous la tutelle de l'Etat. Elle vous accompagne dans les aides au financement et au développement. Au lieu de cela, il offre des solutions de cautionnement et de garantie pour convaincre votre banque de vous suivre dans vos projets.",
                                val2: 'http://www.bpifrance.com/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Toutes les Banques et entre autres:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Banque de France", val2: 'https://www.banque-france.fr/statistiques/credit/credit/taux-des-credits-aux-entreprises/'
                            },
                            key: {
                                val1: "Caisse d’épargne", val2: 'https://www.caisse-epargne.fr/professionnels/financement/produit-pret-bancaire-entreprises/'
                            },
                            key: {
                                val1: "Bpifrance", val2: 'https://les-aides.fr/focus/bZNi/les-prets-de-bpifrance-pour-le-financement-du-developpement-des-entreprises.html/'
                            },
                            key: {
                                val1: "ING direct", val2: 'https://pretprodirect.ingdirect.fr/lending?gclid=CjwKCAiAlvnfBRA1EiwAVOEgfKuju6EACWQUmgUUUwLqiB9RknHngEt4a2gV9bBa6atYRBDhcyVFpRoCcI8QAvD_BwE&gclsrc=aw.ds/'
                            },
                            key: {
                                val1: "Crédit Agricole", val2: 'https://www.credit-agricole.fr/professionnel/financement/creation-reprise/pret-createur.html/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
