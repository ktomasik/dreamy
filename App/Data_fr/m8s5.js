import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img8.png')
    }
}];

export default class M8S5 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Emballage & commercialisation</Text>
                <Text style={styles.dmlH3}> Étiquette de manutention spéciale </Text>
                <Text>Les étiquettes d’expédition et de manutention sont des étiquettes pré imprimées qui identifient les instructions de manipulation appropriées et, dans certains cas, les
                    informations relatives à la destination. Ces étiquettes peuvent indiquer le contenu d’emballages, telles que Inflammabilité ou Fragile, ou indiquer les instructions de manipulation
                    (Anonymous 2018j). Les étiquettes d’expédition et de manutention signalent au personnel d’expédition toutes les instructions et tous les besoins en matière de manipulation. Il
                    existe plusieurs types d'étiquettes d'expédition et de manutention, y compris celles indiquées ci-dessous.
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Enveloppes postales et petites étiquettes </Text>sont utilisés pour avertir le personnel des instructions de manipulation spécifiques
                    pour les petits colis envoyés par la poste (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Etiquettes de production et petits emballages </Text>attireront l'attention sur les colis tout au long du processus de production et
                    d'expédition (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Marquage pictural international </Text>sont utilisés pour l'expédition internationale. Les étiquettes illustrées réduisent le risque
                    d'erreur, car le personnel d'autres pays peut ne pas comprendre la langue du pays d'origine du colis, mais il est recommandé d'utiliser des étiquettes supplémentaires dans la
                    langue de la destination de l'envoi pour garantir une manipulation correcte. Les étiquettes doivent être utilisées pour assurer la conformité avec les normes environnementales et
                    de sécurité et pour identifier les matières dangereuses (Anonymous 2018j).
                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Étiquettes de la flèche vers le haut </Text>aide à protéger les colis fragiles pour qu'ils arrivent en toute sécurité. Les étiquettes de
                    flèche vers le haut font partie des étiquettes picturales internationales (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img2.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Étiquettes fragiles/verre </Text>attire l'attention sur le contenu de l'emballage et les instructions de manipulation
                    appropriées telles que "this end up" (Anonymous 2018j). .</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img3.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Étiquettes Rush </Text>sont utilisés pour les emballages dépendants de la température ou des fournitures médicales. Les
                    médicaments périssables qui doivent être réfrigérés et qui sont nécessaires dans des situations de sauvetage utilisent souvent des étiquettes de protection immédiate (Anonymous
                    2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img4.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img5.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Étiquettes de sensibilisation à la température </Text>instruisent le personnel d'expédition et de manutention sur les
                    exigences de température appropriées pour le colis. Ces étiquettes sont utilisées pour les produits congelés et / ou périssables (Anonymous 2018j).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img6.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 300, height: 200, marginTop: 5}}
                        source={require('../Images/m8/img7.png')}
                        resizeMode="contain"/>
                </View>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Étiquettes de danger </Text>identifient les colis contenant des marchandises dangereuses. Elles alertent le personnel
                    d'expédition et de manutention des besoins spéciaux en matière de stockage et de séparation pendant le transport. Il existe des réglementations strictes pour l'expédition de
                    marchandises dangereuses et une documentation spéciale, telle que des données chimiques, est souvent nécessaire. Les étiquettes de danger doivent être utilisées pour les matières
                    explosives, les gaz, les liquides inflammables, les solides inflammables, les substances spontanément inflammables ou hydro réactives, les substances toxiques et les matières
                    corrosives (Anonymous 2018j).</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Étiquettes de biorisques </Text>identifient les emballages de produits biologiquement dangereux tels que les bio
                    déchets, les spécimens humains ou animaux et l'équipement de laboratoire usagé (Anonymous 2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Étiquettes de manutention spéciales </Text>couvrent toutes les instructions spéciales d'expédition et de manutention.
                    Les étiquettes peuvent être personnalisées avec des instructions spécifiques. Ce sont souvent des étiquettes d'avertissement ou des instructions d'expédition (Anonymous
                    2018j).</Text>
                <Text style={{marginTop: 5}}><Text style={{fontWeight: 'bold'}}>Conseil de conception: </Text>Les étiquettes doivent être apposées sur trois faces de l'emballage, de préférence sur le
                    côté, et / ou aux extrémités et sur le dessus (Anonymous 2018j).
                    {"\n\n"}Si les marchandises nécessitent une manutention ou un stockage particulier, le colis d'expédition doit être marqué de la sorte et cette information doit également figurer
                    sur le bon de chargement (Anonymous 2018j).

                </Text>
            </View>
        )
    }
}
