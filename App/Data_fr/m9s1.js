import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text>Jusqu'à ce module, vous avez appris à créer un compte de messagerie et le configurer sur vos smartphones. En outre, vous avez appris à configurer des comptes de médias sociaux.
                    D'autre part, vous avez appris à prendre des photos de vos produits et à les télécharger sur ces plates-formes. Le but de ce module est d’expliquer comment configurer vos comptes
                    de médias sociaux existants pour votre entreprise.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
