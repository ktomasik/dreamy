import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Slovénie:</Text>
                <Text style={styles.dmlH3}>Étape 1:</Text>
                <Text>Au service de l'emploi de Slovénie, inscrivez-vous en tant que chômeurs. Dites immédiatement à votre conseiller personnel lors de votre première réunion que vous souhaitez une
                    subvention. Le conseiller personnel préparera un plan de recrutement et une déclaration d'inclusion dans le programme d'aide au travail indépendant.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_18_3319626.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
