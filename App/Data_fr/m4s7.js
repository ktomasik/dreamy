import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Enregistrement auprès d'associations professionnelles</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}>Pour la Turquie</Text>
                <FlatList
                    data={[
                        {key: 'KOSGEB (Organisation de développement des petites et moyennes entreprises)'},
                        {key: 'La Direction générale de la condition de la femme (KSGM)'},
                        {key: 'Association des entreprises de Turquie (İŞKUR)'},
                        {key: 'Union turque des chambres et échanges communautaires (TOBB)'},
                        {key: 'Soutien du ministère de la République de Turquie et des politiques sociales'},
                        {key: 'Programme turc de microfinance Grameen'},
                        {key: 'Prêts bancaires pour les femmes'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la SLOVENIE</Text>
                <Text style={{fontWeight: 'bold'}}>Soutien non financier de l'État</Text>
                <FlatList
                    data={[

                        {key: "Points VEM"},
                        {key: "Incubateurs d'entreprises"},
                        {key: "Incubateurs universitaires"},
                        {key: "Parcs technologiques"},
                        {key: "Initiative de démarrage en Slovénie"},
                        {key: "Le réseau des entreprises européennes"},
                        {key: "ESPRIT Slovénie"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Soutien financier en Slovénie</Text>
                <FlatList
                    data={[

                        {key: "Fonds slovène pour les entreprises"},
                        {key: "Service de l'emploi de la Slovénie"},
                        {key: "Fonds slovène de développement régional"},
                        {key: "Prêts bancaires"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Autres supports</Text>
                <FlatList
                    data={[

                        {key: "Chambre de commerce et d'industrie"},
                        {key: "Chambre de l'artisanat et de la petite entreprise de Slovénie"},
                        {key: "Business Angels of Slovenia"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la POLOGNE</Text>
                <FlatList
                    data={[
                        {key: 'European Union Funds for women-entrepreneurs (NGOs, Public Bodies,..etc)'},
                        {key: 'European Social Fund (PO WER-Operational Program Knowledge Education Development)'},
                        {key: 'Non-EU sources (governmental, private, etc)'},
                        {key: 'Polish Agency for Enterprise Development'},
                        {key: 'The Loan Fund for Women'},
                        {key: 'Business Angels'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la GRECE</Text>
                <FlatList
                    data={[

                        {key: "Capital d'amorçage (petit financement pour une population spécifique telle que les jeunes, le chômage)"},
                        {key: "Programme OAED (pour le public)"},
                        {key: "Le fonds ouvert (pour le secteur privé)"},
                        {key: "Prêts bancaires"},
                        {key: "Accord de partenariat (AP) 2014-2020 (ESPA)"},
                        {key: "Communauté grecque à l'étranger"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la FRANC</Text>
                <FlatList
                    data={[

                        {key: "CFE"},
                        {key: "Registre du commerce et des sociétés (RFS)"},
                        {key: "ACCRE- conçu pour aider les demandeurs d’emploi et faciliter la création de leur entreprise."},
                        {key: "La BPI France (Banque publique d'investissement)"},
                        {key: "PRI (Partenariat régional d'innovation)"},
                        {key: "Business Angels"},
                        {key: "Entreprendre au féminin"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
