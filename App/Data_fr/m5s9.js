import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Premier formulaire de commande </Text>
                <Text style={{marginTop: 15}}>Suivez les instructions ci-dessous pour configurer votre premier bon de commande.
                    {"\n"}Comment créer un formulaire de commande simple avec des paiements en ligne.

                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Afin de recevoir des commandes: </Text>: vous allez avoir une idée sur la façon de créer un formulaire de commande WordPress qui
                    acceptera les paiements par carte de crédit et PayPal.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 1: Créer un formulaire de commande simple dans WordPress
                    Installez et activez le plugin WPForms
                </Text>
                <FlatList
                    data={[

                        {key: "Allez à WPForms »Ajouter nouveau pour créer un nouveau formulaire."},
                        {key: "Nommez votre formulaire et sélectionnez le modèle de formulaire Facturation / Commande."},
                        {key: "Faites défiler la liste jusqu’à la section «Articles disponibles» de l’écran d’aperçu de droite et cliquez dessus."},
                        {key: "Cela ouvrira les «Options de champ» dans le panneau de gauche. Ici, vous pouvez renommer le champ, ajouter ou supprimer des articles de commande et modifier les prix."},
                        {key: "Si vous souhaitez donner aux utilisateurs le choix des images lors du remplissage de votre formulaire de commande, cochez la case Utiliser les choix d'image  dans l'éditeur de formulaire."},
                        {key: "Enfin, vous pouvez ajouter des champs supplémentaires à votre formulaire de commande en les faisant glisser de gauche à droite."},
                        {key: "Cliquez sur Enregistrer lorsque vous avez terminé."},


                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 2: Configurez vos notifications de bon de commande</Text>
                <FlatList
                    data={[

                        {key: "Les notifications sont un excellent moyen d’envoyer un courrier électronique lors de la soumission de votre formulaire."},
                        {key: "Vous pouvez envoyer une notification par courrier électronique à vous-même, à un membre de votre équipe en ajoutant son courrier électronique dans le champ Envoyer à l'adresse électronique ou à un client pour l'informer de la réception de sa commande."},
                        {key: "Cliquez sur l'onglet Paramètres dans le Générateur de formulaires, puis sur Notifications."},
                        {key: "Cliquez sur Afficher les balises dans le champ Envoyer à l’adresse."},
                        {key: "Cliquez sur Email"},
                        {key: "Changement Objet e-mail de votre notification pour être plus précis. De plus, vous pouvez personnaliser les emails «De nom», «De courriel» et «Répondre à»."},
                        {key: "Inclure un message personnalisé si l'e-mail va à tout le monde, mais vous-même."},
                        {key: "Utilisez le {tous les champs} balise active, si vous voulez inclure toutes les informations trouvées dans les champs de formulaire du formulaire de commande soumis."},


                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 3: Configurez vos confirmations de bon de commande</Text>
                <Text>Les confirmations de formulaire sont des messages qui s'affichent pour les clients une fois qu'ils ont soumis un formulaire de commande.
                    {"\n\n"}Vous pouvez choisir parmi trois types de confirmation:

                </Text>
                <FlatList
                    data={[
                        {key: "1. Message. Lorsqu'un client envoie un formulaire de commande, un simple message de confirmation apparaît pour l'informer que son formulaire a été traité."},
                        {key: "2. Afficher la page. Ce type de confirmation amène les clients vers une page Web spécifique de votre site, les remerciant de leur commande."},
                        {key: "3. Aller à l'URL (redirection). Cette option est utilisée lorsque vous souhaitez envoyer des clients vers un autre site Web."},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Voyons comment configurer une confirmation de formulaire simple dans WPForms afin de personnaliser le message que les utilisateurs verront après avoir
                    soumis leurs commandes.</Text>
                <FlatList
                    data={[

                        {key: "Cliquez sur l'onglet de confirmation dans l'éditeur de formulaire sous Paramètres."},
                        {key: "Sélectionnez le type de confirmation que vous souhaitez créer."},
                        {key: "Customisez le message de confirmation à votre goût et cliquez sur Enregistrer lorsque vous avez terminé."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 4: Configurer les paramètres de paiement</Text>
                <Text>WPForms s’intègre à PayPal pour accepter les paiements.
                    {"\n\n"}Pour configurer les paramètres de paiement sur votre formulaire de commande, vous devez d'abord installer et activer le bon « addon » de paiement.
                    {"\n\n"}Une fois que vous avez fait cela :

                </Text>
                <FlatList
                    data={[

                        {key: "Cliquez l'onglet Paiements dans l'éditeur de formulaires."},
                        {key: "Cliquez sur PayPal,"},
                        {key: "Entrez votre adresse e-mail PayPal,"},
                        {key: "Sélectionnez le mode Production,"},
                        {key: "Choisissez Produits et Services,"},
                        {key: "Configurez les paramètres de paiement,"},
                        {key: "Cliquez sur Enregistrer pour enregistrer vos modifications."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 5}}>Vous êtes maintenant prêt à ajouter votre formulaire simple de commande sur votre site.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 5: Ajoutez votre formulaire de commande simple sur votre site</Text>
                <FlatList
                    data={[

                        {key: "Créez un nouveau message ou une nouvelle page dans WordPress, puis cliquez sur le bouton Ajouter un formulaire."},
                        {key: "Sélectionnez votre formulaire de commande simple, dans le menu déroulant, puis cliquez sur Ajouter un formulaire."},
                        {key: "Publiez votre message ou page afin que votre formulaire de commande apparaîtra sur votre site."},
                        {key: "Allez à Apparence »Widgets et ajoutez un widget WPForms à votre barre latérale."},
                        {key: "Sélectionnez le formulaire de facturation / commande dans le menu déroulant."},
                        {key: "Cliquez sur Enregistrer"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Vous pouvez maintenant voir votre formulaire de commande publié en direct sur votre site. Notez que lorsque vous sélectionnez des éléments sur votre
                    formulaire, le prix change automatiquement.
                    {"\n\n"}Vous savez maintenant comment créer un simple formulaire de commande dans WordPress qui accepte les paiements en ligne.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', marginTop: 15}}>Si vous souhaitez envoyer des commandes en tant qu'entreprise, vous pouvez utiliser le formulaire de commande de
                    Google Documents.</Text>
                <Text>Google Documents vous permet de créer des formulaires pouvant être utilisés comme formulaire de commande. Une fois les données enregistrées dans votre feuille de calcul dans
                    Google Docs, vous pouvez gérer la facturation ou la commande à partir de cet emplacement.</Text>
                <FlatList
                    data={[

                        {key: '1.	Ouvrez Google Documents et cliquez sur le bouton "Créer". Sélectionnez "Form".'},
                        {key: '2.	Remplissez le nom et la description de votre formulaire de commande.'},
                        {key: '3.	Découpez votre formulaire en sections, le cas échéant.'},
                        {key: '4.	Ajoutez des questions en cliquant sur "Ajouter un élément". .'},
                        {key: '5.	Choisissez un thème pour votre formulaire en cliquant sur le bouton "Thème" en regard du bouton "Ajouter un élément".'},
                        {key: '6.	Cliquez sur le lien au bas de la fenêtre du formulaire pour afficher votre formulaire dans le navigateur.'},
                        {key: '7.	Distribuez votre formulaire.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
