import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu'est-ce que l'enregistrement du nom de marque? Dois-je enregistrer ma marque (nom commercial)?</Text>
                <Text>Le développement et le lancement d'une nouvelle marque nécessitent l'investissement de beaucoup de capital financier, mental et émotionnel et c'est pour cette raison que
                    l’enregistrement <Text
                        style={{fontWeight: 'bold'}}>de la marque</Text>, ou la protection juridique de la nouvelle marque doit constituer une priorité absolue pour toute nouvelle entreprise. Cela
                    s'applique que la nouvelle marque soit une nouvelle société, un nouveau produit ou service ou une nouvelle activité en ligne (Anonymous 2018e).
                    {"\n\n"}L’enregistrement de marque est un autre nom pour l’enregistrement <Text style={{fontWeight: 'bold'}}>de marque déposée </Text> et c’est la seule façon pour un propriétaire
                    de marque d’obtenir des droits exclusifs d’utilisation de la nouvelle marque sur un territoire national donné. Ni la constitution d'une société anonyme, ni l'enregistrement d'un
                    nom de domaine ne fourniront une protection juridique à une nouvelle marque (Forbes Agency Council 2017).
                    {"\n\n"}Sans enregistrement de marque, il n'y a aucun moyen d'empêcher les concurrents ou les «imitateurs» d'utiliser la même marque.
                </Text>
                <Text style={styles.dmlH2}> Comment enregistrer une marque pour un nom de société?</Text>
                <Text>Chaque pays a son propre bureau ou organisme pour enregistrer le nom ou le logo de la marque. En outre, chaque pays a des procédures différentes selon les lois et les
                    réglementations du pays. Pour enregistrer une marque, une société ou une personne liée doit payer des frais d'enregistrement.
                    {"\n\n"}La durée de l'enregistrement d'une marque peut varier, mais est généralement de dix ans. Ce peut être renouvelé indéfiniment moyennant le paiement de frais supplémentaires.
                    Les droits de marque sont des droits privés et la protection est assurée par des décisions de justice.
                </Text>
            </View>
        )
    }
}
