import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1INTRO extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text>Le groupe cible du projet Dreamy m-Learning est constitué de femmes moins scolarisées qui fabriquent de l'artisanat à domicile. L’objectif est de les aider à acquérir des
                    compétences numériques mobiles et entrepreneuriales pour vendre leurs produits d’artisanat sur les marchés numériques en utilisant efficacement les téléphones intelligents. Selon
                    cet objectif, nous allons développer une application mobile, facile à comprendre et à utiliser pour les systèmes d'exploitation Android et Ios, interface Web reliant les
                    applications mobiles au portail m-learning. Dans le programme de formation que nous avons préparé, nous expliquerons aux femmes comment procéder étape par étape avec des
                    applications mobiles gratuites et communes.</Text>
                <Text style={{marginTop: 10}}>Conformément à ce cadre, ce module a pour but d’expliquer comment les femmes sont capables de créer un compte de messagerie et d’installer ce compte sur
                    leurs smartphones (IOS et Android) à la première étape. En outre, ce module sera un outil utile pour les personnes ayant les mêmes antécédents et qui souhaitent apprendre les
                    sujets pertinents.</Text>
            </View>
        )
    }
}
