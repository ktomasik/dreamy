import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";

// Styles

export default class M4S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Licences et permis pour une nouvelle entreprise</Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10, fontSize: 17}}> Pour la Turquie</Text>
                <Text>Si les femmes veulent être exonérées de l'impôt, il faut obligatoirement obtenir le Certificat d'exemption de l'artisanat (Esnaf Vergi Muafiyet Belgesi)
                    {"\n\n"}Si les femmes veulent être des contribuables pour le futur plan en croissance, le système de déclaration électronique est valide. Ils peuvent appliquer ce système sur une
                    plateforme électronique (en ligne). Les archives des documents reçus et remis par les contribuables sont conservées dans les chambres de la profession à laquelle appartiennent les
                    contribuables (National Report Turkey, 2018).
                    {"\n\n"}"‘Certificat d'exemption de l'artisanat’ (Esnaf Vergi Muafiyet Belgesi) (National Report Turkey, 2018);

                </Text>
                <FlatList
                    data={[

                        {key: "1.	Après l'obtention de ce certificat, l'enregistrement doit être effectué auprès d'une association professionnelle affiliée à la Confédération des artisans et des commerçants turcs."},
                        {key: "2.	Pour l'autorisation de travail, une demande à la municipalité avec une pétition doit être faite."},
                        {key: "3.	Pour les entreprises à domicile, tous les propriétaires d'appartements doivent avoir un accord notarial."},
                        {key: "4.	La facture et la déclaration sont obligatoires"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15, fontWeight: 'bold'}}>Contribuables (Rapport national Turquie, 2018):</Text>
                <FlatList
                    data={[

                        {key: "1.	Appliquer le système e-Déclaration"},
                        {key: "2.	Après l'obtention de ce certificat, l'enregistrement doit être effectué auprès d'une association professionnelle affiliée à la Confédération des artisans et des commerçants turcs."},
                        {key: "3.	Pour l'autorisation de travail, une demande à la municipalité avec une pétition doit être faite."},
                        {key: "4.	Pour les entreprises à domicile, tous les propriétaires d'appartements doivent avoir un accord notarial."},
                        {key: "5.	La facture et la déclaration sont obligatoires"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la SLOVENIE</Text>
                <Text>Pour créer une entreprise, il est nécessaire de remplir la demande de registre CEIDG-1. Avec ce formulaire:</Text>
                <FlatList
                    data={[

                        {key: "Obtenir un numéro de TVA est nécessaire,"},
                        {key: "Déclaration sur le choix de la forme d'imposition avec impôt sur le revenu des personnes physiques,"},
                        {key: "Notification de déclaration de contribution à la sécurité sociale (Rapport national Slovénie, 2018)."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>Il y a 3 secteurs qui nécessitent des licences:</Text>
                <FlatList
                    data={[
                        {key: "1.	Immobilier et courtage et gestion immobilière,"},
                        {key: "2.	Effectuer des services de transport routier,"},
                        {key: "3.	Gestion d'un bureau de travail, d'un bureau de travail intérimaire, d'une institution de formation pour chômage sous fonds publics (Rapport national Slovénie, 2018)."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la POLOGNE</Text>
                <Text>Pour créer une entreprise, il est nécessaire de remplir la demande de registre CEIDG-1. Avec ce formulaire:</Text>
                <FlatList
                    data={[

                        {key: "Obtenir un numéro de TVA est nécessaire,"},
                        {key: "Déclaration sur le choix de la forme d'imposition avec impôt sur le revenu des personnes physiques,"},
                        {key: "Notification de déclaration de contribution à la sécurité sociale (Rapport national Pologne, 2018)"}

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10, fontWeight: 'bold'}}>Il y a 3 secteurs qui nécessitent des licences:</Text>
                <FlatList
                    data={[

                        {key: "1.	Immobilier et courtage et gestion immobilière,"},
                        {key: "2.	Effectuer des services de transport routier,"},
                        {key: "3.	Gestion d'un bureau de travail, d'un bureau de travail intérimaire, d'une institution de formation pour chômage sous fonds publics"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la GRECE</Text>
                <Text>Etapes Description:</Text>
                <FlatList
                    data={[

                        {key: "1.	Obtenir l'approbation du nom de l'entreprise auprès de la Chambre de commerce et d'industrie"},
                        {key: "2.	Classer les documents de l'entreprise auprès du barreau d'Athènes"},
                        {key: "3.	Signer les statuts devant un notaire"},
                        {key: "4.	Dépôt de capital dans une banque"},
                        {key: "5.	Payer l'impôt sur le capital à l'administration fiscale"},
                        {key: "6.	Obtenez un timbre de la caisse de retraite des avocats"},
                        {key: "7.	Obtenir une certification par le Fonds de soutien des avocats"},
                        {key: "8.	Soumettre les statuts et s’enregistrer auprès du secrétariat de la Cour pour obtenir un numéro de registre"},
                        {key: "9.	Soumission des statuts de synthèse pour publication au Journal officiel (FEK)"},
                        {key: "10	S’inscrire à la Chambre de commerce et d'industrie"},
                        {key: "11	S’inscrire auprès d'un organisme d'assurance indépendant (OAEE) Organisation de l'assurance agricole (OGA), etc."},
                        {key: "12	Obtenir un numéro d'identification fiscale (AFM) pour l'entreprise"},
                        {key: "13	Commission e vendeur pour faire le timbre / sceau"},
                        {key: "14	Demander à l'administration fiscale de perforer les carnets de réception et le journal comptable de l'entreprise"},
                        {key: "15	Aviser la main-d'œuvre (OAED) dans les 8 jours suivant l'embauche d'un travailleur"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Tous les nouveaux propriétaires d’entreprise devront compléter les étapes précédentes.
                    {"\n\n"}Les travailleurs indépendants doivent suivre les étapes 4 et 10-15.
                    {"\n\n"}Il n'est pas nécessaire d'obtenir une licence ou un permis spécifique pour ouvrir une boutique en ligne dans le commerce de produits artisanaux (Rapport national Grèce,
                    2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la FRANCE</Text>
                <Text>Pour protéger sa marque il faut passer par l’INPI (Rapport national France, 2018).</Text>
                <Text><Text style={{fontWeight: 'bold'}}>Qu'est-ce que l'INPI?</Text>
                    {"\n\n"}"(Institut national de la propriété industrielle) est un organisme public placé sous la tutelle du ministère de l'Économie, des Finances et du Commerce extérieur du
                    ministère de la Production et ministre délégué aux petites et moyennes entreprises, à l'innovation et à l'économie (National Report France, 2018).
                    {"\n\n"}Une fois que le nom est rempli avec INPI, la société a une durée de 10 ans.
                    {"\n\n"}La structure juridique peut varier :

                </Text>
                <FlatList
                    data={[

                        {key: 'Auto-entreprise,'},
                        {key: 'Entreprise individuelle'},
                        {key: 'Entreprise'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Le plus simple, pour créer une entreprise, c’est le statut d’auto entrepreneur : suivez les étapes ci-dessous (National Report France, 2018);</Text>
                <Text>Une simple déclaration (formulaire PO AE) suffit pour devenir micro-entrepreneur.</Text>
                <Text>La déclaration doit être transmise par internet (obligatoirement, depuis le 1er janvier 2016) au Centre de Formalité des Entreprises (CFE) compétent selon l’activité :</Text>

                <FlatList
                    data={[
                        {key: "La Chambre de commerce et d'industrie (CCI) pour une activité commerciale"},
                        {key: "La Chambre des métiers et de l'artisanat (CMA) pour une activité artisanale ou commerciale ET artisanale,"},
                        {key: "L'URSSAF pour une activité libérale"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text>C’est le CFE compétent qui vous répondra et vous indiquera la marche à suivre en cas de dossier à compléter ou de formalités supplémentaires. Au final, vous recevrez:</Text>

                <FlatList
                    data={[
                        {key: "Un numéro Siret"},
                        {key: "Une notification concernant le régime d'imposition auquel vous serez soumis en matière de bénéfices (BIC ou BNC - régime micro-entreprise) et de TVA (franchise en base)"},
                        {key: "Les coordonnées des interlocuteurs fiscaux que vous devrez contacter afin de déclarer et de payer notamment l'impôt sur les bénéfices et la CFE auto-entrepreneur"},
                        {key: "Les coordonnées d'un correspondant dédié pour obtenir des informations sur les exonérations et allègements fiscaux dont vous pouvez bénéficier."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text>L'immatriculation obligatoire au Registre du Commerce et des Sociétés (RCS) pour les auto-entrepreneurs exerçant une activité commerciale, artisanale ou industrielle, est une
                    démarche additionnelle introduite en 2015. Elle vous permettra d'obtenir un KBIS auto-entrepreneur.</Text>
                <Text>En tant qu’auto-entrepreneur, vous êtes soumis d’office au régime « microsocial</Text>

                <Text style={{fontWeight: 'bold'}}>Les aides à la création d’entreprise</Text>
                <Text>En devenant auto-entrepreneur, vous pouvez bénéficier d’un certain nombre d'aides à la création d'entreprise:</Text>

                <FlatList
                    data={[
                        {key: "si vous êtes demandeur d’emploi ou remplissez certains critères (âge, etc.), vous pouvez faire une demande d’ACCRE en auto-entrepreneur. Si vous obtenez l’ACCRE, vous pouvez faire une demande de NACRE, aide au montage de votre projet et au développement de l’entreprise."},
                        {key: "Si vous exercez votre activité depuis les départements d’outre-mer, vous pouvez être exonéré de charges sociales pendant 24 mois pour les professions artisanales et commerciales."},
                        {key: "Des aides de Pôle emploi, comme le maintien partiel des allocations d’assurance chômage, sont également disponibles."},
                        {key: "» et vous bénéficiez de plein droit du régime fiscal de la micro-entreprise"},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

            </View>
        )
    }
}
