import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S3 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2noBorder}>Marketing sur les réseaux sociaux pour les femmes qui démarrent</Text>
                <Text style={styles.dmlH3}>Buts</Text>
                <Text>Alex York (2018) a rédigé un très bon article sur la stratégie des médias sociaux. Il a souligné que le plus important pour conduire votre entreprise est de suivre constamment
                    vos objectifs. Il est même utile de noter mille objectifs pour s’assurer qu’ils sont plus définis et plus réalistes.</Text>
            </View>
        )
    }
}

