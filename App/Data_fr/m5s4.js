import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m5/img1.png')
    }
}];

export default class M5S4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Ce que vous devez savoir avant d’obtenir votre première carte de crédit (par exemple, qu'est-ce qu'un relevé de carte de crédit, comment est calculé
                    l'intérêt de carte de crédit, comment sont déterminés les paiements minimaux)</Text>
                <Text style={{marginTop: 15}}>Un relevé de facturation est un relevé périodique qui répertorie tous les achats, paiements, autres débits et crédits effectués sur votre compte de carte
                    de crédit au cours du cycle de facturation. Votre émetteur de carte de crédit envoie votre relevé de facturation environ une fois par mois.
                    {"\n\n"}Qu'y a-t-il sur le relevé de facturation?
                    {"\n\n"}Votre relevé de facturation répertorie tout ce que vous devez savoir sur votre compte de carte de crédit.
                    {"\n\n"}Il comprend:

                </Text>
                <FlatList
                    data={[
                        {key: "Votre solde du cycle de facturation précédent"},
                        {key: "Le paiement minimum dû"},
                        {key: "La date d'échéance du paiement"},
                        {key: "Les frais de retard qui seront facturés si vous payez en retard"},
                        {key: "Un récapitulatif et une liste détaillée des paiements, crédits, achats, transferts de solde, avances de fonds, frais, intérêts et autres débits versés sur votre compte"},
                        {key: "Une ventilation des types de soldes sur votre compte et le taux d’intérêt et les frais d’intérêt pour chacun"},
                        {key: "Votre limite de crédit et votre crédit disponible"},
                        {key: "Le nombre de jours dans votre période de facturation"},
                        {key: "Montant total des intérêts et des frais payés depuis le début de l'année"},
                        {key: "Coordonnées de votre émetteur de carte de crédit"},
                        {key: "Récompenses gagnées ou échangées, le cas échéant"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 10}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m5/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

                <Text style={{marginTop: 10}}>Votre relevé de carte de crédit inclura une divulgation du paiement minimum détaillant le temps nécessaire au paiement de votre solde si vous effectuez seulement le paiement minimum et le montant total que vous finirez par payer. Il inclura également le paiement mensuel à effectuer si vous souhaitez payer votre solde en trois ans. Cette information est utile pour déterminer le meilleur moyen de payer le solde de votre carte de crédit.
                    {"\n\n"}Votre relevé de facturation par carte de crédit comprendra également un avertissement de retard de paiement qui vous indiquera l’impact de l’envoi de votre paiement en retard - un retard de paiement et une augmentation du taux de pénalité.
                    {"\n\n"}Votre paiement minimum par carte de crédit    est le montant le moins élevé que vous puissiez payer pour le solde de votre carte de crédit sans être pénalisé de frais de retard et d’une éventuelle augmentation du taux d’intérêt. Si vous prêtez attention à votre relevé de facturation chaque mois, vous avez probablement remarqué que votre paiement minimum peut varier d’un mois à l’autre.
                    {"\n\n"}Certains émetteurs de cartes de crédit calculent le paiement minimum en pourcentage du solde, généralement entre 2% et 5%, à la fin du cycle de facturation.
                    {"\n\n"}Votre paiement minimum peut également être calculé en prenant un pourcentage du solde à la fin du cycle de facturation et en ajoutant les frais financiers mensuels.
                    {"\n\n"}Vous pouvez connaître la méthode utilisée par votre émetteur de carte de crédit en lisant votre contrat de carte de crédit. Recherchez une section intitulée "Comment votre paiement minimum est calculé" ou "Effectuer des paiements".

                </Text>
            </View>
        )
    }
}
