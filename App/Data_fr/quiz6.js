import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz6": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Seulement Google play",
                    "option2": "b) Seulement Apple store",
                    "option3": "c) Aucun d'entre eux",
                    "option4": "d) Les deux"
                },
                "question": "Où un modèle de facture pour un téléphone Apple peut-il être téléchargé?"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Apple Store peut être utilisé pour créer une facture sur un téléphone Android."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Fresh books",
                    "option2": "b) Moon Invoice",
                    "option3": "c) Vague",
                    "option4": "d) Pinterest"
                },
                "question": "Quelle application permet de créer une facture sur Apple et sur Android?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Mettre un logo sur une facture n'est pas absolument nécessaire, mais il est préférable de l'utiliser."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Nom et titre",
                    "option2": "b) Coordonnées, adresse, téléphone, etc.",
                    "option3": "c) Taxe du matériel",
                    "option4": "d) Tous"
                },
                "question": "Quelles informations doivent figurer sur une facture?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) L'identité du commerçant",
                    "option2": "b) Compte de médias sociaux",
                    "option3": "c) Détails de la TVA",
                    "options": "d) Détails du contact"
                },
                "question": "Qu'est-ce qui n'est pas nécessaire parmi les éléments suivants d'un contrat d'achat en ligne?"
            },
            "question7": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Le vendeur ne peut être tenu responsable des produits vendus sous le nom de son entreprise."
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Il n'est pas utile d’avoir une assurance pour le matériel commercial."
            },
            "question9": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Un endroit pour classer les documents",
                    "option2": "b) Une plate-forme gratuite et pratique pour la signature électronique à utiliser sur les téléphones mobiles",
                    "option3": "c) Un document à signer à chaque vente",
                    "option4": "d) Aucun d'entre eux"
                },
                "question": "Qu'est-ce que DocuSign?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Caisse, Cash board",
                    "option2": "b) DocuSign",
                    "option3": "c) DropBox",
                    "option4": "d) Pinterest"
                },
                "question": "_______ offre un modèle téléchargeable que vous pouvez ouvrir dans Microsoft Word."
            }
        }
    }
};

export default class M6Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz6;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>

                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
