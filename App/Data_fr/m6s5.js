import React, {Component} from 'react'
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M6S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Création de contrats de relations acheteur-fournisseur pour les achats sur Internet</Text>
                <Text style={{marginTop: 15}}>Les achats sur Internet étant différents des achats traditionnels, les transactions effectuées via Internet sont soumises au règlement sur les contrats à
                    distance. Ce contrat est conclu entre un commerçant et un consommateur lorsqu'ils ne sont pas ensemble, ce qui est négocié et convenu par un ou plusieurs moyens de communication
                    organisés à distance, par exemple par téléphone, par la poste ou par Internet.</Text>
                <View style={styles2.border}>
                    <Text>Pour la plupart, les principes généraux dans la formation d'une relation contractuelle concernant les achats en ligne peuvent être appliqués après les actions générales
                        suivantes entre un vendeur et l'acheteur:</Text>
                    <FlatList
                        data={[

                            {key: "invitation à traiter: l'affichage de produits en vente sur un site de commerce électronique est généralement considéré comme une invitation à traiter, plutôt qu'une offre, qui est en vain semblable à un magasin traditionnel de brique et de mortier. L'offre est plutôt remplie par les actions du client visitant le site plutôt que par le vendeur"},
                            {key: "le client communique son offre: comme la publicité de biens ou de services sur un site Web n’est pas une offre, il incombe alors à l’acheteur de faire une offre et le client communiquera une offre par voie électronique - ce qui peut être fait sur le site Web - offre d'achat du produit annoncé"},
                            {key: "le vendeur accepte l'offre: une fois qu'une offre a été faite par un client, il appartient ensuite au vendeur d'accepter l'offre de manière non équivoque et inconditionnelle en communiquant son acceptation au client."},

                        ]}
                        renderItem={({item}) => <Text
                            style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                    />
                    <Text style={{marginTop: 15}}>Dès l'acceptation par le vendeur de l'offre d'un client, une relation contractuelle est désormais établie</Text>
                    <Text><Text style={{fontWeight: 'bold'}}>Source: </Text><Text style={{color: 'blue'}}
                                                                                  onPress={() => Linking.openURL('http://www.findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f.aspx')}>findlaw.com.au/articles/4500/internet-shopping-how-a-contractual-agreement-is-f</Text></Text>
                </View>
                <Text style={{marginTop: 15}}>Avant l'établissement d'un contrat entre le consommateur et le vendeur sur Internet, le service ou le fournisseur de biens doit informer le consommateur
                    sur les points soulevés dans le règlement. Certaines des informations que le vendeur devrait fournir au consommateur dans le cadre du règlement avant l'établissement du contrat
                    sont les suivantes:</Text>
                <FlatList
                    data={[

                        {key: "nom, titre, adresse, numéro de téléphone et autres informations d'accès du fournisseur ou du fournisseur,"},
                        {key: "les caractéristiques de base des biens ou des services faisant l'objet du contrat,"},
                        {key: "le prix de vente des biens ou des services, toutes taxes comprises,"},
                        {key: "s'il y a des frais de livraison,"},
                        {key: "informations sur le paiement et la livraison ou l'exécution;"},
                        {key: "conditions pour l'exercice du droit de rétractation."},

                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Ces informations préliminaires doivent être confirmées par le consommateur avant que l'achat soit effectué en ligne, sinon la relation contractuelle n'est
                    pas établie.</Text>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
