import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Problème de formation de contrat pour le commerce électronique</Text>
                <Text style={{marginTop: 10}}>Pour tous les pays, les principales étapes sont décrites ci-dessous:</Text>
                <FlatList
                    data={[

                        {key: "Étape 1: Établissement de la procédure d'offre et d'acceptation"},
                        {key: "Étape 2: Remplir le formulaire de commande"},
                        {key: "Étape 3: Intégration des termes et conditions"},
                        {key: "Étape 4: Intégration des termes et conditions"},
                        {key: "Étape 5: accusé de réception de la commande"},
                        {key: "Étape 6: Confirmation des informations fournies et droit d’annulation"},
                        {key: "Étape 7: Livraison"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la Turquie</Text>
                <Text>La réglementation sur le commerce électronique exige que tous les sites Web commerciaux mettent les informations suivantes à la disposition directe et permanente des
                    consommateurs via le site Web:</Text>
                <FlatList
                    data={[

                        {key: "le nom de la société, son adresse postale (et l'adresse du siège si celle-ci est différente) et son adresse électronique;"},
                        {key: "le numéro d'enregistrement de l'entreprise;"},
                        {key: "toute adhésion à une association professionnelle ou professionnelle;"},
                        {key: "le numéro de TVA de l'entreprise."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Toutes ces données doivent être incluses, que le site vende en ligne ou non. En outre, toute communication commerciale telle qu'un courrier électronique
                    ou un service de SMS utilisé pour fournir un "service de la société de l'information" doit afficher cette information.
                    {"\n\n"}La réglementation sur le commerce électronique exige également que tous les prix soient clairs et les sites Web doivent indiquer si les prix incluent les taxes et les frais
                    de
                    livraison (Rapport national Turquie, 2018).

                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la SLOVENIE</Text>
                <Text>Inscrivez la société auprès de l'administration des paiements publics de la République de Slovénie.
                    {"\n\n"}Obtenez un accès à l’utilisation du portail pour l’émission de factures. (Avant l'obtention d'un certificat numérique).
                    {"\n\n"}Le site Web de l'Administration des paiements publics de la République de Slovénie est accessible (Rapport national Slovénie, 2018)

                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la POLOGNE</Text>
                <Text>Depuis 2018, chaque entrepreneur doit préparer la TVA.
                    {"\n\n"}Par conséquent, l'entrepreneur doit préparer la facture (la préparation de la facture électronique est donnée non seulement par les programmes informatiques, mais également
                    par certaines banques via des comptes bancaires).
                    {"\n\n"}Tous les documents requis relatifs à la sécurité sociale des entrepreneurs doivent être consultés, sous forme électronique, via un programme spécifique préparé par SII
                    (Institution d'assurance sociale appelée PLATNIK) (Rapport national Pologne, 2018)

                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la GRECE</Text>
                <Text>La facturation électronique a été partiellement introduite en Grèce en 2006.
                    {"\n\n"}Mais le système électronique ne fonctionne toujours pas pleinement et sera achevé jusqu'à la fin de 2019. Dès lors qu’une facture est émise, elle permet au système de
                    comptabilité du client d’accepter les frais en temps réel, tout en permettant aux autorités fiscales de collecter la taxe (Rapport national Grèce, 2018).

                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la FRANC</Text>
                <Text>Les parties commerciales qui collectent des informations personnelles (nom, email, ..) et constituent des fichiers de clients et de prospects, doivent faire une déclaration
                    simplifiée à la Commission nationale de l'informatique et des libertés.
                    {"\n\n"}Les sites de commerce en ligne relèvent généralement des normes simplifiées 48 (Rapport national France, 2018).
                </Text>

                <Text>Doivent figurer les éléments suivants:</Text>
                <FlatList
                    data={[

                        {key: "le nom de la société, son adresse postale (et l'adresse du siège si celle-ci est différente) et son adresse électronique;"},
                        {key: "les numéros d'enregistrement de l'entreprise; SIRET, SIREN, RCS, APE"},
                        {key: "le capital (si SAS, SARL…)"},
                        {key: "le numéro de TVA  (si assujetti) de l'entreprise"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
