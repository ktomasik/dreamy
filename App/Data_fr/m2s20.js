import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S20 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer un compte Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 7: Créez un compte avec un numéro de téléphone ou une adresse électronique. Entrez votre numéro de téléphone ou adresse e-mail et
                    appuyez sur suivant.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img34.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img35.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m2/img36.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
