import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S56 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utiliser les sites de réseaux sociaux</Text>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour configurer un blog</Text>
                <Text style={{marginTop: 10}}>Avant de pouvoir commencer à bloguer, vous devez sélectionner une plate-forme sur laquelle publier votre site. Plusieurs options gratuites hébergées existent, telles que le très populaire WordPress et Blogger. Ces deux plates-formes ont des applications qui permettront aux utilisateurs de composer, éditer et publier leurs messages lors de leurs déplacements.
                    {"\n\n"}La principale différence entre Blogger et WordPress réside dans le fait que si Blogger est un peu plus simple à configurer et à utiliser, WordPress est plus facile à personnaliser et éventuellement à passer à un site auto-hébergé lorsque vous dépassez les limites de leurs offres gratuites. Quel que soit votre choix, des applications officielles sont disponibles pour les principales plates-formes mobiles (Bozzo 2014).
                    {"\n\n"}Dans ce didacticiel, nous utiliserons Blogger pour des raisons de simplicité…
                </Text>
            </View>
        )
    }
}
