import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Grèce:</Text>

                <Text style={styles.dmlH3}>Étape 2:</Text>
                <Text>Une première réunion suivra afin de nous donner un aperçu plus détaillé du business plan. L'objectif de la réunion est de confirmer le respect des critères d'évaluation et
                    l'identification des besoins de financement, ainsi que de l'instrument financier approprié pour les satisfaire.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
