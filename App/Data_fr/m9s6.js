import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Twitter pour les entreprises</Text>
                <Text styl={{marginTop: 10}}>L'une des plateformes de médias sociaux les plus largement utilisées; vous pouvez donner votre voix à vos acheteurs directement via Twitter. Les abonnés
                    Twitter sont vos clients potentiels. Vous devez créer vos propres abonnés pour voir les avantages de Twitter en tant qu'outil de marketing. Vous pouvez créer votre propre liste
                    Twitter ou rejoindre un groupe déjà créé.
                    {"\n\n"}Pour ouvrir un compte professionnel Twitter, vous devez d’abord disposer d’un compte Twitter. Comme décrit dans le module 2, allez à la recherche Google; écrire «twitter»
                    et rechercher; Accédez au site Web de Twitter en choisissant «mobile.twitter.com». Cliquez sur «Inscrivez-vous» pour twitter (pour plus de détails, reportez-vous au module 2).

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/twitter.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
