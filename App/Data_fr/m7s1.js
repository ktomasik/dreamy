import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Introduction</Text>
                <Text>Ce module vise à expliquer les différentes opportunités financières et les différentes manières pour les femmes qui souhaitent vendre leur artisanat. Le module fournit des
                    informations et des liens pour les femmes souhaitant un soutien financier et des facilités de crédit pour cinq pays (Slovénie, France, Turquie, Grèce et Pologne) dans le projet
                    Dreamy m-learning. </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_48_3319639.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
