import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration d'un compte de messagerie sur un téléphone mobile pour une application de messagerie Android (Samsung, Sony, HTC...)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 5 : entrez votre mot de passe et appuyez sur suivant</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/514491-PIISA8-993.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
