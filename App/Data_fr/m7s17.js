import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S17 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Incitations fiscales pour Start-up de femmes</Text>
                <Text style={styles.dmlH2}>Pour la Slovénie: </Text>
                <Text>Exemption partielle du versement de cotisations jusqu'à deux ans après la première entrée. - Pour 2 ans - qui ouvre s. p. pour la première fois et est basé sur l'assurance
                    sociale incluse pour les travailleurs indépendants (uniquement les personnes incluses dans l'assurance pension et invalidité sur la base des travailleurs indépendants).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_41_3319603.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
