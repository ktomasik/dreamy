import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S16 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Petites bourses entreprises pour femmes </Text>
                <Text style={styles.dmlH2}>Pour la Pologne:</Text>

                <Text style={styles.dmlH3}>Fonds de l'UE</Text>

                <FlatList
                    data={[
                        {
                            key:
                                {
                                    val1: "Le site d'information commun sur les fonds de l'UE en Pologne. Sur ce site, chaque entrepreneur ou futur entrepreneur (mais aussi les ONG, les organismes publics, etc.) peut trouver des informations intéressantes sur les financements disponibles.",
                                    val2: 'https://www.funduszeeuropejskie.gov.pl/'
                                }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Agence polonaise pour le développement des entreprises</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "En Pologne, l’Agence polonaise pour le développement des entreprises offre un fonds de prêt destiné aux femmes, qui vise à soutenir l’activation professionnelle des femmes et à améliorer leur situation sur le marché du travail en encourageant les femmes à créer leur propre entreprise. Le Fonds de prêts pour les femmes devrait contribuer à réduire le problème du chômage parmi ce groupe. Les femmes peuvent solliciter un prêt préférentiel d’un montant compris entre 5 et 10 000 € (20 et 40 000 PLN).",
                                val2: 'http://en.parp.gov.pl/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
