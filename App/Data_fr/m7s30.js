import React, {Component} from 'react'
import {Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la France:</Text>
                <Text style={styles.dmlH3}>Si vous n'êtes pas un demandeur d'emploi: L'AIDE DE L'ETAT - ACCRE
                </Text>

                <Text style={styles.dmlH3}>Étape 1:</Text>
                <Text>Votre demande doit être déposée auprès du centre de formalités des entreprises (CFE) compétent au moment de la création ou de la reprise de votre entreprise ou dans les 45 jours
                    suivant celle-ci. Vous devez joindre à votre demande:</Text>
                <Text>- Le formulaire de déclaration de la société auprès de la CFE ou sa copie,</Text>
                <Text>- Le formulaire spécifique du formulaire de demande d'aide, qui est valable sur l’honneur de ne pas bénéficier de cette aide depuis 3 ans.</Text>
                <Text>- Preuve que vous appartenez à l’une des catégories bénéficiant de l’ACCRE.</Text>
                <Text>D'autres documents peuvent être demandés par votre CFE, je vous conseille de les contacter.</Text>

            </View>
        )
    }
}
