import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M8S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Options d'expédition du client </Text>
                <Text style={{marginTop: 10}}>Les expéditions et les livraisons constituent un important moteur de vente pour le commerce de détail, car les clients s'attendent à ce qu'ils aient accès
                    aux articles rapidement et facilement. Certaines des options d'expédition du client sont indiquées ci-dessous (Ufford 2018). </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Livraison le jour même </Text>
                <Text>La livraison le jour même est un service qui consiste à commander en ligne et à recevoir la livraison le jour même, ce qui est une sorte de service express. (Ufford
                    2018). </Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Enlèvement en magasin</Text>
                <Text>L'enlèvement en magasin permet aux clients d'acheter des articles en ligne, de les commander et de les récupérer dans un délai déterminé dans un magasin ou lieu local (Ufford
                    2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Expédier du magasin</Text>
                <Text>L'expédition à partir du magasin est un service permettant aux détaillants de transformer leurs sites hors ligne en centres de distribution adaptés aux acheteurs en magasin et
                    aux acheteurs en ligne (Ufford 2018).</Text>
                <Text style={{marginTop: 5, fontWeight: 'bold'}}>Livraison programmée </Text>
                <Text>La livraison programmée est un service ou les sociétés de transport offrent aux clients la possibilité de programmer leurs livraisons dans un créneau donnée moyennant un petit
                    supplément ou gratuitement s'ils sont membres de leur programme de fidélité. (Ufford 2018).</Text>

                <Text style={styles.dmlH3}>Comment facturer les clients</Text>
                <Text>Un des points stressants au début des ventes est de facturer aux clients. À l'exception des frais du produit pendant la période de production, il peut y avoir d'autres coûts
                    après la production à considérer pour attirer l'attention des clients. Les coûts d'expédition sont évalués par dans cette option.
                    {"\n\n"}Pour attirer l'attention sur la réduction du prix du produit, les coûts du transporteur peuvent être indiqués séparément. Pendant les ventes, le prix sans les frais
                    d’expédition peut être indiqué là où les coûts de transfert seront à la charge du client. Dans ce point, la préférence pour le transporteur et les conditions peuvent être définies
                    par le client et, en fonction des besoins du client, l'expédition sera traitée. D'autre part, le prix du produit et les frais d'expédition définis par la compagnie de transport
                    peuvent être indiqués séparément.
                    {"\n\n"}L'imputation des coûts d'expédition sur le prix de vente peut générer des prix concurrentiels pour les produits qui attirent l'attention des clients.

                </Text>

                <Text style={styles.dmlH3}>Offrir la livraison gratuite</Text>
                <Text>Offrir la livraison gratuite (généralement uniquement sur les commandes nationales) est une méthode éprouvée pour attirer l'attention des clients et augmenter le nombre de vente.
                    Mais, selon les marges, les bénéfices peuvent être considérablement réduits (Anonymous 2018b).
                    {"\n\n"}Si la livraison gratuite est offerte, quelle que soit la capacité, quelle que soit la quantité offerte, il convient de prendre en compte le coût réel de l'expédition des
                    produits, la manière dont les concurrents gèrent l'expédition et la marge bénéficiaire. Cela vous aidera à faire le bon choix quant à l’offre ou non quant à la livraison gratuite.
                    Une autre option consiste à définir des totaux de commande minimum supérieurs à la valeur moyenne de la commande ou un nombre minimum de produits par commande (Anonymous 2018b).
                    {"\n\n"}Certains des avantages de la livraison gratuite sont de rester compétitifs ou de saper les options de livraison des concurrents, d’améliorer la transaction, d’augmenter la
                    valeur moyenne des commandes et de réduire le nombre de paniers abandonnés. D'autre part, une augmentation des prix pour couvrir les frais d'expédition devrait être considérée
                    comme comparable à celle de concurrents sur le marché (Anonymous 2018b).

                </Text>
            </View>
        )
    }
}
