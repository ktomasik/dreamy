import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img4.png')
    }
}];

export default class M9S3_7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Créer une page Facebook pour les entreprises </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Étape 6:</Text> Ajouter des images: Téléchargez le profil et couvrez les images pour votre page Facebook.
                    De plus, vous pouvez télécharger le logo de votre entreprise. Si vous n'avez pas de logo, mettez une photo de l'une de vos créations récentes et envisagez de la mettre à jour. Il
                    est important de créer une bonne première impression visuelle (voir cette rubrique module 3).
                    {"\n\n"}D'autre part, la taille de l'image est également importante. La dimension photo de couverture Facebook La partie centrale 563 x 315 pixels de l’image apparaît sur les
                    appareils mobiles .

                </Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img4.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
