import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration des comptes médias sociaux pour les entreprises</Text>
                <Text styl={{marginTop: 10}}>Avec le nombre croissant d'utilisateurs chaque jour, les médias sociaux font désormais partie intégrante du paysage marketing actuel. Ce qui suit est un
                    guide pas à pas qui vous permet de configurer vos comptes de médias sociaux pour votre entreprise..</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522777-PIXIV9-844.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
