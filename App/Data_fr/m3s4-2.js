import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S4_2 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour prendre des photos des produits et les télécharger sur un site Web</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>Étape 2: Prenez la photo de votre produit</Text>
                <View style={{alignItems: 'center'}}>
                <Image
                    style={{flex: 1, width: 300, height:200}}
                    source={require('../Images/m3/img18.png')}
                    resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
