import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";
// Styles

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m4/img1.png')
    }
}];

export default class M4S2 extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Quelle est la différence entre un nom d’entreprise, un nom commercial et un nom légal?</Text>
                <Text>Un nom d’entreprise est le nom officiel de la personne ou de l'entité à laquelle appartient la société.
                    {"\n\n"}C'est le nom légal de l'entreprise. Un nom commercial est utilisé sur les formulaires et les applications du gouvernement (Cameron 2017).

                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>À titre d'exemple: </Text> le nom de l’entrepreneur est John Smith et il possède une entreprise d'assurance. Le nom légal de l'entreprise
                    peut être Assurance John Smith (Anonymous 2018b).
                    {"\n\n"}Les propriétaires d'entreprise peuvent utiliser un nom commercial à des fins publicitaires et de vente. Le nom commercial est le nom que le public voit, comme sur les
                    panneaux et Internet (Cameron 2017).
                    {"\n\n"}Le nom de l’entreprise et le nom commercial peuvent être différents. Un nom commercial n'a pas besoin d'inclure LLC, Corp ou toute autre terminaison légale utilisée pour
                    une entité fiscale (Cameron 2017).

                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>À titre d'exemple: </Text> McDonald's est un nom commercial. La dénomination sociale de la société est McDonald's Corporation (Anonymous
                    2018c).
                </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m4/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 10}}>
                    Un nom légal d'une entreprise est le nom de la personne ou de l'entité propriétaire d'une entreprise. Si l’entreprise est une société de personnes, le nom légal est le nom donné
                    dans un accord de partenariat ou les noms de famille des partenaires.
                    {"\n\n"}Pour les sociétés à responsabilité limitée (SARL) et les sociétés, le nom légal de l'entreprise est celui qui a été enregistré auprès du gouvernement de l'État. Ces noms
                    auront souvent une «fin légale» telle que LLC, Inc. ou LLP (Fishman 2015).

                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', margin: 10}}>Quand utiliser un nom légal ou un nom commercial?</Text>
                <Text>Par exemple, le nom légal de l'entreprise doit être utilisé lors du dépôt des déclarations de revenus, de l'achat d'une propriété ou de la rédaction de chèques.
                    {"\n\n"}Une entreprise peut utiliser un nom commercial à des fins publicitaires et commerciales. C'est souvent le nom que le grand public voit sur les panneaux, Internet et les publicités
                    (Anonymous 2018d).
                    Fondamentalement,
                    {"\n\n"}Le nom légal est pour les procédures gouvernementales,
                    {"\n\n"}Le nom commercial est pour les relations publiques.

                </Text>
            </View>
        )
    }
}
