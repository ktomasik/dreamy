import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S23 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Micro finance et autres Fonds Start-Up pour femmes</Text>
                <Text style={styles.dmlH2}>Pour la Turquie:</Text>
                <Text style={styles.dmlH3}>Kredi Garanti Fonu (Fonds de garantie de crédit)</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "KGF est une société anonyme à but non lucratif qui joue le rôle de garant pour les PME et les entreprises autres que les PME qui ne peuvent pas obtenir un prêt en raison d’une garantie insuffisante. Ainsi, KGF aide les PME et les autres entreprises à accéder au financement. Le prêt aide les femmes entrepreneurs à développer leur entreprise ou à les soustraire à une situation difficile. ",
                                val2: 'http://www.kgf.com.tr/index.php/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Programme Turc de microfinance Grameen</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Le programme Turc de microfinance Grameen (TGMP) est une fondation économique à but non lucratif. Au lieu des dons traditionnels et de la «charité», TGMP propose des services de «microcrédit» pour aider à réduire la pauvreté en Turquie. Le système de microcrédit vise à aider les femmes à faible revenu à s’engager dans des activités génératrices de revenu durables et à contribuer au budget de leur famille. Contrairement au secteur bancaire (commercial) formel, les microcrédits sont proposés sans garantie ni autre document que la carte d'identité nationale Turque\n" +
                                "Certains produits de microcrédit sont énumérés ci-dessous;\n",
                                val2: 'http://www.tgmp.net/tr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Prêt de base</Text>
                <Text>Le prêt de base est le premier type de prêt pour les anciens et les nouveaux membres. Les nouveaux membres peuvent obtenir un prêt allant de 100 à 1 000 TL et le remboursement du
                    prêt est effectué pendant 46 semaines. </Text>

                <Text style={styles.dmlH3}>Prêt Entrepreneurial</Text>
                <Text>Un type de prêt, pour les entrepreneurs et les membres qui ont réussi, on peut se voir accorder un prêt de 1 000 à 5 000 TL et le remboursement du prêt s’effectue au cours de la période de 46 semaines. </Text>

                <Text style={styles.dmlH3}>Prêt pour fracture numérique</Text>
                <Text>En plus des prêts reçus par nos membres, ce type de prêt vise à fournir un développement technologique à leurs membres. Grâce à ce crédit, les membres peuvent utiliser leurs téléphones intelligents avec la technologie actuelle. Les remboursements de prêt sont effectués pendant 46 semaines.</Text>
            </View>
        )
    }
}
