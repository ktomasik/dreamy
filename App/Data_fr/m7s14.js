import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Petites bourses entreprises pour femmes </Text>
                <Text style={styles.dmlH2}>Pour la Grèce:</Text>
                <Text style={styles.dmlH3}>Capital d'amorçage </Text>
                <FlatList
                    data={[
                        {
                            key:
                                {
                                    val1: "La communauté grecque à l'étranger a également été active et a créé de nouvelles actions, telles que le prix Envolve Award Greece , qui concerne un prêt sans intérêt d'un montant maximal de 500 000 euros, avec un remboursement dans les cinq ans.",
                                    val2: 'https://envolveglobal.org/el/envolve-awards/envolve-greece'
                                }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Capital d'amorçage </Text>
                <Text>Le capital d’amorçage est un petit financement pour le démarrage d’une entreprise généralement attribué à des catégories de population spécifiques, telles que les jeunes ou les
                    chômeurs. Les principales caractéristiques de ces actions sont que le capital est assez petit (15 à 50 milliers d’euros), est payé à l’avance et est destiné à couvrir les dépenses
                    d’exploitation de la première année d’exploitation de la société afin de lui donner le temps de développer ses activités. À titre indicatif, il existe des actions à la fois du
                    public ( programme OΑED ) et du secteur privé ( TheOpenFund ), pour divers secteurs de l’économie (produits traditionnels, technologies de l’information, etc.). Le processus est
                    assez simple et la soumission peut être effectuée par toute personne intéressée. Cependant, ces actions ne sont pas ouvertes toute l'année, alors qu'il existe des conditions
                    préalables telles que des séminaires et des cartes de chômage pour les programmes publics (OAED).{"\n"}</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: '',
                                val2: 'http://www.digitalplan.gov.gr/portal/resource/Prosklhsh-Ypobolhs-Protasewn-sta-Ergaleia-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Sporas-Seed-ICT-Fund-kai-Kefalaio-Epiheirhmatikwn-Symmetohwn-sto-stadio-Ekkinhshs-Early-Stage-ICT-Fund-gia-epiheirhseis-ston-klado-twn-Tehnologiwn-Plhroforikhs-kai-Epikoinwniwn-ICT-ths-Prwtoboylias-JEREMIE'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Accord de partenariat (PA) 2014-2020 (ESPA) </Text>
                <Text>ESPA est le programme grec qui utilise des fonds du programme de l'Union européenne pour l'élimination des inégalités entre les régions de l'UE. Dans le cadre de l'ESPA, le
                    gouvernement distribue des fonds destinés au commerce, à la transformation ou à la production primaire. Les candidatures sont présentées dans des délais déterminés par les
                    ministères compétents et le secrétariat général de l'ESPA. L'évaluation des propositions est faite par des évaluateurs indépendants, les résultats sont publiés, puis une période de
                    un à trois ans est donnée pour la mise en œuvre de chaque action. Le financement peut correspondre à un pourcentage de l'investissement ou à une réduction fiscale (généralement
                    entre 40 et 60%). </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Les documents de dépenses sont requis et ils sont examinés lors des contrôles sur place.\n" +
                                "Le grand avantage d’ESPA est sa disponibilité, mais comme le financement dépend des coûts, il n’est pas particulièrement utile pour les nouvelles entreprises. Cependant, cette option est particulièrement intéressante à combiner avec d’autres formes de financement (prêts bancaires, capital-risque, etc.).\n",
                                val2: 'https://www.espa.gr/en/pages/staticPartnershipAgreement.aspx'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}} onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text>Le programme ESPA 2014-2020 finance, entre autres, les initiatives suivantes:</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Start-ups et nouvel entrepreneuriat', val2: 'https://www.efepae.gr/frontend/articles.php?cid=496&t=Neofuis-Epixeirimatikotita'}},
                        {
                            key: {
                                val1: 'Mise à niveau des micros et petites entreprises pour développer leurs compétences sur de nouveaux marchés',
                                val2: 'https://www.efepae.gr/frontend/articles.php?cid=497&t=Anabathmisi-polu-mikrwn-&-mikrwn-epixeirisewn-gia-tin-anaptuksi-twn-ikanotitwn-tous-stis-nees-agores'
                            }
                        },
                        {key: {val1: 'Faire des affaires à bord', val2: 'https://www.efepae.gr/frontend/articles.php?cid=539&t=Epixeiroume-Eksw'}}
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
