import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer un compte Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Step 10: Type in the password and remember it. Then click OK
                    button.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img19.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
