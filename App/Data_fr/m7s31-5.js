import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S31_5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Pologne:</Text>

                <Text style={styles.dmlH3}>Étape 5:</Text>
                <Text>Attendez que la demande reçoive une note positive (cela peut prendre jusqu'à 30 jours).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_22_3319622.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
