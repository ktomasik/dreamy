import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu'est-ce que l’e-business et le commerce électronique?</Text>
                <Text>Le commerce électronique (e-business) fait référence à l'utilisation du Web, Internet, intranets, extranets ou des combinaisons pour faire du business.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img1.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img2.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}> Comment et où vendre vos articles faits à la main en ligne?</Text>
                <Text>Internet est un lieu de marketing unique pour promouvoir et vendre des produits artisanaux à des clients du monde entier. </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img3.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH2}>Les meilleurs sites pour vendre de l'artisanat </Text>
                <Text style={styles.dmlH3}>Etsy</Text>
                <Text>Etsy (<Text style={{color: 'blue'}}
                                  onPress={() => Linking.openURL('https://www.etsy.com/')}>etsy.com</Text>)est une communauté dynamique de 30 millions d’entreprises de création
                    créées sur le site. Une large gamme de produits est vendue sur Etsy, notamment du matériel d'artiste, des produits artisanaux et des pièces. </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img4.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Amazon</Text>
                <Text>Les achats en ligne de la plus grande sélection de livres, magazines, musique, DVD, vidéos, électronique, ordinateurs, logiciels, vêtements et accessoires , chaussures et une
                    bonne plate - forme pour commercialiser les produits. (<Text style={{color: 'blue'}}
                                                                                 onPress={() => Linking.openURL('https://www.amazon.com/')}>amazon.com</Text>)</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img5.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>Bonanza</Text>
                <Text>Bonanza (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.bonanza.com/')}>bonanza.com</Text>) offre la navigation la plus simple et un site attrayant qui
                    vous permettra de vendre vos articles faits main aux consommateurs. En quelques clics, créez facilement votre profil, téléchargez des listes et commencez à vendre. </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img6.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>eBay</Text>
                <Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.ebay.com/')}>eBay</Text> est l'un de ces sites qui a littéralement changé le monde du commerce
                    électronique. Avec plus de 100 millions d'acheteurs, eBay est l'un des marchés en ligne les plus reconnus au monde. </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img7.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>ArtFire</Text>
                <Text>ArtFire (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.artfire.com/')}>artfire.com</Text>) est une place de marché en ligne en pleine croissance pour
                    la vente de produits artisanaux conçus par des artisans du monde entier. Avec plus de 30 000 vendeurs actifs inscrits, ArFire offre une plate-forme conviviale qui permet également
                    des options de personnalisation.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img8.png')}
                    resizeMode="contain"/>
                <Text style={styles.dmlH3}>DaWanda</Text>
                <Text>DaWanda (<Text style={{color: 'blue'}} onPress={() => Linking.openURL('http://en.dawanda.com/')}>dawanda.com</Text>) est l'un des principaux marchés en ligne pour la vente
                    d'objets uniques et faits à la main. Avec plus de 280 000 vendeurs actifs sur Da Wanda, c'est une approche noble pour attirer les acheteurs. </Text>
                <Text style={styles.dmlH3}>Zibbet</Text>
                <Text><Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.zibbet.com/')}>Zibbet</Text> est une place de marché en ligne permettant de vendre des objets
                    d'artisanat en ligne, des objets d'art et de photographies aux fournisseurs d'artisanat. </Text>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img9.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
