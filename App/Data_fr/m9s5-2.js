import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img8.png')
    }
}, {
    url: '',
    props: {
        source: require('../Images/m9/img9.png')
    }
}];

export default class M9S5_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Create a Compte d'entreprise Instagram</Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Étape 1:</Text> Inscrivez-vous avec votre adresse électronique ou votre numéro de téléphone, puis entrez
                    un nom d'utilisateur.</Text>
                <Text style={{textAlign: 'center', color: 'red'}}>OU</Text>
                <Text>Si vous avez un compte Facebook, vous pouvez vous connecter avec les mêmes informations et lier les comptes. (Comment ouvrir le compte Facebook a déjà été décrit dans le module
                    1)</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img8.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Appuyez sur Inscription, puis entrez votre adresse email et appuyez sur Suivant, ou appuyez sur Connexion avec Facebook pour vous inscrire avec votre
                    compte Facebook.</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
