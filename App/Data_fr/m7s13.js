import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Petites bourses entreprises pour femmes </Text>
                <Text style={styles.dmlH2}>Pour la Turquie:</Text>
                <Text style={styles.dmlH3}>KOSGEB</Text>

                <FlatList
                    data={[
                        {
                            key:
                                {
                                    val1: "La KOSGEB (Organisation pour le développement des petites et moyennes entreprises) est le principal organe responsable du développement, de la coordination et de la mise en œuvre de la politique en matière de PME. KOSGEB offre un prêt gouvernemental aux femmes entrepreneurs qui souhaitent créer leur propre entreprise. Dans ce cadre, un soutien au crédit de 50 000 livres turques (10 000 euros) est fourni dans le cadre de la demande de prêt KOSGEB. Ce soutien fourni par l'État est totalement non remboursable. En plus de cette subvention, les femmes bénéficient d'un crédit sans intérêts. En 2018, ce prêt s'élève à 70 000 livres turques (14 000 euros). La première entreprise des femmes entrepreneurs sera également exempte d'impôt pendant les trois premières années.",
                                    val2: 'https://www.kosgeb.gov.tr/site/tr/genel/detay/6057/kadin-girisimciligi-women-entrepreneurship'
                                }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
