import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S36 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour partager à partir d'Instagram vers d'autres réseaux sociaux</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 6: Pour inclure cette publication dans d'autres réseaux sociaux, la vérification échoue dans la section Partager avec. Enfin, cliquez
                    sur Partager dans le coin supérieur droit.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img68.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
