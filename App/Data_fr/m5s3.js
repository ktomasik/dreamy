import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Comment ouvrir un compte bancaire d'entreprise pour votre démarrage?</Text>
                <Text style={{marginTop: 15}}>Un compte bancaire professionnel vous permet de suivre facilement vos dépenses, de gérer la paye des employés, de transférer des finances aux
                    investisseurs, de recevoir et de déposer des paiements et de planifier votre budget avec plus de précision. La création d'un compte bancaire d'entreprise nécessite des étapes
                    simples pour vous permettre de travailler rapidement:</Text>
                <FlatList
                    data={[

                        {key: "Déterminez de quels comptes vous avez besoin"},
                        {key: "Choisissez votre banque"},
                        {key: "Obtenir le nom de votre entreprise"},
                        {key: "Préparez vos documents afin de pouvoir accepter les paiements"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>La différence entre une carte de crédit et une carte de débit</Text>
                <Text style={{marginTop: 15}}>Une carte de crédit est une carte qui vous permet d’emprunter de l’argent sur une marge de crédit, également appelée limite de crédit de la carte. Vous
                    utilisez la carte pour effectuer des transactions de base, qui sont ensuite reflétées sur votre facture. Les cartes de débit tirent de l’argent directement de votre compte courant
                    lorsque vous effectuez l’achat. Cela peut prendre quelques jours et la mise en attente peut disparaître avant la fin de la transaction.</Text>
            </View>
        )
    }
}
