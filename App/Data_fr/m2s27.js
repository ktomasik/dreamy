import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S27 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer un compte Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 14: cliquez sur ajouter une photo pour ajouter la photo de votre profil. Et choisissez la source de votre photo de profil. Vous pouvez
                    le télécharger à partir de Face book, vous pouvez prendre une photo avec l’appareil photo du téléphone ou choisir une photo dans la bibliothèque.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img50.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img51.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
