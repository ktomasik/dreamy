import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S48 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utiliser les sites de réseaux sociaux</Text>
                <Text style={{marginTop: 10}}>Informer les gens des progrès de votre entreprise ou de vos produits est un travail très important. De nombreuses personnes ont décidé de le faire via
                    plusieurs sites ou plates-formes de réseaux sociaux. De loin le plus populaire est Facebook qui n’a pas besoin d’être présenté de nos jours…</Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 300, height: 300}}
                        source={require('../Images/images/1016.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}

