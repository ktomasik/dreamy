import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S22 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer un compte Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 9: Pour commencer à configurer votre profil Instagram, cliquez sur Suivant.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img40.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
