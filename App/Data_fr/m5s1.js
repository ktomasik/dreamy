import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text style={{marginTop: 10}}>Ce module fait référence aux procédures comptables couramment appliquées pour des femmes entrepreneurs en démarrage d’activité. Il se réfère
                    spécifiquement à l'ouverture de comptes bancaires, leurs types, les méthodes de transactions bancaires et des informations sur le commerce électronique et le système de paiement en
                    ligne (ou par carte de crédit). Il contient également des instructions pour leur mise en œuvre.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 280, height: 340}}
                        source={require('../Images/images/6931.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
