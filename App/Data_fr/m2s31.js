import React, {Component} from 'react'
import {FlatList, Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S31 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour partager à partir d'Instagram vers d'autres réseaux sociaux</Text>
                <Text style={{marginTop: 10}}>Dans le prochain exercice, nous montrerons: </Text>
                <FlatList
                    data={[

                        {key: "comment publier un contenu"},
                        {key: "comment partager le contenu avec d'autres réseaux sociaux et"},
                        {key: "comment ajouter un contenu dans un sujet pertinent."},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 3, marginLeft: 15}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Étape 1: pour publier un nouveau contenu, appuyez sur le signe de la caméra en bas de l’écran.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img59.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
