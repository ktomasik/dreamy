import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Rechercher sur Google et créer un compte e-mail depuis un smartphone</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 7: utilisez le compte que vous avez créé pour vous connecter à Gmail, à la fin des étapes, envoyez un mail à:
                    the end of steps, send e mail to: <Text style={{color: 'blue'}}>dreamy m-learning@gmail.com</Text>.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img7.png')}
                        resizeMode="contain"/>
                    <Text style={{marginTop: 10}}/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m1/img8.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Félicitations, vous avez créé votre compte mail!</Text>
            </View>
        )
    }
}
