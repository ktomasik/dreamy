import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Accepter des paiements avec des comptes bancaires </Text>
                <Text style={{marginTop: 15}}>Suivez les instructions ci-dessous pour accepter les paiements avec des comptes bancaires
                    {"\n\n"}Un virement bancaire est effectué comme suit:

                </Text>
                <FlatList
                    data={[

                        {key: "a.	L'entité souhaitant effectuer un transfert s'adresse à une banque et lui donne l'ordre de transférer une certaine somme d'argent. Les codes IBAN et BIC sont également indiqués pour que la banque sache où l’argent doit être envoyé."},
                        {key: "b.	La banque émettrice envoie un message, via un système sécurisé, à la banque réceptrice, lui demandant d'effectuer le paiement conformément aux instructions données."},
                        {key: "c.	Le message comprend également des instructions de règlement. Le virement n’est pas instantané: les fonds peuvent nécessiter plusieurs heures, voire plusieurs jours, pour passer du compte de l’expéditeur au compte du destinataire."},
                        {key: "d.	Soit les banques concernées doivent détenir un compte réciproque entre elles, soit le paiement doit être envoyé à une banque disposant d'un tel compte, une banque correspondante, pour que le destinataire final en tire un avantage supplémentaire."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Avant de recevoir un paiement international, vous devez fournir à l'expéditeur certaines informations, notamment:</Text>
                <FlatList
                    data={[

                        {key: "Votre numéro de compte bancaire international (IBAN)"},
                        {key: "Votre code de tri"},
                        {key: "Numéro de compte"},
                        {key: "Ton nom complet"},
                        {key: "Votre adresse"},
                        {key: "Le montant et la devise dans lesquels vous souhaitez recevoir le paiement"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
