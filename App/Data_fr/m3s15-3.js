import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour vendre au meilleur prix dans l'environnement en ligne.</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 3: Mettez un prix plus bas que si vous voyiez un produit similaire au vôtre, sur la page</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmarks.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
