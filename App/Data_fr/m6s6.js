import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S6 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Informations contractuelles </Text>
                <Text style={{marginTop: 15}}>En décembre 2015, la Commission européenne a proposé une directive sur les contrats de vente de biens en ligne et à distance (directive sur la vente en
                    ligne de biens). La proposition de directive sur la vente en ligne de biens permettrait une harmonisation maximale, interdisant ainsi aux États membres d'introduire un niveau
                    supérieur de protection des consommateurs dans le champ d'application de la directive . Comme indiqué ci-dessus, tout d'abord, lorsqu'un produit ou un service est acheté dans l'UE,
                    le commerçant doit fournir des informations claires, précises et compréhensibles sur le produit ou le service avant de l'acheter.
                    {"\n\n"}Dans ce contexte, avec les nouvelles dispositions proposées, un contrat d'achat en ligne typique devrait inclure les éléments suivants :

                </Text>
                <FlatList
                    data={[

                        {key: "L’identité, adresse, e-mail et numéro de téléphone du commerçant"},
                        {key: "Le titre professionnel et TVA du commerçant (le cas échéant)"},
                        {key: "Le numéro de registre du commerçant"},
                        {key: "Les principales caractéristiques du produit"},
                        {key: "Le   prix total   toutes taxes et tous frais compris"},
                        {key: "Les frais de livraison (le cas échéant) - et tous autres frais supplémentaires"},
                        {key: "Les  arrangements pour   paiement, livraison ou exécution"},
                        {key: "La   durée   du contrat (si applicable)"},
                        {key: "Toute restriction de livraison dans certains pays"},
                        {key: "Le droit d’annulation de commande   dans les 14 jours"},
                        {key: "Les services après-vente disponibles"},
                        {key: "Les mécanismes de résolution des litiges"},

                    ]}
                    renderItem={({item}) => <Text
                        style={{marginLeft: 15, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Vous pouvez être tenu responsable des produits vendus car vous vendez les produits que vous avez fabriqués sous le nom de votre entreprise. Par exemple, vous êtes un détaillant en ligne vendant des gâteaux extravagants personnalisés et les livrant aux clients.
                    {"\n\n"}Si quelqu'un vous reproche une intoxication alimentaire peu de temps après avoir mangé l'un de vos gâteaux, vous pourriez être tenu de payer des demandes d'indemnisation. En outre, le produit peut être endommagé pendant les opérations de chargement ou votre produit peut être volé. À ce stade, l’assurance pourrait couvrir le coût de remplacement, sur la base du prix de revient. À cet égard, contacter un agent d’assurance sera la bonne approche pour vous aider à comprendre le type d’assurance dont vous avez réellement besoin, tel que la responsabilité du fait des produits et la responsabilité commerciale.
                    {"\n\n"}Par ailleurs, comme on peut le voir dans les étapes suivantes, l’achat d’une assurance devient désormais un processus très simple et rapide. Seules les offres les plus appropriées avec vos informations sont préparées dans les meilleurs délais. Vous recevrez une proposition après l'examen préliminaire. Par conséquent, vous pouvez acheter l’assurance qui vous convient le mieux.

                </Text>
            </View>
        )
    }
}
