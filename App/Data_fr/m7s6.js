import React, { Component } from 'react'
import { Text, Image, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M7S6 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Crédits et soutien aux femmes dans les pays partenaires du projet</Text>
                <Text>Dans la section suivante, il est expliqué comment les femmes peuvent obtenir un financement pour chaque pays.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_1_3319637.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
