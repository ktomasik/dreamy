import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img1.png')
    }
}];

export default class M6S3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Facturation sur mobile</Text>
                <Text style={styles.dmlH3}>Pour IOS</Text>
                <Text>La version gratuite d’Invoice Simple est disponible sur iPhone, iPad et iPod Touch et peut être utilisée pour créer 3 factures ou estimations gratuites sur votre appareil
                    mobile.</Text>
                <Text style={{fontSize: 18, marginTop: 10}}><Text style={{fontWeight: 'bold'}}>Étape 1:</Text> Allez dans l'App Store et recherchez «facture» (voir les modules 1 et 2 pour savoir
                    comment s'y rendre et comment effectuer une recherche)</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img1.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
