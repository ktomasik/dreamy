import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S4 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Compte d'entreprise Instagram</Text>
                <Text styl={{marginTop: 10}}>En ce qui concerne ce qui précède, si vous avez déjà ouvert un compte professionnel sur Facebook, d’un point de vue commercial, Instagram est plus simple
                    et prend moins de temps que d’autres sites.
                    {"\n\n"}Si vous le souhaitez, vous pouvez élargir votre réseau de clients en utilisant Facebook et Instagram

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
