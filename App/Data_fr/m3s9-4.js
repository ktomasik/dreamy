import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S9_4 extends Component {


    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour vendre vos produits sur Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 4: Écrivez le texte relatif à votre produit, l'explication et le prix, donnez votre adresse e-mail privée en cas de communication avec
                    le client</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/522778-PIXKA7-878.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
