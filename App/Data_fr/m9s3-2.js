import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M9S3 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Créer une page Facebook pour les entreprises </Text>
                <Text>Pour vendre sur Facebook, il faut d’abord avoir une page sur Facebook (voir cette rubrique module 2)
                    {"\n\n"} À l'intérieur de votre page Facebook, vos visiteurs verront l'onglet de votre magasin et pourront accéder à vos produits à partir de cet emplacement.

                </Text>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Étape 1:</Text> connectez-vous à votre profil Facebook
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/facebook.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
