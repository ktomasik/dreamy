import React, {Component} from 'react'
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {Row, Rows, Table} from 'react-native-table-component';
// Styles

export default class M4S5 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tableHead: ['Format', 'Explication', 'Remarque'],
            tableData: [
                ['99 99 999 999 999', 'Le NIF est composé de 13 chiffres', 'e premier chiffre du NIF est un "0", un "1", un "2" ou un "3". Pour le traitement informatique, le NIF doit être écrit en bloc de 13 chiffres sans espace']
            ],
            tableHead2: ['Nombre', 'Objet', 'Forme', 'Quel administrateur le livre?', 'Nombre'],
            tableData2: [
                ['Siren', 'Identification unique de chaque entreprise auprès des administrations.', '9 chiffres', 'Insee via le CFE', 'Siren'],
                ['Siret', 'Identification de chaque établissement de la même entreprise avec les organisations sociales et fiscales.\nDoit apparaître sur les bulletins de salaire des employés.', '14 chiffres = 9 chiffres Siren + 5 chiffres spécifiques à chaque établissement', 'Insee via le CFE', 'Siret'],
                ['Code APE (activité principale exercée) ou code NAF', 'Identification de la branche d\'activité de l\'entreprise ou du travailleur indépendant.\nUtilisé pour déterminer la convention collective applicable.\nDoit apparaître sur les bulletins de salaire des employés.', '4 chiffres + 1 lettre faisant référence à la nomenclature statistique nationale des activités de la France (Nacre 2, en vigueur depuis 2008)', 'Insee via la CFE.\nIl est possible de le faire modifier si c\'est faux', 'Code APE (activité principale exercée) ou code NAF'],
                [
                    'RCS (Registre du commerce et des sociétés)',
                    'Enregistrement des commerçants et des sociétés commerciales auprès du RCS\nFigure à Extrait de K ou Kbis',
                    'RCS + ville d\'enregistrement + numéro de sirène',
                    'Greffe du tribunal de commerce via le CFE',
                    'RCS (Registre du commerce et des sociétés)'
                ],
                [
                    'Annuaire des métiers (RM)',
                    'Inscription obligatoire au Répertoire des métiers des artisans et des entreprises artisanales employant 10 personnes au plus.\nFigure dans D1 extrait',
                    'N ° Sirène + RM + chiffres désignant la chambre des métiers et métiers correspondants',
                    'Chambre des Métiers et Métiers (CMA) via la CFE',
                    'Annuaire   des métiers (RM)'
                ],
                [
                    'TVA intracommunautaire',
                    'Identification fiscale pour toute entreprise assujettie à la TVA.\nDoit apparaître sur les factures et les déclarations de TVA',
                    'FR + 2 chiffres + Numéro de sirène',
                    'Service de la taxe professionnelle (SIE) via le CFE',
                    'TVA intracommunautaire'
                ]
            ]
        }
    }

    render() {
        const state = this.state;
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu’est-ce que le numéro d'identification fiscale?</Text>
                <Text>Un Numéro d'identification fiscale (TIN) est un numéro d'identification utilisé à des fins fiscales dans le pays (Anonymous 2018f).
                    {"\n\n"}Toutes les personnes morales, entités non constituées en société et tous les individus doivent obtenir un numéro d'identification fiscale.

                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la Turquie</Text>
                <Text>Il existe un système de déclaration électronique valide. En fait, le numéro d’identité de la personne est maintenant utilisé comme numéro d’identification fiscale (TIN).
                    {"\n\n"}Les pièces d'identité doivent être notariées ou approuvées par les agents des services fiscaux. Ainsi, ils peuvent obtenir le numéro d'identification fiscale (Rapport
                    national Turquie, 2018).</Text>
                <FlatList
                    data={[
                        {key: "TIN afin d’entreprendre des activités professionnelles ou commerciales en Turquie."},
                        {key: "Depuis le 1 er juillet 2006, le numéro d’identité national est utilisé comme numéro d’identification unique pour les citoyens turcs et tous les TIN des citoyens ont été appariés à leur système de base de données du numéro d’identification national dans la base de données fiscale"},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la SLOVENIE</Text>
                <Text>Lors de la saisie des données requises dans le registre des taxes.
                    {"\n\n"}Des caractères alphanumériques ou numériques supplémentaires au numéro de taxe peuvent être requis.
                    {"\n\n"}Si une personne s'inscrit dans le registre des impôts en tant qu'entrepreneur personnel, l'administration financière ne lui attribue pas de nouveau TIN (Rapport national
                    Slovénie, 2018).
                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la POLOGNE</Text>
                <Text>Chaque adulte doit avoir un numéro d’identification fiscale pour rendre compte au bureau des impôts.
                    {"\n\n"}Si un adulte devient un entrepreneur individuel, il / elle a le même numéro d'identification fiscale à utiliser (Rapport national Pologne, 2018).

                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la GRECE</Text>
                <Text>
                    Chaque adulte doit avoir un numéro d'identification fiscale.
                    {"\n\n"}Dans le cas où un adulte souhaite devenir un entrepreneur individuel, il / elle doit enregistrer l'entreprise auprès de l'administration fiscale.
                    {"\n\n"}Avec cette autorisation, il / elle peut utiliser un numéro d’identification fiscale personnel à des fins commerciales (Rapport national Grèce, 2018).
                </Text>

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour FRANCE</Text>

                <Text>
                    Les autorités fiscales françaises attribuent un numéro d'identification fiscale à toutes les personnes physiques ayant l'obligation de déclarer des impôts en France.
                    {"\n\n"}Le TIN est attribué lorsque la personne s'inscrit dans la base de données des autorités fiscales.
                    {"\n\n"}Il est attribué à toutes les personnes créées dans le système d'enregistrement de la Direction générale des finances publiques (DGFiP) (PERS référentiel) pour toutes les taxes. C'est un numéro d'identification unique, non significatif, fiable et permanent.
                    {"\n\n"}Ce numéro d’impôt est indiqué sur le formulaire de déclaration d’impôt pré imprimé et sur les avis d’impôt sur le revenu et d’impôt foncier.
                    {"\n\n"}Le NIF qui doit être obtenu par le titulaire du compte ou du contrat, ou le titulaire de l'actif ou le bénéficiaire des revenus (National Report France, 2018).
                </Text>

                <Text>Structure du NIF</Text>
                <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                    <Row data={state.tableHead} style={styles2.head} textStyle={styles2.text}/>
                    <Rows data={state.tableData} textStyle={styles2.text}/>
                </Table>
                <Text>
                    Description du NIF
                    {"\n\n"}Les autorités fiscales françaises délivrent un numéro d'identification fiscale à toutes les personnes physiques ayant une obligation déclarative fiscale en France. Le NIF est attribué lors de l'enregistrement de la personne dans les bases de données de l'administration fiscale. Il est connu sous le nom de "numéro fiscal de référence" (ou numéro "SPI", Simplification des Procédures d'Imposition). Il est attribué à toute personne créée dans le référentiel des personnes de la DGFiP (référentiel PERS), pour l'ensemble des fiscalités. Il est unique, non signifiant, fiable et pérenne.
                    {"\n\n"}Où trouver le NIF ?
                    {"\n\n"}Le numéro fiscal de référence figure sur la déclaration pré-remplie d'impôt sur le revenu et les avis d'imposition à l'impôt sur le revenu, à la taxe d'habitation et à la taxe foncière. Si un document est commun à plusieurs personnes, le numéro fiscal de chacun des contribuables concernés y est indiqué conformément aux informations de la rubrique "Etat civil" de la déclaration pré-remplie d'impôt sur le revenu. Le NIF qui doit être collecté est, selon les cas, le NIF du titulaire du compte ou du contrat, le NIF du détenteur du bien ou le NIF du bénéficiaire des revenus. Si plusieurs personnes sont concernées (par exemple dans le cas d'un compte-joint détenu par deux personnes), le NIF de chacune des personnes doit être collecté.

                </Text>
                <Text>
                    Site Internet NIF national: <Text style={{color: 'blue'}} onPress={() => Linking.openURL('https://www.impots.gouv.fr/portail/')}>impots.gouv.fr/portail</Text>
                    {"\n"}Source: <Text style={{color: 'blue'}}
                                        onPress={() => Linking.openURL('https://ec.europa.eu/taxation_customs/tin/pdf/fr/TIN_-_country_sheet_FR_fr.pdf')}>ec.europa.eu/taxation_customs/tin/pdf/fr/TIN_-_country_sheet_FR_fr.pdf</Text>
                </Text>
                <Text>
                    Numéros d'identification d'entreprise
                </Text>
                <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                    <Row data={state.tableHead2} style={styles2.head} textStyle={styles2.text}/>
                    <Rows data={state.tableData2} textStyle={styles2.text}/>
                </Table>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    head: {minHeight: 40, backgroundColor: '#f1f8ff'},
    text: {margin: 6}
});
