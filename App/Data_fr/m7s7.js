import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S7 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Où Trouver la Petite entreprise de prêts pour Femmes?</Text>
                <Text style={styles.dmlH2}>Pour la Slovénie:</Text>
                <Text style={styles.dmlH3}>Banques</Text>

                <FlatList
                    data={[
                        {key: {val1: 'Simple et vite en ligne prêt en haut à 7000 EUR sans frais.',val2: 'http://www.hipkredit.si/'}},
                        {key: {val1: 'Microcrédits avec un taux d\'intérêt et des coûts plus bas lorsqu\'un coût mensuel fixe favorable est alloué tout au long de la période de remboursement du prêt.', val2: 'http://www.intesasanpaolobank.si/'}},
                        {key: {val1: 'Offre attractive pour s.p.',val2:'http://www.sparkasse.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Services financiers</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Prêt de 500 à 1000 EUR.',val2:'http://www.skupina8.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Chambre Régionale de Commerce et d'Industrie</Text>
                <FlatList
                    data={[
                    {key: {val1: 'Accorde des prêts avec une subvention d\'une partie du taux d\'intérêt sur les prêts à court terme pour les membres.',val2:'http://www.eng.gzs.si/'}},
                ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Centre d'investissement d'entreprise</Text>
                <FlatList
                        data={[
                        {key: {val1: 'Prêt rapide en ligne de 1000 EUR à 30000 EUR.',val2:'http://www.pnc.si/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                       onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
