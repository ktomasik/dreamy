import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'


export default class M7S29_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Grèce:</Text>

                <Text style={styles.dmlH3}>Étape 4:</Text>
                <Text>Le plan d'activités final est évalué par le comité compétent et, s'il est approuvé, nous procéderons à la fourniture d'un micro financement d'un montant maximal de 10 000 € et à
                    un soutien consultatif pour commencer sa mise en œuvre.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
