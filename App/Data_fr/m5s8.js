import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S8 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Réception des paiements</Text>
                <Text style={{marginTop: 15}}>Suivez les instructions ci-dessous pour configurer votre entreprise afin de recevoir des paiements</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 1 Configurez votre entreprise</Text>
                <Text>Vérifiez les options à l'avance, afin de mettre en place la bonne organisation et la bonne structure. De cette façon, vous ne vous retrouverez pas avec le mauvais type
                    d'entreprise. Dans de nombreux cas, vous pouvez le changer plus tard, mais il est utile de rechercher à l'avance ce qui pourrait le mieux fonctionner pour vous, qu'il s'agisse d’un
                    propriétaire unique, d'une LLC, d'un S-Corp, d'un C-Corp ou d'un autre type d'organisation. . Un comptable peut vous aider à créer votre entreprise et à vous occuper des documents
                    nécessaires.
                    {"\n\n"}Si vous avez des partenaires, assurez-vous que toutes leurs informations sont également exactes.

                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 2 Obtenez un numéro d'identification pour votre entreprise</Text>
                <Text>L'obtention d'un numéro d'identification est une première étape essentielle car vous aurez besoin de ce numéro important si vous souhaitez ouvrir un compte bancaire.
                    {"\n\n"}Vous pouvez avoir besoin de plus d’un numéro d’identification pour votre entreprise (par exemple, votre numéro de TVA lors de votre enregistrement auprès de
                    l’administration fiscale, votre numéro d’identification lors de votre enregistrement auprès de la Chambre de commerce locale, etc.).
                    {"\n\n"}Le numéro d'identification de votre société peut également être utile si vous souhaitez recevoir des paiements via un processeur tel que PayPal. Il doit également
                    apparaître sur tous les reçus et factures que vous émettez.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 3 Ouvrir un compte bancaire d'entreprise</Text>
                <Text>Lorsque vous commencez à recevoir des paiements, vous avez besoin d’un endroit pour les mettre.
                    {"\n\n"}Avoir des comptes séparés est important en tant que propriétaire d'entreprise. Tout d'abord, il est judicieux de séparer les actifs de votre entreprise de vos actifs
                    personnels. Deuxièmement, cela facilite beaucoup la tenue des dossiers. Lorsque vient le temps des impôts, il est beaucoup plus facile de tout régler si tout est réuni au même
                    endroit.
                    {"\n\n"}Si vous mélangez votre argent avec celui de votre entreprise, vous risquez d'avoir de graves problèmes. Vous devez toujours garder à l'esprit que le temps que vous recevez
                    de l'argent de vos clients ne coïncide pas avec le temps que vous devez payer, par exemple votre loyer, vos taxes, vos fournisseurs, etc. Ne pas comprendre l'importance de faire
                    cette distinction peut entraîner une pénurie d'argent lorsque des paiements réels sont effectués, car l'argent a été dépensé en dépenses personnelles.
                    {"\n\n"}Votre compte bancaire professionnel est l'endroit où vous devez déposer votre revenu et, en général, où vous devez effectuer toutes les transactions de votre société.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 4 Configurer pour recevoir des paiements via un tiers</Text>
                <Text>Il est important d'accepter de nombreuses méthodes de paiement. Vos clients et vos clients ont leurs propres préférences de paiement. L'un des meilleurs moyens de vous assurer de
                    leur adaptation est d'utiliser un processeur tiers.
                    {"\n\n"}Vous pouvez créer un compte marchand avec un processeur de carte ou recevoir des paiements via un site tel que PayPal. Lorsque vous utilisez ce type de processeur, les
                    cartes de crédit sont généralement prises en charge.
                    {"\n\n"}Il est également possible d'utiliser des processeurs pour accepter les cartes de crédit physiques de votre appareil mobile. Cela peut être utile si vous avez des objets
                    physiques à vendre et si vous le faites en personne.
                </Text>
                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 5 Exigences commerciales de l'État</Text>
                <Text>Avant de vous préparer à recevoir des paiements, assurez-vous de connaître les exigences des entreprises de votre État. Le ministère de l’Économie de votre pays (ou une autre
                    autorité compétente) devrait avoir des informations sur ce que vous devez faire pour créer votre entreprise et commencer à faire des affaires. </Text>
            </View>
        )
    }
}
