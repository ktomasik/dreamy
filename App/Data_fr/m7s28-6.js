import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S28_6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Turquie:</Text>

                <Text style={styles.dmlH3}>Étape 6: </Text>
                <Text>À ce stade, la femme entrepreneure présente son plan d’affaires à KOSGEB (la préparation de l’enseignement du plan d’affaires est dispensée pendant la formation KOSGEB).</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_14_3319630.png')}
                        resizeMode="contain"/>
                </View>

            </View>
        )
    }
}
