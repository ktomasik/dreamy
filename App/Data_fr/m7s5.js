import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S5 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Quelques fondations pour femmes fondatrices</Text>
                <Text>500 femmes: financer des fondatrices sans faille
                    {"\n\n"}Astia: réseau offrant un accès à des capitaux et à une formation / un soutien aux femmes entrepreneurs
                    {"\n\n"}BBG (Built By Girls) Ventures: investit dans de nouvelles entreprises Internet grand public avec au moins une femme fondatrice
                    {"\n\n"}Chloe Capital: une société de capital-risque en phase de démarrage axée sur les entreprises dirigées par des femmes
                    {"\n\n"}Female Founders Fund: investit dans des start-up dirigées par des femmes dans le commerce électronique, les plateformes et les services Web
                    {"\n\n"}Golden Seeds: réseau d'investisseurs providentiels et fonds investissant dans les femmes entrepreneurs
                    {"\n\n"}Intel Capital Diversity Fund: fonds qui investit dans des start-up dirigées par des femmes et des minorités
                    {"\n\n"}Mergelane: accélérateur et investisseur de jeunes start-up dirigées par des femmes (Boulder, CO)
                    {"\n\n"}Next Wave Impact: un fonds de capital-risque novateur d'apprentissage par la formation
                    {"\n\n"}Pipeline Fellowship: investisseurs femmes investissant dans des entreprises sociales dirigées par des femmes
                    {"\n\n"}Valor Ventures: Fonds basé à Atlanta qui finance des fondatrices
                    {"\n\n"}Women’s Venture Fund: aide les entrepreneurs à suivre des cours, des conseils, des crédits et plus encore
                </Text>
            </View>
        )
    }
}
