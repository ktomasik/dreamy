import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz8": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Les frais d’expédition sont l’une des dépenses les plus élevées pour les grands travaux et les grandes entreprises"
            },
            "question2": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Livraison automatique",
                    "option2": "b) Livraison le jour même",
                    "option3": "c) Ramassage en magasin",
                    "option4": "d) Expédier du magasin"
                },
                "question": "Lequel des éléments suivants ne fait pas partie des options d'expédition?"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "La stratégie d’expédition et de réalisation fait partie intégrante de la rentabilité des ventes."
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Pour les matériaux artisanaux en petites quantités, les entreprises de transport liées peuvent transférer les marchandises par route."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Sacs en plastique",
                    "option2": "b) Cartons",
                    "option3": "c) Forfaits Air Channel",
                    "option4": "d) Tous"
                },
                "question": "Lequel des éléments suivants est utilisé pour l’emballage de la cargaison?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Offrir la livraison gratuite n'est pas l'un des moyens d'attirer l'attention du client."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Assurance",
                    "option2": "b) Facture d'achat",
                    "option3": "c) Déclaration en douane",
                    "option4": "d) Étiquette d'emballage"
                },
                "question": "______ est un document officiel qui répertorie et donne des détails sur les produits importés ou exportés."
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Des accords de travail continu avec une entreprise de transport peuvent aider à réduire les frais d’expédition en raison de la capacité de travail."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Les branches",
                    "option2": "b) Le libre-service",
                    "option3": "c) Les  copains",
                    "option4": "d) Le Centre d'appel"
                },
                "question": "Lequel des éléments suivants ne fait pas partie des moyens d’atteindre les entreprises de fret?"
            },
            "question10": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Nom de l'expéditeur, adresse, téléphone",
                    "option2": "b) Couleur de l'article à l'intérieur de l'emballage",
                    "option3": "c) Nom du destinataire, adresse, téléphone",
                    "option4": "d) Détails du paiement (qui paiera)"
                },
                "question": "Lequel des éléments suivants n'est pas inclus sur la facture?"
            }
        }
    }
};

export default class M8Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz8;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
