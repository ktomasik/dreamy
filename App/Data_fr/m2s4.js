import React, {Component} from 'react'
import {Text, View, SectionList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Marketing sur les réseaux sociaux pour les femmes qui démarrent</Text>
                <Text style={styles.dmlH3}>Buts</Text>
                <Text>L'établissement d'objectifs est un élément essentiel de toutes les stratégies marketing et commerciales. Les médias sociaux ne font pas exception. Bien sûr, avec une gamme
                    de capacités sociales, il peut être difficile de déterminer exactement quels devraient être vos objectifs. À titre indicatif, voici quelques objectifs communs à prendre en compte
                    dans les médias sociaux:
                </Text>
                <SectionList
                    sections={[

                        {
                            title: "Augmenter la notoriété de la marque:",
                            data: ["pour créer une notoriété authentique et durable, évitez toute une série de messages promotionnels. Concentrez-vous plutôt sur des contenus significatifs et sur une forte personnalité de marque via vos canaux sociaux."]
                        },
                        {
                            title: "Augmentation de la qualité des ventes:",
                            data: ["il est presque impossible de creuser avec vos réseaux sociaux sans surveiller ni écouter des mots clés, des phrases ou des hashtags spécifiques. Grâce à un ciblage plus efficace des médias sociaux, vous atteignez votre public cible beaucoup plus rapidement."]
                        },
                        {
                            title: "Augmenter les ventes en personne:",
                            data: ["Certains détaillants comptent sur les efforts de marketing des médias sociaux pour stimuler les ventes en magasin. Votre marque fait-elle suffisamment de publicité sur les réseaux sociaux pour récompenser ceux qui viennent à vous? Pourquoi ne pas alerter les clients sur ce qui se passe dans vos magasins?"]
                        },
                        {
                            title: "Améliorer le retour sur investissement:",
                            data: ["il n’y a pas une marque sur les médias sociaux qui ne veuille pas augmenter son retour sur investissement. Mais sur le plan social, cet objectif est spécifique à la réalisation d’un audit approfondi de vos canaux et au maintien du coût de la main-d’œuvre, des publicités et de la conception"]
                        },
                        {
                            title: "Créer une base de fans fidèles:",
                            data: ["votre marque promeut-elle le contenu généré par les utilisateurs? Vos disciples réagissent-ils positivement sans aucune initiation? Atteindre ce stade nécessite du temps et des efforts pour créer une image de marque positive sur la société."]
                        },
                        {
                            title: "Meilleure impulsion sur le secteur:",
                            data: ["que font vos concurrents qui semblent fonctionner? Quelles stratégies utilisent-ils pour stimuler l'engagement ou les ventes? Avoir une impulsion sur l'industrie pourrait simplement vous aider à améliorer vos efforts et à prendre quelques conseils de ceux qui vont bien"]
                        },

                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Audience</Text>
                <Text>Nous avons beaucoup d'opportunités et de choix quant à l'affichage de nos produits. Pour choisir un ou plusieurs d'entre eux, nous devons nous demander: «Quel est notre groupe
                    cible?». Lorsque nous avons la réponse, nous pouvons suivre les données statistiques démographiques de personnes utilisant différents médias sociaux:</Text>
                <SectionList
                    sections={[
                        {
                            title: 'Les données démographiques les plus populaires de Facebook incluent:',
                            data: ["Femmes utilisatrices (89%)", "18-29 ans (88%)", "Utilisateurs des zones urbaines et rurales (81% chacun)", "Ceux qui gagnent moins de 30 000 $ (84%)", "Utilisateurs avec une certaine expérience universitaire (82%)"]

                        },
                        {
                            title: "Les données démographiques les plus populaires d’Instagram incluent:",
                            data: ["Femmes utilisatrices (38%)", "18-29 ans (59%),", "Utilisateurs situés en ville (39%)", "Ceux qui gagnent moins de 30 000 $ (38%)", "Utilisateurs avec une certaine expérience universitaire (37%)"]
                        },
                        {
                            title: 'Les données démographiques les plus populaires de Twitter incluent:',
                            data: ["Femmes utilisatrices (25%)", "18-29 ans (36%)", "Utilisateurs situés en ville (26%)", "Ceux qui gagnent entre 50 000 et 74 999 $ (28%)", "Utilisateurs avec expérience universitaire ou plus (29%)"]
                        }
                    ]}
                    renderItem={({item}) => <Text style={styles.itemList}>{'\u2022'}{item}</Text>}
                    renderSectionHeader={({section}) => <Text style={styles.sectionHeader}>{section.title}</Text>}
                    keyExtractor={(item, index) => index}
                />
                <Text style={styles.dmlH3}>Produits similaires</Text>
                <Text>Avant de commencer à créer du contenu, il est vraiment judicieux d’enquêter sur vos concurrents. Faites-le avant le processus de création de contenu, car vous trouvez souvent de
                    nouvelles façons de regarder le contenu en analysant ce qui fait le succès de vos concurrents. Le moyen le plus simple de rechercher des concurrents consiste à effectuer une simple
                    recherche sur Google. Recherchez vos mots-clés, expressions et termes du secteur les plus utiles pour savoir qui apparaît.</Text>
                <Text style={styles.dmlH3}>Contenu</Text>
                <Text>Pour commencer, il est recommandé de créer un contenu qui correspond à l’identité de votre marque. Cela signifie que vous devriez éviter des choses comme tendre la main à vos
                    données démographiques impopulaires sans une stratégie complète en place. Le contenu doit être pertinent et la page publiée ne doit pas être remplie de publicité. Les acheteurs en
                    ligne croient plutôt que le contenu vidéo uniquement au contenu vidéo. Si possible, utilisez des thèmes préparés. Conservez un format de contenu cohérent et simple pour que vos
                    lecteurs ne le confondent pas.</Text>
                <Text style={styles.dmlH3}>N'ignorez pas </Text>
                <Text>
                    Les canaux de médias sociaux sont construits en tant que réseaux. Cela signifie que leur objectif principal est d'être un espace de conversation, de discussion et de partage de
                    contenu. Votre marque ne peut pas oublier ces éléments essentiels du «réseautage» et il faut s’efforcer d’assurer que les conversations ou les opportunités d’engagement ne soient
                    pas laissées sans surveillance.
                    {"\n"}Grâce aux médias sociaux, vous gagnez le respect en tant que marque en étant simplement présent et en parlant à votre public. C’est pourquoi le service client est si important pour
                    les marques qui souhaitent accroître la notoriété de leur public. Tout est question d'engagement.

                </Text>
            </View>
        )
    }
}
