import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration d'un compte de messagerie sur un téléphone mobile pour une application de messagerie Android (Samsung, Sony, HTC...)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Etape 4: Entrez (connectez-vous) votre adresse e-mail complète et appuyez sur configuration manuelle (avec votre compte mail précédent,
                    votre mot de passe et une description de votre compte)
                </Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img15.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
