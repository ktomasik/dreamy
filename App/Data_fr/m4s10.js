import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S10 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Obligations fiscales</Text>
                <Text style={{marginTop: 10}}>Suivez les instructions pour remplir vos obligations fiscales:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Appliquer la règlementation des impôts{"\n"}(où vous habitez)</Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Obtenir un certificat d'exonération fiscale</Text>
                    </View>
                </View>
            </View>
        )
    }
}
