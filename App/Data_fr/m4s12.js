import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Application aux licences et permis locaux</Text>
                <Text style={{marginTop: 10}}>Suivez les instructions pour demander des licences et des permis: </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Obtenir le consentement des propriétaires{"\n"} de l'appartement (si nécessaire)
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Appliquer la règlementation des impôts{"\n"}(où vous habitez)
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Fournir la facture et la feuille de route
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Obtenir les documents des chambres{"\n"}ou des associations
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Demander à la municipalité un permis de travail
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
