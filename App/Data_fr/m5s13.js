import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S13 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Traitement des cartes de crédit au point de vente</Text>
                <Text style={{marginTop: 15}}>Follow the directions below to point of sale credit card processing:</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>1. Choisissez d’abord le bon système de point de vente pour votre entreprise. </Text>
                <Text>Le choix d’un système de point de vente est soumis à de nombreuses considérations. Tout d'abord, vous voudrez peut-être réfléchir à la manière dont vous souhaitez accepter les
                    paiements. Ferez-vous la plupart de vos affaires en magasin, en ligne ou une combinaison des deux? Voulez-vous pouvoir prendre des paiements hors site? Quel est votre budget pour
                    acheter du matériel, des logiciels et des périphériques POS tels que des scanners de codes à barres et des imprimantes de reçus? Quels sont vos projets pour développer votre
                    entreprise dans le futur?
                    {"\n\n"}Les réponses à toutes ces questions vous aideront à choisir le système le mieux adapté à votre entreprise.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>2. Choisissez un processeur de paiement compatible ou une passerelle de paiement </Text>
                <Text>
                    Vous devez également décider avec quel processeur de paiement ou quelle passerelle de paiement travailler. Une passerelle de paiement est une solution de paiement hébergée par une
                    tierce partie qui vous permet de vous connecter au processeur de paiement choisi, à condition que la passerelle fonctionne avec ce processeur.</Text>


                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>3. Assurez-vous que votre solution inclut les fonctionnalités de sécurité nécessaires pour
                    sécuriser les transactions et protéger vos clients et votre entreprise contre les violations.</Text>
                <Text>Une violation de la sécurité des données n’est pas seulement mauvaise pour les affaires, elle peut aussi être dévastatrice. Il est donc dans votre intérêt de faire tout ce qui
                    est en votre pouvoir pour empêcher qu'une violation ne se produise.
                    {"\n\n"}La sécurité des paiements exige une approche à plusieurs volets englobant à la fois les solutions technologiques et les meilleures pratiques. C'est mieux réalisé en
                    partenariat avec un fournisseur de traitement de paiement.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>4. Donnez la priorité aux fonctionnalités à valeur ajoutée de votre solution de paiement.</Text>
                <Text>Votre entreprise est unique et, de ce fait, aura des besoins uniques en matière de traitement des paiements.
                    {"\n\n"}Afin de tirer le meilleur parti de votre solution de point de vente, il est utile de réfléchir aux fonctionnalités que vous souhaitez et d'évaluer la disponibilité et les
                    coûts proposés par différents fournisseurs.

                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>5. N'oubliez pas de poser des questions sur l'assistance à l'installation et la formation du
                    personnel..</Text>
                <Text>La meilleure solution de paiement dans le monde offre peu de valeur à votre entreprise si c’est trop difficile à installer ou d’apprendre à l’utiliser. Demandez une démonstration
                    du produit pour pouvoir essayer la solution en temps réel. Assurez-vous de poser des questions sur la configuration et l'intégration à vos systèmes existants. Découvrez les
                    protocoles de fraude et si les mises à jour nécessaires sont exécutées automatiquement ou si vous avez de les mettre en œuvre vous-même Et bien sûr, il y a les bases du traitement.
                    Découvrez comment vous allez exécuter différents types de paiement.</Text>
            </View>
        )
    }
}
