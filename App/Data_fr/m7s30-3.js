import React, {Component} from 'react'
import {FlatList, Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S30_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la France:</Text>
                <Text style={styles.dmlH3}>Si vous n'êtes pas un demandeur d'emploi: L'AIDE DE L'ETAT - ACCRE</Text>

                <Text style={styles.dmlH3}>Étape 3:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "En cas de réponse favorable, Urssaf vous délivrera un certificat d'admission. Dans le cas contraire, il doit en exposer les motifs et notifier sa décision de rejet. En l’absence de réponse dans le mois qui suit, l’ACCRE est considéré comme acquis.",
                                val2: 'https://www.legalstart.fr/fiches-pratiques/aides-creation-entreprise/aide-creation-entreprise-femmes/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_43_3319642.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
