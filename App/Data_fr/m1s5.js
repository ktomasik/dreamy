import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Rechercher sur Google et créer un compte e-mail depuis un smartphone</Text>
                <Text style={{marginTop: 10, fontSize: 18 }}>Étape 3: écrivez ce que vous voulez ou une phrase, choisissez celle dont vous voulez regarder les résultats</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img2.png')}
                        resizeMode="contain"/>
                        <Text style={{marginTop: 10}}/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 10}}
                        source={require('../Images/m1/img3.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
