import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S12 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Petites bourses entreprises pour femmes </Text>
                <Text style={styles.dmlH2}>Pour la Slovénie: </Text>
                <Text style={styles.dmlH3}>Slovenski podjetniški sklad (Slovene Enterprise Fund)</Text>

                <FlatList
                    data={[
                        {
                            key:
                                {
                                    val1: "Fonds de démarrage pour les entreprises innovantes naissantes (P2A et P2B): capital de départ pour les nouvelles entreprises innovantes, sources de financement plus favorables pour les entreprises d’investissement pour le développement (subventions, garanties), promotion de l’investissement privé (fonds propres, prêts, garanties).",
                                    val2: 'https://www.podjetniskisklad.si/en'
                                }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Agencija RS za kmetijske trge in razvoj podeželja</Text>
                <Text>Soutien à la création et au développement de micro-entreprises.</Text>
                <Text style={styles.dmlH3}>Zavod RS za zaposlovanje</Text>
                <Text>Subventions pour le travail indépendant / octroi occasionnel de subventions pour le travail indépendant.</Text>
                <Text style={styles.dmlH3}>Slovenski regionalno razvojni sklad</Text>
                <Text>Incitations financières, en particulier sous forme de fonds remboursables, pour les investissements initiaux dans les domaines de l'entrepreneuriat, de l'agriculture, du
                    développement régional, des investissements financiers dans des systèmes de garantie régionaux, des projets de préfinancement avec des fonds Européens approuvés.</Text>
                <Text style={styles.dmlH3}>SID banka</Text>
                <Text>Fournir des ressources financières avantageuses aux entreprises et aux entreprises d'assurance exportation.</Text>
                <Text style={styles.dmlH3}>Eko sklad </Text>
                <Text>Fournir des ressources financières favorables pour investir dans des projets respectant l'environnement et l'efficacité énergétique.</Text>

            </View>
        )
    }
}
