import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S19 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Incitations fiscales pour Start-up de femmes</Text>
                <Text style={styles.dmlH2}>Pour la Grèce:</Text>
                <Text>En Grèce, des incitations sont accordées pour renforcer l'innovation car, en vertu d'une nouvelle loi, les sociétés qui fabriquent des produits ou fournissent des services d'enregistrement de brevets internationalement reconnus au nom de l'entreprise sont exonérées de l'impôt sur le revenu pendant trois ans.
                    {"\n\n"}En particulier, il est prévisible que les bénéfices d'une entreprise résultant de la vente de produits destinés à la production d'un brevet internationalement reconnu au nom de l'entreprise sont exonérés de l'impôt sur le revenu pendant trois années consécutives à compter de la première utilisation de la vente des produits utilisant un brevet.
                    {"\n\n"}Une exemption est également accordée lorsque les produits sont fabriqués dans des installations tierces. L'exonération est également accordée aux bénéfices provenant de la fourniture de services lorsqu'il s'agit d'un brevet également reconnu sur le plan international.
                </Text>
            </View>
        )
    }
}
