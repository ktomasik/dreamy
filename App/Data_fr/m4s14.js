import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M4S14 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Inscription aux associations professionnelles et professionnelles</Text>
                <Text style={{marginTop: 10}}>Suivez les instructions pour vous inscrire aux associations professionnelles:</Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>le bureau des impôts vous dirigera vers{"\n"}la chambre concernée qui peut faire une{"\n"}demande d'association professionnelle.
                        </Text>
                    </View>
                </View>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 320}}
                        source={require('../Images/images/30321.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
