import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Pages Web</Text>
                <Text>Une page Web est un document qui convient au Web et des navigateurs Web </Text>
                <Text style={styles.dmlH2}>Web site</Text>
                <Text>Un site Web est un ensemble de pages Web liées, y compris un contenu multimédia généralement identifié par un nom de domaine commun et publié sur au moins un serveur Web .</Text>
                <Text style={styles.dmlH2}>Serveur Web</Text>
                <Text>Un serveur Web est un système qui fournit du contenu ou des services aux utilisateurs finaux via Internet.</Text>
                <Text style={styles.dmlH2}>Moteur de recherche </Text>
                <Text>Un moteur de recherche Web est un logiciel conçu pour rechercher des informations sur le World Wide Web .</Text>
                <Text style={styles.dmlH2}>Recherche sur internet</Text>
                <Text>La recherche sur Internet est le processus d'exploration d'Internet pour chercher des informations avec l'utilisation d'un moteur de recherche comme Google ou Internet
                    Explorer.</Text>
                <Text style={styles.dmlH2}>Signification d’e-mail</Text>
                <Text>Le courrier électronique est un système d'envoi électronique de messages écrits d'un ordinateur à un autre.</Text>
                <Text style={styles.dmlH2}>Qu'est-ce qu’une adresse e-mail?</Text>
                <Text>Une adresse électronique est l'adresse d'une boîte aux lettres électronique pouvant recevoir (et envoyer) des messages électroniques sur un réseau.</Text>
                <Text style={styles.dmlH2}>Que signifie @?</Text>
                <Text>C’est le symbole dans une adresse e-mail qui sépare le nom de l'utilisateur de son adresse Internet.</Text>
                <Text style={styles.dmlH2}>Qu'est-ce qu'un virus de messagerie? </Text>
                <Text>Un virus de messagerie est un virus qui est envoyé avec ou attaché aux communications par email . Alors que de nombreux types de virus de messagerie fonctionnent de différentes
                    manières, il existe également diverses méthodes pour contrer ces cyberattaques difficiles </Text>
                <Text style={styles.dmlH2}>Qu'est-ce que le spam par courrier électronique </Text>
                <Text>Le spam fait référence aux e-mails en masse non sollicités (courrier indésirable). Cela signifie généralement qu'un message contenant une publicité ou même un contenu non
                    pertinent est envoyé à une multitude de destinataires, qui ne l'ont jamais demandé.</Text>
            </View>
        )
    }
}
