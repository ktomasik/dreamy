import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S5 extends Component {

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Create an Compte d'entreprise Instagram</Text>
                <Text>Création d'un compte: On vous a précédemment expliqué comment ouvrir un compte Instagram dans le module 2. Comme indiqué dans le module 2, vous pouvez télécharger l'application
                    Instagram à partir de l'App Store pour iOS, du Google Play Store pour Android ou du Windows Phone Store pour Windows Phone.
                    {"\n\n"}Une fois l'application chargée, appuyez sur l'icône de l'application sur votre téléphone mobile pour ouvrir Instagram.

                </Text>
                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 180, height: 180}}
                        source={require('../Images/socialmedia/instagram.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
