import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S5 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Système de paiement e-commerce </Text>
                <Text style={{marginTop: 15}}>An e-commerce payment system is a way of making transactions or paying for goods and services through an electronic medium, without the use of checks or
                    cash.</Text>
                <Text style={styles.dmlH3}>Qu'est-ce que configurer les paiements automatiques?</Text>
                <Text>Un paiement de facture automatique est un virement prévu à une date prédéterminée pour payer une facture récurrente. Les paiements automatiques de factures sont des paiements
                    courants effectués à partir d'un compte bancaire, de courtage ou de fonds communs de placement aux vendeurs. Ils sont généralement configurés avec la société recevant le paiement,
                    bien qu'il soit également possible de programmer des paiements automatiques via le service de paiement de facture en ligne d'un compte courant.</Text>
                <Text style={styles.dmlH3}>Accepter un paiement en ligne avec une carte de crédit, une carte de débit ou PayPal</Text>
                <Text>Permettre à vos clients de payer avec leur carte de crédit sur votre site Web est le moyen le plus simple d'accepter les paiements en ligne. Pour offrir cette fonctionnalité à
                    vos clients, vous devrez choisir entre votre propre compte marchand dédié ou un compte de dépôt intermédiaire.
                    {"\n\n"}Les petites entreprises ou organisations qui souhaitent accepter les paiements en ligne par carte de crédit pour des services, des abonnements ou des produits vendus sur un
                    site Web peuvent ajouter des boutons PayPal à n'importe quel site Web.

                </Text>
                <Text style={styles.dmlH3}>Le système de paiement en ligne sécurisé nécessite</Text>
                <Text>La sécurité en ligne est quelque chose qui nous concerne tous en tant que consommateurs. En tant que propriétaire d'entreprise, c'est encore plus important. En prenant des
                    paiements en ligne, vous prenez la responsabilité de protéger les données de vos clients et sa gestion en toute sécurité peut être un fardeau coûteux. Mais vous pouvez vous
                    simplifier la tâche en utilisant une solution de paiement conforme à la norme PCI. La conformité PCI fait référence aux règles et réglementations qui régissent la protection des
                    données.</Text>
                <Text style={styles.dmlH3}>Solutions de paiement en ligne avec carte, facture et banque</Text>
                <Text>Les paiements en ligne sont faits instantanément, donc c'est pratique et vous fait gagner beaucoup de temps.
                    {"\n\n"}Les "portefeuilles en ligne" permettent à leurs clients de:

                </Text>
                <FlatList
                    data={[

                        {key: '1. Payez en ligne en révélant les détails de votre carte de crédit,'},
                        {key: '2. Payer une facture.'},
                        {key: '3. Payer sur un compte bancaire.'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH3}>Qu'est-ce qu'un point de vente virtuel (POS)?</Text>
                <Text>POS virtuel Terminal. C'est une passerelle de paiement qui permet aux marchands en ligne et aux vendeurs de services d'autoriser manuellement les transactions par carte initiées
                    par l'acheteur. Ce processus étend considérablement leurs sources de paiement et réduit le temps du processus de paiement, tout en ajoutant une sécurité supplémentaire.
                    L'intégration du terminal de point de vente virtuel est possible avec de nombreuses plates-formes de commerce électronique.</Text>
            </View>
        )
    }
}
