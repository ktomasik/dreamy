import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M8S8_3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Suivez les étapes indiquées ci-dessous pour envoyer la cargaison:</Text>

                <Text style={styles.dmlH3}>ÉTAPE 3: CHOISISSEZ VOTRE TYPE DE CARGAISON ET VOTRE SOCIÉTÉ</Text>
                <Text>Certaines des entreprises de fret Française travailleront de manière nationale et internationale:</Text>
                <FlatList
                    data={[


                        {key: {val1: "France cargo international", val2: "http://www.fci-cie.com/"}},
                        {key: {val1: "LKW WALTER", val2: "http://www.lkw-walter.fr"}},
                        {key: {val1: "FEDEX", val2: "https://www.fedex.com/fr"}},
                        {key: {val1: "ATS FRANCE:", val2: "http://www.atseurope-express.com/"}},
                        {key: {val1: "DIMOTRANS:", val2: "https://www.dimotrans.com/"}},
                        {key: {val1: "UPS:", val2: "https://www.ups.com/fr/fr"}},
                        {key: {val1: "BY EXPRESS", val2: "http://www.byexpresslogistic.com/fr/"}},
                        {key: {val1: "1DAY EXPRESS", val2: "https://www.1dayexpress.fr/"}},
                        {key: {val1: "France cargo international", val2: "http://www.fci-cie.com/"}},
                        {key: {val1: "LKW WALTER", val2: "http://www.lkw-walter.fr"}},
                        {key: {val1: "FEDEX", val2: "https://www.fedex.com/fr"}},
                        {key: {val1: "ATS FRANCE:", val2: "http://www.atseurope-express.com/"}},
                        {key: {val1: "DIMOTRANS:", val2: "https://www.dimotrans.com/"}},
                        {key: {val1: "UPS:", val2: "https://www.ups.com/fr/fr"}},
                        {key: {val1: "BY EXPRESS", val2: "http://www.byexpresslogistic.com/fr/"}},
                        {key: {val1: "1DAY EXPRESS", val2: "https://www.1dayexpress.fr/"}},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key.val1}: <Text
                        style={{color: 'blue'}}
                        onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}

