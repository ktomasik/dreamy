import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S57 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utiliser les sites de réseaux sociaux</Text>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour configurer un blog</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 1: accédez au Google Play Store et recherchez Blogger.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img3.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img116.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
