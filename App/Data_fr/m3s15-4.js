import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S15_4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour vendre au meilleur prix dans l'environnement en ligne.</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 4: Mentionnez des remises lors de l’achat de 2 produits ou plus en même temps, écrivez le montant de réduction</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/24911.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
