import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S11 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Compte commercial PayPal </Text>
                <Text style={{marginTop: 15}}>Suivez les instructions ci-dessous pour configurer un compte PayPal. Suivez les guides ci-dessous pour compléter la configuration de votre compte. Vous
                    devrez confirmer votre adresse e-mail, vérifier votre compte PayPal et choisir une solution de paiement PayPal.
                </Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 1</Text>
                <Text>PayPal vous a envoyé un e-mail lorsque vous avez ouvert votre compte professionnel PayPal. Cliquez sur le lien dans l'email pour confirmer votre adresse email. Si vous ne trouvez
                    pas l'e-mail, connectez-vous à votre compte PayPal et cliquez sur Confirmer l'adresse e-mail dans votre "Liste de tâches" sous l'icône de profil d'entreprise.</Text>

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 2</Text>
                <Text>En vous faisant vérifier, vous gagnerez non seulement en crédibilité auprès de vos vendeurs et de vos acheteurs, mais vous supprimerez également la limite de retrait de votre
                    compte. Il y a 2 façons de se faire vérifier:</Text>
                <FlatList
                    data={[

                        {key: "1.	Confirmez votre carte UnionPay (vérification instantanée)\n\nAjoutez ou examinez les détails de votre carte UnionPay. Confirmez la carte en autorisant China UnionPay à envoyer un code de vérification par SMS. Entrez le code pour confirmer votre carte instantanément."},
                        {key: "2.	Confirmez votre carte de crédit\n\nAjoutez ou examinez votre carte Visa ou MasterCard sur votre compte PayPal et confirmez la carte. Cela génère un code à 4 chiffres, qui sera reflété dans votre relevé de carte de crédit dans un délai de 2-3 jours ouvrables. Connectez-vous à votre compte PayPal, entrez le code pour terminer le processus de vérification."},


                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{textAlign: 'center', fontWeight: 'bold', color: 'green', marginTop: 15}}>Étape 3</Text>
                <Text>Choisissez une solution de paiement adaptée à votre entreprise. </Text>
            </View>
        )
    }
}
