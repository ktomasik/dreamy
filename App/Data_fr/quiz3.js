import React, {Component} from 'react'
import {Alert, ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import { Button } from 'react-native-elements';
import {quizStyles} from '../Containers/Styles/general';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz3": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux",
                },
                "question": "Le commerce électronique a commencé à entrer dans nos vies depuis les années 1950, lorsque les ordinateurs ont commencé à être utilisés."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux",
                },
                "question": "Les comptes Instagram Business ne fournissent pas plus d'informations sur votre marque, ni un outil d’analyse permettant de mesurer votre succès."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Etsy ne facture pas la création d'un magasin.",
                    "option2": "b) Bonanza vous permet de lister vos articles gratuitement.",
                    "option3": "c) Dans Zibbet, vous êtes autorisé à vendre des véhicules électroniques.",
                    "option4": "d) Il n'y a pas de frais pour ouvrir une boutique dans DaWanda."
                },
                "question": "Lequel des énoncés suivants est faux?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux",
                },
                "question": "Le commerce électronique est l'achat et la vente de biens et de services sur une plate-forme en ligne."
            },
            "question5": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) L'esprit humain a tendance à incarner quelque chose d'abstrait.",
                    "option2": "b) Des images de produits de qualité sont un facteur clé de l'engagement du magasin.",
                    "option3": "c) Des études de suivi oculaire montrent que les visiteurs des magasins sont d'abord engagés par des éléments visuels.",
                    "option4": "d) La connexion entre le logo et la marque n'est pas inévitable."
                },
                "question": "Laquelle des affirmations suivantes sur l’importance des images de produits est fausse?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux",
                },
                "question": "La découverte Hashtag dans Instagram aide les produits à apparaître plus souvent sur la page d'accueil des utilisateurs"
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Se concentrer sur la narration pour stimuler l'engagement",
                    "option2": "b) Ouvrir un compte avec une coche bleue",
                    "option3": "c) Ouvrir de faux comptes pour louer le produit",
                    "option4": "d) Transformer l'image d'origine avec une édition",
                },
                "question": "Quel est l’un des moyens de réussir une vente sur Instagram?"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Dans la vente en ligne d'objets d'artisanat, l'utilisation de l'image réelle n'est pas aussi importante que le prix du produit."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "La conception de votre logo doit représenter votre produit et votre philosophie d’entreprise."
            },
            "question10": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Plus de 60% des acheteurs en ligne dans le monde considèrent les prix du commerce électronique comme le tout premier critère déterminant leur décision d'achat.",
                    "option2": "b) Le principal avantage de la stratégie de tarification basée sur les coûts du commerce électronique est la simplicité.",
                    "option3": "c) La tarification du commerce électronique basée sur la concurrence peut conduire à vendre votre produit à un prix inférieur à celui souhaité.",
                    "option4": "d) La tarification du commerce électronique basée sur la valeur promet un profit à court terme.",
                },
                "question": "Lequel des énoncés suivants est faux?"
            }
        }
    }
};

export default class M3Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz3;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{ textAlign: 'center' }} buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno+1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
