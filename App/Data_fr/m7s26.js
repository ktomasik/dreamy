import React, { Component } from 'react'
import { Text, View } from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S26 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Micro finance et autres Fonds Start-Up pour femmes</Text>
                <Text style={styles.dmlH2}>Pour la Pologne:</Text>
                <Text style={styles.dmlH3}>ACADÉMIE DE DÉMARRAGE</Text>
                <Text>Formation, mentorat, méthodes innovantes de construction d'entreprises, programmes d'accélération</Text>

                <Text style={styles.dmlH3}>TWÓJ STARTUP</Text>
                <Text>Pré-incubation, conseil juridique et comptable, conseil en informatique et marketing, formation</Text>

                <Text style={styles.dmlH3}>Inkubator Technologiczny Podkarpckiego Parku Naukowo-Technologicznego</Text>
                <Text>Bureaux, services de conseil, aide au développement</Text>

                <Text style={styles.dmlH3}>Przedsiębiorcze kobiety 2.0</Text>
                <Text>Le projet s'adresse aux femmes qui ne travaillent pas pour les aider à créer leur propre entreprise</Text>

                <Text style={styles.dmlH3}>AIP</Text>
                <Text>Business Consulting, le mentorat et le coaching, le service comptabilité, soutien juridique, formation de démarrage</Text>
            </View>
        )
    }
}
