import React, {Component} from 'react'
import {Image, Linking, Text, TouchableHighlight, View, Modal} from 'react-native'
import ImageViewer from 'react-native-image-zoom-viewer'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

const screen1 = [{
    url: '',
    props: {
        source: require('../Images/m3/img68.png')
    }
}];

export default class M3S4_3 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour prendre des photos des produits et les télécharger sur un site Web</Text>

                <Text style={{marginTop: 10, fontSize: 18}}>Étape 3: Accédez au navigateur Internet de votre téléphone, allez sur <Text style={{color: 'blue'}}
                               onPress={() => Linking.openURL('https://www.etsy.com/')}>etsy.com</Text></Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={screen1}/>
                    <TouchableHighlight onPress={() => {this.setModalVisible(!this.state.modalVisible);}} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {this.setModalVisible(true);}} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height:200}}
                        source={require('../Images/m3/img68.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
