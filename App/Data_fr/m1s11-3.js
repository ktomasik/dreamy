import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S11_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration d'un compte de messagerie sur un téléphone mobile pour un appareil iOS (iPhone, iPad ou iPod touch)</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 6: Choisissez si vous souhaitez que votre code de vérification soit envoyé par SMS ou par courrier électronique.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex:1, width: 320, height: 350}}
                        source={require('../Images/images/483352-PGP3GE-251.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
