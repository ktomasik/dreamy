import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Sites de réseautage populaires des médias sociaux</Text>
                <Text>Il a été estimé qu'environ 81% des Américains utilisaient les médias sociaux en 2017 et de plus en plus. Selon une estimation, plus d’un cinquième du temps en ligne d’une
                    personne est consacré aux médias sociaux. En 2005, le pourcentage d'adultes utilisant les médias sociaux était d'environ 5%. À l'échelle mondiale, il y a environ 1,96 milliard
                    d'utilisateurs de médias sociaux. Ce nombre devrait atteindre 2,5 milliards d’ici la fin de 2018. D’autres estimations sont encore plus élevées (Silver, n.d.). Selon le Centre de
                    recherche Pew (2018), les utilisateurs de médias sociaux ont tendance à être plus jeunes (environ 90% des 18-29 ans utilisent au moins une forme de média social), plus instruits et
                    relativement riches (plus de 75 000 $ par an). Les États-Unis et la Chine sont en tête de la liste des médias sociaux («Principaux réseaux sociaux mondiaux 2018
                    {"\n\n"}Statistique '2018): Facebook (2,167 milliards d'utilisateurs en janvier 2018), YouTube (1,5 milliard), WhatsApp (1,3 milliard), Facebook Messenger (1,3 milliard), WeChat
                    (980 millions), QQ (843 millions), Instagram (800 millions), Tumblr (794M), QZone (568M), Sina Weibo (376M), Twitter (330M), Baidu Tieba (300M), Skype (300M), LinkedIn (260M),
                    Viber (260M), Snapchat (255M), Reddit (250M) ), LINE (203M), Pinterest (200M), AA (117M).
                    {"\n\n"}Différents sites Web et applications destinés aux médias sociaux sont dédiés à différents types de communication et à différents contenus. Une brève description est
                    disponible dans les sections suivantes pour les médias sociaux les plus populaires (Rouse 2016)
                </Text>
                <Text style={styles.dmlH3}>Facebook</Text>
                <Text>Est un site Web populaire de réseautage social gratuit qui permet aux utilisateurs enregistrés de créer des profils, de télécharger des photos et des vidéos, d'envoyer des
                    messages et de rester en contact avec leurs amis, leur famille et leurs collègues. Selon les statistiques du groupe Nielsen, les utilisateurs d'Internet aux États-Unis passent plus
                    de temps sur Facebook que tout autre site Web</Text>
                <Text style={styles.dmlH3}>Twitter</Text>
                <Text>Est un service de micro-blogging gratuit qui permet aux membres enregistrés de diffuser de courts messages appelés tweets. Les membres de Twitter peuvent diffuser des tweets et
                    suivre les tweets d’autres utilisateurs en utilisant plusieurs plates-formes et appareils.</Text>
                <Text style={styles.dmlH3}>Google+</Text>
                <Text>Est le projet de réseau social de Google, conçu pour reproduire la façon dont les gens interagissent hors ligne plus étroitement que ce n’est le cas dans d’autres services de
                    réseau social. Le slogan du projet est «Repenser le partage dans la vie réelle pour le Web”</Text>
                <Text style={styles.dmlH3}>Wikipedia</Text>
                <Text>Est une encyclopédie en ligne à contenu libre et gratuit, créée grâce à la collaboration d’une communauté d’utilisateurs connue sous le nom de Wikipédiens. Toute personne
                    inscrite sur le site peut créer un article pour publication. Il n'est pas nécessaire de s'inscrire pour éditer des articles. Wikipedia a été fondée en janvier 2001.</Text>
                <Text style={styles.dmlH3}>LinkedIn</Text>
                <Text>Est un site de réseautage social conçu spécifiquement pour le monde des affaires. L'objectif du site est de permettre aux membres inscrits d'établir et de documenter des réseaux
                    de personnes qu'ils connaissent et en qui ils ont confiance, de manière professionnelle.</Text>
                <Text style={styles.dmlH3}>Reddit</Text>
                <Text>Est un site de nouvelles sociales et un forum où les histoires sont socialement préparées et promues par les membres du site. Le site est composé de centaines de
                    sous-communautés, appelées «subreddits». Chaque subreddit traite d'un sujet spécifique tel que la technologie, la politique ou la musique. Les membres du site Reddit, également
                    appelés «redditors», soumettent du contenu qui est ensuite soumis au vote des autres membres. L’objectif est d’envoyer des histoires bien considérées en haut de la page principale
                    du site.</Text>
                <Text style={styles.dmlH3}>Pinterest</Text>
                <Text>Est un site de conservation sociale permettant de partager et de catégoriser les images trouvées en ligne. Pinterest nécessite de brèves descriptions, mais l'objectif principal
                    du site est visuel. En cliquant sur une image, vous arrivez à la source d'origine. Ainsi, par exemple, si vous cliquez sur l'image d'une paire de chaussures, vous pourriez être
                    amené sur un site où vous pouvez l'acheter. Une image de crêpes aux bleuets pourrait vous mener à la recette; une photo d'un nichoir fantasque pourrait vous emmener aux
                    instructions.
                </Text>
            </View>
        )
    }
}
