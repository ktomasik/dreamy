import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements'
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz4": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Logo",
                    "option2": "b) Marque",
                    "option3": "c) Brevet",
                    "option4": "d) identification fiscale"
                },
                "question": "A _______ est un nom, un logo, un signe ou une forme qui, individuellement ou en combinaison, permet au consommateur de différencier le produit ou le service des autres sur le marché."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a)Vrai",
                    "option2": "b)Faux"
                },
                "question": "Le nom de marque et la marque doivent être identiques."
            },
            "question3": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Municipalité locale",
                    "option2": "b) Bureau des impôts",
                    "option3": "c) L’adresse",
                    "option4": "d) Chambre"
                },
                "question": "Pour obtenir les licences et les permis locaux pour travailler à la maison, quelle est la première étape?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Le nom légal est pour les procédures gouvernementales, le nom commercial est pour les relations publiques."
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Une marque est une protection juridique de la marque, accordée par le Bureau des marques et des brevets."
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Une demande au bureau des impôts",
                    "option2": "b) Demander un appartement",
                    "option3": "c) Aller à la chambre",
                    "option4": "d) La facture d'approvisionnement"
                },
                "question": "Quelle est la première étape pour devenir contribuable?"
            },
            "question7": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) La marque",
                    "option2": "b) La facture d'achat",
                    "option3": "c) L’appartenance à une chambre",
                    "option4": "d) Le numéro d'identification fiscale"
                },
                "question": "Qu'est-ce qui est requis pour appliquer le système de déclaration électronique?"
            },
            "question8": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Un nom commercial ne peut pas être utilisé sur les formulaires et les applications du gouvernement."
            },
            "question9": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Accord de l'utilisateur",
                    "option2": "b) Politique de confidentialité",
                    "option3": "c) La permission des clients ",
                    "option4": "d) Logo breveté"
                },
                "question": "Qu'est-ce qui n'est pas requis pour le commerce électronique sur un site Web?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Sans enregistrement de marque, il n’y a aucun moyen d’empêcher des concurrents ou des «imitateurs» d’utiliser la même marque."
            }
        }
    }
};

export default class M4Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz4;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
