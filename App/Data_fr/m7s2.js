import React, {Component} from 'react'
import {Text, View} from 'react-native'
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu’est-ce qu’un prêt? </Text>
                <Text>Un prêt c’est quand vous emprunter l'argent à une banque ou un autre prêteur. Les paiements de Prêt sont divisé en deux les parties, la principale (principale) et les intérêts.
                    {"\n\n"}Le principal est le montant que vous empruntez et constitue la majeure partie (principale) du solde de ce compte.
                    {"\n\n"}L'intérêt est la charge pour le temps que vous avez le prêt et est calculé sur le principal.</Text>

                <Text style={styles.dmlH2}>Qu’est-ce que le solde du prêt?</Text>
                <Text>Un solde de prêt est un montant restant à payer sur votre prêt. Chaque prêt a un solde de prêt jusqu’à ce que le prêt soit entièrement remboursé. Il change tous les jours
                    (les intérêts sont ajoutés quotidiennement).
                    {"\n\n"}Calendrier d'amortissement des prêts; Le capital et les intérêts sont séparés, de sorte que vous puissiez voir quelle partie de votre paiement mensuel sert à rembourser
                    le capital et quelle partie est utilisée pour payer les intérêts.
                </Text>
                <Text style={styles.dmlH2}>Qu'est-ce qu'une subvention?</Text>
                <Text>Les subventions sont des fonds non remboursables ou des produits versés ou donnés par une partie, souvent un ministère, une société, une fondation ou une fiducie, à un
                    bénéficiaire. Une subvention gouvernementale est une récompense financière accordée par un gouvernement fédéral, régional ou local à un bénéficiaire éligible. </Text>
                <Text style={styles.dmlH2}>Qu'est-ce qu’une incitation fiscale?</Text>

                <Text>Un incitatif fiscal est une mesure gouvernementale destinée à encourager les particuliers et les entreprises à dépenser de l'argent ou à économiser de l'argent en réduisant le
                    montant de leurs impôts.
                </Text>
            </View>
        )
    }
}
