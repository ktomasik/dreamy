import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S6 extends Component {
    render() {
        return (
            <View style={styles.section}>

                <Text style={styles.dmlH2}>Ouvrir un compte bancaire </Text>
                <Text style={{marginTop: 15}}>Suivez les instructions ci-dessous pour ouvrir un compte bancaire.
                    {"\n\n"}Considérez vos options
                    {"\n\n"}Une fois que vous avez identifié vos besoins, évaluez vos options:

                </Text>
                <FlatList
                    data={[
                        {key: 'Compte courant'},
                        {key: 'Compte épargne'},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5, fontWeight: 'bold'}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 10}}>Vous avez peut-être choisi le compte bancaire qui vous convient le mieux, mais vous devez également vous assurer que vous êtes éligible pour ouvrir un
                    compte. Avant de vous rendre à la banque, vous devrez vérifier si vous remplissez tous les critères pour ouvrir un compte.

                    {"\n\n"}En règle générale, les banques exigeront les éléments suivants:
                    {"\n\n"}Identification valide. Dans certains pays, vous pouvez également avoir besoin de votre numéro de sécurité sociale.
                    {"\n\n"}Un montant minimum d'argent pour l'ouverture du compte. Cela peut varier en fonction de la banque et du compte que vous choisissez. Par exemple, un compte d'épargne d'une
                    banque moyenne nécessite un dépôt minimum de 300 €.

                    {"\n\n"}Choisissez la banque qui vous convient le mieux. Contactez la succursale bancaire dans votre région pour discuter exactement ce que vous obtiendriez si vous avez ouvert un
                    compte de base. Bien que toutes les banques soient différentes, elles peuvent généralement être résumées en deux catégories générales: les grandes banques et les petites.

                    {"\n\n"}Grandes banques: les grandes banques ont généralement des succursales dans la plupart des villes et villages du pays. Vous pouvez éviter les frais que vous devrez payer
                    pour utiliser les services d'autres banques (tels que les frais de guichet automatique, etc.). Les grandes banques offrent également des services tels que des lignes d'assistance
                    24h / 24 pour leurs clients. En outre, ces banques ont généralement une réputation de confiance stable.

                    {"\n\n"}Petites banques locales: Les petites banques offrent une expérience plus personnelle, plus conviviale et humaine. Les petites banques facturent aussi généralement des frais
                    moins élevés pour l’utilisation de leurs services. Les petites banques investissent souvent leur argent dans la communauté locale. En revanche, les petites banques font faillite
                    plus souvent que les grandes (ce qui est encore très rare).

                    {"\n\n"}En outre, les coopératives de crédit sont une autre option pour les banques. Les coopératives de crédit sont des institutions financières à but non lucratif, qui ont
                    souvent pour mission de «servir la population, et non de faire des profits». Elles ont réussi à rendre leurs services plus accessibles en s'associant avec d'autres coopératives de
                    crédit pour offrir des services bancaires partagés et les guichets automatiques.

                    {"\n\n"}Rendez-vous à votre banque et demandez l’ouverture d’un compte. Ouvrir un compte en personne est généralement la meilleure option pour les nouveaux titulaires de compte.
                    Vous pouvez demander au caissier toutes les questions et les doutes que vous avez et obtenir des réponses immédiates. En outre, le processus d'ouverture d'un compte est
                    généralement plus rapide en personne.

                    {"\n\n"}Posez toutes les questions importantes avant de finaliser votre compte. Demandez des éclaircissements sur toute question concernant votre compte.

                    {"\n\n"}Fournissez les informations nécessaires à la création de votre compte. L’ouverture d’un compte courant nécessite quelques pièces de base de renseignements personnels. En
                    général, c'est une bonne idée d'avoir:
                    {"\n\n"}La preuve que vous êtes qui vous dites être: ayez avec vous une pièce d'identité émise par le gouvernement avec votre photo (un permis de conduire ou un passeport peut
                    également suffire).

                    {"\n\n"}La preuve d'adresse: une facture de téléphone, un permis de conduire ou tout autre document officiel avec votre nom et votre adresse suffiront généralement.

                    {"\n\n"}La preuve que vous êtes un citoyen inscrit: la banque vous demandera votre numéro de sécurité sociale, votre numéro d'identification de contribuable ou votre numéro
                    d'identification d'employeur afin de vous assurer que vous êtes "enregistré" avec le gouvernement


                    {"\n\n"}<Text style={{fontWeight: 'bold'}}>Conservez les documents du compte que vous recevez en sécurité. </Text> Une fois votre compte terminé, vous recevrez des documents
                    contenant des informations importantes sur votre compte. Gardez-les dans un endroit sûr. Si vous le pouvez, il est judicieux de conserver en mémoire les informations suivantes afin
                    de ne pas avoir à représenter les documents à l'avenir:
                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Votre code PIN à quatre chiffres',
                                val2: 'vous en aurez besoin pour utiliser votre carte de débit lors d’achats.'
                            }
                        },
                        {
                            key: {
                                val1: 'Votre numéro de compte bancaire: ',
                                val2: 'vous en avez besoin pour des tâches financières telles que la configuration des dépôts directs'
                            }
                        },
                        {
                            key: {
                                val1: 'Votre numéro de sécurité sociale: ',
                                val2: 'vous en aurez besoin pour diverses tâches fiscales et financières à l\'avenir'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
