import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M6S1 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Introduction</Text>
                <Text style={{marginTop: 10}}>Le groupe cible de Dreamy m-Learning Project est constitué de femmes peu scolarisées qui fabriquent de l'artisanat à domicile. L’objectif est de les aider
                    à acquérir des compétences numériques mobiles et entrepreneuriales pour vendre leurs produits d’artisanat sur les marchés numériques en utilisant efficacement les téléphones
                    intelligents. Selon cet objectif, nous allons développer une application mobile, facile à comprendre et à utiliser pour les systèmes d'exploitation Android et iOS, interface Web
                    reliant les applications mobiles au portail m-learning. Dans le programme de formation que nous avons préparé, nous expliquerons aux femmes comment procéder étape par étape avec
                    des applications mobiles gratuites et communes.
                    {"\n\n"}Conformément à ce cadre, ce module vise à expliquer des questions telles que la création de factures, l’établissement de contrats pour les relations fournisseur-acheteur
                    pour les achats sur Internet, la souscription d’une assurance pour la vente de produits en ligne et la création de signatures électroniques pour les femmes souhaitant vendre leurs
                    produits artisanaux sur Internet.</Text>
            </View>
        )
    }
}
