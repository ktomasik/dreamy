import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S24 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Micro finance et autres Fonds Start-Up pour femmes</Text>
                <Text style={styles.dmlH2}>Pour la Grèce:</Text>
                <Text style={styles.dmlH3}>La confiance du peuple</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "The People’s Trust (La confiance du peuple) offre des micro-subventions aux entrepreneurs grecs qui souhaitent créer une nouvelle entreprise ou développer une entreprise existante, mais rencontrent des difficultés pour accéder au crédit. La subvention peut atteindre 10 000 euros par entreprise fournie comme capital de départ pour une nouvelle entreprise ou comme fonds de roulement pour une entreprise existante. Ce programme de financement s'adresse aux groupes ayant un faible accès à d'autres formes de financement.",
                                val2: 'http://www.thepeoplestrust.org'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text style={styles.dmlH3}>Microfinancing (AFI & Eurobank)</Text>
                <Text>AFI (Action Finance Initiative) est une société civile à but non lucratif. Elle a été créée en Grèce en 2014 par ActionAid Hellas et l'organisation française ADIE, leader du microcrédit en Europe.
                    {"\n\n"}Euro Bank coopère avec AFI pour apporter une aide en matière de microcrédit (jusqu'à 15 000 €) aux chômeurs de longue durée, aux personnes appartenant à des catégories vulnérables de citoyens et aux micro-entrepreneurs sans accès au crédit bancaire. Ils leur offrent la possibilité de créer leur propre emploi (travail indépendant) ou de créer de petites unités d’entreprise et de créer de nouveaux emplois.
                </Text>

                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'AFI assume la présélection, la formation et le mentorat des candidats. Euro Bank se charge du contrôle du crédit et du financement.',
                                val2: 'https://www.eurobank.gr/el/business/proionta-kai-upiresies/proionta-upiresies/xrimatodotiseis/anaptuksiaka/easy-afi'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
