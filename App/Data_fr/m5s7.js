import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";


export default class M5S7 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Démarrer les services bancaires mobiles et en ligne</Text>
                <Text style={{marginTop: 15}}>Suivez les instructions ci-dessous pour démarrer les services bancaires mobiles et en ligne:</Text>
                <Text style={{fontWeight: 'bold'}}>What you’ll need </Text>
                <FlatList
                    data={[
                        {key: 'A valid debit or credit card'},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{fontWeight: 'bold', marginTop: 10}}>Que faites-vous</Text>
                <Text>Sur votre appareil mobile:
                    {"\n\n"}Téléchargez et ouvrez l'application Mobile de services bancaires sur votre appareil Android ou Apple. Suivez les étapes ci-dessous.
                    {"\n\n"}À partir d'un ordinateur:

                </Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Étape 1',
                                val2: 'Visitez la page d\'accueil de la banque'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 2',
                                val2: 'Sélectionnez "S\'inscrire"'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 3',
                                val2: 'Entrez votre numéro de carte et la date d\'expiration, puis sélectionnez «Continuer»'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 4',
                                val2: 'Dites comment vous souhaitez recevoir votre code de vérification'
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />

                <Text style={{marginTop: 15}}>Certaines banques offrent également le service suivant:</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: 'Étape 1',
                                val2: "Lorsque vous effectuez des transactions sensibles, un code de vérification vous protège avec une couche de sécurité supplémentaire. Vous pouvez envoyer un code de vérification 6 chiffres par message texte, e-mail ou appel vocal. Vous allez entrer ce code pour compléter la transaction."
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 2',
                                val2: 'Certains peuvent envoyer des codes de vérification ponctuels à des services de messagerie personnels ou gratuits.'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 3',
                                val2: 'Enter Entrez le code de vérification dans la case Vérification d\'identité et sélectionnez «Continuer».'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 4',
                                val2: 'Choisissez un mot de passe'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 5',
                                val2: 'Passez en revue les petits caractères'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 6',
                                val2: 'Vous avez presque fini! Lisez l\'accord d\'accès électronique et cochez la case pour confirmer que vous l\'avez lu.'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 7',
                                val2: 'Ouvrez une session et commencez à faire des opérations bancaires.'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 8',
                                val2: 'Maintenant que vous êtes inscrit, vous pouvez effectuer vos opérations bancaires depuis n\'importe quel ordinateur, smartphone ou tablette.'
                            }
                        },
                        {
                            key: {
                                val1: 'Étape 9',
                                val2: "Si votre appareil prend en charge la connexion par empreinte digitale, vous pouvez utiliser certaines applications de la Banque. Utilisez le lecteur d'empreintes digitales pour vérifier votre identité et accéder à vos comptes en un clic.  "
                            }
                        }
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}><Text style={{fontWeight: 'bold'}}>{item.key.val1}: </Text>{item.key.val2}</Text>}
                />
            </View>
        )
    }
}
