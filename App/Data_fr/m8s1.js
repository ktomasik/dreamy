import React, {Component} from 'react'
import {Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import moduleStyles from "../Containers/Styles/ModuleStyles";

export default class M8S1 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH2}>Stratégie d'expédition et de livraison efficace </Text>
                <Text style={{marginTop: 10}}>L'expédition est l'une des étapes cruciales pour fournir un produit efficace au client avec des coûts idéaux pour la production de vos marchandises, pour atteindre le client et pour recevoir le paiement. Pour livrer le produit en toute sécurité avec un coût efficace pour le client, vous devez prendre en compte certains facteurs tels que la destination, la spécification du contenu, le poids, les dimensions, etc.
                    {"\n\n"}Les coûts d'expédition représentent l'une des dépenses les plus élevées pour les petits travaux et les petites entreprises, mais les stratégies d'expédition et de traitement des commandes aident les entrepreneurs à atteindre leurs clients à temps, à des prix raisonnables.
                    {"\n\n"}Les calculs de prix pour les petites entreprises sont basés sur le montant et la fréquence à envoyer pour toutes les compagnies de transport. Si le volume de l'expédition augmente, le prix diminue essentiellement. Cependant, la négociation est possible pour de nombreuses entreprises de fret. Pour de nombreuses entreprises, des comptes d’adhésion sont disponibles, ce qui permet de réduire les coûts d’expédition.
                    {"\n\n"}L'utilisation de colis fournis par les entreprises de transport peut éviter de devoir payer des frais de dimensions supplémentaires. Les entreprises peuvent fournir différents types de colis pour différents produits. Les plus courants sont les sacs en plastique, les boîtes en carton, les systèmes de canaux d’air pour les produits sensibles, les systèmes à rouleaux pour peintures et le papier de différentes tailles et dimensions.
                    {"\n\n"}La stratégie d’expédition fait partie intégrante de la rentabilité des ventes. Si cela est fait correctement, la stratégie et le package d'expédition peuvent aider à favoriser les ventes répétées et même à aider à acquérir de nouveaux clients.
                </Text>
                <View style={moduleStyles.content}>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Production
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Atteindre le client
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Accord et paiement
                        </Text>
                    </View>
                    <View style={moduleStyles.SquareView}/>
                    <View style={moduleStyles.TriangleView}/>
                    <View style={moduleStyles.RectangleShapeView}>
                        <Text>Livraison
                        </Text>
                    </View>
                </View>
            </View>
        )
    }
}
