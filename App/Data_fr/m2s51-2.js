import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S51_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utiliser les sites de réseaux sociaux</Text>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer une page Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 7: Vous pouvez taper une URL de votre page d’accueil pour envoyer vos visiteurs à cette page (ou ignorer cette étape), puis cliquer sur SUIVANT.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img104.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
