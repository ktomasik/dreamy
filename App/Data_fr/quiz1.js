import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz1": {
            "question1": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Nom d'utilisateur",
                    "option2": "b) Mot de passe",
                    "option3": "c) Informations de contact",
                    "option4": "d) Adresse du domicile"
                },
                "question": "Pour ouvrir un compte de messagerie, lequel des éléments suivants n'est pas requis?"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Le spam par courrier électronique et le virus de courrier électronique ont la même signification"
            },
            "question3": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Il peut y avoir des différences pour configurer un compte de messagerie sur différentes marques de smartphones"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) L’espace Mondial web",
                    "option2": "b) l'Internet",
                    "option3": "c) Compte email",
                    "option4": "d) Courrier indésirable"
                },
                "question": "Le ________ est fabriqué à partir de millions de liens   de pages Web"
            },
            "question5": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Pour utiliser un courrier électronique, il est nécessaire d'avoir un compte."
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) compte bancaire",
                    "option2": "b) Compte email",
                    "option3": "c) Adresse du domicile",
                    "option4": "d) Numéro de téléphone"
                },
                "question": "Pour envoyer un courrier électronique via Internet, lequel des éléments suivants est nécessaire?"
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Un nom de média social",
                    "option2": "b) Un compte e-mail",
                    "option3": "c) Un logiciel pour rechercher des informations sur internet",
                    "option4": "d) Un site Web"
                },
                "question": "Quel est le sens de moteur de recherche?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Le compte de messagerie de l'utilisateur peut être appliqué à un compte sur un téléphone intelligent."
            },
            "question9": {
                "correctoption": "option4",
                "options": {
                    "option1": "a) Faire une recherche sur internet",
                    "option2": "b) Nettoyer le virus",
                    "option3": "c) Transférer des données sur internet",
                    "option4": "d) Pour séparer le nom de l'utilisateur de l'adresse Internet de l'utilisateur"
                },
                "question": "À quoi sert @?"
            },
            "question10": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Un moteur de recherche",
                    "option2": "b) Une adresse email",
                    "option3": "c) Une sorte de virus",
                    "option4": "d) Courrier indésirable"
                },
                "question": "Qu'est-ce que Google?"
            }
        }
    }
};

export default class M1Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz1;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }
    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
        /*console.log('countCheck: ' + this.state.countCheck);
        console.log('punkty: ' + this.score)*/
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{color: 'black', textAlign: 'center'}}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert(); this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>

                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({

    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
