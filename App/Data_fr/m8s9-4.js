import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img15.png')
    }
}];

export default class M8S9_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Suivez les étapes indiquées ci-dessous pour envoyer la cargaison:</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>ÉTAPE 8: TRACK</Text>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img15.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
                <Text style={{marginTop: 5}}>Conseils pour travailler avec des compagnies de fret:</Text>
                <FlatList
                    data={[
                        {key: "Demander au client la préférence (société, entreprise, etc.) avant le transfert pourrait être une bonne idée. Elle / il peut avoir une exigence spéciale pour cela."},
                        {key: "Le produit doit correspondre à l'emballage. Un emballage approprié peut réduire les coûts."},
                        {key: "La signature d’un accord avec le transporteur peut réduire les coûts."},
                        {key: "L'accord peut offrir certains avantages, tels que la réception de fret sur votre adresse sans aucun coût."},
                        {key: "Avant le transfert, les succursales et le délai de livraison doivent être vérifiés par la cargaison."},
                        {key: "La saisie et le partage du numéro de fret avec le client peuvent aider à livrer à temps."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

            </View>
        )
    }
}
