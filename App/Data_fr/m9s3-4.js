import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S3_4 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Créer une page Facebook pour les entreprises </Text>

                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Étape 3:</Text> cliquez sur l'onglet «Pages» dans la section «Explorer» de la barre latérale gauche de la
                    page d'accueil de votre profil. Dans le menu déroulant, cliquez sur «Créer une page».
                </Text>

                <View style={{alignItems: 'center', marginTop: 15}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m9/img1.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
