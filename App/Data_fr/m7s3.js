import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu'est-ce que la Microfinance </Text>
                <Text>La microfinance est un terme générique utilisé pour désigner des produits et services financiers destinés aux personnes exclues du circuit bancaire traditionnel. La microfinance
                    permet à ces personnes de financer leurs moyens de subsistance, d’épargner, de subvenir aux besoins de leur famille et de se protéger des risques de la vie quotidienne.</Text>
                <Text style={styles.dmlH2}>Comment fonctionne la microfinance?</Text>
                <Text>Les institutions de microfinance proposent aux emprunteurs un «microcrédit» assorti d'une assistance (financement d'une nouvelle entreprise ou d'un plan d'expansion, prise en
                    charge des besoins urgents de la famille, facilitation de la mobilité pour trouver un emploi, etc.), même si ces emprunteurs n'offrent pas une garantie solide de remboursement. Les
                    revenus générés par l’activité économique des micro-emprunteurs leur permettent de rembourser le solde du prêt.</Text>
                <Text style={styles.dmlH2}>Comment la contribution de la microfinance aide-t-elle les femmes entrepreneurs?</Text>
                <Text>La contribution de la microfinance consiste à aider les pauvres, y compris les femmes, à trouver un emploi, à accroître leur confiance en eux-mêmes, à améliorer leurs compétences
                    en communication et à d’autres aspects également. Les femmes acquièrent un plus grand contrôle sur les ressources telles que la possession matérielle, les ressources
                    intellectuelles telles que le savoir, les informations, les idées et la prise de décision chez elles, dans la communauté, la société et la nation en participant à ces programmes de
                    microfinance.
                    {"\n\n"}La microfinance est un outil important pour l'autonomisation des femmes par les entreprises dans une perspective de ressources. Les prêts de groupe, qui ont été présentés
                    comme un outil important dans la réduction de la pauvreté, profitent aux membres grâce au réseautage.
                </Text>
            </View>
        )
    }
}
