import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S9 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Lois sur les affaires en ligne (commerce électronique)</Text>
                <Text style={{marginTop: 10}}>Pour le commerce électronique, les informations suivantes doivent être mises à la disposition des utilisateurs sur le site Web:</Text>
                <Text style={{marginLeft: 10, marginTop: 5}}>Informations concernant:</Text>
                <FlatList
                    data={[

                        {key: "Titre commercial, adresse commerciale, numéro de registre fiscal ou du commerce, adresse électronique, numéro de téléphone et noms des administrateurs du site Web (et, s’ils exploitent un marché en ligne, informations de communication officielles des vendeurs / fournisseurs) ;"},
                        {key: "Si le site Web fonctionne sous licence ou autorisation d'un organisme gouvernemental et de l'organisme compétent."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 25}}>{item.key}</Text>}
                />
                <FlatList
                    data={[
                        {key: "Termes et conditions de visite et d'utilisation du site."},
                        {key: "Politique de confidentialité."},
                        {key: "La signature électronique est obligatoire."},
                        {key: "Accord de l'utilisateur (si l'adhésion est requise)."},
                        {key: "Le contrat à distance doit être préparé conformément au Règlement sur les contrats à distance (si le site Web vend des biens ou des services aux consommateurs) en vertu de la Loi n ° 6502 sur la protection du consommateur."},
                        {key: "\tPour coordonner les flux monétaires entre les consommateurs et l'entreprise en ligne, l'entreprise en ligne doit collaborer avec une banque ou un fournisseur de services de paiement (Loi sur les services de paiement n ° 6493) (Dora et al. 2018)"}
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
