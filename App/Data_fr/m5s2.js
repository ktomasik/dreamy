import React, {Component} from 'react'
import {Text, View, FlatList} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M5S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Qu’est-ce qu’un compte bancaire?</Text>
                <Text style={{marginTop: 10}}>Un compte bancaire est un lieu sûr et utile de votre choix pour déposer tout votre argent. Vous pouvez accéder à votre argent depuis n'importe quel
                    guichet automatique. Cela facilite également l’épargne et le placement de votre argent pour votre avenir.

                    {"\n\n"}Types de comptes bancaires
                    {"\n\n"}La plupart des banques et des coopératives de crédit offrent les types de comptes suivants:

                </Text>
                <FlatList
                    data={[
                        {key: "1.	Comptes d'épargne"},
                        {key: "2.	Vérification des comptes"},
                        {key: "3.	Comptes du marché monétaire"},
                        {key: "4.	Certificats de dépôt (CD)"},
                        {key: "5.	Comptes de retraite"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={styles.dmlH2}>Qu'est-ce qu'un compte courant? Qu'est-ce que la banque Mobile & Online?</Text>
                <Text style={{marginTop: 10}}>Un compte courant offre un accès facile à votre argent pour vos besoins transactionnels quotidiens et vous aide à garder votre argent en sécurité. Les
                    clients peuvent utiliser une carte de débit ou des chèques pour faire des achats ou payer des factures. Les comptes peuvent comporter différentes options ou offres permettant
                    d'éviter certains frais de service mensuels. Pour déterminer le choix le plus économique, comparez les avantages de différents packages de contrôle avec les services dont vous avez
                    réellement besoin.
                    {"\n\n"}Les services bancaires mobiles vous permettent de réaliser bon nombre des mêmes activités que les services bancaires en ligne en utilisant un smartphone ou une tablette
                    plutôt qu'un ordinateur de bureau. La polyvalence de la banque mobile comprend:

                </Text>
                <FlatList
                    data={[
                        {key: "Se connecter au site Web mobile d'une banque"},
                        {key: "Utiliser une application de banque mobile"},
                        {key: "Services bancaires par SMS (SMS banque)"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{marginTop: 15}}>Les services bancaires en ligne désignent toute transaction bancaire pouvant être effectuée sur Internet, généralement sur le site Web d'une banque sous
                    un profil privé, et avec un ordinateur de bureau ou un ordinateur portable. La banque en ligne est généralement définie comme ayant les caractéristiques suivantes:</Text>
                <FlatList
                    data={[

                        {key: "Transactions financières via le site Web sécurisé de la banque."},
                        {key: "Les succursales physiques ou uniquement en ligne."},
                        {key: "L'utilisateur doit créer un identifiant de connexion et un mot de passe."},

                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
            </View>
        )
    }
}
