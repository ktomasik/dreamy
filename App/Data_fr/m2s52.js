import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S52 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Utiliser les sites de réseaux sociaux</Text>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer une page Facebook</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 8: Ajoutez une photo de profil à partir de votre galerie en bloquant le bouton AJOUTER UNE PHOTO DE PROFIL, choisissez une photo et
                    cliquez sur TERMINER et ENREGISTRER.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img105.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img106.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img107.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
