import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S36_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour partager à partir d'Instagram vers d'autres réseaux sociaux</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 7: le contenu est publié …</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img69.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
