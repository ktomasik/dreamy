import React, {Component} from 'react'
import {Text, Image, View, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Pourquoi les images de produits sont-elles importantes?</Text>
                <Text>L'époque dans laquelle nous vivons est telle que la visualisation revêt une grande importance. Quand les gens pensent ou font quelque chose, ils conçoivent toujours une forme
                    dans leur esprit. Parce que l'esprit humain travaille à transformer quelque chose d'abstrait en concret. C'est pourquoi les articles visuels ont une grande importance dans les
                    ventes de produits fabriqués à la main.</Text>
                <Image
                    style={{flex: 1, width: 300, height: 120}}
                    source={require('../Images/m3/img10.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 200}}
                    source={require('../Images/m3/img11.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 130}}
                    source={require('../Images/m3/img12.png')}
                    resizeMode="contain"/>

                <Text style={styles.dmlH2}>Comment vendre vos produits sur Instagram?</Text>
                <Text>La vente de vos produits sur votre site Web n'est plus le seul moyen de générer des bénéfices. Avec la popularité croissante du commerce social, il n'y a jamais eu autant de
                    moyens de vendre en ligne et de promouvoir votre marque. Avec plus de 700 millions d'utilisateurs, Instagram devient rapidement le moteur du commerce social. Environ 80% des
                    Instagramistes suivent une entreprise sur Instagram et 60% affirment l'utiliser pour découvrir de nouveaux produits.</Text>
                <Image
                    style={{flex: 1, width: undefined}}
                    source={require('../Images/m3/img13.png')}
                    resizeMode="contain"/>
                <Text style={{marginTop: 10}}>Pour vendre votre produit sur Instagram, vous devez utiliser quelques astuces:</Text>
                <FlatList
                    data={[

                        {key: "Assurez-vous d'avoir un compte professionnel."},
                        {key: "Publiez des photos et des vidéos de haute qualité."},
                        {key: "Concentrez-vous sur la narration pour stimuler l'engagement."},
                        {key: "Créez des publicités Instagram attrayantes."},
                        {key: "Faites confiance aux ambassadeurs de la marque et aux influenceurs pour mieux faire connaître votre entreprise."},
                        {key: "Echangez avec les clients."},
                        {key: "Trouvez de nouveaux clients grâce à la découverte de hashtag"},
                        {key: "Mettez à jour votre lien de profil souvent."},
                        {key: "Offrez des réductions spéciales pour les abonnés Instagram"},

                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 5, marginLeft: 12}}>{item.key}</Text>}
                />
                <Image
                    style={{flex: 1, width: 300, height: 480}}
                    source={require('../Images/m3/img14.png')}
                    resizeMode="contain"/>
                <Image
                    style={{flex: 1, width: 300, height: 400}}
                    source={require('../Images/m3/img15.png')}
                    resizeMode="contain"/>
            </View>
        )
    }
}
