import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S15 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Petites bourses entreprises pour femmes </Text>
                <Text style={styles.dmlH2}>Pour la France:</Text>
                <Text style={styles.dmlH3}>PRI (Partenariats régionaux d'innovation): </Text>

                <FlatList
                    data={[
                        {key: {val1: '',val2: 'https://www.bpifrance.fr/Toutes-nos-solutions/Aides-concours-et-labels/Aides-a-l-innovation-projets-individuels/PRI-Faisabilite'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
                <Text>Ce dispositif est ouvert aux PME innovantes en cours de création mais ce n’est pas leur cible prioritaire.
                    {"\n\n"}Il est mené en partenariat uniquement avec 5 régions: Grand Est (Alsace, Champagne-Ardenne, Lorraine), Hauts de France (Picardie), Aquitaine / Poitou Charentes, Pays de la Loire, PACA.
                    {"\n\n"}Les projets les plus innovants seront sélectionnés et recevront une subvention de 100 000 à 200 000 euros maximum par projet. Cette assistance vous permettra de couvrir les dépenses liées aux études préliminaires et à la mise en œuvre du projet. Il est payé en 2 fois (70% et 30%).
                    {"\n\n"}Enfin, le projet de la PME doit se dérouler sur 12 mois maximum.
                </Text>
            </View>
        )
    }
}
