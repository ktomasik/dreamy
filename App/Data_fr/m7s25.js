import React, {Component} from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S25 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Micro finance et autres Fonds Start-Up pour femmes</Text>
                <Text style={styles.dmlH2}>Pour la France:</Text>
                <Text>Un prêt généralement inférieur à 25 000 €, destiné à des personnes souhaitant créer ou reprendre une entreprise mais dont les ressources sont insuffisantes pour prétendre à un
                    emprunt classique. Pour bénéficier d'un microcrédit, l'emprunteur doit être accompagné d'un réseau de soutien spécialisé et compétent tel que: « France Active », « France
                    Initiative », les « Boutiques de gestion » ou «la Fondation de la 2e chance ». Ces réseaux l'aideront à mettre en place son projet, à enquêter sur sa demande de financement et à
                    développer son activité. L'acteur principal est l'ADIE (Association pour le développement de l'initiative économique).</Text>

                <Text style={styles.dmlH3}>ADIE</Text>
                <Text>Sensibilise, guide et informe les femmes sur la création d'entreprise, avec l'organisation depuis 2015 d'une campagne de sensibilisation annuelle pour les femmes.
                    {"\n\n"}Promeut le financement des porteurs de projets, avec le microcrédit accompagné pour les entreprises qui n’ont pas accès au crédit bancaire.
                    {"\n\n"}Renforce le soutien des créateurs d’entreprises, avec des modules de formation et de sensibilisation adaptés à leurs spécificités.
                </Text>
                <FlatList
                    data={[
                        {key: 'https://www.adie.org/nos-actions/pour-les-femmes'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />

                <Text style={styles.dmlH3}>FRANCE ACTIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "France Active soutient et finance des entreprises depuis près de 30 ans et a mobilisé 270 millions d'euros pour servir 7 400 entreprises l'an dernier.\n" +
                                "Bien plus qu’un réseau, France Active est un véritable mouvement d’entrepreneurs engagés dont l’ambition est de construire une société plus inclusive. La mission de France Active est d'accélérer le succès des entrepreneurs en leur donnant les moyens de s'impliquer.\n",
                                val2: 'https://www.franceactive.org/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />


                <Text style={styles.dmlH3}>FRANCE INITIATIVE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Après avoir été nommés France Initiative Network, puis France Initiative, le réseau a repensé son système de marques. Depuis le 1er octobre 2012, l'association nationale s'appelle Initiative France. Les plates-formes locales et la coordination régionale apportent le même changement. C'est plus qu'un simple renversement de mots. Cette marque met désormais en valeur le terme commun à tous: Initiative, tout en affichant le nom du territoire. Il est accompagné d'un logo qui reflète graphiquement la force d'un réseau national et sa diversité, liée à ses racines locales. Enfin, il porte une signature qui donne tout son sens à notre action collective: \"Un réseau, un esprit\"",
                                val2: 'http://www.initiative-france.fr/'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>BGE</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "Depuis plus de 35 ans, BGE soutient la création d’entreprise et s’efforce de la rendre accessible à tous. En accompagnant les entrepreneurs à toutes les étapes de la création, de l’émergence au développement des entreprises, nous donnons à tous ceux qui prennent les chances de leur succès.\n" +
                                "En tant que réseau associatif à but non lucratif, BGE regroupe 50 associations implantées dans les territoires pour ouvrir des perspectives, sécuriser le parcours des entrepreneurs et créer des solutions durables pour l'emploi et le développement local.\n",
                                val2: 'http://www.bge.asso.fr/nous-sommes/notre-engagement.html'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>2nd CHANCE FOUNDATION</Text>
                <FlatList
                    data={[
                        {
                            key: {
                                val1: "La Fondation 2nd Chance a pour but de soutenir les personnes âgées de 18 à 62 ans, qui ont vécu des moments difficiles dans leur vie et qui se trouvent actuellement dans une situation très précaire, mais qui ont un réel désir de rebondir.\n" +
                                "La Fondation 2nd Chance leur offre un soutien humain et financier pour mener à bien un projet professionnel réaliste et durable: formation qualifiante, création ou reprise d'entreprise.\n",
                                val2: 'http://www.deuxiemechance.org/fr'
                            }
                        },
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
