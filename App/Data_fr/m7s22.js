import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S22 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Micro finance et autres Fonds Start-Up pour femmes</Text>
                <Text style={styles.dmlH2}>For Slovenia:</Text>
                <Text style={styles.dmlH3}>Vstopne točke SPOT (VEM)</Text>
                <Text>Information, de base Conseil, enregistrement d’entreprise </Text>

                <Text style={styles.dmlH3}>Podjetniški inkubatorji</Text>
                <Text>Équipé des bureaux, affaires et autre soutien aux prestations de service</Text>

                <Text style={styles.dmlH3}>Univerzitetni inkubatorji</Text>
                <Text>Équipé des bureaux, consultation affaires et mentorat, ateliers libre éducatif
                    {"\n\n"}Podjetniški inkubator Univerze v Mariboru
                    {"\n\n"}Ljubljanski univerzitetni inkubator
                    {"\n\n"}Univerzitetni razvojni center in inkubator Primorske
                </Text>

                <Text style={styles.dmlH3}>Tehnološki parki</Text>
                <Text>Bureaux équipés, mentorat, consultant, Information de coworking</Text>

                <Text style={styles.dmlH3}>Iniciativa Start:up Slovenija</Text>
                <Text>Networking, organisation de la concurrence</Text>

                <Text style={styles.dmlH3}>Mreža European Enterprise Network (EEN)</Text>
                <Text>Recherche de partenaires pour affaires, information, conseils</Text>

                <Text style={styles.dmlH3}>Coworking prostori Coworking MB Hekovnik</Text>
                <Text>Business start-up, mise en réseau, Recherche de partenaires pour affaires, information, counseling, formation</Text>

                <Text style={styles.dmlH3}>Gospodarska zbornica Slovenije</Text>
                <Text>Conseils, formation, assistance avec internationalisation</Text>

                <Text style={styles.dmlH3}>Obrtno podjetniška zbornica Slovenije</Text>
                <Text>Conseils, formation, émission de licences en artisanat, Certificats de l’UE, certificats pour Activités d’artisanat occasionnel dans la République de La Slovénie, licences pour
                    transport</Text>

                <Text style={styles.dmlH3}>Program Erasmus za mlade podjetnike</Text>
                <Text>Cofinancement de formation d’entrepreneuriat - échange d’expérience parmi entrepreneurs au sein de l’UE</Text>
            </View>
        )
    }
}
