import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";

export default class M9S2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Que signifient « achats en ligne »?</Text>
                <Text>Le shopping en ligne est une forme de commerce électronique qui permet aux consommateurs d'acheter directement des biens ou des services auprès d'un vendeur via Internet à l'aide
                    d'un navigateur Web.</Text>

                <Text style={styles.dmlH2}>Qu'est-ce que signifie Marketing en ligne?</Text>
                <Text>Le marketing en ligne est un ensemble d’outils et de méthodologies utilisés pour promouvoir des produits et services via Internet. Le marketing en ligne comprend une gamme plus
                    large d'éléments marketing que le marketing traditionnel, en raison des canaux et des mécanismes de marketing supplémentaires disponibles sur Internet.</Text>

                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/images/qmark2.jpg')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
