import React, {Component} from 'react'
import {Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m6/img20.png')
    }
}];

export default class M6S7 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Créer une signature électronique </Text>
                <Text style={{marginTop: 15}}>De nouvelles opportunités commerciales sont apparues alors que les systèmes de transaction sur papier sont mis en ligne. L'utilisation de votre signature
                    électronique dans des processus basés sur du papier d'entreprise tels que vos factures, contrats, courriers électroniques, etc. est importante pour améliorer votre activité. Il
                    existe de nombreuses plates-formes disponibles au téléchargement depuis les téléphones mobiles pour la signature électronique. L'un d'entre eux est DocuSign. DocuSign est une
                    plate-forme gratuite et pratique pour la signature électronique à utiliser sur les téléphones mobiles. Il est disponible sur: iPhone, iPad, Android et Windows.</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m6/img20.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>
            </View>
        )
    }
}
