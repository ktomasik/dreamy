import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S14_2 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration d'un compte de messagerie sur un téléphone mobile pour une application de messagerie Android (Samsung, Sony, HTC...)</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Félicitations, vous avez configuré votre compte e-mail!</Text>
                <Text style={{color: 'red', marginTop: 50, textAlign: 'center', fontSize: 20}}>Vous pouvez vous inscrire à tous les canaux sociaux avec le compte de messagerie que vous avez créé. Le
                    module suivant expliquera comment configurer vos comptes de médias sociaux.</Text>
            </View>
        )
    }
}
