import React, { Component } from 'react'
import {FlatList, Linking, Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S11 extends Component {
    render () {
        return (
            <View style={styles.section} >
                <Text style={styles.dmlH1}>Où Trouver la Petite entreprise de prêts pour Femmes?</Text>
                <Text style={styles.dmlH2}>Pour la Pologne:</Text>
                <Text style={styles.dmlH3}>mBank</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Prêts pour les dirigeants d\'une entreprise pas plus de 6 mois, jusqu\'à concurrence de 30 000 PLN. Vous pouvez le faire en ligne.',val2: 'https://www.mbank.pl/firmy/kredyty/na-start/kredyty-na-start/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Aasa Polska</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Un prêt pour le développement des entreprises à concurrence de 20 000 PLN. Dès le premier jour d\'opération.',val2:'https://aasadlabiznesu.pl/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>Credit Agricole</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Prêts aux petites et moyennes entreprises.',val2:'https://www.credit-agricole.pl/male-i-srednie-firmy/kredyty/pozyczka-biznes/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>PKO BP</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Un prêt de démarrage pour les entreprises.',val2:'https://www.pkobp.pl/firmy/kredyty/produkty-kredytowe/pozyczka-na-start/'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />

                <Text style={styles.dmlH3}>ING</Text>
                <FlatList
                    data={[
                        {key: {val1: 'Prêt aux entreprises.',val2:'https://www.ingbank.pl/male-firmy/kredyty-i-pozyczki/pozyczka-dla-malych-firm'}},
                    ]}
                    renderItem={({item}) => <Text style={{marginBottom: 10}}>{item.key.val1} <Text style={{color: 'blue'}}
                                                                                                   onPress={() => Linking.openURL(item.key.val2)}>{item.key.val2}</Text></Text>}
                />
            </View>
        )
    }
}
