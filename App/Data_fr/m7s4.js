import React, {Component} from 'react'
import {Text, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S4 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Que sont les business angels?</Text>
                <Text>Les business angels (également appelés anges ou investisseurs providentiels) sont des personnes qui utilisent leur patrimoine personnel pour fournir des capitaux aux entreprises
                    en démarrage ou en démarrage, en échange d’une part des fonds propres de la société. L'afflux de capitaux peut aider une idée à devenir une entreprise viable et à fournir la base
                    pour commencer à produire le produit ou le service proposé.
                    {"\n\n"}La définition de «business Angel» reste floue, avec les termes «business Angel», «investisseur informel» et «capital-risque informel».
                </Text>
                <Text style={styles.dmlH2}>Que peuvent-ils offrir?</Text>
                <Text>Les business angels sont en mesure d'offrir:
                    {"\n"}une injection de liquidités pour des montants relativement modestes qui ne seraient autrement pas disponibles via du capital-risque,
                    {"\n"}souvent faire un suivi avec des tours de financement ultérieurs pour la même entreprise,
                    {"\n"}sont généralement intéressés pour s'impliquer dans le projet en agissant comme guide ou mentor,
                    {"\n"}investissent leur temps et établissent des liens avec leur plus grand réseau afin de guider l’entrepreneur dans la nouvelle entreprise

                </Text>
                <Text style={styles.dmlH2}>Le fonds des business angels des femmes</Text>
                <Text>Le fond des business angels des femmes fait généralement partie d’une association qui est une organisation professionnelle à but non lucratif et non politique, qui soutient la
                    participation des femmes à la prise de décision en matière d’innovation. En tant que business angels, les investissements impliquent non seulement de fournir des ressources
                    financières, mais également de partager connaissances, expériences et capital social. L’Association est convaincue que la participation des femmes à un plus grand nombre et sous de
                    nouvelles formes apporterait des avantages économiques à toutes et à tous.</Text>
            </View>
        )
    }
}
