import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m9/img6.png')
    }
}];

export default class M9S3_9 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Configuration des comptes médias sociaux pour les entreprises </Text>
                <Text style={styles.dmlH3}>Créer une page Facebook pour les entreprises </Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={{marginTop: 10, fontSize: 18}}><Text style={{fontWeight: 'bold'}}>Étape 8:</Text> Ajoutez un bouton d’appel à l’action à votre page Facebook. Grâce à l'appel à l'action
                    des boutons de vos clients peuvent communiquer avec vous par e-mail, téléphone ou site web et ils peuvent faire des achats…
                    {"\n\n"}Pour ajouter un bouton d’appel à l’action à votre page

                </Text>
                <FlatList
                    data={[
                        {key: "Sous la photo de couverture de votre page, cliquez sur + Ajouter un bouton."},
                        {key: "Sélectionnez un bouton dans le menu contextuel et suivez les instructions à l'écran."},
                        {key: "Cliquez sur Terminé."},

                        ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m9/img6.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
