import React, {Component} from 'react'
import {FlatList, Image, Linking, Modal, StyleSheet, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img9.png')
    }
}];

export default class M8S8 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Suivez les étapes indiquées ci-dessous pour envoyer la cargaison:</Text>
                <Text style={styles.dmlH3}>ÉTAPE 1: EMBALLER VOTRE PRODUIT AVEC LES CONDITIONS APPROPRIÉES</Text>
                <View style={styles2.border}>
                    <Text>Astuces pour fabriquer un colis cargo approprié:</Text>
                    <FlatList
                        data={[
                            {key: 'Choisissez le type de paquet approprié,'},
                            {key: 'Choisissez la taille appropriée du paquet,'},
                            {key: 'Couvrir les produits sensibles avec des matériaux mous protecteurs,'},
                            {key: 'Si nécessaire, utilisez des étiquettes de précaution telles que «fragile»,'},
                            {key: 'Si nécessaire, marquez l’angle de la poignée'},

                        ]}
                        renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                    />
                </View>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img9.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    border: {
        marginTop: 15,
        width: 'auto',
        height: 'auto',
        borderWidth: 3,
        borderColor: '#f09609',

        padding: 10
    }
});
