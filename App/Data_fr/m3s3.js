import React, {Component} from 'react'
import {Text, Image, View} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M3S3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}> Qu'est-ce qu'un logo de marque?</Text>
                <Text>Un logo est une marque graphique, un emblème ou un symbole utilisé pour aider et promouvoir la reconnaissance publique. Il peut s'agir d'un dessin abstrait ou figuratif ou
                    inclure le texte du nom qu'il représente sous la forme d'un logotype ou d'un mot-symbole.
                    {"\n"}Une marque c’est toute interaction et pratique marketing d’un nom ou d’un dessin qui identifie et différencie une entreprise, un produit ou un service d’un autre.
                </Text>

                <Text style={styles.dmlH2}>Comment choisir le bon logo</Text>
                <Text>Lorsque vous décidez de créer un logo, vous devez garder à l’esprit certaines choses qui contribueront à donner à votre entreprise l’image de marque qu’elle mérite.
                    {"\n\n"}Si vous souhaitez créer un logo créatif pour votre entreprise, essayez d’abord de vous inspirer du logo en consultant les concepts de marques à succès.
                </Text>

                <Text style={styles.dmlH2}>Stratégie de prix pour le commerce électronique (fixer un prix pour votre produit)</Text>
                <Text>Dans l’environnement du shopping en ligne, la meilleure chose à faire est de voir le prix avec le produit. Il sera plus facile pour le client d’acheter le produit s’il le voit
                    bien conçu sur la plate-forme et à son coût.</Text>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m3/img16.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={styles.dmlH2}>Différents types de stratégies de prix </Text>
                <Text style={styles.dmlH3}>Tarification du commerce électronique basée sur les coûts</Text>
                <Text>La tarification basée sur les coûts peut être le modèle de tarification le plus populaire dans le secteur de la vente au détail, garantissant un retour minimum sur chaque produit
                    vendu.</Text>
                <Text style={styles.dmlH3}>Prix du commerce électronique basé sur la concurrence </Text>
                <Text>Avec une stratégie de prix basée sur la concurrence, vous surveillez simplement ce que vos concurrents directs facturent pour un produit particulier et fixez votre prix par
                    rapport au leur.</Text>
                <Text style={styles.dmlH3}>Tarification du commerce électronique basée sur la valeur </Text>
                <Text>Si vous vous concentrez sur la valeur que vous pouvez offrir à un client, la détermination des prix en fonction de ce que vous percevrez de l’acheteur - dans le secteur de votre
                    secteur d'activité - vous permettra de payer pour un produit particulier à un moment donné, vous avez pris la tarification du commerce électronique basée sur la valeur ou sur
                    l’approche optimisée de la valeur (Roggio, 2017).</Text>
            </View>
        )
    }
}
