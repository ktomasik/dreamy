import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz5": {
            "question1": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Un compte courant n'offre pas d'accès à de l'argent pour les besoins transactionnels quotidiens"
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Les paiements automatiques de factures sont des paiements irréguliers effectués à partir d'un compte bancaire, de courtage ou de fonds communs de placement aux vendeurs."
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Payez en ligne avec les informations de votre carte de crédit",
                    "option2": "b) Payer en argent comptant",
                    "option3": "c) Payer sur un compte bancaire",
                    "option4": "d) Payer une facture"
                },
                "question": "Lequel n'est pas un moyen de faire un paiement en ligne?"
            },
            "question4": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "La banque en ligne désigne toute transaction bancaire pouvant être effectuée sur Internet."
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Choisissez la meilleure banque qui convient le mieux.",
                    "option2": "b) Visitez la banque et demandez à ouvrir un compte.",
                    "option3": "c) Gardez les informations personnelles secrètes auprès de la banque.",
                    "option4": "d) Conservez les documents du compte reçus  en sécurité."
                },
                "question": "Laquelle des instructions suivantes pour ouvrir un compte bancaire est fausse?"
            },
            "question6": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Les cartes de débit retirent de l'argent directement à partir du compte courant lorsque l'achat est comptabilisé immédiatement."
            },
            "question7": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Obtenir un numéro d'identification pour l’entreprise.",
                    "option2": "b) Configuré pour recevoir des paiements via un tiers",
                    "option3": "c) Ouvrir un compte bancaire professionnel.",
                    "option4": "d) Créer un compte de média social"
                },
                "question": "Laquelle des étapes suivantes n'est pas requise pour recevoir des paiements?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "L’émetteur de carte de crédit envoie le relevé de facturation environ une fois par mois."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Créer un simple formulaire de commande sur Facebook",
                    "option2": "b) Configurer les notifications du bon de commande",
                    "option3": "c) Configurer les confirmations du bon de commande",
                    "option4": "d) Configurez les paramètres de paiement"
                },
                "question": "Laquelle des affirmations suivantes est fausse ? Pour recevoir des commandes, il est nécessaire de:"
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Compte marchand",
                    "option2": "b) Terminal virtuel",
                    "option3": "c) Argent liquide",
                    "option4": "d) Passerelle"
                },
                "question": "Pour accepter les paiements par carte de crédit, lequel des éléments suivants n'est pas nécessaire?"
            }
        }
    }
};

export default class M5Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz5;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
