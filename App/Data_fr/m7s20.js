import React, {Component} from 'react'
import {Text, View, Linking, FlatList} from 'react-native'

// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S20 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Incitations fiscales pour Start-up de femmes</Text>
                <Text style={styles.dmlH2}>Pour la France:</Text>
                <Text>Les femmes entrepreneures font l’objet de plusieurs soutiens spécifiques. Ce public d'entrepreneurs fait l'objet d'une attention particulière, afin de les aider à mieux réaliser leur projet de création ou de reprise. Les femmes entrepreneures font face à des situations personnelles parfois plus compliquées ou à une plus grande méfiance externe. Des accompagnements ou une aide spécifique existent pour les aider à faire avancer leur projet de création d’entreprise.
                    {"\n\n"}En termes d’appui, des réseaux spécialisés sont en place depuis quelques années. Le réseau Les Premières a mis en place des incubateurs et des incubateurs d’entreprises, dédiés aux projets de création d’entreprise dirigés par des femmes. Ces projets créatifs doivent avoir un côté innovant. L'intérêt de cette forme d'accompagnement? L'incubateur assure le suivi du créateur de l'entreprise, mais héberge également le projet d'entreprise pendant 1 an. La femme d'affaires créative est donc entourée pour démarrer la première année de son entreprise. Le réseau Force Femmes soutient les femmes dans la seconde moitié de leur carrière, c'est-à-dire plus de 45 ans. Celles qui ont un projet de création ou de reprise d'entreprise sont suivies lors de la préparation de leur projet: validation du projet, élaboration, réalisation du business plan, etc. Actionelles accompagne également les femmes dans leur projet de création d'entreprise. L'association propose, en plus du soutien, notamment une relation entre femmes créatives et propriétaires d'entreprises expérimentés, de briser l'isolement.
                    {"\n\n"}Ces 3 réseaux ne sont pas nécessairement présents sur tout le territoire national.
                    {"\n\n"}Support d'accès au financement pour les femmes entrepreneurs
                    {"\n\n"}En termes d’accès au financement, la Garantie EGALITE Femmes a été créée par France Active. Il s’agit d’une garantie bancaire destinée à faciliter l’obtention de prêts bancaires par les femmes entrepreneurs. Cette garantie peut être mobilisée pour des projets de création, de redressement ou de développement commercial.
                    {"\n\n"}Cet appareil n'est pas exclusif d'autres aides à la création d'entreprise. Mais cela complète le système de soutien aux projets de création ou de prise de contrôle de femmes.
                </Text>

                <FlatList
                    data={[
                        {key: 'https://les-aides.fr/focus/a5Zi/les-aides-pour-les-femmes-creatrices-ou-repreneuses-d-entreprise.html'},
                    ]}
                    renderItem={({item}) => <Text style={{color: 'blue', marginBottom: 10}} onPress={() => Linking.openURL(item.key)}>{item.key}</Text>}
                />
            </View>
        )
    }
}
