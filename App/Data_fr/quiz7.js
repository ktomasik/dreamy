import React, {Component} from 'react'
import {ScrollView, StyleSheet, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
import {quizStyles} from '../Containers/Styles/general';
import {Button} from 'react-native-elements';
import AwesomeAlert from 'react-native-awesome-alerts';
import I18n from "../Services/I18n";

let arrnew = [];
const jsonData = {
    "quiz": {
        "quiz7": {
            "question1": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Plus l’intérêt est faible, plus le prêt doit être fait."
            },
            "question2": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Dans une banque.",
                    "option2": "b) Chambre régionale de Commerce et de l’industrie.",
                    "option3": "c) Prestataire de service Financier.",
                    "option4": "d) Centre d’investissement Entreprise."
                },
                "question": "Où serait le meilleur endroit pour traiter une affaire de prêt?"
            },
            "question3": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Des subventions sont  des fonds payable souvent  par un gouvernement"
            },
            "question4": {
                "correctoption": "option2",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Les incitations  de l’Etat  pour les start-up sont seulement de type non financier"
            },
            "question5": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Promotion d’investissement privé.",
                    "option2": "b) Soutien pour la création et développement de micro-entreprises.",
                    "option3": "c) Fourniture de ressources financières favorables aux entreprises.",
                    "option4": "d) Financier des incitations dans des régimes régionaux garanties."
                },
                "question": "Quelle est la banque la plus typique de soutien   des petites entreprises  pour femmes?"
            },
            "question6": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Un impôt incitatif est une mesure gouvernementale qui encourage les personnes à faire des économies par réduction du montant de l’impôt"
            },
            "question7": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) MSicrocredit",
                    "option2": "b) Finance parapluie",
                    "option3": "c) Microloan",
                    "option4": "d) Aucun"
                },
                "question": "Qu’est ce qui est associé à microfinance?"
            },
            "question8": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Microfinance et capital social ont une importante relation avec l'autonomisation des entreprises."
            },
            "question9": {
                "correctoption": "option1",
                "options": {
                    "option1": "a) Vrai",
                    "option2": "b) Faux"
                },
                "question": "Les Business angels sont généralement intéressés à s'impliquer dans le projet en agissant comme un guide ou un mentor."
            },
            "question10": {
                "correctoption": "option3",
                "options": {
                    "option1": "a) Un Crédit",
                    "option2": "b) L’Équilibre",
                    "option3": "c) Un Prêt",
                    "option4": "d) Un Microcrédit"
                },
                "question": "____________ est quand vous emprunter l'argent à une banque ou autre prêteur."
            }
        }
    }
};

export default class M7Q1 extends Component {
    constructor(props) {
        super(props);
        this.qno = 0;
        this.score = 0;

        const jdata = jsonData.quiz.quiz7;
        arrnew = Object.keys(jdata).map(function (k) {
            return jdata[k]
        });
        this.state = {
            question: arrnew[this.qno].question,
            options: arrnew[this.qno].options,
            correctoption: arrnew[this.qno].correctoption,
            countCheck: 0,
            showAlertTrue: false,
            showAlertFalse: false
        }
    }

    showAlertT = () => {
        this.setState({
            showAlertTrue: true,
        });
    };
    showAlertF = () => {
        this.setState({
            showAlertFalse: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlertTrue: false,
            showAlertFalse: false
        });
    };

    _next() {
        if (this.qno < arrnew.length - 1) {
            this.qno++;

            this.setState({
                countCheck: 0,
                question: arrnew[this.qno].question,
                options: arrnew[this.qno].options,
                correctoption: arrnew[this.qno].correctoption,
            })
        } else {
            this.props.quizFinish(this.score * 100 / 10)
        }
    }

    _answer(ans) {
        if (ans === this.state.correctoption) {
            this.score += 1;
            this.showAlertT();
        } else {
            this.showAlertF();
        }
    }

    render() {
        let _this = this;
        const currentOptions = this.state.options;
        const options = Object.keys(currentOptions).map(function (k) {
            return (<View key={k} style={{margin: 10}}>
                <Button onPress={() => {
                    _this._answer(k);
                }} title={currentOptions[k]} textStyle={{textAlign: 'center'}}
                        buttonStyle={{backgroundColor: '#1ba1e2', minHeight: 45}}/>
            </View>)
        });
        const {showAlertTrue} = this.state;
        const {showAlertFalse} = this.state;

        return (
            <View style={styles.mainContainer}>
                <ScrollView style={{paddingTop: 30}}>

                    <View style={quizStyles.container}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: "space-between",
                            alignItems: 'center',
                        }}>

                            <View style={styles2.oval}>
                                <Text style={{
                                    color: 'black',
                                    textAlign: 'center'
                                }}>{I18n.t('_question')}: {this.qno + 1}</Text>
                                <Text style={styles2.welcome}>
                                    {this.state.question}
                                </Text>
                            </View>
                            <View style={{marginTop: 35}}>
                                {options}
                            </View>
                        </View>
                        <AwesomeAlert
                            show={showAlertTrue}
                            showProgress={false}
                            title={I18n.t('_correct')}
                            message={I18n.t('_correctFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="green"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: 'green', fontWeight: 'bold'}}
                        />
                        <AwesomeAlert
                            show={showAlertFalse}
                            showProgress={false}
                            title={I18n.t('_incorrect')}
                            message={I18n.t('_incorrectFeedback')}
                            closeOnTouchOutside={false}
                            closeOnHardwareBackPress={false}
                            showCancelButton={false}
                            showConfirmButton={true}
                            confirmText="OK"
                            confirmButtonColor="#DD6B55"
                            onConfirmPressed={() => {
                                this.hideAlert();
                                this._next();
                            }}
                            titleStyle={{color: '#DD6B55', fontWeight: 'bold'}}
                        />
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles2 = StyleSheet.create({
    oval: {
        borderRadius: 20,
        backgroundColor: 'green',
        padding: 10,
        marginRight: 15,
        marginLeft: 15,
    },
    container: {
        flex: 1,
        alignItems: 'center'
    },
    welcome: {
        fontSize: 20,
        margin: 15,
        color: "white",
        textAlign: 'center'
    }
});
