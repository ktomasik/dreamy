import React, {Component} from 'react'
import {Image, Linking, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M1S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Rechercher sur Google et créer un compte e-mail depuis un smartphone</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 4: Ensuite, écrivez «Je veux créer un compte de messagerie» dans votre propre langue</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m1/img4.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
