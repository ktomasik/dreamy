import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M2S30 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Suivez les étapes ci-dessous pour créer un compte Instagram</Text>
                <Text style={{marginTop: 10, fontSize: 18}}>Étape 17: Sauvegardez les données du profil en cliquant sur Sauvegarder.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 320, height: 400}}
                        source={require('../Images/m2/img57.png')}
                        resizeMode="contain"/>
                    <Image
                        style={{flex: 1, width: 320, height: 400, marginTop: 15}}
                        source={require('../Images/m2/img58.png')}
                        resizeMode="contain"/>
                </View>
                <Text style={{marginTop: 15}}>… Votre profil Instagram est complet et vous êtes prêt à poster</Text>
            </View>
        )
    }
}
