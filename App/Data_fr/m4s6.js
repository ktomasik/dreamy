import React, {Component} from 'react'
import {FlatList, Text, View} from 'react-native'
import styles from "../Containers/Styles/LaunchScreenStyles";
// Styles

export default class M4S6 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2}>Bases fiscales </Text>
                <Text>L'impôt à payer est la somme d'argent que doit une personne aux autorités (Cameron 2017). Le gouvernement utilise les paiements d'impôt pour financer les programmes sociaux et
                    les rôles administratifs.
                    {"\n\n"}Fondamentalement, une obligation fiscale correspond généralement à un certain pourcentage du revenu et varie en fonction du revenu.

                </Text>
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la Turquie</Text>
                <FlatList
                    data={[
                        {key: "Il est nécessaire de faire une déclaration d'adresse. Une adresse personnelle peut être une adresse de facturation."},
                        {key: "Lors de l’application, la taxation simple peut être rabaissée."},
                        {key: "Après évaluation du bureau des taxes, le bureau enverra la carte de taxe au contribuable"},
                        {key: "L'enregistrement doit être effectué auprès de la Confédération des artisans et commerçants turcs."},
                        {key: "L'application doit être faite à la municipalité avec une pétition pour un permis de travail"},
                        {key: "Les contribuables assujettis à la fiscalité des petites entreprises se procureront les documents auprès des chambres de commerce ou des associations."},
                        {key: "Les contribuables doivent déclarer leurs revenus par déclaration annuelle."},
                        {key: "La déclaration sera faite au bureau des impôts enregistré. En conséquence, le contribuable paiera la taxe sur la base de la facture (Rapport national Turquie, 2018)."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la SLOVENIE</Text>
                <FlatList
                    data={[
                        {key: "TVA (taxe sur la valeur ajoutée)"},
                        {key: "Impôt sur les sociétés"},
                        {key: "Impôt sur le revenu"},
                        {key: "Cotisations de sécurité sociale"},
                        {key: "Taxe de transfert de propriété immobilière"},
                        {key: "Impôt sur les gains en capital"},
                        {key: "Choix de la forme de l'entreprise;"},
                        {key: "Les entrepreneurs personnels, les impôts sont un revenu d'une activité, qui est dérivé de la performance de toute activité entrepreneuriale agricole, forestière, professionnelle ou d'une autre activité indépendante."},
                        {key: "Société à responsabilité limitée, est une personnalité juridique et soumise à l'impôt sur les sociétés"},
                        {key: "Les taxes sur les e-vendeurs, qui sont des assujettis, sont identifiées à des fins de TVA et doivent calculer la TVA à partir de la livraison du produit au client en Slovénie (Rapport national Slovénie, 2018)"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />

                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la POLOGNE</Text>
                <Text>Pour les particuliers qui souhaitent gérer leur propre entreprise, les impôts à payer sont payés sur la base d'un impôt sur le revenu, comme c'est le cas pour les employés à
                    temps plein. Un individu doit en choisir un:</Text>
                <FlatList
                    data={[
                        {key: "Taxe de Règles générales,"},
                        {key: "Taxe de 19% (taxe forfaitaire),"},
                        {key: "Montant forfaitaire du revenu enregistré,"},
                        {key: "Carte d'impôt (rapport national Pologne, 2018)"},
                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la GRECE</Text>
                <FlatList
                    data={[
                        {key: "Les particuliers et les entreprises doivent soumettre une déclaration de taxe électronique via le système en ligne de l'autorité indépendante du revenu public, sur la base de laquelle ils seront taxés."},
                        {key: "À la fin de chaque mois, ils soumettent la liste des factures et la TVA."},
                        {key: "Pour les particuliers qui souhaitent gérer leur propre entreprise, les impôts à payer sont payés sur la base d’un impôt sur le revenu."},
                        {key: "Comme pour les employés à plein temps sans déduction fiscale."},
                        {key: "Indépendamment des bénéfices, toutes les entités grecques sont taxées au taux de 29%."},
                        {key: "Les actions sont taxées au taux de 15% (Rapport national Grèce, 2018)"},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text style={{color: 'green', textAlign: 'center', fontWeight: 'bold', marginBottom: 10, marginTop: 20, fontSize: 17}}>Pour la FRANC</Text>
                <Text>La taxation des bénéfices dépend de la structure juridique de l'entreprise. Les entités peuvent être soumises à l'impôt sur le revenu (IR) ou à l'impôt sur les sociétés (CI).
                    {"\n\n"}Les entreprises sont soumises à
                </Text>
                <FlatList
                    data={[

                        {key: "La taxation de ses bénéfices,"},
                        {key: "Contribution économique territoriale (CET),"},
                        {key: "T.V.A."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <Text>Les entreprises individuelles (artisans, commerçants), les professions libérales et EURL (société à responsabilité limitée à un homme) doivent payer le montant de l'indemnité.
                    Les associés sont imposés personnellement au titre de l’impôt sur le revenu uniquement sur les salaires ou les dividendes
                    (Rapport national France, 2018)
                </Text>
            </View>
        )
    }
}
