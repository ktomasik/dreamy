import React, {Component} from 'react'
import {Image, Text, View} from 'react-native'
// Styles
import styles from '../Containers/Styles/LaunchScreenStyles'

export default class M7S29_3 extends Component {
    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH1}>Financement pas à pas dans les pays partenaires du projet</Text>
                <Text style={styles.dmlH2}>Pour la Grèce:</Text>

                <Text style={styles.dmlH3}>Étape 3:</Text>
                <Text>Après la première réunion, l’élaboration et la finalisation du plan d’entreprise suivent avec la coopération d’un consultant compétent. Cette étape peut nécessiter plus d'une
                    réunion, en fonction du degré de préparation de l'homme d'affaires concerné et des besoins de son entreprise.</Text>
                <View style={{alignItems: 'center'}}>
                    <Image
                        style={{flex: 1, width: 240, height: 240}}
                        source={require('../Images/FinanceIcon/iconfinder_37_3319607.png')}
                        resizeMode="contain"/>
                </View>
            </View>
        )
    }
}
