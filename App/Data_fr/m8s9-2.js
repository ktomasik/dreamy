import React, {Component} from 'react'
import {FlatList, Image, Modal, Text, TouchableHighlight, View} from 'react-native'

import styles from "../Containers/Styles/LaunchScreenStyles";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [{
    url: '',
    props: {
        source: require('../Images/m8/img13.png')
    }
}];

export default class M8S9_2 extends Component {

    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    render() {
        return (
            <View style={styles.section}>
                <Text style={styles.dmlH2noBorder}>Suivez les étapes indiquées ci-dessous pour envoyer la cargaison:</Text>
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={this.state.modalVisible}
                    onRequestClose={() =>
                        this.setModalVisible(!this.state.modalVisible)
                    }>

                    <ImageViewer imageUrls={images}/>
                    <TouchableHighlight onPress={() => {
                        this.setModalVisible(!this.state.modalVisible);
                    }} style={{padding: 15}}>
                        <Text style={{textAlign: 'center'}}>Close Window</Text>
                    </TouchableHighlight>
                </Modal>
                <Text style={styles.dmlH3}>ÉTAPE 6: DEMANDEZ LA FACTURE</Text>
                <Text>La facture comprend des informations comme ci-dessous:</Text>
                <FlatList
                    data={[
                        {key: "Nom, adresse, téléphone, etc. de l'expéditeur"},
                        {key: "Nom du destinataire, adresse, téléphone, etc."},
                        {key: "Détails de la taille et du type de cargaison tels que la quantité, les dimensions, etc.,"},
                        {key: "Détails du paiement (destinataire / expéditeur),"},
                        {key: "Date, détails de la facture et numéro de suivi."},

                    ]}
                    renderItem={({item}) => <Text style={{marginLeft: 10, marginTop: 5}}>{item.key}</Text>}
                />
                <TouchableHighlight onPress={() => {
                    this.setModalVisible(true);
                }} style={{alignItems: 'center', marginTop: 5}}>
                    <Image
                        style={{flex: 1, width: 300, height: 200}}
                        source={require('../Images/m8/img13.png')}
                        resizeMode="contain"/>
                </TouchableHighlight>

            </View>
        )
    }
}
